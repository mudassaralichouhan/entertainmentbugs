<?php

namespace App;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Payout extends Model
{
    protected $fillable =["user_id","amount","transaction_id","is_pending","status","created_at","updated_at"];
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
