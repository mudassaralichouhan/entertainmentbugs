<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
	protected $commands = [
		Commands\DistributeRewardCommand::class,
		Commands\UpdateWatchVideoTime::class,
		Commands\UpdateVideoTotalEarning::class,
		Commands\UpdateAds::class,
        \App\Console\Commands\UpdateRewardVideoTotalEarning::class,
	];

	protected function schedule(Schedule $schedule)
	{
		// $schedule->command('inspire')
		//          ->hourly();
		$schedule
			->command('update_watch_video_time')
			->cron("0 */3 * * *")
			->appendOutputTo(storage_path("logs/update_watch_video_time.log"));

		$schedule
			->command('update_video_total_earning')
			->cron("0 0 * * *")
			->appendOutputTo(storage_path("logs/update_video_total_earning.log"));

		$schedule
			->command('update_ads')
			->cron("0 0 * * *")
			->appendOutputTo(storage_path("logs/update_ads.log"));

		$schedule
			->command('reward:distribute')
			->cron("0 0 * * 0")
			->appendOutputTo(storage_path("logs/rewards.log"));
	}

	protected function commands()
	{
		require base_path('routes/console.php');
	}
}
