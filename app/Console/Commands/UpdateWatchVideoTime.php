<?php

namespace App\Console\Commands;

use App\WatchTrack;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateWatchVideoTime extends Command
{
	protected $signature = 'update_watch_video_time';

	protected $description = 'This is to update watch time.';

	public function __construct()
	{
		parent::__construct();
	}

	public function handle() {
		//
		$pathToData = base_path("data/");
		$dirs = ["video", "reward-video"];
		$date = Carbon::now()->format('Y-m-d H:i:s');
		$this->comment("Run at: $date.");

		foreach ($dirs as $dir) {
			$dir_name = $pathToData.$dir;
			if (!is_dir($dir_name)) continue;
			$scanned = scandir($dir_name);

			foreach ($scanned as $userDirectory) {
				if ($userDirectory == "." || $userDirectory == "..") continue;
				$pathToUserDirectory = $dir_name."/".$userDirectory;
				if (!is_dir($pathToUserDirectory)) continue;

				$files = scandir($pathToUserDirectory);
				foreach( $files as $file ){
					if($file == "." || $file == "..") continue;
					$pathToFile = $pathToUserDirectory . "/" . $file;
					if (!file_exists($pathToFile)) continue;
					if (!preg_match("/(.*)\.json$/", $pathToFile)) continue;
					$content = file_get_contents($pathToFile);
					if (empty($content) || !preg_match("/^\[(.*)\]$/", $content)) continue;
					$video_id = str_replace(".json", "", $file);

					$watchTrack = new WatchTrack();
					$watchTrack->video_id = $video_id;
					$watchTrack->user_id = $userDirectory;
					$watchTrack->ip = "";
					$watchTrack->session = "";
					$watchTrack->agent = "";
					$watchTrack->track = $content;
					$watchTrack->is_paid = 0;
					$watchTrack->created_at = $date;
					$watchTrack->updated_at = $date;
					$watchTrack->save();
					unlink($pathToFile);
					$this->comment("successfully executed for $dir $userDirectory $video_id at $date.");
				}
			}
		}
	}
}