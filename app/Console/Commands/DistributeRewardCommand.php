<?php

namespace App\Console\Commands;

use App\Models\RewardPrize;
use App\Models\Video;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DistributeRewardCommand extends Command
{
	protected $signature = 'reward:distribute';

	protected $description = 'Distribute reward at the end of every week';

	private $startOfPreviousWeek;
	private $endOfPreviousWeek;
	private $ranks;

	public function __construct()
	{
		parent::__construct();
		$this->ranks = collect([]);
	}

	public function handle()
	{
		$this->startOfPreviousWeek = Carbon::now()->subWeek()->startOfWeek();
		$this->endOfPreviousWeek = Carbon::now()->subWeek()->endOfWeek();

		Video::where(function($query) {
			$query->where('isRewardVideo', 1)
				->where('created_at', '>=', $this->startOfPreviousWeek)
				->where('created_at', '<=', $this->endOfPreviousWeek);
		})->chunk(100, function($videos) {
			foreach ($videos as $video) {
				$totalViews = $video->getUniqueViews($this->startOfPreviousWeek, $this->endOfPreviousWeek);

				if ($totalViews <= 0) continue;
				if ($this->ranks->count() == 100) {
					$isRankable = false;

					$this->ranks->each(function($data) use (&$isRankable, $totalViews) {
						if ($data['totalViews'] < $totalViews) {
							$isRankable = 1;
						}
					});

					if ($isRankable) {
						$this->ranks->pop();
						$this->ranks->push([
							'video_id' => $video->id,
							'user_id' => $video->user_id,
							'totalViews' => $totalViews
						]);
					}

					continue;
				}

				$this->ranks->push([
					'video_id' => $video->id,
					'user_id' => $video->user_id,
					'totalViews' => $totalViews
				]);
			}
		});

		if ($this->ranks->count() > 0) {
			$prizes = RewardPrize::first();
			$defaultPrize = false;
			
			if ($prizes) {
				$prizes = json_decode($prizes->prizes, true);
			}

			if (!is_array($prizes) || count($prizes) == 0) {
				$defaultPrize = true;
			} else {
				$prizeKeys = array_keys($prize);
			}
			
			DB::transaction(function () use ($prizes, $defaultPrize, $prizeKeys) {
				$rank = 0;
				$this->ranks
					->sortByDesc('totalViews')
					->values()
					->pluck('video_id', 'user_id')
					->chunk(20)
					->each(function($rankers) use (&$rank, $prizes, $defaultPrize, $prizeKeys) {
						$walletCases = "";
						$walletIds = [];
						$walletParams = [];
						
						$rankers->each(function($videoId, $userId) use (&$walletCases, &$walletIds, &$walletParams, &$rank, $prizes, $defaultPrize, $prizeKeys) {
							$prize = 0;
							if(!$defaultPrize) $prize = $prizes[$prizeKeys[$rank]];

							$walletCases .= "WHEN user_id=? THEN `balance` + ? ";
							array_push($walletIds, "?");
							array_push($walletParams, $userId, $prize, $userId);
							$rank = $ranks + 1;
						});

						if($walletCases) {
							DB::update("UPDATE `wallet` SET `balance` = CASE $walletCases END WHERE `user_id` IN (".implode(",", $walletIds).")", $walletParams);
						}
					});
			}, 5);
		}
	}
}
