<?php

namespace App\Console\Commands;

use App\Models\Video;
use App\PageViewTrack;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class VideoStatisticsCommand extends Command
{
	protected $signature = 'video:statistics';

	protected $description = 'This command helps you generate video statistics on daily basis';

	public function __construct()
	{
		parent::__construct();
	}

	private $rewardVideoURL;
	private $videoURL;

	public function handle()
	{
		$currentTime = Carbon::now()->format("Y-m-d H:i:s");
		$this->comment("Run at $currentTime");
		$domain = config("session.domain");
		$this->rewardVideoURL = "$domain/watch-reward/";
		$this->videoURL = "$domain/watch/";

		PageViewTrack::where(function($query) {
			$query->where('url', 'like', "%$this->videoURL%")
				->orWhere('url', 'like', "%$this->rewardVideoURL%");
		})->chunk(100, function($tracks) {
//			DB::transaction(function () use ($tracks) {
//				$totalUniqueUsers = 0;
//				$totalUsers = 0;

//				$watchKeys = collect([]);
//				foreach ($tracks as $track) {
//					if (preg_match("/https?:\/\/($this->rewardVideoURL|$this->videoURL)\/(.+?)(?=\?|$)/", $track->url, $match)) {
//						if (isset($match[2]) && !$watchKeys->search($match[2])) {
//							$watchKeys->push($match[2]);
//						}
//					}
//				}

//				if ($watchKeys->count() > 0) {
//					$videos = Video::whereIn('video_key', $watchKeys->all())->get();
//
//					if ($videos->count()) {
//						foreach ($videos as $video) {
//
//						}
//					}
//				}
//			});
		});
	}
}
