<?php

namespace App\Console\Commands;

use App\Helpers\VideoHelper;
use App\Models\Video;
use App\WatchTrack;
use App\WatchVideoEarning;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class UpdateRewardVideoTotalEarning extends Command
{
    protected $signature = 'update:update-reward-video-total-earning';

	protected $description = 'update reward videos earning command';

	public function __construct()
	{
		parent::__construct();
	}

	public function handle()
    {
        [$toSunday, $fromSunday] = get_second_last_week();
        $result = VideoHelper::prices();

        $watchTracks = Video::select([
            'videos.id',
            'videos.video_key',
            'videos.title',
            'videos.video_thumb_id',
            'videos.video_time',
            'videos.user_id',
            'videos.created_at',
            'watch_track.id as track_id'
        ])
            ->leftJoin('watch_track', 'videos.id', '=', 'watch_track.video_id')
            ->leftJoin('watch_histories', 'videos.id', '=', 'watch_histories.video_id')
            ->where([
                ['videos.isRewardVideo', 1],
                ['videos.isDeleted', 0],
                ['watch_track.is_calculated', 0],
                ['videos.created_at', '<=', $toSunday->format("Y-m-d 00:00:00")],
                ['videos.created_at', '>=', $fromSunday->format("Y-m-d 00:00:00")]
            ])
            ->selectRaw('COUNT(DISTINCT watch_histories.user_id) AS total_views_count')
            ->selectRaw('(SELECT SUM(wt.track) FROM watch_track wt WHERE wt.video_id = videos.id) AS total_track_sum')
            ->groupBy('videos.id')
            ->orderBy('total_track_sum', 'desc')
            ->get();

        foreach ($watchTracks ?? [] as $idx => $watch) {
            (new WatchVideoEarning([
                'video_id' => $watch->id,
                'user_id' => $watch->user_id,
                'total_watch_time' => $watch->total_track_sum,
                'unpaid_watch_time' => $watch->total_track_sum,
                'unpaid_earning' => $result[$idx + 1] ?? 0,
                'total_earning' => $result[$idx + 1] ?? 0,
                'total_views' => $watch->total_views_count,
            ]))->save();

            WatchTrack::where('id', $watch->track_id)
                ->update([
                    'is_calculated' => 1,
                ]);
        }
	}
}