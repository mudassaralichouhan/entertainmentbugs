<?php

namespace App\Console\Commands;

use App\Helpers\VideoHelper;
use App\WatchVideoEarning;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\Video;
use App\WatchTrack;
use Illuminate\Support\Facades\DB;

class UpdateVideoTotalEarning extends Command
{
	protected $signature = 'update_video_total_earning';

	protected $description = 'Update video update time on specified interval basis';

	private $watchVideoEarningTable;

	public function __construct()
	{
		parent::__construct();
	}

	public function handle() {
		$this->comment("Start Command on ".Carbon::now().".");
		$this->watchVideoEarningTable = (new WatchVideoEarning())->getTable();

		Video::select("id", "user_id")->chunk(20, function($videos) {
			DB::transaction(function () use ($videos) {
				$time = Carbon::now();
				$earningVideos = collect([]);
				$trackIds = collect([]);

				foreach ($videos as $video) {
					$watchTrack = $video->watchTrack(true);
					if (!is_object($watchTrack) || $watchTrack->count() == 0) continue;
	
					$track = VideoHelper::getCalculatedTracks($watchTrack);
					$earningVideos->push([
						'video_id' => $video->id,
						'user_id' => $video->user_id,
						'total_watch_time' => $track->get('totalSeconds'),
						'unpaid_watch_time' => $track->get('totalUnpaidSeconds'),
						'unpaid_earning' => $track->get('totalEarning'),
						'total_earning' => $track->get('totalUnpaidEarning'),
						'total_views' => $track->get('totalViews'),
						'created_at' => $time->format("Y-m-d H:i:s"),
						'updated_at' => $time->format('Y-m-d H:i:s')
					]);

					$trackIds->merge($track->get('ids'));
				}

				if ($trackIds->count() > 0) {
					$trackIds->chunk(20)->each(function($ids) {
						WatchTrack::whereIn('id', $ids->all())->delete();
					});
				}

				$existingVideos = WatchVideoEarning::whereIn('video_id', $earningVideos->pluck('video_id'))->get();
				$existingVideoIds = $existingVideos->pluck('video_id')->toArray();

				$earningVideos->filter(function($video) use ($existingVideoIds) {
					return !in_array($video->video_id, $existingVideoIds);

				})->pipe(function($videos) {
					if ($videos->count() > 0) DB::table($this->watchVideoEarningTable)->insert($videos);
				});

				$totalWatchTime = [];
				$unpaidWatchTime = [];
				$totalEarning = [];
				$unpaidEarning = [];
				$totalViews = [];
				$totalWatchTimeParams = [];
				$unpaidWatchTimeParams = [];
				$totalEarningParams = [];
				$unpaidEarningParams = [];
				$totalViewsParams = [];
				$ids = [];

				if ($existingVideos->count() > 0) {
					foreach ($existingVideos as $video) {
						foreach ($earningVideos as $v) {
							if ($v->video_id == $video->video_id) {
								array_push($totalWatchTime, "WHEN `id` = ? THEN `total_watch_time` + ?");
								array_push($totalWatchTimeParams, $video->id, $v->total_watch_time);

								array_push($unpaidWatchTime, "WHEN `id` = ? THEN `unpaid_watch_time` + ?");
								array_push($unpaidWatchTimeParams, $video->id, $v->unpaid_watch_time);

								array_push($totalEarning, "WHEN `id` = ? THEN `total_earning` + ?");
								array_push($totalEarningParams, $video->id, $v->total_earning);

								array_push($unpaidEarning, "WHEN `id` = ? THEN `unpaid_earning` + ?");
								array_push($unpaidEarningParams, $video->id, $v->unpaid_earning);

								array_push($totalViews, "WHEN `id` = ? THEN `total_views` + ?");
								array_push($totalViewsParams, $video->id, $v->total_views);

								array_push($ids, $video->id);
								break;
							}
						}
					}

					$totalWatchTimeCases = implode(",", $totalWatchTime);
					$unpaidWatchTimeCases = implode(",", $unpaidWatchTime);
					$totalEarningCases = implode(",", $totalEarning);
					$unpaidEarningCases = implode(",", $unpaidEarning);
					$totalViewsCases = implode(",", $totalViews);
					$idIn = implode(",", array_fill(0, count($ids), "?"));

					DB::update("UPDATE $this->watchVideoEarningTable SET `total_watch_time` = CASE $totalWatchTimeCases END, `unpaid_watch_time` = CASE $unpaidWatchTimeCases END, `total_earning` = CASE $totalEarningCases END, `unpaid_earning` = CASE $unpaidEarningCases END, `total_views` = CASE $totalViewsCases END WHERE `id` IN ($idIn)", array_merge($totalWatchTimeParams, $unpaidWatchTimeParams, $totalEarningParams, $unpaidEarningParams, $totalViewsParams, $ids));
				}
			});
		});

		$this->comment("End process at ".Carbon::now());
	}
}