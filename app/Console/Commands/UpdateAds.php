<?php

namespace App\Console\Commands;

use App\Models\PromotePostModel;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class UpdateAds extends Command
{
	protected $signature = 'update_ads';

	protected $description = 'Run everday at 00:00 IST update ads status and user wallet';

	public function __construct() {
		parent::__construct();
	}

	public function handle() {
		$OFFSET = 0;
		$LIMIT = 5000;
		$time = Carbon::now();
		$this->comment("Start Command on $time.");

		$totalPromotePosts = PromotePostModel::where(['status' => 1])->count();
		$this->comment("Total posts: $totalPromotePosts");

		if($totalPromotePosts == 0){
			$this->comment("End process at ".Carbon::now());
			return;
		}

		if($LIMIT > $totalPromotePosts){
			$LIMIT = $totalPromotePosts;
		}

		while ($totalPromotePosts > $OFFSET) {
			$promote_posts = PromotePostModel::where(['status' => 1])->paginate($OFFSET, $LIMIT);

			if($promote_posts){
				$posts = $promote_posts->filter(function($post) use ($time){
					if(Carbon::parse($post->created_at)->startOfDay()->addDays($post->days)->timestamp >= $time){
						return false;
					}

					return true;
				});

				if ($posts->count() > 0) {
					$postIds = $posts->pluck('id')->all();
					PromotePostModel::whereIn('id', $postIds)->update(['status' => 2]);

					$totalPosts = count($postIds);
					$this->comment("Change status for $totalPosts posts.");

					$cases = [];
					$params = [];
					$ids = [];

					$transactions = [];
					foreach ($posts as $post) {
						$amountOnClicks = $post->clicks * $post->clicks_range;
						$amountOnViews = $post->views * $post->views_range;

						if (($amountOnClicks + $amountOnViews) < $post->amount) {
							$addAmountToWallet = $post->amount - ($amountOnClicks + $amountOnViews);
							$cases[] = "WHEN `user_id` = {$post->user_id} THEN ?";
							$params[] = $addAmountToWallet;
							$ids[] = $post->user_id;
							$transactions[] = [
								'user_id' => $post->user_id,
								'type' => "credit-ad",
								'txn_id' => "ret_".$post->id,
								'amount' => $addAmountToWallet,
								'currency' => "INR",
								'source' => "ebugs",
								'details' => "{}",
								'status' => "1",
								'created_at' => Carbon::now(),
								'updated_at' => Carbon::now(),
								'related_id' => $post->id
							];
						}
					}

					if(count($ids) > 0){
						$ids = implode(",", $ids);
						$cases = implode(",", $cases);
						$params[] = $time;

						DB::update("UPDATE `wallet` SET `balance` = CASE {$cases} END, `updated_at` = ? WHERE `user_id` IN ({$ids})", $params);
						DB::table('transactions')->insert($transactions);
					}
				}
			}

			$OFFSET += $LIMIT;
		}

		$this->comment("End process at ".Carbon::now());
	}
}
