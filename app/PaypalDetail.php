<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaypalDetail extends Model
{
    protected $fillable = ["paypal_id",'created_at','updated_at'];
}
