<?php

namespace App\Listeners;

use App\Events\PageViewEvent;
use App\PageViewTrack;
use Carbon\Carbon;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PageViewListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PageViewEvent  $event
     * @return void
     */
    public function handle(PageViewEvent $event)
    {
        $url = $event->url;
        if( !preg_match("/^https?:\/\/(.+)\/public\/(.+)$/", $url) )
        {
            $request = request();
            $allowed = false;
            if( preg_match("/^https?:\/\/(.+)\/photos-details$/", $url) || preg_match("/^https?:\/\/(.+)\/photos-details\/mobile$/", $url) ){
                if( $request->input('value') ){
                    $url = url("single-photo/".$request->input('value'));
                    $allowed = true;
                }
            }

            if( !$request->ajax() || $allowed ){
                $pageViewTrack = new PageViewTrack();
                $pageViewTrack->url = $url;
                $pageViewTrack->ip = $request->getClientIp();
                $pageViewTrack->user_id = session()->get('user_id') ?? '';
                $pageViewTrack->location = "";
                $pageViewTrack->created_at = Carbon::now()->format("Y-m-d H:i:s");
                $pageViewTrack->updated_at = Carbon::now()->format("Y-m-d H:i:s");
                $pageViewTrack->save();
            }
        }
    }
}
