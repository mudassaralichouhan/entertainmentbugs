<?php
namespace App\Observers;

use App\Notifications\VideoUploaded;
use App\Models\Video;

class VideoObserver
{
    public function created(Video $video)
    {
        $user = $video->user;
        foreach ($user->followers as $follower) {
            $follower->notify(new VideoUploaded($user, $video));
        }
    }
}