<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\BroadcastMessage;

use App\Models\User;
use App\Models\Video;

class VideoUploaded extends Notification implements ShouldQueue
{
    use Queueable;
    protected $video;
    protected $following;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $following, Video $video)
    {
        $this->following = $following;
        $this->video = $video;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting($this->details['greeting'])
                    ->line($this->details['body'])
                    ->line($this->details['thanks']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'user_id'       => $this->following->id,
            'video_id'      => $this->video->id,
            'user_name'     => $this->following->name,
            'user_photo'    => $this->following->photo,
            'video_key'     => $this->video->video_key,
            'title'         => $this->video->title,
            'type'          => $this->video->isRewardVideo == 1 ? 'reward-video-uploaded' : 'video-uploaded',
            'message'       => $this->video->isRewardVideo == 1 ? 'reward video uploaded successfully' : 'video uploaded successfully'
        ];
    }
}
