<?php

namespace App\Notifications;

use App\UploadPhotos;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PhotoUploaded extends Notification
{
    use Queueable;
    protected $user;
    protected $photo;
    protected $isUpdated;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(\App\Models\User $user, UploadPhotos $photo, $isUpdated = false)
    {
        //
        $this->user = $user;
        $this->photo = $photo;
        $this->isUpdated = $isUpdated;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        if ( !$this->isUpdated ){
            return [
                'user_id'   => $this->user->id,
                'photo_id'  => $this->photo->id,
                'type'      => 'photo-updated',
                'message'   => 'photo updated successfully'
            ];
        } else {
            return [
                'user_id'   => $this->user->id,
                'photo_id'  => $this->photo->id,
                'type'      => 'photo-uploaded',
                'message'   => 'photo uploaded successfully'
            ];
        }
    }
}
