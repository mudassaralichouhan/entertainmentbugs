<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\User;
use App\Models\UploadPhotos;

class Notices extends Notification
{
    use Queueable;
    protected $photo;
    protected $user;
    protected $other_user;
    protected $message;
    protected $type;
    protected $comment;
    protected $ratings;
    protected $following_id;
    protected $action_id;
    /**
     * Create a new notification instance.
    *
     * @return void
     */
    public function __construct(User $user, $message, $type, $comment = '', $ratings = '', $following_id = '', $action_id = "")
    {
        $this->user = $user;
        $this->photo = $user->photo;
        $this->message = $message;
        $this->type = $type;
        $this->comment = $comment;
        $this->ratings = $ratings;
        $this->following_id = $following_id;
        $this->action_id = $action_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // return (new MailMessage)
        //             ->line('The introduction to the notification.')
        //             ->action('Notification Action', url('/'))
        //             ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        if($this->type == 'following-post') {
            return [
                'user_id'   => $this->user->id,
                'user_name' => $this->user->name,
                'user_photo' => $this->user->photo,
                'message'       => $this->message,
                'type' => $this->type,
                'comment' => $this->comment,
                'following_id' => $this->following_id,
                'action_id'     => $this->action_id
            ];
        }

        if($this->following_id) {
            return [
                'user_id'   => $this->user->id,
                'user_name' => $this->user->name,
                'user_photo' => $this->user->photo,
                'message'       => $this->message,
                'type' => $this->type,
                'comment' => $this->comment,
                'rating'    => $this->ratings,
                'following_id' => $this->following_id,
                'action_id'     => $this->action_id
            ];
        }

        if($this->comment) {
            return [
                'user_id'   => $this->user->id,
                'user_name' => $this->user->name,
                'user_photo' => $this->user->photo,
                'message'       => $this->message,
                'type' => $this->type,
                'comment' => $this->comment,
                'action_id'     => $this->action_id
            ];
        }
        
        if($this->ratings) {
            return [
                'user_name' => $this->user->name,
                'user_photo' => $this->user->photo,
                'message'       => $this->message,
                'type' => $this->type,
                'rating' => $this->ratings,
                'action_id'     => $this->action_id
            ];
        }

        return [
            'user_id'       => $this->user->id,
            'user_name'     => $this->user->name,
            'user_photo'    => $this->user->photo,
            'message'       => $this->message,
            'type'          => $this->type,
            'action_id'     => $this->action_id
        ];
    }
}
