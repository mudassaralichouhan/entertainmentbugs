<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\User;
use App\Models\Video;

class LikedVideo extends Notification
{
    use Queueable;
    protected $video;
    protected $user;
    protected $isReward;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, Video $video, $isReward = false)
    {
        $this->user = $user;
        $this->video = $video;
        $this->isReward = $isReward;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // return (new MailMessage)
        //             ->line('The introduction to the notification.')
        //             ->action('Notification Action', url('/'))
        //             ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'user_id' => $this->user->id,
            'video_id' => $this->video->id,
            'user_name' => $this->user->name,
            'user_photo' => $this->user->photo,
            'video_key' => $this->video->video_key,
            'title' => $this->video->title,
            'type'  => 'like-video',
            'message'   => $this->isReward ? 'liked your reward video' : 'liked your video'
        ];
    }
}
