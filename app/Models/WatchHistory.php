<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class WatchHistory extends Model
{
    use Sortable;
    use SoftDeletes;

    protected $fillable = ['user_id', 'video_id', 'title', 'url', 'session_id', 'ip', 'agent'];

    public $sortable = ['id', 'title'];

    public function thumb(){
		  return $this->belongsTo(VideoThumb::class, 'video_thumb_id','id');
    }
    
    public function video(){
	  	return $this->belongsTo(Video::class)->withDefault();
    }

    public function viewsCount(){
		  return $this->hasMany(VideoViews::class, 'video_id', 'video_id');
    }
    

}
