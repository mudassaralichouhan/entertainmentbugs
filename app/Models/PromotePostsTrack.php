<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotePostsTrack extends Model
{
    //
    protected $table = 'promote_posts_track';

    public function promote_post(){
        return $this->belongsTo(PromotePostModel::class, 'promote_post_id')->withDefault();;
    }
}
