<?php

namespace App\Models;

use App\UploadPhotos;
use Illuminate\Database\Eloquent\Model;

class PhotoLikes extends Model
{
    protected $table="photo_likes";

    public function photo(){
        return $this->belongsTo(UploadPhotos::class)->withDefault();;
    }
}
