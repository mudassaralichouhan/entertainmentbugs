<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model {

    use Sortable;
    use SoftDeletes;
    
    public $sortable = ['id', 'tag'];
	
	public static function getTagIdByName( $tag ){
		if(!empty($tag)){
			 $tagId = Tag::whereTag($tag)->pluck('id')->first();
			 return $tagId;
		}
	  
	}
	
	public function Video(){
		return $this->hasone(video::class, 'id')->withDefault();;
	}
	
	

}
