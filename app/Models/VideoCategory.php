<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class VideoCategory extends Model {

    use Sortable;
    use SoftDeletes;
    
    public $sortable = ['id', 'video_id', 'category_id'];
	
	public static function getVideoCategoryName( $videoId = null ){
		if(!empty($videoId)){
			$videoCategoryData = DB::select('SELECT C.slug,C.category FROM '.(new VideoCategory)->getTable().' VC INNER JOIN '.(new Category)->getTable().' C ON C.id = VC.category_id WHERE C.status = 1 AND VC.video_id='.$videoId);
			$categoryList = array();
			if(!empty($videoCategoryData)){
				foreach($videoCategoryData as $data){
					$categoryList[trim($data->slug)] = trim($data->category);
				}
			}
			return $categoryList;						
		}
	}
	
	public function video(){
		return $this->belongsTo(Video::class)->withDefault();
	}

	public function category(){
		return $this->belongsTo(Category::class)->withDefault();
	}
	

}
