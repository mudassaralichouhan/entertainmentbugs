<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class photocomment extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();;
    }
    
    public function replies()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    public function totalChildren($parent_id, $id){
        return $this->children($parent_id, $id)->count();
    }

    public function children($parent_id, $id){
        return photocomment::where([['commentable_id', $id], ['parent_id', $parent_id]]);
    }
}
