<?php
namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;

class ArtistVideo extends Model {

    use Sortable;
    use SoftDeletes;
    protected $table = 'artist_video';
	public static $blocks = 3;
	public static $premiumBlocks = 6;
    
    // public $sortable = ['id', 'title'];
	
	// public function thumb(){
	// 	return $this->belongsTo(VideoThumb::class, 'video_thumb_id');
	// }
	
	public function user(){
		return $this->belongsTo(User::class, 'user_id','id')->withDefault();
	}

	public function aboutAuthor(){
		return $this->belongsTo(ArtistAbout::class, 'user_id','user_id')->where('isDeleted',0)->withDefault();
	}
	
	public function scopeHoldUser($query)
    {
        $query->where('users.isDeleted', 0)->join('users', 'users.id', '=', 'artist_video.user_id');
        return $query;
    }

	// public function __get($key){
	// 	if( $key == 'blocks' ){
	// 		return self::$blocks;
	// 	} elseif( $key == 'premiumBlocks' ){
	// 		return self::$premiumBlocks;
	// 	}
	// }

	// public function isWatchedVideo(){
	// 	return $this->hasMany(WatchHistory::class)->where('user_id', Session::get('user_id'));
	// }
	

	// public function video_category(){
	// 	return $this->hasMany(VideoCategory::class);
	// }
	
	// public function video_tag(){
	// 	return $this->hasMany(VideoTag::class);
	// }
	
	
	// public static function getSimilarVideos( $videoId = null ){
	// 	if(!empty($videoId)){
	// 		$videoData = DB::select('SELECT V.id, V.video_key, V.title, V.video_time, VT.thumb FROM '.(new Video)->getTable().' V INNER JOIN '.(new VideoThumb)->getTable().' VT ON VT.id = V.video_thumb_id WHERE V.deleted_at = V.status = 1 AND  isRewardVideo = 0 AND  V.id !='.$videoId.' limit 12');
			
	// 		return $videoData;
	// 	}
	// }
	
	// 	public static function getRewardSimilarVideos( $videoId = null ){
	// 	if(!empty($videoId)){
	// 		$videoData = DB::select('SELECT V.id, V.video_key, V.title, V.video_time, VT.thumb FROM '.(new Video)->getTable().' V INNER JOIN '.(new VideoThumb)->getTable().' VT ON VT.id = V.video_thumb_id WHERE V.status = 1 AND isRewardVideo = 1 AND V.deleted_at = NULL AND V.id !='.$videoId.' limit 12');
			
	// 		return $videoData;
	// 	}
	// }
	
	
	
	// public static function getRecommendedVideos( $videoId = null ){
	// 	if(!empty($videoId)){
	// 		$videoData = DB::select('SELECT V.id, V.video_key, V.title, V.video_time, VT.thumb FROM '.(new Video)->getTable().' V INNER JOIN '.(new VideoThumb)->getTable().' VT ON VT.id = V.video_thumb_id WHERE V.status = 1 AND isRewardVideo = 0 AND V.deleted_at = NULL AND V.id !='.$videoId.' limit 20');
			
	// 		return $videoData;
	// 	}
	// }

	// public function totalViews(){
	// 	return $this->hasMany(VideoViews::class);
	// }
	
	// public function comments()
    // {
    //     return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id')->orderBy('created_at','desc');
    // }
    
    // public function video_likes(){
	// 	return $this->hasMany(VideoLikes::class)->where('liked', 1);
	// }

	// public function video_dislikes(){
	// 	return $this->hasMany(VideoLikes::class)->where('disliked', 1);
	// }
	
	// public function totalComments(){
	// 	return $this->hasMany(Comment::class, 'commentable_id');
	// }
	
	// public function watchTime(){
	// 	return $this->hasOne(VideoViews::class)->where('user_id', Session::get('user_id'));
	// }
}
