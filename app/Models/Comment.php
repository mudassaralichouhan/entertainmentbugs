<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault();
    }

    public function replies()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }
}
