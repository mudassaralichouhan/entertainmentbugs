<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BrandProducts extends Model
{
    //
    protected $table = "brand_products";

    public function artist_brand_collaboration(){
        return $this->belongsTo(ArtistBrandCollaboration::class, 'relation_id')->withDefault();
    }
}
