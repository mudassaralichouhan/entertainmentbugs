<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistAchivement extends Model
{
    protected $table = 'artist_achievment';
    public static $blocks = 2;
    public static $premiumBlocks = 5;

	public function user(){
		return $this->belongsTo(User::class, 'user_id');
	}
    
	public function artist(){
		return $this->belongsTo(ArtistAbout::class, 'user_id','user_id')->withDefault();;
	}

	// public function __get($key){
	// 	if( $key == 'blocks' ){
	// 		return self::$blocks;
	// 	} elseif( $key == 'premiumBlocks' ){
	// 		return self::$premiumBlocks;
	// 	}
	// }
}
