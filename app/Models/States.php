<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    //
    protected $table = "tbl_states";
    protected $fillable = ['name'];

    public function country(){
        return $this->belongsTo(Country::class, "country_id")->withDefault();;
    }
}
