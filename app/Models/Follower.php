<?php
namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;

class Follower extends Model {

    use Sortable;
    // use SoftDeletes;
    protected $primaryKey = 'id_key';
    public $sortable = ['id_key', 'user_id','follows_id'];
	
	public function followers(){
		return $this->belongsTo(User::class, 'follows_id','id')->withDefault();
	}
	
	public function user(){
		return $this->belongsTo(User::class, 'user_id')->withDefault();
	}
	
}
