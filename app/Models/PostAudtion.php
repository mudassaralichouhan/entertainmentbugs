<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PostAudtion extends Model
{

    protected $table ="post_audtion";
    public $fillable = [
            'title',
            'category',
            'country',
            'state',
            'city',
            'language',
            'post_by_name',
            'valid_till',
            'description',
            'synopsis',
            'eligibility_criteria',
            'email_address',
            'whatsapp_number',
            'photo',
            'user_id',
            'posted_date',
            'slug'
        ];
    
    
    public function user(){
		return $this->belongsTo(User::class, 'user_id')->withDefault();;
	}
	
    public function isApplied($user_id = null){
        return $this->hasMany(ApplyAudition::class, 'audition_id')->where('user_id', $user_id ?? session()->get('user_id'))->count();
    }

    public function productionHouse(){
        return $this->belongsTo(ProductionAbout::class, 'user_id', 'user_id')->withDefault();;
    }
}