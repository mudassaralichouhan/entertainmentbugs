<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoStatitics extends Model
{
    protected $table = "videos_statistics";
    protected $attributes = [
        "unique_users" => 0,
        "total_users" => 0
    ];
}
