<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    //
    protected $table = 'wallet';
    protected $fillable =['user_id','balance','currency','status','created_at','updated_at'] ;
    public function user(){
        return $this->belongsTo(User::class, 'user_id')->withDefault();
    }
}
