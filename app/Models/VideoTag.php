<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class VideoTag extends Model {

    use Sortable;
    use SoftDeletes;
    
    public $sortable = ['id', 'tag_id'];
	
	public static function getVideoTagName( $videoId = null ){
		if(!empty($videoId)){
			$videoTagsData = DB::select('SELECT T.id, T.tag FROM '.(new VideoTag)->getTable().' VT INNER JOIN '.(new Tag)->getTable().' T ON T.id = VT.tag_id WHERE T.status = 1 AND VT.video_id ='.$videoId);
			$tagList = array();
			if(!empty($videoTagsData)){
				foreach($videoTagsData as $data){
					$tagList[trim($data->id)] = trim($data->tag);
				}
			}
			return $tagList;						
		}
	}
	
	public function tag(){
		return $this->belongsTo(Tag::class)->withDefault();
	}
	
	
	
	

}
