<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class WatchLater extends Model
{
    use Sortable;
    use SoftDeletes;
    
    public $sortable = ['id', 'title'];

    public function thumb(){
		return $this->belongsTo(VideoThumb::class, 'video_id')->withDefault();
    }
    
    public function viewsCount(){
		return $this->hasMany(VideoViews::class, 'video_id');
    }
      
    public function video(){
        return $this->belongsTo(Video::class)->withDefault();
    }
}
