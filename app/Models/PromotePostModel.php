<?php

namespace App\Models;

use App\UploadPhotos;
use App\AdsPresetModel;
use Illuminate\Database\Eloquent\Model;

class PromotePostModel extends Model
{
    //
    protected $table = 'promote_posts';
    protected $fillable =['last_run_at','remain_bal','post_type','post_id','user_id','country_id','state_ids','button_label','url','clicks','views','click_range','view_range','days','is_custom','amount','paused_at','paused_for','status','is_paid','isDeleted'];
    public function track(){
        return $this->hasMany(PromotePostsTrack::class, 'promote_post_id');
    }

    public function story(){
        return $this->belongsTo(Story::class, 'post_id')->withDefault();;
    }

    public function video(){
        return $this->belongsTo(Video::class, 'post_id')->withDefault();;
    }

    public function photo(){
        return $this->belongsTo(UploadPhotos::class, 'post_id')->withDefault();;
    }

    public function ad(){
        return $this->belongsTo(AdsPresetModel::class, 'post_id')->withDefault();;
    }

    /**
     * Get most recent transaction for ad or post.
     */
    public function transaction(){
        return $this
            ->hasOne(Transactions::class, 'related_id')
            ->where('type', $this->getTransactionType())
            ->orderByDesc('id');
    }

    /**
     * Get type of transaction
     * @return string either ads-campaign or promote-post
     */
    public function getTransactionType(){
        switch($this->post_type){
            case 'ad':
                return 'ads-campaign';
                break;

            case 'video':
            case 'photo':
            case 'story':
                return 'promote-post';
                break;

            default:
                return 'promote-post';
        }
    }
}