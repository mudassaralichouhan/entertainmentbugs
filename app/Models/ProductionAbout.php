<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductionAbout extends Model
{

    protected $table ="production_about";
    public $fillable = [
            'name',
            'language',
            'working_hours_one',
            'working_hours_two',
            'country',
            'state',
            'city',
            'facebook',
            'instagram',
            'youtube',
            'linkedin',
            'production_house_date',
            'profile_photo',
            'best_tagline',
            'about',
            'multiple_category',
            'user_id',
            'video',
            'thumb'
        ];
    
    
    public function user(){
		return $this->belongsTo(User::class, 'user_id')->withDefault();
	}
	
	public function scopeHoldUser($query)
    {
        $query->where('users.isDeleted', 0)->join('users', 'users.id', '=', 'production_about.user_id');
        return $query;
    }
	

}