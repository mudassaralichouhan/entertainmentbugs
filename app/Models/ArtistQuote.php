<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistQuote extends Model
{
    //
    protected $table = 'artist_quote';

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function artist(){
        return $this->belongsTo(ArtistAbout::class, 'user_id', 'user_id')->withDefault();;
    }
}
