<?php
namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;

class ArtistAbout extends Model {
    use Sortable;
    use SoftDeletes;
    protected $table = 'artist_about';
    public $isPremium = false;
	
	public function user(){
		return $this->belongsTo(User::class, 'user_id')->withDefault();
	}

    public function achievement(){
        $this->hasMany(ArtistAchivement::class, 'user_id', 'user_id');
    }

    public function brand_collaboration(){
        return $this->hasMany(ArtistBrandCollaboration::class, 'user_id', 'user_id');
    }

    public function quote(){
        return $this->hasOne(ArtistQuote::class, 'user_id', 'user_id')->withDefault();;
    }

    public function contact(){
        return $this->hasMany(ArtistContact::class, 'user_id', 'user_id');
    }

    public function gallery(){
        return $this->hasMany(ArtistGallery::class, 'user_id', 'user_id');
    }

    public function project(){
        return $this->hasMany(ArtistProject::class, 'user_id', 'user_id');
    }

    public function reel(){
        return $this->hasMany(ArtistShowreel::class, 'user_id', 'user_id');
    }

    public function video(){
        return $this->hasMany(ArtistVideo::class, 'user_id', 'user_id');
    }

    public function what_we_do(){
        return $this->hasMany(ArtistWhatWeDo::class, 'user_id', 'user_id');
    }
    
	public function scopeHoldUser($query)
    {
        $query->where('users.isDeleted', 0)->join('users', 'users.id', '=', 'artist_about.user_id');
        return $query;
    }
}
