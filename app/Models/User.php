<?php

namespace App\Models;
use App\Payout;
use App\UserPaymentMethod;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Model {

	use Sortable;
	use Notifiable;
	use SoftDeletes;
	
	public $sortable = ['id', 'name', 'email', 'created_at','status'];
	public $transactionRowsLimit = 10;

	public function artist(){
		return $this->hasMany(ArtistAbout::class, 'user_id','id');
	}

	public function production(){
		return $this->hasOne(ProductionAbout::class, 'user_id')->withDefault();
	}
	
	public function followers() 
	{
		return $this->belongsToMany(self::class, 'followers', 'follows_id', 'user_id')
					->withTimestamps();
	}

	public function follows() 
	{
		return $this->belongsToMany(self::class, 'followers', 'user_id', 'follows_id')
					->withTimestamps();
	}
	
	public function scopeHoldUser($query)
	{
		$query->where('users.isDeleted', 0);
		return $query;
	}
	
	public function follow($userId) 
	{
		$this->follows()->attach($userId);
		return $this;
	}

	public function unfollow($userId)
	{
		$this->follows()->detach($userId);
		return $this;
	}

	public function isFollowing($userId) 
	{
		return (boolean) $this->follows()->where('follows_id', $userId)->first(['id_key']);
	}

	public function is_follower($user_id = null){
		return $this->hasMany(Follower::class, 'user_id')
					->where('follows_id', $user_id ?? session()->get('user_id'))
					->orderByDesc('id')
					->limit(1);
	}
	
	public function getRouteKeyName()
	{
		return 'name';
	}
	
	public function video()
	{
		return $this->hasMany(Video::class);
	}

	public function getFollowers($limit = 5){
		return $this->hasMany(Follower::class, 'follows_id')
					->orderByDesc('id_key')
					->limit($limit)
					->get();
	}
   
	public function isFollowedBy($user_id = null){
		return $this->hasMany(Follower::class, 'follows_id')->where('user_id', $user_id ?? session()->get('user_id'))->orderByDesc('id')->count();
	}

	public function getFollowerCount(){
		return $this->hasMany(Follower::class, "follows_id")->count();
	}

	public function getFollowingCount(){
		return $this->hasMany(Follower::class, "user_id")->count();
	}
	
	public function isArtist(){
		return $this->hasOne(ArtistAbout::class, "user_id")->count();
	}

	public function wallet(){
		return $this->hasOne(Wallet::class, 'user_id')->withDefault();;
	}

	public function transactions(){
		return $this->hasMany(Transactions::class, 'user_id')->paginate($this->transactionRowsLimit);
	}

	public function activeThemePlan(){
		return $this->hasOne(Transactions::class, 'user_id')->where('type', 'theme-plan')->orderByDesc('id');
	}

	public function activePremiumPlan(){
		return $this->hasOne(Transactions::class, 'user_id')->where('type', 'like', 'premium-profile%')->orderByDesc('id');
	}

	public function isPremium(){
		if( $this->is_premium && Carbon::now()->timestamp < Carbon::createFromTimestamp($this->premium_ended_at)->timestamp ){
			return true;
		}

		return false;
	}

	public function hasThemePlan(){
		if( $this->plan == 2 && $this->purchase && Carbon::now()->timestamp < Carbon::createFromTimestamp($this->plan_expired_at)->timestamp ){
			return true;
		}
		return false;
	}
	public function paymentmethods(){
		return $this->hasMany(UserPaymentMethod::class,'user_id','id');
	}
	public function payout(){
		return $this->hasMany(Payout::class,'user_id','id');
	}
	
	public function country(){
		return $this->belongsTo(Country::class,'country_id','id');
	}
	
	public function state(){
		return $this->belongsTo(States::class,'state_id','id');
	}
}