<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model {

    use Sortable;
    use SoftDeletes;
    
    public $sortable = ['id', 'category'];
	
	public static function getCategoryList(){
	   return Category::where('status', 1)->orderBy('category', 'ASC')->pluck('category','id')->all();
	}
   

}
