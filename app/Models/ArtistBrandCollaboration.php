<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtistBrandCollaboration extends Model
{
    //
    protected $table = "artist_brand_collaboration";

    public function brand_products(){
        return $this->hasMany(BrandProducts::class, 'relation_id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id')->withDefault();;
    }

    public function artist(){
        return $this->belongsTo(ArtistAbout::class, 'user_id', 'user_id')->withDefault();;
    }
}
