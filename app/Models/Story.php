<?php

namespace App\Models;

use App\Helpers\DateTimeHelper;
use App\Helpers\StoryHelper;
use App\PageViewTrack;
use Carbon\Carbon;
use Ghanem\Rating\Traits\Ratingable as Rating;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Story extends Model
{
	use Rating;
	use SoftDeletes;
	
	public $fillable = ['title','tags','language', 'about','isDeleted'];
	
	public function user(){
		return $this->belongsTo(User::class, 'user_id')->withDefault();;
	}
	
	public function comments()
	{
		return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id')->orderBy('created_at','desc');
	}
	
	public function totalViews(){
		return $this->hasMany(StoryViews::class);
	}

	public function totalComments(){
		return $this->hasMany(Comment::class, 'commentable_id');
	}
	
	public function story_likes(){
		return $this->hasMany(StoryLikeDislike::class)->where('liked', 1);
	}

	public function story_dislikes(){
		return $this->hasMany(StoryLikeDislike::class)->where('disliked', 1);
	}

	public function likes(){
		return $this->hasMany(StoryLikeDislike::class, "story_id")->where('liked', 1)->count();
	}

	public function dislikes(){
		return $this->hasMany(StoryLikeDislike::class, "story_id")->where('disliked', 1)->count();
	}

	public function current_reaction(){
		return $this->hasMany(StoryLikeDislike::class, 'story_id')->where('is_paid', 0)->whereRaw("(liked='1' || disliked='1')");
	}

	public function total_reaction(){
		return $this->hasMany(StoryLikeDislike::class, 'story_id')->whereRaw("(liked='1' || disliked='1')");
	}

	public function scopeHoldUser($query)
	{
		$query->where('users.isDeleted', 0)->join('users', 'users.id', '=', 'stories.user_id');
		return $query;
	}
	
	public function getTotalReaction($days = null, $user_id = null){
		if( !$user_id ){
			if( !$days ){
				return $this->hasMany(StoryLikeDislike::class, 'story_id')->where('liked', '1')->orWhere('disliked', '1')->count();
			} else {
				return $this->hasMany(StoryLikeDislike::class, 'story_id')->where([['liked', '1'], ['created_at', ">=", $days]])->orWhere('disliked', '1')->count();
			}
		} else {
			if( !$days ){
				return $this->join("story_like_dislikes", "story_like_dislikes.story_id", "=", "stories.id")->whereRaw("story_like_dislikes.user_id = $user_id && (story_like_dislikes.liked = 1 || story_like_dislikes.disliked = 1)")->count();
			} else {
				return $this->join("story_like_dislikes", "story_like_dislikes.story_id", "=", "stories.id")->whereRaw("stories.user_id = $user_id && story_like_dislikes.created_at >= '$days' && (story_like_dislikes.liked = 1 || story_like_dislikes.disliked = 1)")->count();
			}
		}
	}

	public function getGraphData($days = 7){
		$url = url("");
		$date = DateTimeHelper::fromDate(null, $days);
		$story = PageViewTrack::selectRaw("DATE(created_at) as day")
			->where([['created_at', '>=', $days], ['url', "$url/story/display/$this->slug"]])
			->groupBy(\DB::Raw('url, user_id, ip, DATE(created_at)'));

		$stories = \DB::table(\DB::raw("({$story->toSql()}) as a"))
			->select(\DB::raw("COUNT(*) as views, a.day"))
			->mergeBindings($story->getQuery())
			->groupBy("a.day")
			->get();

		$carbon = Carbon::parse($date);
		$graphObj['date'] = [];
		$graphObj['stories'] = StoryHelper::GRAPH_DATA;
		$graphObj['total'] = 0;

		for($i = 0; $i < $days; $i++){
			$storyViews = 0;
			$date = $carbon->addDay($i ? 1 : 0);
			array_push($graphObj['date'], $date->format("d M"));
			
			if($stories) {
				if(isset($stories[0]) && $stories[0]->day == $date->format("Y-m-d")){
					$storyViews = $stories[0]->views;
					$stories->splice(0, 1);
				}
			}

			$graphObj['total'] += $storyViews;
			array_push($graphObj['stories']['data'], $storyViews);
		}
		
		return $graphObj;
	}

	public function promoted(){
		return $this->hasOne(PromotePostModel::class, 'post_id')->where('status', '1')->withDefault();
	}

	public function is_promoted(){
		return $this->hasOne(PromotePostModel::class, 'post_id')->where('status', '1')->where('post_type', 'story')->whereRaw('DATE_ADD(created_at, INTERVAL days DAY) >= ? AND ((views * view_range) + (clicks * click_range) < `amount`) AND is_paid=1', [Carbon::now()])->orderByDesc('id')->limit(1);
	}

	public function getURL() {
		return route('story.display', ['key' => $this->slug]);
	}

	public function getThumbnailURL() {
		return StoryHelper::getThumbnailURL($this->thumb_image);
	}

	public function getImageURL() {
		return StoryHelper::getImageURL($this->cover_image);
	}

	public function getTimesAgo() {
		return DateTimeHelper::getTimesAgo($this->created_at);
	}

	public function getTotalReactionEarning() {
		return StoryHelper::getTotalReactionEarning($this->total_reaction_count);
	}

	public function getUnpaidReactionEarning() {
		return StoryHelper::getUnpaidReactionEarning($this->current_reaction_count);
	}

	public function getGraphElement($days = 7, $graph = null) {
		$graph = $graph ?? $this->getGraphData($days);
		return StoryHelper::getGraphElement($this->id, $graph);
	}
}
