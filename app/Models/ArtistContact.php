<?php
namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;

class ArtistContact extends Model {

    use Sortable;
    use SoftDeletes;
    protected $table = 'artist_contact';
    
    
	// public function thumb(){
	// 	return $this->belongsTo(VideoThumb::class, 'video_thumb_id');
	// }
	
	public function user(){
		return $this->belongsTo(ArtistAbout::class, 'user_id')->withDefault();;
	}

	// public function isWatchedVideo(){
	// 	return $this->hasMany(WatchHistory::class)->where('user_id', Session::get('user_id'));
	// }
	

	// public function video_category(){
	// 	return $this->hasMany(VideoCategory::class);
	// }
	
	// public function video_tag(){
	// 	return $this->hasMany(VideoTag::class);
	// }
	
	
	// public static function getSimilarVideos( $videoId = null ){
	// 	if(!empty($videoId)){
	// 		$videoData = DB::select('SELECT V.id, V.video_key, V.title, V.video_time, VT.thumb FROM '.(new Video)->getTable().' V INNER JOIN '.(new VideoThumb)->getTable().' VT ON VT.id = V.video_thumb_id WHERE V.deleted_at = V.status = 1 AND  isRewardVideo = 0 AND  V.id !='.$videoId.' limit 12');
			
	// 		return $videoData;
	// 	}
	// }
	
	// 	public static function getRewardSimilarVideos( $videoId = null ){
	// 	if(!empty($videoId)){
	// 		$videoData = DB::select('SELECT V.id, V.video_key, V.title, V.video_time, VT.thumb FROM '.(new Video)->getTable().' V INNER JOIN '.(new VideoThumb)->getTable().' VT ON VT.id = V.video_thumb_id WHERE V.status = 1 AND isRewardVideo = 1 AND V.deleted_at = NULL AND V.id !='.$videoId.' limit 12');
			
	// 		return $videoData;
	// 	}
	// }
	
	
	
	// public static function getRecommendedVideos( $videoId = null ){
	// 	if(!empty($videoId)){
	// 		$videoData = DB::select('SELECT V.id, V.video_key, V.title, V.video_time, VT.thumb FROM '.(new Video)->getTable().' V INNER JOIN '.(new VideoThumb)->getTable().' VT ON VT.id = V.video_thumb_id WHERE V.status = 1 AND isRewardVideo = 0 AND V.deleted_at = NULL AND V.id !='.$videoId.' limit 20');
			
	// 		return $videoData;
	// 	}
	// }

	// public function totalViews(){
	// 	return $this->hasMany(VideoViews::class);
	// }
	
	// public function comments()
    // {
    //     return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id')->orderBy('created_at','desc');
    // }
    
    // public function video_likes(){
	// 	return $this->hasMany(VideoLikes::class)->where('liked', 1);
	// }

	// public function video_dislikes(){
	// 	return $this->hasMany(VideoLikes::class)->where('disliked', 1);
	// }
	
	// public function totalComments(){
	// 	return $this->hasMany(Comment::class, 'commentable_id');
	// }
	
	// public function watchTime(){
	// 	return $this->hasOne(VideoViews::class)->where('user_id', Session::get('user_id'));
	// }
}
