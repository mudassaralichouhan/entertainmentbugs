<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Session;

class ApplyAudition extends Model {

    use Sortable;
    use SoftDeletes;
    protected $table = 'apply_audition';

    public function audition(){
        return $this->belongsTo(PostAudtion::class, 'audition_id')->withDefault();
    }
    
	public function user(){
		return $this->belongsTo(User::class, 'user_id')->withDefault();
	}
}
