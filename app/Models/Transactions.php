<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $guarded = array();
    //
    public function user(){
        return $this->belongsTo(User::class, 'user_id')->withDefault();;
    }
}
