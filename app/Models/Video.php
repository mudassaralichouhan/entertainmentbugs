<?php

namespace App\Models;

use App\Helpers\DateTimeHelper;
use App\Helpers\VideoHelper;
use App\PageViewTrack;
use App\WatchTrack;
use App\WatchVideoEarning;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Kyslik\ColumnSortable\Sortable;

class Video extends Model
{

    use Sortable;
    use SoftDeletes;

    public $sortable = ['id', 'title'];
    private $totalUniqueViews;

    protected $casts = [
        'video_time' => 'integer',
    ];

    public function thumb()
    {
        return $this->belongsTo(VideoThumb::class, 'video_thumb_id')->withDefault();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault();
    }

    public function isWatchedVideo()
    {
        return $this->hasMany(WatchHistory::class)->where('user_id', Session::get('user_id'));
    }

    public function history()
    {
        return $this->hasMany(WatchHistory::class, 'video_id', 'id');
    }

    public function getUniqueViews($from, $to)
    {
        if (!is_null($this->totalUniqueViews)) return $this->totalUniqueViews;

        $this->totalUniqueViews = 0;
        $views = $this->history()
            ->select(DB::raw('COUNT(user_id, ip) AS total_views'))
            ->where('video_id', $this->id)
            ->where(function ($query) use ($from, $to) {
                $query->where('created_at', '>=', $from)
                    ->where('created_at', '<=', $to);
            })
            ->groupBy('user_id')
            ->first();

        if ($views) {
            $this->totalUniqueViews = $views->total_views;
        }

        return $this->totalUniqueViews;
    }

    public function video_category()
    {
        return $this->hasMany(VideoCategory::class);
    }

    public function video_tag()
    {
        return $this->hasMany(VideoTag::class);
    }


    public static function getSimilarVideos($videoId = null)
    {
        if (!empty($videoId)) {
            $videoData = DB::select('SELECT V.id, V.video_key, V.title, V.video_time, VT.thumb FROM ' . (new Video)->getTable() . ' V INNER JOIN ' . (new VideoThumb)->getTable() . ' VT ON VT.id = V.video_thumb_id WHERE V.deleted_at = V.status = 1 AND  isRewardVideo = 0 AND  V.id !=' . $videoId . ' limit 12');
            return $videoData;
        }
    }

    public static function getRewardSimilarVideos($videoId = null)
    {
        if (!empty($videoId)) {
            $videoData = DB::select('SELECT V.id, V.video_key, V.title, V.video_time, VT.thumb FROM ' . (new Video)->getTable() . ' V INNER JOIN ' . (new VideoThumb)->getTable() . ' VT ON VT.id = V.video_thumb_id WHERE V.status = 1 AND isRewardVideo = 1 AND V.deleted_at = NULL AND V.id !=' . $videoId . ' limit 12');
            return $videoData;
        }
    }


    public static function getRecommendedVideos($videoId = null)
    {
        if (!empty($videoId)) {
            $videoData = DB::select('SELECT V.id, V.video_key, V.title, V.video_time, VT.thumb FROM ' . (new Video)->getTable() . ' V INNER JOIN ' . (new VideoThumb)->getTable() . ' VT ON VT.id = V.video_thumb_id WHERE V.status = 1 AND isRewardVideo = 0 AND V.deleted_at = NULL AND V.id !=' . $videoId . ' limit 20');
            return $videoData;
        }
    }

    public function scopeHoldUser($query)
    {
        $query->where('users.isDeleted', 0)->join('users', 'users.id', '=', 'videos.user_id');
        return $query;
    }

    public function totalHistory()
    {
        return $this->hasMany(WatchHistory::class, 'video_id', 'id');
    }

    public function totalViews()
    {
        return $this->hasMany(VideoViews::class, 'video_id', 'id');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->whereNull('parent_id')->orderBy('created_at', 'desc');
    }

    public function likes()
    {
        return $this->video_likes->count();
    }

    public function dislikes()
    {
        return $this->video_dislikes->count();
    }

    public function video_likes()
    {
        if ($this->isRewardVideo) {
            $from = Carbon::parse($this->created_at)->startOfWeek();
            $to = Carbon::parse($this->created_at)->endOfWeek();
            return $this->hasMany(VideoLikes::class)
                ->where('liked', 1)
                ->where([['created_at', '>=', $from], ['created_at', '<=', $to]]);
        } else {
            return $this->hasMany(VideoLikes::class)->where('liked', 1);
        }
    }

    public function video_dislikes()
    {
        if ($this->isRewardVideo) {
            $from = Carbon::parse($this->created_at)->startOfWeek();
            $to = Carbon::parse($this->created_at)->endOfWeek();
            return $this->hasMany(VideoLikes::class)
                ->where('disliked', 1)
                ->where([['created_at', '>=', $from], ['created_at', '<=', $to]]);
        } else {
            return $this->hasMany(VideoLikes::class)->where('disliked', 1);
        }
    }

    public function totalComments()
    {
        return $this->hasMany(Comment::class, 'commentable_id');
    }

    public function watchTime()
    {
        return $this->hasOne(VideoViews::class)->where('user_id', Session::get('user_id'));
    }

    public function track()
    {
        return $this->hasOne(WatchVideoEarning::class, 'video_id');
    }

    public function watchTrack($isCalculated = null)
    {
        if (!$isCalculated) {
            $isRewardVideo = $this->isRewardVideo;
            $createdAt = $this->created_at;
            return $this->hasMany(WatchTrack::class, 'video_id')
                ->select("id", "track", "user_id", "is_paid")
                ->where('track', "!=", "null")
                ->where(function ($query) use ($isRewardVideo, $createdAt) {
                    if ($isRewardVideo) {
                        $from = Carbon::parse($createdAt)->startOfWeek();
                        $to = Carbon::parse($createdAt)->endOfWeek();
                        $query->where([['created_at', '>=', $from], ['created_at', '<=', $to]]);
                    }
                })
                ->orderBy('user_id');
        }

        return $this->hasMany(WatchTrack::class, 'video_id')
            ->select("id", "track", "user_id", "is_paid")
            ->where([['track', "!=", "null"], ['is_calculated', '0']])
            ->orderBy('user_id')
            ->get();
    }

    public function getGraphData($days)
    {
        $url = url("");
        $date = DateTimeHelper::fromDate(null, $days);
        $videos = PageViewTrack::selectRaw("DATE(created_at) as day")
            ->where([['created_at', '>=', $date]])
            ->whereIn('url', function ($query) use ($url) {
                $url = $this->isRewardVideo ? "$url/watch-reward/" : "$url/watch/";
                $query->selectRaw("CONCAT('$url', video_key)")
                    ->where('id', $this->id)
                    ->where(function ($query) {
                        if ($this->isRewardVideo) {
                            $from = Carbon::parse($this->created_at)->startOfWeek();
                            $to = Carbon::parse($this->created_at)->endOfWeek();
                            $query->where([['created_at', '>=', $from], ['created_at', '<=', $to]]);
                        }
                    })
                    ->from('videos');
            })
            ->groupBy(DB::raw('url, user_id, ip, DATE(created_at)'));

        $videos = DB::table(DB::raw("({$videos->toSql()}) as a"))
            ->select(DB::raw("COUNT(*) as views, a.day"))
            ->mergeBindings($videos->getQuery())
            ->groupBy("a.day")
            ->get();

        $carbon = Carbon::parse($date);
        $graphObj['date'] = [];
        $graphObj['videos'] = VideoHelper::GRAPH_DATA;
        $graphObj['total'] = 0;

        for ($i = 0; $i < $days; $i++) {
            $videosViews = 0;
            $date = $carbon->addDay($i ? 1 : 0);
            array_push($graphObj['date'], $date->format("d M"));
            if ($videos) {
                if (isset($videos[0]) && $videos[0]->day == $date->format("Y-m-d")) {
                    $videosViews = $videos[0]->views;
                    $videos->splice(0, 1);
                }
            }

            $graphObj['total'] += $videosViews;
            array_push($graphObj['videos']['data'], $videosViews);
        }

        return $graphObj;
    }

    public function getUniqueUsers()
    {
        return $this->hasMany(WatchTrack::class, "video_id")->distinct()->count("user_id");
    }

    public function promoted()
    {
        return $this->hasOne(PromotePostModel::class, 'post_id')->where('status', '1');
    }

    public function is_promoted()
    {
        return $this->hasOne(PromotePostModel::class, 'post_id')->where('status', '1')->where('post_type', 'video')->whereRaw('DATE_ADD(created_at, INTERVAL days DAY) >= ? AND ((views * view_range) + (clicks * click_range) < `amount`) AND is_paid=1', [Carbon::now()])->orderByDesc('id')->limit(1);
    }

    public function scopeGetPromoted($query)
    {
        return $query->where('id', '<>', 0);
    }

    public function getURL()
    {
        return route($this->isRewardVideo ? 'watch.reward-video' : 'watch.video', ['key' => $this->video_key]);
    }

    public function getTimesAgo()
    {
        return DateTimeHelper::getTimesAgo($this->created_at);
    }

    public function getGraphElement($days = 7, $graph = null)
    {
        $graph = $graph ?? $this->getGraphData($days);
        return VideoHelper::getGraphElement($this->id, $graph, $this->isRewardVideo);
    }

    public function getThumbnailURL($returnDefaultOnEmpty = true)
    {
        if ($this->thumb && $this->thumb->thumb) {
            if (File::exists($this->thumb->thumb)) {
                return asset($this->thumb->thumb);
            }
        }

        return $returnDefaultOnEmpty ? asset(VideoHelper::DEFAULT_THUMBNAIL) : '';
    }
}