<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterMail extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    /**
     * Build the message.
     *
     * @return $this
     */
     public function __construct($user)
    {
        $this->user = $user;
    }
    public function build()
    {
        return $this->view('email.auth.thankyou');
    }
}
