<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PageViewTrack extends Model
{
    //
    protected $table = "page_view_track";

    /**
     * Get unique for specified time period by default it's all time
     * @param float|int $timeperiod This value should be in years, if you want record for months then it should be like 0.01 for 1 month, 0.02 for 2 months ... 0.11 for 11 months
     * @return int|null
     */
    public function get_unique_users($timeperiod = null, $isCarbonDate = null){
        if( $isCarbonDate ){
            return $this->where("created_at", ">=", $isCarbonDate)->groupBy("ip")->count();
        } else {
            if( is_null($timeperiod) ){
                return $this->groupBy("ip")->count();
            }
            elseif( is_int($timeperiod) || is_float($timeperiod) ){
                if( $timeperiod > 0 ){
                    if( $timeperiod < 1 ){
                        $timeperiod = intval($timeperiod * 100);
                        $timeperiod = Carbon::now()->subMonth($timeperiod)->format("Y-m-d 00:00:00");
                    } else {
                        $timeperiod = Carbon::now()->subYear($timeperiod)->format("Y-m-d 00:00:00");
                    }
                    return $this->where("created_at", ">=", $timeperiod)->groupBy("ip")->count();
                }
            }
        }
    }
}
