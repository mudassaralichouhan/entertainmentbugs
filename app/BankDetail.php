<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\States;
class BankDetail extends Model
{
    protected $fillable = ['bank_name','account_no','ifsc_code','state_id','city','phone_no','created_at','updated_at'];
    public function states(){
        return $this->belongsTo(States::class,'state_id','id');
    }
}
