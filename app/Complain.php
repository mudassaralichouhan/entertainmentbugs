<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complain extends Model
{
    protected $fillable = ["user_id","name","file","phone","email","subject","link","message","status","comments","created_at","updated_at"];
}
