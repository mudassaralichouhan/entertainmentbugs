<?php

namespace App;
use Eloquent;
use App\Models\Follower;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;  

class User extends Eloquent implements Authenticatable
{
    use Notifiable,AuthenticableTrait;
    protected $table = 'users';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','google_id','status', 'country', 'state', 'language','later_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isFollowedBy($user_id = null){
        return $this->hasMany(Follower::class, 'follows_id')->where('user_id', $user_id ?? session()->get('user_id'))->orderByDesc('id')->count();
    }

    public function getFollowerCount(){
        return $this->hasMany(Follower::class, "follows_id")->count();
    }

    public function getFollowingCount(){
        return $this->hasMany(Follower::class, "user_id")->count();
    }
}
