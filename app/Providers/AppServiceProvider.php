<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Observers\VideoObserver;
use App\Models\Video;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app('view')->composer('layouts.home', function ($view) {
			$action = app('request')->route()->getAction();

			$controller = class_basename($action['controller']);

			list($controller, $action) = explode('@', $controller);

			$view->with(compact('controller', 'action'));
		});
		
		Video::observe(VideoObserver::class);
		 
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        require_once app_path("Helpers/helpers.php");
    }
}
