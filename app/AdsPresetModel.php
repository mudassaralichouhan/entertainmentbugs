<?php

namespace App;

use App\Models\PromotePostModel;
use App\Models\PromotePostsTrack;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AdsPresetModel extends Model
{
    //
    protected $table = "ads_presets";

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function promote_post($offset = 0){
        return $this->hasOne(PromotePostModel::class, 'post_id')->where('post_type' , 'ad')->orderByDesc('id')->limit(1)->offset($offset);
    }
}
