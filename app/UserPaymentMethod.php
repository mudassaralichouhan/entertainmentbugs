<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPaymentMethod extends Model
{
	protected $fillable =['user_id','method_id','value_id','created_at','updated_at'];
	public function bankdetil(){
		return $this->belongsTo(BankDetail::class,'value_id','id');
	} 
	public function paypaldetails(){
		return $this->belongsTo(PaypalDetail::class,'value_id','id');
	}
}
