<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Bus\Dispatchable;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $mailable;
    protected $type;
    protected $mailTo;
    
    private $types = ['register', 'forgot-password', 'audition-posted', 'apply-audition', 'profile-boost', 'payment'];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type, $mailable, $mailTo)
    {
        $this->type = $type;
        $this->mailable = $mailable;
        $this->mailTo = $mailTo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if(!in_array($this->type, $this->types)) return;
        $output = '';
        exec('ps aux | grep queue:work', $output);

        if (is_array($output) && count($output)) {
            Artisan::call("queue:flush");
            Artisan::call("queue:work");
        }
    
        Mail::to($this->mailTo)->send($this->mailable);
    }
}
