<?php

namespace App;

use App\Helpers\DateTimeHelper;
use App\Helpers\PhotoHelper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\photocomment;
use App\Models\PhotoLikes;
use App\Models\PromotePostModel;
use DB;
use Illuminate\Support\Facades\File;

class UploadPhotos extends Model
{
	public $commentLimits = 3;

	public function user(){
		return $this->belongsTo(User::class, 'user_id')->select("id", "slug", "name", "photo")->withDefault();
	}

	public function comments(){
		return $this->hasMany(photocomment::class, 'parent_id')->select("id", "user_id", "body", "created_at")->where('commentable_id', 0)->orderByDesc('id')->paginate($this->commentLimits);
	}

	public function likes(){
		return $this->belongsTo(PhotoLikes::class, 'id', 'photo_id')->where('liked', 1);
	}

	public function commentCount(){
		return $this->hasMany(photocomment::class, 'parent_id')->count();
	}

	public function get_likes() {
		return $this->hasMany(PhotoLikes::class, 'photo_id')->where('liked', '1');
	}

	public function get_dislikes() {
		return $this->hasMany(PhotoLikes::class, 'photo_id')->where('disliked', '1');
	}

	public function unpaid_reaction(){
		return $this->hasMany(PhotoLikes::class, 'photo_id')->where('is_paid', '0')->whereRaw("(liked='1' || disliked='1')");
	}

	public function total_reaction(){
		return $this->hasMany(PhotoLikes::class, 'photo_id')->whereRaw("(liked='1' || disliked='1')");
	}
	
	public function scopeHoldUser($query)
	{
		$query->where('users.isDeleted', 0)->whereNull('users.deleted_at')->join('users', 'users.id', '=', 'upload_photos.user_id');
		return $query;
	}

	public function getTotalReaction($days = null, $user_id = null){
		if(!$user_id) {
			if(!$days) {
				return $this->hasMany(PhotoLikes::class, 'photo_id')->where('liked', '1')->orWhere('disliked', '1')->count();
			} else {
				return $this->hasMany(PhotoLikes::class, 'photo_id')->where([['liked', '1'], ['created_at', ">=", $days]])->orWhere('disliked', '1')->count();
			}
		} else {
			if(!$days) {
				return $this->join("photo_likes", "photo_likes.photo_id", "=", "upload_photos.id")->whereRaw("upload_photos.user_id = $user_id && (photo_likes.liked = 1 || photo_likes.disliked = 1)")->count();
			} else {
				return $this->join("photo_likes", "photo_likes.photo_id", "=", "upload_photos.id")->whereRaw("upload_photos.user_id = $user_id && photo_likes.created_at >= '$days' && (photo_likes.liked = '1' || photo_likes.disliked = '1')")->count();
			}
		}
	}

	public function getGraphData($days){
		$url = url("");
		$date = DateTimeHelper::fromDate(null, $days);
		$rewardVideo = PageViewTrack::selectRaw("DATE(created_at) as day")
			->where([['created_at', '>=', $date]])
			->whereRaw("url = CONCAT('$url/single-photo/', $this->id)")
			->groupBy(DB::Raw('url, user_id, ip, DATE(created_at)'));

		$photos = DB::table(DB::raw("({$rewardVideo->toSql()}) as a"))
			->select(DB::raw("COUNT(*) as views, a.day"))
			->mergeBindings($rewardVideo->getQuery())
			->groupBy("a.day")
			->get();

		$carbon = Carbon::parse($date);
		$graphObj['date'] = [];
		$graphObj['photos'] = PhotoHelper::GRAPH_DATA;
		$graphObj['total'] = 0;

		for ($i = 0; $i < $days; $i++) {
			$photosViews = 0;
			$date = $carbon->addDay($i ? 1 : 0);
			array_push($graphObj['date'], $date->format("d M"));

			if ($photos) {
				if(isset($photos[0]) && $photos[0]->day == $date->format("Y-m-d")){
					$photosViews = $photos[0]->views;
					$photos->splice(0, 1);
				}
			}

			$graphObj['total'] += $photosViews;
			array_push($graphObj['photos']['data'], $photosViews);
		}
		
		return $graphObj;
	}

	public function promoted(){
		return $this->hasOne(PromotePostModel::class, 'post_id')->where('status', '1')->orderBy('id', 'desc')->limit(1);
	}

	public function is_promoted(){
		return $this->hasOne(PromotePostModel::class, 'post_id')->where('status', '1')->where('post_type', 'photo')->whereRaw('DATE_ADD(created_at, INTERVAL days DAY) >= ? AND ((views * view_range) + (clicks * click_range) < `amount`) AND is_paid=1', [Carbon::now()])->orderByDesc('id')->limit(1);
	}

	public function getURL() {
		return route('single-photo', ['id' => $this->id]);
	}

	public function getPhotosURL($first = false, $returnDefaultOnEmpty = false) {
		$photos = collect([]);
		if ($this->images) {
			$images = unserialize($this->images);

			if (is_array($images)) {
				foreach ($images as $image) {
					$url = PhotoHelper::getImageURL($image, $returnDefaultOnEmpty);
					if ($url) {
						$photos->push($url);
					}
				}
			}
		}

		return $first ? ($photos->first() ?? asset(PhotoHelper::DEFAULT_IMAGE)) : $photos;
	}

	public function getTimesAgo() {
		return DateTimeHelper::getTimesAgo($this->created_at);
	}

	public function getUnpaidReactionEarning() {
		return PhotoHelper::getUnpaidReactionEarning($this->unpaid_reaction_count);
	}

	public function getTotalReactionEarning() {
		return PhotoHelper::getTotalReactionEarning($this->total_reaction_count);
	}

	public function getGraphElement($days = 7, $graph = null) {
		$graph = $graph ?? $this->getGraphData($days);
		return PhotoHelper::getGraphElement($this->id, $graph);
	}
}
