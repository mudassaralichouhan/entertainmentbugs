<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WatchTrack extends Model
{
    use SoftDeletes;

    protected $table = "watch_track";

    protected $casts = [
        'track' => 'integer',
    ];

    protected $fillable = [
        'video_id',
        'user_id',
        'ip',
        'session',
        'agent',
        'track',
        'is_paid',
        'is_calculated',
    ];

    public function video()
    {
        return $this->belongsTo(App\Model\Video::class, 'video_id');
    }
}
