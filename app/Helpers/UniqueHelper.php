<?php

namespace App\Helpers;

class UniqueHelper {
	public static function generate_uuid4() {
		$random = random_bytes(16);
		$uuid = bin2hex($random);
		$uuid = sprintf('%04s-%04s-%04s-%04s-%04s%04s%04s', $uuid[0], $uuid[1], $uuid[2], $uuid[3], $uuid[4], $uuid[5], $uuid[6]);
		return $uuid;
	}
}