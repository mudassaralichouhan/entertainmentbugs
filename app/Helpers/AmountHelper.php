<?php

namespace App\Helpers;

class AmountHelper {
	public static function getReadableFormat($amount, $withCurrency = true) {
		$splittedAmount = explode(".", $amount);
		$afterDecimal = intval($splittedAmount[1] ?? 0);
		$afterDecimal = sprintf("%02d", abs($afterDecimal));
		$beforeDecimal = preg_replace("/(\d)(?=(\d\d)+\d$)/", "$1,", $splittedAmount[0]);
		return ($withCurrency ? "₹" : "")."$beforeDecimal.$afterDecimal";
	}
}