<?php

namespace App\Helpers;

use Carbon\Carbon;

class DateTimeHelper {
	const DEFAULT_DATE_TIME = "Y-m-d H:i:s";
	const DEFAULT_DATE = "Y-m-d";
	const HOUR_TO_SECONDS = 3600;
	const HOUR_TO_MINUTE = 60;
	const MINUTE_TO_SECONDS = 60;
	const START_OF_THE_DAY = "Y-m-d 00:00:00";

	private static $now = null;

	public static function fromDate($timestamp = null, $subDays = null) {
		$carbon = self::decodeElseToday($timestamp);
		if ($subDays) $carbon = $carbon->subDay($subDays);
		return $carbon->startOfDay();
	}

	public static function decodeElseToday($timestamp = null) {
		return $timestamp ? self::decode($timestamp) : self::now();
	}

	public static function now() {
		self::$now = self::$now ?? Carbon::now();
		return clone self::$now;
	}

	public static function getTimesAgo($timestamp) {
		return self::decode($timestamp)->diffForHumans();
	}

	public static function decode($timestamp) {
		if ($timestamp instanceof Carbon) return $timestamp;
		if (preg_match("/^([0-9]+)$/", $timestamp)) return Carbon::parse($timestamp);
		return Carbon::createFromTimestamp($timestamp);
	}

	public static function secondsToHours($timeInSecond) {
		return $timeInSecond ? $timeInSecond / self::HOUR_TO_SECONDS : 0;
	}

	public static function secondsToMinutes($timeInSecond) {
		return $timeInSecond ? $timeInSecond / self::MINUTE_TO_SECONDS : 0;
	}

	public static function hoursToMinutes($timeInHours) {
		return $timeInHours ? $timeInHours / self::HOUR_TO_MINUTE : 0;
	}

	public static function endOfMonth($timestamp = null, $format = null) {
		return self::decodeElseToday($timestamp)->endOfMonth()->format($format ?? self::DEFAULT_DATE_TIME);
	}

	public static function isEndOfMonth($timestamp = null) {
		return self::now()->format('d') <= self::endOfMonth($timestamp, 'd');
	}
}