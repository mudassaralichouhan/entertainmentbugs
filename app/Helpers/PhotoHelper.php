<?php

namespace App\Helpers;

use Illuminate\Support\Facades\File;

class PhotoHelper {
	const DEFAULT_DIR = "/public/uploads/photo/";
	const DEFAULT_IMAGE = "/public/img/story_thumb.jpg";
	const EARNING_PER_REACTION = 0.01;
	const GRAPH_DATA = array(
		"label" 			=> "Visits",
		"backgroundColor" 	=> "rgba(32, 212, 137, 1)",
		"borderColor" 		=> "transparent",
		"borderWidth" 		=> 2,
		"data" 				=> array()
	);

	public static function getGraphElement ($id = null, $graph = null) {
		$dates = json_encode($graph ? $graph['date'] : []);
		$views = json_encode($graph ? $graph['photos'] : self::GRAPH_DATA);

		return "<canvas data-chart-type='photos' data-chart-key='$id' style='height: 370px; width: 100%;' data-chart-date='$dates' data-chart-data='$views' />";
	}

	public static function getImageURL($filename, $defaultOnEmpty = false) {
		if (!empty($filename) && File::exists(self::DEFAULT_DIR.$filename)) {
			return self::DEFAULT_DIR.$filename;
		}

		return $defaultOnEmpty ? asset(self::DEFAULT_IMAGE) : null;
	}

	public static function getTotalReactionEarning($totalReactionCount) {
		return $totalReactionCount * self::EARNING_PER_REACTION;
	}

	public static function getUnpaidReactionEarning($unpaidReactionCount) {
		return $unpaidReactionCount * self::EARNING_PER_REACTION;
	}
}