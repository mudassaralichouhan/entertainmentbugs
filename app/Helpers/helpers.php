<?php

use App\Http\Controllers\MailController;
use App\Models\PostAudtion;
use App\Models\Story;
use App\Models\User;
use App\Models\Video;

if (!function_exists('name_to_pic')) {
    function name_to_pic($name)
    {
        $word = explode(" ", $name);
        return substr($word[0], 0, 1) . (isset($word[0]) ? substr($word[0], 0, 1) : "");
    }
}

if (!function_exists('is_valid_email')) {
    function is_valid_email($email)
    {
        $domain = explode('@', $email)[1];
        return getmxrr($domain, $mxhosts);
    }
}

if (!function_exists('send_mail')) {
    function send_mail($type, $email, $options)
    {
        // if(!$options['template']) return "TEMPLATE NOT FOUND";
        // $from = config('mail.username');
        // $subject = $options['subject'];
        // $message = view($options['template'], ['options' => $options])->render();

        // $custom_dir = base_path('custom-tmp');
        // if (!is_dir($custom_dir)) {
        //     mkdir($custom_dir, 0777, true);
        // }
        // $tempFile = $custom_dir."/".uniqid("email_".time()."_".rand()."_").".txt";
        // $file = fopen($tempFile, "w+");
        // fwrite($file, $message);
        // fclose($file);
        // $command = "~/commands/sendmail.sh $from $email $tempFile $subject";
        // shell_exec($command);
        return MailController::send($type, $email, $options);
    }
}

if (!function_exists('user_url')) {
    function user_url($slug)
    {
        return url("userprofile/" . $slug);
    }
}

if (!function_exists('artist_url')) {
    function artist_url($slug)
    {
        return url("artist/" . $slug);
    }
}

if (!function_exists('photo_url')) {
    function photo_url($slug)
    {
        return asset("public/uploads/photo/" . $slug);
    }
}

if (!function_exists('photo_path')) {
    function photo_path($filename)
    {
        return public_path("uploads/photo/" . $filename);
    }
}

if (!function_exists('story_path')) {
    function story_path($filename)
    {
        return "" . $filename;
    }
}

if (!function_exists('story_url')) {
    function story_url($filename)
    {
        return url("" . $filename);
    }
}

if (!function_exists('to_story')) {
    function to_story($slug)
    {
        return url("story/display/$slug");
    }
}

if (!function_exists('video_path')) {
    function video_path($filename)
    {
        return "" . $filename;
    }
}

if (!function_exists('video_url')) {
    function video_url($filename)
    {
        return asset('public/uploads/video/' . $filename);
    }
}

if (!function_exists('video_thumb_url')) {
    function video_thumb_url($filename)
    {
        return asset('public/uploads/video/thumbs/' . $filename);
    }
}

if (!function_exists('get_notitication_icon')) {
    function get_notification_icon($type, $message = "")
    {
        if (in_array($type, ['video-like', 'photo-like', 'story-like', 'like-photo', 'like-video', 'like-story', 'dislike-video', 'dislike-story'])) {
            return "<i class='fa fa-thumbs-up' aria-hidden='true'></i>";
        } elseif (in_array($type, ['video-comment', 'photo-comment', 'story-comment', 'comment-photo', 'comment-video', 'comment-story'])) {
            return '<i class="fa fa-comments-o" aria-hidden="true"></i>';
        } elseif (in_array($type, ['video-reply', 'photo-reply', 'story-reply', 'comment-reply'])) {
            return '<i class="fa fa-reply" aria-hidden="true"></i>';
        } elseif ($type == 'follows') {
            return '<i class="fa fa-heart" aria-hidden="true"></i>';
        } elseif ($type == 'apply-for-audition') {
            return '<i class="fa fa-signal" aria-hidden="true"></i>';
        } elseif ($type == 'promote-post') {
            return '<i class="fa fa-rocket" aria-hidden="true"></i>';
        } elseif ($type == 'ads-campaign') {
            return '<i class="fa fa-buysellads" aria-hidden="true"></i>';
        } else {
            return '<i class="fa fa-check-square" aria-hidden="true"></i>';
        }
    }
}

if (!function_exists('get_notitication_link')) {
    function get_notification_link($data)
    {
        switch ($data['type']) {
            case 'video-like':
            case 'like-video':
            case 'comment-video':
            case 'video-comment':
            case 'video-reply':
            case 'video-uploaded':
            case 'dislike-video':
                $id = isset($data['video_id']) ? $data['video_id'] : (isset($data['action_id']) ? $data['action_id'] : "");
                $video = Video::find($id);
            if ($video) {
                return url("watch/" . ($video->video_key));
                }
                return "#";
                break;

            case 'photo-like':
            case 'like-photo':
            case 'comment-photo':
            case 'photo-comment':
            case 'photo-reply':
            case 'photo-uploaded':
                $id = isset($data['photo_id']) ? $data['photo_id'] : (isset($data['action_id']) ? $data['action_id'] : "");
                return url("single-photo/$id");
                break;

            case 'story-like':
            case 'like-story':
            case 'comment-story':
            case 'story-comment':
            case 'story-reply':
            case 'story-uploaded':
                $id = isset($data['story_id']) ? $data['story_id'] : (isset($data['action_id']) ? $data['action_id'] : "");
                $story = Story::find($id);
            if ($story) {
                    return url("story/display/$story->slug");
                }
                return;
                break;

            case 'follows':
                if (isset($data['user_id'])) {
                    $user = \App\Models\User::find($data['user_id']);
                    if ($user) {
                        return user_url($user->slug);
                    }
                }
                return;
                break;

            case 'apply-for-audition':
            case 'applied-for-audition':
                $id = isset($data['action_id']) ? $data['action_id'] : "";
            if ($id) {
                    $audition = PostAudtion::find($id);
                if ($audition) {
                        return url("audition/$audition->slug");
                    }
                }
                return "#";
                break;

            case "promote-post":
                $id = isset($data['action_id']) ? $data['action_id'] : "";
                if ($id) {
                    return url("promoted-post/$id");
                }
                return "#";
                break;

            case "ads-campaign":

                break;

            case "theme-plan":
                return url("theme-plan");
                break;

            case "premium-profile":
                return url("boost-plan");
                break;

            default:
        }

        switch (strtolower(trim($data['message']))) {
            case 'video uplaoded successfully':
                $id = isset($data['video_id']) ? $data['video_id'] : (isset($data['action_id']) ? $data['action_id'] : "");
                $video = Video::find($id);
                if ($video) {
                    return url("watch/" . ($video->video_key));
                }
                return "#";
                break;

            case 'reward video uplaoded successfully':
                $id = isset($data['video_id']) ? $data['video_id'] : (isset($data['action_id']) ? $data['action_id'] : "");
                $video = Video::find($id);
                if ($video) {
                    return url("watch-reward/" . ($video->video_key));
                }
                return "#";
                break;

            case 'added a new photo':
                $id = isset($data['photo_id']) ? $data['photo_id'] : (isset($data['action_id']) ? $data['action_id'] : "");
                return url("single-photo/$id");
                break;

            case 'added a new story':
                $id = isset($data['video_id']) ? $data['story_id'] : (isset($data['action_id']) ? $data['action_id'] : "");
                $story = Story::find($id);
                if ($story) {
                    return url("story/display/$story->slug");
                }
                return;
                break;

            default:
        }
    }
}

if (!function_exists('get_like_notification_icon')) {
    function get_like_notification_icon($needle)
    {
        if (in_array($needle, ['video-like', 'photo-like', 'story-like', 'like-photo', 'like-video', 'like-story'])) {
            return '<strong><i class="fa fa-heart" aria-hidden="true"></i></strong>';
        }
    }
}

if (!function_exists('get_comment_notification_icon')) {
    function get_comment_notification_icon($needle)
    {
        if (in_array($needle, ['video-comment', 'photo-comment', 'story-comment', 'comment-photo', 'comment-video', 'comment-story'])) {
            return '<strong><i class="fa fa-comments-o" aria-hidden="true"></i></strong>';
        }
    }
}

if (!function_exists('get_success_notification_icon')) {
    function get_success_notification_icon($haystack)
    {
        $haystack = strtolower($haystack);
        if (strpos($haystack, "success") !== false || strpos($haystack, "successfully") !== false) {
            return '<strong><i class="fa fa-check-square" aria-hidden="true"></i></strong>';
        }
    }
}

if (!(function_exists('get_notification_icon'))) {
    function get_notification_icon($notifiable_id, $notifier_id, $message, $isFollowing = false)
    {
        if ($notifiable_id == $notifier_id) {
            return '<strong><i class="fa fa-heart" aria-hidden="true"></i></strong>';
        } else {
            if (strpos($message, "commented") != false) {
                return '<strong><i class="fa fa-comments-o" aria-hidden="true"></i></strong>';
            } elseif ($isFollowing && (strpos($message, "added") != false || strpos($message, "uploaded") != false || strpos($message, "successfully") != false)) {
                return '<strong><i class="fa fa-upload"></i></strong>';
            }
        }
    }
}

if (!function_exists('get_story_thumb_url')) {
    function get_story_thumb_url($image, $type = "thumb")
    {
        $url = preg_match("/^(http|https):\/\//", $image)
            ? $image
            : ($type == "cover" ? STORY_COVER_DISPLAY_PATH : STORY_THUMB_DISPLAY_PATH) . $image;

        $splitted = explode("/", $url);
        $len = count($splitted);
        if ($splitted[$len - 1] == $splitted[$len - 2]) {
            array_pop($splitted);
            $url = implode("/", $splitted);
        }

        return $url;
    }
}

if (!function_exists('check_asset_existence')) {
    function check_asset_existence($url)
    {
        return file_exists(preg_replace("/^(http|https):\/\/(.+)\/public\//", public_path("/"), $url));
    }
}

if (!function_exists('seconds_to_duration')) {
    function seconds_to_duration($time)
    {
        return gmdate("H:i:s", $time);
    }
}

if (!function_exists('get_promoted_post')) {
    function get_promoted_post($tag, $post_id, $status = 1)
    {
        $whereClause = [
            ['post_type', $tag],
            ['post_id', $post_id]
        ];

        if ($status) {
            array_push($whereClause, ['status', $status]);
        }

        return \App\Models\PromotePostModel::where($whereClause)->first();
    }
}

if (!function_exists('to_amount')) {
    function to_amount($amount)
    {
        return number_format($amount, 2, '.', ',');
    }
}

if (!function_exists('utf8ize')) {
    function utf8ize($d)
    {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = utf8ize($v);
            }
        } elseif (is_object($d)) {
            foreach ($d as $k => $v) {
                $d->$k = utf8ize($v);
            }
        } elseif (is_string($d)) {
            return mb_convert_encoding($d, 'UTF-8', mb_detect_encoding($d));
        } elseif (is_bool($d) && !$d) {
            return 0;
        } elseif (is_bool($d) && $d) {
            return 1;
        } elseif (is_null($d)) {
            return 0;
        }

        return $d;
    }
}

if (!function_exists('is_var_exist')) {
    function is_var_exist($haystack, $key, $index = 0)
    {
        if ($haystack && count($haystack) > 0) {
            if (((is_int($index) || is_string($index)) && isset($haystack[$index]) && isset($haystack[$index]->{$key})) || (!is_int($index) && !is_string($index) && isset($haystack->{$key}))) {
                return true;
            }
        }

        return false;
    }
}

if (!function_exists('get_user_photo_from_id')) {
    function get_user_photo_from_id(User $user)
    {
        return $user->photo;
    }
}

if (!function_exists('get_avatar')) {
    function get_avatar($photo, $name)
    {
        if (preg_match("/^https?:\/\//", $photo)) {
            return "<img src='$photo' alt='$name' class='small-img' />";
        }

        if (!(empty($photo)) && file_exists(public_path('uploads/users/small/' . $photo))) {
            return "<img src='" . asset('public/uploads/users/small/' . $photo) . "' alt='$name' class='small-img' />";
        }

        return "<div class='shortname'>" . name_to_pic($name) . "</div>";
    }
}

if (!function_exists('get_second_last_week')) {
    function get_second_last_week()
    {
        $toSunday = \Carbon\Carbon::now()->startOfWeek()->subDay(2);
        $fromSunday = \Carbon\Carbon::now()->startOfWeek()->subWeek()->subDay(1);

        return [$toSunday, $fromSunday];
    }
}


if (!function_exists('get_last_week')) {
    function get_last_week()
    {
        $fromSunday = \Carbon\Carbon::now()->startOfWeek(\Carbon\Carbon::SUNDAY)->subDay(1);
        $toSunday = \Carbon\Carbon::now()->startOfWeek(\Carbon\Carbon::SUNDAY)->addWeek()->subDay(2);

        return [$toSunday, $fromSunday];
    }
}
