<?php

namespace App\Helpers;

class TransactionHelper {
	const MIN_PAYOUT_AMOUNT = 1500;

	public static function isPayoutRequestDay() {
		return DateTimeHelper::isEndOfMonth();
	}

	public static function getMinPayoutAmount($formatted = true) {
		return $formatted ? AmountHelper::getReadableFormat(self::MIN_PAYOUT_AMOUNT) : self::MIN_PAYOUT_AMOUNT;
	}
}