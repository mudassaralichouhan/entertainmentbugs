<?php

namespace App\Helpers;

use App\Models\User;
use Illuminate\Support\Facades\Session;

class UserHelper {
	private static $currentUser = null;

	public static function getAuthUser() {
		if (Session::has('user_id')) {
            self::$currentUser = self::$currentUser ?? User::find(Session::get('user_id'));
        }

        return self::$currentUser;
	}
}