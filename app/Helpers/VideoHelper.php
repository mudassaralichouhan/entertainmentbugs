<?php

namespace App\Helpers;

class VideoHelper
{
    const SECONDS_SET = 3;
    const EARNING_PER_SECOND = 0.01;
    const REWARD_VIDEOS = 1;
    const NORMAL_VIDEOS = 0;
    const DEFAULT_THUMBNAIL = "public/img/video1-1.png";
    const GRAPH_DATA = array(
        "label" => "Visits",
        "backgroundColor" => "rgba(32, 212, 137, 1)",
        "borderColor" => "transparent",
        "borderWidth" => 2,
        "data" => array()
    );

    public static function getGraphElement($id = null, $graph = null, $isRewardVideo = false)
    {
        $type = $isRewardVideo ? "reward" : "normal";
        $dates = json_encode($graph ? $graph['date'] : []);
        $views = json_encode($graph ? $graph['videos'] : self::GRAPH_DATA);

        return "<canvas data-chart-type='$type' data-chart-key='$id' style='height: 370px; width: 100%;' data-chart-date='$dates' data-chart-data='$views' />";
    }

    public static function getCalculatedTracks($tracks)
    {
        $totalUnpaidSeconds = 0;
        $totalUnpaidEarning = 0;
        $totalSeconds = 0;
        $totalEarning = 0;
        $totalViews = 0;
        $track = array();
        $lastUserId = null;

        foreach ($tracks as $track) {
            if ($track->user_id != $lastUserId) {
                $totalViews = $totalViews + 1;
                $lastUserId = $track->user_id;
            }

            if (!preg_match("/^\[(.+)\]$/", $track->track)) {
                continue;
            }

            $isTrack = false;
            if (!(isset($track[$track->user_id]))) {
                $track[$track->user_id] = array(
                    "unpaid" => array(),
                    "paid" => array()
                );
            }

            $sets = json_decode($track->track, true);
            if (json_last_error() == JSON_ERROR_NONE) {
                $isTrack = true;
            }

            if (!$isTrack) {
                continue;
            }

            foreach ($sets as $k => $set) {
                if ($track->is_paid == 0) {
                    if (isset($track[$track->user_id]["unpaid"][$k])) {
                        if ($track[$track->user_id]["unpaid"][$k] != 1 && $set == 1) {
                            $track[$track->user_id]["unpaid"][$k] = 1;
                        }
                    } else {
                        array_push($track[$track->user_id]["unpaid"], $set);
                        if (!(isset($track[$track->user_id]["paid"][$k]))) {
                            $track[$track->user_id]["paid"][$k] = 0;
                        }
                    }
                } else {
                    if (isset($track[$track->user_id]["paid"][$k])) {
                        if ($track[$track->user_id]["paid"][$k] != 1 && $set == 1) {
                            $track[$track->user_id]["paid"][$k] = 1;
                        }
                    } else {
                        array_push($track[$track->user_id]["paid"], $set);
                        if (!(isset($track[$track->user_id]["unpaid"][$k]))) {
                            $track[$track->user_id]["unpaid"][$k] = 0;
                        }
                    }
                }
            }
        }

        foreach ($track as $user => $status) {
            foreach ($status['paid'] as $k => $v) {
                $totalSeconds += $v;
                if ($v == 1) {
                    continue;
                }

                if ($status['unpaid'][$k] != 1) {
                    continue;
                }

                $totalUnpaidSeconds += 1;
                if ($v == 0) {
                    $totalSeconds += 1;
                }
            }
        }

        if ($totalUnpaidSeconds != 0) {
            $totalUnpaidSeconds = $totalUnpaidSeconds * self::SECONDS_SET;
            $totalUnpaidEarning = $totalUnpaidSeconds * self::EARNING_PER_SECOND;
        }

        if ($totalSeconds != 0) {
            $totalSeconds = $totalSeconds * self::SECONDS_SET;
            $totalEarning = $totalSeconds * self::EARNING_PER_SECOND;
        }

        return collect([
            'unpaidSeconds' => $totalUnpaidSeconds,
            'totalSeconds' => $totalSeconds,
            'totalHours' => DateTimeHelper::secondsToHours($totalSeconds),
            'totalViews' => $totalViews,
            'totalEarning' => $totalEarning,
            'totalUnpaidEarning' => $totalUnpaidEarning,
            'totalUnpaidHours' => DateTimeHelper::secondsToHours($totalUnpaidSeconds),
            'totalMinutes' => DateTimeHelper::secondsToMinutes($totalSeconds),
            'ids' => $track->pluck('id')->toArray()
        ]);
    }

    public static function prices()
    {
        [$toSunday, $fromSunday] = get_second_last_week();

        $reward_prizes = \DB::table('reward_prizes')
            ->select(['prizes'])
            ->whereBetween('created_at', [
                $fromSunday->format("Y-m-d 00:00:00"),
                $toSunday->format("Y-m-d 00:00:00"),
            ])
            ->orderBy('created_at', 'desc')
            ->first();

        foreach (json_decode($reward_prizes->prizes ?? '') as $reward => $price) {
            // Check if the key contains a range
            if (strpos($reward, '-') !== false) {
                list($start, $end) = explode('-', $reward);
                for ($i = $start; $i <= $end; $i++) {
                    $result[$i] = $price;
                }
            } else {
                // If the key does not contain a range, simply add it to the result
                $result[$reward] = $price;
            }
        }

        return $result ?? [];
    }
}