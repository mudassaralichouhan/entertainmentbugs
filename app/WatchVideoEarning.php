<?php

namespace App;

use App\Helpers\DateTimeHelper;
use App\Models\Video;
use Illuminate\Database\Eloquent\Model;

class WatchVideoEarning extends Model {
	protected $table = "watch_video_earning";

    protected $fillable = [
        'video_id',
        'user_id',
        'total_watch_time',
        'unpaid_watch_time',
        'unpaid_earning',
        'total_earning',
        'total_views',
        'status',
    ];

    protected $casts = [
        'video_id' => 'integer',
        'user_id' => 'integer',
        'total_watch_time' => 'integer',
        'unpaid_watch_time' => 'integer',
        'unpaid_earning' => 'integer',
        'total_earning' => 'integer',
        'total_views' => 'integer',
        'status' => 'boolean',
    ];

	protected $attributes = [
		'total_watch_time' => 0,
		'unpaid_watch_time' => 0,
		'unpaid_earning' => 0,
		'total_earning' => 0,
		'total_views' => 0,
	];

	public function video() {
		return $this->belongsTo(Video::class, 'video_id');
	}

	public function getTotalWatchTime() {
		return DateTimeHelper::secondsToHours($this->total_watch_time);
	}

	public function getUnpaidWatchTime() {
		return DateTimeHelper::secondsToHours($this->unpaid_watch_time);
	}

	public function getTotalEarning() {
		return $this->total_earning;
	}

	public function getUnpaidEarning() {
		return $this->unpaid_earning;
	}
}
