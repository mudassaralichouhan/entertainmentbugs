<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RedirectNonWww
{
    public function handle(Request $request, Closure $next)
    {
        if (strpos($request->header('host'), 'www.') !== 0) {
            $url = $request->url();
            $url = preg_replace('#^https?://#', '', $url);
            return redirect('https://www.'.$url);
        }

        return $next($request);
    }
}
