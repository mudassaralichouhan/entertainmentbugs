<?php

namespace App\Http\Middleware;

use App\Events\PageViewEvent;
use Closure;

class PageViewMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        event(new PageViewEvent($request->fullUrl()));
        return $response;
    }
}
