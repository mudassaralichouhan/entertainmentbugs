<?php
namespace App\Http\Middleware;
use Closure;
use Session;
use Illuminate\Support\Facades\Route;

class IsUserlogin
{
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * 
     */
    public function handle($request, Closure $next){        
        if (Session::has('user_id')){
           
            return $next($request);
        }else{
            if(Route::getFacadeRoot()->current()->uri() == 'userprofile/{user}'){

                session(['url.intended' => url()->current()]);
            } else {
                session(['url.intended' => url()->current()]);
            }
            
            return redirect('/login');
        }
    }
}