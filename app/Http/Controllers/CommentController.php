<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Video;
use App\Models\Story;
use App\Models\Comment;
use App\Models\photocomment;
use App\UploadPhotos;
use Session;
use App\Notifications\Notices;
use App\Models\User;

class CommentController extends Controller
{
    public function store(Request $request)
    {
		$user = User::find(Session::get('user_id')); 
        
        $comment = new Comment;
        $comment->body = $request->get('comment_body');
        $model = $request->get('model');
        $comment->user()->associate(Session::get('user_id'));
        if($model == 'Video'){
            $video = Video::find($request->get('video_id'));
            $video->comments()->save($comment);
           
            
            $videoData = Video::where(['id' => $request->get('video_id'), 'status' => 1])
                            ->with([
                                'thumb' => function($thumb){
                                    $thumb->select('id','thumb');
                                },
                                'user' => function($thumb){
                                    $thumb->select('id','name','photo');
                                },
                                'video_category' => function($thumb){
                                    $thumb->select('id', 'video_id', 'category_id');
                                },
                                ])
                            ->first();
            $comments = $videoData->comments;
            $video_id = $videoData->id;
			$other_user = User::find($videoData->user_id);        
			$other_user->notify(new Notices($user, 'Commented on your video', 'video-comment', $request->get('comment_body'), '', '', $video_id));
            
            $view = view('comment.video._comment_replies',compact('comments', 'videoData','video_id'))->render();
            return  response()->json(['html'=>$view]);

        } else if($model == 'Story'){
            $story = Story::find($request->get('story_id'));
            $story->comments()->save($comment);
            $story =  Story::where('id', $story->id)
                    ->select('id','title','about', 'cover_image', 'language', 'category', 'user_id', 'created_at')
                    ->orderBy('created_at','desc')
                    ->with(['user' => function($user){
                        $user->select('id','name', 'photo');
                    }])
                    ->first();
            $comments = $story->comments;

            $other_user = User::find($story->user_id);        
			$other_user->notify(new Notices($user, 'Commented on your story', 'story-comment', $request->get('comment_body'), '', '', $story->id));
            
            $view = view('comment.story._comment_replies',compact('comments', 'story'))->render();
            return  response()->json(['html'=>$view]);

        }        
       
    }

    public function replyStore(Request $request)
    {
      
        $reply = new Comment();
        $reply->body = $request->get('comment_body');
        $model = $request->get('model');
        $reply->user()->associate(Session::get('user_id'));
        $reply->parent_id = $request->get('comment_id');
        if($model == 'Video'){
            $video = Video::find($request->get('video_id'));
            $video->comments()->save($reply);

            $videoData = Video::where(['id' => $request->get('video_id'), 'status' => 1])
                            ->with([
                                'thumb' => function($thumb){
                                    $thumb->select('id','thumb');
                                },
                                'user' => function($thumb){
                                    $thumb->select('id','name','photo');
                                },
                                'video_category' => function($thumb){
                                    $thumb->select('id', 'video_id', 'category_id');
                                },
                                ])
                            ->first();
            $comments = $videoData->comments;
            $video_id = $videoData->id;
            $view = view('comment.video._comment_replies',compact('comments', 'videoData','video_id'))->render();
            return  response()->json(['html'=>$view]);
        } else if($model == 'Story'){
            $story = Story::find($request->get('story_id'));
            $story->comments()->save($reply);

            $story =  Story::where('id', $story->id)
                    ->select('id','title','about', 'language', 'cover_image', 'category', 'user_id', 'created_at')
                    ->orderBy('created_at','desc')
                    ->with(['user' => function($user){
                        $user->select('id','name', 'photo');
                    }])
                    ->first();
            $comments = $story->comments;
            $view = view('comment.story._comment_replies',compact('comments', 'story'))->render();
            return  response()->json(['html'=>$view]);
        }   
    }


    public function store2(Request $request)
    {
		$user = User::find(Session::get('user_id'));
        $model = $request->get('model');
       
        if($model == 'photo'){
            $photo = UploadPhotos::find($request->get('photo_id'));

            $comments = new photocomment;
            $comments->user()->associate(Session::get('user_id'));
            $comments->body = $request->get('comment_body');
            $comments->parent_id = $photo->id;

            $commentableId = $request->get('comment_id');
            if( $commentableId ){
                $comments->commentable_id = $commentableId;
            }

            $comments->save();

            $other_user = User::find($photo->user_id);
            $other_user->notify(new Notices($user, $commentableId ? 'replied to your photo' : 'Commented on your photo', $commentableId ? 'photo-reply' : 'photo-comment', $request->get('comment_body'), '', '', $photo->id));

            $query = photocomment::select("commentable_id", "user_id")->where([["id", $comments->id], ['parent_id', $photo->id]])->first();

            if( $query ){
                $ids = [$other_user->id];
                $_id = $query->commentable_id;
                
                if( !in_array($query->user_id, $ids) ){
                    $_user = User::find($query->user_id);
                    $_user->notify(new Notices($user, $commentableId ? 'replied to your comment' : 'Commented on your photo', $commentableId ? 'photo-reply' : 'photo-comment', $request->get('comment_body'), '', '', $photo->id));
                    array_push($ids, $query->user_id);
                }
                
                while( $_id ){
                    $query = photocomment::select("commentable_id", "user_id")->where([["id", $query->commentable_id], ['parent_id', $photo->id]])->first();

                    if( $query ){
                        $_id = $query->commentable_id;
                        if( !in_array($query->user_id, $ids) ){
                            $_user = User::find($query->user_id);
                            $_user->notify(new Notices($user, $commentableId ? 'replied to your comment' : 'Commented on your photo', $commentableId ? 'photo-reply' : 'photo-comment', $request->get('comment_body'), '', '', $photo->id));
                        }
                    } else {
                        $_id = null;
                    }
                }
            }

            $comments->name = $user->name;
            $comments->photo = $user->photo;

            $html = view("comment.photo", [
                'comments'  => [$comments]
            ])->render();
           
            return response()->json(['html' => $html], 200);
        }
    }

    public function showcmnt(Request $request)
    {
		$user = User::find(Session::get('user_id')); 
        $comments = new photocomment;
        $comments->body = $request->get('comment_body');
        $model = $request->get('model');
        $comments->user()->associate(Session::get('user_id'));
       
        if($model == 'photo'){
            $video = UploadPhotos::find($request->get('photo_id'));
            $comments->parent_id = $video->id;
            $comments->save();
            dd();

            // $other_user = User::find($comments->user_id);        
			// $other_user->notify(new Notices($user, 'Commented on your photo', 'story-comment', $request->get('comment_body')));
            // $view = view('comment.video._photo_comment_replies',compact('comments'))->render();
           
            return  response()->json(['html'=>$comments]);
        }
        
    }

    public function storeReelComment(Request $request){
		// $user = User::find(Session::get('user_id'));       
    
        // $comments = new photocomment;
        // $comments->body = $request->get('comment_body');
        // $model = $request->get('model');
        // $comments->user()->associate(Session::get('user_id'));

        // photocomment::select('photocomments.*', 'users.id as userID', 'users.photo as photo', 'users.name as name', 'users.slug as slug')
		// 	->join('users', 'photocomments.user_id', '=', 'users.id')
		// 	->where(['parent_id' => $request->value])->get();


        // return response()->json([
            
        // ], 201);
    }

    public function getPhotoComments($photo_id, $comment_id = null){
        $pattern = [
            ['parent_id', $photo_id]
        ];

        array_push($pattern, ['commentable_id', $comment_id ? $comment_id : 0]);

		$comments = photocomment::select('photocomments.*', 'users.id as userID', 'users.photo as photo', 'users.name as name', 'users.slug as slug')
			->join('users', 'photocomments.user_id', '=', 'users.id')
			->where($pattern)->orderByDesc('photocomments.id')->paginate(3);

        $comment_id
        ? $comments->setPath("comment/parent/$comment_id")
        : $comments->setPath("comment/parent");

        $html = view("comment.photo", [
            'comments'  => $comments,
            'type'      => $comment_id ? 'reply' : 'comment'
        ])->render();

        $res = ['html' => $html];
        if( $comment_id ) { $res['length'] = $comments ? count($comments) : 0; }
        return response()->json($res, 200);
    }
}
