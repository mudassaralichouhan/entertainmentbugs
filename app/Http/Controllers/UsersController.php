<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Cookie;
use Session;
use Redirect;
use Input;
use Validator;
use DB;
  use Illuminate\Foundation\Auth\AuthenticatesUsers;
//use Mail;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;
use Response;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
use App\Models\Country;
use App\Models\States;
use App\Models\Language;
use App\User as users;
use App\Models\Category;
use App\Models\Video;
use App\Notifications\UserFollowed;
use App\Mail\MailNotify;
use Carbon\Carbon;

class UsersController extends Controller {
	use AuthenticatesUsers;
	// Function for making slug
	 public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}
	public function get()
	{
		return "Hello";
	}
	public static function make_slug($slug)
	{
		return strtolower(preg_replace('/\s+/u', '-', trim($slug)));
	}

	private function verifyLogin(){
		$request = request();
		$storedSession = $request->session();
		$hasSession = $hasCookie = false;
		$user = null;

		if($storedSession && $storedSession->get('user_id') && $storedSession->get('user_name') && $storedSession->get('slug') && $storedSession->get('email') && !(empty($storedSession->get('user_id')))){
			$user = User::where([['id', $storedSession->get('user_id')], ['email', $storedSession->get('email')], ['slug', $storedSession->get('slug')]])->first();

			if( $user ){
				$hasSession = true;
			}
		}
		elseif($request->cookie('user_email_address') && $request->cookie('user_password') && $request->cookie('user_remember') && $request->cookie('user_remember') == 1){
			$user = User::where([['email', $request->cookie('email')], ['password', $request->cookie('user_password')]])->first();

			if( $user ){
				$hasCookie = true;
			}
		}

		return [$hasSession, $hasCookie, $user];
	}

	public function login(Request $request) {
		$pageTitle = 'Login';
		$input = Input::all();
		$error = false;
		list($hasSession, $hasCookie, $user) = [false, false, null];
		
		if (!empty($input)) {
			$rules = array(
				'email_address' => 'required|email',
				'password' => 'required'
			);
			
			$validator = Validator::make($input, $rules);
			if ($validator->fails()) {
				return Redirect::to('/login')->withErrors($validator)->withInput(Input::except('password'));
			} else {
				$user = User::where('email', $input['email_address'])->first();
				if (!empty($user)) {
					if (password_verify($input['password'], $user->password)) {
						if ($user->status == 1 && $user->verified == 1) {
							if($user->isDeleted){
								return Redirect::to('/accountblock');
							}
							if (isset($input['user_remember']) && $input['user_remember'] == '1') {
								$hasCookie = true;
							} else {
								$hasSession = true;
							}
						}
					} else {
						$error = 'Invalid email or password.';
					}
				} else {
					$error = 'Invalid email or password.';
				}

				if( $error ){
					return Redirect::to('/login')->withErrors($error)->withInput(Input::except('password'));
				}
			}
		} else {
			list($hasSession, $hasCookie, $user) = $this->verifyLogin();
		}

		if(($hasSession || $hasCookie) && $user){
			if( $user->verified == 0 ){
				$error = 'You need to activate your account before login.';
			} elseif( $user->status == 0 ){
				$error = "Your account might have been temporarily disabled. Please contact us for more details.";
			}

			if( $error ){
				Redirect::to("/login")->withErrors($error);
			}

			if( $hasCookie ){
				$time = Carbon::now()->addWeek()->timestamp;

				Cookie::queue('user_email_address', $time, "/");
				Cookie::queue('user_password', $user->password, $time, "/");
				Cookie::queue('user_remember', '1', $time, "/");
			}
			
		
            
			if(!$user->name || !$user->slug || $user->country_id == 0 || $user->state_id == 0 ){
		       
				Session::put('is_profile_completed', true);
				
			}else{
				Session::put('is_profile_completed', false);
			}
					
		   	
			Session::put('user_id', $user->id);
			Session::put('user_name', ucwords($user->name));
			Session::put('slug', $user->slug);
			Session::put('email', $user->email);
			Session::put('profile_pic', $user->photo);

			if(session()->get('url.intended')){
				return redirect(session()->get('url.intended'));
			}

			return Redirect::to('/');
		}
		return view('users.login', ['title' => $pageTitle]);
	}

	public function forgotPassword() {
		$pageTitle = 'Forgot Password';
		$input = Input::all();
		if (!empty($input)) {
			$rules = array(
				'email_address' => 'required|email',
			);
			$validator = Validator::make($input, $rules);
			if ($validator->fails()) {
				return Redirect::to('/forgot-password')->withErrors($validator);
			} else {
				$userInfo = User::where('email', $input['email_address'])->first();
			  
				if (!empty($userInfo)) {
					$uniqueKey = bin2hex(openssl_random_pseudo_bytes(25));
					User::where('id', $userInfo->id)->update(array('forget_password_status' => 1, 'unique_key' => $uniqueKey));

					$link = HTTP_PATH . "/reset-password/" . $uniqueKey;
					$name = ucwords($userInfo->name);
					send_mail('forgot-password', $userInfo->email, ['template' => 'email.auth.forgot-password', 'subject' => 'Forgot Password', 'link' => $link, 'name' => $name]);

					Session::flash('success_message', "A link to reset your password was sent to your email address.");
					return Redirect::to('/login');
				} else {
					$error = 'Please enter valid email address.';
				}
				return Redirect::to('/forgot-password')->withErrors($error);
			}
		}
		
		return view('users.forgotPassword', ['title' => $pageTitle]);
	}

	public function resetPassword($ukey = null) {
		$pageTitle = 'Reset Password';
		$userInfo = User::where('unique_key', $ukey)->first();
		if ($userInfo && $userInfo->forget_password_status == 1) {
			$input = Input::all();
			if (!empty($input)) {
				$rules = array(
					'password' => 'required|min:8',
					'confirm_password' => 'required|same:password',
				);
				$validator = Validator::make($input, $rules);
				if ($validator->fails()) {
					return Redirect::to('/reset-password/' . $ukey)->withErrors($validator);
				} elseif (password_verify($input['password'], $userInfo->password)) {
					return Redirect::to('/reset-password/' . $ukey)->withErrors('You cannot put your old password as new password, please another password.');
				} else {
					$new_password = $this->encpassword($input['password']);
					User::where('id', $userInfo->id)->update(array('forget_password_status' => 0, 'password' => $new_password));
					Session::flash('success_message', "A link to reset your password was sent to your email address.");
					return Redirect::to('/login');
				}
			}
			return view('users.resetPassword', ['title' => $pageTitle]);
		} else {
			Session::flash('error_message', "You have already use this link!");
			return Redirect::to('/login');
		}
	}

	public function register() {
		$pageTitle = 'Register';
		$input = Input::all();
		if (!empty($input)) {
			$rules = array(
				'name' => 'required|max:30',
				'email' => 'required|email|unique:users',
				'password' => 'required|min:8',
				'confirm_password' => 'required|same:password'                
			);
			$validator = Validator::make($input, $rules);
			if ($validator->fails()) {
				return Redirect::to('/register')->withErrors($validator)->withInput(Input::except('password'));
			} else {
				unset($input['g-recaptcha-response']);
				$input['name'] = ucfirst(trim($input['name']));
				$getMaxId = DB::select('select max(id) as maxId from users');
				$input['slug'] = self::make_slug($input['name']). '-'.($getMaxId[0]->maxId + 1);
				$serialisedData = $this->serialiseFormData($input);
				$serialisedData['status'] = 1;
				$serialisedData['verified'] = 1;
				$serialisedData['password'] = $this->encpassword($input['password']);
				$uniqueKey = bin2hex(openssl_random_pseudo_bytes(25));
				$serialisedData['unique_key'] = $uniqueKey;
				$user_id = User::insertGetId($serialisedData);
				$link = HTTP_PATH . "/email-confirmation/" . $uniqueKey;
				$name = $input['name'];
				$emailId = $input['email'];
				$new_password = $input['password'];

				// $emailTemplate = DB::table('emailtemplates')->where('id', 3)->first();
				// $toRepArray = array('[!email!]', '[!username!]', '[!password!]', '[!link!]', '[!HTTP_PATH!]', '[!SITE_TITLE!]');
				// $fromRepArray = array($emailId, $name, $new_password, $link, HTTP_PATH, SITE_TITLE);
				// $emailSubject = str_replace($toRepArray, $fromRepArray, $emailTemplate->subject);
				// $emailBody = str_replace($toRepArray, $fromRepArray, $emailTemplate->template);
				// Mail::to($emailId)->send(new SendMailable($emailBody, $emailSubject));

				//Session::flash('success_message', "We have sent you an account activation link by email. Please check your spam folder if you do not receive the email within the next few minutes.");
	//send_mail('register',$emailId, ['template' => 'email.auth.thankyou', 'subject' => 'Thank you For signup ', 'link' => $link, 'name' => $input['name']]);
              Mail::to($emailId)->send(new \App\Mail\RegisterMail($input));
                Session::flash('success_message', "Great!!! Your account successfully created.");
                //return Redirect::to('/login');
            }
        }
        return view('users.register', ['title' => $pageTitle]);
    }

	public function emailConfirmation($ukey = null) {
		$userInfo = User::where('unique_key', $ukey)->first();
		if ($userInfo) {
			if ($userInfo->activation_status == 1) {
				Session::flash('error_message', "You have already use this link!");
			} else {
				User::where('id', $userInfo->id)->update(array('activation_status' => 1, 'status' => 1));
				Session::flash('success_message', "Your account has been activated successfully.");
			}
		} else {
			Session::flash('error_message', "Invalide URL!");
		}
		return Redirect::to('/login');
	}

	public function redirectToGoogle() {
	   
	   return Socialite::driver('google')->redirect();
	}
	

	public function handleGoogleCallback() {
	   //return $request->all();
		try {
		  
		$user = Socialite::driver('google')->stateless()->user();
		$user= $user->getData();
			$finduser = users::where('google_id', $user->id)->first();
			if($finduser){
			Session::put('user_id', $finduser->id);
			Session::put('user_name', ucwords($finduser->name));
			Session::put('slug', $finduser->slug);
			Session::put('email', $finduser->email);
			Session::put('profile_pic', $finduser->photo);
			 return  "<script>window.close();window.opener.location.reload();</script>";
		  
			}else{
			   
				$data = array();
				$data['login_type'] = 'google';
				$data['id'] = $user->id;
				$data['name'] = $user->name;
				$data['email'] = $user->email;
				$data['avatar_original'] = $user->picture;
			 return $this->sociallogin($data);
			  
			}
		} catch (\Exception $e) {
		   
		   
			dd($e);
			//return redirect('google');
		}
	}

	public function redirectToFacebook() {
		return Socialite::driver('facebook')->redirect();
	}

	public function handleFacebookCallback() {
		try {
			$user = Socialite::driver('facebook')->user()->getData();
			$data = array();
			$data['login_type'] = 'facebook';
			$data['social_id'] = $user->id;
			$data['name'] = $user->name;
			$data['email'] = $user->email;
			$data['avatar_original'] = $user->avatar_original;
			$this->sociallogin($data);
		} catch (Exception $e) {
			echo $e->getMessage();
			exit;
			return redirect('auth/facebook');
		}
	}

	public function sociallogin($data = array()) {
	 
		if (empty($data['email'])) {
			Session::flash('error_message', "Social login not return email address, so please try with mnormal login/signup.");
		} else {
			$emailAddress = $data['email'];
			$userInfo = users::where('email', $emailAddress)->first();
			if ($userInfo) {
				if($userInfo->status ==null){
					 $userInfo->update(['google_id'=> $data['id'],'status'=>1]);
				}
				elseif ($userInfo->status == 1) {
				   
				Session::put('user_id', $userInfo->id);
				Session::put('user_name', ucwords($userInfo->name));
				Session::put('slug', $userInfo->slug);
				Session::put('email', $userInfo->email);
				Session::put('profile_pic', $userInfo->photo);
					 //return Redirect::to('users/dashboard');
				} 
				else {
				Session::put('user_id', $userInfo->id);
				Session::put('user_name', ucwords($userInfo->name));
				Session::put('slug', $userInfo->slug);
				Session::put('email', $userInfo->email);
				Session::put('profile_pic', $userInfo->photo);
			   $userInfo->update(['google_id'=> $data['id']]);
				
				  Session::flash('error_message', "Your account might have been temporarily disabled. Please contact us for more details.");
				}
			} else {
				//$nameArray = explode(' ', $data['name']);
				// $firstName = array_shift($nameArray);
				// $lastName = implode(' ', $nameArray);
				$serialisedData = array();
				 $serialisedData['name'] = $data['name'];
				$serialisedData['email'] = $data['email'];
				if ($data['login_type'] == 'google') {
				   
			  
					$serialisedData['google_id'] = $data['id'];
					$serialisedData['facebook_id'] = '';
				} else {
					// $serialisedData['first_name'] = $firstName;
					// $serialisedData['last_name'] = $lastName;
					$serialisedData['facebook_id'] = $data['social_id'];
					$serialisedData['google_id'] = '';
				}

				$serialisedData['slug'] = $this->createSlug($data['name'], 'users');
				$serialisedData['status'] = 1;
				$password = bin2hex(openssl_random_pseudo_bytes(4));
				$serialisedData['password'] = $this->encpassword($password);
				$fan = \App\Models\User::insert($serialisedData);
				$name = $data['name'];
				$emailId = $data['email'];
				$login_type = $data['login_type'];

				// $emailTemplate = DB::table('emailtemplates')->where('id', 5)->first();
				// $toRepArray = array('[!email!]', '[!name!]', '[!username!]', '[!password!]', '[!login_type!]', '[!HTTP_PATH!]', '[!SITE_TITLE!]');
				// $fromRepArray = array($emailId, $name, $name, $password, $login_type, HTTP_PATH, SITE_TITLE);
				// $emailSubject = str_replace($toRepArray, $fromRepArray, $emailTemplate->subject);
				// $emailBody = str_replace($toRepArray, $fromRepArray, $emailTemplate->template);
				// Mail::to($emailId)->send(new SendMailable($emailBody, $emailSubject));
			}
		}
		echo "<script>window.close();window.opener.location.reload();</script>";
		exit;
	}

	public function logout() {
	  
		Session::forget('user_id');
		Session::forget('user_name');
		Session::forget('email_address');
		Session::forget('url.intended'); 
		Session::forget('is_profile_completed'); 
		Session::save();
		Session::flash('success_message', "You have been successfully Logout.");
		
		
		
		return Redirect::to('/');
	}

	public function dashboard() {
		$pageTitle = 'User Dashboard';
		return view('users.dashboard', ['title' => $pageTitle]);
	}

	public function editprofile() {
		$pageTitle = 'Edit Profile';
		return view('users.editprofile', ['title' => $pageTitle]);
	}

	public function uploadprofileimage() {
		echo '<pre>';
		print_r($_FILES);
		exit;
	}
	
	/*My Profile*/
	public function profile($slug = null){
		if( is_null($slug) ){
			$slug = session()->get('slug');
			if( !$slug ){
				return redirect('/login');
			}
		}
		Session::put('all_inputs', Input::all());
		return redirect('/'.$slug);
	}
	

	/****
	 * 
	 * Show User Profile
	 */
	public function showProfile(Request $request)
	{
		$user = User::query()->where(['slug'=>$request->slug])
				 ->with(['country' => function ($country) {
					$country->select('id', 'name');
				}])
				 ->with(['state' => function ($state) {
					$state->select('id', 'name', 'country_id');
				}])
			->first();
		
		if( $user && $user->id == session()->get('user_id') ) {
			$pageTitle = "My Profile";
			$input = Session::get('all_inputs');
			$user = User::where(['id' => Session::get('user_id')])->first();
			$userID = $user->id;
			if (!empty($input)) {
				if ($input['slug'] != session()->get('slug')) {
					$rules = array(
						'name' => 'required|string',
						'email' => 'required|string',
						'slug'  => 'unique:users',
						'about' => 'string',
									   
					);
				}else{
					$rules = array(
						'name' => 'required|string',
						'email' => 'required|string',
						'about' => 'string',                                       
					);
				}
			   
				$messages = array(
						'slug.unique' => 'The username has already been taken.'
					);
				$validator = Validator::make($input, $rules,$messages);
				
				if ($validator->fails()) {

					return back()->withErrors($validator)->withInput();
				} else {
					
					
					
					$user = User::find($userID);
					$user['name'] = $input['name'];
					$user['slug'] = self::make_slug($input['slug']);
					$user['email'] = $input['email'];
					$user['facebook'] = $input['facebook'];
					$user['twitter'] = $input['twitter'];
					$user['instagram'] = $input['instagram'];               
					$user['about'] = $input['about'];
					$user['website'] = $input['website'];
					$user['country_id'] = $input['country'];
					$user['state_id'] = $input['state'];
					$user['language'] = $input['language'];
					

					if (isset($input['email_status'])) {
						$user['email_status'] = 1;
					}else{
						$user['email_status'] = NULL;
					}
					
					if(!empty($input['upload_image'])){
						$image_data = $input['upload_image'];
						$image_array_1 = explode(";", $image_data);
						$image_array_2 = explode(",", $image_array_1[1]);
						$data = base64_decode($image_array_2[1]);
						$image_name = time() . '.png';
						$upload_path = public_path('uploads/users/small/' . $image_name);
						file_put_contents($upload_path, $data);
	
						$user['photo'] = $image_name;
					}
					$response = $user->save();	
					if($input['country'] && $input['state'] && $input['language'] && $response)
						{
							if(Session::get('is_profile_completed') == true)
								Session::put('is_profile_completed', false);
						}
					Session::put('user_id', $user->id);
					Session::put('user_name', ucwords($user->name));
					Session::put('slug', $user->slug);
					Session::put('email', $user->email);
					Session::put('profile_pic', $user->photo);
				}
	
			} 
			$videoData =  Video::where(['user_id' => $user->id, 'isRewardVideo' => 0])
								->orderBy('created_at','desc')
								->select('id','video_key','user_id','title','video_thumb_id','video_time', 'created_at')
								->with(['thumb' => function($thumb){
									$thumb->select('id','thumb');
								}])						
								->paginate(20);
			$followers = DB::table('followers')->where('follows_id', '=', $user->id)->count();
			$following = DB::table('followers')->where('user_id', '=', $user->id)->count();	
			if(isset($input['slug'])){
				return redirect()->route('profileAdd');
			}else{
				return view('users.profile', ['title' => $pageTitle, 'user' => $user,'videoData' => $videoData, 'followers' => $followers,'following' => $following]);
			}
			Session::forget('all_inputs');


		}else{
			if( $user ){
				$pageTitle = "Profile";
				$user = User::where(['id' => $user->id])->first();
				$followers = DB::table('followers')->where('follows_id', '=', $user->id)->count();
				$following = DB::table('followers')->where('user_id', '=', $user->id)->count();
				$videoData =  Video::where(['user_id' => $user->id, 'isRewardVideo' => 0])
									->orderBy('created_at','desc')
									->select('id','video_key','user_id','title','video_thumb_id','video_time', 'created_at')
									->with(['thumb' => function($thumb){
										$thumb->select('id','thumb');
									}])
									->paginate(10);
	
				return view('users.index', ['title' => $pageTitle,'videoData' => $videoData, 'user' => $user, 'followers' => $followers, 'following' => $following, "topMedia" => $this->topMedia($user->id)]);
			}
		}
	}

	public function topMedia($userid){
		$videos = DB::table("videos")
			->selectRaw("'video' as tag, id, video_key as slug, video_thumb_id as image, created_at, title as _slug")
			->where([["user_id", $userid], ['isRewardVideo', 0]])
			->orderByDesc("id")
			->limit(10);

		$photos = DB::table("upload_photos")
			->selectRaw("'photo' as tag, id, id as slug, images as image, created_at,photo_title as _slug")
			->where("user_id", $userid)
			->orderByDesc("id")
			->limit(10);

		$stories = DB::table("stories")
			->selectRaw("'story' as tag, id ,slug, thumb_image as image, created_at, title as _slug")
			->where("user_id", $userid)
			->orderByDesc("id")
			->limit(10);
			
		$rewardVideos = DB::table("videos")
			->selectRaw("'reward_video' as tag, id, video_key as slug, video_thumb_id as image, created_at,title as _slug")
			->where([["user_id", $userid], ['isRewardVideo', 1]])
			->orderByDesc("id")
			->union($videos)
			->union($photos)
			->union($stories)
			->orderByDesc("created_at")
			->limit(10)
			->get();

		return $rewardVideos;
	}

	/*My Uploaded Videos*/
	
	function getStatesByCountry(Request $request)
	{
		if($request->id){
			$getState = DB::table('tbl_states')->where('country_id', $request->id)->get();
			return response()->json($getState);
		}
	}
	
	public function getLanguage()
	{
		$languages = Language::select('name', 'id')->get();
		return response()->json($languages);
	}
	
	public function profileEdit($slug = null){
		$user_id = null;
		if( !$slug ){
			$slug = Session::get('slug');
		}
	   // $countries = DB::table('tbl_countries')->get();
		$countries = Country::select('name', 'id')->get()->prepend(new Country(['name'=>'Select Country']));
		$states = States::select('name', 'id', 'country_id')->get();
	  //  echo "<pre>"; print_r($states); exit;

		$user = User::where('slug', $slug)->first();
		$user_id = $user->id;
		$pageTitle = "My Profile";
		$videoData =  Video::where(['user_id' => $user_id, 'isRewardVideo' => 0])
						->orderBy('created_at','desc')
						->select('id','video_key','title','video_thumb_id','video_time', 'created_at')
						->with(['thumb' => function($thumb){
							$thumb->select('id','thumb');
						}])
						->paginate(20);
		$followers = DB::table('followers')->where('follows_id', '=', $user_id)->count();
		$following = DB::table('followers')->where('user_id', '=', $user_id)->count();	
		//$languages = Language::select('name', 'id')->get()->prepend(new Language(['name'=>'Select Language']));
		$languages = Language::select('name', 'id')->get();

		return view('users.profile', ['title' => $pageTitle, 'videoData' => $videoData, 'user' => $user, 'followers' => $followers,'following' => $following, 'countries'=>$countries, 'states'=>$states, 'languages'=>$languages]);

		return abort(404);
	}
	
	
	/*Upload Video File*/
	public function uploadVideoFile(){
		
	}
	
	public function follow($user_id)
	{
		$follower = User::find(Session::get('user_id'));
		$user = User::find($user_id);
		
		if ($follower->id == $user->id) {
			return array("message" => "You can't follow yourself", "status" => true);
		}

		if(!$follower->isFollowing($user->id)) {
			$follower->follow($user->id);
			
			$message = 'followed you.';
			
			// sending a notification
			$user->notify(new UserFollowed($follower, $message));
			
			return array("message" => "You are now friends with {$user->name}",
						"isFollowing" => true,
						"followCount" => DB::table('followers')->where('follows_id', '=', $user->id)->count()
						);
		}
		return array("message" => "You are already following {$user->name}", "status" => true);
	}

	public function unfollow($user_id)
	{
		$follower = User::find(Session::get('user_id'));
		$user = User::find($user_id);
		if($follower->isFollowing($user->id)) {
			$follower->unfollow($user->id);
			$message = 'unfollowed you.';
			// sending a notification
			$user->notify(new UserFollowed($follower, $message));
			
			return array(
				"message" => "You are no longer friends with {$user->name}",
				"isFollowing" => false,
				"followCount" => DB::table('followers')->where('follows_id', '=', $user->id)->count()
				);
		}
		return array("message" => "You are not following {$user->name}", "status" => true);
	}
	
	public function allnotifications()
	{
		// $user = User::find(Session::get('user_id'));
		// dd($user->notifications()->get());

		// return $user->notifications()->get()->toArray();
	}

	public function usernotifications() {
		$user = User::find(Session::get('user_id'));
		$notices = $user->notifications()->paginate(20);
		$title = 'Notifications';
		return view('users.notifications', compact('notices', 'title'));      
	}
	
	public function index($slug)
	{
		if( !$slug ){
			$slug = Session::get('slug');
		}
		return redirect('/'.$slug);
	}
	


	public function unfollowUser(Request $request)
	{
		$follows_id = $request->id;
		$follower_id = session()->get('user_id');
		DB::table('followers')
					->where('user_id',session()->get('user_id'))
					->where('follows_id',$follows_id)
					->delete();
		return back();
	}

	public function followUser(Request $request)
	{
		$follows_id = $request->id;
		$follower_id = session()->get('user_id');
		$data = array();
		$data['user_id'] = session()->get('user_id');
		$data['follows_id'] = $follows_id;
		$data['created_at'] =  date('Y-m-d H:i:s');
		$data['updated_at'] =  date('Y-m-d H:i:s');
		DB::table('followers')->insert($data);
		return back();
	}

	public function showRewards(Request $request)
	{
		$user = User::where('slug',$request->slug)->first();
		$title = "Rewards - ".$user->name;
		$rewardVideos =  Video::where(['isRewardVideo' => 1, 'user_id' => $user->id])
								->select('id','video_key','title','video_thumb_id','video_time', 'user_id', 'isRewardVideo', 'created_at')
								->orderBy('created_at','desc')
								->with(['thumb' => function($thumb){
									$thumb->select('id','thumb');
								}])
								->with(['user' => function($user){
									$user->select('id','name', 'photo');
								}])
								->withCount(['totalViews'])
								->with('watchTime')
								->paginate(10);
		$followers = DB::table('followers')->where('follows_id', '=', $user->id)->count();
		$following = DB::table('followers')->where('user_id', '=', $user->id)->count();	
		$topMedia = (new UsersController())->topMedia($user->id);
		return view('homes.user_rewards',compact('rewardVideos','title','user','followers','following', 'topMedia'));
	}

	public function showMyFollwers(Request $request)
	{
		$title = "Follow List";
		$user = User::where('slug',$request->slug)->first();

		$myFollowersId = DB::table('followers')
						->where('follows_id',$user->id)
						->pluck('user_id')
						->toArray();
		$countFollowers = count($myFollowersId);
		$myFollowers = User::whereIn('id',$myFollowersId)->paginate(20);
	   return view('homes.my_followers',compact('myFollowers','title','user','countFollowers'));
	}

	public function showMyFollowing(Request $request)
	{
		$title = "Following List";
		$user = User::where('slug',$request->slug)->first();

		$myFollowingId = DB::table('followers')
						->where('user_id',$user->id)
						->pluck('follows_id')
						->toArray();
		$countFollowing = count($myFollowingId);
		$myFollowing = User::whereIn('id',$myFollowingId)->paginate(20);
	   return view('homes.my_following',compact('myFollowing','title','user','countFollowing'));
	}

	public function markNotificationsAsRead(){
		User::find(Session::get('user_id'))->notifications()->whereNull('read_at')->update(['read_at' => Carbon::now()]);
	}
	
	public function laterProfile(Request $request)
	{
	   // $date = date('Y-m-d H:i:s');
		$date = Carbon::now();
		
		$status = false;
		if($request->id){
			if(User::where('id', $request->id)->update(['later_date' => $date, 'is_completed'=> 1])){
				if(Session::get('is_profile_completed')==true)
					Session::put('is_profile_completed', false);
				$status = true;
				$message = "successfully update";
			}else{
				$message = "Fail to update";
			}
		}else{
			$message = "User Not Found";
		}
		return response()->json(['status'=>$status, 'message'=>$message]);
	}
	
	
}

?>