<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;

use App\Models\Restaurant;
use App\Models\Country;
use Cookie;
use Session;
use Redirect;
use Validator;
use DB;
use App\Mail\SendMailable;
use Input;
use App\Models\Category;
use App\Models\Video;

class ChannelsController extends Controller {
    /*Channels Listing*/
	public function index() {
        $pageTitle = 'Channels';
        return view(
				'channels.index', ['title' => $pageTitle]
			);
    }
	
	/*Create Channel*/
	public function create(){
		$pageTitle = 'Create Channel';
		return view('channels.create', [
						'title' => $pageTitle
					]
                );
	}
	
	/*Channel details*/
	public function detail(){
		
		
	}
	

}
