<?php

namespace App\Http\Controllers;

use App\Helpers\TransactionHelper;
use Illuminate\Http\Request;
use Razorpay\Api\Api;

class RazorPayController extends Controller
{
	const BASE_URL = "https://api.razopay.com";
	const PAYOUT_ENDPOINT = "/v1/payouts";
	const CONTACT_ENDPOINT = "/v1/contacts";
	const FUND_ACCOUNT_ENDPOINT = "/v1/fund_accounts";

	private $key;
	private $secret;
	private $api;
	private $attributes;

	public function __construct($attributes = [], $loadPaymentApi = true) {
		$this->key = env('RAZOR_KEY');
		$this->secret = env('RAZOR_SECRET');
		$this->attributes = $attributes;

		if ($loadPaymentApi) {
			$this->loadPaymentApi();
		}
	}

	public function loadPaymentApi() {
		$this->api = new Api($this->key, $this->secret);
	}

	public function createOrder($receipt, $amount, $notes = []){
		$this->api->order->create(array(
			'receipt'   => $receipt,
			'amount'    => $amount,
			'currency'  => 'INR',
			'notes'     => $notes
		));
	}

	private function build($endpoint, $data, $request = null) {
		$headers = array(
			"Content-Type: application/json",
			"Authorization: Bearer $this->key:$this->secret"
		);

		$data = !$data ? [] : $data;
		$curl = curl_init(self::BASE_URL.$endpoint);
		if (!$request) {
			curl_setopt($curl, CURLOPT_POST, true);
		} elseif ($request == 'update') {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
		} elseif ($request == 'delete') {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
		}
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($curl);

		return $response === false ? null : json_decode($response);
	}

	public function createPayoutRequest($data) {
		// $attributes = [
		// 	'account_number' => $this->attributes['account_number'],
		// 	'fund_account_id' => $this->attributes['fund_account_id'],
		// 	'amount' => $this->attributes['amount'],
		// 	'currency' => 'INR',
		// 	'mode' => 'IMPS',
		// 	'purpose' => 'Earning withdrawl',
		// 	'queue_if_low_balance' => true,
		// 	'reference_id' => '',
		// 	'narration' => 'Earning withdrawl',
		// 	'notes' => isset($this->attributes['notes']) ? $this->attributes['notes'] : [],
		// ];

		return $this->build(self::PAYOUT_ENDPOINT, $data);
	}

	public function getPayouts($data) {
		return $this->build(self::PAYOUT_ENDPOINT, $data, "fetch");
	}

	public function getPayout($id, $data = []) {
		return $this->build(self::PAYOUT_ENDPOINT."/$id", $data, "fetch");
	}

	public function cancelPayout($id, $data = []) {
		return $this->build(self::PAYOUT_ENDPOINT."/$id/cancel", $data);
	}

	public function createContact($data) {
		return $this->build(self::CONTACT_ENDPOINT, $data);
	}

	public function updateContact($contact_id, $data) {
		return $this->build(self::CONTACT_ENDPOINT."/$contact_id", $data, "update");
	}

	public function getContact($id, $data = []) {
		return $this->build(self::CONTACT_ENDPOINT."/$id", $data, "fetch");
	}

	public function getContacts($data = []) {
		return $this->build(self::CONTACT_ENDPOINT, $data, "fetch");
	}

	public function createFundAccount($data) {
		return $this->build(self::FUND_ACCOUNT_ENDPOINT, $data);
	}

	public function updateFundAccount($id, $data) {
		return $this->build(self::FUND_ACCOUNT_ENDPOINT."/$id", $data, "update");
	}

	public function getFundAccounts($data = []) {
		return $this->build(self::FUND_ACCOUNT_ENDPOINT, $data, "fetch");
	}

	public function getFundAccount($id, $data = []) {
		return $this->build(self::FUND_ACCOUNT_ENDPOINT."/$id", $data, "fetch");
	}

	public function capturePayment(){
		if( !isset($this->attributes['razorpay_payment_id']) || !$this->attributes['razorpay_payment_id'] ){
			return ['error' => "Payment Id must be specified."];
		}

		// if( !isset($this->attributes['razorpay_order_id']) || !$this->attributes['razorpay_order_id'] ){
		//     return ['error' => "Order Id must be specified"];
		// }

		// if ( !isset($this->attributes['razorpay_signature']) || !$this->attributes['razorpay_signature'] ){
		//     return ['error' => "Signature must be specified."];
		// }

		// $this->api->utility->verifyPaymentSignature($this->attributes);

		$payment = $this->api->payment->fetch($this->attributes['razorpay_payment_id']);
		if( !$payment['id'] ){
			return ['error' => "Invalid payment Id"];
		}

		if( $payment['status'] == 'captured' ){
			return ['error' => "Payment is already captured"];
		}
		
		$payment = $payment->capture(['amount' => $payment['amount'], 'currency' => 'INR']);
		return $payment;
	}
}
