<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use Cookie;
use Session;
use Redirect;
use Validator;
use DB;
use App\Mail\SendMailable;
use Input;
use App\Models\Category;
use App\Models\Video;
use App\File;
use VideoThumbnail;
use App\Models\Story;
use App\Models\User;
use App\Models\StoryLikeDislike;
use App\Models\StoryViews;
use Carbon\Carbon;

class UploadController extends Controller
{
	public static function make_slug($slug)
	{
		return strtolower(preg_replace('/\s+/u', '-', trim($slug)));
	}
	/*Upload Video*/
	public function uploadVideo(Request $request)
	{
		$file = $request->file('video');
		$userId = session()->get('user_id');
		$sh1 = strtoupper(sha1(time() . $userId));
		$newFile =  trim($file->getClientOriginalName());
		$destinationPath = TEMP_UPLOAD_PATH;
		$thumbUploadPath = VIDEO_THUMB_UPLOAD_PATH;
		$displayPath = VIDEO_THUMB_DISPLAY_PATH;
		$data = array();
		$data['status'] = false;

		if ($request->store && preg_match("/^\{(.+)\}$/", $request->store)) {
			$decode = json_decode($request->store, true);

			if (JSON_ERROR_NONE != json_last_error()) {
				return response()->json(['error' => true, 'title' => 'Invalid store variable'], 400);
			}

			if (count($decode) > 0) {
				if (preg_match("/^\//", $decode['path'])) {
					$decode['path'] = substr($decode['path'], 1);
				}

				if (preg_match("/\/$/", $decode['path'])) {
					$decode['path'] = substr($decode['path'], 0, -1);
				}
				
				if( !(empty($decode['path'])) ){
					$destinationPath = BASE_PATH . '/' . $decode['path'] . '/';
					$thumbUploadPath = $destinationPath."/thumb/";
					$displayPath = HTTP_PATH . '/' . $decode['path'] . '/thumb/';
				}

			}
		}

		if ($file->move($destinationPath, $newFile)) {
		   
			$fileName = str_replace('mp4', 'png', $newFile);
			if( $request->thumb ){
				$splittedThumb = explode(";", $request->thumb);
				$splittedThumb = explode(",", $splittedThumb[1]);
				$thumb = base64_decode($splittedThumb[1]);
				$filePath = $thumbUploadPath . $fileName;
				file_put_contents($filePath, $thumb);
				 DB::table('temp_files')->updateOrInsert(['user_id'=>$userId],['user_id'=>$userId,'video_path'=>'/uploads/temp/'.$newFile,'thumb_path'=>'/uploads/video/thumbs/'.$fileName]);
				VideoThumbnail::resizeCropImage(640, 320, $filePath, $filePath);
			} else {
				VideoThumbnail::createThumbnail($destinationPath . $newFile, $thumbUploadPath, $fileName, 2, $width = 640, $height = 320);
			}

			$data['file'] = $newFile;
			$data['thumbnail'] = $displayPath . $fileName;
			$data['status'] = true;
		}

		echo json_encode($data);
		die;
	}
    public function deletetemp($id){
        $tempvideo = DB::table('temp_files')->where(['id'=>$id])->latest()->first();
        if($tempvideo){
            \File::delete(public_path(). '/'.$tempvideo->video_path);
        \File::delete(public_path() . '/'.$tempvideo->thumb_path);
        DB::table('temp_files')->where('id',$tempvideo->id)->delete();
        }
         
        return redirect()->back()->with('success','Video Deleted Sucessfully');
    }
	function tempBase64Image(Request $request)
	{
		$image_data = $request->image;
		
		if( is_string($image_data) ){
			$image_array_1 = explode(";", $image_data);
			$image_array_2 = explode(",", $image_array_1[1]);
			$imagedata = base64_decode($image_array_2[1]);
		} else {
			$file = $request->file('image');
		}

		$userId = session()->get('user_id');
		$sh1 = strtoupper(sha1(time() . $userId));

		$image_name = md5(Session::get('user_id') . time()) . '.png';

		$destinationPath = TEMP_UPLOAD_PATH;
		$displayPath = TEMP_DISPLAY_PATH;

		if ($request->store && preg_match("/^\{(.+)\}$/", $request->store)) {
			$decode = json_decode($request->store, true);

			if (JSON_ERROR_NONE != json_last_error()) {
				return response()->json(['error' => true, 'title' => 'Invalid store variable'], 400);
			}

			if (count($decode) > 0) {
				if (preg_match("/^\//", $decode['path'])) {
					$decode['path'] = substr($decode['path'], 1);
				}

				if (preg_match("/\/$/", $decode['path'])) {
					$decode['path'] = substr($decode['path'], 0, -1);
				}
				
				if( !(empty($decode['path'])) ){
					$destinationPath = BASE_PATH . '/' . $decode['path'] . '/';
					$displayPath = HTTP_PATH . '/' . $decode['path'] . '/';
				}

			}
		}
		
		if( isset($file) ){
			$file->move($destinationPath, $image_name);
		} else {
			$destinationPath = $destinationPath.$image_name;
			file_put_contents($destinationPath, $imagedata);
		}
		
		$data = array();
		$displayPath = $displayPath . $image_name;
		$data['file'] = $image_name;
		$data['status'] = true;
		$data['image'] = $displayPath;
		echo json_encode($data);
		die;
	}

	/*Upload Image*/
	public function tempImage(Request $request)
	{
		$file = $request->file('image');
		$userId = session()->get('user_id');
		$sh1 = strtoupper(sha1(time() . $userId));
		$newFile = $sh1 . '.' . $file->getClientOriginalExtension();
		$destinationPath = TEMP_UPLOAD_PATH;
		$data = array();
		$data['status'] = false;
		if ($file->move($destinationPath, $newFile)) {
			$data['file'] = $newFile;
			$data['status'] = true;
			$data['image'] = TEMP_DISPLAY_PATH . $newFile;
		}
		echo json_encode($data);
		die;
	}

	// Sort Query for Category Video
	public function categoryVideoSortByRecent(Request $request)
	{
		$title = $pageTitle = "Category";

		$categories = Category::getCategoryList();
		$category_name = $request->keyword;

		$string = str_replace('&', '-', $category_name);
		if ($string === 'Cars -amp; Vehicles') {
			$category_name = 'Cars & Vehicles';
		}

		$searchdata = $request->searchData;
		if ($searchdata) {
			$videoData = Video::where('title', 'like', '%' . $searchdata . '%')
				->orWhere('description', 'like', '%' . $searchdata . '%')
				->orWhereHas('video_category', function ($query) use ($searchdata) {
					$query->whereHas('category', function ($query) use ($searchdata) {
						$query->where('category', 'like', '%' . $searchdata . '%');
					}
					);
				})
				->orWhereHas('video_tag', function ($query) use ($searchdata) {
					$query->whereHas('tag', function ($query) use ($searchdata) {
						$query->where('tag', 'like', '%' . $searchdata . '%');
					}
					);
				})
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('id', 'desc')
				->where('isRewardVideo', 0)
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo');
					}
				])
				->withCount(['isWatchedVideo'])
				->withCount(['totalViews'])
				->paginate(28);

			return view('elements.home.search_card_one', compact('title', 'categories', 'videoData'));

		} else {

			$videoData = Video::whereHas('video_category', function ($query) use ($category_name) {
				$query->whereHas('category', function ($query) use ($category_name) {
					$query->where('category', 'like', '%' . $category_name . '%');
				}
				);
			})
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('id', 'desc')
				->where('isRewardVideo', 0)
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo');
					}
				])
				->withCount(['isWatchedVideo'])
				->withCount(['totalViews'])
				->paginate(28);

			return view('elements.post.categoryvideo', compact('title', 'categories', 'videoData'));
		}


	}

	public function categoryVideoSortByViewed(Request $request)
	{
		$title = $pageTitle = "Category";

		$categories = Category::getCategoryList();
		$category_name = $request->keyword;
		$searchdata = $request->searchData;

		$string = str_replace('&', '-', $category_name);
		if ($string === 'Cars -amp; Vehicles') {
			$category_name = 'Cars & Vehicles';
		}

		if ($searchdata) {
			$videoData = Video::where('title', 'like', '%' . $searchdata . '%')
				->orWhere('description', 'like', '%' . $searchdata . '%')
				->orWhereHas('video_category', function ($query) use ($searchdata) {
					$query->whereHas('category', function ($query) use ($searchdata) {
						$query->where('category', 'like', '%' . $searchdata . '%');
					}
					);
				})
				->orWhereHas('video_tag', function ($query) use ($searchdata) {
					$query->whereHas('tag', function ($query) use ($searchdata) {
						$query->where('tag', 'like', '%' . $searchdata . '%');
					}
					);
				})
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('total_views_count', 'desc')
				->where('isRewardVideo', 0)
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo');
					}
				])
				->withCount(['isWatchedVideo'])
				->withCount(['totalViews'])
				->paginate(28);

			return view('elements.home.search_card_one', compact('title', 'categories', 'videoData'));

		} else {
			$videoData = Video::whereHas('video_category', function ($query) use ($category_name) {
				$query->whereHas('category', function ($query) use ($category_name) {
					$query->where('category', 'like', '%' . $category_name . '%');
				}
				);
			})
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('total_views_count', 'desc')
				->where('isRewardVideo', 0)
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo');
					}
				])
				->withCount(['isWatchedVideo'])
				->withCount(['totalViews'])
				->paginate(28);

			return view('elements.post.categoryvideo', compact('title', 'categories', 'videoData'));
		}
	}


	public function categoryVideoSortByLiked(Request $request)
	{
		$title = $pageTitle = "Category";

		$categories = Category::getCategoryList();
		$category_name = $request->keyword;
		$searchdata = $request->searchData;

		$string = str_replace('&', '-', $category_name);
		if ($string === 'Cars -amp; Vehicles') {
			$category_name = 'Cars & Vehicles';
		}

		if ($searchdata) {
			$videoData = Video::where('title', 'like', '%' . $searchdata . '%')
				->orWhere('description', 'like', '%' . $searchdata . '%')
				->orWhereHas('video_category', function ($query) use ($searchdata) {
					$query->whereHas('category', function ($query) use ($searchdata) {
						$query->where('category', 'like', '%' . $searchdata . '%');
					}
					);
				})
				->orWhereHas('video_tag', function ($query) use ($searchdata) {
					$query->whereHas('tag', function ($query) use ($searchdata) {
						$query->where('tag', 'like', '%' . $searchdata . '%');
					}
					);
				})
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('video_likes_count', 'desc')
				->where('isRewardVideo', 0)
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo');
					}
				])
				->withCount(['isWatchedVideo'])
				->withCount(['video_likes'])
				->paginate(28);

			return view('elements.home.search_card_one', compact('title', 'categories', 'videoData'));
		} else {
			$videoData = Video::whereHas('video_category', function ($query) use ($category_name) {
				$query->whereHas('category', function ($query) use ($category_name) {
					$query->where('category', 'like', '%' . $category_name . '%');
				}
				);
			})
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('video_likes_count', 'desc')
				->where('isRewardVideo', 0)
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo');
					}
				])
				->withCount(['isWatchedVideo'])
				->withCount(['video_likes'])
				->paginate(28);
			return view('elements.post.categoryvideo', compact('title', 'categories', 'videoData'));
		}

	}

	public function categoryVideoSortByTrending(Request $request)
	{

		$title = $pageTitle = "Category";

		$categories = Category::getCategoryList();
		$category_name = $request->keyword;

		$string = str_replace('&', '-', $category_name);
		if ($string === 'Cars -amp; Vehicles') {
			$category_name = 'Cars & Vehicles';
		}

		$searchdata = $request->searchData;
		if ($searchdata) {
			$videoData = Video::where('title', 'like', '%' . $searchdata . '%')
				->orWhere('description', 'like', '%' . $searchdata . '%')
				->orWhereHas('video_category', function ($query) use ($searchdata) {
					$query->whereHas('category', function ($query) use ($searchdata) {
						$query->where('category', 'like', '%' . $searchdata . '%');
					}
					);
				})
				->orWhereHas('video_tag', function ($query) use ($searchdata) {
					$query->whereHas('tag', function ($query) use ($searchdata) {
						$query->where('tag', 'like', '%' . $searchdata . '%');
					}
					);
				})
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('total_views_count', 'desc')
				->where('isRewardVideo', 0)
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo');
					}
				])
				->withCount(['isWatchedVideo'])
				->withCount(['totalViews'])
				->where("created_at", ">", Carbon::now()->subMonths(1))
				->paginate(28);

			return view('elements.home.search_card_one', compact('title', 'categories', 'videoData'));

		} else {
			$videoData = Video::whereHas('video_category', function ($query) use ($category_name) {
				$query->whereHas('category', function ($query) use ($category_name) {
					$query->where('category', 'like', '%' . $category_name . '%');
				}
				);
			})
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('total_views_count', 'desc')
				->where('isRewardVideo', 0)
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo');
					}
				])
				->withCount(['isWatchedVideo'])
				->withCount(['totalViews'])
				->where("created_at", ">", Carbon::now()->subMonths(1))
				->paginate(28);

			return view('elements.post.categoryvideo', compact('title', 'categories', 'videoData'));
		}


	}

	public function categoryVideoSortByLongest(Request $request)
	{
		$title = $pageTitle = "Category";

		$categories = Category::getCategoryList();
		$category_name = $request->keyword;
		$searchdata = $request->searchData;

		$string = str_replace('&', '-', $category_name);
		if ($string === 'Cars -amp; Vehicles') {
			$category_name = 'Cars & Vehicles';
		}

		if ($searchdata) {
			$videoData = Video::where('title', 'like', '%' . $searchdata . '%')
				->orWhere('description', 'like', '%' . $searchdata . '%')
				->orWhereHas('video_category', function ($query) use ($searchdata) {
					$query->whereHas('category', function ($query) use ($searchdata) {
						$query->where('category', 'like', '%' . $searchdata . '%');
					}
					);
				})
				->orWhereHas('video_tag', function ($query) use ($searchdata) {
					$query->whereHas('tag', function ($query) use ($searchdata) {
						$query->where('tag', 'like', '%' . $searchdata . '%');
					}
					);
				})
				->orderBy('video_time', 'desc')
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->where('isRewardVideo', 0)
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo');
					}
				])
				->withCount(['isWatchedVideo'])
				->withCount(['video_likes'])
				->paginate(28);

			return view('elements.home.search_card_one', compact('title', 'categories', 'videoData'));

		} else {
			$videoData = Video::whereHas('video_category', function ($query) use ($category_name) {
				$query->whereHas('category', function ($query) use ($category_name) {
					$query->where('category', 'like', '%' . $category_name . '%');
				}
				);
			})
				->orderBy('video_time', 'desc')
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->where('isRewardVideo', 0)
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo');
					}
				])
				->withCount(['isWatchedVideo'])
				->withCount(['video_likes'])
				->paginate(28);
			return view('elements.post.categoryvideo', compact('title', 'categories', 'videoData'));
		}

	}



	////////////////////////////////////////////////////////////

	/************* Start Story Sort Query **************************/

	public function storyVideoSortByRecent(Request $request)
	{
		$keyword = substr($request->keyword, 0, 6);
		$searchdata = $request->searchData;

		$categories = Category::getCategoryList();

		if ($searchdata) {
			$stories = Story::where('title', 'like', '%' . $searchdata . '%')
				->orWhere('tags', 'like', '%' . $searchdata . '%')
				->orWhere('language', 'like', '%' . $searchdata . '%')
				->orWhere('about', 'like', '%' . $searchdata . '%')
				->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at')
				->orderBy('created_at', 'desc')
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo');
					}
				])
				->withCount(['totalViews'])
				->withCount(['totalComments'])
					//->withCount(['video_likes'])
					//->withCount(['video_dislikes'])
				->paginate(10);

		} else {

			$catID = Category::select('id')->where('category', 'like', '%' . $keyword . '%')->get();
			$stories = Story::where('category', 'like', '%' . $catID[0]->id . '%')
				->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at')
				->orderBy('created_at', 'desc')
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
					}
				])
				->withCount(['totalViews'])
				->withCount(['totalComments'])
				->paginate(12);


		}
		return view('elements.story.cards', compact('categories', 'stories'));
	}

	public function storyVideoSortByViewed(Request $request)
	{
		$categories = Category::getCategoryList();
		$keyword = substr($request->keyword, 0, 6);
		$searchdata = $request->searchData;
		if ($searchdata) {
			$stories = Story::where('title', 'like', '%' . $searchdata . '%')
				->orWhere('tags', 'like', '%' . $searchdata . '%')
				->orWhere('language', 'like', '%' . $searchdata . '%')
				->orWhere('about', 'like', '%' . $searchdata . '%')
				->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at')
				->orderBy('total_views_count', 'desc')
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
					}
				])
				->withCount(['totalViews'])
				->withCount(['totalComments'])
				->paginate(12);

		} else {
			$catID = Category::select('id')->where('category', 'like', '%' . $keyword . '%')->get();
			$stories = Story::where('category', 'like', '%' . $catID[0]->id . '%')
				->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at')
				->orderBy('total_views_count', 'desc')
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
					}
				])
				->withCount(['totalViews'])
				->withCount(['totalComments'])
				->paginate(12);
		}
		return view('elements.story.cards', compact('categories', 'stories'));
	}


	public function storyVideoSortByLiked(Request $request)
	{
		$keyword = substr($request->keyword, 0, 6);
		$searchdata = $request->searchData;
		$categories = Category::getCategoryList();
		if ($searchdata) {
			$stories = Story::where('title', 'like', '%' . $searchdata . '%')
				->orWhere('tags', 'like', '%' . $searchdata . '%')
				->orWhere('language', 'like', '%' . $searchdata . '%')
				->orWhere('about', 'like', '%' . $searchdata . '%')
				->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at')
				->orderBy('story_likes_count', 'desc')
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
					}
				])
				->withCount(['story_likes'])
				->withCount(['totalViews'])
				->withCount(['totalComments'])
				->paginate(12);
		} else {
			$catID = Category::select('id')->where('category', 'like', '%' . $keyword . '%')->get();
			$stories = Story::where('category', 'like', '%' . $catID[0]->id . '%')
				->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at')
				->orderBy('story_likes_count', 'desc')
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
					}
				])
				->withCount(['story_likes'])
				->withCount(['totalViews'])
				->withCount(['totalComments'])
				->paginate(12);
		}
		return view('elements.story.cards', compact('categories', 'stories'));

	}

	public function storyVideoSortByTrending(Request $request)
	{
		$keyword = substr($request->keyword, 0, 6);
		$categories = Category::getCategoryList();
		$searchdata = $request->searchData;
		if ($searchdata) {
			$stories = Story::where('title', 'like', '%' . $searchdata . '%')
				->orWhere('tags', 'like', '%' . $searchdata . '%')
				->orWhere('language', 'like', '%' . $searchdata . '%')
				->orWhere('about', 'like', '%' . $searchdata . '%')
				->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at')
				->orderBy('story_likes_count', 'desc')
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
					}
				])
				->withCount(['story_likes'])
				->withCount(['totalViews'])
				->withCount(['totalComments'])
				->orderBy('total_views_count', 'desc')
				->where("created_at", ">", Carbon::now()->subMonths(1))
				->paginate(12);
		} else {
			$catID = Category::select('id')->where('category', 'like', '%' . $keyword . '%')->get();
			$stories = Story::where('category', 'like', '%' . $catID[0]->id . '%')
				->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at')
				->orderBy('story_likes_count', 'desc')
				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
					}
				])
				->withCount(['story_likes'])
				->withCount(['totalViews'])
				->withCount(['totalComments'])
				->orderBy('total_views_count', 'desc')
				->where("created_at", ">", Carbon::now()->subMonths(1))
				->paginate(12);
		}

		return view('elements.story.cards', compact('categories', 'stories'));

	}

	public function storyVideoSortByTopRated(Request $request)
	{
		return $request->keyword;

	}




	/**************** End Story Sort Query ***********************/

	////////////////////////////////////////////////////////////
	public function allRewardOrderBy(Request $request)
	{
        $days = Carbon::now()->subDays(Carbon::now()->dayOfWeek)->startOfDay()->addDay();
		if ($request->keyword == 'recent') {
			$pageTitle = "Rewards";
			$rewardVideos = Video::where(['isRewardVideo' => 1,'isDeleted'=>0],['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")])
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('id', 'desc')
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])


				->with([
					'totalViews' => function ($query) {
						$query->select('video_id')
							->groupBy('video_id', 'video_id')
							->orderBY(DB::raw('count(video_id)'), 'DESC');

					}
				])

				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo')->where('isDeleted',0);
					}
				])

		
				->withCount(['totalViews'])
				->with('watchTime')
				->withCount(['video_likes'])
				->withCount(['video_dislikes'])
				->paginate(10);

			return view('elements.home_sorting_videos.all_reward_video', compact('rewardVideos'));
		}
		if ($request->keyword == 'top_viewed') {
			$pageTitle = "Rewards";
			$rewardVideos = Video::where(['isRewardVideo' => 1],['isDeleted'=>0],['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")])
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('total_views_count', 'desc')
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])


				->with([
					'totalViews' => function ($query) {
						$query->select('video_id')
							->groupBy('video_id', 'video_id')
							->orderBY(DB::raw('count(video_id)'), 'DESC');

					}
				])

				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo')->where('isDeleted',0);
					}
				])

				->withCount(['totalViews'])
				->with('watchTime')
				->withCount(['video_likes'])
				->withCount(['video_dislikes'])
				->paginate(10);

			return view('elements.home_sorting_videos.all_reward_video', compact('rewardVideos'));
		}


		if ($request->keyword == 'top_liked') {
			$pageTitle = "Rewards";
			$rewardVideos = Video::where(['isRewardVideo' => 1,'isDeleted'=>0],['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")])
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('video_likes_count', 'desc')
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])


				->with([
					'totalViews' => function ($query) {
						$query->select('video_id')
							->groupBy('video_id', 'video_id')
							->orderBY(DB::raw('count(video_id)'), 'DESC');

					}
				])

				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo')->where('isDeleted',0);
					}
				])





				->withCount(['totalViews'])
				->with('watchTime')
				->withCount(['video_likes'])
				->withCount(['video_dislikes'])
				->paginate(10);

			return view('elements.home_sorting_videos.all_reward_video', compact('rewardVideos'));
		}


		if ($request->keyword == 'longest') {
			$pageTitle = "Rewards";
			$rewardVideos = Video::where(['isRewardVideo' => 1,'isDeleted'=>0],['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")])
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('video_time', 'desc')
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])


				->with([
					'totalViews' => function ($query) {
						$query->select('video_id')
							->groupBy('video_id', 'video_id')
							->orderBY(DB::raw('count(video_id)'), 'DESC');

					}
				])

				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo')->where(['isDeleted'=>0]);
					}
				])

				->withCount(['totalViews'])
				->with('watchTime')
				->withCount(['video_likes'])
				->withCount(['video_dislikes'])
				->paginate(10);

			return view('elements.home_sorting_videos.all_reward_video', compact('rewardVideos'));
		}


		if ($request->keyword == 'recentdata') {
			$title = "Reward Most Watched";

			$rewardMostWatchedVideos = Video::where([['isRewardVideo', '=', 1], ['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where('videos.isDeleted',0)
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at','isDeleted')
				->orderBy('id', 'desc')
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])

				->with([
					'totalViews' => function ($query) {
						$query->select('video_id')
							->groupBy('video_id', 'video_id')
							->orderBY(DB::raw('count(video_id)'), 'DESC');

					}
				])

				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo','isDeleted')->where(['isDeleted'=>0]);
					}
				])

				->withCount(['totalViews'])
				->with('watchTime')
				->withCount(['video_likes'])
				->withCount(['video_dislikes'])
				->paginate(10);
			
			return view('elements.reward.reward', compact('rewardMostWatchedVideos', 'title'));
			// return view('elements.reward.reward',compact('rewardMostWatched'));
		}



		if ($request->keyword == 'topviwed') {
			$title = "Reward Most Watched";

			$rewardMostWatchedVideos = Video::where([['isRewardVideo', '=', 1], ['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where('videos.isDeleted',0)
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderByRaw('total_views_count', 'desc')
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])

			

				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo')->where('isDeleted',0);
					}
				])

				->withCount(['totalViews'])
				->with('watchTime')
				->withCount(['video_likes'])
				->withCount(['video_dislikes'])
				->paginate(10);
			return view('elements.reward.reward', compact('rewardMostWatchedVideos', 'title'));
			// return view('elements.reward.reward',compact('rewardMostWatched'));
		}


		if ($request->keyword == 'topliked') {
			$title = "Reward Most Watched";

			$rewardMostWatchedVideos = Video::where([['isRewardVideo', '=', 1], ['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where('videos.isDeleted',0)
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
					->orderByRaw('video_likes_count', 'desc')
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])

				->with([
					'totalViews' => function ($query) {
						$query->select('video_id')
							->groupBy('video_id', 'video_id')
							->orderBY(DB::raw('count(video_id)'), 'DESC');

					}
				])

				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo')->where('users.isDeleted',0);
					}
				])

				->withCount(['totalViews'])
				->with('watchTime')
				->withCount(['video_likes'])
				->withCount(['video_dislikes'])

				->paginate(10);
			
			return view('elements.reward.reward', compact('rewardMostWatchedVideos', 'title'));
			// return view('elements.reward.reward',compact('rewardMostWatched'));
		}


		if ($request->keyword == 'toptranding') {
			$title = "Reward Most Watched";

			$rewardMostWatchedVideos = Video::where([['isRewardVideo', '=', 1], ['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where('videos.isDeleted',0)
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('total_views_count', 'desc')
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])


				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo')->where('isDeleted',0);
					}
				])

			
				->withCount(['totalViews'])
				->with('watchTime')
				->withCount(['video_likes'])
				->withCount(['video_dislikes'])
				->paginate(10);
			return view('elements.reward.reward', compact('rewardMostWatchedVideos', 'title'));
			// return view('elements.reward.reward',compact('rewardMostWatched'));
		}




		if ($request->keyword == 'toplongest') {
			$title = "Reward Most Watched";

			$rewardMostWatchedVideos =  Video::where([['isRewardVideo', '=', 1], ['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where('videos.isDeleted',0)
				->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
				->orderBy('video_time', 'desc')
				->with([
					'thumb' => function ($thumb) {
						$thumb->select('id', 'thumb');
					}
				])

				->with([
					'totalViews' => function ($query) {
						$query->select('video_id')
							->groupBy('video_id', 'video_id')
							->orderBY(DB::raw('count(video_id)'), 'DESC');

					}
				])

				->with([
					'user' => function ($user) {
						$user->select('id', 'name', 'slug', 'photo')->where('isDeleted',0);
					}
				])

				
				->withCount(['totalViews'])
				->with('watchTime')
				->withCount(['video_likes'])
				->withCount(['video_dislikes'])
				->paginate(10);
			return view('elements.reward.reward', compact('rewardMostWatchedVideos', 'title'));
			// return view('elements.reward.reward',compact('rewardMostWatched'));
		}
	}



}