<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegisterMail;
use App\Jobs\SendEmailJob;

class MailController extends Controller
{
    public function basic_email() {
        $data = array('name'=>"Virat Gandhi");
        Mail::send(['text' => 'mail'], $data, function($message) {
            $message
                ->to('abc@gmail.com', 'Tutorials Point')
                ->subject('Laravel Basic Testing Mail');
            $message
                ->from('xyz@gmail.com','Virat Gandhi');
        });
    }

    public function html_email() {
        $data = array('name'=>"Virat Gandhi");
        Mail::send('mail', $data, function($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject
                ('Laravel HTML Testing Mail');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });
        echo "HTML Email Sent. Check your inbox.";
    }

    public function attachment_email() {
        $data = array('name'=>"Virat Gandhi");
        Mail::send('mail', $data, function($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject
                ('Laravel Testing Mail with Attachment');
            $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
            $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });
        echo "Email Sent with attachment. Check your inbox.";
    }
    
    public static function send($type, $mailTo, $options = []){
        switch($type){
            case 'register':
                dispatch(new SendEmailJob($type, new RegisterMail($options), $mailTo));
                break;
                
            default:
                if(!$options['template']) return "TEMPLATE NOT FOUND";
                Mail::to($mailTo)->send(new  \App\Mail\CustomMail($options['template'], $options));
                // dispatch(new SendEmailJob($type, new \App\Mail\CustomMail($options['template'], $options), $mailTo));
        }
        
        return true;
    }
}