<?php

namespace App\Http\Controllers;

use App\Helpers\UserHelper;
use App\Helpers\VideoHelper;
use App\Models\User;
use App\Models\Video;
use App\Models\VideoCategory;
use App\Models\VideoFlag;
use App\Models\VideoLikes;
use App\Models\VideoTag;
use App\Models\VideoViews;
use App\Models\WatchHistory;
use App\Models\WatchLater;
use App\WatchTrack;
use Cache;
use Carbon\Carbon;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Input;
use Redirect;
use Session;
use Validator;
use VideoThumbnail;

class VideoController extends Controller
{
    public function listReward($week = null)
    {
        $auth_user = UserHelper::getAuthUser();

        if ($week !== null) {
            if ($week === 'second') {
                [$toSunday, $fromSunday] = get_second_last_week();
            } else {
                return abort(404);
            }
        } else {
            [$toSunday, $fromSunday] = get_last_week();
        }

        $reward_videos = Video::select([
            'videos.id',
            'videos.video_key',
            'videos.title',
            'videos.video_thumb_id',
            'videos.video_time',
            'videos.user_id',
            'videos.created_at',
        ])
            ->leftJoin('watch_track', 'videos.id', '=', 'watch_track.video_id')
            ->leftJoin('watch_histories', 'videos.id', '=', 'watch_histories.video_id')
            ->where([
                ['videos.isRewardVideo', 1],
                ['videos.isDeleted', 0],
                ['videos.created_at', '<=', $toSunday->format("Y-m-d 23:59:59")],
                ['videos.created_at', '>=', $fromSunday->format("Y-m-d 00:00:00")],
            ])
            ->groupBy('videos.id')
            ->orderBy('total_track_sum', 'desc')
            ->selectRaw('COUNT(DISTINCT watch_histories.user_id) AS total_views_count')
            ->selectRaw('(SELECT SUM(wt.track) FROM watch_track wt WHERE wt.video_id = videos.id) AS total_track_sum')
            ->paginate(30);

        foreach ($reward_videos as $idx => $video) {
            $video->rank = ($idx + 1);
        }

        $result = VideoHelper::prices();
        foreach ($reward_videos as $video) {
            $video->total_current_earning = 'Rs ' . ($result[$video->rank] ?? 0) . '/-';
        }

        return view('homes.all_top_entertainer', [
            'title' => 'All Top Entertainers',
            'videos' => $reward_videos,
            'selected' => $week
        ]);
    }

    public function showReward($slug)
    {
        $video = Video::where([
            'video_key' => $slug,
            'videos.status' => 1,
            'isRewardVideo' => 1,
        ])
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                },
                'user' => function ($thumb) {
                    $thumb->select('id', 'name', 'slug', 'email', 'photo', 'isDeleted');
                }
            ])
            ->withCount(['totalViews'])
            ->withCount(['totalComments'])
            ->first();

        if (!$video)
            return abort(404);

        $videoWatchLaterCount = WatchLater::where(['video_id' => $video->id])->count();
        $videoLikeCount = VideoLikes::where(['video_id' => $video->id, 'liked' => 1])->count();
        $videoDisLikeCount = VideoLikes::where(['video_id' => $video->id, 'disliked' => 1])->count();
        $videoFlagCount = VideoFlag::where(['video_id' => $video->id])->count();

        $isWatchLaterByUser = WatchLater::where(['user_id' => Session::get('user_id'), 'video_id' => $video->id])->count() > 0 ? 1 : 0;
        $isLikedByUser = VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $video->id, 'liked' => 1])->count() > 0 ? 1 : 0;
        $isDisLikedByUser = VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $video->id, 'disliked' => 1])->count() > 0 ? 1 : 0;
        $isFlagByUser = VideoFlag::where(['user_id' => Session::get('user_id'), 'video_id' => $video->id])->count() > 0 ? 1 : 0;

        $videoCategories = VideoCategory::getVideoCategoryName($video->id);
        $videoTags = VideoTag::getVideoTagName($video->id);

        /*Similar Video*/

        $simiVideo = Video::where(['video_key' => $slug, 'status' => 1, 'isRewardVideo' => 1])->first();
        $videoTags_Updated = DB::table('video_tags')->where('video_id', $simiVideo->id)->pluck('tag_id')->toArray();
        $getSimilarVideoId = DB::table('video_tags')->whereIn('tag_id', $videoTags_Updated)->pluck('video_id')->toArray();
        $similarVideos = Video::whereIn('id', $getSimilarVideoId)->where('deleted_at', NULL)->where('isDeleted', 0)->where('video_key', '!=', $slug)->withCount(['totalViews'])->inRandomOrder()->take(20)->get();


        $getVideoCategoryId = DB::table('video_categories')->where('video_id', $video->id)->pluck('category_id')->toArray();

        $days = Carbon::now()->subDays(Carbon::now()->dayOfWeek)->startOfDay();
        $recomendedVideos = Video::where([['status', 1], ['isRewardVideo', 1], ['created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where('isDeleted', 0)->whereHas('video_category', function ($query) use ($getVideoCategoryId) {
            $query->whereHas('category', function ($query) use ($getVideoCategoryId) {
                $query->orderBy('created_at', 'desc');
                if (count($getVideoCategoryId) > 0) {
                    $query->whereIn('id', $getVideoCategoryId);
                }
                $query->where('isRewardVideo', '=', 1);

            }
            );
        })
            ->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
            ->orderBy('created_at', 'desc')
            ->where('isRewardVideo', 1)
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }
            ])
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo');
                }
            ])
            ->withCount(['totalViews'])
            ->with('watchTime')
            ->where('video_key', '!=', $slug)
            ->take(20)->get();

        $recomendedVideos = collect($recomendedVideos)->shuffle();

        $viewsstatus = VideoViews::where(['ip' => \Request::getClientIp(), 'video_id' => $video->id])->first();
        if ($viewsstatus === null) {
            $videoview = new VideoViews();
            $videoview->video_id = $video->id;
            $videoview->title = $video->title;
            $videoview->url = \Request::url();
            $videoview->session_id = \Request::getSession()->getId();
            $videoview->user_id = Session::get('user_id');
            $videoview->ip = \Request::getClientIp();
            $videoview->agent = \Request::header('User-Agent');
            $videoview->save();
        } else {
            VideoViews::where(['ip' => \Request::getClientIp(), 'video_id' => $video->id])->update(['updated_at' => DB::raw('NOW()')]);
        }

        $this->history($video);

        $moreVideoss = Video::where('user_id', '=', $video->user_id)->where('isDeleted', 0)
            ->orderBy('created_at', 'desc')
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }
            ])
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo');
                }
            ])
            ->with([
                'user.artist' => function ($user) {
                    $user->where('isDeleted', 0);
                }
            ])
            ->where('video_key', '!=', $slug)
            ->paginate(7);

        $followCount = DB::table('followers')->where('follows_id', '=', $video->user->id ?? 0)->count();

        $rewardVideos = Video::where([['isRewardVideo', '=', 1], ['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where('videos.isDeleted', 0)
            ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
            ->where('videos.isDeleted', 0)
            ->orderByDesc('id')
            ->holdUser()
            ->withCount(['totalViews'])
            ->with('watchTime')
            ->withCount(['video_likes'])
            ->withCount(['video_dislikes'])
            ->get();

        $array = [];

        foreach ($rewardVideos->sortByDesc('total_views_count')->values() as $reward) {
            foreach ($reward->video_tag as $tags) {
                $array[] = $tags->tag->tag;
            }
        }

        $userVideoCount = Video::where('user_id', '=', $video->user->id ?? 0)->where([
            'isDeleted' => 0,
            'isRewardVideo' => 0,
        ])->count();

        return view('homes.reward_single',
            [
                'title' => $video->title ?? 'untitle',
                'videoData' => $video,
                'videoCategories' => $videoCategories,
                'videoTags' => $videoTags,
                'similarVideos' => $similarVideos,
                'recomendedVideos' => $recomendedVideos,
                'loggedInUser' => Session::get('user_id'),
                'moreVideoss' => $moreVideoss,
                'videoLikeCount' => $videoLikeCount,
                'videoDisLikeCount' => $videoDisLikeCount,
                'videoWatchLaterCount' => $videoWatchLaterCount,
                'videoFlagCount' => $videoFlagCount,
                'isWatchLaterByUser' => $isWatchLaterByUser,
                'isLikedByUser' => $isLikedByUser,
                'isDisLikedByUser' => $isDisLikedByUser,
                'isFlagByUser' => $isFlagByUser,
                'activeUser' => User::find(Session::get('user_id')),
                'userVideoCount' => $userVideoCount,
                'followCount' => $followCount,
                "tags" => array_unique($array)
            ]
        );
    }

    public function trackRewardVideoWatch(Request $request, $video_id)
    {
        $user_id = $request->session()->get('user_id');

        $existingTrack = WatchTrack::where([
            'video_id' => $video_id,
            'user_id' => $user_id,
        ])->first();

        if (!$existingTrack) {
            (new WatchTrack([
                'video_id' => $video_id,
                'user_id' => $user_id,
                'ip' => $request->getClientIp(),
                'session' => $request->session()->getId(),
                'agent' => $request->header('User-Agent'),
                'track' => 0,
                'is_paid' => 0, // or false, depending on your logic
                'is_calculated' => 0, // or true, depending on your logic
            ]))->save();

            Session::put('last_request_times', time());

            return response()->json([
                'status' => true,
                'message' => 'Video watch tracking start'
            ]);
        }

        $lastRequestTimestamp = Session::get('last_request_times');
        $timeElapsed = (time() - $lastRequestTimestamp);
        Session::put('last_request_times', time());
        if ($timeElapsed > 5) {
            return response()->json([
                'status' => false,
                'message' => 'May you toMany request or timezone issue',
            ], 401);
        }

        $video = Video::select(['video_time'])->find($video_id);
        $existingTrack = WatchTrack::find($existingTrack->id);

        if ($video->video_time > $existingTrack->track) {
            if ($video->video_time < ($existingTrack->track + $timeElapsed)) {
                $existingTrack->track = $video->video_time;
                return [
                    'status' => false,
                    'message' => 'duration is completed',
                ];
            }

            $existingTrack->track = $existingTrack->track + $timeElapsed;
            $existingTrack->save();

            return response()->json([
                'status' => true,
                'message' => 'Video watch tracking updated.'
            ], 201);
        }

        return [
            'status' => false,
            'message' => 'duration is completed',
        ];
    }
    public function guestTrackRewardVideoWatch(Request $request, $video_id)
    {
        $existingTrack = WatchTrack::where('video_id', $video_id)
            ->where(function ($query) use ($request) {
                $query->where(function ($query) use ($request) {
                    $query->where('ip', $request->getClientIp())
                        ->orWhere('session', $request->session()->getId());
                })
                    ->orWhere('agent', $request->header('User-Agent'));
            })
            ->first();

        if (!$existingTrack) {
            (new WatchTrack([
                'video_id' => $video_id,
                'user_id' => NULL,
                'ip' => $request->getClientIp(),
                'session' => $request->session()->getId(),
                'agent' => $request->header('User-Agent'),
                'track' => 0,
                'is_paid' => 0, // or false, depending on your logic
                'is_calculated' => 0, // or true, depending on your logic
            ]))->save();

            Session::put('last_request_times', time());

            return response()->json([
                'status' => true,
                'message' => 'Video watch tracking start'
            ]);
        }

        $lastRequestTimestamp = Session::get('last_request_times');
        $timeElapsed = (time() - $lastRequestTimestamp);
        Session::put('last_request_times', time());
        if ($timeElapsed > 5) {
            return response()->json([
                'status' => false,
                'message' => 'May you toMany request or timezone issue',
            ], 401);
        }

        $video = Video::select(['video_time'])->find($video_id);
        $existingTrack = WatchTrack::find($existingTrack->id);

        if ($video->video_time > $existingTrack->track) {
            if ($video->video_time < ($existingTrack->track + $timeElapsed)) {
                $existingTrack->track = $video->video_time;
                return [
                    'status' => false,
                    'message' => 'duration is completed',
                ];
            }

            $existingTrack->track = $existingTrack->track + $timeElapsed;
            $existingTrack->save();

            return response()->json([
                'status' => true,
                'message' => 'Video watch tracking updated.'
            ], 201);
        }

        return [
            'status' => false,
            'message' => 'duration is completed',
        ];
    }

    public function UpdatewatchVideoTime(Request $request)
    {
        $user_id = $request->session()->get('user_id');

        if ($user_id) {
            $video_id = $request->videoID;
            $duration = $request->duration;

            $video = Video::find($video_id);
            if ($video) {
                $video_dir = $video->isRewardVideo == 1 ? "reward-video" : "video";
                $dir = base_path("data/$video_dir/$user_id");
                $file = "$dir/$video_id.json";
                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                }

                if (!file_exists($file)) {
                    touch($file);
                }

                $content = file_get_contents($file);
                $_sets = 0;
                $isNew = false;
                if (!(empty($content)) && preg_match("/^\[(.*)\]$/", $content)) {
                    $content = json_decode($content, true);

                    if (json_last_error() == JSON_ERROR_NONE) {
                        $_sets = count($content);
                    } else {
                        $_sets = [];
                    }
                } else {
                    $content = [];
                    // $ffmpeg = FFMpeg::fromDisk("video")
                    // 	->open($video->video);
                    // $_sets = intval(($ffmpeg
                    // 	->getDurationInSeconds()) / 3);
                    $_sets = $video->video_time / 3;
                    $isNew = true;
                }

                if ($_sets > 0 && intval($duration) >= 0) {
                    for ($i = 1; $i <= $_sets; $i++) {
                        if ($isNew) {
                            if ($i * 3 > $duration && (($i * 3) - 3) <= $duration) {
                                array_push($content, 1);
                            } else {
                                array_push($content, 0);
                            }
                        } else {
                            if ($i * 3 > $duration && (($i * 3) - 3) <= $duration) {
                                $content[$i - 1] = 1;
                            }
                        }
                    }

                    $stream = function ($attempt = 0) use ($file, $content) {
                        $writer = fopen($file, "w+");
                        $isWritten = false;
                        if (flock($writer, LOCK_EX | LOCK_NB)) {
                            $isWritten = true;
                            fwrite($writer, json_encode($content));
                            flock($writer, LOCK_UN);
                        }
                        fclose($writer);

                        if (!$isWritten && $attempt < 2) {
                            $stream($attempt + 1);
                        }
                    };

                    $stream();
                } else {
                    $watch_track = new WatchTrack;
                    $watch_track->video_id = $video_id;
                    $watch_track->user_id = $user_id;
                    $watch_track->ip = $request->getClientIp();
                    $watch_track->session = $request->session()->getId() ?? "";
                    $watch_track->agent = $request->header('User-Agent') ?? "";
                    $watch_track->track = "[1]";
                    $watch_track->save();
                }
            }
        }
    }

    public function watchVideo($videoKey = null, Request $request)
    {
        if (!empty($videoKey)) {
            $videoData = Video::where(['video_key' => $videoKey, 'videos.status' => 1, 'isRewardVideo' => 0])
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    },
                    'user' => function ($thumb) {
                        $thumb->select('id', 'name', 'slug', 'photo', 'isDeleted');
                    },
                    'user.artist' => function ($thumb) {

                    }
                ])
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->first();

            if ($videoData) {
                $videoWatchLaterCount = WatchLater::where(['video_id' => $videoData->id])->count();
                $videoLikeCount = VideoLikes::where(['video_id' => $videoData->id, 'liked' => 1])->count();
                $videoDisLikeCount = VideoLikes::where(['video_id' => $videoData->id, 'disliked' => 1])->count();
                $videoFlagCount = VideoFlag::where(['video_id' => $videoData->id])->count();

                $isWatchLaterByUser = WatchLater::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->count() > 0 ? 1 : 0;
                $isLikedByUser = VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id, 'liked' => 1])->count() > 0 ? 1 : 0;
                $isDisLikedByUser = VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id, 'disliked' => 1])->count() > 0 ? 1 : 0;
                $isFlagByUser = VideoFlag::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->count() > 0 ? 1 : 0;

                if (empty($videoData)) {
                    return Redirect::to('/');
                }

                $videoTitle = (!empty($videoData->title) ? $videoData->title : "");
                $videoCategories = VideoCategory::getVideoCategoryName($videoData->id);
                $videoTags = VideoTag::getVideoTagName($videoData->id);

                /*Recomended Video*/
                $getVideoCategoryId = DB::table('video_categories')->where('video_id', $videoData->id)->pluck('category_id')->toArray();
                $categoryWiseVideo = Video::where('isDeleted', 0)->whereHas('video_category', function ($query) use ($getVideoCategoryId) {
                    $query->whereHas(
                        'category',
                        function ($query) use ($getVideoCategoryId) {
                            $query->orderBy('created_at', 'desc');
                            $query->whereIn('id', $getVideoCategoryId);
                            $query->where('isRewardVideo', '=', 0);

                        }
                    );
                })
                    ->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
                    ->orderBy('created_at', 'desc')
                    ->where('isRewardVideo', 0)
                    ->whereRaw("id NOT IN (SELECT post_id from promote_posts WHERE post_type='video' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
                    ->with([
                        'thumb' => function ($thumb) {
                            $thumb->select('id', 'thumb');
                        }
                    ])
                    ->with([
                        'user' => function ($user) {
                            $user->select('id', 'name', 'slug', 'photo');
                        }
                    ])
                    ->withCount(['isWatchedVideo'])
                    ->withCount(['totalViews'])
                    ->with('watchTime')
                    ->where('video_key', '!=', $videoKey)
                    ->take(20)->get();


                /*Similar Video*/
                $getTagCategoryId = DB::table('video_tags')->where('video_id', $videoData->id)->pluck('tag_id')->toArray();
                $tagWiseVideo = Video::whereHas('video_tag', function ($query) use ($getTagCategoryId) {
                    $query->whereHas(
                        'tag',
                        function ($query) use ($getTagCategoryId) {
                            $query->orderBy('created_at', 'desc');
                            if (count($getTagCategoryId) > 0) {
                                $query->whereIn('id', $getTagCategoryId);
                            }
                            $query->where('isRewardVideo', '=', 0);

                        }
                    );
                })
                    ->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
                    ->orderBy('created_at', 'desc')
                    ->where('isRewardVideo', 0)
                    ->whereRaw("id NOT IN (SELECT post_id from promote_posts WHERE post_type='video' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
                    ->with([
                        'thumb' => function ($thumb) {
                            $thumb->select('id', 'thumb');
                        }
                    ])
                    ->with([
                        'user' => function ($user) {
                            $user->select('id', 'name', 'slug', 'photo');
                        }
                    ])
                    ->withCount(['totalViews'])
                    ->with('watchTime')
                    ->where('video_key', '!=', $videoKey)
                    ->take(20)->get();

                $tagWiseVideo = collect($tagWiseVideo);
                $tagWiseVideo = $tagWiseVideo->shuffle();

                //if(!empty(Session::get('user_id'))){
                /*set video views count start here*/
                $viewsstatus = VideoViews::where(['ip' => \Request::getClientIp(), 'video_id' => $videoData->id])->first();
                if ($viewsstatus === null) {
                    $videoview = new VideoViews();
                    $videoview->video_id = $videoData->id;
                    $videoview->title = $videoData->title;
                    $videoview->url = \Request::url();
                    $videoview->session_id = \Request::getSession()->getId();
                    $videoview->user_id = Session::get('user_id');
                    $videoview->ip = \Request::getClientIp();
                    $videoview->agent = \Request::header('User-Agent');
                    $videoview->save();
                } else {
                    VideoViews::where(['ip' => \Request::getClientIp(), 'video_id' => $videoData->id])->update(['updated_at' => DB::raw('NOW()')]);
                }


                $this->history($videoData);

                $moreVideos = Video::where([['user_id', $videoData->user_id], ['isRewardVideo', 0], 'isDeleted' => 0])
                    //->select('id','video_key','title','video_thumb_id','video_time', 'user_id', 'created_at')
                    //->orWhere('isRewardVideo', '=', 1)
                    ->whereRaw("id NOT IN (SELECT post_id from promote_posts WHERE post_type='video' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
                    ->orderBy('created_at', 'desc')
                    ->with([
                        'thumb' => function ($thumb) {
                            $thumb->select('id', 'thumb');
                        }
                    ])
                    ->with([
                        'user' => function ($user) {
                            $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                        }
                    ])
                    ->where('video_key', '!=', $videoKey)
                    ->take(7)
                    ->get();

                $activeUser = User::where(['isDeleted' => 0, 'id' => Session::get('user_id')])->first();
                $userVideoCount = Video::where('user_id', '=', @$videoData->user->id)->where(['isDeleted' => 0, 'isRewardVideo' => 0])->count();
                $followCount = DB::table('followers')->where('follows_id', '=', @$videoData->user->id)->count();
                $user = User::where(['id' => Session::get('user_id'), 'isDeleted' => 0])->first();
                $days = Carbon::now()->subDays(15);
                $tages = Video::where([['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where(['videos.isDeleted' => 0, 'videos.isRewardVideo' => 0])
                    ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'videos.created_at')
                    ->orderByDesc('id')
                    ->holdUser()
                    ->withCount(['totalViews'])
                    ->with('watchTime')
                    ->withCount(['video_likes'])
                    ->withCount(['video_dislikes'])
                    ->get();
                $array = [];

                if (count($tages) > 0) {
                    foreach ($tages->sortByDesc('total_views_count')->values() as $reward) {
                        foreach ($reward->video_tag as $tags) {
                            $array[] = $tags->tag->tag;
                        }
                    }
                }
                return view('homes.watch',
                    [
                        'title' => $videoTitle,
                        'videoData' => $videoData,
                        'videoCategories' => $videoCategories,
                        'videoTags' => $videoTags,
                        'categoryWiseVideo' => $categoryWiseVideo,
                        'tagWiseVideo' => $tagWiseVideo,
                        'loggedInUser' => Session::get('user_id'),
                        'moreVideos' => $moreVideos,
                        'videoLikeCount' => $videoLikeCount,
                        'videoDisLikeCount' => $videoDisLikeCount,
                        'videoWatchLaterCount' => $videoWatchLaterCount,
                        'videoFlagCount' => $videoFlagCount,
                        'isWatchLaterByUser' => $isWatchLaterByUser,
                        'isLikedByUser' => $isLikedByUser,
                        'isDisLikedByUser' => $isDisLikedByUser,
                        'isFlagByUser' => $isFlagByUser,
                        'activeUser' => $activeUser,
                        'userVideoCount' => $userVideoCount,
                        'followCount' => $followCount,
                        'user' => $user,
                        'tags' => array_unique($array)
                    ]
                );
            } else {
                return abort(404);
            }
        } else {
            return Redirect::to('/');
        }
    }

    public function myrewards()
    {
        $pageTitle = "My Rewards";
        $rewardVideos = Video::where(['isRewardVideo' => 1, 'user_id' => session()->get('user_id')])
            ->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
            ->orderByDesc('id')
            ->where('videos.isDeleted', 0)
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }
            ])
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo');
                }
            ])
            ->withCount(['totalViews'])
            ->with('watchTime')
            ->paginate(10);

        return view('rewards.index', ['title' => $pageTitle, 'rewardVideos' => $rewardVideos]);
    }


    public function rewardTrendingTags($name)
    {
        $pageTitle = "Rewards Trending Tags";
        $days = Carbon::now()->subDays(Carbon::now()->dayOfWeek)->startOfDay();
        $id = DB::table('tags')->where('tag', $name)->get(['id']);
        $video_id = DB::table('video_tags')->where('tag_id', $id->pluck('id'))->get(['video_id']);
        $videos = Video::whereIn('videos.id', $video_id->pluck('video_id'))->where('videos.isDeleted', 0)->where([['videos.isRewardVideo', '=', 1], ['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]]);
        $videos = $videos->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
            ->where('videos.isDeleted', 0)
            ->orderByDesc('id')
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }, 'video_tag.tag' => function ($tag) {
                    $tag->groupBy('id');
                }
            ])
            ->with([
                'totalViews' => function ($query) {
                    $query->select('video_id')
                        ->groupBy('video_id', 'video_id')
                        ->orderBY(DB::raw('count(video_id)'), 'DESC');
                }
            ])
            ->with([
                'user.artist' => function ($user) {
                    $user->where('isDeleted', 0);
                }

            ])
            ->holdUser()
            ->withCount(['totalViews'])
            ->with('watchTime')
            ->withCount(['video_likes'])
            ->withCount(['video_dislikes'])
            ->paginate(10);
        $array = [];

        if (count($videos) > 0) {
            foreach ($videos->sortByDesc('total_views_count')->values() as $reward) {
                foreach ($reward->video_tag as $tags) {
                    $array[] = $tags->tag->tag;
                }
            }
        }
        return view('homes.rewards', [
            'title' => $pageTitle,
            'rewardVideos' => $videos,
            "selected" => null,
            "tags" => array_unique($array)
        ]);
    }

    public function history($video)
    {
        // Get the user ID from the session
        $userId = Session::get('user_id');

        // Check if a watch history entry exists for this user
        if ($userId && !WatchHistory::where(['user_id' => $userId, 'video_id' => $video->id])->exists()) {
            // Create a new WatchHistory entry
            $history = new WatchHistory([
                'user_id' => $userId,
                'video_id' => $video->id,
                'title' => $video->title,
                'url' => \Request::url(),
                'session_id' => \Request::getSession()->getId(),
                'ip' => \Request::getClientIp(),
                'agent' => \Request::header('User-Agent'),
            ]);

            // Save the history entry
            $history->save();
        }
    }
}