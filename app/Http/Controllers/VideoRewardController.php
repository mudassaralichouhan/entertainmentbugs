<?php

namespace App\Http\Controllers;

use App\Models\Video;

use Cache;
use Carbon\Carbon;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Input;
use Redirect;
use Session;
use Validator;
use VideoThumbnail;

class VideoRewardController extends Controller
{
    /*Rewards*/
    public function rewards($week = null)
    {
        $pageTitle = "Rewards";
        $days = Carbon::now()->subDays(Carbon::now()->dayOfWeek)->startOfDay();
        $fromDays = Carbon::now()->subDays(Carbon::now()->dayOfWeek + 7);
        if (!(is_null($week))) {
            if ($week == "second") {

                $rewardVideos = Video::where('videos.isDeleted', 0)->where([['videos.isRewardVideo', '=', 1], ['videos.created_at', '<', $days->format("Y-m-d 00:00:00")], ['videos.created_at', '>=', $fromDays->format("Y-m-d 00:00:00")]]);
            } else {
                return abort(404);
            }
        } else {

            $rewardVideos = Video::where([['isRewardVideo', '=', 1], ['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where('videos.isDeleted', 0);
        }

        $rewardVideos = $rewardVideos
            ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
            ->where('videos.isDeleted', 0)
            ->orderByDesc('id')
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }, 'video_tag.tag' => function ($tag) {
                    $tag->groupBy('id');
                }
            ])
            ->with([
                'totalViews' => function ($query) {
                    $query->select('video_id')
                        ->groupBy('video_id', 'video_id')
                        ->orderBY(DB::raw('count(video_id)'), 'DESC');
                }
            ])
            ->with([
                'user.artist' => function ($user) {
                    $user->where('isDeleted', 0);
                }

            ])
            ->holdUser()
            ->withCount(['totalViews'])
            ->with('watchTime')
            ->withCount(['video_likes'])
            ->withCount(['video_dislikes'])
            ->paginate(10);

        $array = [];

        if (count($rewardVideos) > 0) {
            foreach ($rewardVideos->sortByDesc('total_views_count')->values() as $reward) {
                foreach ($reward->video_tag as $tags) {
                    $array[] = $tags->tag->tag;
                }
            }
        }

        return view('homes.rewards', [
        'title' => $pageTitle,
        'rewardVideos' => $rewardVideos,
        "selected" => $week,
        "tags" =>array_unique($array)
    ]);
    }
}