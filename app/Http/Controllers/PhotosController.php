<?php

namespace App\Http\Controllers;

use App\Models\Follower;
use App\Models\photocomment;
use App\Models\PhotoLikes;
use App\Models\User;
use App\Notifications\Notices;
use App\Notifications\PhotoUploaded;
use App\UploadPhotos;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Redirect;
use Session;
use Validator;


class PhotosController extends Controller
{
    private $per_page = 30;

    public function index(Request $request)
    {
        if ($request->ajax() && $request->action == 'photo_lists') {
            return $this->photo_lists();
        }

        $latestPhotos = UploadPhotos::holdUser()->select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->with('user')
            ->whereRaw("upload_photos.isDeleted = 0 AND upload_photos.id NOT IN (SELECT post_id from promote_posts WHERE `isDeleted`='0' AND post_type='photo' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->latest()
            ->groupBY('upload_photos.id')
            ->inRandomOrder()
            ->take(15)
            ->get();

        $days = Carbon::now()->subDays(15);
        $all_image = UploadPhotos::where([['upload_photos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->whereRaw("upload_photos.id NOT IN (SELECT post_id from promote_posts WHERE post_type='photo' AND `status`='1' AND `is_paid`='1')")
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->leftjoin('users', 'upload_photos.user_id', '=', 'users.id')
            ->where('users.isDeleted', '=', 0)
            ->where('upload_photos.isDeleted', '=', 0)
            ->groupBY('upload_photos.id')
            ->orderBy('id', 'desc')
            ->get();

        if (count($all_image) > 0) {
            foreach ($all_image->sortByDesc('total')->values() as $reward) {
                $array[] = preg_split('/[^a-zA-Z0-9_ \-()\/%-&]/s', trim(str_replace(array("#", "'", ";"), '', $reward->tag_list)));
            }
        }

        return view('photos.index', [
            'title' => 'Photos',
            'tags' => array_unique(array_merge(...$array ?? [])),
            'latestPhotos' => $latestPhotos
        ]);
    }

    public function loadmorePhotoAjax(Request $request)
    {
        $start = $request->get("start");
//        $rowperpage = $this->per_page;
//        $totalrecords = UploadPhotos::select('*')->where('upload_photos.isDeleted', 0)->count();
        $getPhotoList = UploadPhotos::select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->whereRaw("upload_photos.isDeleted = 0 AND upload_photos.id NOT IN (SELECT post_id from promote_posts WHERE post_type='photo' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->groupBY('upload_photos.id')
            ->orderBy('id', 'desc')
            ->skip($start)
            ->take($this->per_page)
            ->get();

        $data = ['html' => ''];
        if ($request->get('model') && $request->get('model') == 'details-listing') {
            $data['html'] = view('photos.inc.details-listing', ['photos' => $getPhotoList])->render();
        } else {
            $data['html'] = view('photos.inc.listing', ['photos' => $getPhotoList])->render();
        }

        return response()->json($data);
    }

    public function create()
    {
        if (!empty(Session::get('user_id'))) {
            $pageTitle = 'Photos Create';
            $followers = Follower::with('user', 'followers')->where('user_id', Session::get('user_id'))->get();
            return view('photos.create', ['title' => $pageTitle, 'followers' => $followers]);
        } else {
            return redirect('login');
        }
    }

    public function store(Request $request)
    {
        $user = User::find(Session::get('user_id'));
        $input = $request->all();

        $rules = array(
            'photoTitle' => 'required|max:200'
        );

        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            return redirect()->to('/photos/create')->withErrors($validator)->withInput();
        }

        $photNewModel = new UploadPhotos();
        $photNewModel->photo_title = !empty($input['photoTitle']) ? $input['photoTitle'] : '';
        $photNewModel->user_id = !empty($input['photo_user_id']) ? $input['photo_user_id'] : '';
        $photNewModel->followers_list = !empty($input['followersList']) ? json_encode($input['followersList']) : '';
        $photNewModel->tag_list = !empty($input['tags']) ? trim(str_replace(array("#", "'", ";"), '', $input['tags'])) : '';

        $photoData = $input['Photos'];
        $dataImageUrl = [];

        if (is_array($photoData) && count($photoData) > 0) {
            foreach ($photoData as $photo) {
                if (!$photo) continue;

                $photoPath = str_replace(HTTP_PATH . "/public", "", $photo);
                $tempPublicPath = public_path($photoPath);
                $filename = basename($photo);
                $filePath = 'uploads/photo/' . $filename;

                if (File::exists($tempPublicPath)) {
                    File::move($tempPublicPath, public_path($filePath));
                    array_push($dataImageUrl, $filename);
                }
            }
        }

        $photNewModel->images = is_array($dataImageUrl) && count($dataImageUrl) > 0 ? serialize($dataImageUrl) : '';
        if ($photNewModel->save()) {
            $followers = DB::table('followers')->where('follows_id', '=', $user->id)->get();
            foreach ($followers as $f) {
                $follower = User::find($f->user_id);
                if ($follower) {
                    $follower->notify(new Notices($follower, ' added a new photo', 'following-post', '', '', $user->id, $photNewModel->id));
                }
            }

            $user->notify(new PhotoUploaded($user, $photNewModel));
            return redirect()->to('/' . session()->get('slug') . '/photo')->with('success', 'Your photos uploaded successfully');
        }

        return redirect()->back();
    }

    public function edit($id)
    {
        if (!empty(Session::get('user_id'))) {
            $photo = UploadPhotos::where(['id' => $id, 'user_id' => Session::get('user_id')])->first();
            if ($photo) {
                $pageTitle = 'Photos Edit';
                $followers = Follower::with('user', 'followers')->where('user_id', Session::get('user_id'))->get();
                return view('photos.edit', ['photo' => $photo, 'title' => $pageTitle, 'followers' => $followers]);
            }
        } else {
            return redirect('login');
        }
    }

    public function update(Request $request, $id)
    {
        $user = User::find(Session::get('user_id'));
        $input = $request->all();
        $photoModel = UploadPhotos::find($id);

        if ($photoModel) {
            $rules = array(
                'photoTitle' => 'required|max:200'
            );

            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return redirect()->to("/photos/$id/edit")->withErrors($validator)->withInput();
            } else {
                $photNewModel = array();
                $photNewModel['photo_title'] = !empty($input['photoTitle']) ? $input['photoTitle'] : '';
                $photNewModel['user_id'] = !empty($input['photo_user_id']) ? $input['photo_user_id'] : '';
                $photNewModel['followers_list'] = !empty($input['followersList']) ? json_encode($input['followersList']) : '';
                $photNewModel['tag_list'] = !empty($input['tags']) ? $input['tags'] : '';

                $photoData = $input['Photos'];
                $dataImageUrl = [];

                $oldPhotos = unserialize($photoModel->images);
                if ($photoData) {
                    foreach ($photoData as $key => $item) {
                        $source = $item['baseSixFourImageDataSource'];
                        if ($source) {
                            $folderPath = public_path('uploads/photo/');

                            $image_parts = explode(";base64,", $source);

                            if (is_array($image_parts) && count($image_parts) > 1) {
                                $image_type_aux = explode("image/", $image_parts[0]);
                                $image_type = $image_type_aux[1];
                                $image_base64 = base64_decode($image_parts[1]);

                                $imageName = $request->session()->get('user_id') . "-" . uniqid() . ".$image_type";
                                $imageFullPath = $folderPath . $imageName;

                                file_put_contents($imageFullPath, $image_base64);
                                $dataImageUrl[$key] = $imageName;
                            } else {
                                $dataImageUrl[$key] = $oldPhotos[$key];
                            }
                        }
                    }
                }

                $photNewModel['images'] = !empty($dataImageUrl) ? serialize($dataImageUrl) : '';
                if (UploadPhotos::where(['id' => $id])->update($photNewModel)) {
                    $photNewModel = UploadPhotos::find($id);
                    $followers = DB::table('followers')->where('follows_id', '=', $user->id)->get();
                    foreach ($followers as $f) {
                        $follower = User::find($f->user_id);
                        if ($follower) {
                            $follower->notify(new Notices($follower, ' added a new photo', 'following-post', '', '', $user->id, $photNewModel->id));
                        }
                    }

                    $user->notify(new PhotoUploaded($user, $photNewModel, true));
                    return redirect()->back()->with('success', 'Photo updated successfully');
                }
            }
        }
        return redirect()->back();
    }

    public function destroy($id)
    {
        if (!empty(Session::get('user_id'))) {
            $photos = UploadPhotos::findorfail($id);
            if ($photos->user_id == Session::get('user_id')) {
                PhotoLikes::where('photo_id', $id)->delete();
                $photos->delete();
                return redirect()->back();
            } else {
                return redirect()->back();
            }
        } else {

        }
    }

    public function uploadCropImage(Request $request)
    {
        $folderPath = public_path('uploads/photo/');

        $image_parts = explode(";base64,", $request->image);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);

        $imageName = uniqid() . '.png';

        $imageFullPath = $folderPath . $imageName;

        file_put_contents($imageFullPath, $image_base64);

//        $saveFile = new Picture;
//        $saveFile->name = $imageName;
//        $saveFile->save();

        return response()->json(['success' => 'Crop Image Uploaded Successfully']);
    }

    public function myphotos($slug)
    {
        return redirect('/' . $slug . '/photo');
    }

    public function showPhotos(Request $request)
    {
        $pageTitle = 'Stories';
        $user = User::where('slug', $request->slug)->first();
        $userId = $user->id;
        $user = User::where(['id' => $userId])->first();
        $photos = UploadPhotos::where('user_id', $userId)->where('isDeleted', 0)->orderByDesc('id')->get();
        $followers = DB::table('followers')->where('follows_id', '=', $userId)->count();
        return view('photos.myphoto', ['title' => $pageTitle, 'followers' => $followers, 'user' => $user, 'user_photos' => $photos]);
    }

    public function single_photo($id)
    {
        $single_page_all_details = UploadPhotos::select('upload_photos.*', DB::raw('COUNT(photo_likes.liked) as total'), 'upload_photos.created_at', 'upload_photos.id', 'users.id AS user_id', 'users.name', 'users.photo', 'users.facebook', 'users.twitter')
            ->where(['upload_photos.id' => $id])
            ->where('upload_photos.isDeleted', 0)
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->join('users', 'upload_photos.user_id', '=', 'users.id')
            ->where('users.isDeleted', '=', 0)
            ->first();


        $single_page_all_details_id = @$single_page_all_details->user_id;
        $random_image = UploadPhotos::select('upload_photos.created_at', 'upload_photos.images as images', 'upload_photos.id as uploadID', 'users.id as id', 'users.name', 'users.photo', 'users.facebook', 'users.twitter')
            ->join('users', 'upload_photos.user_id', '=', 'users.id')
            ->where('users.isDeleted', '=', 0)
            ->where('upload_photos.isDeleted', '=', 0)
            ->where('upload_photos.id', '!=', $id)
            ->whereRaw("upload_photos.user_id = ? AND (CASE WHEN (SELECT COUNT(*) FROM upload_photos WHERE `user_id`=?) <= 5 THEN 1 WHEN ? >= (SELECT id FROM upload_photos WHERE `user_id`=? ORDER BY id DESC LIMIT 1 OFFSET 5) THEN 1 ELSE upload_photos.id > ? END) ORDER BY (CASE WHEN ? >= (SELECT id FROM upload_photos WHERE `user_id`=? ORDER BY id DESC LIMIT 1 OFFSET 5) THEN upload_photos.id ELSE NULL END) DESC", [$single_page_all_details_id, $single_page_all_details_id, $id, $single_page_all_details_id, $id, $id, $id])
            ->limit(5)
            ->get();

        //print_r($id);
        //print_r($single_page_all_details_id);
        //exit;

        $totalrecords = UploadPhotos::select('*')->where('isDeleted', 0)->count();

        $all_image = UploadPhotos::select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->whereRaw("upload_photos.id NOT IN (SELECT post_id from promote_posts WHERE post_type='photo' AND `status`='1' AND `is_paid`='1')")
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->leftjoin('users', 'upload_photos.user_id', '=', 'users.id')
            ->where('users.isDeleted', '=', 0)
            ->where('upload_photos.isDeleted', '=', 0)
            ->where('upload_photos.id', '!=', $id)
            ->where('users.id', '!=', $single_page_all_details_id)
            ->groupBY('upload_photos.id')
            ->orderBy('id', 'desc')
            ->paginate(30);
        // return $single_page_all_details ;
        // return $single_page_all_details;
        $pageTitle = 'Photos';
        return view('photos.single_page', ['title' => $pageTitle, 'single_page_all_details' => $single_page_all_details, 'random_image' => $random_image, 'all_image' => $all_image, "picID" => $id, 'totalrecords' => $totalrecords, 'rowperpage' => $this->per_page]);
    }

    public function categoryPhotoSortByRecent(Request $request)
    {
        $getPhotoList = UploadPhotos::select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->where('upload_photos', 0)
            ->groupBY('upload_photos.id')
            ->orderBy('id', 'desc')
            ->paginate(30);
        // return $getPhotoList;

        $data = "";

        $data .= '<div class="photo-block-all-in-one" id="">';

        if ($getPhotoList) {
            foreach ($getPhotoList as $key => $items) {
                $photos = unserialize($items->images);
                $tagsList = explode(',', $items->tag_list);


                $data .= '<div class="photo-block"  value="' . $items->id . '" 
					<a href="#" class="user_info_mini"><i class="fa fa-thumbs-up"></i><span>' . $items->total . '</span></a>
	  
					<a href="javaScript:void(0)"  ><img src="' . asset('public/uploads/photo/' . $photos[0] . '') . '"></a>
					<span style="position: absolute;width: 25px;bottom: 13px;right: 6px;z-index: 99999;">
					<i class="far fa-clone" style="font-size: 17px;color: #fff;text-shadow: 2px 1px 1px #000;"></i>
				</span>
				</div>';

            }
        }


        $data .= '</div>';

        return response()->json(['data' => $data]);
    }

    public function categoryPhotoSortByLikes(Request $request)
    {
        $getPhotoList = UploadPhotos::select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->where('upload_photos.isDeleted', 0)
            ->groupBY('upload_photos.id')
            ->orderBy('total', 'desc')
            ->paginate(30);

        $data = "";

        $data .= '<div class="photo-block-all-in-one" id="">';

        if ($getPhotoList) {
            foreach ($getPhotoList as $key => $items) {
                $photos = unserialize($items->images);
                $tagsList = explode(',', $items->tag_list);


                $data .= '<div class="photo-block"  value="' . $items->id . '" 
					<a href="#" class="user_info_mini"><i class="fa fa-thumbs-up"></i><span>' . $items->total . '</span></a>
	  
					<a href="javaScript:void(0)"  ><img src="' . asset('public/uploads/photo/' . $photos[0] . '') . '"></a>
					<span style="position: absolute;width: 25px;bottom: 13px;right: 6px;z-index: 99999;">
					<i class="far fa-clone" style="font-size: 17px;color: #fff;text-shadow: 2px 1px 1px #000;"></i>
				</span>
				</div>';

            }
        }


        $data .= '</div>';

        return response()->json(['data' => $data]);
    }

    public function popup(Request $request)
    {
        $photolikes = PhotoLikes::select('photo_likes.*', DB::raw('COUNT(photo_likes.liked) as total'))
            ->where(['photo_likes.photo_id' => $request->value])
            ->groupBy('photo_likes.liked')
            ->get();

        $photo_details = UploadPhotos::select('upload_photos.*', 'upload_photos.created_at', 'upload_photos.id', 'users.id', 'users.name', 'users.slug', 'users.photo', 'users.facebook', 'users.twitter')
            ->join('users', 'upload_photos.user_id', '=', 'users.id')
            ->find(['upload_photos.id' => $request->value])
            ->where('users.isDeleted', 0)
            ->where('upload_photos.isDeleted', 0)
            ->first();


        $tags = json_decode($photo_details->followers_list);
        $photo = unserialize($photo_details->images);
        $words = explode(' ', $photo_details->name);
        $user_image = strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1));

        $photo_user = UploadPhotos::select('upload_photos.*', 'users.*')
            ->join('users', 'upload_photos.user_id', '=', 'users.id')
            ->first();
        $image = unserialize($photo_user->images);

        $tags = json_decode($photo_details->followers_list);
        $tags_count = empty($tags) ? 0 : count($tags);

        $isFollowing = DB::table('followers')
            // ->select('followers.*')
            ->where(['follows_id' => $photo_details->user_id])
            ->where(['user_id' => session()->get('user_id')])
            ->first();

        if (!empty($tags[0])) {
            $tags_details1 = User::select('users.*')->where(['name' => $tags[0]])->first();
        }
        if (!empty($tags[1])) {
            $tags_details2 = User::select('users.*')->where(['name' => $tags[1]])->first();
        }
        if (!empty($tags[2])) {
            $tags_details3 = User::select('users.*')->where(['name' => $tags[2]])->first();
        }

        $video = photocomment::select('photocomments.*', 'users.id as userID', 'users.photo as photo', 'users.name as name', 'users.slug as slug')
            ->join('users', 'photocomments.user_id', '=', 'users.id')
            ->where([['parent_id', $request->value], ['commentable_id', 0]])->orderByDesc('photocomments.id')->paginate(3);
    }

    public function show(Request $request)
    {
        $photos = UploadPhotos::where('id', '<=', $request->value)
            ->with("user")
            ->orderByDesc("id")
            ->paginate(10);

        $screen = $request->get('screen');
        $template = 'photos.mobile';
        if ($screen && strtolower($screen) == 'l') {
            $template = 'photos.inc.popup';
        }

        return [
            "html" => view($template,
                compact('photos'))->render()
        ];
    }

    public function loadPhotos(Request $request)
    {
        return __FUNCTION__;
    }

    public function photo_lists()
    {
        $latestPhotos = UploadPhotos::holdUser()
            ->select('upload_photos.id as id')
            ->whereRaw("upload_photos.isDeleted = 0 AND upload_photos.id NOT IN (SELECT post_id from promote_posts WHERE `isDeleted`='0' AND post_type='photo' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->take(15)
            ->get();

        $followers = UploadPhotos::whereNotIn('upload_photos.id', $latestPhotos->pluck('id'))
            ->join('followers', 'followers.follows_id', '=', 'upload_photos.user_id')
            ->join('users', 'users.id', '=', 'followers.user_id')
            ->where([
                'upload_photos.isDeleted' => 0,
                'users.isDeleted' => 0,
                'followers.follows_id' => Session::get('user_id'),
            ])->get();

        $getPhotoList = UploadPhotos::whereNotIn('upload_photos.id', $latestPhotos->pluck('id'))
            ->holdUser()
            ->select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->whereRaw("upload_photos.isDeleted = 0 AND upload_photos.id NOT IN (SELECT post_id from promote_posts WHERE `isDeleted`='0' AND post_type='photo' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->inRandomOrder()
            ->groupBY('upload_photos.id')
            ->get();

        if ($followers) {
            $getPhotoList = $getPhotoList->merge($followers);
        }

        return view('photos.inc.listing', [
            'photos' => $getPhotoList,
        ]);
    }
}
