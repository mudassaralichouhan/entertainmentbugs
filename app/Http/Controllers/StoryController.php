<?php

namespace App\Http\Controllers;
use DB;
use Image;
use Input;
use Session;
use Redirect;
use Validator;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Story;
use App\Models\Video;
use App\Models\Category;
use App\Models\StoryViews;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Notifications\Notices;
use App\Models\StoryLikeDislike;
use App\Notifications\LikedStory;
use App\Notifications\DislikedStory;
use App\Notifications\StoryUploaded;

class StoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pageTitle = 'Stories';
        
        $followers = Story::join('followers','followers.follows_id','=','stories.user_id')
        ->where('stories.isDeleted', 0)
        ->where('followers.follows_id',Session::get('user_id'))
            ->withCount(['totalViews'])
            ->withCount(['totalComments'])
             ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram')->where('isDeleted', 0);
                },'user.artist' => function ($user) {
                    $user->where('isDeleted', 0);
                }
            ])
       
       
        ->inRandomOrder()
        ->take(10)
        ->get();
     
        $topOptionsStrory = Story::select('stories.id', 'title', 'stories.about', 'cover_image', 'thumb_image', 'category', 'user_id', 'stories.created_at', 'stories.slug')
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram')->where('isDeleted', 0);
                },'user.artist' => function ($user) {
                    $user->where('isDeleted', 0);
                }
            ])
			->whereRaw("stories.id NOT IN (SELECT post_id from promote_posts WHERE post_type='story' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
			->holdUser()
			->where('stories.isDeleted', 0)
            ->withCount(['totalViews'])
            ->withCount(['totalComments'])
            
            ->inRandomOrder()
            
            ->paginate(10);
        if($request->ajax()){
           
            $view2 = view('elements.story.cards', ["stories" => $topOptionsStrory])->render();

			return response()->json(['html' => $view2]);
        }
        $tages = StoryViews::where('story_views.user_id',Session::get('user_id')??0)->join('stories','stories.id','=','story_id')->get()->toArray();
          $mostViewedStory =[];
      $datas = []; 
        if(count($tages)>0){
             
              foreach($tages as $item){
                 
                $datas=array_merge($datas,explode(',', $item['category']));
            }
             $mostViewedStory = Story::select('stories.id', 'title', 'stories.about', 'cover_image', 'thumb_image', 'category', 'user_id', 'stories.created_at', 'stories.slug')
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram')->where('isDeleted', 0);
                },'user.artist' => function ($user) {
                    $user->where('isDeleted', 0);
                }
            ])
            ->holdUser()
            ->whereIn('stories.category',$datas)
            ->where('stories.isDeleted', 0)
            ->withCount(['totalViews'])
            ->withCount(['totalComments'])
            
            ->inRandomOrder()
            ->limit(20)
            ->get();
        }
        
    
        //return $tages;
       
           //return $mostViewedStory;
        
        /******* Order By Rating *****/
        $mostRatedStory = Story::select(array('stories.id', 'stories.title', 'stories.about', 'stories.cover_image', 'stories.thumb_image', 'stories.category', 'stories.slug', 'stories.user_id', DB::raw('AVG(rating) as ratings_average')))
            ->leftJoin('ratings', 'stories.user_id', '=', 'ratings.author_id')
            ->where('stories.user_id',Session::get('user_id'))
            ->where('stories.isDeleted', 0)
            ->groupBy('stories.user_id')
            
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram')->where('isDeleted', 0);
                }
                ,'user.artist' => function ($user) {
                    $user->where('isDeleted', 0);
                }
            ])
            ->holdUser()
            ->withCount(['totalViews'])
            ->withCount(['totalComments'])
            ->limit(12)
            ->inRandomOrder()
            ->get();

        //$story_categories = Category::getCategoryList();
		
			$story_categories = DB::table('categories')
            ->select('categories.category')
            ->where('parent_id',0)
           
            ->get();
        
     //return $mostRatedStory;
    $days = Carbon::now()->subDays(15);
     $tages = Story::where([ ['stories.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where('stories.isDeleted',0)

			->orderByDesc('id')
			->holdUser()
			->withCount(['totalViews'])
		
			->get(['id', 'title', 'tags', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug']);
			
		$array = [];
            
            if(count($tages)>0){
            foreach($tages->sortByDesc('total_views_count')->values()  as $reward){
               $array[]= explode(",",$reward->tags);
            
        }
    }
    
        return view('story.index', [
            'title' => $pageTitle,
            'mostViewedStory' => $mostViewedStory,
            'mostRatedStory' => $mostRatedStory,
            'followers' => $followers,
            'category'=>'',
            'story_categories' => $story_categories,
            'topOptionsStrory' => $topOptionsStrory,
            'tages'=>empty($array)?$array:array_unique(array_merge(...$array))
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = 'Write Story';
     //   $videoCategories = Category::getCategoryList();
		
		$videoCategories = DB::table('categories')
            ->select('*')
            ->where('parent_id',0)           
            ->get();
			
			
        $stories = Story::where('user_id', Session::get('user_id'))
            ->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug')
            ->orderBy('created_at', 'desc')
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
                }
            ])
            ->paginate(3);
        
        return view('story.create', [
            'title' => $pageTitle,
            'videoCategories' => $videoCategories,
            'stories' => $stories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = Input::all();

        $user = User::find(Session::get('user_id'));

        //  dd($input);

        if (!empty($input)) {

            $rules = array(
                'title' => 'required|max:100',
                'tags' => 'required|string',
                'language' => 'required|string',
                'about' => 'required|string'

            );
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                return Redirect::to(route('story.create'))->withErrors($validator)->withInput();
            } else {

                $story = new Story();
                $story['title'] = $input['title'];
                $story['tags'] = $input['tags'];
                $story['language'] = $input['language'];
                $story['about'] = $input['about'];
                $story['category'] = implode(',', $input['categories']);
                $story['user_id'] = Session::get('user_id');
                $story['slug'] = Str::slug($input['title']);

                if (!empty($input['cover_image'])) {
                    $story['cover_image'] = $input['cover_image'];
                }

                if (!empty($input['thumb'])) {
                    $story['thumb_image'] = $input['thumb'];
                }

                $story->save();
                $story->slug = $story->slug."-".$story->id;
                $story->save();

                $followers = DB::table('followers')->where('follows_id', '=', $user->id)->get();
                foreach ($followers as $f) {
                    $follower = User::find($f->user_id);
                    if( !is_null($follower) ){
                        $follower->notify(new Notices($follower, ' added a new story', 'following-post', '', '', $user->id, $story->id));
                    }
                }

                if( !is_null($user) ){
                    $user->notify(new StoryUploaded($user, $story));
                }
                return redirect()->to('/'.$request->session()->get('slug').'/stories')->with('success', 'Your story is uploaded successfully');
            }
        }

        return response()->back()->with('success', 'Thanks for posting your story with us!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function show(Story $story, $slug)
    {
        $pageTitle = 'Story Details';
        $user = User::where(['id' => Session::get('user_id')])->first();
        if (!empty(Session::get('user_id'))) {
            $story = Story::where("slug", $slug)
                
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                     
                    },
                    'user.artist'=> function ($user) {
                     $user->where('isDeleted',0);
                    }
                ])
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->first();
                
            if( is_null($story) ){
                return abort(404);
            }

            $pageTitle = $story->title;
            $storyRating = $story->avgRating;
            $userStoryRating = DB::table('ratings')->where(['author_id' => Session::get('user_id'), 'ratingable_id' => $story->id])->value('rating');

            $categories = explode(',', $story->category);
            $story_categories = Category::whereIn('id', $categories)->get();
            $sql_category = "(";
            foreach( $categories as $key => $category){
                if( $key == 0 ){
                    $sql_category .= "$category IN (category)";
                } else {
                    $sql_category .= " || $category IN (category)";
                }
            }
            $sql_category .= ")";
            $recommended = Story::where('isDeleted',0)->whereRaw($sql_category)
                ->whereRaw("id NOT IN (SELECT post_id from promote_posts WHERE post_type='story' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
                ->limit(10)
                ->get();
            $storyLikeCount = StoryLikeDislike::where(['story_id' => $story->id, 'liked' => 1])->count();
            $storyDisLikeCount = StoryLikeDislike::where(['story_id' => $story->id, 'disliked' => 1])->count();

            $isLikedByUser = StoryLikeDislike::where(['user_id' => Session::get('user_id'), 'story_id' => $story->id, 'liked' => 1])->count() > 0 ? 1 : 0;
            $isDisLikedByUser = StoryLikeDislike::where(['user_id' => Session::get('user_id'), 'story_id' => $story->id, 'disliked' => 1])->count() > 0 ? 1 : 0;

            $followers = DB::table('followers')->where('follows_id', '=', Session::get('user_id'))->count();


            $storystatus = StoryViews::where(['ip' => \Request::getClientIp(), 'story_id' => $story->id])->first();
            if ($storystatus === null) {
                $storyview = new StoryViews();
                $storyview->story_id = $story->id;
                $storyview->title = $story->title;
                $storyview->url = \Request::url();
                $storyview->session_id = \Request::getSession()->getId();
                $storyview->user_id = Session::get('user_id');
                $storyview->ip = \Request::getClientIp();
                $storyview->agent = \Request::header('User-Agent');
                $storyview->save();
            } else {
                StoryViews::where(['ip' => \Request::getClientIp(), 'story_id' => $story->id])->update(['updated_at' => DB::raw('NOW()')]);
            }

            $tagsArray = explode(',', $story->tags);
            $search = $tagsArray[0];

            $stories = Story::where('user_id', Session::get('user_id'))->where('stories.isDeleted',0)
               
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                      
                    },
                    'user.artist'=> function ($user) {
                     $user->where('isDeleted',0);
                    }
                ])
                ->whereRaw("find_in_set('" . $search . "',tags)")
                ->inRandomOrder()
                ->paginate(4);
            return view('story.story_details', ['title' => $pageTitle, 'userStoryRating' => $userStoryRating, 'storyRating' => $storyRating, 'user' => $user, 'story_categories' => $story_categories, 'loggedInUser' => Session::get('user_id'), 'followers' => $followers, 'storyDisLikeCount' => $storyDisLikeCount, 'storyLikeCount' => $storyLikeCount, 'isDisLikedByUser' => $isDisLikedByUser, 'isLikedByUser' => $isLikedByUser, 'story' => $story, 'stories' => $stories, 'recommended' => $recommended]);
        } else {
            
            $story = Story::where('slug', $slug)
               
                ->orderBy('created_at', 'desc')
                ->with([
                    'user.artist'=> function ($user) {
                     $user->where('isDeleted',0);
                    }
                ])
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->first();

            $pageTitle = $story->title;
            $tagsArray = explode(',', $story->tags);
            $search = $tagsArray[0];

            $storyRating = $story->avgRating;
            $userStoryRating = DB::table('ratings')->where(['author_id' => Session::get('user_id'), 'ratingable_id' => $story->id])->value('rating');

            $categories = explode(',', $story->category);
            $sql_category = "(";
            foreach( $categories as $key => $category){
                if( $key == 0 ){
                    $sql_category .= "$category IN (category)";
                } else {
                    $sql_category .= " || $category IN (category)";
                }
            }
            $sql_category .= ")";
            $recommended = Story::whereRaw($sql_category)
                ->whereRaw("id NOT IN (SELECT post_id from promote_posts WHERE post_type='story' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
                ->limit(10)
                ->get();
            $story_categories = Category::whereIn('id', $categories)->get();

            $storyLikeCount = StoryLikeDislike::where(['story_id' => $story->id, 'liked' => 1])->count();
            $storyDisLikeCount = StoryLikeDislike::where(['story_id' => $story->id, 'disliked' => 1])->count();

            $followers = DB::table('followers')->where('follows_id', '=', $story->user->id)->count();

            $stories = Story::
                orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                      
                    },
                    'user.artist'=> function ($user) {
                     $user->where('isDeleted',0);
                    }
                ])
                ->whereRaw("find_in_set('" . $search . "',tags)")
                ->inRandomOrder()
                ->paginate(4);
            return view('story.story_details', ['title' => $pageTitle, 'userStoryRating' => $userStoryRating, 'storyRating' => $storyRating, 'user' => $user, 'story_categories' => $story_categories, 'loggedInUser' => Session::get('user_id'), 'followers' => $followers, 'storyDisLikeCount' => $storyDisLikeCount, 'storyLikeCount' => $storyLikeCount, 'story' => $story, 'stories' => $stories, 'recommended' => $recommended]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function editstory(Story $story, $mystories, $id)
    {
        $pageTitle = "Edit Story";
        $input = Input::all();
        $videoCategories = Category::getCategoryList();
        if (!empty($input)) {

            $rules = array(
                'title' => 'required|max:100',
                'about' => 'required',
                'tags' => 'required',

            );
            $validator = Validator::make($input, $rules);
            if ($validator->fails()) {
                return Redirect::to('/story.edit_story')->withErrors($validator)->withInput();
            } else {

                $storyData = Story::where(['id' => $id])->first();
                $storyData['title'] = $input['title'];
                $storyData['about'] = $input['about'];
                $storyData['tags'] = $input['tags'];
                $storyData['category'] = implode(',', $input['categories']);
                // $storyData['thumb_image'] = $input['thumb_image'];
                // 	$storyData['cover_image'] = $input['cover_image'];
                $storyData['language'] = $input['language'];

                if (!empty($input['covers'])) {
                    $total = count($input['covers']);
                    foreach ($input['covers'] as $k => $thumbImage) {
                        $thumbTempPath = TEMP_UPLOAD_PATH . $thumbImage;
                        $thumbDestinationPath = STORY_COVER_UPLOAD_PATH . $thumbImage;
                        if (!empty($thumbImage) && file_exists($thumbTempPath)) {
                            \File::copy($thumbTempPath, $thumbDestinationPath);
                            \File::delete($thumbTempPath);
                            $storyData['cover_image'] = $thumbImage;
                        }

                    }
                }

                if (!empty($input['thumbs'])) {
                    $total = count($input['thumbs']);
                    foreach ($input['thumbs'] as $k => $thumbImage) {
                        $thumbTempPath = TEMP_UPLOAD_PATH . $thumbImage;
                        $thumbDestinationPath = STORY_THUMB_UPLOAD_PATH . $thumbImage;
                        if (!empty($thumbImage) && file_exists($thumbTempPath)) {
                            \File::copy($thumbTempPath, $thumbDestinationPath);
                            \File::delete($thumbTempPath);
                            $storyData['thumb_image'] = $thumbImage;
                        }
                    }
                }


                $storyData->save();
                $storyData = $storyData->id;


                return Redirect::to('/mystories/' . Session::get('slug') . '?status=updated');
            }

        } else if (!empty($id)) {
            $user = User::where(['id' => Session::get('user_id')])->first();
            $story = Story::where('id', $id)
                ->select('id', 'title', 'about', 'tags', 'cover_image', 'thumb_image', 'language', 'category', 'user_id', 'created_at')
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
                    }
                ])
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->first();

            $storyRating = $story->avgRating;
            $userStoryRating = DB::table('ratings')->where(['author_id' => Session::get('user_id'), 'ratingable_id' => $story->id])->value('rating');

            $categories = explode(',', $story->category);
            $story_categories = Category::whereIn('id', $categories)->get();

            $storyLikeCount = StoryLikeDislike::where(['story_id' => $story->id, 'liked' => 1])->count();
            $storyDisLikeCount = StoryLikeDislike::where(['story_id' => $story->id, 'disliked' => 1])->count();

            $isLikedByUser = StoryLikeDislike::where(['user_id' => Session::get('user_id'), 'story_id' => $story->id, 'liked' => 1])->count() > 0 ? 1 : 0;
            $isDisLikedByUser = StoryLikeDislike::where(['user_id' => Session::get('user_id'), 'story_id' => $story->id, 'disliked' => 1])->count() > 0 ? 1 : 0;

            $followers = DB::table('followers')->where('follows_id', '=', Session::get('user_id'))->count();


            $storystatus = StoryViews::where(['ip' => \Request::getClientIp(), 'story_id' => $story->id])->first();
            if ($storystatus === null) {
                $storyview = new StoryViews();
                $storyview->story_id = $story->id;
                $storyview->title = $story->title;
                $storyview->url = \Request::url();
                $storyview->session_id = \Request::getSession()->getId();
                $storyview->user_id = Session::get('user_id');
                $storyview->ip = \Request::getClientIp();
                $storyview->agent = \Request::header('User-Agent');
                $storyview->save();
            } else {
                StoryViews::where(['ip' => \Request::getClientIp(), 'story_id' => $story->id])->update(['updated_at' => DB::raw('NOW()')]);
            }


            $stories = Story::select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug')
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
                    }
                ])
                ->paginate(4);
            return view('story.edit_story', ['title' => $pageTitle, 'userStoryRating' => $userStoryRating, 'storyRating' => $storyRating, 'user' => $user, 'story_categories' => $story_categories, 'loggedInUser' => Session::get('user_id'), 'followers' => $followers, 'storyDisLikeCount' => $storyDisLikeCount, 'storyLikeCount' => $storyLikeCount, 'story' => $story, 'stories' => $stories, 'videoCategories' => $videoCategories]);


        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = Input::all();
        if (!empty($input)) {
            $rules = array(
                'title' => 'required|max:100',
                'about' => 'required',
                'tags' => 'required',
            );

			$validator = Validator::make($input, $rules);

			if ($validator->fails()) {
				return Redirect::back()->withErrors($validator)->withInput();
            } else {
                $data = array(
                    'title' => $input['title'],
                    'tags' => $input['tags'],
                    'language' => $input['language'],
                    'about' => $input['about'],
                    'category' => implode(',', $input['categories']),
                    'user_id' => $request->session()->get('user_id'),
                );

                if (!empty($input['cover_image'])) {
                    $data['cover_image'] = $input['cover_image'];
                }

                if (!empty($input['thumb'])) {
                    $data['thumb_image'] = $input['thumb'];
                }

                if( !empty($input['title']) ){
                    $data['slug'] = Str::slug($input['title'])."-".$id;
                }

                Story::where(['id' => $id, 'user_id' => $request->session()->get('user_id')])->update($data);
                return redirect()->back()->with('success', 'Your story is updated');
            }
            return redirect()->back()->withErrors($validator)->withInput();
        }

        return abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function destroy(Story $story)
    {
        //
    }

    public function likedVideo($id = null)
    {
        $pageTitle = "Liked Video";
        if (!empty($id)) {
            $storyData = Story::where(['id' => $id])
                ->first();
            $user = User::find(Session::get('user_id'));
            $other_user = User::find($storyData->user_id);
            $viewsLikesstatus = StoryLikeDislike::where(['user_id' => Session::get('user_id'), 'story_id' => $storyData->id])->first();
            if ($viewsLikesstatus === null) {
                $videoLikes = new StoryLikeDislike();
                $videoLikes->story_id = $storyData->id;
                $videoLikes->title = $storyData->title;
                $videoLikes->url = \Request::url();
                $videoLikes->session_id = \Request::getSession()->getId();
                $videoLikes->user_id = Session::get('user_id');
                $videoLikes->ip = \Request::getClientIp();
                $videoLikes->liked = true;
                $videoLikes->disliked = false;
                $videoLikes->save();
                if( !(is_null($other_user)) ){
    				$other_user->notify(new LikedStory($user, $storyData));
                }
            } else {
                StoryLikeDislike::where(['user_id' => Session::get('user_id'), 'story_id' => $storyData->id])->update(['updated_at' => DB::raw('NOW()'), 'disliked' => false, 'liked' => true]);

                if($viewsLikesstatus->liked != 1){
                    if( !(is_null($other_user)) ){
        				$other_user->notify(new LikedStory($user, $storyData));
                    }
                }
            }
        }

        $likeDislike = array(
            'likeCount' => StoryLikeDislike::where(['story_id' => $storyData->id, 'liked' => true])->count(),
            'disLikeCount' => StoryLikeDislike::where(['story_id' => $storyData->id, 'disliked' => true])->count()
        );
        return $likeDislike;
    }

    /*Video DisLiked*/
    public function dislikedVideo($id = null)
    {
        if (!empty($id)) {
            $storyData = Story::where(['id' => $id])
                ->first();
            $viewsLikesstatus = StoryLikeDislike::where(['user_id' => Session::get('user_id'), 'story_id' => $storyData->id])->first();
            if ($viewsLikesstatus === null) {
                $videoLikes = new StoryLikeDislike();
                $videoLikes->story_id = $storyData->id;
                $videoLikes->title = $storyData->title;
                $videoLikes->url = \Request::url();
                $videoLikes->session_id = \Request::getSession()->getId();
                $videoLikes->user_id = Session::get('user_id');
                $videoLikes->ip = \Request::getClientIp();
                $videoLikes->liked = false;
                $videoLikes->disliked = true;
                $videoLikes->save();
            } else {
                StoryLikeDislike::where(['user_id' => Session::get('user_id'), 'story_id' => $storyData->id])->update(['updated_at' => DB::raw('NOW()'), 'disliked' => true, 'liked' => false]);
            }

            $user = User::find($storyData->user_id);
            if( $user && Session::get('user_id') ){
                $user->notify(new DislikedStory(User::find(Session::get('user_id')), $storyData));
            }
        }

        $likeDislike = array(
            'likeCount' => StoryLikeDislike::where(['story_id' => $storyData->id, 'liked' => true])->count(),
            'disLikeCount' => StoryLikeDislike::where(['story_id' => $storyData->id, 'disliked' => true])->count()
        );
        return $likeDislike;
    }

    public function mystories($slug = null)
    {
        if (!$slug) {
            $slug = session()->get('slug');

            if (!($slug)) {
                return redirect('/login');
            }
        }
        return redirect('/' . $slug . '/stories');
    }

    public function showStories(Request $request)
    {
        $user = User::where('slug', $request->slug)->first();
        $pageTitle = 'Stories';
        $userId = $user->id;
        $stories = Story::where(['user_id' => $userId])
            ->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug')
            ->orderBy('created_at', 'desc')
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
                }
            ])
            ->paginate(10);
        $user = User::where(['id' => $userId])->first();
        $followers = DB::table('followers')->where('follows_id', '=', $userId)->count();
        
        return view('story.mystories', ['title' => $pageTitle, 'followers' => $followers, 'stories' => $stories, 'user' => $user, 'topMedia' => (new UsersController())->topMedia($userId)]);
    }


    public function searchstory(Request $request)
    {
        $pageTitle = 'Stories';

        if (!empty(Session::get('user_id'))) {
            $stories = Story::where('title', 'like', '%' . $request->searchStory . '%')
                ->orWhere('tags', 'like', '%' . $request->searchStory . '%')
                ->orWhere('language', 'like', '%' . $request->searchStory . '%')
                ->orWhere('about', 'like', '%' . $request->searchStory . '%')
                ->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug')
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
                    }
                ])
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->paginate(10);

        } else {
            $stories = Story::where('title', 'like', '%' . $request->searchStory . '%')
                ->orWhere('tags', 'like', '%' . $request->searchStory . '%')
                ->orWhere('language', 'like', '%' . $request->searchStory . '%')
                ->orWhere('about', 'like', '%' . $request->searchStory . '%')
                ->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug')

                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
                    }
                ])
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->paginate(10);

        }

        $story_categories = Category::getCategoryList();
        return view('story.index', ['title' => $pageTitle, 'stories' => $stories, 'category' => $request->searchStory, 'story_categories' => $story_categories]);
    }

    public function storyByCategory($category = null, Request $request)
    {
        $pageTitle = 'Stories';
        $categories = Category::getCategoryList();
        $catID = Category::select('id')->where('category', 'like', '%' . $category . '%')->get();

        if (!empty(Session::get('user_id'))) {
            // $stories =  Story::where('category', 'like', '%'.$catID[0]->id.'%')
            $stories = Story::where(['category' => $catID[0]->id])
                ->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug')
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram')->where('isDeleted',0);
                    }
                ])
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->paginate(4);

        } else {
            $stories = Story::where(['category' => $catID[0]->id])
                ->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug')
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram')->where('isDeleted',0);
                    }
                ])
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->paginate(4);
        }

        if ($request->ajax()) {
            $view = view('elements.story.cards', compact('categories', 'stories'))->render();
            return response()->json(['html' => $view]);
        }

        return view('story.story_by_category', ['title' => $pageTitle, 'stories' => $stories, 'categories' => $categories]);
    }

    public function rateStory($id, $userrating)
    {
        $user = User::where(['id' => Session::get('user_id')])->first();
        $story = Story::where('id', $id)->first();
        $rating = $story->ratingUnique([
            'rating' => $userrating
        ], $user);

        $storyRating = $story->avgRating;
        $view = view('elements.story.rating', compact('storyRating'))->render();
        $other_user = User::find($story->user_id);
        
        if( !(is_null($other_user)) ){
            $other_user->notify(new Notices($user, 'Rated your story', 'story-rating', '', $userrating, Session::get('user_id') ?? '', $story->id));
        }

        return response()->json(['html' => $view, 'rating' => $userrating]);
    }

    public function storyByAll(Request $request)
    {

        $pageTitle = 'Stories';
        $categories = Category::getCategoryList();
        if (!empty(Session::get('user_id'))) {
            $stories = Story::select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug')
                ->where('isDeleted',0)
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram')->where('isDeleted',0);
                    }
                ])
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->get();

        } else {
            $stories = Story::select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug')
                 ->where('isDeleted',0)
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram')->where('isDeleted',0);
                    }
                ])
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->get();
        }

        return view('story.story_by_category', ['title' => $pageTitle, 'stories' => $stories, 'categories' => $categories]);
    }

    public function mostPopularStories()
    {
        $pageTitle = 'Most Popular Stories';

        $mostViewedStory = Story::select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug')
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
                }
            ])
            ->withCount(['totalViews'])
            ->withCount(['totalComments'])
            ->orderBy('total_views_count', 'desc')
            ->paginate(20);

        $story_categories = Category::getCategoryList();
        return view('story.most-popular-stories', [
            'title' => $pageTitle,
            'mostViewedStory' => $mostViewedStory,
            'story_categories' => $story_categories
        ]);
    }
}