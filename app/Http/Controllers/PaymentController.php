<?php

namespace App\Http\Controllers;

use App\Models\Transactions;
use App\Models\User;
use App\Models\Wallet;
use App\Notifications\Notices;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    private $amount = 0;
    private $type = '';
    private $currency = 'INR';
    private $source = '';
    private $details = '';
    private $status = 0;
    private $txnId = '';
    private $relatedId = 0;
    public $txnTypes = ['razorpay'];
    private $api = null;
    private $attributes = [];

    public function __construct($txnType = null)
    {
        if( !in_array($txnType, $this->txnTypes) ){
            return ['error' => 'Unknown transaction type.'];
        }
        
        $this->source = $txnType;
    }

    public function load($attributes = []){
        $this->attributes = $attributes;

        switch($this->source){
            case 'razorpay':
                $this->api = new RazorPayController($this->attributes);
            break;

            default:
                return ['error' => 'Unknown source'];
        }
    }

    public function createOrder($receipt, $amount, $notes = []){
        return $this->api->createOrder($receipt, $amount, $notes);
    }

    public function capturePayment(){
        $payment = $this->api->capturePayment();

        if( is_array($payment) && isset($payment['error']) ){
            return $payment;
        }

        if( !$payment['id'] ){
            return ['error' => "Unable to capture payment"];
        }

        if( $payment['status'] != 'captured' ){
            return ['error' => 'Payment is not captured'];
        }

        $user = User::find(session()->get('user_id'));
        if( $user ){
            if( $this->source == 'razorpay' ){
                $payment['amount'] = $payment['amount']/100;
                $this->setType($payment['notes']['type']);
                $this->setTxnId($payment['id']);
                
                if( $payment['notes']['type'] == 'theme-plan' ){
                    $user->purchase = 1;
                    $user->plan_term = 12;
                    $user->plan_started_at = Carbon::now()->startOfDay()->timestamp;
                    $user->plan_expired_at = Carbon::now()->addYear()->subSecond()->timestamp;
                    $user->plan = 2;
                    $user->save();

                    $this->attributes['notice.message'] = 'Purchased theme plan successfully';
                }
                elseif( $payment['notes']['type'] == 'premium-profile' ){
                    if( $payment['notes']['setting'] == 'gold' || $payment['notes']['setting'] == 'silver' ){
                        $this->setType($payment['notes']['type'].".".$payment['notes']['setting']);
                        $user->is_premium = 1;
                        $user->premium_started_at = Carbon::now()->startOfDay()->timestamp;
                        
                        if( $payment['notes']['setting'] == 'gold' ){
                            $user->premium_plan_type = 2;
                            $user->premium_ended_at = Carbon::now()->addMonths(6)->subSecond()->timestamp;
                            $user->premium_term = 6;
                        } else {
                            $user->premium_plan_type = 1;
                            $user->premium_ended_at = Carbon::now()->addMonth(1)->subSecond()->timestamp;
                            $user->premium_term = 1;
                        }
                        $user->save();
                        $this->attributes['notice.message'] = "Purchased premium profile {$payment['notes']['setting']} plan successfully";
                        
                        send_mail('profile-boost', $user->email, ['template' => 'email.boost-profile', 'subject' => 'Profile Boosted']);
                    } else {
                        return ['error' => 'Invalid profile plan'];
                    }
                } elseif( $payment['notes']['type'] == 'promote-post' ){
                    $this->attributes['notice.message'] = 'Congratulation your post is active and ready for boost !!!';
                } elseif( $payment['notes']['type'] == 'ads-campaign' ){
                    $this->attributes['notice.message'] = 'Congratulation your ad campaign is active !!!';
                } else {
                    return ['error' => 'Invalid payment type'];
                }
            } else {
                return ['error' => 'Invalid payment source'];
            }
            
            $this->setStatus(1);
            $this->setAmount($payment['amount']);
            $this->setDetails($payment);
            $res = $this->store($user->id);
            
            send_mail('payment', $user->email, ['template' => 'email.payment', 'subject' => 'Payment Successfull']);
            return array_merge($res, ['user' => $user, 'payment' => $payment, 'message' => $this->attributes['notice.message']]);
        }

        return ['error' => "Unknown user"];
    }

    //
    public function store($userId, $shouldEffectWallet = false){
        $transaction = new Transactions();
        $transaction->user_id = $userId;
        $transaction->type = $this->type;
        $transaction->txn_id = $this->txnId;
        $transaction->related_id = $this->relatedId;
        $transaction->amount = $this->amount;
        $transaction->currency = $this->currency;
        $transaction->source = $this->source;
        $transaction->details = $this->details;
        $transaction->status = $this->status;
        $transaction->save();

        $wallet = null;
        if( $shouldEffectWallet ){
            $wallet = Wallet::where([['user_id', $userId], ['status', '1']])->first();
    
            if( !$wallet ){
                $wallet = new Wallet();
                $wallet->user_id = $userId;
                $wallet->currency = $this->currency;
                $wallet->status = 1;
                $wallet->balance = $this->amount;
            } else {
                $wallet->balance = $wallet->balance + $this->amount;
            }

            $wallet->save();
        }

        return ['transaction' => $transaction, 'wallet' => $wallet];
    }

    public function updateStatus($transactionId, $userId){
        $transaction = Transactions::find($transactionId);

        if( is_null($transaction) ){
            return false;
        }

        if( $userId && $transaction->user_id != $userId ){
            return false;
        }

        $transaction->status = $this->status;
        $transaction->save();

        // if status is set to 1 then add amount to wallet
        // if status is set to 0 then deduct amount from wallet
    }

    public function setRelatedId($related_id){
        $this->relatedId = $related_id;
    }

    public function getRelatedId(){
        return $this->relatedId;
    }

    public function setRequest(array $attributes){
        $this->attributes = $attributes;
    }

    public function getTxnId(){
        return $this->txnId;
    }

    public function setTxnId($txn_id){
        $this->txnId = $txn_id;
    }

    public function getType(){
        return $this->type;
    }

    public function setType($type){
        $this->type = $type;
    }

    public function getCurrency(){
        return $this->currency;
    }

    public function setCurrency($currency){
        $this->currency = $currency;
    }

    public function getAmount(){
        return $this->amount;
    }

    public function setAmount($amount){
        $this->amount = $amount;
    }

    public function getSource(){
        return $this->source;
    }

    public function setSource($source){
        $this->source = $source;
    }

    public function getDetails(){
        return json_decode($this->details);
    }

    public function setDetails($details){
        $this->details = json_encode(utf8ize($details));
    }

    public function getStatus(){
        return $this->status;
    }

    public function setStatus($status){
        $this->status = $status;
    }
}