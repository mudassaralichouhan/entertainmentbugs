<?php

namespace App\Http\Controllers;

use App\Models\ArtistAbout;
use App\Models\ArtistBrandCollaboration;
use App\Models\ArtistGallery;
use App\Models\ArtistQuote;
use App\Models\ArtistVideo;
use App\Models\BrandProducts;
use Artist;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Session;

class ArtistController extends Controller
{
    public function video($slug, $_slug){
		
        if( !(empty($slug)) ){
            $video = ArtistVideo::selectRaw("artist_video.*, artist_about.*, users.id AS userid")
                ->join("users", "users.id", "=", "artist_video.user_id")
                ->leftJoin("artist_about", "users.id", "=", "artist_about.user_id")
                ->where([["artist_about.username", $slug], ["artist_video.slug", $_slug]])
                ->first();
                
            if( $video ){
                $title = $video->video_title;
                $videos = ArtistVideo::where([["id", "<>", $video->id], ['user_id', $video->userid]])
                    ->limit(5)
                    ->get();
    
                $latestVideos = ArtistVideo::orderByDesc("updated_at")
                    ->limit(12)
                    ->get();
    
                $similarCategoryProfiles = ArtistAbout::whereRaw("'$video->category' IN (IFNULL(`multiple_category`, ''))")
                    ->orderByDesc("id")
                    ->limit(7)
                    ->get();
                    $latestPhotos = ArtistGallery::orderByDesc("id")
                    ->limit(12)
                    ->get();
                return view("artist.video", compact('video', 'title', 'slug', 'videos', 'latestPhotos','latestVideos', 'similarCategoryProfiles'));
            }
        }

            

        return abort(404);
    }

    public function gallery($slug, $_slug){
        if( !empty($slug) ){
            $gallery = ArtistGallery::selectRaw("artist_gallery.*, artist_about.*, users.id AS userid")
                ->join("users", "users.id", "=", "artist_gallery.user_id")
                ->leftJoin("artist_about", "users.id", "=", "artist_about.user_id")
                ->where([["artist_about.username", $slug], ["artist_gallery.slug", $_slug]])
                ->first();
    
                
            if( $gallery ){
                $title = $gallery->photo_title;
                $photos = ArtistGallery::where([["id", "<>", $_slug], ['user_id', $gallery->userid]])
                    ->limit(5)
                    ->get();
    
                $latestPhotos = ArtistGallery::orderByDesc("id")
                    ->limit(12)
                    ->get();
                $latestVideos = ArtistVideo::orderByDesc("updated_at")
                    ->limit(12)
                    ->get();
                $similarCategoryProfiles = ArtistAbout::whereRaw("'$gallery->category' IN (IFNULL(`multiple_category`, ''))")
                    ->orderByDesc("id")
                    ->limit(7)
                    ->get();
    
                return view("artist.gallery", compact('gallery', 'title', 'slug', 'photos', 'latestVideos','latestPhotos', 'similarCategoryProfiles'));
            }
        }

        return abort(404);
    }

    public function saveBrandCollaboration(Request $request){
        $brands = $request->get('brand');
        if( !$brands || !is_array($brands) || count($brands) < 1 ){
            return redirect()->back()->withErrors(['Unknown Brands']);
        }

        $timestamp = Carbon::now()->format("Y-m-d H:i:s");
        $productsToAdd = [];

        $brandModels = ArtistBrandCollaboration::select("id")->where(['user_id' => Session::get('user_id')])->get();

        if( $brandModels && count($brandModels) > 0 ){
            foreach($brandModels as $key => $brandModel){
                $brand = ArtistBrandCollaboration::find($brandModel->id);
                $brand->brand_name     = $brands[$key]['name'] ?? '';
                $brand->brand_logo     = $brands[$key]['logo'] ?? '';
                $brand->slug           = Str::slug('name')."-".$brandModel->id;
                $brand->updated_at     = $timestamp;
                $brand->save();

                if( isset($brands[$key]['product']) && is_array($brands[$key]['product']) ){
                    $products = $brands[$key]['product'];
                    
                    $productModels = BrandProducts::where(['user_id' => Session::get('user_id'), 'relation_id' => $brandModel->id, 'type' => 'artist-brand-collaboration'])->get();
                    foreach($productModels as $k => $productModel){
                        $p = $products[$k];
                        $product = BrandProducts::find($productModel->id);
                        $product->relation_id   = $brandModel->id;
                        $product->title         = $p['title'];
                        $product->button_label  = strtolower($p['button_text']) == 'select' ? '' : $p['button_text'];
                        $product->url           = $p['button_link'];
                        $product->banner        = $p['logo'];
                        $product->slug          = Str::slug($p['title']).'-'.$productModel->id;
                        $product->active        = $p['active'] ?? 0;
                        $product->updated_at    = $timestamp;
                        $product->save();
                    }
                }
            }
        } else {
            $brandIds = [];
            
            foreach( $brands as $brand ){
                $brandModel = new ArtistBrandCollaboration();
                $brandModel->brand_name    = $brand['name'] ?? '';
                $brandModel->brand_logo    = $brand['logo'] ?? '';
                $brandModel->user_id       = Session::get('user_id');
                $brandModel->slug          = Str::slug('name');
                $brandModel->created_at    = $timestamp;
                $brandModel->save();
                
                $brand_id = $brandModel->id;
                $brandIds[] = $brand_id;

                if( isset($brand['product']) && is_array($brand['product']) ){
                    $products = $brand['product'];
                    foreach($products as $product){
                        array_push($productsToAdd, [
                            'type'          => 'artist-brand-collaboration',
                            'relation_id'   => $brand_id,
                            'title'         => $product['title'] ?? '',
                            'button_label'  => strtolower($product['button_text']) == 'select' ? '' : $product['button_text'],
                            'url'           => $product['button_link'] ?? '',
                            'banner'        => $product['logo'] ?? '',
                            'user_id'       => Session::get('user_id'),
                            'slug'          => Str::slug('title'),
                            'active'        => $product['active'] ?? 0,
                            'created_at'    => $timestamp
                        ]);
                    }
                }
            }
    
            if( count($productsToAdd) > 0 ){
                BrandProducts::insert($productsToAdd);
            }

            if( count($brandIds) > 0 ){
                ArtistBrandCollaboration::whereIn('id', $brandIds)->update(['slug' => DB::raw("CONCAT(`slug`, '-', `id`)")]);
                BrandProducts::whereIn('relation_id', $brandIds)->update(['slug' => DB::raw("CONCAT(`slug`, '-', `id`)")]);
            }
        }
        
        return response()->json(['status' => '1']);
    }
    
    public function saveQuote(Request $request){
        $userId = Session::get('user_id');
        $quote = ArtistQuote::find($userId);
        $timestamp = Carbon::now()->format('Y-m-d H:i:s');

        if( !$quote ){
            $quote = new ArtistQuote();
            $quote->user_id = $userId;
            $quote->created_at = $timestamp;
        } else {
            $quote->updated_at = $timestamp;
        }

        $quote->name = $request->get('author_name') ?? '';
        $quote->quote = $request->get('author_quote') ?? '';
        $quote->save();
        
        return response()->json(['status' => '1']);
    }
}