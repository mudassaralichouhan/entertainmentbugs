<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Complain as compaints;
class Complain extends Controller
{
   public function index(){}
   
   public function save(Request $request){
     
         $this->validate($request,[
            "name" => "required",
            "phone" => "required",
            "email" => "required|email",
            "subject" => "required",
            "link" => "required",
            "message" => "required",
        ]);
        $input = $request->all();
       if($request->hasFile('file')){
           $filename = $request->file('file')->getClientOriginalName();
            $request->file('file')->move(public_path('img/compain/'), $filename);
            $input['file'] = $filename;
       }
       $input['user_id'] = session()->get('user_id');
       if(compaints::create($input)){
           return redirect()->back()->with('success','Request Sent Successfully!');
       }else{
           return redirect()->back()->with('error','Operation Failed!');
       }
   }
}
