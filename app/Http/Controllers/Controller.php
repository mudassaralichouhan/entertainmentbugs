<?php

namespace App\Http\Controllers;

use App\Helpers\UserHelper;
use App\Models\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Image;
use DB;
use Illuminate\Support\Facades\Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected static $currentUser = null;
    
    public function encpassword($passwordPlain = 0) {
       return password_hash($passwordPlain, PASSWORD_DEFAULT);
    }
    
    public function serialiseFormData($data=array(), $isEdit=0){
        $formData = array();
        unset($data['_token']);
        unset($data['_method']);
        unset($data['confirm_password']);
        if($isEdit == 0){
            $data['created_at'] = date('Y-m-d H:i:s');
        }
        $data['updated_at'] = date('Y-m-d H:i:s');
        return $data;
    }

    public function uploadImageWithSameName($file, $upload_path=null){
        $orgName = $file->getClientOriginalName();
        $newFileName = $orgName;
        $file->move($upload_path, $newFileName);
        return $newFileName;
    }
    public function uploadImage($file, $upload_path=null){
        $orgName = $file->getClientOriginalName();
        $newFileName = bin2hex(openssl_random_pseudo_bytes(4)).'_'.$orgName;
        $file->move($upload_path, $newFileName);
        return $newFileName;
    }
    public function resizeImage($newFileName, $from_path=null, $to_path, $max_width=null, $max_height=null){
        list($width, $height)  = getimagesize($from_path . $newFileName);
        $image = Image::make($from_path . $newFileName);
        if($width > $height){
            if($max_width < $width){
                $image->resize($max_width, null, function ($constraint) { $constraint->aspectRatio();});
            }            
        }else{
            if($max_height < $height){
                $image->resize(null, $max_height, function ($constraint) { $constraint->aspectRatio();});
            } 
        }
        $image->save($to_path.$newFileName);
        return;
    }
    
    public function createSlug($slug=null,$tablename=null, $fieldname='slug'){
        $slug = filter_var($slug, FILTER_SANITIZE_STRING);
        $slug = str_replace(' ', '-', strtolower($slug));
        $isSlugExist = DB::table($tablename)->where($fieldname,$slug)->first();               
        if (!empty($isSlugExist)) {
            $slug = $slug.'-'.bin2hex(openssl_random_pseudo_bytes(6));
            $this->createSlug($slug, $tablename, $fieldname);
        }
        return $slug;
    }
    
    public function getRandString($length) {
        $length = ceil($length/2);
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
    
    public function getSiteSetting() {   
        return mysqli_query(mysqli_connect('localhost', env('DB_USERNAME'), env('DB_PASSWORD'), env('DB_DATABASE')), 'SELECT * FROM tbl_settings WHERE id=1')->fetch_object();
    }

    public function getCurrentUser() {
        return UserHelper::getAuthUser();
    }
}