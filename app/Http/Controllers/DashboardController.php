<?php

namespace App\Http\Controllers;

use App\Helpers\BankHelper;
use App\Helpers\PhotoHelper;
use App\Helpers\StoryHelper;
use App\Helpers\TransactionHelper;
use App\Helpers\VideoHelper;
use App\Models\Story;
use App\Models\Transactions;
use App\Models\User;
use App\Models\Video;
use App\Models\Wallet;
use App\PageViewTrack;
use App\UploadPhotos;
use App\WatchTrack;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $pageTitle = 'Dashboard';
        $user = $this->getCurrentUser();
        $photos = new UploadPhotos();
        $stories = new Story();

        $videosTotalEarning = Video::where([['videos.user_id', $user->id], ['videos.isRewardVideo', 0]])
                ->join("watch_video_earning", "watch_video_earning.video_id", "=", "videos.id")
                ->sum("watch_video_earning.total_watch_time") * VideoHelper::EARNING_PER_SECOND;

        $photosTotalEarning = $photos
                ->getTotalReaction(null, $user->id) * PhotoHelper::EARNING_PER_REACTION;
        $storiesTotalEarning = $stories
                ->getTotalReaction(null, $user->id) * StoryHelper::EARNING_PER_REACTION;
        $totalRewardVideosEarning = Video::where([['videos.user_id', $user->id], ['videos.isRewardVideo', 1]])
                ->join("watch_video_earning", "watch_video_earning.video_id", "=", "videos.id")
                ->sum("watch_video_earning.total_watch_time") * VideoHelper::EARNING_PER_SECOND;

        $totalEarning = $videosTotalEarning + $photosTotalEarning + $storiesTotalEarning + $totalRewardVideosEarning;

        $lastThreeWeekVisitors = $this
            ->getVisitors($user, Carbon::now()
                ->subWeek(3)
                ->addDay(1)
                ->format("Y-m-d 00:00:00"));

        $lastSixMonthVisitors = $this
            ->getVisitors($user, Carbon::now()
                ->subMonths(6)
                ->addDay(1)
                ->format("Y-m-d 00:00:00"));

        $videosLastThirtyDaysEarning = Video::where([['videos.user_id', $user->id], ['videos.isRewardVideo', 0], ['watch_video_earning.created_at', '>=', Carbon::now()
                ->subDay(30)
                ->format("Y-m-d 00:00:00")]])
                ->join("watch_video_earning", "watch_video_earning.video_id", "=", "videos.id")
                ->sum("watch_video_earning.total_watch_time") * 0.01;

        $rewardVideoLastThirtyDaysEarning = Video::where([['videos.user_id', $user->id], ['videos.isRewardVideo', 1], ['watch_video_earning.created_at', '>=', Carbon::now()
                ->subDay(30)
                ->format("Y-m-d 00:00:00")]])
                ->join("watch_video_earning", "watch_video_earning.video_id", "=", "videos.id")
                ->sum("watch_video_earning.total_watch_time") * 0.01;

        $photosLastThirtyDaysEarning = $photos
                ->getTotalReaction(Carbon::now()
                    ->subDay(30)
                    ->format("Y-m-d 00:00:00"), $user->id) * 0.01;

        $storiesLastThirtyDaysEarning = $stories
                ->getTotalReaction(Carbon::now()
                    ->subDay(30)
                    ->format("Y-m-d 00:00:00"), $user->id) * 0.01;

        $lastThirtyDaysEarning = $videosLastThirtyDaysEarning + $photosLastThirtyDaysEarning + $storiesLastThirtyDaysEarning + $rewardVideoLastThirtyDaysEarning;

        $totalVideos = Video::where([["user_id", $user->id], ['isRewardVideo', '0']])
            ->count();
        $totalPhotos = UploadPhotos::where("user_id", $user->id)
            ->count();
        $totalStories = Story::where("user_id", $user->id)
            ->count();

        $recentVideos = Video::where([["user_id", $user->id], ['isRewardVideo', '0']])
            ->selectRaw("'video' as tag, title, id, video_key as slug, video_thumb_id as image, created_at")
            ->orderByDesc("id");
        $recentPhotos = UploadPhotos::where([["user_id", $user->id]])
            ->selectRaw("'photo' as tag, photo_title as title, id, id as slug, images as image, created_at")
            ->orderByDesc("id");
        $recentStories = Story::where([["user_id", $user->id]])
            ->selectRaw("'story' as tag, title, id, slug, thumb_image as image, created_at")
            ->orderByDesc("id")
            ->unionAll($recentVideos)
            ->unionAll($recentPhotos);
        $recentPosts = DB::table(DB::raw("({$recentStories->toSql()}) as x"))
            ->setBindings($recentStories->getBindings())
            ->orderByDesc("created_at")
            ->limit(4)
            ->get();

        $totalRewardVideos = Video::where([["user_id", $user->id], ['isRewardVideo', '1']])->count();
        $lastRewardVideoTotalHours = $this->getVideoTotalHours(Video::where('user_id', $user->id)->orderByDesc("id")->first());

        return view('dashboard.dashboard', [
            'title' => $pageTitle,
            'videosTotalEarning' => $videosTotalEarning,
            'totalEarning' => $totalEarning,
            'lastThreeWeekVisitors' => $lastThreeWeekVisitors,
            'lastSixMonthVisitors' => $lastSixMonthVisitors,
            'lastThirtyDaysEarning' => $lastThirtyDaysEarning,
            'totalVideos' => $totalVideos,
            'totalPhotos' => $totalPhotos,
            'totalStories' => $totalStories,
            'recentPosts' => $recentPosts,
            'totalRewardVideos' => $totalRewardVideos,
            'lastRewardVideoTotalHours' => $lastRewardVideoTotalHours, 'totalRewardVideosEarning' => $totalRewardVideosEarning,
            "lastThirtyDaysGraph" => $this->getViewPageGraphData($user, 29),
            "lastSevenDaysRewardVideoGraph" => $this->getRewardVideoGraphData($user, 6),
        ]);
    }

    private function getVisitors($user, $days)
    {
        $url = url("");
        $object = new \stdClass;

        $video = PageViewTrack::selectRaw("COUNT(*) as count, user_id")
            ->where([['created_at', '>=', $days]])
            ->whereIn('url', function ($query) use ($user, $url) {
                $query
                    ->selectRaw("CONCAT('$url/watch/', video_key)")
                    ->where([['user_id', $user->id]])
                    ->from('videos');
            })
            ->groupBy(DB::Raw('user_id, ip'));

        $object->videos = DB::table(DB::raw("({$video->toSql()}) as a"))
            ->select(DB::raw("COUNT(a.count) as views, user_id"))
            ->mergeBindings($video->getQuery())
            ->groupBy("a.user_id");

        $photo = PageViewTrack::selectRaw("COUNT(*) as count, user_id")
            ->where([['created_at', '>=', $days]])
            ->whereIn('url', function ($query) use ($user, $url) {
                $query
                    ->selectRaw("CONCAT('$url/single-photo/', id)")
                    ->where([['user_id', $user->id]])
                    ->from('upload_photos');
            })
            ->groupBy(DB::Raw('user_id, ip'));

        $object->photos = DB::table(DB::raw("({$photo->toSql()}) as b"))
            ->select(DB::raw("COUNT(b.count) as views, user_id"))
            ->mergeBindings($photo->getQuery())
            ->groupBy("b.user_id");

        $story = PageViewTrack::selectRaw("COUNT(*) as count, user_id")
            ->where('created_at', '>=', $days)
            ->whereIn('url', function ($query) use ($user, $url) {
                $query
                    ->selectRaw("CONCAT('$url/story/display/', REPLACE(LOWER(title), ' ', '-'), '-', id)")
                    ->where('user_id', $user->id)
                    ->from('stories');
            })
            ->groupBy(DB::Raw('user_id, ip'));

        $object->stories = DB::table(DB::raw("({$story->toSql()}) as c"))
            ->select(DB::raw("COUNT(c.count) as views, user_id"))
            ->mergeBindings($story->getQuery())
            ->groupBy("c.user_id");

        $others = PageViewTrack::selectRaw("COUNT(*) as count, user_id")
            ->where('created_at', '>=', $days)
            ->whereIn('url', [url("/$user->slug"), url("/myphotos/$user->slug"), url("/$user->slug/stories"), url("/$user->slug/rewards"), url("/artist/$user->slug")])
            ->groupBy(DB::Raw('user_id, ip'));

        $rows = DB::table(DB::raw("({$others->toSql()}) as d"))
            ->select(DB::raw("COUNT(d.count) as count, user_id"))
            ->mergeBindings($others->getQuery())
            ->unionAll($object->videos)
            ->unionAll($object->photos)
            ->unionAll($object->stories)
            ->groupBy("d.user_id");

        $total = DB::table(DB::raw("(SELECT COUNT(*) AS views FROM ({$rows->toSql()}) AS e GROUP BY e.`user_id`) AS f"))
            ->setBindings($rows->getBindings())
            ->count();

        return $total;
    }

    private function getViewPageGraphData($user, $days)
    {
        $days = Carbon::now()->subDay($days)->format("Y-m-d 00:00:00");
        $url = url("");
        $object = new \stdClass;
        $graphObj = array();

        $video = PageViewTrack::selectRaw("DATE(created_at) as day")
            ->where([['created_at', '>=', $days]])
            ->whereIn('url', function ($query) use ($user, $url) {
                $query
                    ->selectRaw("CONCAT('$url/watch/', video_key)")
                    ->where([['user_id', $user->id], ['isRewardVideo', 0]])
                    ->from('videos');
            })
            ->groupBy(DB::Raw('url, user_id, ip, DATE(created_at)'));


        $object->videos = DB::table(DB::raw("({$video->toSql()}) as a"))
            ->select(DB::raw("COUNT(*) as views, a.day"))
            ->mergeBindings($video->getQuery())
            ->groupBy("a.day")
            ->get();

        $photo = PageViewTrack::selectRaw("DATE(created_at) as day")
            ->where([['created_at', '>=', $days]])
            ->whereIn('url', function ($query) use ($user, $url) {
                $query
                    ->selectRaw("CONCAT('$url/single-photo/', id)")
                    ->where([['user_id', $user->id]])
                    ->from('upload_photos');
            })
            ->groupBy(DB::Raw('url, user_id, ip, DATE(created_at)'));

        $object->photos = DB::table(DB::raw("({$photo->toSql()}) as b"))
            ->select(DB::raw("COUNT(*) as views, b.day"))
            ->mergeBindings($photo->getQuery())
            ->groupBy("b.day")
            ->get();

        $story = PageViewTrack::selectRaw("DATE(created_at) as day")
            ->where('created_at', '>=', $days)
            ->whereIn('url', function ($query) use ($user, $url) {
                $query
                    ->selectRaw("CONCAT('$url/story/display/', REPLACE(LOWER(title), ' ', '-'), '-', id)")
                    ->where('user_id', $user->id)
                    ->from('stories');
            })
            ->groupBy(DB::Raw('url, user_id, ip, DATE(created_at)'));

        $object->stories = DB::table(DB::raw("({$story->toSql()}) as c"))
            ->select(DB::raw("COUNT(*) as views, c.day"))
            ->mergeBindings($story->getQuery())
            ->groupBy("c.day")
            ->get();

        $carbon = new Carbon;
        $graphObj['date'] = [];
        $graphObj['videos'] = [
            "label" => "Videos",
            "backgroundColor" => "rgba(32, 212, 137, 1)",
            "borderColor" => "transparent",
            "borderWidth" => 2,
            "borderRadius" => 4,
            "borderSkipped" => false,
            "data" => []
        ];
        $graphObj['photos'] = [
            "label" => "Photos",
            "backgroundColor" => "rgba(32, 212, 137, 0.25)",
            "borderColor" => "transparent",
            "borderWidth" => 2,
            "borderRadius" => 4,
            "borderSkipped" => false,
            "data" => []
        ];
        $graphObj['stories'] = [
            "label" => "Story",
            "backgroundColor" => "rgba(32, 212, 137, 0.25)",
            "borderColor" => "transparent",
            "borderWidth" => 2,
            "borderRadius" => 4,
            "borderSkipped" => false,
            "data" => []
        ];
        $graphObj['total'] = 0;

        for ($i = 0; $i < 30; $i++) {
            $photosViews = 0;
            $storiesViews = 0;
            $videosViews = 0;

            $date = $carbon->parse($days)->addDay($i);
            array_push($graphObj['date'], $date->format("d M"));
            if ($object->photos) {
                if (isset($object->photos[0]) && $object->photos[0]->day == $date->format("Y-m-d")) {
                    $photosViews = $object->photos[0]->views;
                    $object->photos->splice(0, 1);
                }
            }

            if ($object->stories) {
                if (isset($object->stories[0]) && $object->stories[0]->day == $date->format("Y-m-d")) {
                    $storiesViews = $object->stories[0]->views;
                    $object->stories->splice(0, 1);
                }
            }

            if ($object->videos) {
                if (isset($object->videos[0]) && $object->videos[0]->day == $date->format("Y-m-d")) {
                    $videosViews = $object->videos[0]->views;
                    $object->videos->splice(0, 1);
                }
            }

            $graphObj['total'] += $videosViews + $storiesViews + $photosViews;
            array_push($graphObj['videos']['data'], $videosViews);
            array_push($graphObj['stories']['data'], $storiesViews);
            array_push($graphObj['photos']['data'], $photosViews);
        }

        return $graphObj;
    }

    private function getRewardVideoGraphData($user, $days)
    {
        $url = url("");
        $days = Carbon::now()->subDay($days)->format("Y-m-d 00:00:00");
        $rewardVideo = PageViewTrack::selectRaw("DATE(created_at) as day")
            ->where([['created_at', '>=', $days]])
            ->whereIn('url', function ($query) use ($user, $url) {
                $query
                    ->selectRaw("CONCAT('$url/watch-reward/', video_key)")
                    ->where([['user_id', $user->id], ['isRewardVideo', 1]])
                    ->from('videos');
            })
            ->groupBy(DB::Raw('url, user_id, ip, DATE(created_at)'));

        $videos = DB::table(DB::raw("({$rewardVideo->toSql()}) as a"))
            ->select(DB::raw("COUNT(*) as views, a.day"))
            ->mergeBindings($rewardVideo->getQuery())
            ->groupBy("a.day")
            ->get();

        $carbon = new Carbon;
        $graphObj['date'] = [];
        $graphObj['rewardVideos'] = [
            "label" => "Visits",
            "backgroundColor" => "rgba(32, 212, 137, 1)",
            "borderColor" => "transparent",
            "borderWidth" => 2,
            "data" => []
        ];
        $graphObj['total'] = 0;

        for ($i = 0; $i < 7; $i++) {
            $videosViews = 0;

            $date = $carbon->parse($days)->addDay($i);
            array_push($graphObj['date'], $date->format("d M"));
            if ($videos) {
                if (isset($videos[0]) && $videos[0]->day == $date->format("Y-m-d")) {
                    $videosViews = $videos[0]->views;
                    $videos->splice(0, 1);
                }
            }

            $graphObj['total'] += $videosViews;
            array_push($graphObj['rewardVideos']['data'], $videosViews);
        }

        return $graphObj;
    }

    public function getVideoTotalHours($video)
    {
        $allWatchTime = 0;
        if ($video) {
            $watchTrack = $video->watchTrack(true);
            if (is_object($watchTrack) && $watchTrack->count() > 0) {
                $myTrack = array();
                $lastUserId = null;
                foreach ($watchTrack as $track) {
                    if (preg_match("/^\[(.+)\]$/", $track->track)) {
                        $isTrack = false;
                        $sets = json_decode($track->track, true);

                        if (json_last_error() == JSON_ERROR_NONE) {
                            $isTrack = true;
                        }

                        if ($isTrack) {
                            foreach ($sets as $k => $set) {
                                if (isset($myTrack[$k])) {
                                    if ($myTrack[$k] != 1 && $set == 1) {
                                        $myTrack[$k] = 1;
                                    }
                                } else {
                                    array_push($myTrack, $set);
                                }
                            }
                        }
                    }

                    if ($track->user_id != $lastUserId) {
                        $lastUserId = $track->user_id;
                    }

                    WatchTrack::where("id", $track->id)->update(['is_calculated' => '1', 'updated_at' => date("Y-m-d H:i:s")]);
                }

                if (is_array($myTrack) && count($myTrack) > 0) {
                    foreach ($myTrack as $value) {
                        $allWatchTime += $value;
                    }
                }

                if ($allWatchTime > 0) {
                    $allWatchTime = $allWatchTime * 3;
                }
            }
        }

        return $allWatchTime;
    }

    public function totalEarning()
    {
        $pageTitle = 'Total Earning';

        $user = $this->getCurrentUser();

        $videosTotalEarning = Video::where([
                ['videos.user_id', $user->id],
                ['videos.isRewardVideo', 0]
            ])->join("watch_video_earning", "watch_video_earning.video_id", "=", "videos.id")
                ->sum("watch_video_earning.total_watch_time") * 0.01;

        $photosTotalEarning = (new UploadPhotos)->getTotalReaction(null, $user->id) * 0.01;
        $storiesTotalEarning = (new Story)->getTotalReaction(null, $user->id) * 0.01;

        [$toSunday, $fromSunday] = get_second_last_week();
        $rewardVideosTotalEarning = Video::where([
            ['videos.user_id', $user->id],
            ['videos.isRewardVideo', 1],
            ['videos.isDeleted', 0],
            ['videos.created_at', '<=', $toSunday->format("Y-m-d 00:00:00")],
            ['videos.created_at', '>=', $fromSunday->format("Y-m-d 00:00:00")],
        ])->join("watch_video_earning", "watch_video_earning.video_id", "=", "videos.id")
            ->sum("watch_video_earning.total_earning");

        return view('dashboard.total-earning', [
            'title' => $pageTitle,
            "videoTotalEarning" => $videosTotalEarning,
            "photoTotalEarning" => $photosTotalEarning,
            "storiesTotalEarning" => $storiesTotalEarning,
            "rewardVideosTotalEarning" => $rewardVideosTotalEarning
        ]);
    }

    public function paymentWithdraw()
    {
        $pageTitle = 'Payment Withdraw';
        $user = $this->getCurrentUser();

        $videosTotalEarning = Video::where([
                ['videos.user_id', $user->id],
                ['videos.isRewardVideo', 0]
            ])->join("watch_video_earning", "watch_video_earning.video_id", "=", "videos.id")
                ->sum("watch_video_earning.total_watch_time") * 0.01;

        $photosTotalEarning = (new UploadPhotos)->getTotalReaction(null, $user->id) * 0.01;
        $storiesTotalEarning = (new Story)->getTotalReaction(null, $user->id) * 0.01;

        $rewardVideosTotalEarning = Video::where([
                ['videos.user_id', $user->id],
                ['videos.isRewardVideo', 1]
            ])->join("watch_video_earning", "watch_video_earning.video_id", "=", "videos.id")
                ->sum("watch_video_earning.total_watch_time") * 0.01;

        $totalEarning = $videosTotalEarning + $photosTotalEarning + $storiesTotalEarning + $rewardVideosTotalEarning;

        $availablewallet = Wallet::where('user_id', $user->id)->first();
        $withdrawAmount = Transactions::where([
            ['user_id', $user->id],
            ['status', 1],
            ['type', 'like', 'withdraw%']
        ])->sum('amount');

        $availableBalance = $totalEarning - $withdrawAmount;
        if ($availablewallet && $availablewallet->balance) {
            if ($availablewallet->balance != $availableBalance) {
                $availablewallet->update([
                    'balance' => $availableBalance
                ]);
            }
        } else {
            Wallet::create([
                'user_id' => $user->id,
                'balance' => $totalEarning,
                'currency' => 'INR',
                'status' => 0
            ]);
        }

        $user = User::with('paymentmethods.bankdetil.states', 'paymentmethods.paypaldetails')->find($user->id);

        $transactions = Transactions::select('type', 'txn_id', 'type', 'amount', 'currency', 'source', 'status', 'created_at', 'updated_at', 'related_id')
            ->where([
                ['user_id', $user->id],
                ['status', 1]
            ])->paginate(15);

        return view('dashboard.payment-withdraw', [
            'userpaymentdetils' => $user,
            'title' => $pageTitle,
            "availableBalance" => $availableBalance,
            "withdrawAmount" => $withdrawAmount,
            'transactions' => $transactions,
            'banks' => BankHelper::BANKS,
            'isPayoutRequestDay' => TransactionHelper::isPayoutRequestDay(),
            'minPayoutAmount' => TransactionHelper::getMinPayoutAmount(),
            'numericMinPayoutAmount' => TransactionHelper::getMinPayoutAmount(false)
        ]);
    }
}
