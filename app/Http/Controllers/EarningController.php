<?php

namespace App\Http\Controllers;

use App\Helpers\PhotoHelper;
use App\Helpers\StoryHelper;
use App\Helpers\VideoHelper;
use App\Models\Story;
use App\Models\Video;
use App\UploadPhotos;
use App\WatchVideoEarning;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EarningController extends Controller
{
    public function index(Request $request)
    {
        $pageTitle = 'My Earning';
        $user = $this->getCurrentUser();

        if ($request->ajax() && $request->has('action')) {
            if ($request->has('period') && $request->has('id'))
                return $this->getGraphData($request, $user);

            return $this->getActionContent($request->get('action'), $user);
        }

        $followers = DB::table('followers')
            ->where('follows_id', '=', $user->id)
            ->count();

        return view('earnings.index', [
            'title' => $pageTitle,
            'user' => $user,
            'followers' => $followers,
            'videos' => $this->getVideosEarning($user)
        ]);
    }

    public function earningReward()
    {
        $pageTitle = 'Reward Earning';
        $user = $this->getCurrentUser();

        $user = $user ?? $this->getCurrentUser();

        $videos = Video::select([
            'videos.id',
            'videos.video_key',
            'videos.title',
            'videos.video_thumb_id',
            'videos.video_time',
            'videos.user_id',
            'videos.isRewardVideo',
            'videos.created_at',
        ])
            ->where([
                ['videos.user_id', $user->id],
                ['videos.isRewardVideo', 1],
            ])
            ->leftJoin('watch_track', 'videos.id', '=', 'watch_track.video_id')
            ->leftJoin('watch_histories', 'videos.id', '=', 'watch_histories.video_id')

            ->groupBy('videos.id')
            ->orderBy('total_track_sum', 'desc')
            ->selectRaw('COUNT(DISTINCT watch_histories.user_id) AS total_views_count')
            ->selectRaw('(SELECT SUM(wt.track) FROM watch_track wt WHERE wt.video_id = videos.id) AS total_track_sum')
            ->paginate(5);

        if ($videos) {
            foreach ($videos as &$video) {
                if (!$video->track) {
                    $video->track = new WatchVideoEarning();
                }
            }
        }

        return view('earnings.earning-reward', [
            'title' => $pageTitle,
            'user' => $user,
            'videos' => $videos,
        ]);
    }

    public function getGraphData($request, $user = null)
    {
        $user = $user ?? $this->getCurrentUser();

        switch ($request->get('action')) {
            case 'reward-videos':
            case 'videos':
                $video = Video::where([['id', $request->get('id')], ['user_id', $user->id]])->first();

                if ($video) {
                    return response()->json(['html' => $video->getGraphElement($request->get('period'))]);
                }

                return VideoHelper::getGraphElement($request->get('id'), null);
                break;

            case 'photos':
                $photos = UploadPhotos::where([['id', $request->get('id')], ['user_id', $user->id]])->first();
                dd($photos);

                if ($photos) {
                    return response()->json(['html' => $photos->getGraphElement($request->get('period'))]);
                }

                return PhotoHelper::getGraphElement($request->get('id'), null);
                break;

            case 'stories':
                $stories = Story::where([['id', $request->get('id')], ['user_id', $user->id]])->first();

                if ($stories) {
                    return response()->json(['html' => $stories->getGraphElement($request->get('period'))]);
                }

                return StoryHelper::getGraphElement($request->get('id'), null);
                break;

            default:
                return response()->json(['html' => '', 'error' => ['errorCode' => 'invalid-action', 'message' => 'Action is not a valid action.']]);
        }
    }

    public function getActionContent($action, $user)
    {
        switch ($action) {
            case 'reward-videos':
                return response()->json([
                    'html' => view('earnings.layout.videos', [
                        'videos' => $this->getVideosEarning($user, 'reward-videos'),
                        'action' => 'reward-videos'
                    ])->render()]);
                break;

            case 'photos':
                return response()->json([
                    'html' => view('earnings.layout.photos', [
                        'photos' => $this->getPhotosEarning($user)
                    ])->render()
                ]);
                break;

            case 'stories':
                return response()->json(['html' => view('earnings.layout.stories', ['stories' => $this->getStoriesEarning($user)])->render()]);
                break;

            default:
                return response()->json([
                    'html' => view('earnings.layout.videos', [
                        'videos' => $this->getVideosEarning($user),
                        'action' => 'videos'
                    ])->render()]);
        }
    }

    public function getPhotosEarning($user = null)
    {
        $user = $user ?? $this->getCurrentUser();
        return UploadPhotos::where('user_id', $user->id)
            ->withCount(['unpaid_reaction', 'total_reaction', 'get_likes', 'get_dislikes'])
            ->paginate(5);
    }

    public function getVideosEarning($user = null, $action = 'videos')
    {
        $user = $user ?? $this->getCurrentUser();

        $videos = Video::where([
            ['user_id', $user->id],
            ['isRewardVideo', $action == 'reward-videos' ? VideoHelper::REWARD_VIDEOS : VideoHelper::NORMAL_VIDEOS]
        ])
            ->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                },
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo');
                },
                'totalHistory' => function ($totalHistory) {
                    $totalHistory->count();
                },
                'track'
            ])
            ->withCount(['isWatchedVideo', 'totalHistory'])
            ->orderBy('created_at', 'desc')
            ->paginate(5);

        if ($videos) {
            foreach ($videos as &$video) {
                if (!$video->track) {
                    $video->track = new WatchVideoEarning();
                }
            }
        }

        return $videos;
    }

    public function getStoriesEarning($user = null)
    {
        $user = $user ?? $this->getCurrentUser();

        return Story::where('user_id', $user->id)
            ->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug')
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram');
                }
            ])
            ->withCount(['totalViews', 'totalComments', 'story_likes', 'story_dislikes', 'current_reaction', 'total_reaction'])
            ->orderBy('created_at', 'desc')
            ->paginate(5);
    }
}
