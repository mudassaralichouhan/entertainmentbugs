<?php

namespace App\Http\Controllers;

use \App\AdsPresetModel;
use \App\Models\Country;
use App\Models\PromotePostModel;
use \App\Models\PromotePostsTrack;
use \App\Models\States;
use \App\Models\Story;
use \App\Models\Transactions;
use \App\Models\Video;
use \App\Models\User;
use \App\Notifications\Notices;
use \App\UploadPhotos;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Session;
use Validator;

class PromotePostController extends Controller
{
	public $perViewPrice = 0.25;
	public $perClickPrice = 0.5;
	
	public function video($watch_key, $promotedPost = null){
		$title = "Promote Video - Dashboard";
		$post = Video::where($promotedPost ? 'id' : 'video_key', $watch_key)->where('isRewardVideo', 0)->first();

		if( $post ){
			return view('dashboard.promote-post.plan', [
				'title'         => $title,
				'post'          => $post,
				'type'          => 'video',
				'countries'     => Country::get(),
				'promoted'      => $promotedPost
			]);
		}
		
		return abort(404);
	}

	public function photo($id, $promotedPost = null){
		$title = "Promote Photo - Dashboard";
		$post = UploadPhotos::where('id', $id)->first();
        ////return $promotedPost ;
		if( $post ){
			return view('dashboard.promote-post.plan', [
				'title'         => $title,
				'post'          => $post,
				'type'          => 'photo',
				'countries'     => Country::get(),
				'promoted'  => $promotedPost
			]);
		}
		return abort(404);
	}
 
	public function story($slug, $promotedPost = null){
		$title = "Promote Story - Dashboard";
		$post = Story::where($promotedPost ? 'id' : 'slug', $slug)->first();
       // return $promotedPost;
		if( $post ){
			return view('dashboard.promote-post.plan', [
				'title'         => $title,
				'post'          => $post,
				'type'          => 'story',
				'countries'     => Country::get(),
				'promoted'  => $promotedPost
			]);
		}
		return abort(404);
	}

	public function createPost(Request $request){

		$payment = new PaymentController('razorpay');
		$payment->load([
			'razorpay_payment_id' 	=> $request->get('razorpay_payment_id'),
			'razorpay_order_id' 	=> $request->get('razorpay_order_id'),
			'razorpay_signature' 	=> $request->get('razorpay_signature'),
		]);

		$result = $payment->capturePayment();

		if( isset($result['error']) ){
			return redirect()->back()->withErrors([$result['error']]);
		}

// 		$rules = [
// 			'type'      => 'required|in:video,photo,story',
// 			'id'        => 'required',
// 			'country'   => 'required',
// 			'states'    => 'required',
// 			'plan'      => 'required',
// 		];

// 		$validator = Validator::make($request->all(), $rules);
// 		if( $validator->fails() ){
// 			return redirect()->back()->withErrors($validator);
// 		}

        if(!$request->type){
            $request->type = 'photo';
        }
        
        if(!$request->country){
            $request->country = Country::select('id')->first()->id;
        }
        
        if(!$request->states){
            $request->states = 'all';
        }

		$post = $plan = null;
		if( $request->type == 'video' ){
			$post = Video::where([['id', $request->id], ['user_id', session()->get('user_id')], ['isRewardVideo', '<>', '1']])->first();
		} elseif( $request->type == 'photo' ){
		    $whereClause = ['user_id', session()->get('user_id')];
		    if(!$request->id){
		        $whereClause['id'] = $request->id;
		    }
		    
			$post = UploadPhotos::where([$whereClause])->first();
		} else{
			$post = Story::where([['id', $request->id], ['user_id', session()->get('user_id')]])->first();
		}

		if( is_null($post) ){
			return redirect()->back()->withErrors(['Invalid Post Id.']);
		}

		if( !Country::find($request->country) ){
			return redirect()->back()->withErrors(['Unknown country']);
		}

		$promotePost = new PromotePostModel();
		$promotePost->post_type = $request->type;
		$promotePost->post_id = $request->id;
		$promotePost->user_id = session()->get('user_id');
		$promotePost->country_id = $request->country;

		if( $request->states == "selected" && is_array($request->selected_states) && count($request->selected_states) > 0 ){
			$states = [];
			foreach($request->selected_states as $state){
				if( States::find($state) ){
					array_push($states, $state);
				}
			}

			if( count($states) > 0 ){
				$promotePost->state_ids = implode(",", $states);
			}
		}
		
		if(!$promotePost->state_ids){
			$promotePost->state_ids = "all";
		}

		if( $request->plan == "custom" ){
			$plan = [
				'days'          => intval($request->custom_days),
				'amount'        => $request->custom_amount,
			];
			$promotePost->plan_id = 'custom';
			$promotePost->is_custom = 1;
		} else {
		   
		    if($request->input('promotephoto')){
		      	$plan = DB::table('photo_price')->whereId($request->plan)->first(); 
		      	    $rate = DB::table('photo_views_click')->first();    
		      		$promotePost->click_range = $rate->clicks_price;
		            $promotePost->view_range = $rate->views_price;
		    }else if($request->input('promotevideo')){
		       $plan = DB::table('photo_price')->whereId($request->plan)->first(); 
		       $rate = DB::table('video_views_click')->first();  
		       	$promotePost->click_range = $rate->clicks_price;
		        $promotePost->view_range = $rate->views_price;
		    }
		
			$promotePost->plan_id = $plan->id;
		}

		$promotePost->button_label = $request->button;
	
		$promotePost->days = $plan->days;
		$promotePost->amount = $plan->price;
		$promotePost->status = 1;
		$promotePost->url = $request->button_url;
		$promotePost->is_paid = $result['transaction']->id ? 1 : 0;
		$promotePost->save();

		if( $promotePost->id ){
			PromotePostModel::where([['post_type', $promotePost->post_type], ['post_id', $promotePost->post_id], ['id', '<>', $promotePost->id]])->update(['status' => '0']);
			if( $result['transaction']->id ){
				Transactions::find($result['transaction']->id)->update(['related_id' => $promotePost->id]);
			}
			
			$result['user']->notify(new Notices($result['user'], $result['message'], $result['payment']['notes']['type'], '', '', '', $promotePost->id));
			return redirect('/promoted-post/'.$promotePost->id)->with('success', 'Your post is promote');
		}
		return redirect()->back()->withError(['Something went wrong']);
	}

	public function editPost(Request $request, $id){
		$rules = [
			'country'   => 'required',
			'states'    => 'required',
			'plan'      => 'required',
		];

		$validator = Validator::make($request->all(), $rules);
		if( $validator->fails() ){
			return redirect()->back()->withErrors($validator);
		}

		$promote = PromotePostModel::where(['id' => $id, 'user_id' => session()->get('user_id')])->first();
		if( $promote ){
			if( $promote->status != 1 || (\Carbon\Carbon::parse($promote->created_at)->timestamp + (86400 * $promote->days)) < \Carbon\Carbon::now()->timestamp ){
				return redirect()->back()->withErrors(["This plan is already closed. You can create new plan or update the active plan."]);
			}

			if( $request->action && $request->action == "CLOSE" ){
				if( PromotePostModel::where(['id' => $id])->update(['status' => '2']) ){
					return redirect()->back()->withErrors(['Promotion has been closed for this post']);
				}
			}

			if( !Country::find($request->country) ){
				return redirect()->back()->withErrors(['Unknown country']);
			}

			// if( !empty($request->country) ){
			//     $promote->country_id = $request->country;
			// }
			// if( $request->states == "selected" && is_array($request->selected_states) && count($request->selected_states) > 0 ){
			//     $states = [];
			//     foreach($request->selected_states as $state){
			//         if( States::find($state) ){
			//             array_push($states, $state);
			//         }
			//     }

			//     if( count($states) > 0 ){
			//         $promote->state_ids = implode(",", $states);
			//     }
			// }
			
			// if(!$promote->state_ids){
			//     $promote->state_ids = "all";
			// }

			// if( $request->plan == "custom" ){
			//     $plan = [
			//         'days'          => intval($request->custom_days),
			//         'amount'        => $request->custom_amount,
			//     ];
			//     $promote->plan_id = 'custom';
			//     $promote->is_custom = 1;
			// } else {
			//     $plan = $this->getPlan($request->plan);
			//     $promote->plan_id = $plan['id'];
			// }

			// if( $promote->amount != $plan['amount'] ){
			//     if( $promote->amount > $plan['amount'] ){
			//         // transfer to wallet
			//     } else {
			//         // redirect to payment gateway.
			//     }
			// }

			// $promote->button_label = $request->button;
			// $promote->click_range = $this->perClickPrice;
			// $promote->view_range = $this->perViewPrice;
			// $promote->days = $plan['days'];
			// $promote->amount = $plan['amount'];
			$promote->status = $request->action == 'CLOSE' ? 0 : 1;
			$promote->save();

			return redirect()->back()->with('success', 'Plan updated successfully');
		}

		return abort(404);
	}

	public function getTotalAudience($type){
		$tracker = PromotePostModel::from(DB::raw("`promote_posts` pp"))
			->select("ppt.user_ip", "ppt.user_id")
			->where([
				'pp.post_type'     => $type,
				'pp.status'        => '1',
				'pp.user_id'       => Session::get('user_id')
			])
			->rightJoin(DB::raw('`promote_posts_track` ppt'), 'ppt.promote_post_id', '=', 'pp.id')
			->groupBy('ppt.user_id', 'ppt.user_ip');

		return DB::table(DB::raw("({$tracker->toSql()}) as audience"))
			->setBindings($tracker->getBindings())
			->count();
	}

	public function promotedPost($id){
		$promotedPost = PromotePostModel::find($id);
       // return 	$promotedPost;
		if( $promotedPost ){
			$promotedPost->states = $this->getStates($promotedPost->country_id);
			$promotedPost->graph = $this->getGraph($promotedPost->id);
			if( $promotedPost->post_type == 'video'){
				return $this->video($promotedPost->post_id, $promotedPost);
			} elseif( $promotedPost->post_type == 'photo' ){
				return $this->photo($promotedPost->post_id, $promotedPost);
			} elseif( $promotedPost->post_type == 'story' ) {
				return $this->story($promotedPost->post_id, $promotedPost);
			}
		}

		return abort(404);
	}

	private function getStates($country_id){
		return States::select('id', 'name')->where('country_id', $country_id)->get();
	}

	/**
	 * this is plan information
	 * @param int $planId
	 * @return array which contains id, days, amount of plan
	 */
	public function getPlan($planId){
		if( $planId == 1 ){
			return [
				'id'            => $planId,
				'days'          => 3,
				'amount'        => 100,
			];
		} elseif( $planId == 2 ){
			return [
				'id'            => $planId,
				'days'          => 7,
				'amount'        => 200,
			];
		} elseif( $planId == 3 ){
			return [
				'id'            => $planId,
				'days'          => 15,
				'amount'        => 500,
			];
		} elseif( $planId == 4 ){
			return [
				'id'            => $planId,
				'days'          => 24,
				'amount'        => 1000,
			];
		} else{
			return [
				'id'            => 5,
				'days'          => 30,
				'amount'        => 2000,
			];
		}
	}

	public function setGeoIp(){
		$ip = request()->ip();
		// set user location in session  using geoIp
	}

	public function getGeoIp(){
		// get user location using geoIp
	}
	
	private function getAd($now, $sql){
	    $post = PromotePostModel::where([['status', '1']])
			->whereRaw('DATE_ADD(`created_at`, INTERVAL days DAY) >= ? AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`) AND IF(DATE_ADD(IFNULL(`last_run_at`, "2000-01-01 00:00:00"), INTERVAL 1 HOUR) <= ?, 1, 0) AND `is_paid`=1'.$sql, [$now, $now])
			->orderByRaw("(((`views` * `view_range`) + (`clicks` * `click_range`)) / CAST(`amount` AS DECIMAL(10,2))) ASC, IFNULL(`last_run_at`, '2000-01-01 00:00:00') ASC, DATE_ADD(`created_at`, INTERVAL `days` DAY) ASC")
			->first();

		if( is_null($post) ){
			$post = PromotePostModel::where([['status', '1']])
			->whereRaw('DATE_ADD(created_at, INTERVAL days DAY) >= ? AND ((views * view_range) + (clicks * click_range) < `amount`) AND is_paid=1'.$sql, [$now])
			->orderByRaw("(((views * view_range) + (clicks * click_range)) / CAST(`amount` AS DECIMAL(10,2))) ASC, DATE_ADD(created_at, INTERVAL days DAY) ASC")
			->first();
		}
		
		return $post;
	}

	/**
	 * Call this using ajax to get ads data to show
	 * @return json
	 */
	public function getPostToShow(Request $request){
		if( !$request->ajax() ){
			return response()->json(['status' => 0, 'error' => 'Bad Request'], 400);
		}
		
		$sql = "";
		if( $request->get('ids') ){
			$ids = $request->get('ids');
			if( is_array($ids) && count($ids) > 0 ){
				$joined_ids = implode(',', $ids);
				$sql .= " AND `id` NOT IN ($joined_ids)";
			}
		}
		
		$type = $request->get('type');
		if( !$type || !in_array($type, ['photo', 'video', 'story', 'ad']) ){
			return response()->json(['status' => 0, 'error' => 'Invalid type'], 400);
		}

		$sql .= " AND `post_type`='$type'";
		if( $type == 'ad' ){
		    $getAllVideos = false;
		    
			$adType = $request->get('adType');
			$adsSql = "";
			
		    if (!$adType || !$request->get('adSize')){
    			if($adType != 'video'){
    				return response()->json(['status' => 0, 'error' => 'Bad Request'], 400);
    			} else {
    			    $getAllVideos = true;
    			}
			}

			$adSize = $request->get('adSize');
			if($adType == 'video'){
			    if( !$getAllVideos ){
    				$adsSql = " AND `vlength`=$adSize";
			    }
			} elseif($adType == 'image'){
				$adSize = explode("x", strtolower($adSize));
				$adsSql = " AND `width`=$adSize[0] AND `height`=$adSize[1]";
			} else {
				return response()->json(['status' => 0, 'error' => 'Invalid Type']);
			}

			$sql .= " AND `post_id` IN (SELECT id FROM `ads_presets` WHERE `type`='$adType' $adsSql)";
		}

		$now = Carbon::now();
		
		$user_id = 0; //Session::get('user_id');
		$user_ip = $request->getClientIp();
		
		if(!$user_ip){
		    $user_ip = '';
		}

		$response = [
			'status'    => false,
			'data'      => '',
		];
		
	    $today = Carbon::now()->format("Y-m-d 00:00:00");
		$getTrack = PromotePostsTrack::select('promote_post_id')
    		->where([['user_ip', $user_ip], ['created_at', '>=', $today]])
    		->groupBy('promote_post_id')
    		->havingRaw('COUNT(promote_post_id) >= 10')
    		->get();
		
		if($getTrack){
    		$blockAds = $getTrack->pluck('promote_post_id')->toArray();
    		if($blockAds && count($blockAds) > 0){
    		    $ids = implode(",", $blockAds);
        		$sql .= " AND (`id` NOT IN ($ids))";
    		}
		}
		
		$post = $this->getAd($now, $sql);

		
		  	
		if( $request->id && intval($request->id) != 0 ){
		  	$count = 	$this->getGraph($request->id);
		$PromotePost =PromotePostModel::where([['status', '1'], ['is_paid', '1'], ['id', $request->id]])->first();
	
		if($PromotePost){
		    
		    if($PromotePost->remain_bal?$PromotePost->remain_bal >= $PromotePost->click_range:$PromotePost->amount >= ((count['totalClicks']+1)* $PromotePost->click_range)){
		 
		        $PromotePost->update(['clicks' =>$count['totalClicks']+1,'remain_bal'=>$PromotePost->remain_bal?$PromotePost->remain_bal - $PromotePost->click_range:$PromotePost->amount - (($count['totalClicks']+1)*$PromotePost->click_range)]);
		    }else{
		        $PromotePost->update(['status'=>0,'remain_bal'=>0]);
		    }
		}
			$tracker = new PromotePostsTrack();
			$tracker->promote_post_id = $request->id;
			$tracker->event_type = "click";
			$tracker->user_id = $user_id;
			$tracker->user_ip = $user_ip;
			$tracker->country = '';
			$tracker->state = '';
			$tracker->city = '';
			$tracker->save();
		
		}

		if( $post ){
		   
			$_post = null;
			if( $post->post_type == 'video' ){
				$_post = Video::where([['id', $post->post_id], ['isRewardVideo', '0']])->first();
			} elseif( $post->post_type == 'photo' ){
				$_post = UploadPhotos::select('upload_photos.*', DB::raw('(SELECT COUNT(photo_likes.liked) FROM photo_likes WHERE `photo_likes`.photo_id = `upload_photos`.id) as total'))->where([['id', $post->post_id]])->first();
			} elseif( $post->post_type == 'story' ){
				$_post = Story::where([['id', $post->post_id]])->first();
			} elseif( $post->post_type == 'ad' ){
				$_post = AdsPresetModel::find($post->post_id);
			}
       
			if( $_post ){
				$src = null;
				$url = null;
				$html = "";

				$_post->isAd = true;
				$_post->button_url = $post->url;
				$_post->button_text = $post->button_label;
				if( $post->post_type == 'story' ){
					$src = get_story_thumb_url($_post->cover_image);
					$url = to_story($_post->slug);

					$html = view('elements.story.cards', ['stories' => [$_post]])->render();
				} elseif( $post->post_type == 'video' ){
					$src = video_url($_post->video);
					$url = url("watch/".$_post->video_key);

					if( $request->get('model') )
					{
						if( $request->get('model') == 'category-listing' ){
							$html = view('elements.post.video_v', ['videos' => $_post])->render();
						}
					}

					if(!$html){
						$html = view('elements.post.categoryvideo', ['videoData' => [$_post]])->render();
					}
				} elseif( $post->post_type == 'photo' ){
					$images = unserialize($_post->images);
					$url = url("single-photo/".$_post->id);
					
					foreach($images as $image){
						if( file_exists(photo_path($image)) ){
							$src = photo_url($image);
							break;
						}
					}

					if( $src ){
						if( $request->get('model') ){
							if ($request->get('model') == 'details-listing') {
								$html = view('photos.inc.details-listing', ['photos' => [$_post]])->render();
							} elseif($request->get('model') == 'mobile-listing') {
								$html = view('photos.mobile', ['photos' => [$_post]])->render();
							} elseif( $request->get('model') == 'popup-listing' ) {
								$html = view('photos.inc.popup', ['photos' => [$_post]])->render();
							}
						}
						
						if(!$html){
							$html = view('photos.inc.listing', ['photos' => [$_post]])->render();
						}
					}
				} elseif( $post->post_type == 'ad' ){
        		    $view = 'elements.ad';
        		    $options = [];
        		    
				    if($request->adType == 'video'){
    					$view = 'elements.ad-video';
    					
    					if(is_array($request->options) && count($request->options)){
    					    $options = $request->options;
    					}
				    }
				    
					$html = view($view, ['ad' => $_post, 'options' => $options])->render();
				}

				if( $src || $post->post_type == 'ad' ){
				    	$PromotePost = PromotePostModel::where('id', $post->id)->first();
		if($PromotePost){
		    	$count1 = 	$this->getGraph($post->id);
		    
		    if( $PromotePost->remain_bal?$PromotePost->remain_bal >= $PromotePost->view_range:$PromotePost->amount  >= (($count1['totalViews'] + 1)*$PromotePost->view_range)){
		        
		        $PromotePost->update(['last_run_at' => date('Y-m-d H:i:s'), 'views' =>$count1['totalViews'] + 1,'remain_bal'=>$PromotePost->remain_bal?$PromotePost->remain_bal - $PromotePost->view_range:$PromotePost->amount - (($count1['totalViews'] + 1)*$PromotePost->view_range)]);
		    }else{
		        $PromotePost->update(['status'=>0,'remain_bal'=>0]);
		    }
		}
					
					$tracker = new PromotePostsTrack();
					$tracker->promote_post_id = $post->id;
					$tracker->event_type = "view";
					$tracker->user_id = $user_id;
					$tracker->user_ip = $user_ip;
					$tracker->country = '';
					$tracker->state = '';
					$tracker->city = '';
					$tracker->save();
	
					$data = [
						'ads' => [
							'mode'          => $post->post_type ==  'ad' ? 'ad' : "promoted_post",
							'type'          => $post->post_type,
							'id'            => $post->id,
							'html'          => $html,
							'post_id'       => $post->post_id,
							'src'           => $src,
							// 'url'           => $url,
							// 'buttonUrl'     => $post->url,
							// "buttonText"    => $post->button_label
						]
					];
					$response['data'] = $data;
					$response['status'] = true;
				}
			} else {
				PromotePostModel::where('id', $post->id)->update(['status' => '0']);
			}
		}

		return response()->json($response, 200);
	}

	public function getGraph($id, $subdays = 30, $forceSubDays = false){
		$post = PromotePostModel::find($id);
		
		if (!$post) {
			return;
		}

		$carbon = new Carbon();
		$days = $carbon->now()->subDay($subdays)->format("Y-m-d 00:00:00");
		$track = PromotePostsTrack::selectRaw('event_type, DATE(created_at) AS date, COUNT(*) AS count')
			->where('promote_post_id', $id)
			->where('created_at', '>=', $days)
			->whereIn('event_type', ['click', 'view'])
			->groupBy(DB::raw("DATE(created_at), event_type"))
			->orderByRaw('DATE(created_at) ASC')
			->orderBy('event_type', 'DESC')
			->get();

		if( ($post->created_at > $days) && !$forceSubDays ){
			$days = $post->created_at;
			$subdays = Carbon::parse($days)->diffInDays($carbon->now());
		}

		$graphObj = [
			'date'      => [],
			'views'     => [
				"label" 			=> "views",
				"backgroundColor" 	=> "rgba(32, 212, 137, 1)",
				"borderColor" 		=> "transparent",
				"borderWidth" 		=> 2,
				"borderRadius" 		=> 4,
				"borderSkipped" 	=> false,
				"data" 				=> []
			],
			'clicks'    => [
				"label" 			=> "clicks",
				"backgroundColor" 	=> "rgba(32, 212, 137, 0.25)",
				"borderColor" 		=> "transparent",
				"borderWidth" 		=> 2,
				"borderRadius" 		=> 4,
				"borderSkipped" 	=> false,
				"data" 				=> []
			],
			'total'                 => 0,
			'totalViews'            => 0,
			'totalClicks'           => 0
		];

		for( $i = 0; $i <= $subdays; $i++ ){
			$clicks = 0;
			$views = 0;

			$date = $carbon->parse($days)->addDay($i);
			array_push($graphObj['date'], $date->format("d M"));

			if( $track ){
				if(isset($track[0]) && $track[0]->event_type == 'view' && $track[0]->date == $date->format("Y-m-d")){
					$views = $track[0]->count;
					$track->splice(0, 1);
				}

				if(isset($track[0]) && $track[0]->event_type == 'click' && $track[0]->date == $date->format("Y-m-d")){
					$clicks = $track[0]->count;
					$track->splice(0, 1);
				}
			}

			$graphObj['totalViews'] += intval($views);
			$graphObj['totalClicks'] += intval($clicks);
			array_push($graphObj['views']['data'], $views);
			array_push($graphObj['clicks']['data'], $clicks);
		}
		
		$graphObj['total'] = $graphObj['totalViews'] + $graphObj['totalClicks'];
		return $graphObj;
	}

	public function promoted_posts(Request $request)
	{
		$pageTitle = 'Promoted Posts';
		$user_id = Session::get('user_id');
		$postTitle = "";

		// if( !is_null($request->get('post-title')) ){
		// 	$postTitle = $request->get('post-title');
		// }

		$recentPosts = PromotePostModel::where([['user_id', $user_id], ['is_paid', '<>', 0]])
			->orderByDesc('created_at')
			->paginate(20);
 ;
		return view('dashboard.promote-post.promoted', ['title' => $pageTitle, "recentPosts" => $recentPosts, 'postTitle' => $postTitle]);
	}
}