<?php

namespace App\Http\Controllers;

use App\AdsPresetModel;
use App\BankDetail;
use App\Models\ApplyAudition;
use App\Models\ArtistAbout;
use App\Models\ArtistGallery;
use App\Models\ArtistVideo;
use App\Models\Category;
use App\Models\Country;
use App\Models\photocomment;
use App\Models\PhotoLikes;
use App\Models\PostAudtion;
use App\Models\ProductionAbout;
use App\Models\PromotePostModel;
use App\Models\States;
use App\Models\Story;
use App\Models\Tag;
use App\Models\Transactions;
use App\Models\User;
use App\Models\Video;
use App\Models\VideoCategory;
use App\Models\VideoFlag;
use App\Models\VideoLikes;
use App\Models\VideoTag;
use App\Models\VideoThumb;
use App\Models\VideoViews;
use App\Models\Wallet;
use App\Models\WatchHistory;
use App\Models\WatchLater;
use App\Notifications\DislikedVideo;
use App\Notifications\LikedPhoto;
use App\Notifications\LikedVideo;
use App\Notifications\Notices;
use App\Notifications\VideoUploaded;
use App\Payout;
use App\PaypalDetail;
use App\UploadPhotos;
use App\UserPaymentMethod;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail as LaravelMail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Input;
use Redirect;
use Session;
use Validator;
use VideoThumbnail;

class HomesController extends Controller
{
    private $per_page = 30;

    public function index()
    {
        $pageTitle = 'Welcome to Entertainment Bugs';

        $latestFollowerVideo = NULL;

        if (!empty(Session::get('user_id'))) {

            $videoData = Video::where('isRewardVideo', '=', 0)
                ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->where("videos.isDeleted", 0)
                ->orderBy('created_at', 'desc')
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where("users.isDeleted", 0);
                    }
                ])
                ->with([
                    'totalViews' => function ($query) {
                        $query->select('video_id', 'watchTime')->where('user_id', Session::get('user_id'));
                    }
                ])
                ->holdUser()
                ->withCount(['isWatchedVideo'])
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);


            $rewardvideoData = Video::where('isRewardVideo', '=', 1)
                ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->orderBy('created_at', 'desc')
                ->where("videos.isDeleted", 0)
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where("users.isDeleted", 0);
                    }
                ])
                ->with([
                    'totalViews' => function ($query) {
                        $query->select('video_id', 'watchTime')->where('user_id', Session::get('user_id'));
                    }
                ])
                ->holdUser()
                ->withCount(['isWatchedVideo'])
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);

            $LatestVideos = Video::where('isRewardVideo', '=', 0)
                ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->where("videos.isDeleted", 0)
                ->orderBY(DB::raw('user_id', 'desc'))
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where("users.isDeleted", 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(15);
            if (session()->get('user_id')) {
                $getFollowers = DB::table('followers')->where('user_id', session()->get('user_id'))->pluck('follows_id')->toArray();
                $latestFollowerVideo = Video::whereIn('user_id', $getFollowers)->where("videos.isDeleted", 0)
                    ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                    ->orderBy('created_at', 'desc')
                    ->with([
                        'thumb' => function ($thumb) {
                            $thumb->select('id', 'thumb');
                        }
                    ])
                    ->with([
                        'user' => function ($user) {
                            $user->select('id', 'name', 'slug', 'photo')
                                ->where("users.isDeleted", 0)
                                ->orderBY(DB::raw('(name)'), 'DESC');
                        }
                    ])
                    ->holdUser()
                    ->withCount(['totalViews'])
                    ->with('watchTime')
                    ->take(12)
                    ->get();

            }
            $stories = Story::select('stories.id', 'title', 'stories.about', 'cover_image', 'thumb_image', 'category', 'user_id', 'stories.created_at', 'stories.slug')
                ->orderBy('created_at', 'desc')
                ->where("stories.isDeleted", 0)
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram')->where("users.isDeleted", 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->paginate(10);
            /* Story index page pagination */

            /* Comedy */
            $commedy = 'Comedy';
            $comedyvideo = Video::whereHas('video_category', function ($query) use ($commedy) {
                $query->whereHas(
                    'category',
                    function ($query) use ($commedy) {
                        $query->orderBy('created_at', 'desc');
                        $query->where('category', 'like', '%' . $commedy . '%');
                        $query->where('isRewardVideo', '=', 0);

                    }
                );
            })
                ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->orderBy('created_at', 'desc')
                ->where("videos.isDeleted", 0)
                ->where('isRewardVideo', 0)
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where("users.isDeleted", 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);


            /* Enetrtainment */
            $entertainment = 'Entertainment';
            $entertainmentvideo = Video::whereHas('video_category', function ($query) use ($entertainment) {
                $query->whereHas(
                    'category',
                    function ($query) use ($entertainment) {
                        $query->where('category', 'like', '%' . $entertainment . '%');
                        $query->where('isRewardVideo', '=', 0);
                        $query->where("videos.isDeleted", 0);
                        $query->orderBy('created_at', 'desc');
                    }
                );
            })
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where("users.isDeleted", 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);

            /* Music */
            $music = 'Music';
            $musicvideo = Video::whereHas('video_category', function ($query) use ($music) {
                $query->whereHas(
                    'category',
                    function ($query) use ($music) {
                        $query->where('category', 'like', '%' . $music . '%');
                        $query->where('isRewardVideo', '=', 0);
                        $query->where("videos.isDeleted", 0);
                        $query->orderBy('created_at', 'desc');
                    }
                );
            })
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where("users.isDeleted", 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);


            /* Gaming */
            $gami = 'Gaming';
            $gamevideo = Video::whereHas('video_category', function ($query) use ($gami) {
                $query->whereHas(
                    'category',
                    function ($query) use ($gami) {
                        $query->orderBy('created_at', 'desc');
                        // 		$query->where('category', 'like', '%'.$gami.'%');
                        $query->where('isRewardVideo', '=', 0);
                        $query->where("videos.isDeleted", 0);

                    }
                );
            })
                ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->orderBy('created_at', 'desc')
                ->where('isRewardVideo', 0)
                ->where("videos.isDeleted", 0)
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where("users.isDeleted", 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);


            /* Trending Reward Videos */

            $trendingRewardVideos = Video::select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->orderBy('total_views_count', 'desc')
                ->where('isRewardVideo', 0)
                ->where("videos.isDeleted", 0)
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where("users.isDeleted", 0);
                    }
                ])
                ->holdUser()
                ->withCount(['isWatchedVideo'])
                ->withCount(['totalViews'])
                ->paginate(12);

            $mostViewedVideos = Video::select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->orderBy('total_views_count', 'desc')
                ->where('isRewardVideo', 0)
                ->where("videos.isDeleted", 0)
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where("users.isDeleted", 0);
                    }
                ])
                ->holdUser()
                ->withCount(['isWatchedVideo'])
                ->withCount(['totalViews'])
                ->paginate(12);

            $UploadPhotos = UploadPhotos::select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
                ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
                ->where("upload_photos.isDeleted", 0)
                ->groupBY('upload_photos.id')
                ->orderBy('id', 'desc')
                ->paginate(30);
        } else {
            $videoData = Video::where('isRewardVideo', '=', 0)
                ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->where("videos.isDeleted", 0)
                ->orderBy('created_at', 'desc')
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);

            $rewardvideoData = Video::where('isRewardVideo', '=', 1)
                ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->where("videos.isDeleted", 0)
                ->orderBy('created_at', 'desc')
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                    }
                ])
                ->with([
                    'totalViews' => function ($query) {
                        $query->select('video_id', 'watchTime')->where('user_id', Session::get('user_id'));
                    }
                ])
                ->holdUser()
                ->withCount(['isWatchedVideo'])
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);


            /* Comedy */
            $commedy = 'Comedy';
            $comedyvideo = Video::whereHas('video_category', function ($query) use ($commedy) {
                $query->whereHas(
                    'category',
                    function ($query) use ($commedy) {
                        $query->orderBy('created_at', 'desc');
                        $query->where('category', 'like', '%' . $commedy . '%');
                        $query->where('isRewardVideo', '=', 0);
                    }
                );
            })
                ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->where("videos.isDeleted", 0)
                ->orderBy('created_at', 'desc')
                ->where('isRewardVideo', 0)
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);

            /* Enetrtainment */
            $entertainment = 'Entertainment';
            $entertainmentvideo = Video::whereHas('video_category', function ($query) use ($entertainment) {
                $query->whereHas(
                    'category',
                    function ($query) use ($entertainment) {
                        $query->orderBy('created_at', 'desc');
                        $query->where('category', 'like', '%' . $entertainment . '%');
                        $query->where('isRewardVideo', '=', 0);

                    }
                );
            })->where("videos.isDeleted", 0)->orderBy('created_at', 'desc')
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);


            /* Music */
            $music = 'Music';
            $musicvideo = Video::whereHas('video_category', function ($query) use ($music) {
                $query->whereHas(
                    'category',
                    function ($query) use ($music) {
                        $query->orderBy('created_at', 'desc');
                        $query->where('category', 'like', '%' . $music . '%');

                    }
                );
            })
                ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->where("videos.isDeleted", 0)
                ->orderBy('created_at', 'desc')
                ->where('isRewardVideo', 0)
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);

            /* Gaming */
            $gami = 'Gaming';
            $gamevideo = Video::whereHas('video_category', function ($query) use ($gami) {
                $query->whereHas(
                    'category',
                    function ($query) use ($gami) {
                        $query->orderBy('created_at', 'desc');
                        $query->where('category', 'like', '%' . $gami . '%');
                        $query->where('isRewardVideo', '=', 0);

                    }
                );
            })
                ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->where("videos.isDeleted", 0)
                ->orderBy('created_at', 'desc')
                ->where('isRewardVideo', 0)
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);

            $LatestVideos = Video::where('isRewardVideo', '=', 0)->where("videos.isDeleted", 0)
                ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->orderBy('created_at', 'desc')
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')
                            ->where('isDeleted', 0)
                            ->orderBY(DB::raw('(name)'), 'DESC');
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(12);

            $stories = Story::select('stories.id', 'title', 'stories.about', 'cover_image', 'thumb_image', 'category', 'user_id', 'stories.created_at', 'stories.slug')
                ->where("stories.isDeleted", 0)
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo', 'facebook', 'twitter', 'instagram')->where('isDeleted', 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->paginate(8);

            $trendingRewardVideos = Video::select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->where("videos.isDeleted", 0)
                ->orderBy('total_views_count', 'desc')
                ->where('isRewardVideo', 0)
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                    }
                ])
                ->holdUser()
                ->withCount(['isWatchedVideo'])
                ->withCount(['totalViews'])
                ->paginate(12);

            $mostViewedVideos = Video::select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->orderBy('total_views_count', 'desc')
                ->where('isRewardVideo', 0)
                ->where("videos.isDeleted", 0)
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                    }
                ])
                ->holdUser()
                ->withCount(['isWatchedVideo'])
                ->withCount(['totalViews'])
                ->paginate(12);

        }

        $users = User::select('id', 'name', 'slug', 'photo')
            ->holdUser()
            ->withCount(['followers'])
            ->withCount(['video' => function ($query) {
                $query->where('isDeleted', '=', 0);
            }])
            ->paginate(7);

        $mostFollowers = User::select('users.*', "followers.*", DB::raw("count(followers.follows_id) as count"))
            ->join('followers', 'users.id', '=', 'followers.follows_id')
            ->groupBy('followers.follows_id')
            ->orderBy('count', 'desc')
            ->limit(8)
            ->get();

        $categories = DB::table('video_categories as vc')
            ->where('vc.deleted_at', NULL)
            ->select('vc.category_id', DB::raw('count(vc.video_id) as total'), 'c.category', 'c.id')
            ->join('categories as c', 'c.id', '=', 'vc.category_id')
            ->groupBy('category_id', 'c.category')
            ->orderBY(DB::raw('count(video_id)'), 'DESC')
            ->limit(7)
            ->get();

        $trendingVideo = Video::select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
            ->orderBy('total_views_count', 'desc')
            ->where('isRewardVideo', 0)
            ->where("videos.isDeleted", 0)
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }
            ])
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                }
            ])
            ->holdUser()
            ->withCount(['isWatchedVideo'])
            ->withCount(['totalViews'])
            ->paginate(28);

        $UploadPhotos = UploadPhotos::select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->leftjoin('users', 'users.id', '=', 'upload_photos.user_id')
            ->where('upload_photos.isDeleted', 0)
            ->groupBY('upload_photos.id')
            ->orderBy('id', 'desc')
            ->paginate(30);

        $latestPhotos = UploadPhotos::holdUser()->select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->with('user')
            ->whereRaw("`upload_photos`.`isDeleted`=? AND `upload_photos`.`id` NOT IN (SELECT post_id from promote_posts WHERE post_type=? AND `status`=? AND `is_paid`=? AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))", [0, 0, 'photo', 1, 1])
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            //->take($this->rowperpage)
            ->latest()
            ->groupBY('upload_photos.id')
            ->inRandomOrder()
            ->get();

        return view('homes.index', ['title' => $pageTitle, 'trendingVideos' => $trendingVideo, 'latestFollowerVideo' => $latestFollowerVideo, 'mostFollowers' => $mostFollowers, 'trendingRewardVideos' => $trendingRewardVideos, 'mostViewedVideos' => $mostViewedVideos, 'categories' => $categories, 'gamevideo' => $gamevideo, 'musicvideo' => $musicvideo, 'entertainmentvideo' => $entertainmentvideo, 'comedyvideo' => $comedyvideo, 'videoData' => $videoData, 'rewardvideoData' => $rewardvideoData, 'LatestVideos' => $LatestVideos, 'stories' => $stories, 'users' => $users, 'UploadPhotos' => $UploadPhotos, 'latestPhotos' => $latestPhotos]);
    }

    public function chat()
    {
        $pageTitle = 'Welcome';
        return view('homes.chat', ['title' => $pageTitle]);
    }

    function getStates(Request $request)
    {
        $getCountryId = DB::table('tbl_countries')->where('name', $request->id)->first();
        if ($getCountryId) {
            $getState = DB::table('tbl_states')->where('country_id', $getCountryId->id)->get();
            echo json_encode($getState);
        }
    }

    function getTeamImage(Request $request)
    {
        $getUserDetail = DB::table('users')->where('name', $request->teamname)->get();
        if (count($getUserDetail) > 0) {
            if ($getUserDetail[0]->photo != NULL) {
                echo URL::to('public/uploads/users/small/' . $getUserDetail[0]->photo);
            } else {
                echo "https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-1-400x400.jpg";
            }
        } else {
            echo "https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-1-400x400.jpg";
        }
    }

    public function auditionview(Request $request, $slug)
    {
        $userid = session()->get('user_id');
        $date = Carbon::now()->format("Y-m-d");
        if ($request->tp) {
            $getAudDetail = PostAudtion::with('productionHouse')->where(['id' => $request->tp])->whereRaw("(`status` IS NULL OR `status` <> 'close') AND `valid_till` >= ?", [$date])->where('user_id', $userid)->get();

        } else {
            $getAudDetail = PostAudtion::with('productionHouse')->where('slug', $slug)->whereRaw("(`status` IS NULL OR `status` <> 'close') AND `valid_till` >= ?", [$date])->get();
        }
// 	dd($getAudDetail);
        if ($getAudDetail && count($getAudDetail) > 0) {
            $pageTitle = str_replace('-', ' ', $getAudDetail[0]->title);

            $is_view_exist = DB::table('post_view')->where(['post_id' => $getAudDetail[0]->id, 'user_id' => $userid])->get()->first();
            if ($userid && $is_view_exist === null) {
                DB::table('post_view')->insert(['post_id' => $getAudDetail[0]->id, 'user_id' => $userid]);
            }

            $get_views = DB::table('post_view')->where(['post_id' => $getAudDetail[0]->id])->get();
            if (count($getAudDetail) > 0) {
                $userid = $getAudDetail[0]->user_id;
            } else {
                $userid = 0;
            }

            $getUserDetail = DB::table('users')->where('id', $userid)->get();
            $ogdescription = '';
            $keywords = 'Entertainment Video, Comedy Video, Joke Video, love video, new song, serial, Web series, movies, video';
            $ogimage = 'https://i.pinimg.com/originals/1e/d1/c4/1ed1c4d0c212727f9083e9a838c9ed04.jpg';
            if ($userid != 0) {
                $ogimage = ((count($getAudDetail) > 0) ? (($getAudDetail[0]->photo != NULL || $getAudDetail[0]->photo != '') ? $getAudDetail[0]->photo : 'https://i.pinimg.com/originals/1e/d1/c4/1ed1c4d0c212727f9083e9a838c9ed04.jpg') : 'https://i.pinimg.com/originals/1e/d1/c4/1ed1c4d0c212727f9083e9a838c9ed04.jpg');
                $ogdescription = ((count($getAudDetail) > 0) ? (($getAudDetail[0]->description != NULL || $getAudDetail[0]->description != '') ? $getAudDetail[0]->description : '') : '');
                $keywords = ((count($getAudDetail) > 0) ? (($getAudDetail[0]->category != NULL || $getAudDetail[0]->category != '') ? $getAudDetail[0]->category : 'Entertainment Video, Comedy Video, Joke Video, love video, new song, serial, Web series, movies, video') : 'Entertainment Video, Comedy Video, Joke Video, love video, new song, serial, Web series, movies, video');
            }

            $categories = explode(",", $getAudDetail[0]->category);
            if ($categories > 0) {
                $_category = array_shift($categories);

                if (count($categories) > 0) {
                    $query = "(" . DB::table('post_audtion')
                            ->whereRaw("id != {$getAudDetail[0]->id} AND FIND_IN_SET('$_category', category) AND (`status` IS NULL OR `status` <> 'close') AND `valid_till` >= '$date'")
                            ->orderByDesc('id')
                            ->limit(10)
                            ->toSql();

                    foreach ($categories as $category) {
                        $query .= ") union (" . DB::table('post_audtion')
                                ->whereRaw("id != {$getAudDetail[0]->id} AND FIND_IN_SET('$category', category) AND (`status` IS NULL OR `status` <> 'close') AND `valid_till` >= '$date'")
                                ->orderByDesc('id')
                                ->limit(10)
                                ->toSql();
                    }

                    $query .= ")";

                    $sideAuditions = DB::select(DB::raw("SELECT * FROM ($query) as u ORDER BY id DESC LIMIT 10"));
                } else {
                    $sideAuditions = DB::table("post_audtion")
                        ->where('id', '<>', $getAudDetail[0]->id)
                        ->whereRaw("FIND_IN_SET(?, category) AND (`status` IS NULL OR `status` <> 'close') AND `valid_till` >= ?", [$_category, Carbon::now()->format("Y-m-d")])
                        ->orderByDesc("id")
                        ->limit(10)
                        ->get();
                }
            }

            if (!$sideAuditions) {
                $sideAuditions = DB::table("post_audtion")
                    ->whereRaw("(`status` IS NULL OR `status` <> 'close') AND `valid_till` >= ?", [Carbon::now()->format("Y-m-d")])
                    ->orderByDesc('id')
                    ->limit(10)
                    ->get();
            }

            return view('audition.audition-view', ['getIfAlready' => $getAudDetail, 'views' => $get_views, 'ogdescription' => $ogdescription, 'ogimage' => $ogimage, 'keywords' => $keywords, 'ogtitle' => str_replace('-', ' ', $getAudDetail[0]->title), 'username' => str_replace('-', ' ', $getUserDetail[0]->name), 'title' => $pageTitle, 'sideAuditions' => $sideAuditions]);
        }

        return abort(404);
    }

    public function applyForAudition(Request $request, $id)
    {
        $audition = PostAudtion::where('id', $id)->whereRaw("(`status` IS NULL OR `status` <> 'close') AND `valid_till` >= ?", [Carbon::now()->format("Y-m-d")])->first();
        $user = User::find(Session::get('user_id'));

        if (!(is_null($audition)) && !(is_null($user))) {
            if ($user->isArtist()) {
                $applyForAudition = ApplyAudition::where([['audition_id', $id], ['user_id', $user->id]])->first();
                if ($applyForAudition) {
                    return redirect()->back()->with('status', 'Already applied');
                }

                $applyForAudition = new ApplyAudition();
                $applyForAudition->audition_id = $id;
                $applyForAudition->user_id = $user->id;
                $applyForAudition->save();

                $poster = User::find($audition->user_id);
                if ($poster) {
                    $poster->notify(new Notices($user, ' Applied for audition', 'apply-for-audition', '', '', '', $audition->id));
                    $user->notify(new Notices($user, 'you applied for audition', 'applied-for-audition', '', '', '', $audition->id));
                }
                try {
                    LaravelMail::to('choubeyazad@gmail.com')->send('email.applyartist', $data, function ($message) use ($user) {
                        $message->subject
                        ('Applied for Audition');
                        $message->from('choubeyazad7@gmail.com', 'Azad');
                    });
                } catch (\Exception $e) {
                    echo 'Message: ' . $e->getMessage();
                }
                send_mail('apply-audition', $user->email, ['template' => 'email.apply', 'subject' => 'Applied for Audition']);
                return redirect()->back()->with('status', 'Applied for audition');
            } else {
                return redirect()->to("/audition/$audition->slug")->withErrors(['Artist profile is required to apply.']);
            }
        } else {
            return redirect()->to("/find-auditions")->withErrors(['Audition not found']);
        }
    }

    public function myaudition()
    {
        // dd($_GET['open']);
        $user = User::find(Session::get('user_id'));
        // $production_about = User::find();
        $production_about = DB::table('production_about')->where('user_id', Session::get('user_id'))->get();

        if (isset($_GET['open'])) {
            DB::table('post_audtion')->where('id', $_GET['open'])->update(['status' => 'open']);
            // $user->notify(new Auditions('job opened'));
            return Redirect::back()->with('msg', 'Audition Opened Successfully.');
        }
        if (isset($_GET['close'])) {
            DB::table('post_audtion')->where('id', $_GET['close'])->update(['status' => 'close']);
            return Redirect::back()->with('msg', 'Audition Closed Successfully.');
        }
        $pageTitle = 'My Audition';
        $userId = session()->get('user_id');
        $audtion = DB::table('post_audtion')->where('user_id', $userId)->get();
        return view('audition.my-audition', ['title' => $pageTitle, 'audtion' => $audtion, 'production_about' => $production_about]);
    }


    function getCities(Request $request)
    {
        $getStateId = DB::table('tbl_states')->where('name', $request->id)->get();
        $getCity = DB::table('tbl_cities')->where('state_id', $getStateId[0]->id)->get();
        echo json_encode($getCity);
    }


    public function artist()
    {
        $pageTitle = 'Artist';

        //	\DB::enableQueryLog();

        $premiumArtist = ArtistAbout::join("users", "users.id", "=", "artist_about.user_id")
            ->where("users.isDeleted", 0)
            ->where("artist_about.isDeleted", 0)
            ->whereNotNull("users.plan")
            ->where([["users.plan", '2'], ['users.plan_expired_at', '>', Carbon::now()->timestamp]])
            ->limit(3)
            ->get();
        //	dd(\DB::getQueryLog());

        return view('homes.artist', ['title' => $pageTitle, 'premiumArtist' => $premiumArtist]);
    }

    public function productionhouse()
    {
        $pageTitle = 'Production House';
        $premiumArtist = ArtistAbout::join("users", "users.id", "=", "artist_about.user_id")
            ->where("users.isDeleted", 0)
            ->where("artist_about.isDeleted", 0)
            ->whereNotNull("users.plan")
            ->where([["users.plan", '2'], ['users.plan_expired_at', '>', Carbon::now()->timestamp]])
            ->limit(3)
            ->get();

        return view('homes.production-house', ['title' => $pageTitle, 'premiumArtist' => $premiumArtist]);
    }

    public function casting()
    {
        $pageTitle = 'Casting';
        $qry = "model";
        $modeling = ArtistAbout::holdUser()->with('user')->where('multiple_category', 'LIKE', '%' . $qry . '%')->where('artist_about.isDeleted', 0)->latest('artist_about.created_at')->take(12)->get();
        $dancer_videos = ArtistVideo::with('aboutAuthor')->where('category', 'LIKE', '%Dancer%')->get();
        $latest_videos = ArtistVideo::with('aboutAuthor')->latest()->take(12)->get();
        $artist_cat_modeling = ArtistAbout::holdUser()->where('multiple_category', 'LIKE', '%Model%')->where('artist_about.isDeleted', 0)->latest('artist_about.created_at')->take(12)->get();
        $artist_cat_influencer = ArtistAbout::holdUser()->with('user')->where('artist_about.isDeleted', 0)->where('multiple_category', 'LIKE', '%New Influncer%')->latest('artist_about.created_at')->take(12)->get();
        $trending_production_houses = ProductionAbout::where('production_about.isDeleted', 0)->holdUser()->take(8)->get();
        $post_auditions = DB::table('post_audtion')->leftJoin("users", "users.id", "post_audtion.user_id")->where("users.isDeleted", 0)->where("post_audtion.isDeleted", 0)->whereRaw("(`post_audtion`.`status` IS NULL OR `post_audtion`.`status` <> 'close') AND `valid_till` >= ?", [Carbon::now()->format("Y-m-d")])->take(8)->orderBy('post_audtion.id', 'DESC')->get();

        return view('casting.index', [
            'title' => $pageTitle,
            'modeling' => $modeling,
            'dancer_videos' => $dancer_videos,
            'latest_videos' => $latest_videos,
            'artist_cat_modeling' => $artist_cat_modeling,
            'artist_cat_influencer' => $artist_cat_influencer,
            'trending_production_houses' => $trending_production_houses,
            'post_auditions' => $post_auditions
        ]);
    }

    public function productionhouseprofile($username)
    {
        // $pageTitle = str_replace('-', ' ', $username);
        $getAboutDetail = DB::table('production_about')->where(['username' => $username])->get();
        if (count($getAboutDetail) > 0) {
            $pageTitle = $getAboutDetail[0]->name;
            $getUserDetail = User::where('id', $getAboutDetail[0]->user_id)->get();
            if (!is_array($getUserDetail) && count($getUserDetail) > 0) {
                $userid = $getUserDetail[0]->id;
            }
        }

        $ogdescription = '';
        $keywords = 'Entertainment Video, Comedy Video, Joke Video, love video, new song, serial, Web series, movies, video';
        $ogimage = 'https://i.pinimg.com/originals/1e/d1/c4/1ed1c4d0c212727f9083e9a838c9ed04.jpg';
        if (isset($userid) && $userid) {
            $getAboutDetail = ProductionAbout::where('user_id', $userid)->get();
            $ogimage = ((count($getAboutDetail) > 0) ? (($getAboutDetail[0]->profile_photo != NULL || $getAboutDetail[0]->profile_photo != '') ? $getAboutDetail[0]->profile_photo : 'https://i.pinimg.com/originals/1e/d1/c4/1ed1c4d0c212727f9083e9a838c9ed04.jpg') : 'https://i.pinimg.com/originals/1e/d1/c4/1ed1c4d0c212727f9083e9a838c9ed04.jpg');
            $ogdescription = ((count($getAboutDetail) > 0) ? (($getAboutDetail[0]->about != NULL || $getAboutDetail[0]->about != '') ? $getAboutDetail[0]->about : '') : '');
            $keywords = ((count($getAboutDetail) > 0) ? (($getAboutDetail[0]->multiple_category != NULL || $getAboutDetail[0]->multiple_category != '') ? $getAboutDetail[0]->multiple_category : 'Entertainment Video, Comedy Video, Joke Video, love video, new song, serial, Web series, movies, video') : 'Entertainment Video, Comedy Video, Joke Video, love video, new song, serial, Web series, movies, video');

            return view('production-house.production-house-profile', ['ogdescription' => $ogdescription, 'ogimage' => $ogimage, 'keywords' => $keywords, 'ogtitle' => str_replace('-', ' ', $username), 'username' => str_replace('-', ' ', $username), 'title' => $pageTitle, 'productionHouse' => $getAboutDetail[0]]);
        } else {
            return abort(404);
        }
    }

    public function artistprofile($slug)
    {
        $artist = ArtistAbout::where('username', $slug)->first();
        if ($artist) {
            $user = $artist->user;

            $username = $artist->username;
            $pageTitle = $artist->profile_name;

            $userid = $user->id;
            $ogdescription = '';
            $keywords = 'Entertainment Video, Comedy Video, Joke Video, love video, new song, serial, Web series, movies, video';
            $ogimage = 'https://i.pinimg.com/originals/1e/d1/c4/1ed1c4d0c212727f9083e9a838c9ed04.jpg';

            $ogimage = $artist->profile_photo && !(empty($artist->profile)) ? $artist->profile_photo : 'https://i.pinimg.com/originals/1e/d1/c4/1ed1c4d0c212727f9083e9a838c9ed04.jpg';
            $ogdescription = $artist->profile_title && !(empty($artist->profile_title)) ? $artist->profile_title : "";
            $keywords = $artist->multiple_category && !(empty($artist->multiple_category)) ? $artist->multiple_category : 'Entertainment Video, Comedy Video, Joke Video, love video, new song, serial, Web series, movies, video';

            if ($user->hasThemePlan()) {
                return view('artist.artist-pro-profile', ['ogdescription' => $ogdescription, 'ogimage' => $ogimage, 'keywords' => $keywords, 'ogtitle' => str_replace('-', ' ', $username), 'username' => str_replace('-', ' ', $username), 'title' => $pageTitle, 'id' => $userid, 'artist' => $artist]);
            } else {
                return view('artist.artist-profile', ['ogdescription' => $ogdescription, 'ogimage' => $ogimage, 'keywords' => $keywords, 'ogtitle' => str_replace('-', ' ', $username), 'username' => str_replace('-', ' ', $username), 'title' => $pageTitle, 'id' => $userid, 'artist' => $artist]);
            }
        }

        return abort(404);
    }

    public function artistprofiles(Request $request)
    {
        $title = "Artists Profiles";
        $artists = ArtistAbout::rightJoin('users', 'users.id', '=', 'artist_about.user_id')->leftJoin('followers', 'followers.user_id', '=', 'artist_about.user_id')->where('artist_about.isDeleted', 0);

        if ($request['followers'] && !empty($request['followers']) && $request['followers'] != "Select") {
            $followers = $request['followers'] . "000";
            if ($request['followers'] == 99) {
                $followers = "0 && (SELECT count(followers.follows_id) AS follower) < 100000";
            }
            $artists = $artists->whereRaw("(SELECT count(followers.follows_id) AS follower) >= $followers");
        }

        if ($request['min_experience'] && !empty($request['min_experience']) && $request['min_experience'] != "Min:") {
            $artists = $artists->whereRaw("CAST(artist_about.exp AS UNSIGNED) >= {$request['min_experience']}");
        }

        if ($request['max_experience'] && !empty($request['max_experience']) && $request['max_experience'] != "Max:") {
            $artists = $artists->whereRaw("CAST(artist_about.exp AS UNSIGNED) <= {$request['max_experience']}");
        }

        if ($request['min_age'] && !empty($request['min_age']) && $request['min_age'] != "Min:") {
            $artists = $artists->where('artist_about.age', '>=', $request['min_age']);
        }
        if ($request['max_age'] && !empty($request['max_age']) && $request['max_age'] != "Max:") {
            $artists = $artists->where('artist_about.age', '<=', $request['max_age']);
        }

        if ($request['min_height'] && !empty($request['min_height']) && $request['min_height'] != "Min:") {
            $artists = $artists->where('artist_about.height', '>=', $request['min_height']);
        }
        if ($request['max_height'] && !empty($request['max_height']) && $request['max_height'] != "Max:") {
            $artists = $artists->where('artist_about.height', '<=', $request['max_height']);
        }

        if ($request['min_weight'] && !empty($request['min_weight']) && $request['min_weight'] != "Min:") {
            $artists = $artists->where('artist_about.weight', '>=', $request['min_weight']);
        }
        if ($request['max_weight'] && !empty($request['max_weight']) && $request['max_weight'] != "Max:") {
            $artists = $artists->where('artist_about.weight', '<=', $request['max_weight']);
        }

        if ($request['artist_name'] && !empty($request['artist_name'])) {
            $artists = $artists->whereRaw("(profile_name like '%{$request['artist_name']}%' OR (`tags` != '' AND FIND_IN_SET('{$request['artist_name']}', `tags`)))");
        }

        if ($request['category'] && !empty($request['category'])) {
            $category = "(";
            foreach ($request['category'] as $key => $cat) {
                if ($key == 0)
                    $category .= "artist_about.multiple_category like '%$cat%'";
                else
                    $category .= " || artist_about.multiple_category like '%$cat%'";
            }
            $category .= ")";
            $artists = $artists->whereRaw($category);
            // $artists = $artists->whereIn("artist_about.category", $request['category']);
        }

        if ($request['gender'] && !empty($request['gender']) && $request['gender'] != "Select") {
            $artists = $artists->where('artist_about.gender', '=', $request['gender']);
        }

        if ($request['lang'] && !empty($request['lang'])) {
            $languages = "(";
            foreach ($request['lang'] as $key => $lang) {
                if ($key == 0)
                    $languages .= "artist_about.language like '%$lang%'";
                else
                    $languages .= " || artist_about.language like '%$lang%'";
                // $video = $video->orwhere('artist_about.language', 'like', "%{$lang}%");
            }
            $languages .= ")";
            $artists = $artists->whereRaw($languages);
        }

        if ($request['city'] && !empty($request['city'])) {
            $artists = $artists->where('artist_about.city', 'like', '%' . $request['city'] . '%');
        }

        $now = \Carbon\Carbon::now()->timestamp;

        $artists = $artists->select('artist_about.*', 'users.*')->whereNotNull('artist_about.id')->groupBy('artist_about.id')->orderBy(DB::raw("((users.plan = 2 AND users.purchase > 0 AND users.plan_expired_at > $now) || (users.is_premium > 0 AND users.premium_ended_at > $now))"), "DESC")->paginate(36);

        return view('artist.artist-profiles', compact('title', 'artists'));
    }

    public function promoterecentpost(Request $request)
    {
        $pageTitle = 'Promote Recent Post';
        $user_id = Session::get('user_id');
        $postTitle = "";

        if (!is_null($request->get('post-title'))) {
            $postTitle = $request->get('post-title');
        }

        $recentVideos = Video::where([["user_id", $user_id], ['isRewardVideo', '0'], ['title', 'like', "%$postTitle%"]])
            ->selectRaw("'video' as tag, title, id, video_key as slug, video_thumb_id as image, created_at")
            ->orderByDesc("id");

        $recentPhotos = UploadPhotos::where([["user_id", $user_id], ['photo_title', 'like', "%$postTitle%"]])
            ->selectRaw("'photo' as tag, photo_title as title, id, id as slug, images as image, created_at")
            ->orderByDesc("id");

        $recentStories = Story::where([["user_id", $user_id], ['title', 'like', "%$postTitle%"]])
            ->selectRaw("'story' as tag, title, id, slug, thumb_image as image, created_at")
            ->orderByDesc("id")
            ->unionAll($recentVideos)
            ->unionAll($recentPhotos);

        $recentPosts = DB::table(DB::raw("({$recentStories->toSql()}) as x"))
            ->setBindings($recentStories->getBindings())
            ->orderByDesc("created_at")
            ->paginate(15);

        return view('dashboard.promote-recent-post', ['title' => $pageTitle, "recentPosts" => $recentPosts, 'postTitle' => $postTitle]);
    }

    public function boostplan()
    {
        $pageTitle = 'Boost Plan';
        $userId = Session::get('user_id');
        $user = User::find($userId);

        if (!$user) {
            return abort(404);
        }
        return view('dashboard.boost-plan', ['title' => $pageTitle, 'my' => $user]);
    }

    public function themeplan()
    {
        $pageTitle = "Theme Plan";
        $userId = Session::get('user_id');
        $user = User::find($userId);

        if (!$user) {
            return abort(404);
        }

        return view('dashboard.theme-plan', ['title' => $pageTitle, 'my' => $user]);
    }

    public function adsoverview()
    {
        $pageTitle = 'Ads Overview';
        $userId = Session::get('user_id');

        $totalAdCampaigns = PromotePostModel::where([
            'post_type' => 'ad',
            'user_id' => $userId,
            'status' => '1'
        ])
            ->count();

        $totalSpent = Transactions::where([
            'type' => 'ads-campaign',
            'status' => '1',
            'user_id' => $userId
        ])
            ->where('related_id', '<>', 0)
            ->sum('amount');

        $latestPlan = PromotePostModel::where([
            'post_type' => 'ad',
            'user_id' => $userId,
            'status' => '1'
        ])
            ->with('ad')
            ->orderByDesc('id')
            ->limit(2)
            ->get();

        $promotePostController = new PromotePostController;
        $latestPlanGraphData = null;
        $totalAudience = $promotePostController->getTotalAudience('ad');

        if ($latestPlan && count($latestPlan) > 0) {
            $latestPlanGraphData = $promotePostController->getGraph($latestPlan[0]->id, 30, true);
        }

        return view('dashboard.ads-overview', [
            'title' => $pageTitle,
            'totalAdCampaigns' => $totalAdCampaigns,
            'totalSpent' => $totalSpent,
            'totalAudience' => $totalAudience,
            'latestPlan' => $latestPlan,
            'latestPlanGraphData' => $latestPlanGraphData,
        ]);
    }

    public function activeadscampaign()
    {
        $pageTitle = 'Active Ads campaign';
        $ads = AdsPresetModel::where('user_id', Session::get("user_id"))->orderByDesc('id')->paginate(5);
        return view('dashboard.active-ads-campaign', ['title' => $pageTitle, 'ads' => $ads]);
    }

    public function editadscampaign(Request $request, $slug)
    {
        $pageTitle = 'Edit Ads campaign';
        $ad = AdsPresetModel::where([['slug', $slug], ['user_id', Session::get('user_id')]])->first();
        if ($ad) {
            if (strtolower($request->method()) == "put") {
                $ads = AdsPresetModel::find($ad->id);
                $ads->title = $request[$ad->type]['title'] ?? "";
                $ads->tags = $request[$ad->type]['tags'] ?? "";
                $ads->language = $request[$ad->type]['language'] ?? "";
                $ads->description = $request[$ad->type]['description'] ?? "";
                $ads->button_name = $request[$ad->type]['button'] ?? "";
                $ads->website_link = $request[$ad->type]['website'] ?? "";
                $category = "";
                if ($request[$ad->type]['category'] && count($request[$ad->type]['category']) > 0) {
                    foreach ($request[$ad->type]['category'] as $key => $c) {
                        if ($key == 0) {
                            $category = $c;
                        } else {
                            $category .= ", $c";
                        }
                    }
                }
                $ads->categories = $category;
                $ads->file = $request['file'];
                $ads->slug = Str::slug($ads->title) . "-" . $ad->id;
                $ads->thumb = $request['thumb'] ?? "";
                $ads->updated_at = date("Y-m-d H:i:s");

                $ads->save();
                return redirect()->to("active-ads-campaign")->with('success', 'Your Ads campaign is updated.');
            }
            return view('dashboard.edit-ads-campaign', ['title' => $pageTitle, 'ad' => $ad]);
        }
        return abort(404);
    }

    public function closedadscampaign()
    {
        $pageTitle = 'Closed Ads Campaign';
        return view('dashboard.closed-ads-campaign', ['title' => $pageTitle]);
    }

    public function createnewadscampaign(Request $request)
    {
        $pageTitle = 'Create New Ads campaign';
        if ($request->isMethod('post') && $request['type']) {
            $ads = new AdsPresetModel();
            $ads->user_id = Session::get('user_id');
            $ads->type = $request['type'];
            $ads->title = $request[$request['type']]['title'] ?? "";
            $ads->tags = $request[$request['type']]['tags'] ?? "";
            $ads->language = $request[$request['type']]['language'] ?? "";
            $ads->description = $request[$request['type']]['description'] ?? "";
            $ads->button_name = $request[$request['type']]['button'] ?? "";
            $ads->website_link = $request[$request['type']]['website'] ?? "";
            $ads->slug = Str::slug($ads->title);
            $category = "";
            if ($request[$request['type']]['category'] && count($request[$request['type']]['category']) > 0) {
                foreach ($request[$request['type']]['category'] as $key => $c) {
                    if ($key == 0) {
                        $category = $c;
                    } else {
                        $category .= ", $c";
                    }
                }
            }

            $ads->categories = $category;
            $ads->file = $request['file'];
            $ads->thumb = $request['thumb'] ?? "";

            if ($request['type'] == "image") {
                $template = explode("x", $request['template']);
                $ads->vlength = 0;
                $ads->width = $template[0];
                $ads->height = $template[1];
            } else {
                $ads->vlength = $request['template'];
                $ads->width = 0;
                $ads->height = 0;
            }

            $ads->campaign_id = 0;
            $ads->created_at = Carbon::now();

            $ads->save();
            $ads->slug = $ads->slug . "-" . $ads->id;
            $ads->save();

            $payment = new PaymentController('razorpay');
            $payment->load([
                'razorpay_payment_id' => $request->get('razorpay_payment_id'),
                'razorpay_order_id' => $request->get('razorpay_order_id'),
                'razorpay_signature' => $request->get('razorpay_signature'),
            ]);

            $result = $payment->capturePayment();
            if (isset($result['error'])) {
                return redirect()->back()->withErrors([$result['error']]);
            }

            $rules = [
                'country' => 'required',
                'states' => 'required',
                'plan' => 'required',
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }

            $promoteController = new PromotePostController;

            $promotePost = new PromotePostModel();
            $promotePost->post_type = 'ad';
            $promotePost->post_id = $ads->id;
            $promotePost->user_id = session()->get('user_id');
            $promotePost->country_id = $request->country;

            if ($request->states == "selected" && strlen($request->selected_states) > 0) {
                $states = [];
                $selectedStates = explode(",", $request->selected_states);
                $_states = States::select('id')->whereHas('id', $selectedStates)->get();

                if ($_states && count($_states) > 0) {
                    foreach ($_states as $state) {
                        array_push($states, $state);
                    }
                }

                if (count($states) > 0) {
                    $promotePost->state_ids = implode(",", $states);
                }
            }

            if (!$promotePost->state_ids) {
                $promotePost->state_ids = "all";
            }

            if ($request->plan == "custom") {
                $plan = [
                    'days' => intval($request->custom_days),
                    'amount' => $request->custom_amount,
                ];
                $promotePost->plan_id = 'custom';
                $promotePost->is_custom = 1;
            } else {
                if ($request->imageplan) {
                    $plan = DB::table('photo_price')->whereId($request->plan)->first();
                    $rate = DB::table('photo_views_click')->first();
                    $promotePost->click_range = $rate->clicks_price;
                    $promotePost->view_range = $rate->views_price;
                } else {
                    $plan = DB::table('photo_price')->whereId($request->plan)->first();
                    $rate = DB::table('video_views_click')->first();
                    $promotePost->click_range = $rate->clicks_price;
                    $promotePost->view_range = $rate->views_price;
                }
                $promotePost->plan_id = $plan->id;
            }

            $promotePost->button_label = $request[$request['type']]['button'] ?? "";
            $promotePost->url = $request[$request['type']]['website'] ?? "";
            $promotePost->days = $plan->days;
            $promotePost->amount = $plan->price;
            $promotePost->status = 1;
            $promotePost->is_paid = $result['transaction']->id ? 1 : 0;
            $promotePost->save();

            if ($promotePost->id) {
                $ads->campaign_id = $promotePost->id;
                $ads->save();

                if ($result['transaction']->id) {
                    Transactions::find($result['transaction']->id)->update(['related_id' => $promotePost->id]);
                }
            }

            return redirect()->to("active-ads-campaign")->with('success', 'Your Ads campaign is created.');
        }

        $countries = Country::get();
        return view('dashboard.create-new-ads-campaign', ['title' => $pageTitle, 'countries' => $countries]);
    }

    public function information()
    {
        $pageTitle = 'Information';
        return view('dashboard.information', ['title' => $pageTitle]);
    }

    public function contactus()
    {
        $pageTitle = 'Contact Us';
        return view('dashboard.contact-us', ['title' => $pageTitle]);
    }

    public function Complainrequest()
    {
        $pageTitle = 'Complain Request';
        return view('dashboard.complain-request', ['title' => $pageTitle]);
    }


    public function artistgallery(Request $request)
    {
        $Images = ArtistGallery::leftJoin('artist_about', 'artist_about.user_id', '=', 'artist_gallery.user_id')->RightJoin('users', 'users.id', '=', 'artist_gallery.user_id')
            ->leftJoin('followers', 'followers.user_id', '=', 'artist_gallery.user_id')
            ->where('users.isDeleted', 0)
            ->where('artist_about.isDeleted', 0);


        if ($request['followers'] && !empty($request['followers']) && $request['followers'] != "Select") {
            $followers = $request['followers'] . "000";
            if ($request['followers'] == 99) {
                $followers = "0 && (SELECT count(followers.follows_id) AS follower) < 100000";
            }
            $Images = $Images->whereRaw("(SELECT count(followers.follows_id) AS follower) >= $followers");
        }

        if ($request['min_experience'] && !empty($request['min_experience']) && $request['min_experience'] != "Min:") {
            $Images = $Images->whereRaw("CAST(artist_about.exp AS UNSIGNED) >= {$request['min_experience']}");
        }

        if ($request['max_experience'] && !empty($request['max_experience']) && $request['max_experience'] != "Max:") {
            $Images = $Images->whereRaw("CAST(artist_about.exp AS UNSIGNED) <= {$request['max_experience']}");
        }

        if ($request['min_age'] && !empty($request['min_age']) && $request['min_age'] != "Min:") {
            $Images = $Images->where('artist_about.age', '>=', $request['min_age']);
        }
        if ($request['max_age'] && !empty($request['max_age']) && $request['max_age'] != "Max:") {
            $Images = $Images->where('artist_about.age', '<=', $request['max_age']);
        }

        if ($request['min_height'] && !empty($request['min_height']) && $request['min_height'] != "Min:") {
            $Images = $Images->where('artist_about.height', '>=', $request['min_height']);
        }
        if ($request['max_height'] && !empty($request['max_height']) && $request['max_height'] != "Max:") {
            $Images = $Images->where('artist_about.height', '<=', $request['max_height']);
        }

        if ($request['min_weight'] && !empty($request['min_weight']) && $request['min_weight'] != "Min:") {
            $Images = $Images->where('artist_about.weight', '>=', $request['min_weight']);
        }
        if ($request['max_weight'] && !empty($request['max_weight']) && $request['max_weight'] != "Max:") {
            $Images = $Images->where('artist_about.weight', '<=', $request['max_weight']);
        }

        if ($request['artist_name'] && !empty($request['artist_name'])) {
            $Images = $Images->where('artist_about.profile_name', 'like', '%' . $request['artist_name'] . '%');
        }

        if ($request['category'] && !empty($request['category'])) {
            $Images = $Images->whereIn("artist_gallery.category", $request['category']);
            // $Images = $Images->whereIn("artist_gallery.category", $request['category']);
            // foreach ($request['category'] as $key => $cat) {
            // 	if ($key == 0)
            // 		$Images = $Images->where('artist_gallery.category', 'like', "%{$cat}%");
            // 	else
            // 		$Images = $Images->orWhere('artist_gallery.category', 'like', "%{$cat}%");
            // }
        }

        if ($request['gender'] && !empty($request['gender']) && $request['gender'] != "Select") {
            $Images = $Images->where('artist_about.gender', '=', $request['gender']);
        }

        if ($request['lang'] && !empty($request['lang'])) {
            // foreach ($request['lang'] as $key => $lang) {
            // 	if ($key == 0)
            // 		$Images = $Images->where('artist_about.language', 'like', "%{$lang}%");
            // 	else
            // 		$Images = $Images->orWhere('artist_about.language', 'like', "%{$lang}%");
            // }
            $languages = "(";
            foreach ($request['lang'] as $key => $lang) {
                if ($key == 0)
                    $languages .= "artist_about.language like '%$lang%'";
                else
                    $languages .= " || artist_about.language like '%$lang%'";
                // $video = $video->orwhere('artist_about.language', 'like', "%{$lang}%");
            }
            $languages .= ")";
            $Images = $Images->whereRaw($languages);
        }

        if ($request['city'] && !empty($request['city'])) {
            $Images = $Images->where('artist_about.city', 'like', '%' . $request['city'] . '%');
        }

        $now = \Carbon\Carbon::now()->timestamp;

        $Images = $Images->select('artist_gallery.*', 'artist_about.*', 'users.slug AS slug', 'artist_gallery.slug AS _slug', 'artist_gallery.id AS iid')->whereNotNull('artist_gallery.id')->groupBy('artist_gallery.user_id')->orderBy(DB::raw("((users.plan = 2 AND users.purchase > 0 AND users.plan_expired_at > $now) || (users.is_premium > 0 AND users.premium_ended_at > $now))"), "DESC")->paginate(36);
        $title = 'Search Artist Gallery';
        //return $Images;

        $videos = $this->artistvideo($request);
        return view('artist.artist-gallery', compact('title', 'Images', 'request', 'videos'));
    }

    public static function artistgallies($request)
    {
        $Images = ArtistGallery::leftJoin('artist_about', 'artist_about.user_id', '=', 'artist_gallery.user_id')->RightJoin('users', 'users.id', '=', 'artist_gallery.user_id')
            ->leftJoin('followers', 'followers.user_id', '=', 'artist_gallery.user_id')
            ->where('users.isDeleted', 0)
            ->where('artist_about.isDeleted', 0);


        if ($request['followers'] && !empty($request['followers']) && $request['followers'] != "Select") {
            $followers = $request['followers'] . "000";
            if ($request['followers'] == 99) {
                $followers = "0 && (SELECT count(followers.follows_id) AS follower) < 100000";
            }
            $Images = $Images->whereRaw("(SELECT count(followers.follows_id) AS follower) >= $followers");
        }

        if ($request['min_experience'] && !empty($request['min_experience']) && $request['min_experience'] != "Min:") {
            $Images = $Images->whereRaw("CAST(artist_about.exp AS UNSIGNED) >= {$request['min_experience']}");
        }

        if ($request['max_experience'] && !empty($request['max_experience']) && $request['max_experience'] != "Max:") {
            $Images = $Images->whereRaw("CAST(artist_about.exp AS UNSIGNED) <= {$request['max_experience']}");
        }

        if ($request['min_age'] && !empty($request['min_age']) && $request['min_age'] != "Min:") {
            $Images = $Images->where('artist_about.age', '>=', $request['min_age']);
        }
        if ($request['max_age'] && !empty($request['max_age']) && $request['max_age'] != "Max:") {
            $Images = $Images->where('artist_about.age', '<=', $request['max_age']);
        }

        if ($request['min_height'] && !empty($request['min_height']) && $request['min_height'] != "Min:") {
            $Images = $Images->where('artist_about.height', '>=', $request['min_height']);
        }
        if ($request['max_height'] && !empty($request['max_height']) && $request['max_height'] != "Max:") {
            $Images = $Images->where('artist_about.height', '<=', $request['max_height']);
        }

        if ($request['min_weight'] && !empty($request['min_weight']) && $request['min_weight'] != "Min:") {
            $Images = $Images->where('artist_about.weight', '>=', $request['min_weight']);
        }
        if ($request['max_weight'] && !empty($request['max_weight']) && $request['max_weight'] != "Max:") {
            $Images = $Images->where('artist_about.weight', '<=', $request['max_weight']);
        }

        if ($request['artist_name'] && !empty($request['artist_name'])) {
            $Images = $Images->where('artist_about.profile_name', 'like', '%' . $request['artist_name'] . '%');
        }

        if ($request['category'] && !empty($request['category'])) {
            $Images = $Images->whereIn("artist_gallery.category", $request['category']);
            // $Images = $Images->whereIn("artist_gallery.category", $request['category']);
            // foreach ($request['category'] as $key => $cat) {
            // 	if ($key == 0)
            // 		$Images = $Images->where('artist_gallery.category', 'like', "%{$cat}%");
            // 	else
            // 		$Images = $Images->orWhere('artist_gallery.category', 'like', "%{$cat}%");
            // }
        }

        if ($request['gender'] && !empty($request['gender']) && $request['gender'] != "Select") {
            $Images = $Images->where('artist_about.gender', '=', $request['gender']);
        }

        if ($request['lang'] && !empty($request['lang'])) {
            // foreach ($request['lang'] as $key => $lang) {
            // 	if ($key == 0)
            // 		$Images = $Images->where('artist_about.language', 'like', "%{$lang}%");
            // 	else
            // 		$Images = $Images->orWhere('artist_about.language', 'like', "%{$lang}%");
            // }
            $languages = "(";
            foreach ($request['lang'] as $key => $lang) {
                if ($key == 0)
                    $languages .= "artist_about.language like '%$lang%'";
                else
                    $languages .= " || artist_about.language like '%$lang%'";
                // $video = $video->orwhere('artist_about.language', 'like', "%{$lang}%");
            }
            $languages .= ")";
            $Images = $Images->whereRaw($languages);
        }

        if ($request['city'] && !empty($request['city'])) {
            $Images = $Images->where('artist_about.city', 'like', '%' . $request['city'] . '%');
        }

        $now = \Carbon\Carbon::now()->timestamp;

        return $Images = $Images->select('artist_gallery.*', 'artist_about.*', 'users.slug AS slug', 'artist_gallery.slug AS _slug', 'artist_gallery.id AS iid')->whereNotNull('artist_gallery.id')->groupBy('artist_gallery.user_id')->orderBy(DB::raw("((users.plan = 2 AND users.purchase > 0 AND users.plan_expired_at > $now) || (users.is_premium > 0 AND users.premium_ended_at > $now))"), "DESC")->paginate(36);
    }

    public static function artistvideo($request)
    {
        $video = ArtistVideo::join('artist_about', 'artist_about.user_id', '=', 'artist_video.user_id')->RightJoin('users', 'users.id', '=', 'artist_video.user_id')
            ->leftJoin('followers', 'followers.user_id', '=', 'artist_video.user_id')
            ->where('artist_about.isDeleted', 0);
        //  dd($request['category']);
        if ($request['followers'] && !empty($request['followers']) && $request['followers'] != "Select") {
            $followers = $request['followers'] . "000";
            if ($request['followers'] == 99) {
                $followers = "0 && (SELECT count(followers.follows_id) AS follower) < 100000";
            }
            $video = $video->whereRaw("(SELECT count(followers.follows_id) AS follower) >= $followers");
        }

        if ($request['min_experience'] && !empty($request['min_experience']) && $request['min_experience'] != "Min:") {
            $video = $video->whereRaw("CAST(artist_about.exp AS UNSIGNED) >= {$request['min_experience']}");
        }

        if ($request['max_experience'] && !empty($request['max_experience']) && $request['max_experience'] != "Max:") {
            $video = $video->whereRaw("CAST(artist_about.exp AS UNSIGNED) <= {$request['max_experience']}");
        }

        if ($request['min_age'] && !empty($request['min_age']) && $request['min_age'] != "Min:") {
            $video = $video->where('artist_about.age', '>=', $request['min_age']);
        }
        if ($request['max_age'] && !empty($request['max_age']) && $request['max_age'] != "Max:") {
            $video = $video->where('artist_about.age', '<=', $request['max_age']);
        }

        if ($request['min_height'] && !empty($request['min_height']) && $request['min_height'] != "Min:") {
            $video = $video->where('artist_about.height', '>=', $request['min_height']);
        }
        if ($request['max_height'] && !empty($request['max_height']) && $request['max_height'] != "Max:") {
            $video = $video->where('artist_about.height', '<=', $request['max_height']);
        }

        if ($request['min_weight'] && !empty($request['min_weight']) && $request['min_weight'] != "Min:") {
            $video = $video->where('artist_about.weight', '>=', $request['min_weight']);
        }
        if ($request['max_weight'] && !empty($request['max_weight']) && $request['max_weight'] != "Max:") {
            $video = $video->where('artist_about.weight', '<=', $request['max_weight']);
        }

        if ($request['artist_name'] && !empty($request['artist_name'])) {
            $video = $video->where('artist_about.profile_name', 'like', '%' . $request['artist_name'] . '%');
        }

        if ($request['category'] && !empty($request['category'])) {
            $video = $video->whereIn("artist_video.category", $request['category']);
        }

        if ($request['gender'] && !empty($request['gender']) && $request['gender'] != "Select") {
            $video = $video->where('artist_about.gender', '=', $request['gender']);
        }

        if ($request['lang'] && !empty($request['lang'])) {
            // $video = $video->where('artist_about.language', 'like', "%{$lang}%");
            $languages = "(";
            foreach ($request['lang'] as $key => $lang) {
                if ($key == 0)
                    $languages .= "artist_about.language like '%$lang%'";
                else
                    $languages .= " || artist_about.language like '%$lang%'";
                // $video = $video->orwhere('artist_about.language', 'like', "%{$lang}%");
            }
            $languages .= ")";
            $video = $video->whereRaw($languages);
        }

        if ($request['city'] && !empty($request['city'])) {
            $video = $video->where('artist_about.city', 'like', '%' . $request['city'] . '%');
        }

        $now = \Carbon\Carbon::now()->timestamp;

        return $videos = $video->select('artist_video.*', 'artist_about.*', 'users.*', 'artist_video.id AS vid', 'users.slug AS slug', 'artist_video.slug AS _slug', 'artist_video.id AS iid')->whereNotNull('artist_video.id')->groupBy('artist_video.user_id')->orderBy(DB::raw("((users.plan = 2 AND users.purchase > 0 AND users.plan_expired_at > $now) || (users.is_premium > 0 AND users.premium_ended_at > $now))"), "DESC")->paginate(36);
    }

    public function artistvideos(Request $request)
    {
        $video = ArtistVideo::join('artist_about', 'artist_about.user_id', '=', 'artist_video.user_id')->RightJoin('users', 'users.id', '=', 'artist_video.user_id')
            ->leftJoin('followers', 'followers.user_id', '=', 'artist_video.user_id')
            ->where('artist_about.isDeleted', 0);
        //  dd($request['category']);
        if ($request['followers'] && !empty($request['followers']) && $request['followers'] != "Select") {
            $followers = $request['followers'] . "000";
            if ($request['followers'] == 99) {
                $followers = "0 && (SELECT count(followers.follows_id) AS follower) < 100000";
            }
            $video = $video->whereRaw("(SELECT count(followers.follows_id) AS follower) >= $followers");
        }

        if ($request['min_experience'] && !empty($request['min_experience']) && $request['min_experience'] != "Min:") {
            $video = $video->whereRaw("CAST(artist_about.exp AS UNSIGNED) >= {$request['min_experience']}");
        }

        if ($request['max_experience'] && !empty($request['max_experience']) && $request['max_experience'] != "Max:") {
            $video = $video->whereRaw("CAST(artist_about.exp AS UNSIGNED) <= {$request['max_experience']}");
        }

        if ($request['min_age'] && !empty($request['min_age']) && $request['min_age'] != "Min:") {
            $video = $video->where('artist_about.age', '>=', $request['min_age']);
        }
        if ($request['max_age'] && !empty($request['max_age']) && $request['max_age'] != "Max:") {
            $video = $video->where('artist_about.age', '<=', $request['max_age']);
        }

        if ($request['min_height'] && !empty($request['min_height']) && $request['min_height'] != "Min:") {
            $video = $video->where('artist_about.height', '>=', $request['min_height']);
        }
        if ($request['max_height'] && !empty($request['max_height']) && $request['max_height'] != "Max:") {
            $video = $video->where('artist_about.height', '<=', $request['max_height']);
        }

        if ($request['min_weight'] && !empty($request['min_weight']) && $request['min_weight'] != "Min:") {
            $video = $video->where('artist_about.weight', '>=', $request['min_weight']);
        }
        if ($request['max_weight'] && !empty($request['max_weight']) && $request['max_weight'] != "Max:") {
            $video = $video->where('artist_about.weight', '<=', $request['max_weight']);
        }

        if ($request['artist_name'] && !empty($request['artist_name'])) {
            $video = $video->where('artist_about.profile_name', 'like', '%' . $request['artist_name'] . '%');
        }

        if ($request['category'] && !empty($request['category'])) {
            $video = $video->whereIn("artist_video.category", $request['category']);
        }

        if ($request['gender'] && !empty($request['gender']) && $request['gender'] != "Select") {
            $video = $video->where('artist_about.gender', '=', $request['gender']);
        }

        if ($request['lang'] && !empty($request['lang'])) {
            // $video = $video->where('artist_about.language', 'like', "%{$lang}%");
            $languages = "(";
            foreach ($request['lang'] as $key => $lang) {
                if ($key == 0)
                    $languages .= "artist_about.language like '%$lang%'";
                else
                    $languages .= " || artist_about.language like '%$lang%'";
                // $video = $video->orwhere('artist_about.language', 'like', "%{$lang}%");
            }
            $languages .= ")";
            $video = $video->whereRaw($languages);
        }

        if ($request['city'] && !empty($request['city'])) {
            $video = $video->where('artist_about.city', 'like', '%' . $request['city'] . '%');
        }

        $now = \Carbon\Carbon::now()->timestamp;

        $videos = $video->select('artist_video.*', 'artist_about.*', 'users.*', 'artist_video.id AS vid', 'users.slug AS slug', 'artist_video.slug AS _slug', 'artist_video.id AS iid')->whereNotNull('artist_video.id')->groupBy('artist_video.user_id')->orderBy(DB::raw("((users.plan = 2 AND users.purchase > 0 AND users.plan_expired_at > $now) || (users.is_premium > 0 AND users.premium_ended_at > $now))"), "DESC")->paginate(36);
        $title = 'Search Artist Videos';
        $Images = $this->artistgallies($request);
        return view(
            'artist.artist-videos',
            compact('title', 'videos', 'request', 'Images')
        );

    }

    public function findauditions(Request $request)
    {
        $pageTitle = 'Find Auditions';

        $auctions = PostAudtion::with(['user' => function ($q) {
            $q->where('isDeleted', 0);
        },
            'user.artist' => function ($q) {
                $q->where('isDeleted', 0);
            }])
            ->where('post_audtion.isDeleted', 0)
            ->whereRaw("(`status` IS NULL OR `status` <> 'close') AND `valid_till` >= ?", [Carbon::now()->format("Y-m-d")]);

        //dd(\DB::getQueryLog());

        if ($request['category'] && !empty($request['category'])) {
            $category = '';
            foreach ($request['category'] as $key => $cat) {
                if ($key == 0)
                    $category .= "`category` LIKE '%{$cat}%'";
                else
                    $category .= " || `category` LIKE '%{$cat}%'";
            }
            // $auctions = $auctions->where('category', 'like', "%{$cat}%");
            $auctions->whereRaw("($category)");
        }
        if ($request['lang'] && !empty($request['lang'])) {
            $language = '';
            foreach ($request['lang'] as $key => $lang) {
                if ($key == 0)
                    $language .= "`language` LIKE '%{$lang}%'";
                else
                    $language .= " || `language` LIKE '%{$lang}%'";

                // $auctions = $auctions->where('language', 'like', "%{$lang}%");
            }
            $auctions->whereRaw("($language)");
        }


        if ($request['city'] && !empty($request['city'])) {
            $auctions = $auctions->where('city', 'like', '%' . $request['city'] . '%');
        }

        if ($request['title'] && !empty($request['title'])) {
            $auctions = $auctions->where('title', 'like', '%' . $request['title'] . '%');
        }
        $auctions = $auctions->orderByDesc('id')->paginate(36);
        // $userId = session()->get('user_id');
        return view('audition.find-auditions', ['title' => $pageTitle, 'auctions' => $auctions, 'request' => $request]);

    }


    public function findproductionhouses(Request $request)
    {
        $pageTitle = 'Search Production House';
        $prductionHouses = ProductionAbout::with(['user' => function ($query) {
            $query->where('isDeleted', 0);
        }])->where('isDeleted', 0);

        if ($request['category'] && !empty($request['category'])) {
            foreach ($request['category'] as $key => $cat) {
                if ($key == 0)
                    $prductionHouses = $prductionHouses->where('multiple_category', 'like', "%{$cat}%");
                else
                    $prductionHouses = $prductionHouses->where('multiple_category', 'like', "%{$cat}%");
            }
        }

        if ($request['lang'] && !empty($request['lang'])) {
            foreach ($request['lang'] as $key => $lang) {
                if ($key == 0)
                    $prductionHouses = $prductionHouses->where('language', 'like', "%{$lang}%");
                else
                    $prductionHouses = $prductionHouses->where('language', 'like', "%{$lang}%");

            }
        }

        if ($request['city'] && !empty($request['city'])) {
            $prductionHouses = $prductionHouses->where('city', 'like', '%' . $request['city'] . '%');
        }

        if ($request['title'] && !empty($request['title'])) {
            $prductionHouses = $prductionHouses->where('name', 'like', '%' . $request['title'] . '%');
        }

        $prductionHouses = $prductionHouses->paginate(10);
        return view('production-house.production-houses', ['title' => $pageTitle, 'prductionHouses' => $prductionHouses, 'request' => $request]);

    }


    public function locationandlanguage()
    {
        $pageTitle = 'Location & Language';
        return view('dashboard.location-and-language', ['title' => $pageTitle]);
    }

    public function privacysetting()
    {
        $pageTitle = 'Privacy Setting';
        return view('dashboard.privacy-setting', ['title' => $pageTitle]);
    }


    public function createanartistprofile()
    {
        $pageTitle = 'Create an Artist Profile';
        $userId = session()->get("user_id");
        if ($userId) {
            $count = ProductionAbout::select("user_id")->where("production_about.user_id", $userId)
                ->union(
                    DB::table("production_contact_us")->select("user_id")->where("production_contact_us.user_id", $userId)
                )
                ->union(
                    DB::table("production_awards")->select("user_id")->where("user_id", $userId)
                )
                ->union(
                    DB::table("production_my_team")->select("user_id")->where("user_id", $userId)
                )
                ->union(
                    DB::table("production_gallery")->select("user_id")->where("production_gallery.user_id", $userId)
                )
                ->union(
                    DB::table("production_video")->select("user_id")->where("production_video.user_id", $userId)
                )
                ->union(
                    DB::table("production_what_we_do")->select("user_id")->where("user_id", $userId)
                )
                ->union(
                    DB::table("production_latest_project")->select("user_id")->where("user_id", $userId)
                )->count();

            if ($count) {
                return abort(404);
            }
        }
        return view('artist.create-an-artist-profile', ['title' => $pageTitle]);
    }

    function createTemp()
    {
        $pageTitle = 'Create an Artist Profile';
        return view('artist.create-artist-copy', ['title' => $pageTitle]);
    }

    public function createanproductionhouseprofile()
    {
        $pageTitle = 'Create an Production House Profile';
        $userId = session()->get("user_id");
        if ($userId) {
            $count = ArtistAbout::select("user_id")->where("artist_about.user_id", $userId)
                ->union(
                    DB::table("artist_contact")->select("user_id")->where("artist_contact.user_id", $userId)
                )
                ->union(
                    DB::table("artist_gallery")->select("user_id")->where("artist_gallery.user_id", $userId)
                )
                ->union(
                    DB::table("artist_showreel")->select("user_id")->where("artist_showreel.user_id", $userId)
                )
                ->union(
                    DB::table("artist_video")->select("user_id")->where("artist_video.user_id", $userId)
                )
                ->union(
                    DB::table("artist_what_we_do")->select("user_id")->where("user_id", $userId)
                )
                ->union(
                    DB::table("artist_project")->select("user_id")->where("user_id", $userId)
                )->count();

            if ($count) {
                return abort(404);
            }
        }

        return view('production-house.create-an-production-house-profile', ['title' => $pageTitle]);
    }

    function deleteProdProjImage($id)
    {
        $update = DB::Table('production_latest_project')->where('id', $id)->update([
            'photo' => NULL
        ]);
        return Redirect::to('/create-an-production-house-profile?status=success');
    }

    function deleteProjImage($id)
    {
        $update = DB::Table('artist_project')->where('id', $id)->update([
            'photo' => NULL
        ]);
        return Redirect::to('/create-an-artist-profile?status=success');
    }

    function deletePhotoImage($id)
    {
        $update = DB::Table('artist_gallery')->where('id', $id)->update([
            'photo' => NULL
        ]);
        return Redirect::to('/create-an-artist-profile?status=success');
    }

    function deleteAchivementImage($id)
    {
        $update = DB::Table('artist_achievment')->where('id', $id)->update([
            'photo_1' => NULL
        ]);
        return Redirect::to('/create-an-artist-profile?status=success');
    }

    function deleteAwardImage($id)
    {
        $update = DB::Table('production_awards')->where('id', $id)->update([
            'photo_1' => NULL
        ]);
        return Redirect::to('/create-an-production-house-profile?status=success');
    }

    function deleteProductionVideoImage($id)
    {
        $update = DB::Table('production_video')->where('id', $id)->update([
            'thumb' => NULL
        ]);
        return Redirect::to('/create-an-production-house-profile?status=success');
    }

    function deleteVideoImage($id)
    {
        $update = DB::Table('artist_video')->where('id', $id)->update([
            'thumb' => NULL
        ]);
        return Redirect::to('/create-an-artist-profile?status=success');
    }

    function deleteReelImage($id)
    {
        $update = DB::Table('artist_showreel')->where('id', $id)->update([
            'video_thumb' => NULL
        ]);
        return Redirect::to('/create-an-artist-profile?status=success');
    }

    function saveProductionTeam(Request $request)
    {
        $userId = session()->get('user_id');
        $j = 0;
        for ($i = 1; $i <= 12; $i++) {
            $data = array();
            if ($_POST['position' . $i] != '') {
                $data['position'] = $_POST['position' . $i];
            } else {
                $data['position'] = '';
            }
            if ($_POST['name' . $i] != '') {
                $data['search_by_name'] = $_POST['name' . $i];
            } else {
                $data['search_by_name'] = '';
            }
            if ($_POST['team' . $i] != '') {
                $data['photo'] = $_POST['team' . $i];
            } else {
                $data['photo'] = $request->oldTeamPhoto[$j];
            }
            $data['user_id'] = $userId;
            if ($request->oldTeamid[$j] != '' || $request->oldTeamid[$j] != NULL) {
                DB::table('production_my_team')->where('id', $request->oldTeamid[$j])->update($data);
            } else {
                if (($data['position'] != '' || $data['position'] != NULL) || ($data['search_by_name'] != '' || $data['search_by_name'] != NULL)) {
                    DB::table('production_my_team')->insert($data);
                }
            }
            $j++;
        }
    }

    function deleteTeam($id)
    {
        DB::table('production_my_team')->where('id', $id)->delete();
        return Redirect::to('/create-an-production-house-profile?status=success');
    }

    function deleteTeamImage($id)
    {
        DB::table('production_my_team')->where('id', $id)->update(['photo' => NULL]);
        return Redirect::to('/create-an-production-house-profile?status=success');
    }

    function saveProductionContact(Request $request)
    {
        $data = array();
        $userId = session()->get('user_id');
        $getIfAlready = DB::select('select * from production_contact_us where user_id=' . $userId);
        $data['user_id'] = $userId;
        $data['title'] = $request->titlecontact;
        $data['description'] = $request->descriptioncontact;
        $data['number'] = $request->numbercontact;
        $data['email_id'] = $request->emailcontact;
        $data['address'] = $request->addresscontact;
        $data['landline_number'] = $request->phonecontact;
        $data['whatsapp_tick'] = (isset($request->tick_whatsapp_feature) ? 'yes' : 'no');
        if (count($getIfAlready) == 0) {
            DB::table('production_contact_us')->insert($data);
        } else {
            DB::table('production_contact_us')->where('user_id', $userId)->update($data);
        }
    }

    function saveContact(Request $request)
    {
        $data = array();
        $userId = session()->get('user_id');
        $getIfAlready = DB::select('select * from artist_contact where user_id=' . $userId);
        $data['user_id'] = $userId;
        $data['title'] = $request->titlecontact;
        $data['description'] = $request->descriptioncontact;
        $data['phone'] = $request->phonecontact;
        $data['email'] = $request->emailcontact;
        $data['facebook'] = $request->facebookcontact;
        $data['instagram'] = $request->instacontact;
        $data['tick_whatsapp_feature'] = (isset($request->tick_whatsapp_feature) ? 'yes' : 'no');
        $data['youtube'] = $request->youtubecontact;
        if (count($getIfAlready) == 0) {
            DB::table('artist_contact')->insert($data);
        } else {
            DB::table('artist_contact')->where('user_id', $userId)->update($data);
        }
    }

    function saveproductionachivement(Request $request)
    {
        $userId = session()->get('user_id');
        $j = 0;
        for ($i = 0; $i < 3; $i++) {
            $j++;
            $data = array();
            if ($_POST['titleachieve'][$i] != '') {
                $data['title_1'] = $request->titleachieve[$i];
            } else {
                $data['title_1'] = '';
            }
            if ($_POST['linkachieve'][$i] != '') {
                $data['link_1'] = $request->linkachieve[$i];
            } else {
                $data['link_1'] = '';
            }
            if ($_POST['date'][$i] != '') {
                $data['date_1'] = $request->date[$i];
            } else {
                $data['date_1'] = '';
            }
            if ($_POST['descachieve'][$i] != '') {
                $data['des_1'] = $request->descachieve[$i];
            } else {
                $data['des_1'] = '';
            }
            if ($_POST['fileachive' . $j] != '') {
                $data['photo_1'] = $_POST['fileachive' . $j];
            } else {
                $data['photo_1'] = $request->oldFile[$i];
            }
            $data['user_id'] = $userId;
            if ($data['title_1'] != NULL || $data['link_1'] != NULL || $data['date_1'] != NULL || $data['des_1'] != NULL || $data['photo_1'] != NULL) {
                if ($request->id[$i] != '' || $request->id[$i] != NULL) {
                    DB::table('production_awards')->where('id', $request->id[$i])->update($data);
                } else {
                    DB::table('production_awards')->insert($data);
                }
            }
        }
    }

    function saveachivement(Request $request)
    {
        $userId = session()->get('user_id');
        $user = User::find($userId);

        if ($user) {
            $length = $user->hasThemePlan() ? \App\Models\ArtistAchivement::$premiumBlocks : \App\Models\ArtistAchivement::$blocks;
            $j = 0;
            for ($i = 0; $i < $length; $i++) {
                $j++;
                $data = array();
                if ($_POST['titleachieve'][$i] != '') {
                    $data['title_1'] = $request->titleachieve[$i];
                } else {
                    $data['title_1'] = '';
                }
                if ($_POST['linkachieve'][$i] != '') {
                    $data['link_1'] = $request->linkachieve[$i];
                } else {
                    $data['link_1'] = '';
                }
                if ($_POST['date'][$i] != '') {
                    $data['date_1'] = $request->date[$i];
                } else {
                    $data['date_1'] = '';
                }
                if ($_POST['descachieve'][$i] != '') {
                    $data['des_1'] = $request->descachieve[$i];
                } else {
                    $data['des_1'] = '';
                }
                if ($_POST['fileachive' . $j] != '') {
                    $data['photo_1'] = $_POST['fileachive' . $j];
                } else {
                    $data['photo_1'] = $request->oldFile[$i];
                }
                $data['user_id'] = $userId;
                if ($data['title_1'] != NULL || $data['link_1'] != NULL || $data['date_1'] != NULL || $data['des_1'] != NULL || $data['photo_1'] != NULL) {
                    if ($request->id[$i] != '' || $request->id[$i] != NULL) {
                        DB::table('artist_achievment')->where('id', $request->id[$i])->update($data);
                    } else {
                        DB::table('artist_achievment')->insert($data);
                    }
                }
            }
        }
    }

    function savereel(Request $request)
    {
        $data = $request->all();
        unset($data['_token']);
        $userId = session()->get('user_id');
        $data['user_id'] = $userId;
        if ($request->video != '') {
            $data['video'] = TEMP_DISPLAY_PATH . $request->video;
        } else {
            unset($data['video']);
        }
        if ($request->video_thumb != '') {
            $data['video_thumb'] = $request->video_thumb;
        } else {
            if ($request->generated_video_thumb != '') {
                $data['video_thumb'] = $request->generated_video_thumb;
            } else {
                unset($data['video_thumb']);
            }
        }
        unset($data['generated_video_thumb']);
        $getIfAlready = DB::select('select * from artist_showreel where user_id=' . $userId);
        if (count($getIfAlready) == 0) {
            DB::table('artist_showreel')->insert($data);
        } else {
            DB::table('artist_showreel')->where('user_id', $userId)->update($data);
        }
    }

    function saveGallery(Request $request)
    {
        $userId = session()->get('user_id');
        $user = User::find($userId);

        if ($user) {
            $length = $user->hasThemePlan() ? \App\Models\ArtistGallery::$premiumBlocks : \App\Models\ArtistGallery::$blocks;
            for ($i = 0; $i < $length; $i++) {
                $data = array();
                $j = $i + 1;
                if (count($request->photo_title) > 0) {
                    $data['photo_title'] = $request->photo_title[$i];
                }
                if (count($request->photo_category) > 0) {
                    $data['category'] = $request->photo_category[$i];
                }
                if (($_POST['photosectionphoto' . $j]) != '') {
                    $data['photo'] = $_POST['photosectionphoto' . $j];
                } else {
                    $data['photo'] = $request->oldPhotoFile[$i];
                }

                $data['user_id'] = $userId;
                $data['slug'] = Str::slug($data['photo_title']);
                if ($request->oldPhotoid[$i] != '' || $request->oldPhotoid[$i] != NULL) {
                    $data['slug'] = $data['slug'] . "-" . $request->oldPhotoid[$i];
                    DB::table('artist_gallery')->where('id', $request->oldPhotoid[$i])->update($data);
                } else {
                    if (($data['photo_title'] != '' || $data['photo_title'] != NULL) || ($data['category'] != '' || $data['category'] != NULL) || ($data['photo'] != '' || $data['photo'] != NULL)) {
                        $id = DB::table('artist_gallery')->insertGetId($data);
                        DB::table("artist_gallery")->where(['id' => $id])->update(['slug' => $data['slug'] . "-" . $id]);
                    }
                }
            }
        }
    }

    function saveProductionGallery(Request $request)
    {
        $userId = session()->get('user_id');
        for ($i = 0; $i < 12; $i++) {
            $data = array();
            if (count($request->photo_title) > 0) {
                $data['photo_title'] = $request->photo_title[$i];
            }
            if (count($request->photo_category) > 0) {
                $data['category'] = $request->photo_category[$i];
            }
            if (($_POST['photosectionphoto' . $i]) != '') {
                $data['photo'] = $_POST['photosectionphoto' . $i];
            } else {
                $data['photo'] = $request->oldPhotoFile[$i];
            }
            $data['user_id'] = $userId;
            if ($request->oldPhotoid[$i] != '' || $request->oldPhotoid[$i] != NULL) {
                DB::table('production_gallery')->where('id', $request->oldPhotoid[$i])->update($data);
            } else {
                if (($data['photo_title'] != '' && $data['photo_title'] != NULL) || ($data['category'] != '' && $data['category'] != NULL) || ($data['photo'] != '' && $data['photo'] != NULL)) {
                    DB::table('production_gallery')->insert($data);
                }
            }
        }
    }

    function saveproductionproject(Request $request)
    {
        $userId = session()->get('user_id');
        $j = 0;
        for ($i = 0; $i < 3; $i++) {
            $j++;
            $data = array();
            if (isset($request->categoryproject[$i])) {
                $data['category'] = $request->categoryproject[$i];
            } else {
                $data['category'] = '';
            }
            if (isset($request->projecttitle[$i])) {
                $data['title'] = $request->projecttitle[$i];
            } else {
                $data['title'] = '';
            }
            if (isset($request->projectdate[$i])) {
                $data['date'] = $request->projectdate[$i];
            } else {
                $data['date'] = '';
            }
            if (isset($request->projectlink[$i])) {
                $data['link'] = $request->projectlink[$i];
            } else {
                $data['link'] = '';
            }
            if (isset($request->projectbuttonname[$i])) {
                $data['button_name'] = $request->projectbuttonname[$i];
            } else {
                $data['button_name'] = '';
            }
            if (isset($request->projectdesc[$i])) {
                $data['description'] = $request->projectdesc[$i];
            } else {
                $data['description'] = '';
            }
            if ($_POST['fileproj' . $j] != '') {
                $data['photo'] = $_POST['fileproj' . $j];
            } else {
                $data['photo'] = $request->oldProjFile[$i];
            }
            $data['user_id'] = $userId;
            if (($data['category'] != NULL || $data['category'] != '') || ($data['title'] != NULL || $data['title'] != '') || ($data['date'] != NULL || $data['date'] != '') || ($data['link'] != NULL || $data['link'] != '') || ($data['button_name'] != NULL || $data['button_name'] != '') || ($data['description'] != NULL || $data['description'] != '')) {
                if ($request->oldProjid[$i] != '' || $request->oldProjid[$i] != NULL) {
                    DB::table('production_latest_project')->where('id', $request->oldProjid[$i])->update($data);
                } else {
                    DB::table('production_latest_project')->insert($data);
                }
            }
        }
    }

    function saveproject(Request $request)
    {
        $userId = session()->get('user_id');
        $j = 0;

        $user = User::find($userId);

        if ($user) {
            $length = $user->hasThemePlan() ? \App\Models\ArtistProject::$premiumBlocks : \App\Models\ArtistProject::$blocks;
            for ($i = 0; $i < $length; $i++) {
                $j++;
                $data = array();
                if (array_key_exists($i, $request->categoryproject)) {
                    $data['category'] = $request->categoryproject[$i];
                }
                if (array_key_exists($i, $request->projecttitle)) {
                    $data['title'] = $request->projecttitle[$i];
                }
                if (array_key_exists($i, $request->projectdate)) {
                    $data['date'] = $request->projectdate[$i];
                }
                if (array_key_exists($i, $request->projectlink)) {
                    $data['link'] = $request->projectlink[$i];
                }
                if (array_key_exists($i, $request->projectbuttonname)) {
                    $data['button_name'] = $request->projectbuttonname[$i];
                }
                if (array_key_exists($i, $request->projectdesc)) {
                    $data['description'] = $request->projectdesc[$i];
                }
                if ($_POST['fileproj' . $j] != '') {
                    $data['photo'] = $_POST['fileproj' . $j];
                } else {
                    $data['photo'] = $request->oldProjFile[$i];
                }
                $data['user_id'] = $userId;
                if (($data['category'] != NULL || $data['category'] != '') || ($data['title'] != NULL || $data['title'] != '') || ($data['date'] != NULL || $data['date'] != '') || ($data['link'] != NULL || $data['link'] != '') || ($data['button_name'] != NULL || $data['button_name'] != '') || ($data['description'] != NULL || $data['description'] != '')) {
                    if ($request->oldProjid[$i] != '' || $request->oldProjid[$i] != NULL) {
                        DB::table('artist_project')->where('id', $request->oldProjid[$i])->update($data);
                    } else {
                        DB::table('artist_project')->insert($data);
                    }
                }
            }
        }
    }

    function saveProductionVideo(Request $request)
    {
        $userId = session()->get('user_id');
        $j = 0;
        for ($i = 0; $i < 6; $i++) {
            $j++;
            $data = array();
            if (count($request->videotitle) > 0) {
                $data['video_title'] = $request->videotitle[$i];
            }
            if (count($request->categoryvideo) > 0) {
                $data['category'] = $request->categoryvideo[$i];
            }
            if (count($request->videodesc) > 0) {
                $data['description'] = $request->videodesc[$i];
            }
            $data['user_id'] = $userId;
            if ($_POST['uploadedvideosec' . $j] != '') {
                $data['video'] = TEMP_DISPLAY_PATH . $_POST['uploadedvideosec' . $j];
            } else {
                $data['video'] = $request->oldVidFile[$i];
            }
            if ($_POST['videoThumbFile' . $j]) {
                $data['thumb'] = $_POST['videoThumbFile' . $j];
            } else {
                if ($_POST['generatedvideosecthumb' . $j] != '') {
                    $image_data = $_POST['generatedvideosecthumb' . $j];
                    $image_array_1 = explode(";", $image_data);
                    $image_array_2 = explode(",", $image_array_1[1]);
                    $imagedata = base64_decode($image_array_2[1]);
                    $image_name = time() . '.png';
                    $destinationPath = TEMP_UPLOAD_PATH . '/' . $image_name;
                    file_put_contents($destinationPath, $imagedata);
                    $data['thumb'] = TEMP_DISPLAY_PATH . $image_name;
                } else {
                    $data['thumb'] = $request->oldThumbFile[$i];
                }
            }
            if ($request->oldVidid[$i] != '' || $request->oldVidid[$i] != NULL) {
                DB::table('production_video')->where('id', $request->oldVidid[$i])->update($data);
            } else {
                if (($data['video_title'] != '' || $data['video_title'] != NULL) || ($data['category'] != '' || $data['category'] != NULL) || ($data['description'] != '' || $data['description'] != NULL) || ($data['thumb'] != '' || $data['thumb'] != NULL)) {
                    DB::table('production_video')->insert($data);
                }
            }
        }
    }

    function saveVideo(Request $request)
    {
        $userId = session()->get('user_id');
        $j = 0;
        $user = User::find($userId);

        if ($user) {
            $length = $user->hasThemePlan() ? \App\Models\ArtistVideo::$premiumBlocks : \App\Models\ArtistVideo::$blocks;
            for ($i = 0; $i < $length; $i++) {
                $j++;
                $data = array();
                if (count($request->videotitle) > 0) {
                    $data['video_title'] = $request->videotitle[$i];
                }
                if (count($request->categoryvideo) > 0) {
                    $data['category'] = $request->categoryvideo[$i];
                }
                if (count($request->videodesc) > 0) {
                    $data['description'] = $request->videodesc[$i];
                }
                $data['user_id'] = $userId;
                if ($_POST['uploadedvideosec' . $j] != '') {
                    $data['video'] = TEMP_DISPLAY_PATH . $_POST['uploadedvideosec' . $j];
                } else {
                    $data['video'] = $request->oldVidFile[$i];
                }
                if ($_POST['videoThumbFile' . $j]) {
                    $data['thumb'] = $_POST['videoThumbFile' . $j];
                } else {
                    if ($_POST['generatedvideosecthumb' . $j] != '') {
                        $image_data = $_POST['generatedvideosecthumb' . $j];
                        $image_array_1 = explode(";", $image_data);
                        $image_array_2 = explode(",", $image_array_1[1]);
                        $imagedata = base64_decode($image_array_2[1]);
                        $image_name = time() . '.png';
                        $destinationPath = TEMP_UPLOAD_PATH . '/' . $image_name;
                        file_put_contents($destinationPath, $imagedata);
                        $data['thumb'] = TEMP_DISPLAY_PATH . $image_name;
                    } else {
                        $data['thumb'] = $request->oldThumbFile[$i];
                    }
                }
                if ($request->oldVidid[$i] != '' || $request->oldVidid[$i] != NULL) {
                    $data['slug'] = Str::slug($data['video_title']) . "-" . $request->oldVidid[$i];
                    DB::table('artist_video')->where('id', $request->oldVidid[$i])->update($data);
                } else {
                    if (($data['video_title'] != '' || $data['video_title'] != NULL) || ($data['category'] != '' || $data['category'] != NULL) || ($data['description'] != '' || $data['description'] != NULL) || ($data['thumb'] != '' || $data['thumb'] != NULL)) {
                        $data['slug'] = Str::slug($data['video_title']);
                        $id = DB::table('artist_video')->insertGetId($data);

                        if ($id) {
                            DB::table('artist_video')->where('id', $id)->update(['slug' => $data['slug'] . "-" . $id]);
                        }
                    }
                }
            }
        }
    }

    function productionwhatwedoprofile(Request $request)
    {
        $data = $request->all();
        $userId = session()->get('user_id');
        $getIfAlready = DB::select('select * from production_what_we_do where user_id=' . $userId);
        $data['user_id'] = $userId;
        if (count($request->title) > 0) {
            foreach ($request->title as $k => $v) {
                $data['title_' . ++$k] = $v;
            }
        }
        if (count($request->desc) > 0) {
            foreach ($request->desc as $k => $v) {
                $data['des_' . ++$k] = $v;
            }
        }
        unset($data['title']);
        unset($data['desc']);
        unset($data['_token']);
        if (count($getIfAlready) == 0) {
            DB::table('production_what_we_do')->insert($data);
        } else {
            DB::table('production_what_we_do')->where('user_id', $userId)->update($data);
        }
    }

    function savewhatwedoprofile(Request $request)
    {
        $data = $request->all();
        $userId = session()->get('user_id');
        $getIfAlready = DB::select('select * from artist_what_we_do where user_id=' . $userId);
        $data['user_id'] = $userId;
        if (count($request->title) > 0) {
            foreach ($request->title as $k => $v) {
                $data['title_' . ++$k] = $v;
            }
        }
        if (count($request->desc) > 0) {
            foreach ($request->desc as $k => $v) {
                $data['des_' . ++$k] = $v;
            }
        }
        unset($data['title']);
        unset($data['desc']);
        unset($data['_token']);
        if (count($getIfAlready) == 0) {
            DB::table('artist_what_we_do')->insert($data);
        } else {
            DB::table('artist_what_we_do')->where('user_id', $userId)->update($data);
        }
    }

    function saveproductionprofile(Request $request)
    {
        $data = $request->all();
        $userId = session()->get('user_id');
        $getIfAlready = DB::select('select * from production_about where user_id=' . $userId);
        $usernameExists = DB::table('production_about')->where('username', $data['username'])->first();
        if ($usernameExists) {
            if ($data['current_user'] != $data['username']) {
                if ($usernameExists != null) {
                    \Session::put('error', 'Username exists');
                    // return false;
                    // dd('check this');
                    echo 'exists';
                    exit;
                }
            }
        }


        if ($request->myfile != '') {
            $image_data = $request->myfile;
            $image_array_1 = explode(";", $image_data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $imagedata = base64_decode($image_array_2[1]);
            $image_name = time() . '.png';
            $destinationPath = TEMP_UPLOAD_PATH . '/' . $image_name;
            file_put_contents($destinationPath, $imagedata);
            $data['profile_photo'] = TEMP_DISPLAY_PATH . $image_name;
        }
        unset($data['cropImage']);
        unset($data['myfile']);
        unset($data['current_user']);
        $data['user_id'] = $userId;
        unset($data['phousecheck']);
        unset($data['_token']);
        $data['multiple_category'] = implode(',', $request->phousecheck);

        if ($_POST['uploadedvideosec_3'] != '') {
            $data['video'] = TEMP_DISPLAY_PATH . $_POST['uploadedvideosec_3'];
        }

        if ($request->about_thumb && !(empty($request->about_thumb))) {
            $data['thumb'] = $request->about_thumb;
        } else {
            if ($_POST['generatedvideosecthumb_3'] != '') {
                $image_data = $_POST['generatedvideosecthumb_3'];
                $image_array_1 = explode(";", $image_data);
                $image_array_2 = explode(",", $image_array_1[1]);
                $imagedata = base64_decode($image_array_2[1]);
                $image_name = time() . '.png';
                $destinationPath = TEMP_UPLOAD_PATH . '/' . $image_name;
                file_put_contents($destinationPath, $imagedata);
                $data['thumb'] = TEMP_DISPLAY_PATH . $image_name;
            }
        }

        unset($data['uploadedvideosec_3']);
        unset($data['generatedvideosecthumb_3']);
        unset($data['about_thumb']);

        if (count($getIfAlready) == 0) {
            if (!isset($data['video']) || empty($data['video'])) {
                $data['video'] = '';
            }
            if (!isset($data['thumb']) || empty($data['thumb'])) {
                $data['thumb'] = '';
            }
            DB::table('production_about')->insert($data);
        } else {
            DB::table('production_about')->where('user_id', $userId)->update($data);
        }
        \Session::put('success', 'Updated successfully');

    }

    function saveauditionprofile(Request $request)
    {
        $data = $request->all();
        $userId = session()->get('user_id');
        $user = User::find($userId);
        $rules = array(
            'title' => 'required|max:150'
        );

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        } else {
            if ($request->tp) {
                $getIfAlready = DB::select('select * from post_audtion where user_id=' . $userId . ' and id=' . $request->tp);
            } else {
                $getIfAlready = array();
            }
            if ($request->myfile != '') {
                $image_data = $request->myfile;
                $image_array_1 = explode(";", $image_data);
                $image_array_2 = explode(",", $image_array_1[1]);
                $imagedata = base64_decode($image_array_2[1]);
                $image_name = time() . '.png';
                $destinationPath = TEMP_UPLOAD_PATH . '/' . $image_name;
                file_put_contents($destinationPath, $imagedata);
                $data['photo'] = TEMP_DISPLAY_PATH . $image_name;
            }
            $data['slug'] = Str::slug($data['title']);
            unset($data['cropImage']);
            unset($data['myfile']);
            $data['user_id'] = $userId;
            unset($data['category']);
            unset($data['_token']);
            unset($data['tp']);
            $data['category'] = implode(',', $request->category);

            $status = "";
            if (count($getIfAlready) == 0) {
                $audition = PostAudtion::create($data);
                $audition->slug = $audition->slug . "-" . $audition->id;
                $audition->save();
                $status = "Audition created successfully";

                send_mail('audition-posted', $user->email, ['template' => 'email.audition-posted', 'subject' => 'Audition posted']);
                $user->notify(new Notices($user, ' Audition job is posted successfully', 'audition-post', '', '', '', $audition->id));
                return response()->json(["status" => 1, "redirectTo" => url("post-audition/?tp=" . $audition->id), "message" => $status], 201);
            } else {
                $data['slug'] = $data['slug'] . "-" . $request->tp;
                DB::table('post_audtion')->where('id', $request->tp)->update($data);
                $status = "Audition updated successfully";
                return response()->json(["status" => 1, "redirectTo" => url("post-audition/?tp=" . $request->tp), "message" => $status], 200);
            }
        }
    }

    function saveartistprofile(Request $request)
    {
        $data = $request->all();
        // dd($data);
        $userId = session()->get('user_id');
        $getIfAlready = DB::select('select * from artist_about where user_id=' . $userId);
        $usernameExists = DB::table('artist_about')->where('username', $data['username'])->first();

        if ($usernameExists) {
            if ($data['current_user'] != $data['username']) {
                if ($usernameExists != null) {
                    Session::put('error', 'Username exists');
                    // return false;
                    // dd('check this');
                    echo 'exists';
                    exit;
                }
            }
        }

        if ($request->myfile != '') {
            $data['profile_photo'] = $request->myfile;
        }

        if ($request->profile_bkg != '') {
            $data['profile_bkg'] = $request->profile_bkg;
        }
        unset($data['current_user']);
        unset($data['cropImage']);
        unset($data['myfile']);
        $data['user_id'] = $userId;
        unset($data['artistcheck']);
        unset($data['_token']);
        $data['multiple_category'] = implode(',', $request->artistcheck);
        if (count($getIfAlready) == 0) {
            DB::table('artist_about')->insert($data);
        } else {
            if (!$data['profile_bkg']) {
                unset($data['profile_bkg']);
            }
            DB::table('artist_about')->where('user_id', $userId)->update($data);
        }
        \Session::put('success', 'Updated successfully');
    }


    public function messages()
    {
        $pageTitle = 'Messages';
        return view('homes.messages', ['title' => $pageTitle]);
    }

    function createProdTemp()
    {
        $pageTitle = 'Create an Production House Profile';
        return view('production-house.create-an-production-house-profile-temp', ['title' => $pageTitle]);
    }

    public function audition()
    {
        $pageTitle = 'Audition';
        return view('audition.audition', ['title' => $pageTitle]);
    }

    public function postaudition(Request $request)
    {
        $pageTitle = 'Post Audition';
        $userId = session()->get('user_id');
        $user = User::find(Session::get('user_id'));

        if ($request->tp) {
            $getIfAlready = DB::select('select * from post_audtion where user_id=' . $userId . ' and id=' . $request->tp);
        } else {
            $getIfAlready = array();
        }

        return view('audition.post-audition', ['title' => $pageTitle, 'getIfAlready' => $getIfAlready, 'userId' => $userId]);

    }

    /*Upload Video*/
    public function uploadVideo(Request $request)
    {
        $pageTitle = "Upload Video";
        $user = User::find($request->session()->get('user_id'));

        $videoCategories = Category::getCategoryList();

        $videoCategories2 = DB::table('categories')
            ->select('*')
            ->where('parent_id', 0)
            ->get();
        $input = $request->all();

        if ($input) {

            $rules = array(
                'video' => 'required|max:100',
                'title' => 'required|max:100',
                'about' => 'required',
                'tags' => 'required',
            );
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                //    return response()->json(array(
                //     'success' => false,
                //     'errors' => $validator->getMessageBag()->toArray()

                // ), 400);
                return Redirect::to('/upload-video')->withErrors($validator)->withInput();
            } else {

                $videoData = new Video();
                $videoData['title'] = $input['title'];
                $videoData['description'] = $input['about'];
                // $videoData['feature'] = $input['feature'];
                $videoData['sub_title'] = ''; //$input['sub_title'];
                // $videoData['privacy_setting'] = $input['privacy_setting'];
                $videoData['language'] = $input['language'];
                $videoData['status'] = 1;
                $videoData['user_id'] = Session::get('user_id');
                $videoData['video_key'] = $this->__generateRandomString(12);
                /*Video Data*/
                $videoTempPath = TEMP_UPLOAD_PATH . $input['video'];
                if (file_exists($videoTempPath)) {
                    $videoData['video'] = $input['video'];
                    $videoSize = filesize($videoTempPath);
                    $videoData['video_size'] = $videoSize / 1024;
                    $duration = 60;

                    $videoData['video_time'] = $input['duration'] ?? "0";
                    /*Move video file in video main folder*/
                    $destinationPath = VIDEO_UPLOAD_PATH . $input['video'];
                    \File::copy($videoTempPath, $destinationPath);
                    \File::delete($videoTempPath);
                    /*Save Video*/
                    $videoData->save();
                    $videoId = $videoData->id;

                    /*Save Video Categories*/
                    if (!empty($input['categories'])) {
                        foreach ($input['categories'] as $categoryId) {
                            $videoCategory = new VideoCategory();
                            $videoCategory['video_id'] = $videoId;
                            $videoCategory['category_id'] = $categoryId;
                            $videoCategory->save();
                        }
                    }
                    /*Save Thumbnail*/
                    if (!empty($input['thumb'])) {
                        $total = count($input['thumb']);
                        foreach ($input['thumb'] as $k => $thumbImage) {
                            $thumbImage = str_replace(VIDEO_THUMB_DISPLAY_PATH, "", $thumbImage);
                            $thumbDestinationPath = VIDEO_THUMB_UPLOAD_PATH . $thumbImage;
                            if (!empty($thumbImage) && file_exists($thumbDestinationPath)) {
                                $default = 0;
                                $videoThumb = new VideoThumb();
                                $videoThumb['video_id'] = $videoId;
                                $videoThumb['thumb'] = $thumbImage;
                                if (!empty($input['selected_thumb'])) {
                                    if ($thumbImage == $input['selected_thumb']) {
                                        $default = 1;
                                    }
                                } else {
                                    if ($k == $total) {
                                        $default = 1;
                                    }
                                }
                                /*Save Video Thumb*/
                                $videoThumb->save();
                                if ($default == 1) {
                                    /*Update Video Thum*/
                                    $thumbId = $videoThumb->id;
                                    $videoUpdate = Video::find($videoId);
                                    if ($videoUpdate) {
                                        $videoUpdate->video_thumb_id = $thumbId;
                                        $videoUpdate->save();
                                    }
                                }

                            }

                        }
                    } else {
                        $fileName = str_replace('mp4', 'png', $input['video']);
                        $vTempPath = public_path(env('VIDEO_PATH') . $input['video']);

                        $uploadpath = public_path(env('VIDEO_THUMBNAIL_PATH'));
                        //Thumbnail::getThumbnail($destinationPath, VIDEO_THUMB_UPLOAD_PATH, $fileName, 2, $width = 640, $height = 320);
                        VideoThumbnail::createThumbnail($vTempPath, $uploadpath, $fileName, 2, $width = 640, $height = 320);
                        // VideoThumbnail::createThumbnail($destinationPath, VIDEO_THUMB_UPLOAD_PATH, $fileName, 2, $width = 640, $height = 320);
                        $videoThumb = new VideoThumb();
                        $videoThumb['video_id'] = $videoId;
                        $videoThumb['thumb'] = $fileName;
                        $videoThumb->save();

                        $thumbId = $videoThumb->id;
                        $videoUpdate = Video::find($videoId);
                        if ($videoUpdate) {
                            $videoUpdate->video_thumb_id = $thumbId;
                            $videoUpdate->save();
                        }
                    }

                    /*Video Tags*/
                    if (!empty($input['tags'])) {
                        $tags = explode(',', $input['tags']);
                        if (!empty($tags)) {
                            foreach ($tags as $tag) {
                                /*Check tag in tags table*/
                                $tagId = Tag::getTagIdByName($tag);
                                /*Save tag name in Tag Table*/
                                if (empty($tagId)) {
                                    $tagData = new Tag();
                                    $tagData['tag'] = $tag;
                                    $tagData->save();
                                    $tagId = $tagData->id;
                                }
                                /*Save TagId in Video Tag Table*/
                                $checkVideoTag = VideoTag::where('tag_id', '=', $tagId)->where('video_id', '=', $videoId)->count();
                                if (empty($checkVideoTag)) {
                                    $videoTag = new VideoTag();
                                    $videoTag['tag_id'] = $tagId;
                                    $videoTag['video_id'] = $videoId;
                                    $videoTag->save();
                                }
                            }
                        }
                    }
                    if (!empty($input['tempid'])) {
                        $tempvideo = DB::table('temp_files')->where(['user_id' => session()->get('user_id'), 'id' => $input['tempid']])->delete();


                    } else {
                        DB::table('temp_files')->where(['user_id' => session()->get('user_id')])->delete();
                    }


                    /*Video Language*/
                    $user->notify(new VideoUploaded($user, $videoData));
                    return redirect()->to('/' . $request->session()->get('slug'))->with('success', 'Your video uploaded Successfully');
                } else {

                    return Redirect::to('/upload-video?status=error');
                }
            }
        }

        return view('homes.upload_video',
            ['title' => $pageTitle, 'videoCategories' => $videoCategories, 'videoCategories2' => $videoCategories2]
        );
    }

    public function complatevideo($id)
    {
        $pageTitle = "Upload Video";
        $videoCategories = Category::getCategoryList();
        $tempvideo = DB::table('temp_files')->where('id', $id)->first();
        if ($tempvideo) {
            return view('homes.upload_video_procced', ['title' => $pageTitle, 'tempvideo' => $tempvideo, 'videoCategories' => $videoCategories]);

        }
        return abort(404);
    }

    public function photostag($slug)
    {
        $getPhotoList = UploadPhotos::where('tag_list', 'like', '%' . $slug . '%')->holdUser()->select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->with('user')
            ->whereRaw("upload_photos.isDeleted=? AND upload_photos.id NOT IN (SELECT post_id from promote_posts WHERE `isDeleted`=? AND post_type=? AND `status`=? AND `is_paid`=? AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))", [0, 0, 'photo', 1, 1])
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->inRandomOrder()
            ->groupBY('upload_photos.id')
            ->get();

        $pageTitle = 'Photos Tag';
        $days = Carbon::now()->subDays(15);
        $all_image = UploadPhotos::where([['upload_photos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->whereRaw("upload_photos.id NOT IN (SELECT post_id from promote_posts WHERE post_type='photo' AND `status`='1' AND `is_paid`='1')")
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->leftjoin('users', 'upload_photos.user_id', '=', 'users.id')
            ->where('users.isDeleted', '=', 0)
            ->where('upload_photos.isDeleted', '=', 0)
            ->groupBY('upload_photos.id')
            ->orderBy('id', 'desc')
            ->get();

        $array = [];

        if (count($all_image) > 0) {
            foreach ($all_image->sortByDesc('total')->values() as $reward) {

                $array[] = preg_split('/[^a-zA-Z0-9_ \-()\/%-&]/s', trim(str_replace(array("#", "'", ";"), '', $reward->tag_list)));

            }
        }
        $totalrecords = UploadPhotos::holdUser()->select('*')->where('upload_photos.isDeleted', 0)->count();
        return view('homes.photostag', ['title' => $pageTitle, 'getPhotoList' => $getPhotoList, 'totalrecords' => $totalrecords, 'tags' => array_unique(array_merge(...$array))]);
    }

    public function alltagphotos()
    {
        $pageTitle = 'All Photos Tag';
        $days = Carbon::now()->subDays(30);
        $latestPhotos = UploadPhotos::where([['upload_photos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->holdUser()->select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->with(['user' => function ($query) {
                $query->where('isDeleted', 0);
            }])
            ->whereRaw("upload_photos.isDeleted=? AND upload_photos.id NOT IN (SELECT post_id from promote_posts WHERE `isDeleted`=? AND post_type=? AND `status`=? AND `is_paid`=? AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))", [0, 0, 'photo', 1, 1])
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            //->take($this->rowperpage)
            ->latest()
            ->groupBY('upload_photos.id')
            ->inRandomOrder()
            ->get();
        $array = [];

        if (count($latestPhotos) > 0) {
            foreach ($latestPhotos->sortByDesc('total')->values() as $reward) {

                $array[] = preg_split('/[^a-zA-Z0-9_ \-()\/%-&]/s', trim(str_replace(array("#", "'", ";"), '', $reward->tag_list)));

            }
        }

        return view('homes.alltagphotos', ['title' => $pageTitle, 'tags' => array_unique(array_merge(...$array)), 'latestPhotos' => $latestPhotos, 'totalrecords' => 30]);
    }

    /*Edit Video*/
    public function editVideo($video_key)
    {
        $pageTitle = "Edit Video";

        $videoCategories = DB::table('categories')
            ->select('*')
            ->where('parent_id', 0)
            ->get();
        //$input = $request->all();
        //$videoCategories = Category::getCategoryList();
        $input = Input::all();

        if (!empty($input)) {

            $rules = array(
                'title' => 'required|max:100',
                'description' => 'required',
                'tags' => 'required',

            );
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            } else {
                $videoData = Video::where(['video_key' => $video_key])->first();
                $videoData['title'] = $input['title'];
                $videoData['description'] = $input['description'];
                // $videoData['feature'] = $input['feature'];
                $videoData['sub_title'] = ''; //$input['sub_title'];
                $videoData['language'] = $input['language'];
                $videoData->save();
                $videoId = $videoData->id;

                /*Save Thumbnail*/
                if (!empty($input['thumb'])) {
                    $total = count($input['thumb']);
                    foreach ($input['thumb'] as $k => $thumbImage) {
                        $thumbImage = str_replace(VIDEO_THUMB_DISPLAY_PATH, "", $thumbImage);
                        $thumbDestinationPath = VIDEO_THUMB_UPLOAD_PATH . $thumbImage;
                        if (!empty($thumbImage) && file_exists($thumbDestinationPath)) {
                            $default = 0;
                            $videoThumb = new VideoThumb();
                            $videoThumb['video_id'] = $videoId;
                            $videoThumb['thumb'] = $thumbImage;
                            if (!empty($input['selected_thumb'])) {
                                if ($thumbImage == $input['selected_thumb']) {
                                    $default = 1;
                                }
                            } else {
                                if ($k == $total) {
                                    $default = 1;
                                }
                            }

                            /*Save Video Thumb*/
                            $videoThumb->save();
                            if ($default == 1) {
                                /*Update Video Thum*/
                                $thumbId = $videoThumb->id;
                                $videoUpdate = Video::find($videoId);
                                if ($videoUpdate) {
                                    $videoUpdate->video_thumb_id = $thumbId;
                                    $videoUpdate->save();
                                }
                            }
                        }
                    }
                }

                /*Save Video Categories*/
                if (!empty($input['categories'])) {
                    $videoCategory = VideoCategory::where(['video_id' => $videoId])->delete();
                    foreach ($input['categories'] as $categoryId) {
                        $videoCategory = new VideoCategory();
                        $videoCategory['video_id'] = $videoId;
                        $videoCategory['category_id'] = $categoryId;
                        $videoCategory->save();
                    }
                } else {
                    $videoCategory = VideoCategory::where(['video_id' => $videoId])->delete();
                }


                /*Video Tags*/
                if (!empty($input['tags'])) {
                    $tags = explode(',', $input['tags']);
                    if (!empty($tags)) {
                        $videoTag = VideoTag::where(['video_id' => $videoId])->delete();
                        foreach ($tags as $tag) {
                            /*Check tag in tags table*/
                            $tagId = Tag::getTagIdByName($tag);
                            /*Save tag name in Tag Table*/
                            if (empty($tagId)) {
                                $tagData = new Tag();
                                $tagData['tag'] = $tag;
                                $tagData->save();
                                $tagId = $tagData->id;
                            }
                            /*Save TagId in Video Tag Table*/
                            $checkVideoTag = VideoTag::where('tag_id', '=', $tagId)->where('video_id', '=', $videoId)->count();
                            if (empty($checkVideoTag)) {
                                $videoTag = new VideoTag();
                                $videoTag['tag_id'] = $tagId;
                                $videoTag['video_id'] = $videoId;
                                $videoTag->save();
                            }
                        }
                    }
                }
                /*Video Language*/
                return Redirect::to('/videos/' . Session::get('slug') . '?status=updated');
            }
        } else if (!empty($video_key)) {
            $videoData = Video::where(['video_key' => $video_key, 'status' => 1])
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    },
                    'user' => function ($thumb) {
                        $thumb->select('id', 'name', 'slug', 'photo');
                    },
                    'video_category' => function ($thumb) {
                        $thumb->select('video_id', 'category_id');
                    }
                ])
                ->first();

            $videoTags = VideoTag::getVideoTagName($videoData->id);

            return view(
                'homes.edit_video',
                ['title' => $pageTitle, 'videoCategories' => $videoCategories, 'videoData' => $videoData, 'videoTags' => $videoTags]
            );
        }

        return view(
            'homes.upload_video',
            ['title' => $pageTitle, 'videoCategories' => $videoCategories]
        );
    }


    //following

    public function following($category = null, Request $request)
    {
        $title = $pageTitle = "Category";

        $categories = Category::getCategoryList();
        $videos = Video::select('videos.id', 'videos.video_key', 'videos.title', 'videos.video_thumb_id', 'videos.video_time', 'videos.user_id', 'videos.created_at')
            // 		->orderBy('created_at','desc')
            ->join('followers', 'videos.user_id', '=', 'followers.follows_id')
            ->where('followers.user_id', '=', Session::get('user_id'))
            ->inRandomOrder()
            ->limit(5)
            ->get();

        if ($request->ajax()) {

            $view2 = view('elements.post.videos', ["videos" => $videos]);

            return response()->json(['html2' => $view2]);
        }


    }

    /*Video */
    public function videos($category = null, Request $request)
    {


        $title = $pageTitle = "Video";
        //$categories = Category::getCategoryList();


        $categories = DB::table('categories')
            ->select('categories.category')
            ->where('parent_id', 0)
            ->get();

        $videoData = Video::with(['user.artist' => function ($query) {
            $query->where('isDeleted', 0);
        }])->whereHas('video_category', function ($query) use ($category) {
            $query->whereHas(
                'category',
                function ($query) use ($category) {
                    $query->where('category', 'like', '%' . $category . '%');
                }
            );
        })
            ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'video', 'user_id', 'isRewardVideo', 'videos.created_at')
            ->orderBy('created_at', 'desc')
            ->where('isRewardVideo', 0)
            ->where('isDeleted', 0)
            ->whereRaw("videos.id NOT IN (SELECT post_id from promote_posts WHERE post_type='video' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }
            ])
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                }
            ])
            ->withCount(['isWatchedVideo'])
            ->withCount(['totalViews'])
            ->paginate(28);


        if ($request->ajax()) {
            $view = view('elements.post.categoryvideo', compact('title', 'categories', 'videoData'))->render();

            return response()->json(['html' => $view]);
        }

        // $locationwise = Video::join('artist_about','artist_about.user_id','=','videos.user_id')->where()

        $videos = Video::select('videos.id', 'videos.video_key', 'videos.title', 'videos.video_thumb_id', 'videos.video_time', 'videos.user_id', 'videos.created_at')
            ->join('followers', 'videos.user_id', '=', 'followers.follows_id')
            ->join('video_views', 'videos.id', '=', 'video_views.video_id')
            ->selectRaw('video_views.*, count(video_views.video_id) as total')
            ->groupBy('videos.id')
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                }
            ])
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }
            ])
            ->with(['user.artist' => function ($query) {
                $query->where('isDeleted', 0);
            }])
            ->where('videos.isDeleted', 0)
            ->where('followers.user_id', '=', Session::get('user_id'))
            ->withCount(['isWatchedVideo'])
            ->withCount(['totalViews'])
            ->take(20)
            ->inRandomOrder()
            ->get();
        //	return $videos;
// 		$history = WatchHistory::where(['watch_histories.user_id' => Session::get('user_id')])

// 			// ->orderBy('created_at','desc')
// 			->join('videos', 'watch_histories.video_id', '=', 'videos.id')

// 	        ->with([
// 				'video.user' => function ($user) {
// 					$user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
// 				}
// 			])
// 			->with([
// 				'thumb' => function ($thumb) {
// 					$thumb->select('id', 'thumb');
// 				}
// 			])
// 			->with(['video.user.artist'=>function($query){
// 		    $query->where('isDeleted',0);
// 		}])
// 			->with(['video.totalViews'])
// 			->groupBy('videos.id')

// 			//->where('followers.user_id', '=', Session::get('user_id'))
// 			->inRandomOrder()
// 			->limit(10)
// 			->get();
        $tages = VideoViews::where('video_views.user_id', Session::get('user_id') ?? 0)->join('video_categories', 'video_categories.video_id', '=', 'video_views.video_id')->get()->toArray();

        $history = [];
        $datas = [];
        if (count($tages) > 0) {

            foreach ($tages as $item) {
                $datas[] = $item['category_id'];
            }
            //return $datas;
            $history = Video::whereIn('video_categories.category_id', $datas)->select('videos.id', 'videos.video_key', 'videos.title', 'videos.video_thumb_id', 'videos.video_time', 'videos.user_id', 'videos.created_at', 'video_views.*', DB::raw('count(video_views.video_id) as total'))
                ->join('video_views', 'videos.id', '=', 'video_views.video_id')
                ->join('video_categories', 'video_categories.video_id', '=', 'videos.id')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                    }
                    , 'history'])
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with(['user.artist' => function ($query) {
                    $query->where('isDeleted', 0);
                }])
                ->where('videos.isDeleted', 0)
                ->withCount(['isWatchedVideo'])
                ->withCount(['totalViews'])
                ->inRandomOrder()
                ->get();
            // return $history;
        }
        $days = Carbon::now()->subDays(15);


        $tages = Video::where([['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where(['videos.isDeleted' => 0, 'videos.isRewardVideo' => 0])
            ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'videos.created_at')
            ->orderByDesc('id')
            ->holdUser()
            ->withCount(['totalViews'])
            ->with('watchTime')
            ->withCount(['video_likes'])
            ->withCount(['video_dislikes'])
            ->get();

        $array = [];

        if (count($tages) > 0) {
            foreach ($tages->sortByDesc('total_views_count')->values() as $reward) {
                foreach ($reward->video_tag as $tags) {
                    $array[] = $tags->tag->tag;
                }
            }
        }
        //return $array;
// 		$tags = DB::table('video_tags as vc')
// 			->select('vc.tag_id', 'users.*', 'videos.*', 'videos.id', 'videos.video_key', 'videos.title', 'videos.video_thumb_id', 'videos.video_time', 'videos.user_id', 'videos.created_at', DB::raw('count(vc.tag_id) as total'), 'c.tag')
// 			->join('videos', 'vc.video_id', '=', 'videos.id')
// 			->join('tags as c', 'c.id', '=', 'vc.tag_id')
// 			->join('users', 'videos.user_id', '=', 'users.id')
// 			->join('video_views', 'videos.id', '=', 'video_views.video_id')
// 			->selectRaw('video_views.*, count(video_views.video_id) as total')
// 			->groupBy('tag_id', 'c.tag')
// 			->orderBY(DB::raw('count(vc.video_id)'), 'DESC')
// 			->limit(7)
// 			->get();

// 		$trendingTagsVideo = DB::table('video_tags as vc')
// 			->select('vc.tag_id', DB::raw('count(vc.tag_id) as total'), 'c.tag')
// 			->join('tags as c', 'c.id', '=', 'vc.tag_id')
// 			->groupBy('tag_id', 'c.tag')
// 			->orderBY(DB::raw('count(video_id)'), 'DESC')
// 			->limit(20)
// 			->get();
        $trendingVideo = Video::select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
            ->orderBy('total_views_count', 'desc')
            ->where('isRewardVideo', 0)
            ->where("videos.isDeleted", 0)
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }
            ])
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                }
            ])
            ->holdUser()
            ->withCount(['isWatchedVideo'])
            ->withCount(['totalViews'])
            ->take(20)
            ->get();
        //return $history;
        return view('homes.video', ['title' => $pageTitle, 'videoData' => $videoData, 'categories' => $categories, 'videos' => $videos, 'history' => $history, 'trending' => $trendingVideo, 'tags' => array_unique($array)]);
    }

    /*Video Categories*/
    public function categories($category = null, Request $request)
    {
        $title = $pageTitle = "Video Categories";
        $category;


        $cat_id = DB::table('categories')
            ->select('*')
            ->where('category', $category)
            ->get();
        $cid = $cat_id[0]->id;

        $categories = DB::table('categories')
            ->select('categories.category')
            ->where('parent_id', $cid)
            ->get();


        //$categories = Category::getCategoryList();


        $videoData = Video::with(['user.artist' => function ($query) {
            $query->where('isDeleted', 0);
        }])->whereHas('video_category', function ($query) use ($category) {
            $query->whereHas(
                'category',
                function ($query) use ($category) {
                    $query->where('category', 'like', '%' . $category . '%');
                }
            );
        })
            ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
            ->orderBy('created_at', 'desc')
            ->where('isRewardVideo', 0)
            ->where('isDeleted', 0)
            ->whereRaw("videos.id NOT IN (SELECT post_id from promote_posts WHERE post_type='video' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }
            ])
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                }
            ])
            ->withCount(['isWatchedVideo'])
            ->withCount(['totalViews'])
            ->paginate(28);


        if ($request->ajax()) {
            $view = view('elements.post.categoryvideo', compact('title', 'categories', 'videoData'))->render();

            return response()->json(['html' => $view]);
        }

        // $locationwise = Video::join('artist_about','artist_about.user_id','=','videos.user_id')->where()

        $videos = Video::select('videos.id', 'videos.video_key', 'videos.title', 'videos.video_thumb_id', 'videos.video_time', 'videos.user_id', 'videos.created_at')
            ->join('followers', 'videos.user_id', '=', 'followers.follows_id')
            ->join('video_views', 'videos.id', '=', 'video_views.video_id')
            ->selectRaw('video_views.*, count(video_views.video_id) as total')
            ->groupBy('videos.id')
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                }
            ])
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }
            ])
            ->with(['user.artist' => function ($query) {
                $query->where('isDeleted', 0);
            }])
            ->where('videos.isDeleted', 0)
            ->where('followers.user_id', '=', Session::get('user_id'))
            ->withCount(['isWatchedVideo'])
            ->withCount(['totalViews'])
            ->take(20)
            ->inRandomOrder()
            ->get();
        //	return $videos;
// 		$history = WatchHistory::where(['watch_histories.user_id' => Session::get('user_id')])

// 			// ->orderBy('created_at','desc')
// 			->join('videos', 'watch_histories.video_id', '=', 'videos.id')

// 	        ->with([
// 				'video.user' => function ($user) {
// 					$user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
// 				}
// 			])
// 			->with([
// 				'thumb' => function ($thumb) {
// 					$thumb->select('id', 'thumb');
// 				}
// 			])
// 			->with(['video.user.artist'=>function($query){
// 		    $query->where('isDeleted',0);
// 		}])
// 			->with(['video.totalViews'])
// 			->groupBy('videos.id')

// 			//->where('followers.user_id', '=', Session::get('user_id'))
// 			->inRandomOrder()
// 			->limit(10)
// 			->get();
        $tages = VideoViews::where('video_views.user_id', Session::get('user_id') ?? 0)->join('video_categories', 'video_categories.video_id', '=', 'video_views.video_id')->get()->toArray();

        $history = [];
        $datas = [];
        if (count($tages) > 0) {

            foreach ($tages as $item) {
                $datas[] = $item['category_id'];
            }
            //return $datas;
            $history = Video::whereIn('video_categories.category_id', $datas)->select('videos.id', 'videos.video_key', 'videos.title', 'videos.video_thumb_id', 'videos.video_time', 'videos.user_id', 'videos.created_at', 'video_views.*', DB::raw('count(video_views.video_id) as total'))
                ->join('video_views', 'videos.id', '=', 'video_views.video_id')
                ->join('video_categories', 'video_categories.video_id', '=', 'videos.id')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                    }
                    , 'history'])
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with(['user.artist' => function ($query) {
                    $query->where('isDeleted', 0);
                }])
                ->where('videos.isDeleted', 0)
                ->withCount(['isWatchedVideo'])
                ->withCount(['totalViews'])
                ->inRandomOrder()
                ->get();
            // return $history;
        }
        $days = Carbon::now()->subDays(Carbon::now()->dayOfWeek)->startOfDay();
        $tages = Video::where([['videos.created_at', '<=', $days->format("Y-m-d 00:00:00")]])->where('videos.isDeleted', 0)
            ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'videos.created_at')
            ->orderByDesc('id')
            ->holdUser()
            ->withCount(['totalViews'])
            ->with('watchTime')
            ->withCount(['video_likes'])
            ->withCount(['video_dislikes'])
            ->get();

        $array = [];

        if (count($tages) > 0) {
            foreach ($tages->sortByDesc('total_views_count')->values() as $reward) {
                foreach ($reward->video_tag as $tags) {
                    $array[] = $tags->tag->tag;
                }
            }
        }
        //return $array;
// 		$tags = DB::table('video_tags as vc')
// 			->select('vc.tag_id', 'users.*', 'videos.*', 'videos.id', 'videos.video_key', 'videos.title', 'videos.video_thumb_id', 'videos.video_time', 'videos.user_id', 'videos.created_at', DB::raw('count(vc.tag_id) as total'), 'c.tag')
// 			->join('videos', 'vc.video_id', '=', 'videos.id')
// 			->join('tags as c', 'c.id', '=', 'vc.tag_id')
// 			->join('users', 'videos.user_id', '=', 'users.id')
// 			->join('video_views', 'videos.id', '=', 'video_views.video_id')
// 			->selectRaw('video_views.*, count(video_views.video_id) as total')
// 			->groupBy('tag_id', 'c.tag')
// 			->orderBY(DB::raw('count(vc.video_id)'), 'DESC')
// 			->limit(7)
// 			->get();

// 		$trendingTagsVideo = DB::table('video_tags as vc')
// 			->select('vc.tag_id', DB::raw('count(vc.tag_id) as total'), 'c.tag')
// 			->join('tags as c', 'c.id', '=', 'vc.tag_id')
// 			->groupBy('tag_id', 'c.tag')
// 			->orderBY(DB::raw('count(video_id)'), 'DESC')
// 			->limit(20)
// 			->get();
        $trendingVideo = Video::select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
            ->orderBy('total_views_count', 'desc')
            ->where('isRewardVideo', 0)
            ->where("videos.isDeleted", 0)
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }
            ])
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                }
            ])
            ->holdUser()
            ->withCount(['isWatchedVideo'])
            ->withCount(['totalViews'])
            ->take(20)
            ->get();
        //return $history;
        return view('homes.categories', ['title' => $pageTitle, 'videoData' => $videoData, 'categories' => $categories, 'videos' => $videos, 'history' => $history, 'trending' => $trendingVideo, 'tags' => array_unique($array)]);
    }

    /*Video Search*/
    public function search()
    {

    }

    /*Video Detail*/
    public function detail()
    {

    }

    /*Video History*/
    public function history($videoKey = null)
    {
        $pageTitle = "History";
        if (!empty($videoKey)) {
            $videoData = Video::where(['video_key' => $videoKey])
                ->first();
            $history = new WatchHistory();
            $history->video_id = $videoData->id;
            $history->title = $videoData->title;
            $history->url = \Request::url();
            $history->session_id = \Request::getSession()->getId();
            $history->user_id = Session::get('user_id');
            $history->ip = \Request::getClientIp();
            $history->agent = \Request::header('User-Agent');
            $history->save();
        }
    }

    //my watch historry
    public function mywatchhistory()
    {
        $pageTitle = "My History";
        if (!empty(Session::get('user_id'))) {
            $myHistory = WatchHistory::where(['user_id' => Session::get('user_id')])
                ->orderBy('created_at', 'desc')
                /*->with(['user' => function($user){
				$user->select('id','name');
				}])	*/
                ->with('thumb')
                ->with([
                    'video' => function ($video) {
                        $video->select('id', 'video_time', 'video_key');
                    }
                ])
                ->with([
                    'viewsCount' => function ($views) {
                        $views->count();
                    }
                ])
                ->paginate(20);
        }


        return view('homes.history', ['title' => $pageTitle, 'histories' => $myHistory]);
    }

    /*Video Liked*/
    public function likedVideo($videoKey = null)
    {

        $pageTitle = "Liked Video";
        $user = User::find(Session::get('user_id'));
        if (!empty($videoKey)) {
            $videoData = Video::where(['video_key' => $videoKey])
                ->first();
            $other_user = User::find($videoData->user_id);
            $viewsLikesstatus = VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->first();
            if ($viewsLikesstatus === null) {
                $videoLikes = new VideoLikes();
                $videoLikes->video_id = $videoData->id;
                $videoLikes->title = $videoData->title;
                $videoLikes->url = \Request::url();
                $videoLikes->session_id = \Request::getSession()->getId();
                $videoLikes->user_id = Session::get('user_id');
                $videoLikes->ip = \Request::getClientIp();
                $videoLikes->liked = true;
                $videoLikes->disliked = false;
                $videoLikes->save();
                $other_user->notify(new LikedVideo($user, $videoData, $videoData->isRewardVideo ? true : false));
            } else {
                if ($viewsLikesstatus->liked == 1) {
                    VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->update(['updated_at' => DB::raw('NOW()'), 'disliked' => false, 'liked' => false]);
                } else {
                    VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->update(['updated_at' => DB::raw('NOW()'), 'disliked' => false, 'liked' => true]);
                    $other_user->notify(new LikedVideo($user, $videoData, $videoData->isRewardVideo ? true : false));
                }

            }
        }

        $likeDislike = array(
            'likeCount' => VideoLikes::where(['video_id' => $videoData->id, 'liked' => true])->count(),
            'disLikeCount' => VideoLikes::where(['video_id' => $videoData->id, 'disliked' => true])->count()
        );

        return $likeDislike;
    }

    /*Video DisLiked*/
    public function dislikedVideo($videoKey = null)
    {
        if (!empty($videoKey)) {
            $notify = false;
            $videoData = Video::where(['video_key' => $videoKey])
                ->first();
            $viewsLikesstatus = VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->first();
            if ($viewsLikesstatus === null) {
                $videoLikes = new VideoLikes();
                $videoLikes->video_id = $videoData->id;
                $videoLikes->title = $videoData->title;
                $videoLikes->url = \Request::url();
                $videoLikes->session_id = \Request::getSession()->getId();
                $videoLikes->user_id = Session::get('user_id');
                $videoLikes->ip = \Request::getClientIp();
                $videoLikes->liked = false;
                $videoLikes->disliked = true;
                $videoLikes->save();
                $notify = true;
            } else {
                if ($viewsLikesstatus->disliked == 1) {
                    VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->update(['updated_at' => DB::raw('NOW()'), 'disliked' => false, 'liked' => false]);
                } else {
                    VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->update(['updated_at' => DB::raw('NOW()'), 'disliked' => true, 'liked' => false]);
                    $notify = true;
                }
            }

            if ($notify) {
                $user = User::find($videoData->user_id);
                if ($user) {
                    $user->notify(new DislikedVideo(User::find(Session::get('user_id')), $videoData, $videoData->isRewardVideo ? true : false));
                }
            }
        }

        $likeDislike = array(
            'likeCount' => VideoLikes::where(['video_id' => $videoData->id, 'liked' => true])->count(),
            'disLikeCount' => VideoLikes::where(['video_id' => $videoData->id, 'disliked' => true])->count()
        );
        return $likeDislike;
    }

    /*Video Flag Video*/
    public function flagVideo($videoKey = null, $comment = null)
    {
        $pageTitle = "Flag Video";
        if (!empty($videoKey)) {
            $videoData = Video::where(['video_key' => $videoKey])
                ->first();
            $flagstatus = VideoFlag::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->first();
            if ($flagstatus === null) {
                $flag = new VideoFlag();
                $flag->video_id = $videoData->id;
                $flag->title = $videoData->title;
                $flag->url = \Request::url();
                $flag->session_id = \Request::getSession()->getId();
                $flag->user_id = Session::get('user_id');
                $flag->ip = \Request::getClientIp();
                $flag->comment = $comment;
                $flag->save();
            } else {
                VideoFlag::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->update(['updated_at' => DB::raw('NOW()')]);
            }
        }
        return VideoFlag::where(['video_id' => $videoData->id])->count();
    }

    /*Video Watch Later*/
    public function watchLater($videoKey = null)
    {
        $pageTitle = "Watch Later";
        if (!empty($videoKey)) {
            $videoData = Video::where(['video_key' => $videoKey])
                ->first();
            $viewsWatchLaterstatus = WatchLater::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->first();
            if ($viewsWatchLaterstatus === null) {
                $watch = new WatchLater();
                $watch->video_id = $videoData->id;
                $watch->title = $videoData->title;
                $watch->url = str_replace('watch-later/', '', \Request::url());
                $watch->session_id = \Request::getSession()->getId();
                $watch->user_id = Session::get('user_id');
                $watch->ip = \Request::getClientIp();
                $watch->agent = \Request::header('User-Agent');
                $watch->save();
                return WatchLater::where(['video_id' => $videoData->id])->count();
            } else {
                WatchLater::where(['ip' => \Request::getClientIp(), 'video_id' => $videoData->id])->update(['updated_at' => DB::raw('NOW()')]);
                return WatchLater::where(['video_id' => $videoData->id])->count();
            }
        } else {
            $watchList = WatchLater::select('watch_laters.*', 'video_views.*', 'videos.*', 'video_thumbs.*', DB::raw('count(video_views.video_id) as total_count'))
                ->where(['watch_laters.user_id' => Session::get('user_id')])
                ->join('users', 'watch_laters.user_id', '=', 'users.id')
                ->join('video_views', 'watch_laters.video_id', '=', 'video_views.video_id')
                ->join('videos', 'watch_laters.video_id', '=', 'videos.id')
                ->join('video_thumbs', 'watch_laters.video_id', '=', 'video_thumbs.video_id')
                ->groupBy('watch_laters.video_id')
                ->orderBy('watch_laters.created_at', 'desc')
                ->paginate(36);

            //  return $watchList;
            return view('homes.watch_later', ['title' => $pageTitle, 'watchlist' => $watchList]);
        }
    }

    /*Video Saved Playlist*/
    public function savedPlaylist()
    {
        $pageTitle = "Saved in Playlist";
        $user = User::where(['id' => Session::get('user_id')])->first();
        return view('homes.saved_playlist', ['title' => $pageTitle, 'user' => $user]);
    }

    public function getVideo(Request $request)
    {
        // return response()->json($request->id);
        if ($request->id) {
            $video = Video::select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'video')->where("video_key", $request->id)->first();
            return response()->json($video);
        }
    }

    /*Video View Page*/
    public function watchVideo($videoKey = null, Request $request)
    {

        if (!empty($videoKey)) {
            $videoData = Video::where(['video_key' => $videoKey, 'videos.status' => 1, 'isRewardVideo' => 0])
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    },
                    'user' => function ($thumb) {
                        $thumb->select('id', 'name', 'slug', 'photo', 'isDeleted');
                    },
                    'user.artist' => function ($thumb) {

                    }
                ])
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->first();

            if ($videoData) {
                $videoWatchLaterCount = WatchLater::where(['video_id' => $videoData->id])->count();
                $videoLikeCount = VideoLikes::where(['video_id' => $videoData->id, 'liked' => 1])->count();
                $videoDisLikeCount = VideoLikes::where(['video_id' => $videoData->id, 'disliked' => 1])->count();
                $videoFlagCount = VideoFlag::where(['video_id' => $videoData->id])->count();

                $isWatchLaterByUser = WatchLater::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->count() > 0 ? 1 : 0;
                $isLikedByUser = VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id, 'liked' => 1])->count() > 0 ? 1 : 0;
                $isDisLikedByUser = VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id, 'disliked' => 1])->count() > 0 ? 1 : 0;
                $isFlagByUser = VideoFlag::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->count() > 0 ? 1 : 0;

                if (empty($videoData)) {
                    return Redirect::to('/');
                }

                $videoTitle = (!empty($videoData->title) ? $videoData->title : "");
                $videoCategories = VideoCategory::getVideoCategoryName($videoData->id);
                $videoTags = VideoTag::getVideoTagName($videoData->id);

                /*Recomended Video*/
                $getVideoCategoryId = DB::table('video_categories')->where('video_id', $videoData->id)->pluck('category_id')->toArray();
                $categoryWiseVideo = Video::where('isDeleted', 0)->whereHas('video_category', function ($query) use ($getVideoCategoryId) {
                    $query->whereHas(
                        'category',
                        function ($query) use ($getVideoCategoryId) {
                            $query->orderBy('created_at', 'desc');
                            $query->whereIn('id', $getVideoCategoryId);
                            $query->where('isRewardVideo', '=', 0);

                        }
                    );
                })
                    ->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
                    ->orderBy('created_at', 'desc')
                    ->where('isRewardVideo', 0)
                    ->whereRaw("id NOT IN (SELECT post_id from promote_posts WHERE post_type='video' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
                    ->with([
                        'thumb' => function ($thumb) {
                            $thumb->select('id', 'thumb');
                        }
                    ])
                    ->with([
                        'user' => function ($user) {
                            $user->select('id', 'name', 'slug', 'photo');
                        }
                    ])
                    ->withCount(['isWatchedVideo'])
                    ->withCount(['totalViews'])
                    ->with('watchTime')
                    ->where('video_key', '!=', $videoKey)
                    ->take(20)->get();


                /*Similar Video*/
                $getTagCategoryId = DB::table('video_tags')->where('video_id', $videoData->id)->pluck('tag_id')->toArray();
                $tagWiseVideo = Video::whereHas('video_tag', function ($query) use ($getTagCategoryId) {
                    $query->whereHas(
                        'tag',
                        function ($query) use ($getTagCategoryId) {
                            $query->orderBy('created_at', 'desc');
                            if (count($getTagCategoryId) > 0) {
                                $query->whereIn('id', $getTagCategoryId);
                            }
                            $query->where('isRewardVideo', '=', 0);

                        }
                    );
                })
                    ->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
                    ->orderBy('created_at', 'desc')
                    ->where('isRewardVideo', 0)
                    ->whereRaw("id NOT IN (SELECT post_id from promote_posts WHERE post_type='video' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
                    ->with([
                        'thumb' => function ($thumb) {
                            $thumb->select('id', 'thumb');
                        }
                    ])
                    ->with([
                        'user' => function ($user) {
                            $user->select('id', 'name', 'slug', 'photo');
                        }
                    ])
                    ->withCount(['totalViews'])
                    ->with('watchTime')
                    ->where('video_key', '!=', $videoKey)
                    ->take(20)->get();

                $tagWiseVideo = collect($tagWiseVideo);
                $tagWiseVideo = $tagWiseVideo->shuffle();

                //if(!empty(Session::get('user_id'))){
                /*set video views count start here*/
                $viewsstatus = VideoViews::where(['ip' => \Request::getClientIp(), 'video_id' => $videoData->id])->first();
                if ($viewsstatus === null) {
                    $videoview = new VideoViews();
                    $videoview->video_id = $videoData->id;
                    $videoview->title = $videoData->title;
                    $videoview->url = \Request::url();
                    $videoview->session_id = \Request::getSession()->getId();
                    $videoview->user_id = Session::get('user_id');
                    $videoview->ip = \Request::getClientIp();
                    $videoview->agent = \Request::header('User-Agent');
                    $videoview->save();
                } else {
                    VideoViews::where(['ip' => \Request::getClientIp(), 'video_id' => $videoData->id])->update(['updated_at' => DB::raw('NOW()')]);
                }


                $this->history($videoKey);
                //}
                /*set video views count end here*/

                $moreVideos = Video::where([['user_id', $videoData->user_id], ['isRewardVideo', 0], 'isDeleted' => 0])
                    //->select('id','video_key','title','video_thumb_id','video_time', 'user_id', 'created_at')
                    //->orWhere('isRewardVideo', '=', 1)
                    ->whereRaw("id NOT IN (SELECT post_id from promote_posts WHERE post_type='video' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
                    ->orderBy('created_at', 'desc')
                    ->with([
                        'thumb' => function ($thumb) {
                            $thumb->select('id', 'thumb');
                        }
                    ])
                    ->with([
                        'user' => function ($user) {
                            $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                        }
                    ])
                    ->where('video_key', '!=', $videoKey)
                    ->take(7)
                    ->get();

                $activeUser = User::where(['isDeleted' => 0, 'id' => Session::get('user_id')])->first();
                $userVideoCount = Video::where('user_id', '=', @$videoData->user->id)->where(['isDeleted' => 0, 'isRewardVideo' => 0])->count();
                $followCount = DB::table('followers')->where('follows_id', '=', @$videoData->user->id)->count();
                $user = User::where(['id' => Session::get('user_id'), 'isDeleted' => 0])->first();
                $days = Carbon::now()->subDays(15);
                $tages = Video::where([['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where(['videos.isDeleted' => 0, 'videos.isRewardVideo' => 0])
                    ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'videos.created_at')
                    ->orderByDesc('id')
                    ->holdUser()
                    ->withCount(['totalViews'])
                    ->with('watchTime')
                    ->withCount(['video_likes'])
                    ->withCount(['video_dislikes'])
                    ->get();
                $array = [];

                if (count($tages) > 0) {
                    foreach ($tages->sortByDesc('total_views_count')->values() as $reward) {
                        foreach ($reward->video_tag as $tags) {
                            $array[] = $tags->tag->tag;
                        }
                    }
                }
                return view('homes.watch',
                    [
                        'title' => $videoTitle,
                        'videoData' => $videoData,
                        'videoCategories' => $videoCategories,
                        'videoTags' => $videoTags,
                        'categoryWiseVideo' => $categoryWiseVideo,
                        'tagWiseVideo' => $tagWiseVideo,
                        'loggedInUser' => Session::get('user_id'),
                        'moreVideos' => $moreVideos,
                        'videoLikeCount' => $videoLikeCount,
                        'videoDisLikeCount' => $videoDisLikeCount,
                        'videoWatchLaterCount' => $videoWatchLaterCount,
                        'videoFlagCount' => $videoFlagCount,
                        'isWatchLaterByUser' => $isWatchLaterByUser,
                        'isLikedByUser' => $isLikedByUser,
                        'isDisLikedByUser' => $isDisLikedByUser,
                        'isFlagByUser' => $isFlagByUser,
                        'activeUser' => $activeUser,
                        'userVideoCount' => $userVideoCount,
                        'followCount' => $followCount,
                        'user' => $user,
                        'tags' => array_unique($array)
                    ]
                );
            } else {
                return abort(404);
            }
        } else {
            return Redirect::to('/');
        }
    }


    /*Video View Page*/
    public function watchRewardVideo($videoKey = null, Request $request)
    {
        if (!empty($videoKey)) {
            $days = Carbon::now()->subDays(Carbon::now()->dayOfWeek)->startOfDay();
            $videoData = Video::where(['video_key' => $videoKey, 'videos.status' => 1, 'isRewardVideo' => 1])
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    },
                    'user' => function ($thumb) {
                        $thumb->select('id', 'name', 'slug', 'email', 'photo', 'isDeleted');
                    }
                ])
                ->withCount(['totalViews'])
                ->withCount(['totalComments'])
                ->first();

            if ($videoData) {
                $videoWatchLaterCount = WatchLater::where(['video_id' => $videoData->id])->count();
                $videoLikeCount = VideoLikes::where(['video_id' => $videoData->id, 'liked' => 1])->count();
                $videoDisLikeCount = VideoLikes::where(['video_id' => $videoData->id, 'disliked' => 1])->count();
                $videoFlagCount = VideoFlag::where(['video_id' => $videoData->id])->count();

                $isWatchLaterByUser = WatchLater::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->count() > 0 ? 1 : 0;
                $isLikedByUser = VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id, 'liked' => 1])->count() > 0 ? 1 : 0;
                $isDisLikedByUser = VideoLikes::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id, 'disliked' => 1])->count() > 0 ? 1 : 0;
                $isFlagByUser = VideoFlag::where(['user_id' => Session::get('user_id'), 'video_id' => $videoData->id])->count() > 0 ? 1 : 0;


                // if (empty($videoData)) {
                // 	return Redirect::to('/reward_single');
                // }

                $videoTitle = (!empty($videoData->title) ? $videoData->title : "");
                $videoCategories = VideoCategory::getVideoCategoryName($videoData->id);
                $videoTags = VideoTag::getVideoTagName($videoData->id);

                /*Similar Video*/

                $simiVideo = Video::where(['video_key' => $videoKey, 'status' => 1, 'isRewardVideo' => 1])->first();
                $videoTags_Updated = DB::table('video_tags')->where('video_id', $simiVideo->id)->pluck('tag_id')->toArray();
                $getSimilarVideoId = DB::table('video_tags')->whereIn('tag_id', $videoTags_Updated)->pluck('video_id')->toArray();
                $similarVideos = Video::whereIn('id', $getSimilarVideoId)->where('deleted_at', NULL)->where('isDeleted', 0)->where('video_key', '!=', $videoKey)->withCount(['totalViews'])->inRandomOrder()->take(20)->get();


                $getVideoCategoryId = DB::table('video_categories')->where('video_id', $videoData->id)->pluck('category_id')->toArray();

                $recomendedVideos = Video::where([['status', 1], ['isRewardVideo', 1], ['created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where('isDeleted', 0)->whereHas('video_category', function ($query) use ($getVideoCategoryId) {
                    $query->whereHas('category', function ($query) use ($getVideoCategoryId) {
                        $query->orderBy('created_at', 'desc');
                        if (count($getVideoCategoryId) > 0) {
                            $query->whereIn('id', $getVideoCategoryId);
                        }
                        $query->where('isRewardVideo', '=', 1);

                    }
                    );
                })
                    ->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
                    ->orderBy('created_at', 'desc')
                    ->where('isRewardVideo', 1)
                    ->with([
                        'thumb' => function ($thumb) {
                            $thumb->select('id', 'thumb');
                        }
                    ])
                    ->with([
                        'user' => function ($user) {
                            $user->select('id', 'name', 'slug', 'photo');
                        }
                    ])
                    ->withCount(['totalViews'])
                    ->with('watchTime')
                    ->where('video_key', '!=', $videoKey)
                    ->take(20)->get();

                $recomendedVideos = collect($recomendedVideos)->shuffle();

                //if(!empty(Session::get('user_id'))){
                /*set video views count start here*/
                $viewsstatus = VideoViews::where(['ip' => \Request::getClientIp(), 'video_id' => $videoData->id])->first();
                if ($viewsstatus === null) {
                    $videoview = new VideoViews();
                    $videoview->video_id = $videoData->id;
                    $videoview->title = $videoData->title;
                    $videoview->url = \Request::url();
                    $videoview->session_id = \Request::getSession()->getId();
                    $videoview->user_id = Session::get('user_id');
                    $videoview->ip = \Request::getClientIp();
                    $videoview->agent = \Request::header('User-Agent');
                    $videoview->save();
                } else {
                    VideoViews::where(['ip' => \Request::getClientIp(), 'video_id' => $videoData->id])->update(['updated_at' => DB::raw('NOW()')]);
                }


                $this->history($videoKey);
                //}
                /*set video views count end here*/

                $moreVideoss = Video::where('user_id', '=', $videoData->user_id)->where('isDeleted', 0)
                    ->orderBy('created_at', 'desc')
                    ->with([
                        'thumb' => function ($thumb) {
                            $thumb->select('id', 'thumb');
                        }
                    ])
                    ->with([
                        'user' => function ($user) {
                            $user->select('id', 'name', 'slug', 'photo');
                        }
                    ])
                    ->with([
                        'user.artist' => function ($user) {
                            $user->where('isDeleted', 0);
                        }
                    ])
                    ->where('video_key', '!=', $videoKey)
                    ->paginate(7);

                $activeUser = User::find(Session::get('user_id'));

                $userVideoCount = Video::where('user_id', '=', @$videoData->user_id)->where(['isDeleted' => 0, 'isRewardVideo' => 0])->count();

                $followCount = DB::table('followers')->where('follows_id', '=', @$videoData->user->id)->count();

                $user = User::where(['id' => Session::get('user_id')])->first();
                $days = Carbon::now()->subDays(Carbon::now()->dayOfWeek)->startOfDay();
                $rewardVideos = Video::where([['isRewardVideo', '=', 1], ['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])->where('videos.isDeleted', 0)
                    ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                    ->where('videos.isDeleted', 0)
                    ->orderByDesc('id')
                    ->holdUser()
                    ->withCount(['totalViews'])
                    ->with('watchTime')
                    ->withCount(['video_likes'])
                    ->withCount(['video_dislikes'])
                    ->get();

                $array = [];

                if (count($rewardVideos) > 0) {
                    foreach ($rewardVideos->sortByDesc('total_views_count')->values() as $reward) {
                        foreach ($reward->video_tag as $tags) {
                            $array[] = $tags->tag->tag;
                        }
                    }
                }

                return view('homes.reward_single',
                    [
                        'title' => $videoTitle,
                        'videoData' => $videoData,
                        'videoCategories' => $videoCategories,
                        'videoTags' => $videoTags,
                        'similarVideos' => $similarVideos,
                        'recomendedVideos' => $recomendedVideos,
                        'loggedInUser' => Session::get('user_id'),
                        'moreVideoss' => $moreVideoss,
                        'videoLikeCount' => $videoLikeCount,
                        'videoDisLikeCount' => $videoDisLikeCount,
                        'videoWatchLaterCount' => $videoWatchLaterCount,
                        'videoFlagCount' => $videoFlagCount,
                        'isWatchLaterByUser' => $isWatchLaterByUser,
                        'isLikedByUser' => $isLikedByUser,
                        'isDisLikedByUser' => $isDisLikedByUser,
                        'isFlagByUser' => $isFlagByUser,
                        'activeUser' => $activeUser,
                        'userVideoCount' => $userVideoCount,
                        'followCount' => $followCount,
                        'user' => $user,
                        "tags" => array_unique($array)
                    ]
                );
            } else {
                return abort(404);
            }
        } else {
            return Redirect::to('/');
        }
    }


    function __generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function deleteVideo($videoKey)
    {
        $Video = Video::where(['video_key' => $videoKey])->first();

        // Delete Video Category
        if ($Video) {
            $isReward = false;
            if ($Video->isRewardVideo == 1) {
                $isReward = true;
            }

            $findCategory = DB::table('video_categories')->where('video_id', $Video['id'])->pluck('video_id');
            // 		dd($findCategory);
            if ($findCategory) {
                DB::table('video_categories')->whereIn('video_id', $findCategory)->delete();
            }
            // End Delete Video Category
            $Video->delete();

            if ($isReward) {
                return redirect('/myrewards')->with('success', 'Reward Video deleted!');
            }
        }

        return redirect('/videos/' . Session::get('slug'))->with('success', 'Video deleted!');
    }

    public function globalSerach(Request $request)
    {

        $pageTitle = 'Welcome';

        $searchType = "";
        $criteria = "";
        $stories = "";
        $searchdata = '';

        if (!$request->isMethod('post')) {
            return view('homes.search', ['title' => $pageTitle, 'searchType' => $searchType, 'criteria' => $criteria, 'stories' => $stories, 'searchdata' => $searchdata]);
        }

        $input = Input::all();
        $searchType = $input['searchType'];
        $searchdata = $input['searchdata'];

        if ($searchType == 'video') {
            $SearchData = Video::where('isRewardVideo', 0)->where('videos.isDeleted', 0)
                ->where(function ($query) use ($searchdata) {
                    $query->where('title', 'like', '%' . $searchdata . '%');
                    $query->orWhere(DB::raw('REPLACE(TRIM(description), "&nbsp;", "")'), 'like', '%' . $searchdata . '%');
                    $query->orWhereHas('video_category', function ($query) use ($searchdata) {
                        $query->whereHas('category', function ($query) use ($searchdata) {
                            $query->where('category', 'like', '%' . $searchdata . '%');
                        });
                    });
                    $query->orWhereHas('video_tag', function ($query) use ($searchdata) {
                        $query->whereHas(
                            'tag',
                            function ($query) use ($searchdata) {
                                $query->where('tag', 'like', '%' . $searchdata . '%');
                            }
                        );
                    });
                })
                ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                ->orderBy('created_at', 'desc')
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    }
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                    }
                ])
                ->with([
                    'totalViews' => function ($query) {
                        $query->select('video_id', 'watchTime')->where('user_id', Session::get('user_id'));
                    }
                ])
                ->holdUser()
                ->withCount(['isWatchedVideo'])
                ->withCount(['totalViews'])
                ->withCount(['video_likes'])
                ->withCount(['video_dislikes'])
                ->paginate(10);

            return view('homes.search', ['title' => $pageTitle, 'searchType' => $searchType, 'criterias' => $SearchData, 'searchdata' => $searchdata]);

        } else if ($searchType == 'story') {
            $like = "%$searchdata%";
            $stories = Story::where('title', 'like', $like)
                ->where('stories.isDeleted', 0)
                ->orWhere('tags', 'like', $like)
                ->orWhere('language', 'like', $like)
                ->orWhere(DB::raw('REPLACE(TRIM(stories.about), "&nbsp;", "")'), 'like', $like)
                ->select('stories.id', 'title', 'stories.about', 'cover_image', 'thumb_image', 'category', 'user_id', 'stories.created_at', 'stories.slug')
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo')->where('isDeleted', 0);
                    }
                ])
                ->holdUser()
                ->withCount(['totalViews'])
                ->paginate(10);
            return view('homes.search', ['title' => $pageTitle, 'searchType' => $searchType, 'stories' => $stories, 'searchdata' => $searchdata]);
        }
    }


    public function globalFilterSerach(Request $request, $slug)
    {
        $allowurl = ['artist_videos', 'artist_gallery', 'artist-profile', 'production_house', 'auditions'];
        if (!in_array($slug, $allowurl)) {
            return response()->view('errors.404', [], 404);
        }

        $pageTitle = 'Welcome to ' . str_replace("_", " ", $slug);
        if ($slug == 'artist_videos') {
            $videoData = ArtistVideo::select('id', 'video_title', 'category', 'description', 'video', 'thumb', 'user_id', 'created_at')
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'profile_name', 'profile_photo');
                    }
                ])
                ->paginate(12);
            $queries = \DB::getQueryLog();
            return view('homes.search.artist_videos', ['title' => $pageTitle, 'video' => $videoData,]);


        } else if ($searchType == 'story') {

            $stories = Story::where('title', 'like', '%' . $searchdata . '%')
                ->orWhere('tags', 'like', '%' . $searchdata . '%')
                ->orWhere('language', 'like', '%' . $searchdata . '%')
                ->orWhere('about', 'like', '%' . $searchdata . '%')
                ->select('id', 'title', 'about', 'cover_image', 'thumb_image', 'category', 'user_id', 'created_at', 'slug')
                ->orderBy('created_at', 'desc')
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'slug', 'photo');
                    }
                ])
                ->withCount(['totalViews'])
                ->paginate(10);
            return view('homes.search.artist_videos', ['title' => $pageTitle, 'searchType' => $searchType, 'stories' => $stories, 'searchdata' => $searchdata]);
        }
    }

    public function deleteHistory($id)
    {
        $Video = WatchHistory::where(['id' => $id]);
        $Video->delete();
        return redirect('/myhistory')->with('success', 'History deleted!');
    }


    public function deleteStory($id)
    {
        $Video = Story::where(['id' => $id]);
        $Video->delete();
        return redirect(Session::get('slug') . "/stories")->with('success', 'Story deleted!');
    }


    public function deleteallHistory()
    {
        $Video = WatchHistory::where(['user_id' => Session::get('user_id')]);
        $Video->delete();
        return redirect('/myhistory')->with('success', 'History deleted!');
    }

    public function videoGraphData($id)
    {
        return response()->json(
            array(
                array(
                    "date" => "2014-08-08",
                    "open" => 43.23,
                    "high" => 43.32,
                    "low" => 43.32,
                    "close" => 42.91,
                    "volume" => 28942700,
                    "adj" => 43.20
                ),
                array(
                    "date" => "2014-08-07",
                    "open" => 1567,
                    "high" => 43.32,
                    "close" => 42.91,
                    "volume" => 43.23,
                    "adj" => 28942700
                )
            )
        );
    }

    /** Reward Most Watched ***/
    public function rewardMostWatched()
    {
        $title = "Reward Most Watched";
        $days = Carbon::now()->subDays(Carbon::now()->dayOfWeek);
        $rewardMostWatchedVideos = Video::where([['isRewardVideo', '=', 1], ['created_at', '>=', $days->format("Y-m-d 00:00:00")]])
            ->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
            ->orderBy('total_views_count', 'desc')
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }
            ])
            ->with([
                'totalViews' => function ($query) {
                    $query->select('video_id')
                        ->groupBy('video_id', 'video_id')
                        ->orderBY(DB::raw('count(video_id)'), 'DESC');
                }
            ])
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo');
                }
            ])
            ->withCount(['totalViews'])
            ->with('watchTime')
            ->withCount(['video_likes'])
            ->withCount(['video_dislikes'])
            ->paginate(10);
        $array = [];

        if (count($rewardMostWatchedVideos) > 0) {
            foreach ($rewardMostWatchedVideos->sortByDesc('total_views_count')->values() as $reward) {
                foreach ($reward->video_tag as $tags) {
                    $array[] = $tags->tag->tag;
                }
            }
        }
        $tags = array_unique($array);
        return view('homes.reward-most-watched', compact('rewardMostWatchedVideos', 'title', 'tags'));
    }

    /*Rewards*/
    public function topentertainer($week = null)
    {
        $pageTitle = "All Top Entertainers";
        $days = Carbon::now()->subDays(Carbon::now()->dayOfWeek)->startOfDay();
        $fromDays = Carbon::now()->subDays(Carbon::now()->dayOfWeek + 7)->startOfDay();

        if (!(is_null($week))) {
            if ($week == "second") {
                $rewardVideos = Video::select('videos.id', 'videos.video_key', 'videos.title', 'videos.video_thumb_id', 'videos.video_time', 'videos.user_id', 'videos.isRewardVideo', 'videos.created_at', DB::raw("COUNT(DISTINCT watch_histories.`user_id`, watch_histories.`ip`) AS `total_views`"))
                    ->leftJoin('watch_histories', function ($join) use ($days, $fromDays) {
                        $join->on('watch_histories.video_id', '=', 'videos.id')
                            ->where('watch_histories.created_at', '>=', $fromDays->format("Y-m-d H:i:s"))
                            ->where('watch_histories.created_at', '<', $days->format("Y-m-d H:i:s"));
                    })
                    ->where([
                        ['videos.isRewardVideo', '=', 1],
                        ['videos.created_at', '<', $days->format("Y-m-d 00:00:00")],
                        ['videos.created_at', '>=', $fromDays->format("Y-m-d 00:00:00")]
                    ])
                    ->groupBy('videos.id', 'videos.video_key', 'videos.title', 'videos.video_thumb_id', 'videos.video_time', 'videos.user_id', 'videos.isRewardVideo', 'videos.created_at')
                    ->orderByDesc('total_views');
            } else {
                return abort(404);
            }
        } else {
            $rewardVideos = Video::select('videos.id', 'videos.video_key', 'videos.title', 'videos.video_thumb_id', 'videos.video_time', 'videos.user_id', 'videos.isRewardVideo', 'videos.created_at')
                ->leftJoin('watch_histories', function ($join) use ($days) {
                    $join->on('watch_histories.video_id', '=', 'videos.id')
                        ->where('watch_histories.created_at', '>=', $days->format("Y-m-d 00:00:00"));
                })
                ->where([['isRewardVideo', '=', 1], ['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]]);

        }

        $rewardVideos = $rewardVideos
            ->with([
                'thumb' => function ($thumb) {
                    $thumb->select('id', 'thumb');
                }
            ])
            ->with([
                'user' => function ($user) {
                    $user->select('id', 'name', 'slug', 'photo', 'slug');
                }
            ])
            ->with('watchTime')
            ->withCount(['totalViews'])
            ->groupBy('videos.id')
            ->orderBy('total_views_count', 'desc')
            ->paginate(30);
        //return $rewardVideos;

        return view('homes.all_top_entertainer', ['title' => $pageTitle, 'rewardVideos' => $rewardVideos, 'selected' => $week]);
    }

    public function getuserbylatestpost()
    {
        $stories = Video::select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
            // 		->orderBy('created_at','desc')
            ->orderBY(DB::raw('unique(user_id)'), 'DESC')

            //         $stories =  Video::select('id','title','description','video', 'user_id', 'created_at')
            // 		->orderBy('created_at','asc')

            ->with([
                'followers' => function ($query) {
                    $query->DB::table('followers')->where('follows_id', '=', Session::get('user_id'))
                        ->orderBY(DB::raw('unique(user_id)'), 'DESC');
                }
            ]);


        // 		->with(['user' => function($user){
        // 			$user->select('id','name', 'photo', 'facebook', 'twitter', 'instagram');
        // 		}]);

        return view('elements.header', ['stories' => $stories]);
    }


    /*Upload Reward Video*/
    public function uploadRewardVideo()
    {
        $pageTitle = "Upload Video";
        $videoCategories = Category::getCategoryList();
        $input = Input::all();
        $user = User::find(Session::get('user_id'));

        if (!empty($input)) {

            $rules = array(
                'video' => 'required|max:100',
                'title' => 'required|max:100',
                'description' => 'required',
                'tags' => 'required',
            );
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                return Redirect::to('/upload-reward-video')->withErrors($validator)->withInput();
            } else {

                $videoData = new Video();
                $videoData['title'] = $input['title'];
                $videoData['description'] = $input['description'];
                $videoData['feature'] = ''; //$input['feature'];
                $videoData['sub_title'] = ''; //$input['sub_title'];
                $videoData['privacy_setting'] = ''; //$input['privacy_setting'];
                $videoData['language'] = $input['language'];
                $videoData['status'] = 1;
                $videoData['user_id'] = Session::get('user_id');
                $videoData['video_key'] = $this->__generateRandomString(12);
                /*Video Data*/
                $videoTempPath = TEMP_UPLOAD_PATH . $input['video'];
                if (file_exists($videoTempPath)) {
                    $videoData['video'] = $input['video'];
                    $videoSize = filesize($videoTempPath);
                    $videoData['video_size'] = $videoSize / 1024;

                    /*$ffprobe = FFMpeg\FFProbe::create([
					'ffmpeg.binaries'  => 'C:/ffmpeg/bin/ffmpeg.exe', // the path to the FFMpeg binary
					'ffprobe.binaries' => 'C:/ffmpeg/bin/ffprobe.exe', // the path to the FFProbe binary
					'timeout'          => 3600, // the timeout for the underlying process
					'ffmpeg.threads'   => 12,   // the number of threads that FFMpeg should use
					]);

					$duration = $ffprobe
					->format($videoTempPath) // extracts file information
					->get('duration');*/
                    $duration = 60;

                    // $videoData['video_time'] = round($duration / 60, 2);
                    $videoData['video_time'] = $input['duration'] ?? "0";
                    /*Move video file in video main folder*/
                    $destinationPath = VIDEO_UPLOAD_PATH . $input['video'];
                    \File::copy($videoTempPath, $destinationPath);
                    \File::delete($videoTempPath);
                    /*Save Video*/
                    $videoData->save();
                    $videoId = $videoData->id;

                    /*Save Thumbnail*/
                    if (!empty($input['thumb'])) {
                        $total = count($input['thumb']);
                        foreach ($input['thumb'] as $k => $thumbImage) {
                            $thumbTempPath = TEMP_UPLOAD_PATH . $thumbImage;
                            $thumbDestinationPath = VIDEO_THUMB_UPLOAD_PATH . $thumbImage;
                            if (!empty($thumbImage) && file_exists($thumbTempPath)) {
                                \File::copy($thumbTempPath, $thumbDestinationPath);
                                \File::delete($thumbTempPath);
                                $default = 0;
                                $videoThumb = new VideoThumb();
                                $videoThumb['video_id'] = $videoId;
                                $videoThumb['thumb'] = $thumbImage;
                                if (!empty($input['selected_thumb'])) {
                                    if ($thumbImage == $input['selected_thumb']) {
                                        $default = 1;
                                    }
                                } else {
                                    if ($k == $total) {
                                        $default = 1;
                                    }
                                }
                                /*Save Video Thumb*/
                                $videoThumb->save();
                                if ($default == 1) {
                                    /*Update Video Thum*/
                                    $thumbId = $videoThumb->id;
                                    $videoUpdate = Video::find($videoId);
                                    if ($videoUpdate) {
                                        $videoUpdate->video_thumb_id = $thumbId;
                                        $videoUpdate->save();
                                    }
                                }

                            }

                        }
                    } else {
                        $fileName = str_replace('mp4', 'png', $input['video']);
                        VideoThumbnail::createThumbnail($destinationPath, VIDEO_THUMB_UPLOAD_PATH, $fileName, 2, $width = 640, $height = 320);
                        $videoThumb = new VideoThumb();
                        $videoThumb['video_id'] = $videoId;
                        $videoThumb['thumb'] = $fileName;
                        $videoThumb->save();

                        $thumbId = $videoThumb->id;
                        $videoUpdate = Video::find($videoId);
                        if ($videoUpdate) {
                            $videoUpdate->video_thumb_id = $thumbId;
                            $videoUpdate->save();
                        }
                    }

                    /*Video Tags*/
                    if (!empty($input['tags'])) {
                        $tags = explode(',', $input['tags']);
                        if (!empty($tags)) {
                            foreach ($tags as $tag) {
                                /*Check tag in tags table*/
                                $tagId = Tag::getTagIdByName($tag);
                                /*Save tag name in Tag Table*/
                                if (empty($tagId)) {
                                    $tagData = new Tag();
                                    $tagData['tag'] = $tag;
                                    $tagData->save();
                                    $tagId = $tagData->id;
                                }
                                /*Save TagId in Video Tag Table*/
                                $checkVideoTag = VideoTag::where('tag_id', '=', $tagId)->where('video_id', '=', $videoId)->count();
                                if (empty($checkVideoTag)) {
                                    $videoTag = new VideoTag();
                                    $videoTag['tag_id'] = $tagId;
                                    $videoTag['video_id'] = $videoId;
                                    $videoTag->save();
                                }
                            }
                        }
                    }
                    /*Video Language*/
                    $followers = DB::table('followers')->where('follows_id', '=', $user->id)->get();
                    foreach ($followers as $f) {
                        $follower = User::find($f->user_id);
                        $follower->notify(new Notices($follower, ' added a new reward video', 'following-post', '', '', $user->id, $videoData->id));
                    }

                    $user->notify(new Notices($user, ' congrats: your reward video is uploaded successfully!', 'reward-post', '', '', '', $videoData->id));
                    return redirect()->to('/myrewards')->with('success', 'Your reward video uploaded successfully');
                } else {
                    return Redirect::to('/upload-reward-video?status=error');
                }
            }
        }

        return view('rewards.upload_reward_video',
            ['title' => $pageTitle, 'videoCategories' => $videoCategories]
        );
    }

    /*Edit Video*/
    public function editRewardVideo($video_key)
    {

        $pageTitle = "Edit Video";
        $videoCategories = Category::getCategoryList();
        $input = Input::all();

        if (!empty($input)) {

            $rules = array(
                'title' => 'required|max:100',
                'description' => 'required',
                'tags' => 'required',
            );
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                return Redirect::to('/edit-reward-video/' . $video_key)->withErrors($validator)->withInput();
            } else {

                $videoData = Video::where(['video_key' => $video_key])->first();
                $videoData['title'] = $input['title'];
                $videoData['description'] = $input['description'];
                $videoData['feature'] = ''; //$input['feature'];
                $videoData['sub_title'] = ''; //$input['sub_title'];
                $videoData['language'] = $input['language'];
                $videoData->save();
                $videoId = $videoData->id;

                /*Save Thumbnail*/
                if (!empty($input['thumb'])) {
                    $total = count($input['thumb']);
                    foreach ($input['thumb'] as $k => $thumbImage) {
                        $thumbImage = str_replace(VIDEO_THUMB_DISPLAY_PATH, "", $thumbImage);
                        $thumbDestinationPath = VIDEO_THUMB_UPLOAD_PATH . $thumbImage;
                        if (!empty($thumbImage) && file_exists($thumbDestinationPath)) {
                            $default = 0;
                            $videoThumb = new VideoThumb();
                            $videoThumb['video_id'] = $videoId;
                            $videoThumb['thumb'] = $thumbImage;
                            if (!empty($input['selected_thumb'])) {
                                if ($thumbImage == $input['selected_thumb']) {
                                    $default = 1;
                                }
                            } else {
                                if ($k == $total) {
                                    $default = 1;
                                }
                            }

                            /*Save Video Thumb*/
                            $videoThumb->save();
                            if ($default == 1) {
                                /*Update Video Thum*/
                                $thumbId = $videoThumb->id;
                                $videoUpdate = Video::find($videoId);
                                if ($videoUpdate) {
                                    $videoUpdate->video_thumb_id = $thumbId;
                                    $videoUpdate->save();
                                }
                            }
                        }
                    }
                }

                /*Video Tags*/
                if (!empty($input['tags'])) {
                    $tags = explode(',', $input['tags']);
                    if (!empty($tags)) {
                        $videoTag = VideoTag::where(['video_id' => $videoId])->delete();
                        foreach ($tags as $tag) {
                            /*Check tag in tags table*/
                            $tagId = Tag::getTagIdByName($tag);
                            /*Save tag name in Tag Table*/
                            if (empty($tagId)) {
                                $tagData = new Tag();
                                $tagData['tag'] = $tag;
                                $tagData->save();
                                $tagId = $tagData->id;
                            }
                            /*Save TagId in Video Tag Table*/
                            $checkVideoTag = VideoTag::where('tag_id', '=', $tagId)->where('video_id', '=', $videoId)->count();
                            if (empty($checkVideoTag)) {
                                $videoTag = new VideoTag();
                                $videoTag['tag_id'] = $tagId;
                                $videoTag['video_id'] = $videoId;
                                $videoTag->save();
                            }
                        }
                    }
                }
                /*Video Language*/
                return Redirect::to('/myrewards?status=updated');
            }
        } else if (!empty($video_key)) {
            $videoData = Video::where(['video_key' => $video_key, 'status' => 1])
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    },
                    'user' => function ($thumb) {
                        $thumb->select('id', 'name', 'slug', 'photo');
                    },
                    'video_category' => function ($thumb) {
                        $thumb->select('video_id', 'category_id');
                    }
                ])
                ->first();

            $videoTags = VideoTag::getVideoTagName($videoData->id);

            return view(
                'rewards.edit_reward_video',
                ['title' => $pageTitle, 'videoCategories' => $videoCategories, 'videoData' => $videoData, 'videoTags' => $videoTags]
            );
        }

        return view(
            'rewards.upload_reawrd_video',
            ['title' => $pageTitle, 'videoCategories' => $videoCategories]
        );
    }


    /*Upload Video*/
    public function altopuploadVideo()
    {
        $pageTitle = "Upload Reward Video";
        $videoCategories = Category::getCategoryList();
        $user = User::find(Session::get('user_id'));
        $input = Input::all();
        $date = Carbon::now()->subDays(Carbon::now()->dayOfWeek - 1)->startOfDay();
        $lastDate = Carbon::now()->subDays(Carbon::now()->dayOfWeek - 1)->startOfDay()->addWeek();

        $currentWeekVideoCount = Video::where([['user_id', $user->id], ['isRewardVideo', '1'], ['created_at', '>=', $date->format("Y-m-d H:i:s")], ['created_at', '<', $lastDate->format("Y-m-d H:i:s")]])->count();

        if (!empty($input)) {
            if ($currentWeekVideoCount > 1) {
                return redirect()->to('/myrewards')->withErrors(['Already uploaded 2 reward videos this week']);
            }

            $rules = array(
                'video' => 'required|max:100',
                'title' => 'required|max:100',
                'description' => 'required',
                'tags' => 'required',
            );
            $validator = Validator::make($input, $rules);

            if ($validator->fails()) {
                return Redirect::to('/upload-video')->withErrors($validator)->withInput();
            } else {
                $videoData = new Video();
                $videoData['title'] = $input['title'];
                $videoData['description'] = $input['description'];
                $videoData['feature'] = ''; //$input['feature'];
                $videoData['sub_title'] = ''; //$input['sub_title'];
                $videoData['privacy_setting'] = ''; //$input['privacy_setting'];
                $videoData['language'] = $input['language'];
                $videoData['status'] = 1;
                $videoData['isRewardVideo'] = 1;
                $videoData['user_id'] = Session::get('user_id');
                $videoData['video_key'] = $this->__generateRandomString(12);
                $videoData['video_time'] = $input['duration'] ?? "0";
                /*Video Data*/
                $videoTempPath = TEMP_UPLOAD_PATH . $input['video'];
                if (file_exists($videoTempPath)) {
                    $videoData['video'] = $input['video'];
                    $videoSize = filesize($videoTempPath);
                    $videoData['video_size'] = $videoSize / 1024;
                    $duration = 60;
                    /*Move video file in video main folder*/
                    $destinationPath = VIDEO_UPLOAD_PATH . $input['video'];
                    \File::copy($videoTempPath, $destinationPath);
                    \File::delete($videoTempPath);
                    /*Save Video*/
                    $videoData->save();
                    $user->notify(new VideoUploaded($user, $videoData));
                    $videoId = $videoData->id;

                    /*Save Video Categories*/
                    if (!empty($input['categories'])) {
                        foreach ($input['categories'] as $categoryId) {
                            $videoCategory = new VideoCategory();
                            $videoCategory['video_id'] = $videoId;
                            $videoCategory['category_id'] = $categoryId;
                            $videoCategory->save();
                        }
                    }
                    /*Save Thumbnail*/
                    if (!empty($input['thumb'])) {
                        $total = count($input['thumb']);
                        foreach ($input['thumb'] as $k => $thumbImage) {
                            $thumbImage = str_replace(VIDEO_THUMB_DISPLAY_PATH, "", $thumbImage);
                            $thumbDestinationPath = VIDEO_THUMB_UPLOAD_PATH . $thumbImage;
                            if (!empty($thumbImage) && file_exists($thumbDestinationPath)) {
                                $default = 0;
                                $videoThumb = new VideoThumb();
                                $videoThumb['video_id'] = $videoId;
                                $videoThumb['thumb'] = $thumbImage;
                                if (!empty($input['selected_thumb'])) {
                                    if ($thumbImage == $input['selected_thumb']) {
                                        $default = 1;
                                    }
                                } else {
                                    if ($k == $total) {
                                        $default = 1;
                                    }
                                }
                                /*Save Video Thumb*/
                                $videoThumb->save();
                                if ($default == 1) {
                                    /*Update Video Thum*/
                                    $thumbId = $videoThumb->id;
                                    $videoUpdate = Video::find($videoId);
                                    if ($videoUpdate) {
                                        $videoUpdate->video_thumb_id = $thumbId;
                                        $videoUpdate->save();
                                    }
                                }

                            }

                        }
                    } else {

                        $fileName = str_replace('mp4', 'png', $input['video']);
                        VideoThumbnail::createThumbnail($destinationPath, VIDEO_THUMB_UPLOAD_PATH, $fileName, 2, $width = 640, $height = 320);
                        $videoThumb = new VideoThumb();
                        $videoThumb['video_id'] = $videoId;
                        $videoThumb['thumb'] = $fileName;
                        $videoThumb->save();

                        $thumbId = $videoThumb->id;
                        $videoUpdate = Video::find($videoId);
                        if ($videoUpdate) {
                            $videoUpdate->video_thumb_id = $thumbId;
                            $videoUpdate->save();
                        }
                    }

                    /*Video Tags*/
                    if (!empty($input['tags'])) {
                        $tags = explode(',', $input['tags']);
                        if (!empty($tags)) {
                            foreach ($tags as $tag) {
                                /*Check tag in tags table*/
                                $tagId = Tag::getTagIdByName($tag);
                                /*Save tag name in Tag Table*/
                                if (empty($tagId)) {
                                    $tagData = new Tag();
                                    $tagData['tag'] = $tag;
                                    $tagData->save();
                                    $tagId = $tagData->id;
                                }
                                /*Save TagId in Video Tag Table*/
                                $checkVideoTag = VideoTag::where('tag_id', '=', $tagId)->where('video_id', '=', $videoId)->count();
                                if (empty($checkVideoTag)) {
                                    $videoTag = new VideoTag();
                                    $videoTag['tag_id'] = $tagId;
                                    $videoTag['video_id'] = $videoId;
                                    $videoTag->save();
                                }
                            }
                        }
                    }
                    /*Video Language*/
                    return redirect()->to('/myrewards')->with('success', 'Your reward video uploaded successfully');
                } else {
                    return Redirect::to('/upload-video?status=error');
                }
            }
        }

        return view('homes.participate_video',
            ['title' => $pageTitle, 'videoCategories' => $videoCategories, 'currentWeekVideoCount' => $currentWeekVideoCount]
        );
    }


    public function showFollow()
    {
        \DB::enableQueryLog();
        $title = "Follow";
        $users = User::where('id', '!=', session()->get('user_id'))->take(0)->get();

        $myFollowers = DB::table('followers')
            ->where('user_id', session()->get('user_id'))
            ->pluck('follows_id')
            ->toArray();
        $friendsFollowerId = DB::table('followers')
            ->whereIn('user_id', $myFollowers)
            ->pluck('follows_id')
            ->toArray();

        $frinedFollwerSlice = array_diff($friendsFollowerId, $myFollowers);

        $friendsFollower = User::whereIn('id', $frinedFollwerSlice)->inRandomOrder()->get();

        //  dd(\DB::getQueryLog());

        return view('homes.follow', compact('title', 'users', 'friendsFollower'));
    }

    public function searchFollow(Request $request)
    {
        $searchKeyword = $request->search_key;
        $searchPage = ($request->page) ? $request->page : 7;

        $users = User::where('name', 'LIKE', "%$searchKeyword%")
            ->orWhere('slug', 'LIKE', "%$searchKeyword%")
            ->skip(0)
            ->take($searchPage)
            ->get();

        return view('homes.follow-search', compact('users'));
    }


    public function followUser(Request $request)
    {
        $follower = User::find(session()->get('user_id'));
        $following = User::find($request->user_id);
        $user_id = $request->user_id;
        $data = array();
        $followers = new \App\Models\Follower();
        $followers->user_id = session()->get('user_id');
        $followers->follows_id = $user_id;
        $followers->created_at = date('Y-m-d H:i:s');
        $followers->updated_at = date('Y-m-d H:i:s');
        // DB::table('followers')->insert($data);
        $followers->save();

        $follower->notify(new Notices($following, 'followed successfully', 'follows', '', '', '', $followers->id));
        $following->notify(new Notices($follower, 'started following you', 'follows', '', '', '', $followers->id));

        return view('homes.follow-search.unfollow_user', compact('user_id'));
    }

    public function unFollowUserSearch(Request $request)
    {

        $user_id = $request->user_id;
        DB::table('followers')
            ->where('user_id', session()->get('user_id'))
            ->where('follows_id', $user_id)
            ->delete();

        return view('homes.follow-search.follow_user', compact('user_id'));
    }


    public function suggestedFollowUser(Request $request)
    {
        $user_id = $request->user_id;
        $data = array();
        $data['user_id'] = session()->get('user_id');
        $data['follows_id'] = $user_id;
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        DB::table('followers')->insert($data);
        return view('homes.follow-suggested.unfollow_user', compact('user_id'));
    }

    public function suggestedUnFollowUserSearch(Request $request)
    {

        $user_id = $request->user_id;
        DB::table('followers')
            ->where('user_id', session()->get('user_id'))
            ->where('follows_id', $user_id)
            ->delete();

        return view('homes.follow-suggested.follow_user', compact('user_id'));
    }

    public function details(Request $request)
    {
        $photolikes = PhotoLikes::select('photo_likes.*', DB::raw('COUNT(photo_likes.liked) as total'))
            ->where(['photo_likes.photo_id' => $request->value])
            ->groupBy('photo_likes.liked')
            ->get();
        // return $photolikes;

        $photo_details = UploadPhotos::select('upload_photos.*', 'upload_photos.created_at', 'upload_photos.id', 'users.id', 'users.name', 'users.slug', 'users.photo', 'users.facebook', 'users.twitter')
            ->join('users', 'upload_photos.user_id', '=', 'users.id')->find(['upload_photos.id' => $request->value])->first();


        $tags = json_decode($photo_details->followers_list);
        $photo = unserialize($photo_details->images);
        $words = explode(' ', $photo_details->name);
        $user_image = strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1));

        $photo_user = UploadPhotos::select('upload_photos.*', 'users.*')
            ->join('users', 'upload_photos.user_id', '=', 'users.id')
            ->first();
        $image = unserialize($photo_user->images);

        $tags = json_decode($photo_details->followers_list);
        $tags_count = empty($tags) ? 0 : count($tags);

        $isFollowing = DB::table('followers')
            // ->select('followers.*')
            ->where(['follows_id' => $photo_details->user_id])
            ->where(['user_id' => session()->get('user_id')])
            ->first();

        if (!empty($tags[0])) {
            $tags_details1 = User::select('users.*')->where(['name' => $tags[0]])->first();
        }
        if (!empty($tags[1])) {
            $tags_details2 = User::select('users.*')->where(['name' => $tags[1]])->first();
        }
        if (!empty($tags[2])) {
            $tags_details3 = User::select('users.*')->where(['name' => $tags[2]])->first();
        }

        $video = photocomment::select('photocomments.*', 'users.id as userID', 'users.photo as photo', 'users.name as name', 'users.slug as slug')
            ->join('users', 'photocomments.user_id', '=', 'users.id')
            ->where([['parent_id', $request->value], ['commentable_id', 0]])->orderByDesc('photocomments.id')->paginate(3);

        $data3 = "";
        // dd(url('userprofile/'));
        $data3 .= '<div class="photo-head">
		<div class="users">
			<a href="' . user_url($photo_details->slug) . '" class="user_img">';
        if (!empty($photo_details->photo)) {
            $data3 .= '<img src="' . url('/') . '/public/uploads/users/small/' . $photo_details->photo . '" alt="" />';
        } else {
            $data3 .= '<div class="shortnamevd">' . $user_image . '</div>';
        }
        $data3 .= '</a>
			<a href="' . user_url($photo_details->slug) . '" style="width:auto">' . $photo_details->name . '</a>
		</div>';

        if (isset($isFollowing)) {
            $data3 .= '<div id="suggested_unfollow_user' . $photo_details->id . '">
								<a onclick="unFollowUser(' . $photo_details->id . ')" class="btn-0flw" style="background:#f72e5e !Important"> <i class="fa fa-solid fa-heart"></i> 
									   Following 
							   </a>
						   </div>';
        } else {
            $data3 .= '<div id="suggested_follow_user' . $photo_details->id . '">
	<a onclick="followUser(' . $photo_details->id . ')" class="btn-0flw"> <i class="fa fa-heart-o"></i> 
		Follow 
	</a>
</div>';
        }
        $data3 .= '<div id="no-mobile15">   <div class="social">
				<a style="color: #000;font-size: 12px;margin: 3px 5px 0 0;">  Share:</a>
				<a href="' . $photo_details->facebook . '" id="facebook" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				<a href="' . $photo_details->twitter . '" id="twitter" class="twitter"><i class="fa fa fa-twitter" aria-hidden="true"></i></a>
				<a href="#" class="tumblr"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
				<a href="#" class="vk"><i class="fa fa-whatsapp" aria-hidden="true" style="line-height: 17px;"></i></a>
			</div>
		</div>
	</div>
	<div class="photo-middle_botttom" style="position:relative" id="no-mb00">
		<a href=' . user_url($photo_details->slug) . '>
			<h2 id="photo_title" style="font-size: 13.5px;margin:3px 0px 3px 0;display:block;font-weight: normal;color: #000;line-height: 17px;">' . $photo_details->photo_title . '</h2>
		</a>
		<span id="photo_tag_list" style="font-size: 12.5px;display:block;color:#8e8e8e;line-height: 7px;margin: 9px 0 6px 0;">' . $photo_details->tag_list . '</span>
		<span style="font-size:10px;color:#8e8e8e">' . date('M d Y', strtotime($photo_details->created_at)) . '</span>
		<div class="tag-followers">
			<span style="font-size:12px;color:#8e8e8e; margin-right:5px"> Tagged to</span>';
        if (isset($tags_details1) && !empty($tags_details1)) {
            $data3 .= '<a href="' . url('/') . '/' . $tags_details1->slug . '" class="user_img">';
            $words3 = explode(' ', $tags_details1->name);
            $user_image3 = strtoupper(substr($words3[0], 0, 1) . substr(end($words3), 0, 1));
            if (!empty($tags_details1->photo)) {
                $data3 .= '<img src="' . url('/') . '/public/uploads/users/small/' . $tags_details1->photo . '" alt="" style="width:30px;border-radius:100px;height:30px;object-fit:cover;margin-bottom:5px;">';
            } else {
                $data3 .= '<div class="shortnamevd" style="width:35px;height: 35px;object-fit: cover;">' . $user_image3 . '</div>';
            }
            $data3 .= '</a>';

        }

        if (isset($tags_details2) && !empty($tags_details2)) {
            $data3 .= '<a href="' . user_url($tags_details2->slug) . '" class="user_img">';
            $words4 = explode(' ', $tags_details2->name);
            $user_image4 = strtoupper(substr($words4[0], 0, 1) . substr(end($words4), 0, 1));
            if (!empty($tags_details2->photo)) {
                $data3 .= '<img src="' . url('/') . '/public/uploads/users/small/' . $tags_details2->photo . '" alt="" style="width:30px;border-radius:100px;height:30px;object-fit:cover;margin-bottom:5px;">';
            } else {
                $data3 .= '<div class="shortnamevd" style="width:35px;height: 35px;object-fit: cover;">' . $user_image4 . '</div>';
            }
            $data3 .= '</a>';

        }

        if (isset($tags_details3) && !empty($tags_details3)) {
            $data3 .= '<a href="' . url('/') . '/' . $tags_details3->slug . '" class="user_img">';
            $words5 = explode(' ', $tags_details3->name);
            $user_image5 = strtoupper(substr($words5[0], 0, 1) . substr(end($words5), 0, 1));
            if (!empty($tags_details3->photo)) {
                $data3 .= '<img src="' . url('/') . '/public/uploads/users/small/' . $tags_details3->photo . '" alt="" style="width:30px;border-radius:100px;height:30px;object-fit:cover;margin-bottom:5px;">';
            } else {
                $data3 .= '<div class="shortnamevd" style="width:35px;height: 35px;object-fit: cover;">' . $user_image5 . '</div>';
            }
            $data3 .= '</a>';

        }
        $data3 .= '</div></div>';

        if ($photo_details) {
            $data2 = "";
            $data2 .= '<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">';
            if (!empty($photo[0])) {
                $data2 .= '<li data-target="#myCarousel" data-slide-to="0" class="active"><img class="my" id="my" src="' . url('/') . '/public/uploads/photo/' . $photo[0] . '" alt="Los Angeles" style="width:100%;"></li>';
            }
            if (!empty($photo[1])) {
                $data2 .= '<li data-target="#myCarousel" data-slide-to="1"><img class="my" src="' . url('/') . '/public/uploads/photo/' . $photo[1] . '" alt="Chicago" style="width:100%;"></li>';
            }
            if (!empty($photo[2])) {
                $data2 .= '<li data-target="#myCarousel" data-slide-to="2"><img class="my" src="' . url('/') . '/public/uploads/photo/' . $photo[2] . '" alt="New york" style="width:100%;"></li>';
            }
            $data2 .= '</ol>
	
		<!-- Wrapper for slides -->
		<div class="carousel-inner">
		  <div class="item active">
			<img class="my" id="my" src="' . url('/') . '/public/uploads/photo/' . $photo[0] . '" alt="Los Angeles" style="width:100%;">
		  </div>';
            if (!empty($photo[1])) {
                $data2 .= '<div class="item">
		  
			<img class="my" src="' . url('/') . '/public/uploads/photo/' . $photo[1] . '" alt="Chicago" style="width:100%;">
		  </div>';
            }
            if (!empty($photo[2])) {
                $data2 .= '<div class="item">
				 
			<img class="my" src="' . url('/public/uploads/photo/' . $photo[2]) . '" alt="New york" style="width:100%;">
		  </div>
		</div>';
            }

            if (count($photo) > 1) {
                $data2 .= '<a class="left carousel-control" href="#myCarousel" data-slide="prev">
								<span class="glyphicon glyphicon-chevron-left"></span>
								<span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#myCarousel" data-slide="next">
								<span class="glyphicon glyphicon-chevron-right"></span>
								<span class="sr-only">Next</span>
							</a>';
            }

            $data2 .= '</div>';
        }

        $data = view("comment.photo", ['comments' => $video, 'type' => 'comment'])->render();

        //   return $photo_user;
        return response()->json(['photo_details' => $photo_details, 'photo' => $photo, 'image' => $image, 'html' => $data, 'html2' => $data2, 'html3' => $data3, 'photolikes' => $photolikes], 201);
    }


    public function childdetails(Request $request)
    {
        $comments = new photocomment;
        $comments->body = $request->value1;
        //$model = $request->get('model');
        //return  $request->get('photo_id');
        $comments->user()->associate(Session::get('user_id'));


        $video = UploadPhotos::find($request->value2);

        $comments->commentable_id = $request->value;
        $comments->save();

        $video = photocomment::select('photocomments.*', 'users.id as userID', 'users.photo as photo', 'users.name as name', 'users.slug as slug')
            ->join('users', 'photocomments.user_id', '=', 'users.id')
            ->where(['parent_id' => $request->value2, 'commentable_id' => 0])
            ->get();


        if ($video) {
            $data = "";
            foreach ($video as $videos) {

                $childreply = photocomment::select('photocomments.*', 'users.id as userID', 'users.photo as photo', 'users.name as name', 'users.slug as slug')
                    ->join('users', 'photocomments.user_id', '=', 'users.id')
                    ->where(['commentable_id' => $videos->id])->get();

                $data .= '  <div class="user mt-2" style="display:flex">
			<a href="#" class="user_img">';
                $words2 = explode(' ', $videos->name);
                $user_image2 = strtoupper(substr($words2[0], 0, 1) . substr(end($words2), 0, 1));
                if (!empty($videos->photo)) {
                    $data .= '<img src="' . url('/') . '/public/uploads/users/small/' . $videos->photo . '" alt="" style="width:35px;height: 35px;object-fit: cover;">';
                } else {
                    $data .= '<div class="shortnamevd" style="width:35px;height: 35px;object-fit: cover;">' . $user_image2 . '</div>';
                }
                $data .= '</a>
			<div style="float:left">
				<a href="' . user_url($videos->slug) . '" class="nm1" style="width:100%">' . $videos->name . '</a>
				<span>' . $videos->body . '
  <div style="clear:both; overflow:hidden; margin:3px 0 5px 0">
	 <small style="display:block;float:left;width:auto;font-size: 12px;padding: 2px 11px 0 0;">' . \Carbon\Carbon::parse($videos->created_at)->diffForHumans() . '</small>
	 <button type="button"  class="child_cmnt" value=' . $videos->id . '>Reply</button></div>
  </span>
			</div>
		</div>';
                foreach ($childreply as $childreplys) {
                    $data .= '<div class="user" style="display:flex; margin-left:3%; width:97%">
									  <a href="' . url('/') . '/' . $childreplys->slug . '" class="user_img">';
                    $words = explode(' ', $childreplys->name);
                    $user_image = strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1));
                    if (!empty($childreplys->photo)) {
                        $data .= '<img src="' . url('/') . '/public/uploads/users/small/' . $childreplys->photo . '" alt="" style="width:35px;height: 35px;object-fit: cover;">';
                    } else {
                        $data .= '<div class="shortnamevd" style="width:35px;height: 35px;object-fit: cover;">' . $user_image . '</div>';
                    }
                    $data .= '</a>
									  <div style="float:left">
										  <a href="' . user_url($childreplys->slug) . '" class="nm1" style="width:100%">' . $childreplys->name . '</a>
										  <span>' . $childreplys->body . '
										  <div style="clear:both; overflow:hidden; margin:3px 0 5px 0">
										   <small style="display:block;float:left;width:auto;font-size: 12px;padding: 2px 11px 0 0;">4min</small>
											  <a href="" style="float:left;width: 100px;padding: 0;font-size:12px; font-fmaliy:"Hind Guntur", sans-serif;">Reply</a>
										  </div>
									  </span>
									  </div>
								  </div>';
                }
            }
        } else {
            $video = "";
        }
        $data .= '<div class="mt-4 replyDiv" style="display: none"> <form class="mt-2" method="post" action=' . route('commentPhoto.add') . ' id="">
		  <textarea id="" name="reply_comment_body" placeholder="Add a comment…" class="PUqUI Ypffh message " autocomplete="off" autocorrect="off"></textarea>
		  <input type="hidden" class="childphotoID" name="photo_id" value="" />
		  <input type="hidden" name="model" value="photo" />
		  <button type="button" class="childsubmit" style="lift: 11px;top: 13px;">Submit</button>
	  </form></div>';


        return response()->json(['html' => $data]);

    }

    public function likedPhoto($photo_id)
    {
        $pageTitle = "Liked Video";
        $user = User::find(Session::get('user_id'));

        if (!empty($photo_id)) {
            $photo = UploadPhotos::where(['id' => $photo_id])
                ->first();

            if ($photo) {
                $other_user = User::find($photo->user_id);
                $likeData = PhotoLikes::where(['user_id' => Session::get('user_id'), 'photo_id' => $photo->id])->first();

                if ($likeData === null) {
                    $photoLikes = new PhotoLikes();
                    $photoLikes->photo_id = $photo->id;
                    $photoLikes->title = $photo->photo_title;
                    $photoLikes->url = \Request::url();
                    $photoLikes->session_id = \Request::getSession()->getId();
                    $photoLikes->user_id = Session::get('user_id');
                    $photoLikes->ip = \Request::getClientIp();
                    $photoLikes->liked = true;
                    $photoLikes->disliked = false;
                    $photoLikes->save();
                    $other_user->notify(new LikedPhoto($user, $photo));
                } else {
                    if ($likeData->liked == 1) {
                        PhotoLikes::where(['user_id' => Session::get('user_id'), 'photo_id' => $photo->id])->update(['updated_at' => DB::raw('NOW()'), 'disliked' => false, 'liked' => false]);
                    } else {
                        PhotoLikes::where(['user_id' => Session::get('user_id'), 'photo_id' => $photo->id])->update(['updated_at' => DB::raw('NOW()'), 'disliked' => false, 'liked' => true]);
                        $other_user->notify(new LikedPhoto($user, $photo));
                    }

                }

                $likeDislike = array(
                    'likeCount' => PhotoLikes::where(['photo_id' => $photo->id, 'liked' => true])->count(),
                    'disLikeCount' => PhotoLikes::where(['photo_id' => $photo->id, 'disliked' => true])->count()
                );
            }
        }

        return $likeDislike;
    }

    public function payment(Request $request)
    {
        $payment = new PaymentController('razorpay');
        $payment->load([
            'razorpay_payment_id' => $request->get('razorpay_payment_id'),
            'razorpay_order_id' => $request->get('razorpay_order_id'),
            'razorpay_signature' => $request->get('razorpay_signature'),
        ]);

        $result = $payment->capturePayment();
        if (isset($result['error'])) {
            return redirect()->back()->withErrors([$result['error']]);
        }

        $result['user']->notify(new Notices($result['user'], $result['message'], $result['payment']['notes']['type'], '', '', '', ''));
        return redirect()->back()->with('success', 'Payment Successull');
    }


    // Start Adding Code
    public function createPhotos(Request $request)
    {
        $pageTitle = 'Photos';
        $rowperpage = $this->per_page;
        $totalrecords = UploadPhotos::count();
        $getPhotoList = UploadPhotos::select('upload_photos.*', 'photo_likes.liked', 'upload_photos.id as id', DB::raw('COUNT(photo_likes.liked) as total'))
            ->leftjoin('photo_likes', 'upload_photos.id', '=', 'photo_likes.photo_id')
            ->where('upload_photos.isDeleted', 0)
            ->groupBY('upload_photos.id')
            ->orderBy('id', 'desc')
            ->skip(0)
            ->take($this->per_page)
            ->get();

        return view('photos.index', [
            'title' => $pageTitle,
            'getPhotoList' => $getPhotoList,
            'rowperpage' => $rowperpage,
            'totalrecords' => $totalrecords
        ]);
    }


    public function advertiseWithUs()
    {
        $pageTitle = 'Advertise With Us';
        return view('homes.advertise-with-us', ['title' => $pageTitle]);
    }


    public function about()
    {
        $pageTitle = 'About Us';
        return view('pages.about', ['title' => $pageTitle]);
    }


    public function contact()
    {
        $pageTitle = 'Contact';
        return view('pages.contact', ['title' => $pageTitle]);
    }

    public function termsandconditions()
    {
        $pageTitle = 'Terms and conditions';
        return view('pages.terms-and-conditions', ['title' => $pageTitle]);
    }


    public function privacy()
    {
        $pageTitle = 'Privacy';
        return view('pages.privacy', ['title' => $pageTitle]);
    }

    public function faq()
    {
        $pageTitle = 'Faq';
        return view('pages.faq', ['title' => $pageTitle]);
    }

    public function test(Request $request)
    {
        $table = "artist_gallery";
        $query = DB::table($table)->get();

        if ($query && count($query) > 0) {
            foreach ($query as $q) {
                $slug = Str::slug(substr($q->photo_title, 0, 200)) . "-" . $q->id;
                DB::table($table)->where("id", $q->id)->update(['slug' => $slug]);
            }
        }
    }

    public function accountblock()
    {
        return view('users.accountblock');
    }

    public function addpaypaldetails(Request $request)
    {
        $this->validate($request, [
            "paypal_id" => "required"
        ]);

        $paypalId = $request->get('paypal_id');
        unset($input['_token']);
        $check = UserPaymentMethod::whereUserId(Session::get('user_id'))->first();
        if ($check) {
            $bank = PaypalDetail::whereId($check->value_id)->first();
            //return $input;
            if ($bank) {
                if ($bank->update([
                    'paypal_id' => $paypalId
                ])) {
                    return redirect()->back()->with('success', 'Bank Details Saved Sucessfully!');
                }
            } else {
                $valueid = PaypalDetail::create([
                    'paypal_id' => $paypalId
                ]);
                $check->update(['value_id' => $valueid]);
                return redirect()->back()->with('success', 'Bank Details Saved Sucessfully!');
            }

            return redirect()->back()->with('error', 'Opreation Failed!');
        } else {
            $valueid = PaypalDetail::create([
                'paypal_id' => $paypalId
            ]);

            $data = UserPaymentMethod::create([
                'user_id' => Session::get('user_id'),
                'value_id' => $valueid->id,
                'method_id' => 1
            ]);

            if ($data) {
                return redirect()->back()->with('success', 'Bank Details Saved Sucessfully!');
            } else {
                return redirect()->back()->with('error', 'Opreation Failed!');

            }
        }
    }

    public function addbankdetails(Request $request)
    {
        $this->validate($request, [
            "account_no" => "required|integer|confirmed|min:9",
            "bank_name" => "required",
            "ifsc_code" => "required",
            "state_id" => "required",
            "city" => "required",
            "phone_no" => "required|digits:10"
        ]);

        $input = $request->all();
        unset($input['_token']);
        unset($input['account_no_confirmation']);
        $check = UserPaymentMethod::whereUserId(Session::get('user_id'))->first();
        if ($check) {
            $bank = BankDetail::whereId($check->value_id)->first();
            //return $input;
            if ($bank) {
                if ($bank->update($input)) {
                    return redirect()->back()->with('success', 'Bank Details Saved Sucessfully!');
                }
            } else {
                $valueid = BankDetail::create($input);
                $check->update(['value_id' => $valueid]);
                return redirect()->back()->with('success', 'Bank Details Saved Sucessfully!');
            }
            return redirect()->back()->with('error', 'Opreation Failed!');
        } else {
            $valueid = BankDetail::create($input);
            $data = UserPaymentMethod::create([
                'user_id' => Session::get('user_id'),
                'value_id' => $valueid->id,
                'method_id' => 1
            ]);
            if ($data) {
                return redirect()->back()->with('success', 'Bank Details Saved Sucessfully!');
            } else {
                return redirect()->back()->with('error', 'Opreation Failed!');
            }
        }
    }

    public function payoutrequest(Request $request)
    {
        // $this->validate($request,[
        // "amount" => "required|integer|min:1500",
        // ]);
        $input = $request->all();
        Payout::create([
            "user_id" => Session::get('user_id'),
            "amount" => $input['amount'],
        ]);
        return redirect()->back()->with('success', 'Data Submited Successfully!');

        $check = Payout::whereUserId(Session::get('user_id'))->where(['is_pending' => 0, 'status' => 0])->first();
        $availablewallet = Wallet::where('user_id', Session::get('user_id'))->first();
        if ($check) {
            return redirect()->back()->with('error', 'You Have Already Submit The Payout Request!');
        } elseif ($availablewallet >= $input['amount']) {
            return redirect()->back()->with('error', "You don't have enough balance");
        } else {
            $availablewallet->update(['balance' => $availablewallet->balance - $input['amount']]);

            return redirect()->back()->with('success', 'Data Submited Successfully!');

        }


    }
}