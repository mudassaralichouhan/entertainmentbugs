/**
 * Created by E-developed Technology
 */
"use strict";
const EBUGS = {};
EBUGS.utils = {
    getThumbnailForVideo: async function (videoUrl) {
        const video = document.createElement("video");
        const canvas = document.createElement("canvas");
        video.style.display = "none";
        canvas.style.display = "none";

        // Trigger video load
        await new Promise((resolve, reject) => {
            video.addEventListener("loadedmetadata", () => {
                video.width = video.videoWidth;
                video.height = video.videoHeight;
                canvas.width = '665';
                canvas.height = '372';
                // Seek the video to 25%;
                video.currentTime = video.duration * 0.25;
            });
            video.addEventListener("seeked", () => resolve());
            video.src = videoUrl;
        });

        // Draw the thumbnailz
        canvas
            .getContext("2d")
            .drawImage(video, 0, 0, '672', '372');
        const imageUrl = canvas.toDataURL("image/png");
        return {thumb: imageUrl, duration: Math.floor(video.duration)};
    },
    doAjax: function (url, successHandler = null, options = {}) {
        $.ajax({
            url: url,
            headers: {
                token: $("meta[name='secure_token']").attr("content"),
            },
            success: successHandler,
            error: function (res) {
                console.log(res);
            },
            ...options
        });
    },
    followWithCount: function (response, user_id, action) {
        if (action == "follow") {
            const buttons = $(`[data-do='follow'][data-user-id='${user_id}']`);
            buttons.attr("style", "background-color: #f72e5e!important");
            buttons.html("<i class='fa fa-solid fa-heart'></i> Following");
            buttons.attr('data-do', 'unfollow');
        } else {
            const buttons = $(`[data-do='unfollow'][data-user-id='${user_id}']`);
            buttons.attr("style", "");
            buttons.html("<i class='fa fa-heart-o'></i> Follow");
            buttons.attr('data-do', 'follow');
        }
        $(`[data-follower-count][data-user-id='${user_id}']`).text(response.followCount);
    },
    isURL: function (url) {
        return /^https?:\/\/([^\/]+)\/(.+)/.test(url)
    },
    getURLObject: function (url) {
        return new URL(url);
    },
    getSearchParams: function (query) {
        return new URLSearchParams(EBUGS.utils.isURL(query) ? EBUGS.utils.getURLObject(query).search : query);
    }
};
EBUGS.action = {
    doFollow: function () {
        let responseHandler = "EBUGS.utils.followWithCount(response, user_id, action)";
        let user_id = $(this).data("userId");
        if ($(this).data("responseHandler")) {
            responseHandler = $(this).data("responseHandler");
        }
        if ($(this).data("template") == "with-count") {
            const target = $(this).data("target");
            EBUGS.utils.doAjax(BASE.url(`users/${user_id}/follow`), function (response) {
                try {
                    const myFunction = Function.apply(null, ["response", "user_id", "action", responseHandler.toString()]);
                    myFunction(response, user_id, "follow");
                } catch (error) {
                    console.error(error);
                }
            }, {type: "POST", data: {_token: $("meta[name='secure_token']").attr("content")}})
        } else {
            EBUGS.utils.doAjax(BASE.url("follow-user"), function (response) {
                const buttons = $(`[data-do='follow'][data-user-id='${user_id}']`);
                buttons.attr("style", "background-color: #f72e5e!important");
                buttons.html("<i class='fa fa-solid fa-heart'></i> Following");
                buttons.attr('data-do', 'unfollow');

                if (typeof responseHandler == "function") {
                    responseHandler(response)
                }
            }, {data: {user_id: user_id}, cache: false});
        }
    },
    doUnfollow: function () {
        let responseHandler = "EBUGS.utils.followWithCount(response, user_id, action)";
        let user_id = $(this).data("userId");
        if ($(this).data("responseHandler")) {
            responseHandler = $(this).data("responseHandler");
        }
        if ($(this).data("template") == "with-count") {
            const target = $(this).data("target");
            EBUGS.utils.doAjax(BASE.url(`users/${user_id}/unfollow`), function (response) {
                try {
                    const myFunction = Function.apply(null, ["response", "user_id", "action", responseHandler]);
                    myFunction(response, user_id, "unfollow");
                } catch (error) {
                    console.error(error);
                }
            }, {type: "POST", data: {_token: $("meta[name='secure_token']").attr("content")}})
        } else {
            EBUGS.utils.doAjax(BASE.url("unfollow-user"), function (response) {
                const buttons = $(`[data-do='unfollow'][data-user-id='${user_id}']`);
                buttons.attr("style", "");
                buttons.html('<i class="fa fa-heart-o"></i> Follow');
                buttons.attr('data-do', 'follow');

                if (typeof responseHandler == "function") {
                    responseHandler(response)
                }
            }, {data: {user_id: user_id}});
        }
    }
};
EBUGS.ads = {
    stack: [],
    fetched: [],
    isRunning: false,
    load: function () {
        if ($("[data-ad-block]:not([data-ad-loaded])").length) {
            $("[data-ad-block]:not([data-ad-loaded])").each(function () {
                const data = {};
                $(this).attr('data-ad-loaded', 1);

                if ($(this).data('adType') == 'ad') {
                    data['adType'] = $(this).data('adMode');
                    data['adSize'] = $(this).data('adSize');
                }

                EBUGS.ads.stack.push({elem: $(this), data: data});
            });

            this.run();
        }
    },
    run: function () {
        if (!this.isRunning) {
            this.isRunning = true;
            if (this.stack.length > 0) {
                let current = this.stack.shift();
                this.fetch(current.elem, current.data);
            }
        }
    },
    fetch: function (elem, data = {}) {
        $.ajax({
            method: "POST",
            url: BASE.url("api/promoted-post/ad"),
            headers: {
                "X-CSRF-TOKEN": $("meta[name='secure_token']").attr('content')
            },
            data: {
                ids: EBUGS.ads.fetched,
                type: elem.data('adType'),
                model: elem.data('adStyle'),
                ...data
            },
            success: function (res) {
                if (res.status) {
                    elem.data('ad-block', res.data.ads.id);
                    elem.attr('value', res.data.ads.post_id);
                    // elem.data('url', res.data.ads.url);
                    EBUGS.ads.fetched.push(res.data.ads.id);
                    elem.find('[data-ad-button]').prop('disabled', false);
                    if (res.data.ads.type == 'photo' || res.data.ads.type == 'story') {
                        // elem.find("[data-elem]").append(`<img src='${res.data.ads.src}' data-image /><button type='button' data-ad-button='${res.data.ads.buttonUrl}'>${res.data.ads.buttonText}</button>`);
                        elem.html(res.data.ads.html);
                        if (elem.data('adStyle') == 'mobile-listing') {
                            elem.get(0).removeAttribute('data-loaded');
                            Comment.initMobile();
                        }
                    } else if (res.data.ads.type == 'video' || res.data.ads.type == 'ad') {
                        // elem.find("[data-elem]").append(`<video controls data-video><source src='${res.data.ads.src}' /></video><button type='button' data-ad-button='${res.data.ads.buttonUrl}'>${res.data.ads.buttonText}</button>`);
                        elem.html(res.data.ads.html);
                        // elem.find("[data-video]").get(0).load();
                    } else {
                        elem.remove();
                    }
                } else {
                    elem.remove();
                }
            },
            complete: function () {
                if (EBUGS.ads.stack.length > 0) {
                    let current = EBUGS.ads.stack.shift();
                    EBUGS.ads.fetch(current.elem, current.data);
                } else {
                    EBUGS.ads.isRunning = false;
                }
            }
        })
    },
    redirect: function (elem) {
        elem.find('[data-ad-button]').prop('disabled', true);
        const button = elem.find("[data-ad-button]").data('adButton');
        EBUGS.ads.stack.push({elem: elem, data: {id: elem.data('adBlock')}});
        EBUGS.ads.run();
        if (new RegExp("^https?:\/\/(.+)$").test(button)) {
            // window.location.href = button;
            window.open(button, "_blank");
        }
    }
};
EBUGS.croppie = {
    index: 0,
    current: 0,
    append: 0,
    collection: {},
    init: function (elem) {
        const croppies = $("[data-croppie]");
        if (croppies.length) {
            croppies.each(function () {
                EBUGS.croppie.load($(this));
            });
        }
    },
    bindCroppie: function (previewElem, viewport = null, boundary = null) {
        const el = previewElem.croppie({
            showZoomer: true,
            enableOrientation: viewport && viewport.length ? true : false,
            viewport: viewport && Object.keys(viewport).length ? viewport : {width: 400, height: 400, type: "square"},
            boundary: boundary && Object.keys(boundary).length ? boundary : {width: 400, height: 400},
        });

        return el;
    },
    load: function (elem, callback = null) {
        this.modal(this.index, elem.data('croppieModalKey'));
        const previewElem = this.bindCroppie(
            $("#croppie-preview-" + this.index),
            elem.data('croppieViewport'),
            elem.data('croppieBoundary')
        );
        this.clickHandler($("#croppie-action-crop-" + this.index), previewElem, elem, callback);
        this.changeHandler($(elem.data("croppieFile")), $("#croppie-action-crop-" + this.index), previewElem, this.index);

        if (elem.data("croppieBind")) {
            if (elem.data('croppieBind') === "file") {
                elem.on("click", function () {
                    $(elem.data('croppieFile')).trigger('click');
                });
            }
        }

        this.index++;
    },
    clickHandler: function (cropBtn, preview, elem, callback) {
        const storeInfo = {};
        const output = elem.data('croppieSize');
        let size = output && (/^(type|size|format|quality|circle)$/.test(output) || Object.keys(output)) ? output : "viewport";

        if (elem.data('croppieStore')) {
            storeInfo['path'] = elem.data('croppieStore');
        }

        const result = {
            type: 'canvas',
            size: size,
            type: "blob",
        };

        cropBtn.on("click", function (e) {
            const that = $(this);
            const target = that.data("croppie-target");

            preview
                .croppie('result', result)
                .then(response => {
                    const formData = new FormData();
                    formData.append("image", response);
                    formData.append("store", JSON.stringify(storeInfo));

                    let progressBar = elem.data('croppieProgress');
                    const xhr = new XMLHttpRequest();
                    if (progressBar && $(progressBar).length) {
                        progressBar = $(progressBar);
                        xhr.upload.addEventListener("progress", function (event) {
                            progressBar.css('display') !== 'block' && progressBar.css('display', 'block');
                            if (event.lengthComputable) {
                                progressBar.find(".progress-bar").css("width", (event.loaded / event.total) * 100 + "%");
                                progressBar.find(".progress-bar").css("aria-valuenow", (event.loaded / event.total) * 100);
                                progressBar.find(".sr-only").html(" " + ((event.loaded / event.total) * 100).toFixed() + "%");
                                progressBar.find(".up-done").html((parseInt(parseInt(event.loaded / 1024)) / 1024).toFixed(2));
                                progressBar.find('.percent-process').html('<strong>' + parseInt((event.loaded / event.total) * 100).toFixed(0) + '%</strong>');
                            } else {
                                alert("Failed to compute file upload length");
                            }
                        }, false);
                    }
                    xhr.open("POST", BASE.url("upload/temp_base64_image"));
                    xhr.onload = function () {
                        const json = xhr.response;
                        progressBar && progressBar.css('display', 'none');
                        try {
                            const result = JSON.parse(json);

                            if (result.status == true) {
                                const input = $(elem.data("croppieInput"));
                                const output = $(elem.data("croppieOutput"));
                                const del = $(elem.data("croppieDelete"));

                                input.length ? input.val(result.image) : null;
                                output.length ? output.attr('src', result.image) : null;
                                del.length ? del.remove() : null;

                                if (callback && typeof callback == 'function') {
                                    callback(result.image, result);
                                }
                            } else {
                                alert("File is not uploading. Please try again");
                            }
                        } catch (error) {
                            console.error(error);
                        }
                    }
                    const csrfToken = $('form').find('input[name=_token]').val();
                    xhr.setRequestHeader('X-csrf-token', csrfToken);
                    xhr.send(formData);
                })
        });
    },
    changeHandler: function (input, cropBtn, preview, index) {
        input.on("change", function () {
            let flag = true;
            const imageType = /image.*/;
            const file = this.files[0];
            if (!file.type.match(imageType)) {
                alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
                return false;
            }

            // check file size
            if (parseInt(file.size / 1024) > (1024 * 2)) {
                alert("File \"" + file.name + "\" is too big.");
                return false;
            }

            if (flag) {
                const fieldName = $(this).attr('target');
                cropBtn.attr('target', fieldName);

                $(".img-container").removeClass('d-none');
                const reader = new FileReader();

                reader.onload = function (event) {
                    preview.croppie('bind', {
                        url: event.target.result
                    });
                }
                reader.readAsDataURL(this.files[0]);
                $("#croppie-modal-" + index).modal();
            } else {
                return flag;
            }
        });
    },
    modal: function (index, key = null) {
        const modal = `<div class="modal" data-croppie-mkey="${key}" id="croppie-modal-${index}" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Select Area ( Use wheel to zoom in & out )</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<i aria-hidden="true" class="fa fa-times"></i>
						</button>
					</div>
					<div class="modal-body">
						<div class="d-none img-container" style="border-right:1px solid #ddd;">
						<div id="croppie-preview-${index}"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" data-croppie-cbkey="${key}" class="btn btn-light-primary font-weight-bold" id="croppie-action-crop-${index}" data-dismiss="modal">Crop</button>
				</div>
			</div>
		</div>
	</div>`;

        $("body").append(modal);
    }
};

const Comment = {
    index: 0,
    current: 0,
    times: 0,
    collection: {},
    timeout: null,
    isMobile: true,
    init: function (photoBlock) {
        this.times = 0;
        this.current = photoBlock.attr('value');
        this.isMobile = false;
        this.index = 0;

        const my = this;
        const elem = $("#popup-wrapper--desk");

        if (elem.length > 0) {
            elem.find(".desk-post:not([data-loaded])").each(function () {
                $(this).attr('data-loaded', 1);
                const photoId = $(this).data('photoId');
                if ($(this).find(".mobile-comments .load-more-btn").length > 0) {
                    my.loadMore($(this).find(".mobile-comments .load-more-btn"), $(this).find(".mobile-comments"), photoId);
                }
                my.loadReplies($(this), true);
            });
        }
        ;
    },
    initMobile: function () {
        const my = this;
        const elem = $(".mobile-view-photos-scroll");
        this.isMobile = true;
        if (elem.length > 0) {
            elem.find(".post:not([data-loaded])").each(function () {
                $(this).attr('data-loaded', 1);
                const photoId = $(this).data('photoId');
                if ($(this).find(".mobile-comments .load-more-btn").length > 0) {
                    my.loadMore($(this).find(".mobile-comments .load-more-btn"), $(this).find(".mobile-comments"), photoId);
                }
                my.loadReplies($(this), true);
            });
        }
        ;
    },
    commentBox: function (comment) {

    },
    event: {
        onBlur: function (e) {

        },
        onClick: function (e) {

        },
        onSubmit: function (e) {

        }
    },
    html: {
        loadMoreBtn: `<button type='button' data-load-more>Load more</button>`,
        loadReplies: function (left) {
            return `<button type='button' data-load-replies>${left} more replies</button>`;
        },
        comment: Comment => {
            return `<div class="user mt-2" style="display:flex" data-comment-id="${Comment.id}">
							<div class="comment">
								<a href="#" class="user_img">
								${
                Comment.icon == "img"
                    ? `<img src="${Comment.userImg}" alt="" style="width:35px;height: 35px;object-fit: cover;">`
                    : `<div class="shortnamevd" style="width:35px;height: 35px;object-fit: cover;">${Comment.alt}</div>`
            }
								</a>
								<div style="float:left">
									<a href="" class="nm1" style="width:100%">${Comment.username}</a>
									<span>${Comment.message}
										<div style="clear:both; overflow:hidden; margin:3px 0 5px 0">
											<small style="display:block;float:left;width:auto;font-size: 12px;padding: 2px 11px 0 0;">${Comment.readableTime}</small>
											<button type="button" class="child_cmnt">Reply</button>
										</div>
									</span>
								</div>
							</div>
						</div>`;
        }
    },
    fetch: function (url, data, success = null, error = null, complete = null) {
        $.ajax({
            url: url,
            method: "GET",
            data: data,
            headers: {
                "Content-Type": "application/json"
            },
            success: success,
            error: error,
            complete: complete
        });
    },
    reply: {
        submit: function (replyTo, msg, elem) {
            const obj = {
                comment_body: msg,
                photo_id: window.innerWidth <= 576
                    ? elem.closest('.post').data('photoId')
                    : elem.closest('.desk-post').data('photoId'),
                model: "photo"
            };

            if (replyTo) {
                obj['comment_id'] = replyTo.data('commentId');
            }

            $.ajax({
                url: BASE.url("comment/storephoto"),
                method: "POST",
                data: obj,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="secure_token"]').attr('content')
                },
                success: function (data) {
                    try {
                        if (data.html) {
                            replyTo.find(".nested-comments .comments").html(data.html);
                        }
                    } catch (error) {
                        console.error("something went wrong.");
                    }
                }
            })
        }
    },
    loadReplies: function (elem = null, isMobile = false) {
        let loadMoreRepliesBtn;
        loadMoreRepliesBtn = elem.find(".view-more-replies:not([data-load])");

        if (loadMoreRepliesBtn.length) {
            const page = 1, my = this;

            loadMoreRepliesBtn.each(function () {
                const that = this;
                this.setAttribute('data-load', page);
                $(this).on("click", function () {
                    let page = $(this).data("load");
                    const left = parseInt($(this).find("span").text().trim());
                    if (left > 0) {
                        const commentContainer = $(this).prev();
                        my.fetch(BASE.url("photo/comments/" + (isMobile ? elem.data('photoId') : my.current) + "/" + $(this).closest(".user").data('commentId')), {page: page}, function (data) {
                            commentContainer.append(data.html);
                            if (data.length) {
                                $(that).data("load", ++page);
                                if ((left - data.length) <= 0) {
                                    commentContainer.next().remove();
                                } else {
                                    $(that).find("span").text((left - data.length));
                                }
                            }

                            my.loadReplies(elem, isMobile);
                        })
                    }
                });
            });
        }
    },
    loadMore: function (loadMoreBtn, target, current = null, page = 2) {
        // const my = this;
        // const pagination = {
        //   current: 1,
        //   total: 0,
        //   isFirst: true,
        // };
        const my = this;
        $(loadMoreBtn).on("click", function () {
            $(this).parent().remove();
            my.fetch(BASE.url("photo/comments/" + (current ?? my.current)), {page: page}, function (data) {
                try {
                    // pagination.current = data.current;
                    // pagination.total = data.total;
                    // pagination.isFirst = false;

                    if (typeof data['html'] !== 'undefined') {
                        $(target).append(data.html);
                        if ($(target).find(".load-more-btn").length) {
                            my.loadMore($(target).find(".load-more-btn").get(0), target, current, ++page);
                        }
                        if (current) {
                            my.loadReplies($(`${my.isMobile ? '.mobile-view-photos-scroll' : '#popup-wrapper--desk'} [data-photo-id="${current}"]`), true);
                        } else {
                            my.loadReplies();
                        }
                        // data.data.forEach(item => {
                        // });
                    }
                } catch (error) {

                }
            });
        });
    }
};

EBUGS.earnings = {
    cache: null,
    loader: $("#earning-board ._loading"),
    init: function () {
        if ($("#earning-board").length) {
            if (!this.cache) {
                this.cache = new Map();
            }

            this.bindOnChangeTab();
            this.bindOnClickPaginate();
            this.bindOnChangePeriod();
            this.bindChartManager();
        }
    },
    load: function (action, selector = null, options = {}) {
        EBUGS.earnings.loader.addClass('active');
        const isURL = EBUGS.utils.isURL(action);
        const currentAction = isURL ? EBUGS.utils.getSearchParams(action)?.get('action') : action;

        if (EBUGS.earnings.cache.has(action)) {
            EBUGS.earnings.setContent(selector ?? `#${currentAction}-analytic`, action);
            EBUGS.earnings.loader.removeClass('active');
            return;
        }

        $.ajax({
            url: isURL ? action : BASE.url(`earnings?action=${action}&page=1`),
            success: function (res) {
                let html = res?.html ?? "";
                EBUGS.earnings.cache.set(action, html);
                EBUGS.earnings.setContent(selector ?? `#${currentAction}-analytic`, null, html);
            },
            error: function (error) {
                console.log('error: ', error);
            },
            complete: function () {
                EBUGS.earnings.loader.removeClass('active');
            }
        });
    },
    setContent: function (selector, action = null, html = null) {
        $(selector).html(html ?? EBUGS.earnings.cache.get(action));

        if (selector.startsWith("#show_graph_")) {
            const context = $(selector).find("canvas")?.get(0);

            if (context) {
                EBUGS.charts.load(context);
            }
        }
    },
    bindOnChangeTab: function () {
        $("#earning-board #myTab .nav-link").on("click", function (e) {
            e.preventDefault();
            if (!$(this).parent().hasClass('active')) {
                const action = $(this).attr('id').replace('-tab', '');

                $("#earning-board").find("#myTab .nav-item, #myTabContent .tab-pane").removeClass('active in');
                $(this).parent().addClass('active');
                $("#earning-board #" + action).addClass('active in');

                EBUGS.earnings.load($(this).data('tab'));
            }
        });
    },
    bindOnClickPaginate: function () {
        $("#earning-board").on("click", ".pagination a", function (e) {
            e.preventDefault();
            EBUGS.earnings.load($(this).attr('href'));
        })
    },
    bindOnChangePeriod: function () {
        $("#earning-board").on('change', '.graph_period', function () {
            const period = $(this).val();
            const elemId = $(this).closest('.videolist').find('.show_graph').attr('id');
            const [elemName, action, id] = elemId.match(/^show_graph_([a-z_]+)_([0-9]+)$/);
            EBUGS.earnings.load(BASE.url(`earnings?action=${action}&period=${period}&id=${id}`), `#${elemName}`, {isChart: true});
        });
    },
    bindChartManager: function () {
        $("#earning-board").on("click", "[data-chart]", function () {
            const container = $($(this).data('chart'));
            const canvas = container.find("[data-chart-type]");
            container.toggle(300, "linear", function () {
                $(this).closest(".videolist").find(".show_graph_period").css('display', $(this).css('display'));
                canvas.height(300);
            });

            if (!canvas.data('loaded')) {
                canvas.data('loaded', 1);

                const ctx = canvas.get(0);
                EBUGS.charts.load(ctx);
            }
        });
    }
};

EBUGS.charts = {
    /**
     * load chart with the provided content
     * @param ctx canvas 2d context, context should contain data-chart-date and data-chart-data
     * @return Chart
     */
    load: function (ctx) {
        return new Chart(ctx, {
            type: 'bar',
            data: {
                labels: $(ctx).data('chartDate'),
                datasets: [$(ctx).data('chartData')],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                elements: {
                    point: {
                        radius: 0
                    }
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        barPercentage: 0.45,
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            // fontColor: "#8a909d",
                        },
                    },],
                    yAxes: [{
                        stacked: true,
                        gridLines: {
                            display: false,
                            color: "#eee",
                        },
                        ticks: {
                            stepSize: 50,
                            // fontColor: "#8a909d",
                        },
                    },],
                },
                tooltips: {
                    mode: "index",
                    intersect: false,
                    titleFontColor: "#888",
                    bodyFontColor: "#555",
                    titleFontSize: 12,
                    bodyFontSize: 15,
                    backgroundColor: "rgba(256,256,256,0.95)",
                    displayColors: true,
                    xPadding: 10,
                    yPadding: 7,
                    borderColor: "rgba(220, 220, 220, 0.9)",
                    borderWidth: 2,
                    caretSize: 6,
                    caretPadding: 5,
                }
            }
        });
    }
}

$(function () {
    EBUGS.earnings.init();

    $(document).on("click", "[data-do='follow']", EBUGS.action.doFollow);
    $(document).on("click", "[data-do='unfollow']", EBUGS.action.doUnfollow);
    $(document).on("click", "[data-ad-button]", function (e) {
        e.preventDefault();
        if ($(this).closest(".photo-block").length == 0) {
            $(this).prop('disabled', true);
            let ad = {};
            let adElem = $(this).closest("[data-ad-block]");
            if (adElem.data('adType') == 'ad') {
                ad['adSize'] = adElem.data('adSize');
                ad['adType'] = adElem.data('adMode');
            }
            EBUGS.ads.stack.push({elem: adElem, data: {id: adElem.data('adBlock'), ...ad}});
            EBUGS.ads.run();
            if (new RegExp("^https?:\/\/(.+)$").test($(this).data('adButton'))) {
                // window.location.href = $(this).data('adButton');
                window.open($(this).data('adButton'), "_blank");
            }
        }
        return false;
    });

    // Initialize EBUGS events here.
    EBUGS.ads.load();

    const debounce = function (callback) {
        this.timer = null;
        this.pause = false;
        this.end = false;
        this.callback = callback;
        this.resume = function () {
            this.pause = true;
            this.call(this.callback)();
        };
        this.call = function (callback, wait = 3000) {
            return () => {
                if (this.timeout) {
                    clearTimeout(this.timeout);
                }

                if (!this.pause) {
                    timeout = setTimeout(callback, wait);
                }
            };
        };

        this.call(this.callback)();
    }

    const VideoTracker = {
        _token: $("meta[name='_token']").attr('content'),
        timer: 0,
        timeout: null,
        events: {
            ontimeupdate: function () {

            },
            onplay: function () {
                VideoTracker.timeout = null;
            },
            onpause: function () {
                VideoTracker.timeout = null;
            },
            onended: function () {

            },
            onstart: function () {

            },
            onresume: function () {

            },
            onunload: function () {

            }
        },
        updateTimer: function () {
            const my = this;
            $.ajax({
                method: "POST",
                url: BASE.url("video/timer/" + video + "/" + my._timer),
                headers: {
                    "X-CSRF-TOKEN": my._token,
                    "CONTENT-TYPE": "application/json"
                },
                data: {}
            })
        }
    };

    /**
     * Initialize Objects
     */
    EBUGS.croppie.init();

    const PhotoManager = {
        scrollReached: 0,
        slide: 0,
        elem: null,
        length: 0,
        loadMobileContent: function (elem, page = null) {
            if (elem) {
                this.elem = elem;
            } else {
                elem = this.elem;
            }
            let photo_id = $(elem).attr("value");
            $('.photoIDLikes').attr("href", photo_id);
            $('#PhotoID').val(photo_id);
            $("#mobile-photos").show();
            $("body").css("overflow", "hidden");
            $.ajax({
                type: 'GET',
                data: {
                    value: photo_id,
                    page: page ?? 1
                },
                url: BASE.url("photos/load"),
                cache: false,
                dataType: "json",
                success: (response) => {
                    if (response.html) {
                        if (!page) {
                            $("#mobile-photos .fw-container").html(response.html);
                        } else {
                            $("#mobile-photos .fw-container").append(response.html);
                        }
                        if ($(".mobile-view-photos-scroll .mobile-load-more-posts").length) {
                            PhotoManager.scrollReached = 0;
                        }
                    } else {
                        $("#mobile-photos").hide();
                    }

                    EBUGS.ads.load();
                    Comment.initMobile();
                },
            })
        },
        loadDeskContent: function (elem, page = null) {
            if (!page) {
                this.slide = 0;
            }
            if (elem) {
                this.elem = elem;
            } else {
                elem = this.elem;
            }
            var photo_id = $(elem).attr('value');
            $('.photoIDLikes').attr("href", photo_id);
            $('#PhotoID').val(photo_id);
            $.ajax({
                type: 'GET',
                data: {
                    value: photo_id,
                    page: page ?? 1,
                    screen: "l"
                },
                url: BASE.url("photos/load"),
                cache: false,
                success: (response) => {

                    if (response.html) {
                        if (!page) {
                            $("#popup-wrapper--desk").html(response.html);
                        } else {
                            $("#popup-wrapper--desk").append(response.html);
                        }
                        if ($("#popup-wrapper--desk .load-more-photos").length) {
                            PhotoManager.scrollReached = 0;
                        }

                        $(".over-all").fadeIn();
                        $(".over-all a").click(function () {
                            $(".over-all").fadeOut();
                        });
                    } else {
                        $(".over-all").fadeOut();
                    }
                    PhotoManager.length += $(".desk-post:not([data-loaded])").length;
                    Comment.init($(elem));
                    if (!page) {
                        PhotoManager.deskSlider(elem);
                    }
                    EBUGS.ads.load();
                },
            });
        },
        deskSlider: function (elem) {
            const container = $("#popup-wrapper--desk").closest('.over-all');
            const arrow = container.find(".arrow");

            const my = this;
            let timer = null;
            arrow.on("click", function () {
                if (!timer) {
                    timer = setTimeout(() => {
                        clearTimeout(timer);
                        timer = null;
                        if ($(this).hasClass('arrow-prev')) {
                            my.slide--;
                            if (my.slide == 0) {
                                $(this).hide();
                            } else {
                                $(this).show();
                            }
                        } else {
                            my.slide++;
                            container.find(".arrow-prev").show();
                        }

                        if (my.slide == my.length - 2 && container.find(".load-more-photos").length) {
                            my.loadDeskContent(elem, container.find(".load-more-photos").data('page'));
                            container.find(".load-more-photos").remove();
                        }

                        if (my.slide < my.length) {
                            $(".desk-post").css('transform', `translateX(-${my.slide * 100}%)`);
                        } else {
                            my.slide = my.length - 1;
                        }

                    }, 300);
                }
            });
        }
    };

    if ($("#category_photo_data").length) {
        if (typeof $("#category_photo_data").data("remove") == 'undefined' || $("#category_photo_data").data("remove") == "true") {
            $(window).on('resize', function () {
                PhotoManager.scrollReached = 0;
                $("body").css("overflow", "unset");
                if (window.innerWidth <= 576) {
                    $(".over-all").hide();
                    $('#comment-box099').html('');
                    $('#slider').html('');
                    $('#photo-container-all').html('');
                } else {
                    $(".mobile-view-photos-scroll").hide();
                    $(".mobile-view-photos-scroll .fw-container").html('');
                }
            });
        }

        $(".mobile-view-photos-scroll").on("scroll", function () {
            if (!PhotoManager.scrollReached) {
                if (($(".mobile-view-photos-scroll").scrollTop() + $(window).height()) >= $(".mobile-view-photos-scroll .fw-container").height() - 50) {
                    PhotoManager.scrollReached = 1;

                    if ($(".mobile-view-photos-scroll .mobile-load-more-posts").length) {
                        let page = $(".mobile-view-photos-scroll .mobile-load-more-posts").data('page');
                        $(".mobile-view-photos-scroll .mobile-load-more-posts").remove();
                        PhotoManager.loadMobileContent(null, page);
                    }
                }
            }
        });

        $('.photo-block-all-in-one').on("click", ".photo-block", function (e) {
            const target = e.target;
            if (target.tagName.toLowerCase() == 'button' && $(target).data('adButton')) {
                e.preventDefault();
                e.stopPropagation();
                EBUGS.ads.redirect($(this));
                return false;
            }

            if ($(this).closest("#category_photo_data").length) {
                if (window.innerWidth <= 576) {
                    PhotoManager.scrollReached = 0;
                    PhotoManager.loadMobileContent(this);
                } else {
                    PhotoManager.loadDeskContent(this);
                }
            }
        });
        $('.photo-block-all-in-one').on("click", ".inner-photo", function (e) {
            const target = e.target;
            if (target.tagName.toLowerCase() == 'button' && $(target).data('adButton')) {
                e.preventDefault();
                e.stopPropagation();
                EBUGS.ads.redirect($(this));
                return false;
            }

            if ($(this).closest("#category_photo_data").length) {
                if (window.innerWidth <= 576) {
                    PhotoManager.scrollReached = 0;
                    PhotoManager.loadMobileContent(this);
                } else {
                    PhotoManager.loadDeskContent(this);
                }
            }
        });
        $(".photo-container-inside, .photo-container-inside1, .mobile-view-photos-scroll").on("click", ".child_cmnt", function () {
            if (window.innerWidth <= 576 && $(this).closest("[data-strict-desk]").length == 0) {
                $(".mobile-view-photos-scroll [data-box]").remove();
            } else {
                $("#comment-box099 [data-box]").remove();
            }

            $(this).closest(".user").find(".comment-box").html("<div data-box><textarea class='form-control' name='comment-box' placeholder='Write your comment here'></textarea><button type='button' class='data-child-comment' style='margin-top: 5px'>Submit</button></div>");
            $(this).closest(".user").find("[name='comment-box']").blur(function () {
                setTimeout(() => {
                    $(this).parent().remove()
                }, 200)
            });
            $(this).closest(".user").find("[name='comment-box']").next().click(function () {
                Comment.reply.submit($(this).closest("[data-comment-id]"), $(this).prev().val(), $(this));
            });
        });
    }
    var image_crop = $('#image-preview').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    var image_crop1 = $('#image-preview1').croppie({
        showZoomer: true,
        viewport: {
            width: 665,
            height: 372,
            type: 'square'
        },
        boundary: {
            width: 665,
            height: 372,
        }
    });

    var image_crop2 = $('#image-preview2').croppie({
        showZoomer: true,
        viewport: {
            width: 1350,
            height: 570,
            type: 'square'
        },
        boundary: {
            width: 1350,
            height: 570,
        }
    });

    var achivementImage1 = $('#ach-preview1').croppie({
        showZoomer: true,
        viewport: {
            width: 300,
            height: 300,
            type: 'square'
        },
        boundary: {
            width: 300,
            height: 300,
        }
    });

    var achivementImage2 = $('#ach-preview2').croppie({
        showZoomer: true,
        viewport: {
            width: 300,
            height: 300,
            type: 'square'
        },
        boundary: {
            width: 300,
            height: 300,
        }
    });

    var achivementImage3 = $('#ach-preview3').croppie({
        showZoomer: true,
        viewport: {
            width: 300,
            height: 300,
            type: 'square'
        },
        boundary: {
            width: 300,
            height: 300,
        }
    });

    var achivementImage4 = $('#ach-preview4').croppie({
        showZoomer: true,
        viewport: {
            width: 300,
            height: 300,
            type: 'square'
        },
        boundary: {
            width: 300,
            height: 300,
        }
    });

    var achivementImage5 = $('#ach-preview5').croppie({
        showZoomer: true,
        viewport: {
            width: 300,
            height: 300,
            type: 'square'
        },
        boundary: {
            width: 300,
            height: 300,
        }
    });

    $('#fileachive1').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_ach1").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                achivementImage1.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_ach_modal1").modal();
        } else {
            return flag;
        }
    });

    $('#crop_ach1').click(function (event) {

        var tr = $(this).attr('target');
        achivementImage1.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_achieve_image1").val(result.image);
                    $("#previewachievesec1").attr('src', response);
                    $('#crossachive1').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#fileachive2').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_ach2").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                achivementImage2.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_ach_modal2").modal();
        } else {
            return flag;
        }
    });

    $('#crop_ach2').click(function (event) {

        var tr = $(this).attr('target');
        achivementImage2.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_achieve_image2").val(result.image);
                    $("#previewachievesec2").attr('src', response);
                    $('#crossachive2').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#fileachive3').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_ach3").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                achivementImage3.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_ach_modal3").modal();
        } else {
            return flag;
        }
    });

    $('#crop_ach3').click(function (event) {

        var tr = $(this).attr('target');
        achivementImage3.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_achieve_image3").val(result.image);
                    $("#previewachievesec3").attr('src', response);
                    $('#crossachive3').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#fileachive4').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_ach4").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                achivementImage4.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_ach_modal4").modal();
        } else {
            return flag;
        }
    });

    $('#crop_ach4').click(function (event) {

        var tr = $(this).attr('target');
        achivementImage4.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_achieve_image4").val(result.image);
                    $("#previewachievesec4").attr('src', response);
                    $('#crossachive4').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#fileachive5').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_ach5").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                achivementImage5.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_ach_modal5").modal();
        } else {
            return flag;
        }
    });

    $('#crop_ach5').click(function (event) {

        var tr = $(this).attr('target');
        achivementImage5.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_achieve_image5").val(result.image);
                    $("#previewachievesec5").attr('src', response);
                    $('#crossachive5').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });


    var projImage1 = $('#proj-preview1').croppie({
        showZoomer: true,
        viewport: {
            width: 370,
            height: 310,
            type: 'square'
        },
        boundary: {
            width: 370,
            height: 310,
        }
    });

    var projImage2 = $('#proj-preview2').croppie({
        showZoomer: true,
        viewport: {
            width: 370,
            height: 310,
            type: 'square'
        },
        boundary: {
            width: 370,
            height: 310,
        }
    });

    var projImage3 = $('#proj-preview3').croppie({
        showZoomer: true,
        viewport: {
            width: 370,
            height: 310,
            type: 'square'
        },
        boundary: {
            width: 370,
            height: 310,
        }
    });

    // $('#fileproj1').change(function( ){
    //     var flag = true;
    //     var imageType = /image.*/;
    //     var file = this.files[0];
    //     if (!file.type.match(imageType)) {
    //         alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
    //         flag = false;
    //         return false;
    //     }
    //     // check file size
    //     if (parseInt(file.size / 1024) > (1024*2)) {
    //         alert("File \""+file.name+"\" is too big.");
    //         flag = false;
    //         return false;
    //     }

    //     if(flag == true){
    //         var field_name = $(this).attr('target');
    //         $("#crop_proj1").attr('target', field_name);

    //         $(".img-container").removeClass('d-none');
    //         var reader = new FileReader();

    //         reader.onload = function(event){

    //             projImage1.croppie('bind', {
    //                 url:event.target.result
    //               }).then(function(){

    //             });
    //         }
    //         reader.readAsDataURL(this.files[0]);

    //         $("#img_cutting_proj_modal1").modal();
    //     }else{
    //         return flag;
    //     }
    //   });

    $('#crop_proj1').click(function (event) {

        var tr = $(this).attr('target');
        projImage1.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_proj_image1").val(result.image);
                    $("#previewprojsec1").attr('src', response);
                    $('#crossproj1').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    // $('#fileproj2').change(function( ){
    //     var flag = true;
    //     var imageType = /image.*/;
    //     var file = this.files[0];
    //     if (!file.type.match(imageType)) {
    //         alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
    //         flag = false;
    //         return false;
    //     }
    //     // check file size
    //     if (parseInt(file.size / 1024) > (1024*2)) {
    //         alert("File \""+file.name+"\" is too big.");
    //         flag = false;
    //         return false;
    //     }

    //     if(flag == true){
    //         var field_name = $(this).attr('target');
    //         $("#crop_proj2").attr('target', field_name);

    //         $(".img-container").removeClass('d-none');
    //         var reader = new FileReader();

    //         reader.onload = function(event){

    //             projImage2.croppie('bind', {
    //                 url:event.target.result
    //               }).then(function(){

    //             });
    //         }
    //         reader.readAsDataURL(this.files[0]);

    //         $("#img_cutting_proj_modal2").modal();
    //     }else{
    //         return flag;
    //     }
    //   });

    $('#crop_proj2').click(function (event) {

        var tr = $(this).attr('target');
        projImage2.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_proj_image2").val(result.image);
                    $("#previewprojsec2").attr('src', response);
                    $('#crossproj2').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    // $('#fileproj3').change(function( ){
    //     var flag = true;
    //     var imageType = /image.*/;
    //     var file = this.files[0];
    //     if (!file.type.match(imageType)) {
    //         alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
    //         flag = false;
    //         return false;
    //     }
    //     // check file size
    //     if (parseInt(file.size / 1024) > (1024*2)) {
    //         alert("File \""+file.name+"\" is too big.");
    //         flag = false;
    //         return false;
    //     }

    //     if(flag == true){
    //         var field_name = $(this).attr('target');
    //         $("#crop_proj3").attr('target', field_name);

    //         $(".img-container").removeClass('d-none');
    //         var reader = new FileReader();

    //         reader.onload = function(event){

    //             projImage3.croppie('bind', {
    //                 url:event.target.result
    //               }).then(function(){

    //             });
    //         }
    //         reader.readAsDataURL(this.files[0]);

    //         $("#img_cutting_proj_modal3").modal();
    //     }else{
    //         return flag;
    //     }
    //   });

    $('#crop_proj3').click(function (event) {

        var tr = $(this).attr('target');
        projImage3.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_proj_image3").val(result.image);
                    $("#previewprojsec3").attr('src', response);
                    $('#crossproj3').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    var videoImage1 = $('#vid-preview1').croppie({
        showZoomer: true,
        viewport: {
            width: 370,
            height: 310,
            type: 'square'
        },
        boundary: {
            width: 370,
            height: 310,
        }
    });

    var videoImage2 = $('#vid-preview2').croppie({
        showZoomer: true,
        viewport: {
            width: 370,
            height: 310,
            type: 'square'
        },
        boundary: {
            width: 370,
            height: 310,
        }
    });

    var videoImage3 = $('#vid-preview3').croppie({
        showZoomer: true,
        viewport: {
            width: 370,
            height: 310,
            type: 'square'
        },
        boundary: {
            width: 370,
            height: 310,
        }
    });

    var videoImage4 = $('#vid-preview4').croppie({
        showZoomer: true,
        viewport: {
            width: 370,
            height: 310,
            type: 'square'
        },
        boundary: {
            width: 370,
            height: 310,
        }
    });

    var videoImage5 = $('#vid-preview5').croppie({
        showZoomer: true,
        viewport: {
            width: 370,
            height: 310,
            type: 'square'
        },
        boundary: {
            width: 370,
            height: 310,
        }
    });

    var videoImage6 = $('#vid-preview6').croppie({
        showZoomer: true,
        viewport: {
            width: 370,
            height: 310,
            type: 'square'
        },
        boundary: {
            width: 370,
            height: 310,
        }
    });

    //     $('#videoThumb1').change(function( ){
    //     var flag = true;
    //     var imageType = /image.*/;
    //     var file = this.files[0];
    //     if (!file.type.match(imageType)) {
    //         alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
    //         flag = false;
    //         return false;
    //     }
    //     // check file size
    //     if (parseInt(file.size / 1024) > (1024*2)) {
    //         alert("File \""+file.name+"\" is too big.");
    //         flag = false;
    //         return false;
    //     }

    //     if(flag == true){
    //         var field_name = $(this).attr('target');
    //         $("#crop_vid1").attr('target', field_name);

    //         $(".img-container").removeClass('d-none');
    //         var reader = new FileReader();

    //         reader.onload = function(event){

    //             videoImage1.croppie('bind', {
    //                 url:event.target.result
    //               }).then(function(){

    //             });
    //         }
    //         reader.readAsDataURL(this.files[0]);

    //         $("#img_cutting_vid_modal1").modal();
    //     }else{
    //         return flag;
    //     }
    //   });

    $('#crop_vid1').click(function (event) {

        var tr = $(this).attr('target');
        videoImage1.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_vid_image1").val(result.image);
                    $("#previewvideosec1").attr('src', response);
                    $('#crossvid1').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    //     $('#videoThumb2').change(function( ){
    //     var flag = true;
    //     var imageType = /image.*/;
    //     var file = this.files[0];
    //     if (!file.type.match(imageType)) {
    //         alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
    //         flag = false;
    //         return false;
    //     }
    //     // check file size
    //     if (parseInt(file.size / 1024) > (1024*2)) {
    //         alert("File \""+file.name+"\" is too big.");
    //         flag = false;
    //         return false;
    //     }

    //     if(flag == true){
    //         var field_name = $(this).attr('target');
    //         $("#crop_vid2").attr('target', field_name);

    //         $(".img-container").removeClass('d-none');
    //         var reader = new FileReader();

    //         reader.onload = function(event){

    //             videoImage2.croppie('bind', {
    //                 url:event.target.result
    //               }).then(function(){

    //             });
    //         }
    //         reader.readAsDataURL(this.files[0]);

    //         $("#img_cutting_vid_modal2").modal();
    //     }else{
    //         return flag;
    //     }
    //   });

    $('#crop_vid2').click(function (event) {

        var tr = $(this).attr('target');
        videoImage2.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_vid_image2").val(result.image);
                    $("#previewvideosec2").attr('src', response);
                    $('#crossvid2').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    //     $('#videoThumb3').change(function( ){
    //     var flag = true;
    //     var imageType = /image.*/;
    //     var file = this.files[0];
    //     if (!file.type.match(imageType)) {
    //         alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
    //         flag = false;
    //         return false;
    //     }
    //     // check file size
    //     if (parseInt(file.size / 1024) > (1024*2)) {
    //         alert("File \""+file.name+"\" is too big.");
    //         flag = false;
    //         return false;
    //     }

    //     if(flag == true){
    //         var field_name = $(this).attr('target');
    //         $("#crop_vid3").attr('target', field_name);

    //         $(".img-container").removeClass('d-none');
    //         var reader = new FileReader();

    //         reader.onload = function(event){

    //             videoImage3.croppie('bind', {
    //                 url:event.target.result
    //               }).then(function(){

    //             });
    //         }
    //         reader.readAsDataURL(this.files[0]);

    //         $("#img_cutting_vid_modal3").modal();
    //     }else{
    //         return flag;
    //     }
    //   });

    $('#crop_vid3').click(function (event) {

        var tr = $(this).attr('target');
        videoImage3.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_vid_image3").val(result.image);
                    $("#previewvideosec3").attr('src', response);
                    $('#crossvid3').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    //   $('#videoThumb4').change(function( ){
    //     var flag = true;
    //     var imageType = /image.*/;
    //     var file = this.files[0];
    //     if (!file.type.match(imageType)) {
    //         alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
    //         flag = false;
    //         return false;
    //     }
    //     // check file size
    //     if (parseInt(file.size / 1024) > (1024*2)) {
    //         alert("File \""+file.name+"\" is too big.");
    //         flag = false;
    //         return false;
    //     }

    //     if(flag == true){
    //         var field_name = $(this).attr('target');
    //         $("#crop_vid4").attr('target', field_name);

    //         $(".img-container").removeClass('d-none');
    //         var reader = new FileReader();

    //         reader.onload = function(event){

    //             videoImage4.croppie('bind', {
    //                 url:event.target.result
    //               }).then(function(){

    //             });
    //         }
    //         reader.readAsDataURL(this.files[0]);

    //         $("#img_cutting_vid_modal4").modal();
    //     }else{
    //         return flag;
    //     }
    //   });

    $('#crop_vid4').click(function (event) {

        var tr = $(this).attr('target');
        videoImage4.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_vid_image4").val(result.image);
                    $("#previewvideosec4").attr('src', response);
                    $('#crossvid4').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    //   $('#videoThumb5').change(function( ){
    //     var flag = true;
    //     var imageType = /image.*/;
    //     var file = this.files[0];
    //     if (!file.type.match(imageType)) {
    //         alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
    //         flag = false;
    //         return false;
    //     }
    //     // check file size
    //     if (parseInt(file.size / 1024) > (1024*2)) {
    //         alert("File \""+file.name+"\" is too big.");
    //         flag = false;
    //         return false;
    //     }

    //     if(flag == true){
    //         var field_name = $(this).attr('target');
    //         $("#crop_vid5").attr('target', field_name);

    //         $(".img-container").removeClass('d-none');
    //         var reader = new FileReader();

    //         reader.onload = function(event){

    //             videoImage5.croppie('bind', {
    //                 url:event.target.result
    //               }).then(function(){

    //             });
    //         }
    //         reader.readAsDataURL(this.files[0]);

    //         $("#img_cutting_vid_modal5").modal();
    //     }else{
    //         return flag;
    //     }
    //   });

    $('#crop_vid5').click(function (event) {

        var tr = $(this).attr('target');
        videoImage5.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_vid_image5").val(result.image);
                    $("#previewvideosec5").attr('src', response);
                    $('#crossvid5').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    //   $('#videoThumb6').change(function( ){
    //     var flag = true;
    //     var imageType = /image.*/;
    //     var file = this.files[0];
    //     if (!file.type.match(imageType)) {
    //         alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
    //         flag = false;
    //         return false;
    //     }
    //     // check file size
    //     if (parseInt(file.size / 1024) > (1024*2)) {
    //         alert("File \""+file.name+"\" is too big.");
    //         flag = false;
    //         return false;
    //     }

    //     if(flag == true){
    //         var field_name = $(this).attr('target');
    //         $("#crop_vid6").attr('target', field_name);

    //         $(".img-container").removeClass('d-none');
    //         var reader = new FileReader();

    //         reader.onload = function(event){

    //             videoImage6.croppie('bind', {
    //                 url:event.target.result
    //               }).then(function(){

    //             });
    //         }
    //         reader.readAsDataURL(this.files[0]);

    //         $("#img_cutting_vid_modal6").modal();
    //     }else{
    //         return flag;
    //     }
    //   });

    $('#crop_vid6').click(function (event) {

        var tr = $(this).attr('target');
        videoImage6.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_vid_image6").val(result.image);
                    $("#previewvideosec6").attr('src', response);
                    $('#crossvid6').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    var teamImage1 = $('#team-preview1').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    var teamImage2 = $('#team-preview2').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    var teamImage3 = $('#team-preview3').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    var teamImage4 = $('#team-preview4').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    var teamImage5 = $('#team-preview5').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    var teamImage6 = $('#team-preview6').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    var teamImage7 = $('#team-preview7').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    var teamImage8 = $('#team-preview8').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    var teamImage9 = $('#team-preview9').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    var teamImage10 = $('#team-preview10').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    var teamImage11 = $('#team-preview11').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    var teamImage12 = $('#team-preview12').croppie({
        showZoomer: true,
        viewport: {
            width: 400,
            height: 400,
            type: 'square'
        },
        boundary: {
            width: 400,
            height: 400,
        }
    });

    $('#team1').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_team1").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                teamImage1.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_team_modal1").modal();
        } else {
            return flag;
        }
    });

    $('#crop_team1').click(function (event) {

        var tr = $(this).attr('target');
        teamImage1.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_team_image1").val(result.image);
                    $("#previewteamsec1").attr('src', response);
                    $('#crossteam1').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#team2').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_team2").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                teamImage2.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_team_modal2").modal();
        } else {
            return flag;
        }
    });

    $('#crop_team2').click(function (event) {

        var tr = $(this).attr('target');
        teamImage2.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_team_image2").val(result.image);
                    $("#previewteamsec2").attr('src', response);
                    $('#crossteam2').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#team3').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_team3").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                teamImage3.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_team_modal3").modal();
        } else {
            return flag;
        }
    });

    $('#crop_team3').click(function (event) {

        var tr = $(this).attr('target');
        teamImage3.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_team_image3").val(result.image);
                    $("#previewteamsec3").attr('src', response);
                    $('#crossteam3').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#team4').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_team4").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                teamImage4.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_team_modal4").modal();
        } else {
            return flag;
        }
    });

    $('#crop_team4').click(function (event) {

        var tr = $(this).attr('target');
        teamImage4.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_team_image4").val(result.image);
                    $("#previewteamsec4").attr('src', response);
                    $('#crossteam4').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#team5').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_team5").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                teamImage5.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_team_modal5").modal();
        } else {
            return flag;
        }
    });

    $('#crop_team5').click(function (event) {

        var tr = $(this).attr('target');
        teamImage5.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_team_image5").val(result.image);
                    $("#previewteamsec5").attr('src', response);
                    $('#crossteam5').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#team6').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_team6").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                teamImage6.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_team_modal6").modal();
        } else {
            return flag;
        }
    });

    $('#crop_team6').click(function (event) {

        var tr = $(this).attr('target');
        teamImage6.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_team_image6").val(result.image);
                    $("#previewteamsec6").attr('src', response);
                    $('#crossteam6').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#team7').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_team7").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                teamImage7.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_team_modal7").modal();
        } else {
            return flag;
        }
    });

    $('#crop_team7').click(function (event) {

        var tr = $(this).attr('target');
        teamImage7.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_team_image7").val(result.image);
                    $("#previewteamsec7").attr('src', response);
                    $('#crossteam7').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#team8').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_team8").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                teamImage8.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_team_modal8").modal();
        } else {
            return flag;
        }
    });

    $('#crop_team8').click(function (event) {

        var tr = $(this).attr('target');
        teamImage8.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_team_image8").val(result.image);
                    $("#previewteamsec8").attr('src', response);
                    $('#crossteam8').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#team9').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_team9").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                teamImage9.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_team_modal9").modal();
        } else {
            return flag;
        }
    });

    $('#crop_team9').click(function (event) {

        var tr = $(this).attr('target');
        teamImage9.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_team_image9").val(result.image);
                    $("#previewteamsec9").attr('src', response);
                    $('#crossteam9').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#team10').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_team10").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                teamImage10.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_team_modal10").modal();
        } else {
            return flag;
        }
    });

    $('#crop_team10').click(function (event) {

        var tr = $(this).attr('target');
        teamImage10.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_team_image10").val(result.image);
                    $("#previewteamsec10").attr('src', response);
                    $('#crossteam10').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#team11').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_team11").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                teamImage11.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_team_modal11").modal();
        } else {
            return flag;
        }
    });

    $('#crop_team11').click(function (event) {

        var tr = $(this).attr('target');
        teamImage11.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_team_image11").val(result.image);
                    $("#previewteamsec11").attr('src', response);
                    $('#crossteam11').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#team12').change(function () {
        var flag = true;
        var imageType = /image.*/;
        var file = this.files[0];
        if (!file.type.match(imageType)) {
            alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;
        }
        // check file size
        if (parseInt(file.size / 1024) > (1024 * 2)) {
            alert("File \"" + file.name + "\" is too big.");
            flag = false;
            return false;
        }

        if (flag == true) {
            var field_name = $(this).attr('target');
            $("#crop_team12").attr('target', field_name);

            $(".img-container").removeClass('d-none');
            var reader = new FileReader();

            reader.onload = function (event) {

                teamImage12.croppie('bind', {
                    url: event.target.result
                }).then(function () {

                });
            }
            reader.readAsDataURL(this.files[0]);

            $("#img_cutting_team_modal12").modal();
        } else {
            return flag;
        }
    });

    $('#crop_team12').click(function (event) {

        var tr = $(this).attr('target');
        teamImage12.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var formdata = new FormData();
            formdata.append("image", response);
            let xhr = new XMLHttpRequest();
            xhr.open("POST", "upload/temp_base64_image");
            xhr.onload = function () {
                var jsonResponse = xhr.response;

                let result = JSON.parse(jsonResponse);
                if (result.status == true) {
                    $("#uploaded_team_image12").val(result.image);
                    $("#previewteamsec12").attr('src', response);
                    $('#crossteam12').hide();
                } else {
                    alert('File is not uploading. Please try again.')
                }
            };
            var csrfToken = $('form').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken);
            xhr.send(formdata);
        });
    });

    $('#upload_image').change(function () {

        var field_name = $(this).attr('target');
        $("#crop_btn").attr('target', field_name);

        $(".img-container").removeClass('d-none');
        var reader = new FileReader();

        reader.onload = function (event) {
            image_crop.croppie('bind', {
                url: event.target.result
            }).then(function () {

            });
        }
        reader.readAsDataURL(this.files[0]);

        $("#img_cutting_modal").modal();

    });

    $('#crop_btn').click(function (event) {
        // console.log('test this button ')

        var tr = $(this).attr('target');

        image_crop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var _token = $('input[name=_token]').val();

            $("input[name=" + tr + "]").val(response);
            $("#profile_photo_preview img").attr('src', response);
        });
    });

    $('#upload_image2').change(function () {
        var field_name = $(this).attr('target');
        $("#crop_btn").attr('target', field_name);

        $(".img-container").removeClass('d-none');
        var reader = new FileReader();

        reader.onload = function (event) {
            image_crop.croppie('bind', {
                url: event.target.result
            }).then(function () {

            });
        }
        reader.readAsDataURL(this.files[0]);

        $("#img_cutting_modal").modal();

    });

    $('#upload_image3').change(function () {
        var field_name = $(this).attr('target');
        $("#crop_btn2").attr('target', field_name);

        $(".img-container").removeClass('d-none');
        var reader = new FileReader();

        reader.onload = function (event) {
            image_crop2.croppie('bind', {
                url: event.target.result
            }).then(function () {

            });
        }
        reader.readAsDataURL(this.files[0]);

        $("#img_cutting_modal2").modal();

    });

    $('#crop_btn1').click(function (event) {

        var tr = $(this).attr('target');

        image_crop1.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var _token = $('input[name=_token]').val();

            $("#video_thumb").val(response);
            $("#profile_photo_preview1 img").attr('src', response);
        });
    });

    $('#crop_btn2').click(function (event) {
        var tr = $(this).attr('target');


        image_crop2.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            var _token = $('input[name=_token]').val();

            $("#video_thumb").val(response);
            $("#cover_photo_preview img").attr('src', response);
        });
    });


    ///////////////////////////////
    // Summernote JS

    if ($("#summernote").length) {
        $('#summernote').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']]
            ],
            placeholder: 'Leave a comment ...',
            height: 250,
            callbacks: {
                onKeydown: function (e) {
                    var t = e.currentTarget.innerText;
                    if (t.trim().length >= 1500) {
                        //delete keys, arrow keys, copy, cut, select all
                        if (e.keyCode != 8 && !(e.keyCode >= 37 && e.keyCode <= 40) && e.keyCode != 46 && !(e.keyCode == 88 && e.ctrlKey) && !(e.keyCode == 67 && e.ctrlKey) && !(e.keyCode == 65 && e.ctrlKey))
                            e.preventDefault();
                    }
                },
                onKeyup: function (e) {
                    var t = e.currentTarget.innerText;
                    $('#maxContentPost').text(1500 - t.trim().length);
                },
                onPaste: function (e) {
                    var t = e.currentTarget.innerText;
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    var maxPaste = bufferText.length;
                    if (t.length + bufferText.length > 1500) {
                        maxPaste = 1500 - t.length;
                    }
                    if (maxPaste > 0) {
                        document.execCommand('insertText', false, bufferText.substring(0, maxPaste));
                    }
                    $('#maxContentPost').text(1500 - t.length);
                }
            }
        });
    }
    // End Summernote JS


    $("form#deleteVideo").submit(function (e) {

        if (confirm('Do you really want to delete the video?')) {
            return true;
        } else {
            e.preventDefault();
        }
    });


    /////////////////////////////////////////
    // Home page
    // Video block hover
    $(this).find(".plus").hide();
    $(this).find(".plus-details").hide();

    $(".videolist .v-img").hover(function () {

            $(this).find(".plus").show();
            $(".plus").hover(function () {
                    $(this).next(".plus-details").show();
                }, function () {

                }
            );

        }, function () {
            $(this).find(".plus").hide();
            $(this).find(".plus-details").hide();
        }
    );

    $(".notification_icon").on('click', function () {
        if (!$(this).hasClass('mark-as-read')) {
            $(this).addClass('mark-as-read');
            $.ajax({
                method: "POST",
                url: BASE.url("mark-notification-as-read"),
                data: {
                    _token: $("meta[name='secure_token']").attr('content')
                },
                success: function (data) {
                    $("#notifications_count").text(0);
                }
            });
        }

        $("#notification_show").toggle("100");
    });

    $(".filter_show").on('click', function () {
        $("#show_main1").toggle("100");
    });


    /////////////////////////////////
    // Header
    // Goto section
    $('[data-toggle="tooltip"]').tooltip();


    /////////////////////////////
    // Video Edit page
    // add color to checked checkboxes
    $('.edit-page input:checked').parent().css("color", "#ea2c5a");
    $('.edit-page input[type=checkbox]').on("click", function () {
        if ($(this).is(':checked')) {
            $(this).parent().css("color", "#ea2c5a");
        } else {
            $(this).parent().css("color", "black");
        }
    });


    var MYAPP = {
        initialize: function () {
            MYAPP.setUpListeners();
            MYAPP.ready();
        },

        setUpListeners: function () {
            var $document = $(document),
                $window = $(window),
                $tabs = $('.custom-tabs'),
                $activeTab = $tabs.find('.tabs-panel > .active'),
                vCategor_Right = $('.v-categories.side-menu .content-block .cb-content > .row > div:last-child'),
                vCategor_Left = $('.v-categories.side-menu .content-block .cb-content > .row > div:first-child'),
                $search_btn = $('.topsearch').find('.input-group-addon'),
                $search_btn_close = $('.topsearch-close'),
                $btn_menu = $('.btn-menu-toggle'),
                $btn_menu_close = $('.mobile-menu-close'),
                $menu = $('.mobile-menu'),
                $categories_mob = $('.mobile-menu-list'),
                $single_footer_switch = $('.single-v-footer-switch'),
                $channels_search = $('.channels-search'),
                $btn_transform = $('.search-group-transform'),
                $range_slider = $('.duration-range > input');

            var scroll_w = (function () {
                var scrollWidth,
                    $div = $('<div>').css({
                        overflowY: 'scroll',
                        width: '50px',
                        height: '50px',
                        visibility: 'hidden'
                    });

                $('body').append($div);
                scrollWidth = $div.get(0).offsetWidth - $div.get(0).clientWidth;
                $div.remove();

                return scrollWidth;
            })();

            $tabs.on('click', '.tabs-panel > a', function (e) {
                MYAPP.singlVideo.changeTab.call(this);
                e.preventDefault();
                e.stopPropagation();
                return false;
            });
            $(document).on('click', '.tab-popup-close', function (e) {
                $('.mfp-bg').trigger('click');

                e.preventDefault();
                return false;
            });
            $activeTab.trigger('click');

            //Clipboard
            //if($('.btn-copy').length)
            // new Clipboard('.btn-copy');

            $('.btn-color-toggle, .mobile-menu-btn-color').on('click', function () {
                if ($('body').hasClass('light')) {
                    $('body').removeClass('light').addClass('dark');
                    $(this).find('img').attr('src', 'images/icon_bulb_dark.png');
                } else {
                    if ($('body').hasClass('dark'))
                        $('body').removeClass('dark').addClass('light');
                    $(this).find('img').attr('src', 'images/icon_bulb_light.png');
                }
            });

            function align_categor_col() {
                vCategor_Left.removeAttr('style');
                if (vCategor_Left.innerHeight() <= vCategor_Right.innerHeight()) {
                    vCategor_Left.css({'height': vCategor_Right.innerHeight()});
                }
            };

            function add_categor_bg() {
                var bg_add = $('.v-categories.side-menu .bg-add'),
                    vCategor_Left_posL = vCategor_Left.get(0).getBoundingClientRect().left;

                bg_add.css({
                    height: vCategor_Left.innerHeight(),
                    width: vCategor_Left_posL + 'px',
                    left: '-' + vCategor_Left_posL + 'px'
                });
            };

            if ($('.v-categories.side-menu').length) {
                align_categor_col();
                add_categor_bg();
            }

            $(window).resize(function () {
                if ($('.v-categories.side-menu').length) {
                    align_categor_col();
                    add_categor_bg();
                }
            });

            $('.u-form input[type="checkbox"]').on('change', function () {
                var $this = $(this),
                    checkboxDiv = $(this).parents('div.checkbox');

                if ($this.next().css('display') == 'block') {
                    checkboxDiv.addClass('checked');
                } else {
                    checkboxDiv.removeClass('checked');
                }

            });

            $search_btn.on('click', function () {
                var wind_w = window.innerWidth;

                if (wind_w < 768) {
                    $('.navbar-container').addClass('search-open');
                }
            });

            $search_btn_close.on('click', function () {
                $('.navbar-container').removeClass('search-open');
            });

            $btn_menu.on('click', function (e) {
                var wind_w = window.innerWidth;

                if (wind_w < 768) {
                    if (!$menu.hasClass('open')) {
                        $('body').addClass('ovf--hidden').css({paddingRight: scroll_w});

                        $menu.addClass('open');
                    } else {
                        $('body').removeClass('ovf--hidden').removeAttr('style');

                        $menu.removeClass('open');
                    }
                }

                e.preventDefault();
                return false;
            });

            $btn_menu_close.on('click', function (e) {
                $('body').removeClass('ovf--hidden').removeAttr('style');

                $menu.removeClass('open');

                e.preventDefault();
                return false;
            });

            $categories_mob.find('li a').on('click', function (e) {
                var wind_w = window.innerWidth;

                if (wind_w < 768) {
                    var $this = $(this),
                        $ul = $this.parent().find('> ul');

                    if ($ul.length) {
                        $ul.slideToggle();

                        e.stopPropagation();
                        e.preventDefault();
                        return false;
                    }
                }
            });

            $single_footer_switch.find('> a').on('click', function (e) {
                var wind_w = window.innerWidth;

                if (wind_w < 768) {
                    var $this = $(this),
                        $btn_elem_toggle = $($this.attr('data-toggle')),
                        $btns = $this.parent().find('> a');

                    $btns.each(function () {
                        var $elem_toggle = $($(this).removeClass('active').attr('data-toggle'));

                        $elem_toggle.hide();
                    });

                    $this.addClass('active');
                    $btn_elem_toggle.show();
                }

                e.preventDefault();
                return false;
            });

            $(function () {
                $single_footer_switch.find('a.active').trigger('click');
            });

            $channels_search.find('i').on('click', function () {
                var $this = $(this),
                    $head = $this.parents('.cb-header');

                $head.toggleClass('channels-search-open');
            });

            $btn_transform.find('.s-s-title').on('click', function () {
                var wind_w = window.innerWidth;

                if (wind_w < 768) {
                    $(this).parent().toggleClass('open');
                }
            });

            if ($range_slider.length) {
                $range_slider.ionRangeSlider({
                    type: "double",
                    min: 0,
                    max: 40,
                    from: 0,
                    to: 20,
                    step: 10,
                    hide_min_max: true,
                    hide_from_to: true,
                    grid: true
                });
            }

            $(window).on('load resize', function () {
                $menu.removeClass('open');
                $('body').removeClass('ovf--hidden').removeAttr('style');
            });
        },

        singlVideo: {
            changeTab: function () {
                var wind_w = window.innerWidth,
                    $this = $(this),
                    $dataTab = $this.attr('data-tab');

                if (wind_w > 767 || $this.attr('data-tab') === 'tab-1') {
                    var $tabs = $this.parents('.custom-tabs').find('.tabs-content').children(),
                        i = 0;

                    for (; i < $tabs.length; i++) {
                        if ($tabs.eq(i).hasClass($dataTab)) {
                            $tabs.removeClass('active').eq(i).addClass('active');
                            $this.parent().children().removeClass('active');
                            $this.addClass('active');
                            return;
                        }
                    }
                } else {
                    var selector = '.' + $dataTab + ' .tab-popup';

                    $.magnificPopup.open({
                        mainClass: 'mfp-with-zoom',
                        removalDelay: 300,
                        closeMarkup: '<button title="%title%" type="button" class="mfp-close icon-Cancel"></button>',
                        items: [
                            {
                                src: '.' + $dataTab + ' .tab-popup',
                                type: 'inline',
                            }
                        ]
                    });
                }
            }
        },

        ready: function () {
            if ($('video').length) {
                /*$('video').mediaelementplayer({
                        alwaysShowControls: false,
                        videoVolume: 'horizontal',
                        features: ['playpause','progress','current','duration','tracks','volume','fullscreen'],
                        enableKeyboard: true,
                        pauseOtherPlayers: true,
                        enableAutosize: true
                });*/
            }
        }
    };

    MYAPP.initialize();

    $('#likevideo').click(function (e) {
        e.preventDefault();
        var video_key = $(this).attr('href');
        $.ajax({
            type: 'get',
            url: 'liked-video/' + video_key,
            success: function (data) {
                $(".cvicon-cv-liked").html(data);
            }
        });
    });

    $('#watchlatervideo').click(function (e) {
        e.preventDefault();
        var video_key = $(this).attr('href');
        $.ajax({
            type: 'get',
            url: 'watch-later/' + video_key,
            success: function (data) {
                $(".cvicon-cv-watch-later").html(data);
            }
        });
    });


});