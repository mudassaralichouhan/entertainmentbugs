
$(document).ready(function(){	
	var dropbox;  
	  
	dropbox = document.getElementById("dropbox");  
	dropbox.addEventListener("dragenter", dragenter, false);  
	dropbox.addEventListener("dragleave", dragleave, false);  
	dropbox.addEventListener("dragover", dragover, false);  
	dropbox.addEventListener("drop", drop, false);  
	
	function defaults(e){
       e.stopPropagation();  
       e.preventDefault();  
	}
    function dragenter(e) {  
	   $(this).addClass("active");
	   defaults(e);
	}  
      
    function dragover(e) { 
	   defaults(e);
    }  
    function dragleave(e) {  
	   $(this).removeClass("active");
	   defaults(e);
    }  

    function drop(e) {  
	   $(this).removeClass("active");
	   defaults(e);      
	   // dataTransfer -> which holds information about the user interaction, including what files (if any) the user dropped on the element to which the event is bound.
	   //console.log(e);
       var dt = e.dataTransfer;  
       var files = dt.files; 
	   if(validateFile(files)){
		  uploadVideoFile.files = files;  
	   }
      // handleFiles(files,e);  
    } 
	
	/*Validate Video File*/
	validateFile = function(files,e){
		var flag = true;
		var imageType = /video.*/;  
		var file = files[0];
		// check file type
		if (!file.type.match(imageType)) {  
		  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
		  flag = false;
		  return false;	
		} 
		// check file size
		if (parseInt(file.size / 1024) > (1024*50)) {  
		  alert("File \""+file.name+"\" is too big. I am using shared server :P");
		  flag = false;
		  return false;	
		} 
		return flag;
	}
   
   /*Handle Video File*/
	handleFiles = function (files, durl="", input = "", output = "", preLoadedThumb = false){
		
		var imageType = /video.*/;  
		var file = files[0];
		// check file type
		if (!file.type.match(imageType)) {  
		  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
		  return false;	
		} 
		// check file size
		if (parseInt(file.size / 1024) > (1024*50)) {  
		  alert("File \""+file.name+"\" is too big");
		  return false;	
		} 
		
		var fileName = file.name;
		fileName.split('.').slice(0, -1).join('.');
		$('.u-title').html(fileName);
		
		var info = '<div class="u-size">'+(parseInt(parseInt(file.size / 1024))/1024).toFixed(2)+' MB / <span class="up-done"></span> MB</div><div class="u-progress"><div class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span class="sr-only">0% Complete</span></div></div><div class="u-close"><a href="#"><i class="cvicon-cv-cancel"></i></a></div></div>';
				
		$("#upload-progress").show("fast",function(){
			$("#upload-progress").html(info); 
			uploadVideoFile(file, durl, input, output, preLoadedThumb);
		});
		
  }
  /*Upload Video File*/
  uploadVideoFile = async function(file, durl="", input = "", output = "", preLoadedThumb = false){
	// check if browser supports file reader object 
	if (typeof FileReader !== "undefined"){
	//alert("uploading "+file.name);  
	reader = new FileReader();
	reader.onload = function(e){
		//alert(e.target.result);
		//$('.preview img').attr('src',e.target.result).css("width","70px").css("height","70px");
	}
	reader.readAsDataURL(file);
	let size = null;
	var formdata = new FormData();
	formdata.append("video", file);
	if( preLoadedThumb ){
		const fileContent = URL.createObjectURL(file);
		const video = await EBUGS.utils.getThumbnailForVideo(fileContent);
		formdata.append("thumb", video.thumb);
		size = video.duration;
	}
	
	xhr = new XMLHttpRequest();
	xhr.open("POST", "upload/video");
	
	xhr.upload.addEventListener("progress", function (event) {
		//console.log(event);
		if (event.lengthComputable) {
			$(".progress-bar").css("width",(event.loaded / event.total) * 100 + "%");
			$(".progress-bar").css("aria-valuenow",(event.loaded / event.total) * 100);
			$(".sr-only").html(" "+((event.loaded / event.total) * 100).toFixed() + "%");
			$(".up-done").html((parseInt(parseInt(event.loaded / 1024))/1024).toFixed(2));
			$('.percent-process').html('<strong>'+parseInt((event.loaded / event.total) * 100).toFixed(0)+'%</strong>');
			// if(durl){
			// 	$('.cvd-thumb').html('<img src="'+parseInt((event.loaded / event.total) * 100).toFixed(0)+'">');
			// }
		}
		else {
			alert("Failed to compute file upload length");
		}
	}, false);

	xhr.onreadystatechange = function (oEvent) {  
	  if (xhr.readyState === 4) {  
		if (xhr.status === 200) {  
		  $(".progress-bar").css("width","100%");
		  $(".progress-bar").attr("aria-valuenow","100");
		  $(".sr-only").html("100%");
		  $(".up-done").html((parseInt(parseInt(file.size / 1024))/1024).toFixed(2));
		  $('.u-desc').html("<strong>100% Completed.</strong> Your video has been successfully upload.");
		  $('.u-desc').addClass('success');

		  // $('.cvu-thumb').html('<img src="'++'">');
		} else {  
		  alert("Error"+ xhr.statusText);  
		}  
	  }  
	}; 
	
	xhr.onload  = function() {
	   var jsonResponse = xhr.response;
	   result = JSON.parse(jsonResponse);
	   if(result.status == true){
		   $('#selected-video-name').val(result.file);
			if( input != "" ){
				$(input).val(result.thumbnail);

				if( $(input).next().data("duration") ){
					$(input).next().val(size);
				}
			}

			if( output != "" ){
				$(output).attr('src', result.thumbnail);
			}
	   }else{
		   alert('File is not uploading. Please try again.')
	   }	  
	};

	
	var csrfToken = $('#video-upload').find('input[name=_token]').val();
	xhr.setRequestHeader('X-csrf-token', csrfToken); 	
	
	xhr.send(formdata);

	}else{
		alert("Your browser doesnt support FileReader object");
	} 		
  }
  
  /*Validate Thumb Image File*/
  validateThumbFile = function(files,e){
		var flag = true;
		var imageType = /image.*/;  
		var file = files[0];
		// check file type
		if (!file.type.match(imageType)) {  
		  alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
		  flag = false;
		  return false;	
		} 
		// check file size
		if (parseInt(file.size / 1024) > (1024*2)) {  
		  alert("File \""+file.name+"\" is too big.");
		  flag = false;
		  return false;	
		} 
		
		if(flag == true){
			uploadImageFile(file);
		}else{
			return flag;
		}	
		
	};
	
	/*Upload Image in Temp Folder*/
	uploadImageFile = function(file){
	var formdata = new FormData();
	formdata.append("image", file);	
	console.log(file);
	xhr = new XMLHttpRequest();
	xhr.open("POST", "upload/temp_image");
	xhr.onload  = function() {
	   var jsonResponse = xhr.response;
	   result = JSON.parse(jsonResponse);
	   if(result.status == true){
		   var fl = $('#thumbnail_image').find('input[type=hidden]').length;
		   fl++;
		   var fNameHtml = '<input name="thumb['+fl+']" type="hidden" id="video-thumb-'+fl+'" value="'+result.file+'" />';
		   var imageHtml = '<a href="javascript:void(0)" key="'+fl+'" class="video-thumb-img"><img src="'+result.image+'" /><span class="delete-video-thumb"><i class="cvicon-cv-cancel"></i><span></a>';
		   $('#video-thumb-image').after(fNameHtml);
		   $('.upload-thumb-image').after(imageHtml);
	   }else{
		   alert('File is not uploading. Please try again.')
	   }	  
	};	
	var csrfToken = $('#video-upload').find('input[name=_token]').val();
	xhr.setRequestHeader('X-csrf-token', csrfToken); 	
	xhr.send(formdata);		
  }
  
  /*Delete Video Thumb Image*/
  $(document).on('click', '.delete-video-thumb', function(){
	  var key = $(this).parent('a:first').attr('key');
	  
	  /*Ajax Request to remove file in temp folder*/
	  $(this).parent('a:first').remove();
	  $('#video-thumb-'+key).remove();
	  	  
  });
	
	
});