<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Rewards extends BaseController
{
    private static $prizes = array(
        1 => 0,
        2 => 0,
        3 => 0,
        4 => 0,
        5 => 0,
        6 => 0,
        7 => 0,
        8 => 0,
        9 => 0,
        10 => 0,
        '11-50' => 0,
        '51-100' => 0,
        '101-250' => 0,
        '251-500' => 0,
    );

    public function __construct()
    {
        parent::__construct();
        $this->load->model('RewardModel', 'RewardModel');

        $this->isLoggedIn();
        //  $this->module = 'Artist';
    }

    public function index()
    {
        $this->load->database(); // Load the database if not already loaded

        $videos = $this->db
            ->select('users.name, videos.*, video_thumbs.thumb')
            ->from('videos')
            ->join('users', 'users.id = videos.user_id', 'left') // Assuming there's a foreign key relationship on user_id
            ->join('video_thumbs', 'video_thumbs.video_id = videos.id', 'left') // Assuming there's a foreign key relationship on video_id
            ->where('videos.isRewardVideo', 1)
            ->get()
            ->result();

        $data = [
            'videos' => $videos,
        ];

        $this->global['pageTitle'] = 'All Rewards Video Listing';

        $this->loadViews("rewards/index", $this->global, $data, null);
    }

    public function createPrices()
    {
        // Check if the user is an admin
        if (!$this->isAdmin()) {
            return $this->loadThis();
        }

        // Query the database to get records between previous and coming Sunday
        $reward_prizes = $this->RewardModel->getCurrentWeekPrice()->row();

        // If no prizes are found, use a default empty array
        $prizes = json_decode($reward_prizes->prizes ?? json_encode(self::$prizes));

        // Load the view with data
        $data = [
            'pageTitle' => 'Reward - Set Prize',
            'prizes' => $prizes,
            'id' => $reward_prizes->id ?? 0,
        ];

        $this->load->helper('form');
        $this->loadViews('rewards/set-prize', $data, null, null, true);
    }

    public function storePrices()
    {
        // Check if the user is an admin
        if (!$this->isAdmin()) {
            return $this->loadThis();
        }

        if (is_array($_POST['prizes'] ?? null) && is_numeric($_POST['id'])) {
            $prizes_data = json_encode($_POST['prizes']);

            $reward_prizes = $this->RewardModel->getCurrentWeekPrice();

            if ($reward_prizes->num_rows() > 0) {
                $this->db
                    ->where('id', $_POST['id'])
                    ->update('reward_prizes', ['prizes' => $prizes_data]);

                setFlashData('success', 'Prices are updated successfully of this week');
            } else {
                $this->db->insert('reward_prizes', ['prizes' => $prizes_data]);

                setFlashData('success', 'Prices are Created successfully for this week');
            }
        }

        redirect('/rewards/set-prize');
    }

    public function Todayrewards()
    {

        if (!$this->isAdmin()) {
            $this->loadThis();
        } else {
            $searchText = '';
            if (!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;

            $this->load->library('pagination');

            $count = $this->RewardModel->videoListingCount($searchText);

            $returns = $this->paginationCompress("video/videoListing/", $count, 10);

            $data['videoRecords'] = $this->RewardModel->videoListing($searchText, $returns["page"], $returns["segment"]);
            $data['rewardOfCurrentWeek'] = $this->RewardModel->Todayrewards();

            $this->global['pageTitle'] = 'CodeInsect : Video Listing';

            $this->loadViews("rewards/current-week", $this->global, $data, NULL);

        }
    }

    public function currentWeek()
    {
        if (!$this->isAdmin()) {
            $this->loadThis();
            exit;
        }

        $searchText = $this->input->post('searchText');
        $searchText = !empty($searchText) ? $this->security->xss_clean($searchText) : '';

        $this->load->library('pagination');
        //$count = $this->RewardModel->videoListingCount($searchText);
        //$returns = $this->paginationCompress("rewards/current-video-listing/", $count, 5);

        $previousSunday = date('Y-m-d 00:00:00', strtotime('last Sunday'));
        $nextSunday = date('Y-m-d 23:59:59', strtotime('next Sunday'));
        $videos = $this->db
            ->select('users.name, videos.*, video_thumbs.thumb')
            ->from('videos')
            ->join('users', 'users.id = videos.user_id', 'left')
            ->join('video_thumbs', 'video_thumbs.id = videos.video_thumb_id', 'left')
            ->where('videos.isRewardVideo', 1)
            ->where("videos.created_at >= '$previousSunday' AND videos.created_at <= '$nextSunday'")
            ->group_by('videos.id')
            ->get()
            ->result();

        $data = [
            'searchText' => $searchText,
            'videos' => $videos,
        ];

        $this->global['pageTitle'] = 'This Week';

        $this->loadViews("rewards/current-week", $this->global, $data, null);
    }


    function previous()
    {
        if (!$this->isAdmin()) {
            $this->loadThis();
            exit;
        }

        $searchText = $this->input->post('searchText');
        $searchText = !empty($searchText) ? $this->security->xss_clean($searchText) : '';

        $this->load->library('pagination');
        //$count = $this->RewardModel->videoListingCount($searchText);
        //$returns = $this->paginationCompress("rewards/current-video-listing/", $count, 5);

        $previousSunday = date('Y-m-d 23:59:59', $previousSundayTimestamp = strtotime('last Sunday'));
        $previousTwoSunday = date('Y-m-d 00:00:00', strtotime('-7 days', $previousSundayTimestamp));

        $videos = $this->db
            ->select('users.name, videos.*, video_thumbs.thumb')
            ->from('videos')
            ->join('users', 'users.id = videos.user_id', 'left')
            ->join('video_thumbs', 'video_thumbs.id = videos.video_thumb_id', 'left')
            ->where('videos.isRewardVideo', 1)
            ->where("videos.created_at >= '$previousTwoSunday' AND videos.created_at <= '$previousSunday'")
            ->group_by('videos.id')
            ->get()
            ->result();

        $data = [
            'searchText' => $searchText,
            'videos' => $videos,
        ];

        $this->global['pageTitle'] = 'This Week';

        $this->loadViews("rewards/current-week", $this->global, $data, null);
    }

    public function deleteAll()
    {
        if ($this->input->post('checkbox_value')) {
            $id = $this->input->post('checkbox_value');
            for ($count = 0; $count < count($id); $count++) {
                $this->RewardModel->delete($id[$count]);
            }
        }
    }

    function deleteVideo()
    {
        if (!$this->isAdmin()) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $id = $this->input->post('id');
            // $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $result = $this->RewardModel->deleteVideo($id);
            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }

    function holdVideo()
    {
        if (!$this->isAdmin()) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $userId = $this->input->post('userId');
            $result = $this->RewardModel->holdUnholdVideo($userId, ['isDeleted' => 1]);
            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }

    function unholdVideo()
    {
        if (!$this->isAdmin()) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $userId = $this->input->post('userId');
            $result = $this->RewardModel->holdUnholdVideo($userId, ['isDeleted' => 1]);
            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }

    function filterVideorewardvideo()
    {

        $query = $this->input->post('dist_id');
        if ($query == 0 || $query == 1) {
            $video = $this->RewardModel->getVideoDetails($query);
        } else {
            $video = $this->RewardModel->getVideoDetails1();
        }
        $searchText = '';
        $this->load->library('pagination');

        $count = $this->RewardModel->videoListingCount($searchText);


        $returns = $this->paginationCompress("rewards/current-video-listing/", $count, 5);
        $videoThumb = $this->RewardModel->videoThumb($searchText, $returns["page"], $returns["segment"], $query == 0 ? 2 : $query);
        //echo "<pre>"; print_r($video); exit();//
        if (!empty($video)) {//
            foreach ($video as $keys => $record) {
                ?>
                <tr style="background:<?php echo $bg; ?>">
                    <!-- <td><input type="checkbox" class="sub_chk" data-id="<?php //echo $record->vid;
                    ?>"></td> -->
                    <td><input type="checkbox" class="delete_checkbox" value="<?php echo $videoThumb[$keys]->vid; ?>"/>
                    </td>


                    <td style="font-weight:bold;">
                        <!-- <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php //echo $record->profile_photo;
                        ?>"><?php //echo $uname
                        ?> -->

                        <img src="https://entertainmentbugs.com/public/uploads/video/thumbs/<?php echo $videoThumb[$keys]->thumb; ?>"
                             width="60px"/>
                    </td>
                    <td><?php echo $record->name; ?></td>
                    <td><?php echo $record->title; ?></td>

                    <td><?php echo date("d-m-Y", strtotime($videoThumb[$keys]->vcreated_at)) ?></td>
                    <!--<td><?php echo $videoThumb[$keys]->vstatus ?></td> -->
                    <td style="position:relative">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
                           style="background:#none;padding-left:0px;">
                            Option <span class="caret"></span></a>
                        <ul class="dropdown-menu" style="left: -100px;">
                            <li><?php if ($record->visDeleted == 0){ ?>
                                <a class="holdVideo" href="javascript:void()"
                                   data-userid="<?php echo $videoThumb[$keys]->vid; ?>">Hold</a></li>
                            <?php } elseif ($record->visDeleted == 1) { ?>
                                <a class="unholdVideo" href="javascript:void()"
                                   data-userid="<?php echo $videoThumb[$keys]->vid; ?>">UnHold</a>
                                </li>
                            <?php } ?>
                            <li><a class="deleteVideo" href="<?php echo base_url(); ?>todaysVideoListing"
                                   data-id="<?php echo $videoThumb[$keys]->vid; ?>">Delete</a></li>
                        </ul>
                    </td>
                </tr>
                <?php
            }
        } else if (empty($artist)) { ?>
            <tr>
                <td><?php echo "Record not Fount...."; ?></td>
            </tr>
        <?php }
    }

    public function videoDetailsByDate()
    {
        $fromDate = $this->input->post('from_date');
        $toDate = $this->input->post('to_date');

        $video = $this->RewardModel->getVideoDetailsByDate($fromDate, $toDate);

        if (!empty($video)) {
            foreach ($video as $record) {
                ?>
                <tr>
                    <!-- <td><input type="checkbox" class="sub_chk" data-id="<?php //echo $record->vid;
                    ?>"></td> -->
                    <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record['vid']; ?>"/></td>
                    <td style="font-weight:bold;">
                        <!-- <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php //echo $record->profile_photo;
                        ?>"><?php //echo $uname
                        ?> -->

                        <video width="150" height="100" controls autoplay>
                            <source src="<?php echo $record['video']; ?>" type="video/mp4">
                            Sorry, your browser doesn't support the video element.
                        </video>
                    </td>
                    <td><?php echo $record['title'] ?></td>
                    <td><?php echo $record['name'] ?></td>
                    <td><?php echo date("d-m-Y", strtotime($record['vcreated_at'])) ?></td>
                    <td><?php echo $record['vstatus'] ?></td>
                    <td style="position:relative">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
                           style="background:#none;padding-left:0px;">
                            Option <span class="caret"></span></a>
                        <ul class="dropdown-menu" style="left: -100px;">
                            <li><?php if ($record['visDeleted'] == 0){ ?>
                                <a class="holdVideo" href="<?php echo base_url(); ?>videoListing"
                                   data-userid="<?php echo $record['vid']; ?>">Hold</a></li>
                            <?php } elseif ($record['visDeleted'] == 1) { ?>
                                <a class="unholdVideo" href="<?php echo base_url(); ?>videoListing"
                                   data-userid="<?php echo $record['vid']; ?>">UnHold</a>
                                </li>
                            <?php } ?>
                            <li><a class="deleteVideo" href="<?php echo base_url(); ?>videoListing"
                                   data-id="<?php echo $record['vid']; ?>">Delete</a></li>
                        </ul>
                    </td>
                </tr>

                <?php
            }
        } else if (empty($artist)) { ?>
            <tr>
                <td><?php echo "Record not Fount...."; ?></td>
            </tr>
        <?php }

    }
}