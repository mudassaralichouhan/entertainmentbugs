<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Plan extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Plan_model', 'rm');
        $this->isLoggedIn();
     //  $this->module = 'Artist';
    }

    /**
     * This is default routing method
     * It routes to default listing page
     */
    public function index()
    {
        redirect('plan/planListing');
    }
    
    function planListing()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->rm->artistListingCount($searchText);

            $returns = $this->paginationCompress ( "artist/artistListing/", $count, 10 );
            
            $data['artistRecords'] = $this->rm->artistListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'CodeInsect : Artist Listing';
            
            $this->loadViews("plan/list", $this->global, $data, NULL);
        }
    }

    function deleteArtist()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
          $id = $this->input->post('id');
         
           // $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
          
            $result = $this->rm->deleteArtist($id);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function holdArtist()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
         
            $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
          
            $result = $this->rm->holdUnholdArtist($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function unholdArtist()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
         
            $userInfo = array('isDeleted'=>0,'deleted_at'=>date('Y-m-d H:i:s'));
          
            $result = $this->rm->holdUnholdArtist($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function filterArtist() {

        $query=$this->input->post('dist_id');
        if($query == 0 || $query == 1){
        $artist = $this->rm->getArtistDetails('artist_about',$query);
       }else{
        $artist = $this->rm->getArtistDetails1('artist_about');
       }
        
        //echo "<pre>"; print_r($artist); exit();
        if(!empty($artist)){
          foreach($artist as $data){

            $uid = $data['user_id'];
            $sql = "select name, email, phone from `users` where id = $uid";
            $query = $this->db->query($sql);
            $ss = $query->result(); 
            foreach($ss as $row){
                $uname =  $row->name;
                $email =  $row->email;
                $phone =  $row->phone;
            }
        ?>
            <tr>
              <td style="font-weight:bold;">
                <!-- <img src="<?php //echo base_url(); ?>assets/login/images/demo/users/face24.jpg" class="img-circle img-xs" alt="" style="margin-right:10px"><?php //echo $data['profile_name']; ?>-->
                <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php echo $data['profile_photo'];?>"><?php echo $uname ?> 
              </td> 
              <td><?php echo $phone ?></td> 
              <td><?php echo $email ?></td>   
              <td><?php echo $data['multiple_category']; ?></td>
              <td><?php echo $data['language']; ?></td>
              <td><?php echo date("d-m-Y", strtotime($data['created_at'])) ?></td>
              <!-- <td>
                  <a href="<?php //echo site_url('edit-inward/'.$data['id']); ?>" class="btn btn-xs btn-success"><i class="far fa-edit">Edit</i></a>
              </td> -->

                <td style="position:relative">  
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                    Option <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="left: -100px;"> 
                        <li>
                        <?php if($data['isDeleted'] == 0){ ?>
                        <a class="holdArtist"  href="<?php echo base_url() ;?>artistListing" data-userid="<?php echo $data['id']; ?>">Hold</a></li>
                        <?php }elseif($data['isDeleted'] == 1){ ?>
                        <a class="unholdArtist" href="<?php echo base_url() ;?>artistListing" data-userid="<?php echo $data['id']; ?>">UnHoad</a>
                        </li>
                        <?php } ?>
                        <li><a class="deleteArtist"  href="<?php echo base_url() ;?>artistListing" data-id="<?php echo $data['id']; ?>">Delete</a></li></li> 
                    </ul> 
                </td>     
            </tr>
    <?php
          }
        }else if(empty($artist)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
    }

   
    public function artistDetailsByDate() {
        $fromDate=$this->input->post('from_date');
        $toDate=$this->input->post('to_date');

        $artist = $this->rm->getArtistDetailsByDate('artist_about',$fromDate,$toDate);

        if(!empty($artist)){
          foreach($artist as $data){
            $uid = $data['user_id'];
            $sql = "select name, email, phone from `users` where id = $uid";
            $query = $this->db->query($sql);
            $ss = $query->result(); 
            foreach($ss as $row){
                $uname =  $row->name;
                $email =  $row->email;
                $phone =  $row->phone;
            }
        ?>
            <tr>
              <td style="font-weight:bold;">
              <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php echo $data['profile_photo'];?>"><?php echo $uname ?> 
              </td> 
              <td><?php echo $phone ?></td> 
              <td><?php echo $email ?></td>  
              <td><?php echo $data['multiple_category']; ?></td>
              <td><?php echo $data['language']; ?></td>
              <td><?php echo date("d-m-Y", strtotime($data['created_at'])) ?></td>
              <!-- <td>
                  <a href="<?php //echo site_url('edit-inward/'.$data['id']); ?>" class="btn btn-xs btn-success"><i class="far fa-edit">Edit</i></a>
              </td> -->

                <td style="position:relative">  
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                    Option <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="left: -100px;"> 
                        <li>
                        <?php if($data['isDeleted'] == 0){ ?>
                        <a class="holdArtist"  href="<?php echo base_url() ;?>artistListing" data-userid="<?php echo $data['id']; ?>">Hold</a></li>
                        <?php }elseif($data['isDeleted'] == 1){ ?>
                        <a class="unholdArtist" href="<?php echo base_url() ;?>artistListing" data-userid="<?php echo $data['id']; ?>">UnHoad</a>
                        </li>
                        <?php } ?>
                        <li><a class="deleteArtist"  href="<?php echo base_url() ;?>artistListing" data-id="<?php echo $data['id']; ?>">Delete</a></li></li> 
                    </ul> 
                </td>     
            </tr>
    <?php
          }
        }else if(empty($artist)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
                    
    }


}

?>