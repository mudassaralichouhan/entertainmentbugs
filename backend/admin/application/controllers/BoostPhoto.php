<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class BoostPhoto extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('BoostPhoto_model', 'sm');
        $this->isLoggedIn();
    }

    public function index()
    {
        redirect('boostphoto/boostPhotoListing');
    }
    
    function boostPhotoListing()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            $count = $this->sm->boostPhotoListingCount($searchText);
            $returns = $this->paginationCompress ( "boostphoto/boostPhotoListing/", $count, 10 );
            $data['boostPhotoRecords'] = $this->sm->boostPhotoListing($searchText, $returns["page"], $returns["segment"]);
      
            $this->global['pageTitle'] = 'CodeInsect : Boost Photo Listing';
            $this->loadViews("boostphoto/list", $this->global, $data, NULL);
        }
    }


    function holdBoostPhoto()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId'); 
            $userInfo = array('isDeleted'=>1);
            $result = $this->sm->holdUnholdBoostPhoto($userId,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

    function unholdBoostPhoto()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted'=>0);
            $result = $this->sm->holdUnholdBoostPhoto($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    
    
    function filterBoostPhoto() {
            $query=$this->input->post('dist_id');
            $boostPhotoData = $this->sm->getBoostPhotoDetails($query);
            // echo "<pre>";
            // print_r($boostPhotoData);
            // die();
            $dateCheck = null; $days=null;
            if(!empty($boostPhotoData)){
              foreach($boostPhotoData as $keys => $record){
                  $days = $record['days'];
                  ?>
                  
                    <tr>
                               <?php if(!is_null($dateCheck) && $dateCheck != date("d-m-Y", strtotime($record['created_at']))){ ?>
                               
                                    <td><?php echo date("d-m-Y", strtotime($record['created_at'])) ?></td> 
                                </tr>
                               <?php }else if($keys==0){ ?>
                                    <td><?php echo date("d-m-Y", strtotime($record['created_at'])) ?></td> 
                              <?php } 
                              
                              $dateCheck = date("d-m-Y", strtotime($record['created_at']));
                              ?>
                            <tr>
                  
                  <?php
                  
                  $bg = '';
                                        if($record['isDeleted'] == 1){
                                            $bg = '#f4f4f4';
                                        }
            ?>
            
               <tr style="background:<?php echo $bg; ?>">
                    <!-- <td><input type="checkbox" class="sub_chk" data-id="<?php //echo $record->vid; ?>"></td> -->
                    <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record['id']; ?>" /></td>
                    <td style="font-weight:bold;">
                    <!-- <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php //echo $record->profile_photo;?>"><?php //echo $uname ?> -->
                    <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php echo 'https://entertainmentbugs.com/'; ?>public/uploads/users/small/<?php  echo $record['photo'] ?>"/>
                    </td>
                    
                    <td><?php echo $record['name'] ?></td> 
                    <td>Rs: <?php echo $record['amount'] ?>/-</td>
                    <td><?php echo date("d-m-Y", strtotime($record['created_at'])); ?></td>
                    <td><?php echo $record['days']." days ( ".date('d-m-Y', strtotime("$dateCheck +$days days"))." )"; ?></td>

                    <td style="position:relative">  
                     <?php if($record['isDeleted'] == 0){ ?>
                                        <button class="holdBoostPhoto btn btn-primary" href="javascript:void(0)" data-userid="<?php echo $record['id']; ?>">Hold</button>
                                            <?php }elseif($record['isDeleted'] == 1){ ?>
                                        <button class="unholdBoostPhoto btn btn-success" href="javascript:void(0)" data-userid="<?php echo $record['id']; ?>">Approve</button>
                                  
                                            <?php }?>
                    <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">-->
                    <!--Option <span class="caret"></span></a>-->
                    <!--<ul class="dropdown-menu" style="left: -100px;"> -->
                    <!--    <li><?php if($record['isDeleted'] == 0){ ?>-->
                    <!--    <a class="holdBoostPhoto"  href="javascript:void(0)" data-userid="<?php echo $record['id']; ?>">Hold</a></li>-->
                    <!--    <?php }elseif($record['isDeleted'] == 1){ ?>-->
                    <!--    <a class="unholdBoostPhoto" href="javascript:void(0)" data-userid="<?php echo $record['id']; ?>">UnHold</a>-->
                    <!--    </li>-->
                    <!--    <?php } ?>-->
                    <!--    <li><a class="deleteVideo"  href="javascript:void(0)" data-id="<?php echo $record['id']; ?>">Delete</a></li>-->
                    <!--</ul> -->
                    </td>     
                </tr> 
        <?php
              }
            }else if(empty($artist)){ ?>
                           <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
            <?php  }
        }
    
    }


?>