<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ArtistPremium extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ArtistPremium_model', 'apm');
        $this->isLoggedIn();
    }

  public function index()
    {
        redirect('artistPremium/artistPremiumListing');
    }
    
    function artistPremiumListing()
    {
                  

        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->apm->artistPremiumListingCount($searchText);
           
            $returns = $this->paginationCompress( "artistPremium/artistPremiumListing/", $count, 10 );
            
            $data['artistPremiumRecords'] = $this->apm->artistPremiumListing($searchText, $returns["page"], $returns["segment"]);
                // echo "<pre>";
                // print_r($data['artistPremiumRecords']) ;
            $this->global['pageTitle'] = 'CodeInsect : Artist Premium Listing';
            
            $this->loadViews("artistpremium/list", $this->global, $data, NULL);
        }
    }


    function holdArtistPremium()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId'); 
            //$userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>1);
            $result = $this->apm->holdUnholdArtistPremium($userId,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

    function unholdArtistPremium()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted'=>0);
            $result = $this->apm->holdUnholdArtistPremium($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    

  function filterArtistPremium() {
        $query=$this->input->post('dist_id');
      
        $artistPremiumData = $this->apm->getArtistPremiumDetails($query);
        $dateCheck = null;
        if(!empty($artistPremiumData)){
          foreach($artistPremiumData as $keys => $record){
               $uid = $record->user_id;
                $sql = "select name, email, phone from `users` where id = $uid";
                $query = $this->db->query($sql);
                $ss = $query->result(); 
                 foreach($ss as $row){
                 $uname =  $row->name;
                 $email =  $row->email;
                 $phone =  $row->phone;
                 }

              $bg = '';
                    if($record->isDeleted == 1){
                        $bg = '#f4f4f4';
                    }
        ?>
        
        
        <tr>
        <td style="font-weight:bold;">
            <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php echo $record->profile_photo;?>"><?php echo $uname ?>
        </td>
                 <td>Rs: <?php echo $record->amount; ?>/-</td>   
                <td><?php echo $email ?></td>               
                <td><?php echo $record->multiple_category ?></td>
                <!--<td><?php echo $record->language ?></td>-->
                <td><?php echo date("d-m-Y", strtotime($record->created_at)) ?></td>
                <td style="position:relative">  
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                  Option <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="left: -100px;"> 
                    <li><?php if($record->isDeleted == 0){ ?>
                        <a class="holdArtistPremium"  href="javascript:void(0)" data-userid="<?php echo $record->id; ?>">Hold</a></li>
                            <?php }elseif($record->isDeleted == 1){ ?>
                        <a class="unholdArtistPremium" href="javascript:void(0)" data-userid="<?php echo $record->id; ?>">UnHold</a>
                    </li>
                            <?php } ?>
                    <li><a class="deleteArtist"  href="javascript:void(0)" data-id="<?php echo $record->id; ?>">Delete</a></li>
                </ul> 
                </td>   
            </tr> 
    <?php
          }
        }else if(empty($artistPremiumData)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
    }

}

?>