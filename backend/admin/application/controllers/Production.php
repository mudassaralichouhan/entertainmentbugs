<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Production extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Production_model', 'pm');
        $this->isLoggedIn();
     //  $this->module = 'Artist';
    }

    /**
     * This is default routing method
     * It routes to default listing page
     */
    public function index()
    {
        redirect('production/productionListing');
    }
    
    function productionListing()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->pm->productionListingCount($searchText);

            $returns = $this->paginationCompress ( "production/productionListing/", $count, 10 );
            
            $data['productionRecords'] = $this->pm->productionListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'CodeInsect : Production Listing';
            
            $this->loadViews("production/list", $this->global, $data, NULL);
        }
    }

    function deleteProduction()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
          $id = $this->input->post('id');
         
           // $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
          
            $result = $this->pm->deleteProduction($id);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function holdProduction()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
         
            //$userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>1);
            $result = $this->pm->holdUnholdProduction($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function unholdProduction()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
         
            //$userInfo = array('isDeleted'=>0,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>0);
            $result = $this->pm->holdUnholdProduction($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function filterProduction() {

        $query=$this->input->post('dist_id');
        if($query == 0 || $query == 1){
        $production = $this->pm->getProductionDetails($query);
       }else{
        $production = $this->pm->getProductionDetails1();
       }
        
        //echo "<pre>"; print_r($artist); exit();
        if(!empty($production)){
          foreach($production as $record){
              
              $bg = '';
                                    if($record['pisDeleted'] == 1){
                                        $bg = '#f4f4f4';
                                    }
            
        ?>
            <tr style="background:<?php echo $bg; ?>">
                <td style="font-weight:bold;">
                <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php echo $record['photo'];?>"><?php echo $record['name'] ?>
                </td>
  
                    <td><?php echo $record['phone'] ?></td> 
                    <td><?php echo $record['email'] ?></td>               
                    <td><?php echo $record['multiple_category'] ?></td>
                    <td><?php echo $record['language'] ?></td>
                    <td><?php echo date("d-m-Y", strtotime($record['pcreated_at'])) ?></td>
                    <td style="position:relative">  
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                        Option <span class="caret"></span></a>
                        <ul class="dropdown-menu" style="left: -100px;"> 
                            <li><?php if($record['pisDeleted'] == 0){ ?>
                            <a class="holdProduction"  href="<?php echo base_url() ;?>productionListing" data-userid="<?php echo $record['pid']; ?>">Hold</a></li>
                            <?php }elseif($record['pisDeleted'] == 1){ ?>
                            <a class="unholdProduction" href="<?php echo base_url() ;?>productionListing" data-userid="<?php echo $record['pid']; ?>">UnHold</a>
                            </li>
                            <?php } ?>
                            <li><a class="deleteProduction"  href="<?php echo base_url() ;?>productionListing" data-id="<?php echo $record['pid']; ?>">Delete</a></li>
                        </ul> 
                    </td>     
                </tr>
    <?php
          }
        }else if(empty($production)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
    }

   
    public function productionDetailsByDate() {
        $fromDate=$this->input->post('from_date');
        $toDate=$this->input->post('to_date');

        $production = $this->pm->getProductionDetailsByDate($fromDate,$toDate);

        if(!empty($production)){
          foreach($production as $record){
              
              
              $bg = '';
                                    if($record['pisDeleted'] == 1){
                                        $bg = '#f4f4f4';
                                    }
        ?>
        <tr style="background:<?php echo $bg; ?>">
            <td style="font-weight:bold;">
            <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php echo $record['photo'];?>"><?php echo $record['name'] ?></td>
  
            <td><?php echo $record['phone'] ?></td> 
            <td><?php echo $record['email'] ?></td>               
            <td><?php echo $record['multiple_category'] ?></td>
            <td><?php echo $record['language'] ?></td>
            <td><?php echo date("d-m-Y", strtotime($record['pcreated_at'])) ?></td>
            <td style="position:relative">  
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                Option <span class="caret"></span></a>
                <ul class="dropdown-menu" style="left: -100px;"> 
                    <li><?php if($record['pisDeleted'] == 0){ ?>
                    <a class="holdProduction"  href="<?php echo base_url() ;?>productionListing" data-userid="<?php echo $record['pid']; ?>">Hold</a></li>
                    <?php }elseif($record['pisDeleted'] == 1){ ?>
                    <a class="unholdProduction" href="<?php echo base_url() ;?>productionListing" data-userid="<?php echo $record['pid']; ?>">UnHold</a>
                    </li>
                    <?php } ?>
                    <li><a class="deleteProduction"  href="<?php echo base_url() ;?>productionListing" data-id="<?php echo $record['pid']; ?>">Delete</a></li>
                </ul> 
            </td>     
        </tr>
        <?php
          }
        }else if(empty($production)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
                    
    }


}

?>