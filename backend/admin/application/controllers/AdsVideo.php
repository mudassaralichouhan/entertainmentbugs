<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class adsVideo extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdsVideo_model', 'sm');
        $this->isLoggedIn();
     //  $this->module = 'Artist';
    }

    /**
     * This is default routing method
     * It routes to default listing page
     */
    public function index()
    {
        redirect('adsvidoe/adsVideoListing');
    }
    
    function adsVideoListing()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->sm->adsVideoListingCount($searchText);

            $returns = $this->paginationCompress( "adsVideo/adsVideoListing/", $count, 25 );
            $data['adsVideoRecords'] = $this->sm->adsVideoListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'CodeInsect : Ads Video Listing';
            
            $this->loadViews("adsvideo/list", $this->global, $data, NULL);
        }
    }

    public function deleteAllStories()
    {
        // $ids = $this->input->post('ids');
 
        // $this->db->where_in('id', explode(",", $ids));
        // $this->db->delete('videos');
 
        // echo json_encode(['success'=>"Video Deleted successfully."]);

      if($this->input->post('checkbox_value'))
      {
       $id = $this->input->post('checkbox_value');
       for($count = 0; $count < count($id); $count++)
       {
        $this->sm->delete($id[$count]);
       }
      }
    }

    // function deleteAdsPhoto()
    // {
    //     if(!$this->isAdmin())
    //     {
    //         echo(json_encode(array('status'=>'access')));
    //     }
    //     else
    //     {
    //       $id = $this->input->post('id');
    //       // $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
    //         $result = $this->sm->deleteStory($id);
    //         if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
    //         else { echo(json_encode(array('status'=>FALSE))); }
    //     }
    //   // exit();
    // }

    function holdAdsVideo()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId'); 
            $userInfo = array('isDeleted'=>1);
            $result = $this->sm->holdUnholdAdsVideo($userId,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

    function unholdAdsVideo()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
         
            $userInfo = array('isDeleted'=>0);
            $result = $this->sm->holdUnholdAdsVideo($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }


    function filterAdsVideo() {
            $query=$this->input->post('dist_id');
            $adsVideoData = $this->sm->getAdsVideoDetails($query);
         
            $dateCheck = null;
            if(!empty($adsVideoData)){
              foreach($adsVideoData as $keys => $record){
                  
                  ?>
                  
                    <tr>
                               <?php if(!is_null($dateCheck) && $dateCheck != date("d-m-Y", strtotime($record['created_at']))){ ?>
                               
                                    <td><?php echo date("d-m-Y", strtotime($record['created_at'])) ?></td> 
                                </tr>
                               <?php }else if($keys==0){ ?>
                                    <td><?php echo date("d-m-Y", strtotime($record['created_at'])) ?></td> 
                              <?php } 
                              
                              $dateCheck = date("d-m-Y", strtotime($record['created_at']));
                              ?>
                            <tr>
                  
                  <?php
                  
                  $bg = '';
                                        if($record['isDeleted'] == 1){
                                            $bg = '#f4f4f4';
                                        }
            ?>
            
            
            <!--<th>Thumbnail</th>-->
            <!--                    <th>User</th>-->
            <!--                    <th>Amount</th>-->
            <!--                    <th>Title</th>-->
            <!--                    <th>Post Date</th>-->
            <!--                    <th>Post Validity</th>-->
            <!--                    <th>Action</th>-->
            
               <tr style="background:<?php echo $bg; ?>">
                    <!-- <td><input type="checkbox" class="sub_chk" data-id="<?php //echo $record->vid; ?>"></td> -->
                    <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record['id']; ?>" /></td>
                    <td style="font-weight:bold;">
    <img class="img-circle img-xs" alt="" style="margin-right:10px" src="https://entertainmentbugs.com/public/uploads/photo/thumb/<?php echo $record['file'] ?>"></td>

                    </td>
                    
                    <td><?php echo $record['name'] ?></td> 
                    <td>Rs: <?php echo $record['amount'] ?>/-</td>
                    <td><?php echo $record['title'] ?></td> 
                    <td><?php echo date("d-m-Y", strtotime($record['created_at'])) ?></td>
                    <td>-</td>
                    <td style="position:relative">  
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                    Option <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="left: -100px;"> 
                        <li><?php if($record['isDeleted'] == 0){ ?>
                        <a class="holdAdsVideo"  href="javascript:void(0)" data-userid="<?php echo $record['id']; ?>">Hold</a></li>
                        <?php }elseif($record['isDeleted'] == 1){ ?>
                        <a class="unholdAdsVideo" href="javascript:void(0)" data-userid="<?php echo $record['id']; ?>">UnHold</a>
                        </li>
                        <?php } ?>
                        <li><a class="deleteVideo"  href="javascript:void(0)" data-id="<?php echo $record['id']; ?>">Delete</a></li>
                    </ul> 
                    </td>     
                </tr> 
        <?php
              }
            }else if(empty($artist)){ ?>
                           <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
            <?php  }
        }
    
    }

?>