<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Video extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Video_model', 'rm');
        $this->isLoggedIn();
     //  $this->module = 'Artist';
    }

    /**
     * This is default routing method
     * It routes to default listing page
     */
    public function index()
    {
        redirect('video/videoListing');
    }
    
    function videoListing()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->rm->videoListingCount($searchText);
           
            $returns = $this->paginationCompress ( "video/videoListing/", $count, 25 );
            
            $data['videoRecords'] = $this->rm->videoListing($searchText, $returns["page"], $returns["segment"]);
            $data['videoThumb'] = $this->rm->videoThumb($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'CodeInsect : Video Listing';
            $this->loadViews("allVideo/list", $this->global, $data, NULL);
        }
    }

    public function deleteAll()
    {
        // $ids = $this->input->post('ids');
 
        // $this->db->where_in('id', explode(",", $ids));
        // $this->db->delete('videos');
 
        // echo json_encode(['success'=>"Video Deleted successfully."]);

      if($this->input->post('checkbox_value'))
      {
       $id = $this->input->post('checkbox_value');
       for($count = 0; $count < count($id); $count++)
       {
        $this->rm->delete($id[$count]);
       }
      }
    }

    function deleteVideo()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
           $id = $this->input->post('id');
           // $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $result = $this->rm->deleteVideo($id);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function holdVideo()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
          
            $userId = $this->input->post('userId');
          //  $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>1);
            $result = $this->rm->holdUnholdVideo($userId,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

    function unholdVideo()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
         
            //$userInfo = array('isDeleted'=>0,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>0);
            $result = $this->rm->holdUnholdVideo($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function filterVideo() {
        $query=$this->input->post('dist_id');
        if($query == 0 || $query == 1){
        $video = $this->rm->getVideoDetails($query);
        $thumb = $this->rm->getThumbDetails($query);
       }else{
        $video = $this->rm->getVideoDetails1();
        $thumb = $this->rm->getThumbDetails1();
       }

        //echo "<pre>"; print_r($artist); exit();
        if(!empty($video)){
          foreach($video as $keys => $record){
              
              $bg = '';
                                    if($record['visDeleted'] == 1){
                                        $bg = '#f4f4f4';
                                    }
        ?>
           <tr style="background:<?php echo $bg; ?>">
                <!-- <td><input type="checkbox" class="sub_chk" data-id="<?php //echo $record->vid; ?>"></td> -->
                <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record['vid']; ?>" /></td>
                <td style="font-weight:bold;">
                <!-- <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php //echo $record->profile_photo;?>"><?php //echo $uname ?> -->

                                <img src="https://entertainmentbugs.com/public/uploads/video/thumbs/<?php echo $thumb[$keys]['thumb']; ?>"  width="150px" />
                </td>
  
                <td><?php echo $record['title'] ?></td> 
                <td><?php echo $record['name'] ?></td>               
                <td><?php echo date("d-m-Y", strtotime($record['vcreated_at'])) ?></td>
                <td><?php echo $record['vstatus'] ?></td> 
                <td style="position:relative">  
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                Option <span class="caret"></span></a>
                <ul class="dropdown-menu" style="left: -100px;"> 
                    <li><?php if($record['visDeleted'] == 0){ ?>
                    <a class="holdVideo"  href="javascript:void(0)" data-userid="<?php echo $record['vid']; ?>">Hold</a></li>
                    <?php }elseif($record['visDeleted'] == 1){ ?>
                    <a class="unholdVideo" href="javascript:void(0)" data-userid="<?php echo $record['vid']; ?>">UnHold</a>
                    </li>
                    <?php } ?>
                    <li><a class="deleteVideo"  href="<?php echo base_url() ;?>videoListing" data-id="<?php echo $record['vid']; ?>">Delete</a></li>
                </ul> 
                </td>     
            </tr> 
    <?php
          }
        }else if(empty($artist)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
    }

   
    public function videoDetailsByDate() {
        $fromDate=$this->input->post('from_date');
        $toDate=$this->input->post('to_date');

        $video = $this->rm->getVideoDetailsByDate($fromDate,$toDate);
        $thumb = $this->rm->getThumbDetailsByDate($fromDate, $toDate);

        if(!empty($video)){
          foreach($video as $keys => $record){
              
              $bg = '';
                                    if($record['visDeleted'] == 1){
                                        $bg = '#f4f4f4';
                                    }
              
        ?>
            <tr style="background:<?php echo $bg; ?>">
                <!-- <td><input type="checkbox" class="sub_chk" data-id="<?php //echo $record->vid; ?>"></td> -->
                <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record['vid']; ?>" /></td>
                <td style="font-weight:bold;">
                <!-- <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php //echo $record->profile_photo;?>"><?php //echo $uname ?> -->
                     <img src="https://entertainmentbugs.com/public/uploads/video/thumbs/<?php echo $thumb[$keys]['thumb']; ?>"  width="150px" />
                </td>
                <td><?php echo $record['title'] ?></td> 
                <td><?php echo $record['name'] ?></td>               
                <td><?php echo date("d-m-Y", strtotime($record['vcreated_at'])) ?></td>
                <td><?php echo $record['vstatus'] ?></td> 
                <td style="position:relative">  
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                Option <span class="caret"></span></a>
                <ul class="dropdown-menu" style="left: -100px;"> 
                    <li><?php if($record['visDeleted'] == 0){ ?>
                    <a class="holdVideo"  href="<?php echo base_url() ;?>videoListing" data-userid="<?php echo $record['vid']; ?>">Hold</a></li>
                    <?php }elseif($record['visDeleted'] == 1){ ?>
                    <a class="unholdVideo" href="<?php echo base_url() ;?>videoListing" data-userid="<?php echo $record['vid']; ?>">UnHold</a>
                    </li>
                    <?php } ?>
                    <li><a class="deleteVideo"  href="<?php echo base_url() ;?>videoListing" data-id="<?php echo $record['vid']; ?>">Delete</a></li>
                </ul> 
                </td>     
            </tr> 
  
    <?php
          }
        }else if(empty($artist)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
                    
    }


}

?>