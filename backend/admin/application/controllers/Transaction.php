<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Transaction extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Transaction_model', 'tm');
        $this->isLoggedIn();
    }


    public function index()
    {
        redirect('transaction/transactionListing');
    }
    
    function transactionListing()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->tm->transactionListingCount($searchText);

            $returns = $this->paginationCompress( "transaction/transactionListing/", $count, 10 );
            
            $data['transactionRecords'] = $this->tm->transactionListing($searchText, $returns["page"], $returns["segment"]);

    
            //echo "pre <pre>"; print_r($data['transactionRecords']); exit;

            $this->global['pageTitle'] = 'CodeInsect : Transaction Listing';
        
            $this->loadViews("transaction/list", $this->global, $data, NULL);
        }
    }
}

?>