<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Flag extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('flag_model', 'sm');
        $this->isLoggedIn();
     //  $this->module = 'Artist';
    }

    /**
     * This is default routing method
     * It routes to default listing page
     */
    public function index()
    {
        redirect('flag/flagListing');
    }
    
    function flagListing()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->sm->flagListingCount($searchText);

            $returns = $this->paginationCompress ( "story/storyListing/", $count, 10 );
            
            $data['storyRecords'] = $this->sm->flagListing($searchText, $returns["page"], $returns["segment"]);
            $data['getflags'] = $this->sm->getFlag();
            
            $this->global['pageTitle'] = 'CodeInsect : Story Listing';
            
            $this->loadViews("flag/list", $this->global, $data, NULL);
        }
    }

    public function deleteAllStory()
    {
        // $ids = $this->input->post('ids');
 
        // $this->db->where_in('id', explode(",", $ids));
        // $this->db->delete('videos');
 
        // echo json_encode(['success'=>"Video Deleted successfully."]);

      if($this->input->post('checkbox_value'))
      {
       $id = $this->input->post('checkbox_value');
       for($count = 0; $count < count($id); $count++)
       {
        $this->sm->delete($id[$count]);
       }
      }
    }

    function deleteStory()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
           $id = $this->input->post('id');
           // $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $result = $this->sm->deleteStory($id);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function holdStory()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId'); 
            $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
           
            $result = $this->sm->holdUnholdStory($userId,$userInfo);
             //print_r($userInfo); exit();
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function unholdStory()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
         
            $userInfo = array('isDeleted'=>0,'deleted_at'=>date('Y-m-d H:i:s'));
          
            $result = $this->sm->holdUnholdStory($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function filterStory() {

        $query=$this->input->post('dist_id');
        if($query == 0 || $query == 1){
        $story = $this->sm->getStoryDetails($query);
       }else{
        $story = $this->sm->getStoryDetails1();
       }
        
        //echo "<pre>"; print_r($artist); exit();
        if(!empty($story)){
          foreach($story as $record){
        ?>
        <tr>
            <!-- <td><input type="checkbox" class="sub_chk" data-id="<?php //echo $record->vid; ?>"></td> -->
            <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record['sid']; ?>" /></td>
  
            <td><?php echo $record['title'] ?></td> 
            <td><?php echo $record['about'] ?></td> 
            <td><?php echo $record['name'] ?></td>               
            <td><?php echo date("d-m-Y", strtotime($record['screated_at'])) ?></td>
            <!-- <td><?php //echo $record->vstatus ?></td>  -->
            <td style="position:relative">  
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                Option <span class="caret"></span></a>
                <ul class="dropdown-menu" style="left: -100px;"> 
                    <li><?php if($record['sisDeleted'] == 0){ ?>
                    <a class="holdStory"  href="<?php echo base_url() ;?>storyListing" data-userid="<?php echo $record['sid']; ?>">Hold</a></li>
                    <?php }elseif($record['sisDeleted'] == 1){ ?>
                    <a class="unholdStory" href="<?php echo base_url() ;?>storyListing" data-userid="<?php echo $record['sid']; ?>">UnHold</a>
                    </li>
                    <?php } ?>
                    <li><a class="deleteStory"  href="<?php echo base_url() ;?>storyListing" data-id="<?php echo $record['sid']; ?>">Delete</a></li>
                </ul> 
            </td>     
        </tr>
    <?php
          }
        }else if(empty($story)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
    }

   
    public function storyDetailsByDate() {
        $fromDate=$this->input->post('from_date');
        $toDate=$this->input->post('to_date');

        $story = $this->sm->getStoryDetailsByDate($fromDate,$toDate);

        if(!empty($story)){
          foreach($story as $record){
        ?>
        <tr>
            <!-- <td><input type="checkbox" class="sub_chk" data-id="<?php //echo $record->vid; ?>"></td> -->
            <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record['sid']; ?>" /></td>
  
            <td><?php echo $record['title'] ?></td> 
            <td><?php echo $record['about'] ?></td> 
            <td><?php echo $record['name'] ?></td>               
            <td><?php echo date("d-m-Y", strtotime($record['screated_at'])) ?></td>
            <!-- <td><?php //echo $record->vstatus ?></td>  -->
            <td style="position:relative">  
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                Option <span class="caret"></span></a>
                <ul class="dropdown-menu" style="left: -100px;"> 
                    <li><?php if($record['sisDeleted'] == 0){ ?>
                    <a class="holdStory"  href="<?php echo base_url() ;?>storyListing" data-userid="<?php echo $record['sid']; ?>">Hold</a></li>
                    <?php }elseif($record['sisDeleted'] == 1){ ?>
                    <a class="unholdStory" href="<?php echo base_url() ;?>storyListing" data-userid="<?php echo $record['sid']; ?>">UnHold</a>
                    </li>
                    <?php } ?>
                    <li><a class="deleteStory"  href="<?php echo base_url() ;?>storyListing" data-id="<?php echo $record['sid']; ?>">Delete</a></li>
                </ul> 
            </td>     
        </tr>
  
    <?php
          }
        }else if(empty($story)){ ?>
            <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
                    
    }


}

?>