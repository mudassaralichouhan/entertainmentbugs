<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Auditions extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Audition_model', 'am');
        $this->isLoggedIn();
     //  $this->module = 'Artist';
    }

    /**
     * This is default routing method
     * It routes to default listing page
     */
    public function index()
    {
        redirect('auditions/auditionsListing');
    }
    
    function auditionsListing()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->am->auditionsListingCount($searchText);

            $returns = $this->paginationCompress ( "auditions/auditionsListing/", $count, 10 );
            
            $data['auditionRecords'] = $this->am->auditionsListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'CodeInsect : Auditions Listing';
            
            $this->loadViews("auditions/list", $this->global, $data, NULL);
        }
    }

    function deleteAudition()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
          $id = $this->input->post('id');
         
           // $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
          
            $result = $this->am->deleteAudition($id);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function holdAudition()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
         
            //$userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>1);
            $result = $this->am->holdUnholdAudition($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function unholdAudition()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
         
            //$userInfo = array('isDeleted'=>0,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>0);
            $result = $this->am->holdUnholdAudition($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function filterAudition() {

        $query=$this->input->post('dist_id');
        if($query == 0 || $query == 1){
        $audition = $this->am->getAuditionDetails('post_audtion',$query);
       }else{
        $audition = $this->am->getAuditionDetails1('post_audtion');
       }
        
        //echo "<pre>"; print_r($artist); exit();
        if(!empty($audition)){
          foreach($audition as $record){

            $uid = $record['user_id'];
            
            $bg = '';
                                    if($record['isDeleted'] == 1){
                                        $bg = '#f4f4f4';
                                    }
        ?>
           <tr style="background:<?php echo $bg; ?>">
                <td style="font-weight:bold;">
                <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php echo $record['photo'];?>"><?php echo $record['post_by_name']; ?></td>
  
                <td><?php echo $record['whatsapp_number']; ?></td> 
                <td><?php echo $record['email_address']; ?></td>               
                <td><?php echo $record['category']; ?></td>
                <!-- <td><?php //echo $record->language ?></td> -->
                <td><?php echo date("d-m-Y", strtotime($record['posted_date'])) ?></td>
                <td style="position:relative">  
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                Option <span class="caret"></span></a>
                <ul class="dropdown-menu" style="left: -100px;"> 
                    <li><?php if($record['isDeleted'] == 0){ ?>
                    <a class="holdAudition"  href="<?php echo base_url() ;?>auditionsListing" data-userid="<?php echo $record['id']; ?>">Hold</a></li>
                    <?php }elseif($record['isDeleted'] == 1){ ?>
                    <a class="unholdAudition" href="<?php echo base_url() ;?>auditionsListing" data-userid="<?php echo $record['id']; ?>">UnHold</a>
                    </li>
                    <?php } ?>
                    <li><a class="deleteAudition"  href="<?php echo base_url() ;?>auditionsListing" data-id="<?php echo $record['id']; ?>">Delete</a></li>
                </ul> 
                </td>     
            </tr>
    <?php
          }
        }else if(empty($audition)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
    }

   
    public function auditionDetailsByDate() {
        $fromDate=$this->input->post('from_date');
        $toDate=$this->input->post('to_date');

        $audition = $this->am->getAuditionDetailsByDate('post_audtion',$fromDate,$toDate);
        if(!empty($audition)){
          foreach($audition as $record){
            $uid = $record['user_id'];
            
            
            $bg = '';
                                    if($record['isDeleted'] == 1){
                                        $bg = '#f4f4f4';
                                    }
        ?>
           <tr style="background:<?php echo $bg; ?>">
                <td style="font-weight:bold;">
                <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php echo $record['photo'];?>"><?php echo $record['post_by_name']; ?></td>
  
                <td><?php echo $record['whatsapp_number']; ?></td> 
                <td><?php echo $record['email_address']; ?></td>               
                <td><?php echo $record['category']; ?></td>
                <!-- <td><?php //echo $record->language ?></td> -->
                <td><?php echo date("d-m-Y", strtotime($record['posted_date'])) ?></td>
                <td style="position:relative">  
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                Option <span class="caret"></span></a>
                <ul class="dropdown-menu" style="left: -100px;"> 
                    <li><?php if($record['isDeleted'] == 0){ ?>
                    <a class="holdAudition"  href="<?php echo base_url() ;?>auditionsListing" data-userid="<?php echo $record['id']; ?>">Hold</a></li>
                    <?php }elseif($record['isDeleted'] == 1){ ?>
                    <a class="unholdAudition" href="<?php echo base_url() ;?>auditionsListing" data-userid="<?php echo $record['id']; ?>">UnHold</a>
                    </li>
                    <?php } ?>
                    <li><a class="deleteAudition"  href="<?php echo base_url() ;?>auditionsListing" data-id="<?php echo $record['id']; ?>">Delete</a></li>
                </ul> 
                </td>     
            </tr>
    <?php
          }
        }else if(empty($audition)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
                    
    }


}

?>