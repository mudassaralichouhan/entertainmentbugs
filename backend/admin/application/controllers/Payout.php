<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Payout extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Payout_model');
        $this->isLoggedIn();
    }
      
    public function showPayout(){
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->Payout_model->payoutListingCount($searchText);

            $returns = $this->paginationCompress ( "showpayouts/", $count, 25 );
            
            $data['payoutRecords'] = $this->Payout_model->payoutListing($searchText, $returns["page"], $returns["segment"]);
            // echo "<pre>";
            // print_r($data['payoutRecords']);
            // exit();
            
            $this->global['pageTitle'] = 'CodeInsect : Payout Listing';
            
            $this->loadViews("payout/list", $this->global, $data, NULL);
        }
    }
    public function filterpayout(){
         $query=$this->input->post('dist_id');
       
            $this->load->library('pagination');
            
     
        if($query == 0 || $query == 1){
       $payoutRecords = $this->Payout_model->getPayoutDetails($query);
       }elseif($query ==3){
          $payoutRecords = $this->Payout_model->getPayoutDetails1();   
       }
       else{
         $payoutRecords = $this->Payout_model->payoutListing();
         
       }
         
                            if(!empty($payoutRecords))
                            {
                                foreach($payoutRecords as $record)
                                {
                                    $bg = '';

                            
                            
                            if($record->pisPending == 1){
                                $bg = '#f4f4f4';
                            }   
                            
                            ?>
                            
                            
                        <tr style="background:<?php echo $bg; ?>">
                     
                        <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record->pid; ?>" /></td>
             
                            
                                 <td style="font-weight:bold;">
                          <?php echo $record->name ?>
                        </td>
                               

                                <td><?php echo $record->email ?></td> 
                                        
                                 <td><?php echo $record->balance ?></td> 
                                <td><?php echo $record->amount ?></td> 
                             
                                 <td><?php echo date("d-m-Y", strtotime($record->pcreated_at)) ?></td>
                                <td style="position:relative">  
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                                  Option <span class="caret"></span></a>
                                    <ul class="dropdown-menu" style="left: -100px;"> 
                                   <li><?php if($record->status ==0 ){ ?>
                                      <a class="holdpayout" href="javascript:void(0)" data-userid="<?php echo $record->pid; ?>">Hold</a></li>
                                          <a class="approvepayout" href="javascript:void(0)" data-userid="<?php echo $record->pid; ?>">Approve</a></li>
                                           <?php }elseif($record->status == 1){ 
                                            if($record->pisPending ==0){
                                              ?><a class="unholdpayout" href="javascript:void(0)" data-userid="<?php echo $record->pid; ?>">Unhold</a>
                                              <?php
                                            }  }
                                            ?>
                                           
                                </ul> 
                                </td>     
                            </tr>
                            <?php
                        }
                    } else if(empty($storyRecords)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
                  <?php  }
                      
    }
    public function unholdpayout(){
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId'); 
           
            $userInfo = array('status'=>0);
            $result = $this->Payout_model->holdUnholdpayout($userId,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    
    }
     public function holdpayout(){
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId'); 
         
            $userInfo = array('status'=>1);
            $result = $this->Payout_model->holdUnholdpayout($userId,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    
    }
     public function approvepayout(){
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId'); 
         
            $userInfo = array('is_pending'=>1,'status'=>1);
            $result = $this->Payout_model->approvepayout($userId,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    
    }
    public function setphotoprice(){
         $this->load->model('AdsVideo_model');
      $this->load->model('AdsVideo_model');
      $data['data'] =  $this->AdsVideo_model->fetchphoto_views_clickdata(); 
    
      $data['photoprices'] =  $this->AdsVideo_model->fetchphotodata(); 
    //           echo "<pre>";
    //   print_r($data);
    //   die();
       $this->loadViews("payout/photoprice", $this->global, $data, NULL);
    }
    public function setvideoprice(){
    $this->load->model('AdsVideo_model');
      $data['data'] =  $this->AdsVideo_model->fetchvideo_views_clickdata(); 
      $data['videoprices'] =  $this->AdsVideo_model->fetchvideodata(); 
    //   echo "<pre>";
    //   print_r($data);
    //   die();
       $this->loadViews("payout/videoprice", $this->global, $data, NULL);
    }
    public function savevideoprice(){
        $this->load->model('AdsVideo_model');
        $this->load->library('form_validation');
         $this->load->helper(array('form', 'url'));
        $this->load->library('user_agent');
        $this->form_validation->set_rules('price', 'price', 'required');
        $this->form_validation->set_rules('days', 'days', 'required');
        $price = $this->input->post('price'); 
         $days = $this->input->post('days'); 
         $count = count($days);
        foreach($days as $key=>$day){
            $this->AdsVideo_model->save_video_price(['days'=>$days[$key],'price'=>$price[$key]],$key);
        }
      
        $this->session->set_flashdata('success', 'Record Updated Successfully');
         redirect($this->agent->referrer());
    }
     public function savevideoviewandprice(){
         $this->load->model('AdsVideo_model');
        $this->load->library('form_validation');
         $this->load->helper(array('form', 'url'));
        $this->load->library('user_agent');
        $this->form_validation->set_rules('videoviews', 'videoclick', 'required');
        $this->form_validation->set_rules('videoclick', 'videoclick', 'required');
        $videoviews = $this->input->post('videoviews'); 
        $videoclick = $this->input->post('videoclick'); 
         $data =  $this->AdsVideo_model->fetchvideo_views_clickdata(); 
       
        if($videoviews != ''){
              $datas = array('views_price'=>$videoviews,'clicks_price'=>$data->clicks_price);
        }
        if($videoclick !=''){
               $datas = array('views_price'=>$data->views_price,'clicks_price'=>$videoclick);
        }
        
       
            $this->AdsVideo_model->savevideoviewandprice($datas); 
            $this->session->set_flashdata('success', 'Record Updated Successfully');
         redirect($this->agent->referrer());
    }
    public function savephotoprice(){
         $this->load->model('AdsVideo_model');
        $this->load->library('form_validation');
         $this->load->helper(array('form', 'url'));
        $this->load->library('user_agent');
        $this->form_validation->set_rules('price', 'price', 'required');
        $this->form_validation->set_rules('days', 'days', 'required');
        $price = $this->input->post('price'); 
         $days = $this->input->post('days'); 
         $count = count($days);
         //echo  $count;
        foreach($days as $key=>$day){
            $this->AdsVideo_model->save_photo_price(['days'=>$days[$key],'price'=>$price[$key]],$key);
        }
      
        $this->session->set_flashdata('success', 'Record Updated Successfully');
         redirect($this->agent->referrer());
    }
    public function savephotoviewsandprice(){
          $this->load->model('AdsVideo_model');
        $this->load->library('form_validation');
         $this->load->helper(array('form', 'url'));
        $this->load->library('user_agent');
        $this->form_validation->set_rules('photoviews', 'photoviews', 'required');
        $this->form_validation->set_rules('photoclick', 'photoclick', 'required');
        $photoviews = $this->input->post('photoviews'); 
        $photoclick = $this->input->post('photoclick'); 
        $data =  $this->AdsVideo_model->fetchphoto_views_clickdata(); 
       
        if($photoviews != ''){
              $datas = array('views_price'=>$photoviews,'clicks_price'=>$data->clicks_price??null);
              $data2 = array('view_range'=>$photoviews,'click_range'=>$data->clicks_price??null);
        }
        if($photoclick !=''){
               $datas = array('views_price'=>$data->views_price??null,'clicks_price'=>$photoclick);
               $data2 = array('view_range'=>$data->views_price??null,'click_range'=>$photoclick);
        }
        
       
            $this->AdsVideo_model->savephotoviewandprice($datas,$data2); 
            $this->session->set_flashdata('success', 'Record Updated Successfully');
         redirect($this->agent->referrer());
    }
}