<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class User extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('Admin');
        $this->isLoggedIn();
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'CodeInsect : Dashboard';
        
        $this->loadViews("general/dashboard", $this->global, NULL , NULL);
    }
    
    /**
     * This function is used to load the user list
     */
    function userListing()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->userListingCount($searchText);

			$returns = $this->paginationCompress ( "userListing/", $count, 25 );
            
            $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'CodeInsect : User Listing';
            
            $this->loadViews("users/users", $this->global, $data, NULL);
        }
    }
    

    /**
     * This function is used to load the add new form
     */
    function addNew()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            
            $this->global['pageTitle'] = 'CodeInsect : Add New User';

            $this->loadViews("users/addNew", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to check whether email already exist or not
     */
    function checkEmailExists()
    {
        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ echo("true"); }
        else { echo("false"); }
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewUser()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','required|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','trim|required|matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->addNew();
            }
            else
            {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $isAdmin = $this->input->post('isAdmin');
                
                $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId,
                        'name'=> $name, 'mobile'=>$mobile, 'isAdmin'=>$isAdmin,
                        'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'));
                
                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);
                
                if($result > 0){
                    $this->session->set_flashdata('success', 'New User created successfully');
                } else {
                    $this->session->set_flashdata('error', 'User creation failed');
                }
                
                redirect('addNew');
            }
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOld($userId = NULL)
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {
            if($userId == null)
            {
                redirect('userListing');
            }
            
            $data['roles'] = $this->user_model->getUserRoles();
            $data['userInfo'] = $this->user_model->getUserInfo($userId);

            $this->global['pageTitle'] = 'CodeInsect : Edit User';
            
            $this->loadViews("users/editOld", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editUser()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $userId = $this->input->post('userId');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password','Password','matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword','Confirm Password','matches[password]|max_length[20]');
            $this->form_validation->set_rules('role','Role','trim|required|numeric');
            $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOld($userId);
            }
            else
            {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $isAdmin = $this->input->post('isAdmin');
                
                $userInfo = array();
                
                if(empty($password))
                {
                    $userInfo = array('email'=>$email, 'roleId'=>$roleId, 'name'=>$name, 'mobile'=>$mobile,
                        'isAdmin'=>$isAdmin, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                else
                {
                    $userInfo = array('email'=>$email, 'password'=>getHashedPassword($password), 'roleId'=>$roleId,
                        'name'=>ucwords($name), 'mobile'=>$mobile, 'isAdmin'=>$isAdmin, 
                        'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
                }
                
                $result = $this->user_model->editUser($userInfo, $userId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'User updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'User updation failed');
                }
                
                redirect('userListing');
            }
        }
    }


    /**
     * This function is used to delete the user using userId
     * @return boolean $result : TRUE / FALSE
     */
    function deleteUser()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }else
        {
            $id = $this->input->post('id');
           // $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $result = $this->user_model->delete($id);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }


     function userHold($user_id=null)
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }else{
            $id = $user_id=null;
            $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $result = $this->user_model->userHoldUnhold($id,$userInfo);
          //  if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
          //  else { echo(json_encode(array('status'=>FALSE))); }
          redirect('userListing');
        }
    }
    
    function userUnhold($user_id=null)
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }else{
            $id = $user_id;
            $userInfo = array('isDeleted'=>0,'deleted_at'=>'');
            $result = $this->user_model->UserHoldUnhold($id,$userInfo);
           // if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
           // else { echo(json_encode(array('status'=>FALSE))); }
           redirect('userListing');
        }
    }
    
    
    

     function holdUser()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }else{
            $id = $this->input->post('userId');
           // $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>1);
            $result = $this->user_model->holdUnholdUser($id,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    


    function unholdUser()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }else{
            

            $id = $this->input->post('userId');
           // $userInfo = array('isDeleted'=>0,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>0);
            $result = $this->user_model->holdUnholdUser($id,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    

    function filterUser() {

       echo $query=$this->input->post('dist_id');
        if($query == 0 || $query == 1){
        $user = $this->user_model->getUserDetails('users',$query);
       }else{
        $user = $this->user_model->getUserDetails1('users');
       }
        
        //echo "<pre>"; print_r($artist); exit();
        if(!empty($user)){
          foreach($user as $data){
                $videos = $this->db->where(['user_id'=>$data['id']])->from("videos")->count_all_results();
                $stories = $this->db->where(['user_id'=>$data['id']])->from("stories")->count_all_results();
                $photos = $this->db->where(['user_id'=>$data['id']])->from("upload_photos")->count_all_results(); 
                $followers = $this->db->where(['user_id'=>$data['id']])->from("followers")->count_all_results();

                $ii = $data['id'];
                
                $bg = '';
                                    if($data['isDeleted'] == 1){
                                        $bg = '#f4f4f4';
                                    }
                              
                $sql = "select sum(`isRewardVideo`) as isRewardVideo from `videos` where user_id = $ii";

                $query = $this->db->query($sql);
                $ss = $query->result(); 
                foreach($ss as $row){
                $reward =  $row->isRewardVideo;
                }                
                //$aa = $this->db->last_query(); print_r($aa); exit();  
            ?>
            <tr style="background:<?php echo $bg; ?>">
                <td style="font-weight:bold;"><img src="<?php echo 'https://entertainmentbugs.com/'; ?>public/uploads/users/small/<?php  echo $data['photo'] ?>" class="img-circle img-xs" alt="" style="margin-right:10px"><?php echo $data['name'] ?></td>
  
                <td><?php echo $data['email'] ?></td> 
                <td><?php echo $videos ?></td>               
                <td><?php echo $photos ?></td>
                <td><?php echo $stories ?></td>
                <?php if($reward != null) { ?>           
                <td>12 /<?php echo $reward ?></td>
                <?php }else{ ?>
                <td>12 /0</td> 
                <?php } ?>     
                <td><?php echo $followers ?></td>
                <td><?php echo date("d-m-Y", strtotime($data['created_at'])) ?></td>
                <td style="position:relative">  
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                Option <span class="caret"></span></a>
                                   
                <ul class="dropdown-menu" style="left: -100px;"> 
                   <li><?php if($data['isDeleted'] == 0){ ?>
                        <a class="holdUser"  href="<?php echo base_url() ;?>userListing" data-userid="<?php echo $data['id']; ?>">Hold</a></li>
                        <?php }elseif($data['isDeleted'] == 1){ ?>
                        <a class="unholdUser" href="<?php echo base_url() ;?>userListing" data-userid="<?php echo $data['id']; ?>">UnHoad</a>
                    </li>
                    <?php } ?>
                    <li><a class="deleteUser"  href="<?php echo base_url() ;?>userListing" data-id="<?php echo $data['id']; ?>">Delete</a></li>
                </ul> 
                </td>   
            </tr>
                <?php }
                }else if(empty($user)){ ?>
                <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
                <?php  }
    }

    public function userDetailsByDate() {
        $fromDate=$this->input->post('from_date');
        $toDate=$this->input->post('to_date');

        $user = $this->user_model->getuserDetailsByDate('users',$fromDate,$toDate);

        if(!empty($user)){
          foreach($user as $data){
           // $id = $data['id'];
            $videos = $this->db->where(['user_id'=>$data['id']])->from("videos")->count_all_results();
           // $count = $this->user_model->getAll('videos',$fromDate,$toDate,$id);
           // $videos = count($count);
            $stories = $this->db->where(['user_id'=>$data['id']])->from("stories")->count_all_results();
            $photos = $this->db->where(['user_id'=>$data['id']])->from("upload_photos")->count_all_results(); 
            $followers = $this->db->where(['user_id'=>$data['id']])->from("followers")->count_all_results();
            $ii = $data['id'];             
            $sql = "select sum(`isRewardVideo`) as isRewardVideo from `videos` where user_id = $ii";
            $query = $this->db->query($sql);
            $ss = $query->result(); 
            foreach($ss as $row){
            $reward =  $row->isRewardVideo;
            
            
                $bg = '';
                                    if($data['isDeleted'] == 1){
                                        $bg = '#f4f4f4';
                                    }
            }
        ?>
            <tr style="background:<?php echo $bg; ?>">
                <td style="font-weight:bold;"><img src="<?php echo 'https://entertainmentbugs.com/'; ?>public/uploads/users/small/<?php  echo $data['photo'] ?>" class="img-circle img-xs" alt="" style="margin-right:10px"><?php echo $data['name'] ?></td>
  
                <td><?php echo $data['email'] ?></td> 
                <td><?php echo $videos ?></td>               
                <td><?php echo $photos ?></td>
                <td><?php echo $stories ?></td>
                <?php if($reward != null) { ?>           
                <td>12 /<?php echo $reward ?></td>
                <?php }else{ ?>
                <td>12 /0</td> 
                <?php } ?>     
                <td><?php echo $followers ?></td>
                <td><?php echo date("d-m-Y", strtotime($data['created_at'])) ?></td>
                <td style="position:relative">  
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                Option <span class="caret"></span></a>
                                   
                <ul class="dropdown-menu" style="left: -100px;"> 
                   <li><?php if($data['isDeleted'] == 0){ ?>
                        <a class="holdUser"  href="<?php echo base_url() ;?>userListing" data-userid="<?php echo $data['id']; ?>">Hold</a></li>
                        <?php }elseif($data['isDeleted'] == 1){ ?>
                        <a class="unholdUser" href="<?php echo base_url() ;?>userListing" data-userid="<?php echo $data['id']; ?>">UnHoad</a>
                    </li>
                    <?php } ?>
                    <li><a class="deleteUser"  href="<?php echo base_url() ;?>userListing" data-id="<?php echo $data['id']; ?>">Delete</a></li>
                </ul> 
                </td>   
            </tr>
        <?php }
        }else if(empty($user)){ ?>
        <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }              
        }

    /**
     * Page not found : error 404
     */
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'CodeInsect : 404 - Page Not Found';
        
        $this->loadViews("general/404", $this->global, NULL, NULL);
    }

    /**
     * This function used to show login history
     * @param number $userId : This is user id
     */
    function loginHistoy($userId = NULL)
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {
            $userId = ($userId == NULL ? 0 : $userId);

            $searchText = $this->input->post('searchText');
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');

            $data["userInfo"] = $this->user_model->getUserInfoById($userId);

            $data['searchText'] = $searchText;
            $data['fromDate'] = $fromDate;
            $data['toDate'] = $toDate;
            
            $this->load->library('pagination');
            
            $count = $this->user_model->loginHistoryCount($userId, $searchText, $fromDate, $toDate);

            $returns = $this->paginationCompress ( "login-history/".$userId."/", $count, 10, 3);

            $data['userRecords'] = $this->user_model->loginHistory($userId, $searchText, $fromDate, $toDate, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'CodeInsect : User Login History';
            
            $this->loadViews("users/loginHistory", $this->global, $data, NULL);
        }        
    }

    /**
     * This function is used to show users profile
     */
    function profile($active = "details")
    {
        $data["userInfo"] = $this->user_model->getUserInfoWithRole($this->vendorId);
        $data["active"] = $active;
        
        $this->global['pageTitle'] = $active == "details" ? 'CodeInsect : My Profile' : 'CodeInsect : Change Password';
        $this->loadViews("users/profile", $this->global, $data, NULL);
    }

    /**
     * This function is used to update the user details
     * @param text $active : This is flag to set the active tab
     */
    function profileUpdate($active = "details")
    {
        $this->load->library('form_validation');
            
        $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]');
        $this->form_validation->set_rules('mobile','Mobile Number','required|min_length[10]');
        $this->form_validation->set_rules('email','Email','trim|required|valid_email|max_length[128]|callback_emailExists');        
        
        if($this->form_validation->run() == FALSE)
        {
            $this->profile($active);
        }
        else
        {
            $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
            $mobile = $this->security->xss_clean($this->input->post('mobile'));
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            
            $userInfo = array('name'=>$name, 'email'=>$email, 'mobile'=>$mobile, 'updatedBy'=>$this->vendorId, 'updatedDtm'=>date('Y-m-d H:i:s'));
            
            $result = $this->user_model->editUser($userInfo, $this->vendorId);
            
            if($result == true)
            {
                $this->session->set_userdata('name', $name);
                $this->session->set_flashdata('success', 'Profile updated successfully');
            }
            else
            {
                $this->session->set_flashdata('error', 'Profile updation failed');
            }

            redirect('profile/'.$active);
        }
    }

    /**
     * This function is used to change the password of the user
     * @param text $active : This is flag to set the active tab
     */
    function changePassword($active = "changepass")
    {
        $this->load->helper(array('form', 'url'));
        $this->load->library('user_agent');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required');
        $this->form_validation->set_rules('newPassword','New password','required');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]');
        
        if($this->form_validation->run() == FALSE)
        { echo validation_errors();
        $this->session->set_flashdata('error', validation_errors());
        
        redirect($this->agent->referrer());
       
        }
        else
        {
           
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
            if(!$this->Admin->getUserInfoWithRole($this->vendorId))
        {
           
          $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword); 
        }else{
             
            $resultPas = $this->Admin->matchOldPassword($this->vendorId, $oldPassword);
        }
            
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password is not correct');
                redirect($this->agent->referrer());
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
                  if(!$this->Admin->getUserInfoWithRole($this->vendorId))
        {
            
                $result = $this->user_model->changePassword($this->vendorId, $usersData);
        }else{
          
              $result = $this->Admin->changePassword($this->vendorId, $usersData);
        }
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                
                redirect('profile/'.$active);
            }
        }
    }

    /**
     * This function is used to check whether email already exist or not
     * @param {string} $email : This is users email
     */
    function emailExists($email)
    {
        $userId = $this->vendorId;
        $return = false;

        if(empty($userId)){
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if(empty($result)){ $return = true; }
        else {
            $this->form_validation->set_message('emailExists', 'The {field} already taken');
            $return = false;
        }

        return $return;
    }

    public function detailsByDate() {
        $fromDate=$this->input->post('from_date');
        $toDate=$this->input->post('to_date');

        $inward = $this->user_model->getInwardDetailsByDate('inward',$fromDate,$toDate);

        if(!empty($inward)){
          $no=0;
          foreach($inward as $data){
            $no++;
    ?>
            <tr>
              <td><?php echo $no; ?></td>
              <td><?php echo $data['inward_date']; ?></td>
              <td><?php echo $data['inward_letter_date']; ?></td>
              <td><?php echo $data['received_from']; ?></td>
              <td><?php echo $data['deliver_to']; ?></td>
              <td><?php echo $data['letter_type']; ?></td>
              <td style="font-family: KrutiDev;"><?php echo $data['subject'] ?></td>
              <td>
                  <a href="<?php echo site_url('edit-inward/'.$data['id']); ?>" class="btn btn-xs btn-success"><i class="far fa-edit">Edit</i></a>
              </td>
            </tr>
    <?php
          }
        }
                    
    }
    public function adminChangePassword(){
        $this->loadViews("users/changepasswordadmin", $this->global, NULL, NULL);
    }
    public function complains(){
        $this->global['pageTitle'] = 'CodeInsect : Complains';
         $data['compaints'] = $this->user_model->complains();
        // echo "<pre>";
        // print_r($data['compaints']);
        $this->loadViews("complains/complains", $this->global, $data, NULL);
    }
      public function subcategories(){
           if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {
          $page=$this->input->get('page');
		  $limit=20;

            $searchText = $this->input->post('searchText');
           
            $data['searchText'] = $searchText;
    
            
            $this->load->library('pagination');
             $count  = count($this->user_model->subcategories($searchText));
           

            $returns = $this->paginationCompress ( "subcategories", $count, 10, 3);
            $data['subcategories'] = $this->user_model->subcategories($searchText,$page, $limit);
			   $data['categories'] = $this->user_model->categories($searchText,$page, $limit);
			
			



        $this->global['pageTitle'] = 'CodeInsect : Subcategories';
        // echo "<pre>";
        // print_r($data['subcategories']);
        $this->loadViews("subcategories/subcategories", $this->global, $data, NULL);
        }
    }
    
    public function changeadminpassword($active = "changepass"){
        $this->load->helper(array('form', 'url'));
        $this->load->library('user_agent');
        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('oldPassword','Old password','required');
        $this->form_validation->set_rules('newPassword','New password','required');
        $this->form_validation->set_rules('cNewPassword','Confirm new password','required|matches[newPassword]');
        
        if($this->form_validation->run() == FALSE)
        { echo validation_errors();
        $this->session->set_flashdata('error', validation_errors());
        
        redirect($this->agent->referrer());
       
        }
        else
        {
           
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');
       
            $resultPas = $this->Admin->matchOldPassword($this->vendorId, $oldPassword);
        
            
            if(empty($resultPas))
            {
                $this->session->set_flashdata('nomatch', 'Your old password is not correct');
                redirect($this->agent->referrer());
            }
            else
            {
                $usersData = array('password'=>getHashedPassword($newPassword), 'updatedBy'=>$this->vendorId,
                                'updatedDtm'=>date('Y-m-d H:i:s'));
              
          
              $result = $this->Admin->changePassword($this->vendorId, $usersData);
        
                if($result > 0) { $this->session->set_flashdata('success', 'Password updation successful'); }
                else { $this->session->set_flashdata('error', 'Password updation failed'); }
                redirect($this->agent->referrer());
                
            }
        }
    }
       public function addSubcategory(){
           if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {
        $this->global['pageTitle'] = 'CodeInsect : Add Subcategory';
         $data['parentCategories'] = $this->user_model->allParentCategories();
        // echo "<pre>";
        // print_r($data['parentCategories']);
        $this->loadViews("subcategories/addSubCategories", $this->global, $data, NULL);
        }
    }
       public function createSubcategory(){
        
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {
            
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('parent','Parent Category','required');
          //  $this->form_validation->set_rules('name','Sub Category Name','trim|required|max_length[128]');
            if($this->form_validation->run() == FALSE)
            {
                $this->addSubcategory();
            }
            else
            {
			$subcategories=$this->input->post('name');
			
				foreach($subcategories As $subcat){
					if($subcat!=''){
                $name = ucwords(strtolower($this->security->xss_clean($subcat)));
                $slug_name = url_title(strtolower($name));
                
                $parent_id = $this->security->xss_clean($this->input->post('parent'));
                
                $subCategory = array('parent_id'=>$parent_id, 'category'=>$name, 'slug'=>$slug_name,
                        'status'=> 1, 'created_at'=>date('Y-m-d H:i:s'));
                
                $this->load->model('user_model');
                $result = $this->user_model->addsubCategory($subCategory);
                }
				}
                if($result > 0){
                    $this->session->set_flashdata('success', 'Record created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Record creation failed');
                }
                
                redirect('subcategories');
            }
            
        }
    }
    public function editSubCategory($id){
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {
            $this->global['pageTitle'] = 'CodeInsect : Add Subcategory';
            $data['parentCategories'] = $this->user_model->allParentCategories();
            $data['subCategory'] =  $this->user_model->editSubCategory($id);
        // echo "<pre>";
        // print_r($data['subCategory']->id);
        $this->loadViews("subcategories/editSubCategory", $this->global, $data, NULL);
        }
    }
	
	
	 public function ajaxupdateSubcategory(){
      
	   
	  echo  $cname=$_POST['name'];
	   echo $id=$_POST['id'];
	     $name = ucwords(strtolower($this->security->xss_clean($cname)));
                $slug_name = url_title(strtolower($name));
                
                //$parent_id = $this->security->xss_clean($this->input->post('parent'));
                
                $subCategory = array( 'category'=>$name, 'slug'=>$slug_name,
                        'status'=> 1, 'updated_at'=>date('Y-m-d H:i:s'));
            $result = $this->user_model->updateSubcategory($subCategory, $id);
                
                if($result == true)
                {
                   return "yes";
                }
                else
                {
                    return "no";
                }
                exit;
                
        }
    
	
	
	
    public function updateSubcategory($id){
         if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('name'))));
                $slug_name = url_title(strtolower($name));
                
                $parent_id = $this->security->xss_clean($this->input->post('parent'));
                
                $subCategory = array('parent_id'=>$parent_id, 'category'=>$name, 'slug'=>$slug_name,
                        'status'=> 1, 'updated_at'=>date('Y-m-d H:i:s'));
            $result = $this->user_model->updateSubcategory($subCategory, $id);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'Record updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Record updation failed');
                }
                
                redirect('subcategories');
        }
    }
    public function unholdSubcategory($id){
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }else{
            $userInfo = array('status'=>1,'updated_at'=>date('Y-m-d H:i:s'));
            $result = $this->user_model->unholdSubcategory($id,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
         // redirect('subcategories');
        }
    }
	
	  public function ajaxholdSubcategory(){
     $id=$_POST['id'];
	
            $userInfo = array('status'=>0,'updated_at'=>date('Y-m-d H:i:s'));
            $result = $this->user_model->holdSubcategory($id,$userInfo);
            if ($result > 0) { 
			
			echo(json_encode(array('status'=>TRUE))); }
            else {

				echo(json_encode(array('status'=>FALSE))); }
          //redirect('subcategories');
       
    }
	
		  public function ajaxunholdSubcategory(){
     $id=$_POST['id'];
	
            $userInfo = array('status'=>0,'updated_at'=>date('Y-m-d H:i:s'));
            $result = $this->user_model->holdSubcategory($id,$userInfo);
            if ($result > 0) { 
			
			echo(json_encode(array('status'=>TRUE))); }
            else {

				echo(json_encode(array('status'=>FALSE))); }
          //redirect('subcategories');
       
    }
	
	
    public function holdSubcategory($id){
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }else{
             $userInfo = array('status'=>1,'updated_at'=>date('Y-m-d H:i:s'));
            $result = $this->user_model->unholdSubcategory($id,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
          //redirect('subcategories');
        }
    }
}

?>