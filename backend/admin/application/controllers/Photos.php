<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Photos extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Photos_model', 'pm');
        $this->isLoggedIn();
     // $this->module = 'Artist';
    }

    /**
     * This is default routing method
     * It routes to default listing page
     */
    public function index()
    {
        redirect('photos/photosListing');
    }
    
    function photosListing()
    {
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->pm->photoListingCount($searchText);

            $returns = $this->paginationCompress ( "photos/photosListing/", $count, 25 );
            
            $data['photoRecords'] = $this->pm->photoListing($searchText, $returns["page"], $returns["segment"]);
            
            $this->global['pageTitle'] = 'CodeInsect : Photo Listing';
            
            $this->loadViews("allPhoto/list", $this->global, $data, NULL);
        }
    }

    public function deleteAllPhotos()
    {
        // $ids = $this->input->post('ids');
 
        // $this->db->where_in('id', explode(",", $ids));
        // $this->db->delete('videos');
 
        // echo json_encode(['success'=>"Video Deleted successfully."]);

      if($this->input->post('checkbox_value'))
      {
       $id = $this->input->post('checkbox_value');
       for($count = 0; $count < count($id); $count++)
       {
        $this->pm->delete($id[$count]);
       }
      }
    }

    function deletePhotos()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
           $id = $this->input->post('id');
           // $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $result = $this->pm->deletePhoto($id);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function holdPhotos()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
           // $userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>1);
            $result = $this->pm->holdUnholdPhoto($userId,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function unholdPhotos()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
         
           // $userInfo = array('isDeleted'=>0,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>0);
            $result = $this->pm->holdUnholdPhoto($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
       // exit();
    }

    function filterPhotos() {
        $query=$this->input->post('dist_id');
        if($query == 0 || $query == 1){
        $photo = $this->pm->getPhotoDetails($query);
       }else{
        $photo = $this->pm->getPhotoDetails1();
       }
        
        //echo "<pre>"; print_r($artist); exit();
        if(!empty($photo)){
          foreach($photo as $record){
            $photo = unserialize($record['images']);
            foreach($photo as $row){
            $img = $row;
            }
            
            $bg = '';
                                    if($record['pisDeleted'] == 1){
                                        $bg = '#f4f4f4';
                                    }
        ?>
            <tr style="background:<?php echo $bg; ?>">
                <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record['pid']; ?>" /></td>
                <td style="font-weight:bold;">
                <img class="img-circle img-xs" alt="" style="margin-right:10px" src="https://entertainmentbugs.com/public/uploads/photo/<?php echo $img;?>">
                </td>
  
                <td><?php echo substr($record['photo_title'], 0, 30); ?></td> 
                <td><?php echo $record['name'] ?></td>               
                <td><?php echo date("d-m-Y", strtotime($record['pcreated_at'])) ?></td>
                <!-- <td><?php //echo $record->vstatus ?></td>  -->
                <td style="position:relative">  
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                    Option <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="left: -100px;"> 
                        <li><?php if($record['pisDeleted'] == 0){ ?>
                        <a class="holdPhoto"  href="<?php echo base_url() ;?>photosListing" data-userid="<?php echo $record['pid']; ?>">Hold</a></li>
                        <?php }elseif($record['pisDeleted'] == 1){ ?>
                        <a class="unholdPhoto" href="<?php echo base_url() ;?>photosListing" data-userid="<?php echo $record['pid'] ?>">UnHold</a>
                        </li>
                        <?php } ?>
                        <li><a class="deletePhoto"  href="<?php echo base_url() ;?>photosListing" data-id="<?php echo $record['pid']; ?>">Delete</a></li>
                    </ul> 
                </td>     
            </tr> 
    <?php
          }
        }else if(empty($photo)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
    }

   
    public function photoDetailsByDate() {
        $fromDate=$this->input->post('from_date');
        $toDate=$this->input->post('to_date');

        $photo = $this->pm->getPhotoDetailsByDate($fromDate,$toDate);

        if(!empty($photo)){
          foreach($photo as $record){
            $photo = unserialize($record['images']);
            foreach($photo as $row){
            $img = $row;
            }
            
            $bg = '';
                                    if($record['pisDeleted'] == 1){
                                        $bg = '#f4f4f4';
                                    }
        ?>
            <tr style="background:<?php echo $bg; ?>">
                <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record['pid']; ?>" /></td>
                <td style="font-weight:bold;">
                <img class="img-circle img-xs" alt="" style="margin-right:10px" src="https://entertainmentbugs.com/public/uploads/photo/<?php echo $img;?>">
                </td>
  
                <td><?php echo substr($record['photo_title'], 0, 30); ?></td> 
                <td><?php echo $record['name'] ?></td>               
                <td><?php echo date("d-m-Y", strtotime($record['pcreated_at'])) ?></td>
                <!-- <td><?php //echo $record->vstatus ?></td>  -->
                <td style="position:relative">  
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                    Option <span class="caret"></span></a>
                    <ul class="dropdown-menu" style="left: -100px;"> 
                        <li><?php if($record['pisDeleted'] == 0){ ?>
                        <a class="holdPhoto"  href="<?php echo base_url() ;?>photosListing" data-userid="<?php echo $record['pid']; ?>">Hold</a></li>
                        <?php }elseif($record['pisDeleted'] == 1){ ?>
                        <a class="unholdPhoto" href="<?php echo base_url() ;?>photosListing" data-userid="<?php echo $record['pid'] ?>">UnHold</a>
                        </li>
                        <?php } ?>
                        <li><a class="deletePhoto"  href="<?php echo base_url() ;?>photosListing" data-id="<?php echo $record['pid']; ?>">Delete</a></li>
                    </ul> 
                </td>     
            </tr>   
    <?php
          }
        }else if(empty($artist)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
                    
    }


}

?>