<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class AdsPhoto extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdsPhotos_model', 'sm');
        $this->isLoggedIn();
     //  $this->module = 'Artist';
    }

    /**
     * This is default routing method
     * It routes to default listing page
     */
    public function index()
    {
        redirect('adsphoto/adsPhotoListing');
    }
    
    function adsPhotoListing()
    {
       // exit("ads photo listing");
        if(!$this->isAdmin())
        {
            $this->loadThis();
        }
        else
        {        
            $searchText = '';
            if(!empty($this->input->post('searchText'))) {
                $searchText = $this->security->xss_clean($this->input->post('searchText'));
            }
            $data['searchText'] = $searchText;
            
            $this->load->library('pagination');
            
            $count = $this->sm->adsPhotoListingCount($searchText);
           
            $returns = $this->paginationCompress( "adsPhoto/adsPhotoListing/", $count, 25 );
            
            $data['adsPhotoRecords'] = $this->sm->adsPhotoListing($searchText, $returns["page"], $returns["segment"]);
          
            $this->global['pageTitle'] = 'CodeInsect : Ads Photo Listing';
            
            $this->loadViews("adsphoto/list", $this->global, $data, NULL);
        }
    }


    function holdAdsPhoto()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId'); 
            //$userInfo = array('isDeleted'=>1,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>1);
            $result = $this->sm->holdUnholdAdsPhoto($userId,$userInfo);
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

    function unholdAdsPhoto()
    {
        if(!$this->isAdmin())
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $userId = $this->input->post('userId');
         
            //$userInfo = array('isDeleted'=>0,'deleted_at'=>date('Y-m-d H:i:s'));
            $userInfo = array('isDeleted'=>0);
            $result = $this->sm->holdUnholdAdsPhoto($userId,$userInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }
    

  function filterAdsPhoto() {
        $query=$this->input->post('dist_id');
        $adsPhotoData = $this->sm->getAdsPhotoDetails($query);
        $dateCheck = null;
        if(!empty($adsPhotoData)){
          foreach($adsPhotoData as $keys => $record){
              
              ?>
              
                <tr>
                           <?php if(!is_null($dateCheck) && $dateCheck != date("d-m-Y", strtotime($record['created_at']))){ ?>
                           
                                <td><?php echo date("d-m-Y", strtotime($record['created_at'])) ?></td> 
                            </tr>
                           <?php }else if($keys==0){ ?>
                                <td><?php echo date("d-m-Y", strtotime($record['created_at'])) ?></td> 
                          <?php } 
                          
                          $dateCheck = date("d-m-Y", strtotime($record['created_at']));
                          ?>
                        <tr>
              
              <?php
              
              $bg = '';
                                    if($record['isDeleted'] == 1){
                                        $bg = '#f4f4f4';
                                    }
        ?>
        
  
           <tr style="background:<?php echo $bg; ?>">
                <!-- <td><input type="checkbox" class="sub_chk" data-id="<?php //echo $record->vid; ?>"></td> -->
                <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record['id']; ?>" /></td>
                <td style="font-weight:bold;">
                <!-- <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php //echo $record->profile_photo;?>"><?php //echo $uname ?> -->
<img class="img-circle img-xs" alt="" style="margin-right:10px" src="https://entertainmentbugs.com/public/uploads/photo/thumb/<?php echo $record['file'] ?>"></td>
                </td>
                
                <td><?php echo $record['name'] ?></td> 
                <td>Rs: <?php echo $record['amount'] ?>/-</td>
                <td><?php echo $record['title'] ?></td> 
                <td><?php echo date("d-m-Y", strtotime($record['created_at'])) ?></td>
                <td>-</td>
                <td style="position:relative">  
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                Option <span class="caret"></span></a>
                <ul class="dropdown-menu" style="left: -100px;"> 
                    <li><?php if($record['isDeleted'] == 0){ ?>
                    <a class="holdAdsPhoto"  href="javascript:void(0)" data-userid="<?php echo $record['id']; ?>">Hold</a></li>
                    <?php }elseif($record['isDeleted'] == 1){ ?>
                    <a class="unholdAdsPhoto" href="javascript:void(0)" data-userid="<?php echo $record['id']; ?>">UnHold</a>
                    </li>
                    <?php } ?>
                    <li><a class="deleteVideo"  href="javascript:void(0)" data-id="<?php echo $record['id']; ?>">Delete</a></li>
                </ul> 
                </td>     
            </tr> 
    <?php
          }
        }else if(empty($artist)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
        <?php  }
    }

}

?>