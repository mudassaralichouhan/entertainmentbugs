<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');
require_once BASEPATH."libraries/Form_validation.php";

class CustomFormValidator extends CI_Form_validation {
    public function setCustomError($field, $message) {
        $this->_error_array[$field] = $message;
    }

    public function getRawPostData() {
        if ($_SERVER["REQUEST_METHOD"] === "POST") {
            if (is_array($_POST)) {
                return $_POST;
            }

            $response = json_decode(file_get_contents("php://input"), true);
            if (json_last_error() === JSON_ERROR_NONE) {
                return $response;
            }
        }
    }
}