<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = "login";
$route['404_override'] = 'error_404';
$route['translate_uri_dashes'] = FALSE;


/*********** USER DEFINED ROUTES *******************/
$route['admin_changepassword'] = 'user/changeadminpassword';
$route['loginMe'] = 'login/loginMe';
$route['dashboard'] = 'user';
$route['changepassword'] = 'user/adminChangePassword';

$route['logout'] = 'user/logout';
$route['userListing'] = 'user/userListing';
$route['userListing/(:num)'] = "user/userListing/$1";
$route['addNew'] = "user/addNew";
$route['addNewUser'] = "user/addNewUser";
$route['editOld'] = "user/editOld";
$route['editOld/(:num)'] = "user/editOld/$1";
$route['editUser'] = "user/editUser";
$route['deleteUser'] = "user/deleteUser";

$route['userHold/(:num)'] = "user/userHold/$1";
$route['userUnhold/(:num)'] = "user/userUnhold/$1";

$route['holdUser'] = "user/holdUser";
$route['unholdUser'] = "user/unholdUser";
$route['user_by_date'] = 'user/userDetailsByDate';
$route['filterUser'] = 'user/filterUser';

$route['profile'] = "user/profile";
$route['profile/(:any)'] = "user/profile/$1";
$route['profileUpdate'] = "user/profileUpdate";
$route['profileUpdate/(:any)'] = "user/profileUpdate/$1";

$route['loadChangePass'] = "user/loadChangePass";
$route['changePassword'] = "user/changePassword";
$route['changePassword/(:any)'] = "user/changePassword/$1";
$route['pageNotFound'] = "user/pageNotFound";
$route['checkEmailExists'] = "user/checkEmailExists";
$route['login-history'] = "user/loginHistoy";
$route['login-history/(:num)'] = "user/loginHistoy/$1";
$route['login-history/(:num)/(:num)'] = "user/loginHistoy/$1/$2";

$route['forgotPassword'] = "login/forgotPassword";
$route['resetPasswordUser'] = "login/resetPasswordUser";
$route['resetPasswordConfirmUser'] = "login/resetPasswordConfirmUser";
$route['resetPasswordConfirmUser/(:any)'] = "login/resetPasswordConfirmUser/$1";
$route['resetPasswordConfirmUser/(:any)/(:any)'] = "login/resetPasswordConfirmUser/$1/$2";
$route['createPasswordUser'] = "login/createPasswordUser";

$route['roleListing'] = "roles/roleListing";
$route['roleListing/(:num)'] = "roles/roleListing/$1";
$route['roleListing/(:num)/(:num)'] = "roles/roleListing/$1/$2";

$route['artistListing'] = "artist/artistListing";
$route['artistListing/(:num)'] = "artist/artistListing/$1";
$route['artistListing/(:num)/(:num)'] = "artist/artistListing/$1/$2";
$route['deleteArtist'] = "artist/deleteArtist";
$route['holdArtist'] = "artist/holdArtist";
$route['unholdArtist'] = "artist/unholdArtist";
$route['artist_by_date'] = 'artist/artistDetailsByDate';
$route['filterArtist'] = 'artist/filterArtist';

$route['auditionsListing'] = "auditions/auditionsListing";
$route['auditionsListing/(:num)'] = "auditions/auditionsListing/$1";
$route['auditionsListing/(:num)/(:num)'] = "auditions/auditionsListing/$1/$2";
$route['deleteAudition'] = "auditions/deleteAudition";
$route['holdAudition'] = "auditions/holdAudition";
$route['unholdAudition'] = "auditions/unholdAudition";
$route['audition_by_date'] = 'auditions/auditionDetailsByDate';
$route['filterAudition'] = 'auditions/filterAudition';
$route['rewards/set-prize'] = "rewards/createPrices";
$route['rewards/save-prize']['post'] = "rewards/storePrices";
$route['rewards/current-video-listing'] = "rewards/currentWeek";
$route['rewards/all'] = "rewards/index";
$route['videoListing'] = "video/videoListing";
$route['videoListing/(:num)'] = "video/videoListing/$1";
$route['videoListing/(:num)/(:num)'] = "video/videoListing/$1/$2";
$route['deleteAll']['post'] = "video/deleteAll";
$route['deleteVideo'] = "video/deleteVideo";
$route['holdVideo'] = "video/holdVideo";
$route['unholdVideo'] = "video/unholdVideo";
$route['video_by_date'] = 'video/videoDetailsByDate';
$route['filterVideo'] = 'video/filterVideo';
$route['filterVideorewardvideo'] = 'rewards/filterVideorewardvideo';

$route['todaysVideoListing'] = "todaysvideo/todaysVideoListing";
$route['todayVideosListing/(:num)'] = "todaysvideo/todaysVideoListing/$1";
$route['todayVideosListing/(:num)/(:num)'] = "todaysvideo/todaysVideoListing/$1/$2";
$route['deletedAll']['post'] = "todaysvideo/deletedAll";
$route['deletedVideo'] = "todaysvideo/deletedVideo";
$route['holdsVideo'] = "todaysvideo/holdsVideo";
$route['unholdsVideo'] = "todaysvideo/unholdsVideo";
//$route['video_by_date'] = 'todaysvideo/videoDetailsByDate';
$route['filtersVideo'] = 'todaysvideo/filtersVideo';

$route['storiesListing'] = "stories/storiesListing";
$route['storiesListing/(:num)'] = "stories/storiesListing/$1";
$route['storiesListing/(:num)/(:num)'] = "stories/storiesListing/$1/$2";
$route['deleteAllStories']['post'] = "stories/deleteAllStories";
$route['deleteStories'] = "stories/deleteStories";
$route['holdStories'] = "stories/holdStories";
$route['unholdStories'] = "stories/unholdStories";
$route['stories_by_date'] = 'stories/storiesDetailsByDate';
$route['filterStories'] = 'stories/filterStories';

$route['storyListing'] = "story/storyListing";
$route['storyListing/(:num)'] = "story/storyListing/$1";
$route['storyListing/(:num)/(:num)'] = "story/storyListing/$1/$2";
$route['deleteAllStory']['post'] = "story/deleteAllStory";
$route['deleteStory'] = "story/deleteStory";
$route['holdStory'] = "story/holdStory";
$route['unholdStory'] = "story/unholdStory";
$route['story_by_date'] = 'story/storyDetailsByDate';
$route['filterStory'] = 'story/filterStory';

$route['photosListing'] = "photos/photosListing";
$route['photosListing/(:num)'] = "photos/photosListing/$1";
$route['photosListing/(:num)/(:num)'] = "photos/photosListing/$1/$2";
$route['deleteAllPhotos']['post'] = "photos/deleteAllPhotos";
$route['deletePhotos'] = "photos/deletePhotos";
$route['holdPhotos'] = "photos/holdPhotos";
$route['unholdPhotos'] = "photos/unholdPhotos";
$route['photo_by_date'] = 'photos/photoDetailsByDate';
$route['filterPhotos'] = 'photos/filterPhotos';

$route['photoListing'] = "photo/photoListing";
$route['photoListing/(:num)'] = "photo/photoListing/$1";
$route['photoListing/(:num)/(:num)'] = "photo/photoListing/$1/$2";
$route['deleteAllPhoto']['post'] = "photo/deleteAllPhoto";
$route['deletePhoto'] = "photo/deletePhoto";
$route['holdPhoto'] = "photo/holdPhoto";
$route['unholdPhoto'] = "photo/unholdPhoto";
$route['photo_by_date'] = 'photo/photoDetailsByDate';
$route['filterPhoto'] = 'photo/filterPhoto';

$route['productionListing'] = "production/productionListing";
$route['productionListing/(:num)'] = "production/productionListing/$1";
$route['productionListing/(:num)/(:num)'] = "production/productionListing/$1/$2";
$route['deleteProduction'] = "production/deleteProduction";
$route['holdProduction'] = "production/holdProduction";
$route['unholdProduction'] = "production/unholdProduction";
$route['production_by_date'] = 'production/productionDetailsByDate';
$route['filterProduction'] = 'production/filterProduction';


$route['transactionListing'] = 'transaction/transactionListing';
$route['transactionListing/(:num)'] = "transaction/transactionListing/$1";
$route['transactionListing/(:num)/(:num)'] = "transaction/transactionListing/$1/$2";

//$route['adsPhoto'] = 'adsphoto/adsphoto';
$route['adsPhotoListing'] = "adsPhoto/adsPhotoListing";
$route['adsPhotoListing/(:num)'] = "adsPhoto/adsPhotoListing/$1";

$route['adsVideoListing'] = "adsVideo/adsVideoListing";
$route['adsVideoListing/(:num)'] = "adsVideo/adsVideoListing/$1";
$route['transactionListing/(:num)/(:num)'] = "transaction/transactionListing/$1/$2";

$route['boostPhotoListing'] = "BoostPhoto/boostPhotoListing";
$route['boostPhotoListing/(:num)'] = "BoostPhoto/boostPhotoListing/$1";
$route['boostPhotoListing/(:num)/(:num)'] = "BoostPhoto/boostPhotoListing/$1/$2";

$route['boostVideoListing'] = "boostVideo/boostVideoListing";
$route['boostVideoListing/(:num)'] = "boostVideo/boostVideoListing/$1";
$route['boostVideoListing/(:num)/(:num)'] = "boostVideo/boostVideoListing/$1/$2";

$route['artistPremiumListing'] = "artistPremium/artistPremiumListing";
$route['artistPremiumListing/(:num)'] = "artistPremium/artistPremiumListing/$1";
$route['artistPremiumListing/(:num)/(:num)'] = "artistPremium/artistPremiumListing/$1/$2";

$route['artistThemePlan'] = "artistThemePlan/artistThemePlanListing";
$route['artistThemePlan/(:num)'] = "artistThemePlan/artistThemePlanListing/$1";
$route['artistThemePlan/(:num)/(:num)'] = "artistThemePlan/artistThemePlanListing/$1/$2";

$route['showpayouts'] = "Payout/showPayout";
$route['filterpayout'] = "Payout/filterpayout";
$route['unholdpayout'] = "Payout/unholdpayout";
$route['holdpayout'] = "Payout/holdpayout";
$route['approvepayout'] = "Payout/approvepayout";
$route['setvideoprice'] =  "Payout/setvideoprice";
$route['setphotoprice'] =  "Payout/setphotoprice";
$route['savevideoprice'] =  "Payout/savevideoprice";
$route['savevideoviewandprice'] =  "Payout/savevideoviewandprice";
$route['savephotoprice'] =  "Payout/savephotoprice";
$route['savephotoviewsandprice'] = "Payout/savephotoviewsandprice";
$route['complains'] = "user/complains";
$route['subcategories'] = "user/subcategories";
$route['addSubcategory'] = "user/addSubcategory";
$route['createSubcategory'] = "user/createSubcategory";
$route['editSubCategory/(:num)'] = "user/editSubCategory/$1";
$route['updateSubcategory/(:num)'] = "user/updateSubcategory/$1";
$route['ajaxupdateSubcategory'] = "user/ajaxupdateSubcategory";
$route['ajaxholdSubcategory'] = "user/ajaxholdSubcategory";



$route['holdSubcategory/(:num)'] = "user/holdSubcategory/$1";
$route['unholdSubcategory/(:num)'] = "user/unholdSubcategory/$1";
// $route['holdBoostVideo'] = "boostVideo/holdBoostVideo"; 
// $route['unHoldBoostVideo'] = "boostVideo/unHoldBoostVideo";

/* End of file routes.php */
/* Location: ./application/config/routes.php */
