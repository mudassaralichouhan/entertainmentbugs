<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Transaction_model extends CI_Model
{
    function transactionListingCount($searchText)
    {
        $this->db->select('transactions.id, transactions.type, amount, transactions.created_at, BaseTbl.name, BaseTbl.photo');
        $this->db->from('transactions');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = transactions.user_id','left');
         if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    
    function transactionListing($searchText, $page, $segment)
    {
        $this->db->select('transactions.id, transactions.type, amount, transactions.created_at, BaseTbl.name, BaseTbl.photo');
        $this->db->from('transactions');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = transactions.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by('id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();  
 
        return $result;
    }
}