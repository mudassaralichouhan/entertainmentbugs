<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Production_model extends CI_Model
{
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function productionListingCount($searchText)
    {
       /* $this->db->select('*');
        $this->db->from('artist_about as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.profile_name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
      //  $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        return $query->num_rows();
        */
        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.phone,BaseTbl.photo, BaseTbl.name,tbl.name as pname, tbl.language, tbl.profile_photo, tbl.multiple_category, tbl.production_house_date as pcreated_at, tbl.isDeleted as pisDeleted');
        $this->db->from('production_about as tbl');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = tbl.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */

    function productionListing($searchText, $page, $segment)
    {
       $this->db->select('BaseTbl.id, BaseTbl.email,BaseTbl.phone, BaseTbl.photo, BaseTbl.name,tbl.name as pname, tbl.id as pid, tbl.language, tbl.profile_photo, tbl.multiple_category, tbl.production_house_date as pcreated_at, tbl.isDeleted as pisDeleted');
        $this->db->from('production_about as tbl');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = tbl.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by('tbl.isDeleted', 0);
        $this->db->order_by('pid', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        $result = $query->result();        
        return $result;
    }

     function holdUnholdProduction($userId, $userInfo)
    {
        $this->db->where('id', $userId);
        $this->db->update('production_about', $userInfo);
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function deleteProduction($userId)
    {
        $this->db->where('id', $userId);
        $this->db->delete('production_about');
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

     function delete($id)
     {
      $this->db->where('id', $id);
      $this->db->delete('production_about');
     }
    
   public function getProductionDetailsByDate($fromDate,$toDate) {
       $this->db->select('BaseTbl.id, BaseTbl.email,BaseTbl.phone, BaseTbl.photo, BaseTbl.name,tbl.name as pname, tbl.id as pid, tbl.language, tbl.profile_photo, tbl.multiple_category, tbl.production_house_date as pcreated_at, tbl.isDeleted as pisDeleted');
        $this->db->from('production_about as tbl');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = tbl.user_id','left');
        $this->db->where('tbl.production_house_date >=', $fromDate);
        $this->db->where('tbl.production_house_date <=', $toDate);
        $this->db->order_by("tbl.id", "desc");
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getProductionDetails($test) {
        $this->db->select('BaseTbl.id, BaseTbl.email,BaseTbl.phone, BaseTbl.photo, BaseTbl.name,tbl.name as pname, tbl.id as pid, tbl.language, tbl.profile_photo, tbl.multiple_category, tbl.production_house_date as pcreated_at, tbl.isDeleted as pisDeleted');
        $this->db->from('production_about as tbl');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = tbl.user_id','left');
        $this->db->where('tbl.isDeleted', $test);
        $this->db->order_by("tbl.id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }

    public function getProductionDetails1() {
         $this->db->select('BaseTbl.id, BaseTbl.email,BaseTbl.phone, BaseTbl.photo, BaseTbl.name,tbl.name as pname, tbl.id as pid, tbl.language, tbl.profile_photo, tbl.multiple_category, tbl.production_house_date as pcreated_at, tbl.isDeleted as pisDeleted');
        $this->db->from('production_about as tbl');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = tbl.user_id','left');
       /// $this->db->where('isDeleted', $test);
        $this->db->order_by("tbl.id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }
}