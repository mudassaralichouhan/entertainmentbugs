<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Flag_model extends CI_Model
{
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function flagListingCount($searchText)
    {
      //   $this->db->select('*');
      //   $this->db->from('videos as BaseTbl');
      //   if(!empty($searchText)) {
      //       $likeCriteria = "(BaseTbl.profile_name  LIKE '%".$searchText."%')";
      //       $this->db->where($likeCriteria);
      //   }
      // //  $this->db->where('BaseTbl.isDeleted', 0);
      //   $query = $this->db->get();
        
      //   return $query->num_rows();

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Stories.title, Stories.about, Stories.created_at as screated_at, Stories.isDeleted as sisDeleted');
        $this->db->from('stories as Stories');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Stories.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(Stories.title  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('Stories.created_at >=', date('Y-m-d'));
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */

    function flagListing($searchText, $page, $segment)
    {
       /* $this->db->select('*');
        $this->db->from('videos as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.profile_name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
       // $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result; */

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,video_flags.id as sid, video_flags.title, video_flags.comment, video_flags.created_at as screated_at, video_flags.url');
        $this->db->from('video_flags as video_flags');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = video_flags.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(video_flags.title  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        //$this->db->where('BaseTbl.isDeleted', 0);
        // $this->db->where('BaseTbl.roleId !=', 1);
       // $this->db->where('video_flags.created_at >=', date('Y-m-d'));
        $this->db->order_by('sid', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        $result = $query->result();        
        return $result;
    }
    
    function getFlag(){
        $q = $this->db->get('video_flags');
        return $q->result();
    }
    
    function getUserDetails(){
        $q2 = $this->db->get('users');
        return $q2->result();
    }

     function holdUnholdStory($userId, $userInfo)
    {   $this->db->where('created_at >=', date('Y-m-d'));
        $this->db->where('id', $userId);
        $this->db->update('stories', $userInfo);
        $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function deleteStory($userId)
    {
        $this->db->where('id', $userId);
        $this->db->delete('stories');
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function delete($id)
     {
      $this->db->where('id', $id);
      $this->db->delete('stories');
     }
    
   public function getStoryDetailsByDate($fromDate,$toDate) {
       /* $this->db->where('created_at >=', $fromDate);
        $this->db->where('created_at <=', $toDate);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result_array();*/

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Stories.id as sid, Stories.title, Stories.about, Stories.created_at as screated_at, Stories.isDeleted as sisDeleted');
        $this->db->from('stories as Stories');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Stories.user_id','left');
        $this->db->where('Stories.created_at >=', $fromDate);
        $this->db->where('Stories.created_at <=', $toDate);
        $this->db->order_by('Stories.id', 'DESC');
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getStoryDetails($test) {
      /*  $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();*/

       $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Stories.id as sid, Stories.title, Stories.about, Stories.created_at as screated_at, Stories.isDeleted as sisDeleted');
        $this->db->from('stories as Stories');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Stories.user_id','left');
        $this->db->where('Stories.isDeleted', $test);
        $this->db->where('Stories.created_at >=', date('Y-m-d'));
        $this->db->order_by('Stories.id', 'DESC');
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }

    public function getStoryDetails1() {
      /* /// $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();*/

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Stories.id as sid, Stories.title, Stories.about, Stories.created_at as screated_at, Stories.isDeleted as sisDeleted');
        $this->db->from('stories as Stories');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Stories.user_id','left');
       // $this->db->where('isDeleted', $test);
        $this->db->where('Stories.created_at >=', date('Y-m-d'));
        $this->db->order_by('Stories.id', 'DESC');
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }
}