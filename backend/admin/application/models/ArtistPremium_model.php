<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ArtistPremium_model extends CI_Model
{
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
  function artistPremiumListingCount($searchText)
    {
        $this->db->select('BaseTbl.*, transactions.*');
        $this->db->from('artist_about as BaseTbl');
        $this->db->join('transactions', 'transactions.user_id = BaseTbl.user_id');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.profile_name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
           $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->like('transactions.type','premium-profile.gold');
        $this->db->or_like('transactions.type','premium-profile.silver');
       
        $query = $this->db->get();
       //$aa = $this->db->last_query(); print_r($aa); exit();
        return $query->num_rows();
    }
    
    function artistPremiumListing($searchText, $page, $segment)
    {
        $this->db->select('BaseTbl.*, transactions.*');
        $this->db->from('artist_about as BaseTbl');
        $this->db->join('transactions', 'transactions.user_id = BaseTbl.user_id');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.profile_name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
           $this->db->where('BaseTbl.isDeleted', 0);
          $this->db->like('transactions.type','premium-profile.gold');
        $this->db->or_like('transactions.type','premium-profile.silver');
     
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

     function holdUnholdArtistPremium($userId, $userInfo)
    {
        $this->db->where('id', $userId);
        $this->db->update('artist_about', $userInfo);
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function deleteArtistPremium($userId)
    {
        $this->db->where('id', $userId);
        $this->db->delete('artist_about');
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }
    
   public function getArtistPremiumDetailsByDate($tblName, $fromDate,$toDate) {
        $this->db->where('created_at >=', $fromDate);
        $this->db->where('created_at <=', $toDate);
        
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getArtistPremiumDetails($test) {
        $this->db->select('BaseTbl.*, transactions.*');
        $this->db->from('artist_about as BaseTbl');
          $this->db->where('BaseTbl.isDeleted', $test);
        $this->db->join('transactions', 'transactions.user_id = BaseTbl.user_id');
        $this->db->like('transactions.type','premium-profile.gold');
        $this->db->or_like('transactions.type','premium-profile.silver');
      
        $this->db->order_by("BaseTbl.id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result();
    }

/*
    public function getArtistPremiumDetails1($tblName) {
       /// $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }
    */
}