<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class TodaysVideo_model extends CI_Model
{
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function videoListingCount($searchText)
    {
        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Video.title, Video.video, Video.created_at as vcreated_at, Video.status as vstatus, Video.isDeleted as visDeleted');
        $this->db->from('videos as Video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Video.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(Video.title  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('Video.created_at >=', date('Y-m-d'));
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */

    function videoListing($searchText, $page, $segment)
    {
        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Video.id as vid,Video.title, Video.video, Video.created_at as vcreated_at, Video.status as vstatus,Video.isDeleted as visDeleted');
        $this->db->from('videos as Video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Video.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(Video.title  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        //$this->db->where('BaseTbl.isDeleted', 0);
        // $this->db->where('BaseTbl.roleId !=', 1);
        $this->db->where('Video.created_at >=', date('Y-m-d'));
        $this->db->order_by('vid', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        $result = $query->result();        
        return $result;
    }

     function holdUnholdVideo($userId, $userInfo)
    {   $this->db->where('created_at >=', date('Y-m-d'));
        $this->db->where('id', $userId);
        $this->db->update('videos', $userInfo);
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function deleteVideo($userId)
    {
        $this->db->where('id', $userId);
        $this->db->delete('videos');
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function delete($id)
     {
      $this->db->where('id', $id);
      $this->db->delete('videos');
     }
    
   public function getVideoDetailsByDate($fromDate,$toDate) {
       /* $this->db->where('created_at >=', $fromDate);
        $this->db->where('created_at <=', $toDate);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result_array();*/

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Video.id as vid,Video.title, Video.video, Video.created_at as vcreated_at, Video.status as vstatus,Video.isDeleted as visDeleted');
        $this->db->from('videos as Video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Video.user_id','left');
        $this->db->where('Video.created_at >=', $fromDate);
        $this->db->where('Video.created_at <=', $toDate);
        $this->db->order_by('Video.id', 'DESC');
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getVideoDetails($test) {
      /*  $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();*/

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Video.id as vid,Video.title, Video.video, Video.created_at as vcreated_at, Video.status as vstatus,Video.isDeleted as visDeleted');
        $this->db->from('videos as Video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Video.user_id','left');
        $this->db->where('Video.isDeleted', $test);
        $this->db->where('Video.created_at >=', date('Y-m-d'));
        $this->db->order_by('Video.id', 'DESC');
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }

    public function getVideoDetails1() {
      /* /// $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();*/

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Video.id as vid,Video.title, Video.video, Video.created_at as vcreated_at, Video.status as vstatus,Video.isDeleted as visDeleted');
        $this->db->from('videos as Video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Video.user_id','left');
       // $this->db->where('isDeleted', $test);
        $this->db->where('Video.created_at >=', date('Y-m-d'));
        $this->db->order_by('Video.id', 'DESC');
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }
}