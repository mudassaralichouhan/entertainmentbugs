<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Plan_model extends CI_Model
{
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function artistListingCount($searchText)
    {
        $this->db->select('*');
        $this->db->from('artist_about as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.profile_name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
      //  $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */

    function artistListing($searchText, $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('artist_about as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.profile_name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
       // $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

     function holdUnholdArtist($userId, $userInfo)
    {
        $this->db->where('id', $userId);
        $this->db->update('artist_about', $userInfo);
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function deleteArtist($userId)
    {
        $this->db->where('id', $userId);
        $this->db->delete('artist_about');
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }
    
   public function getArtistDetailsByDate($tblName, $fromDate,$toDate) {
        $this->db->where('created_at >=', $fromDate);
        $this->db->where('created_at <=', $toDate);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getArtistDetails($tblName, $test) {
        $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }

    public function getArtistDetails1($tblName) {
       /// $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }
}