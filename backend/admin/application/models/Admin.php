<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Model
{
    protected $table      = 'tbl_admin';
      function matchOldPassword($userId, $oldPassword)
    {
        $this->db->select('userId, password');
        $this->db->where('userId', $userId);        
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('tbl_admin');
        
        $user = $query->result();

        if(!empty($user)){
            if(verifyHashedPassword($oldPassword, $user[0]->password)){
                return $user;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }
    
    /**
     * This function is used to change users password
     * @param number $userId : This is user id
     * @param array $userInfo : This is user updation info
     */
    function changePassword($userId, $userInfo)
    {
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_admin', $userInfo);
        
        return $this->db->affected_rows();
    }
     function getUserInfoWithRole($userId)
    {
        $this->db->select('tbl_admin.userId, tbl_admin.email, tbl_admin.name, tbl_admin.mobile, tbl_admin.isAdmin, tbl_admin.roleId, Roles.role');
        $this->db->from('tbl_admin');
        $this->db->join('tbl_roles as Roles','Roles.roleId = tbl_admin.roleId');
        $this->db->where('tbl_admin.userId', $userId);
        $this->db->where('tbl_admin.isDeleted', 0);
        $query = $this->db->get();
        
        return $query->row();
    }
}