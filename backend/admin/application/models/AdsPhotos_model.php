<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class AdsPhotos_model extends CI_Model
{
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function adsPhotoListingCount($searchText)
    {
        $this->db->select('Photo.id, Photo.title, BaseTbl.name');
        $this->db->from('ads_presets as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
        $this->db->join('transactions', 'transactions.related_id = Photo.campaign_id');
        if(!empty($searchText)) {
            $likeCriteria = "(Photo.title  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('Photo.type =', 'image');
        $this->db->where('Photo.isDeleted', 0);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    function adsPhotoListing($searchText, $page, $segment)
    {
        $this->db->select('Photo.id, Photo.title, Photo.created_at, Photo.thumb, Photo.file, Photo.isDeleted, transactions.amount, Photo.user_id, BaseTbl.name, BaseTbl.email, BaseTbl.photo');
        $this->db->from('ads_presets as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
        $this->db->join('transactions', 'transactions.related_id = Photo.campaign_id');
        if(!empty($searchText)) {
            $likeCriteria = "(Photo.title  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('Photo.type =', 'image');
        $this->db->where('Photo.isDeleted', 0);
        $this->db->order_by('id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }

     function holdUnholdAdsPhoto($userId, $userInfo)
    {
        $this->db->where('id', $userId);
        $this->db->update('ads_presets', $userInfo);
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function deletePhoto($userId)
    {
        $this->db->where('id', $userId);
        $this->db->delete('ads_presets');
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function delete($id)
     {
      $this->db->where('id', $id);
      $this->db->delete('ads_presets');
     }
    
   public function getPhotoDetailsByDate($fromDate,$toDate) {
       /* $this->db->where('created_at >=', $fromDate);
        $this->db->where('created_at <=', $toDate);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result_array();*/

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Photo.id as pid,Photo.photo_title, Photo.images, Photo.created_at as pcreated_at, Photo.isDeleted as pisDeleted');
        $this->db->from('upload_photos as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
        $this->db->where('Photo.created_at >=', $fromDate);
        $this->db->where('Photo.created_at <=', $toDate);
        $this->db->order_by('Photo.id', 'DESC');
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getAdsPhotoDetails($test) {
        $this->db->select('Photo.id, Photo.title, Photo.created_at, Photo.thumb, Photo.file, Photo.isDeleted, transactions.amount, BaseTbl.name');
        $this->db->from('ads_presets as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
        $this->db->join('transactions', 'transactions.related_id = Photo.campaign_id');
        $this->db->where('Photo.isDeleted', $test);
        $this->db->order_by('Photo.id', 'DESC');
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getAdsPhotoDetails1() {
      /* /// $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();*/

       $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Photo.id as pid,Photo.photo_title, Photo.images, Photo.created_at as pcreated_at, Photo.isDeleted as pisDeleted');
        $this->db->from('upload_photos as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
       // $this->db->where('isDeleted', $test);
        $this->db->order_by('Photo.id', 'DESC');
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }
}