<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Audition_model extends CI_Model
{
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function auditionsListingCount($searchText)
    {
        $this->db->select('*');
        $this->db->from('post_audtion as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.post_by_name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
      //  $this->db->where('BaseTbl.isDeleted', 0);
        $query = $this->db->get();
        
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */

    function auditionsListing($searchText, $page, $segment)
    {
        $this->db->select('*');
        $this->db->from('post_audtion as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.post_by_name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

     function holdUnholdAudition($userId, $userInfo)
    {
        $this->db->where('id', $userId);
        $this->db->update('post_audtion', $userInfo);
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function deleteAudition($userId)
    {
        $this->db->where('id', $userId);
        $this->db->delete('post_audtion');
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }
    
   public function getAuditionDetailsByDate($tblName, $fromDate,$toDate) {
        $this->db->where('posted_date >=', $fromDate);
        $this->db->where('posted_date <=', $toDate);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getAuditionDetails($tblName, $test) {
        $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }

    public function getAuditionDetails1($tblName) {
       /// $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }
}