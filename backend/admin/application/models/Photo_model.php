<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Photo_model extends CI_Model
{
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @return number $count : This is row count
     */
    function photoListingCount($searchText)
    {
      //   $this->db->select('*');
      //   $this->db->from('videos as BaseTbl');
      //   if(!empty($searchText)) {
      //       $likeCriteria = "(BaseTbl.profile_name  LIKE '%".$searchText."%')";
      //       $this->db->where($likeCriteria);
      //   }
      // //  $this->db->where('BaseTbl.isDeleted', 0);
      //   $query = $this->db->get();
        
      //   return $query->num_rows();

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Photo.photo_title, Photo.images, Photo.created_at as pcreated_at, Photo.isDeleted as pisDeleted');
        $this->db->from('upload_photos as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(Photo.photo_title  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('Photo.created_at >=', date('Y-m-d'));
        $query = $this->db->get();
        return $query->num_rows();
    }
    
    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */

    function photoListing($searchText, $page, $segment)
    {
       /* $this->db->select('*');
        $this->db->from('videos as BaseTbl');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.profile_name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
       // $this->db->where('BaseTbl.isDeleted', 0);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result; */
        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Photo.id as pid,Photo.photo_title, Photo.images, Photo.created_at as pcreated_at, Photo.isDeleted as pisDeleted');
        $this->db->from('upload_photos as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(Photo.photo_title  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        //$this->db->where('BaseTbl.isDeleted', 0);
        // $this->db->where('BaseTbl.roleId !=', 1);
        $this->db->where('Photo.created_at >=', date('Y-m-d'));
        $this->db->order_by('pid', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        $result = $query->result();        
        return $result;
    }

     function holdUnholdPhoto($userId, $userInfo)
    {
        $this->db->where('created_at >=', date('Y-m-d'));
        $this->db->where('id', $userId);
        $this->db->update('upload_photos', $userInfo);
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function deletePhoto($userId)
    {
        $this->db->where('id', $userId);
        $this->db->delete('upload_photos');
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function delete($id)
     {
      $this->db->where('id', $id);
      $this->db->delete('upload_photos');
     }
    
   public function getPhotoDetailsByDate($fromDate,$toDate) {
       /* $this->db->where('created_at >=', $fromDate);
        $this->db->where('created_at <=', $toDate);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result_array();*/

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Photo.id as pid,Photo.photo_title, Photo.images, Photo.created_at as pcreated_at, Photo.isDeleted as pisDeleted');
        $this->db->from('upload_photos as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
        $this->db->where('Photo.created_at >=', $fromDate);
        $this->db->where('Photo.created_at <=', $toDate);
        $this->db->order_by('Photo.id', 'DESC');
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getPhotoDetails($test) {
      /*  $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();*/

       $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Photo.id as pid,Photo.photo_title, Photo.images, Photo.created_at as pcreated_at, Photo.isDeleted as pisDeleted');
        $this->db->from('upload_photos as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
        $this->db->where('Photo.isDeleted', $test);
        $this->db->where('Photo.created_at >=', date('Y-m-d'));
        $this->db->order_by('Photo.id', 'DESC');
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }

    public function getPhotoDetails1() {
      /* /// $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();*/

       $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Photo.id as pid,Photo.photo_title, Photo.images, Photo.created_at as pcreated_at, Photo.isDeleted as pisDeleted');
        $this->db->from('upload_photos as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
       // $this->db->where('isDeleted', $test);
        $this->db->where('Photo.created_at >=', date('Y-m-d'));
        $this->db->order_by('Photo.id', 'DESC');
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }
}