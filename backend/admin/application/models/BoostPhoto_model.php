<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class BoostPhoto_model extends CI_Model
{
 
    function boostPhotoListingCount($searchText)
    {
        $this->db->select('photo.id, photo.days, BaseTbl.name');
        $this->db->from('promote_posts as photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = photo.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('photo.post_type =', 'photo');
        $query = $this->db->get();
        return $query->num_rows();
    }
    

    function boostPhotoListing($searchText, $page, $segment)
    {
        $this->db->select('photo.id, photo.days, photo.isDeleted, photo.created_at, photo.amount, BaseTbl.name, BaseTbl.photo');
        $this->db->from('promote_posts as photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = photo.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('photo.post_type =', 'photo');
        $this->db->where('photo.isDeleted', 0);
        $this->db->order_by('id', 'DESC');
        $this->db->limit($page, $segment);
       // $aa = $this->db->last_query(); print_r($aa); exit();
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }

    function holdUnholdBoostPhoto($userId, $userInfo)
    {
        $this->db->where('id', $userId);
        $this->db->update('promote_posts', $userInfo);
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

   public function getBoostPhotoDetails($test) {
       
        $this->db->select('photo.id, photo.days, photo.isDeleted, photo.created_at, photo.amount, BaseTbl.name,BaseTbl.photo');
        $this->db->from('promote_posts as photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = photo.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('photo.post_type =', 'photo');
        $this->db->where('photo.isDeleted', $test);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        $result = $query->result_array();        
        return $result;
    }
    
}