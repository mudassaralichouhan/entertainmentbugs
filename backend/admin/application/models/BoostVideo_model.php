<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class BoostVideo_model extends CI_Model
{
  
    function boostVideoListingCount($searchText)
    {
        $this->db->select('video.id, video.days, video.amount, BaseTbl.name');
        $this->db->from('promote_posts as video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = video.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('video.post_type =', 'video');
        $this->db->where('video.isDeleted', 0);
        $query = $this->db->get();
        return $query->num_rows();
    }
    
  
    function boostVideoListing($searchText, $page, $segment)
    {
        $this->db->select('video.id, video.days, video.created_at, video.isDeleted, video.amount, BaseTbl.name, BaseTbl.photo');
        $this->db->from('promote_posts as video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = video.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('video.post_type =', 'video');
        $this->db->where('video.isDeleted', 0);
        $this->db->order_by('id', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        $result = $query->result(); 
        
        return $result;
    }

     function holdUnholdBoostVideo($userId, $userInfo)
    {
        $this->db->where('id', $userId);
        $this->db->update('promote_posts', $userInfo);
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }
    
    public function getBoostVideoDetails($test) {
      
        $this->db->select('video.id, video.days, video.created_at, video.isDeleted, video.amount, BaseTbl.name,BaseTbl.photo');
        $this->db->from('promote_posts as video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = video.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('video.post_type =', 'video');
        $this->db->where('video.isDeleted', $test);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        $result = $query->result_array(); 
        return $result;
    }
}