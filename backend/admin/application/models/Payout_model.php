<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Payout_model extends CI_Model
{
     function payoutListingCount($searchText=null)
    {
        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,payouts.amount, payouts.created_at as pcreated_at, payouts.is_pending as pisPending');
        $this->db->from('payouts');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = payouts.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
         $this->db->where('payouts.status',0);
        $this->db->where('payouts.is_pending', 0);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function payoutListing($searchText=null)
    {
        $this->db->select('BaseTbl.id, BaseTbl.email, wallet.balance ,BaseTbl.name,payouts.id as pid ,payouts.status, payouts.amount, payouts.created_at as pcreated_at, payouts.is_pending as pisPending');
        $this->db->from('payouts');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = payouts.user_id','left');
        $this->db->join('wallet', 'BaseTbl.id = wallet.user_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
            $this->db->where('payouts.status',0);
           $this->db->where('payouts.is_pending', 0);
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    public function getPayoutDetails($test){
        $this->db->select('BaseTbl.id, BaseTbl.email, wallet.balance ,BaseTbl.name,payouts.id as pid ,payouts.status, payouts.amount, payouts.created_at as pcreated_at, payouts.is_pending as pisPending');
        $this->db->from('payouts');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = payouts.user_id','left');
        $this->db->join('wallet', 'BaseTbl.id = wallet.user_id','left');
        $this->db->where('payouts.status',$test);
        $this->db->where('payouts.is_pending', 0);
        $this->db->order_by('payouts.id', 'DESC');
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    public function getPayoutDetails1(){
        $this->db->select('BaseTbl.id, BaseTbl.email, wallet.balance ,BaseTbl.name,payouts.id as pid ,payouts.status, payouts.amount, payouts.created_at as pcreated_at, payouts.is_pending as pisPending');
        $this->db->from('payouts');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = payouts.user_id','left');
        $this->db->join('wallet', 'BaseTbl.id = wallet.user_id','left');
        $this->db->where('payouts.is_pending', 1);
        $this->db->order_by('payouts.id', 'DESC');
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }
    public function holdUnholdpayout($userId,$userInfo){ 
        $this->db->where('id', $userId);
        $this->db->update('payouts', $userInfo);
        //$aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }
    public function approvepayout($userId,$userInfo){ 
        $this->db->where('id', $userId);
        $this->db->update('payouts', $userInfo);
        //$aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }
}