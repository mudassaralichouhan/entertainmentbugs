<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class AdsVideo_model extends CI_Model
{
  
    function adsVideoListingCount($searchText)
    {
        $this->db->select('video.id, video.title, video.isDeleted, BaseTbl.name');
        $this->db->from('ads_presets as video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = video.user_id','left');
        $this->db->join('transactions', 'transactions.related_id = video.campaign_id');
        if(!empty($searchText)) {
            $likeCriteria = "(video.title  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('video.type =', 'video');
        $query = $this->db->get();
        return $query->num_rows();
    }
    

    function adsVideoListing($searchText, $page, $segment)
    {
         $this->db->select('video.id, video.title, video.created_at, video.isDeleted, video.file, transactions.amount, BaseTbl.name, BaseTbl.photo');
        $this->db->from('ads_presets as video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = video.user_id','left');
        $this->db->join('transactions', 'transactions.related_id = video.campaign_id');
        if(!empty($searchText)) {
            $likeCriteria = "(video.title  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('video.type =', 'video');
        $this->db->order_by('id', 'DESC');
        $this->db->limit($page, $segment);
        //$aa = $this->db->last_query(); print_r($aa); exit();
        $query = $this->db->get();
        $result = $query->result();        
        return $result;
    }



     function holdUnholdAdsVideo($userId, $userInfo)
    {
        $this->db->where('id', $userId);
        $this->db->update('ads_presets', $userInfo);
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    } 
   
    function deleteVideo($userId)
    {
        $this->db->where('id', $userId);
        $this->db->delete('ads_presets');
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function delete($id)
     {
      $this->db->where('id', $id);
      $this->db->delete('ads_presets');
     }
     
     
    public function getAdsVideoDetails($test) {
        $this->db->select('video.id, video.title, video.created_at, video.isDeleted, video.file, transactions.amount, BaseTbl.name');
        $this->db->from('ads_presets as video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = video.user_id','left');
        $this->db->join('transactions', 'transactions.related_id = video.campaign_id');
        if(!empty($searchText)) {
            $likeCriteria = "(video.title  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('video.type =', 'video');
        $this->db->where('video.isDeleted', $test);
        $this->db->order_by('video.id', 'DESC');
        $query = $this->db->get();
        $result = $query->result_array();        
        return $result;
    }
     
    
   public function getPhotoDetailsByDate($fromDate,$toDate) {
       /* $this->db->where('created_at >=', $fromDate);
        $this->db->where('created_at <=', $toDate);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
        return $data->result_array();*/

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Photo.id as pid,Photo.photo_title, Photo.images, Photo.created_at as pcreated_at, Photo.isDeleted as pisDeleted');
        $this->db->from('upload_photos as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
        $this->db->where('Photo.created_at >=', $fromDate);
        $this->db->where('Photo.created_at <=', $toDate);
        $this->db->order_by('Photo.id', 'DESC');
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getPhotoDetails($test) {
      /*  $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();*/

       $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Photo.id as pid,Photo.photo_title, Photo.images, Photo.created_at as pcreated_at, Photo.isDeleted as pisDeleted');
        $this->db->from('upload_photos as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
        $this->db->where('Photo.isDeleted', $test);
        $this->db->order_by('Photo.id', 'DESC');
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }

    public function getPhotoDetails1() {
      /* /// $this->db->where('isDeleted', $test);
        $this->db->from($tblName);
        $this->db->order_by("id", "desc");
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();*/

       $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Photo.id as pid,Photo.photo_title, Photo.images, Photo.created_at as pcreated_at, Photo.isDeleted as pisDeleted');
        $this->db->from('upload_photos as Photo');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Photo.user_id','left');
       // $this->db->where('isDeleted', $test);
        $this->db->order_by('Photo.id', 'DESC');
        $data = $this->db->get();
       // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result_array();
    }
    public function savevideoviewandprice($datas){
        
        $this->db->where('id',1);
        $data = $this->db->get('video_views_click');
       // $aa = $this->db->last_query(); print_r($datas); exit();
        if($data->num_rows()>0){
            $this->db->where('id',1);
            $this->db->update('video_views_click',$datas);
        }else{
            $this->db->insert('video_views_click',$datas);
        }
    }
     public function savephotoviewandprice($datas,$data2){
        
        $this->db->where('id',1);
        $data = $this->db->get('photo_views_click');
       // $aa = $this->db->last_query(); print_r($datas); exit();
        if($data->num_rows()>0){
            $this->db->where('id',1);
            $this->db->update('photo_views_click',$datas);
        }else{
            $this->db->insert('photo_views_click',$datas);
        }
        //  $this->db->get('promote_posts');
        //  $this->db->where('post_type','photo');
        //  $this->db->or_where('post_type','story');
        //  $this->db->update('promote_posts',$data2);
        
    }
    public function fetchvideo_views_clickdata(){
       $data = $this->db->get('video_views_click');
       return $data->row();
    }
    public function fetchphoto_views_clickdata(){
   $data = $this->db->get('photo_views_click');
   return $data->row();
    }
    public function save_video_price($array,$id){
         $this->db->where('id',$id);
        $data = $this->db->get('video_price');
         if($data->num_rows()>0){
            $this->db->where('id',$id);
            $this->db->update('video_price',$array);
        }else{
            $this->db->insert('video_price',$array);
        }
         
    }
     public function save_photo_price($array,$id){
         $this->db->where('id',$id);
        $data = $this->db->get('photo_price');
         if($data->num_rows()>0){
            $this->db->where('id',$id);
            $this->db->update('photo_price',$array);
        }else{
            $this->db->insert('photo_price',$array);
        }
         
    }
     public function fetchvideodata(){
       $data = $this->db->get('video_price');
       return $data->result();
    }
     public function fetchphotodata(){
       $data = $this->db->get('photo_price');
       return $data->result();
    }
    
}