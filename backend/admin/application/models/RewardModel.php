<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class RewardModel extends CI_Model
{
    function videoListingCount($searchText)
    {
        // $days = date('d');

        // if($days >= 1 || $days <= 7){
        //     $day1 = date('Y').'-'.date('m').'-'.'01';
        //     $day2 = date('Y').'-'.date('m').'-'.'07';
        // }

        // if($days >= 8 || $days <= 14){
        //     $day1 = date('Y').'-'.date('m').'-'.'08';
        //     $day2 = date('Y').'-'.date('m').'-'.'14';
        // }

        // if($days >= 15 || $days <= 21){
        //     $day1 = date('Y').'-'.date('m').'-'.'15';
        //     $day2 = date('Y').'-'.date('m').'-'.'21';
        // }

        // if($days >= 22 || $days <= 30){
        //     $day1 = date('Y').'-'.date('m').'-'.'22';
        //     $day2 = date('Y').'-'.date('m').'-'.'30';
        // }

        $today = new DateTime('now', new DateTimeZone('UTC'));
        $day_of_week = $today->format('w');
        $today->modify('- ' . (($day_of_week - 1 + 7) % 6) . 'days');
        $sunday = clone $today;
        $sunday->modify('+ 7 days');

        $this->db->where('videos.created_at BETWEEN "' . $today->format('Y-m-d') . '" AND "' . $sunday->format('Y-m-d') . '" ');


        $this->db->where('videos.isDeleted', 0);
        $this->db->where('videos.isRewardVideo', 1);
        $this->db->from('videos');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = videos.user_id', 'left');
        $this->db->order_by("videos.id", "desc");;
        $query = $this->db->get();

        //     $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Video.title, Video.video, Video.created_at as vcreated_at, Video.status as vstatus, Video.isDeleted as visDeleted');
        // //    $this->db->where('created_at');
        //     $this->db->from('videos as Video');
        //     $this->db->join('users as BaseTbl', 'BaseTbl.id = Video.user_id','left');
        //     if(!empty($searchText)) {
        //         $likeCriteria = "(Video.title  LIKE '%".$searchText."%')";
        //         $this->db->where($likeCriteria);
        //     }
        //     $this->db->where('Video.isDeleted', 0);
        //     $this->db->where('Video.isRewardVideo', 1);
        //     $query = $this->db->get();

        return $query->num_rows();
    }

    function videoThumb($searchText, $page, $segment, $test = null)
    {

        $this->db->select('BaseTbl.video_id, BaseTbl.thumb, Video.id as vid,Video.title, Video.video, Video.created_at as vcreated_at, Video.status as vstatus,Video.isDeleted as visDeleted');
        $this->db->from('videos as Video');
        $this->db->join('video_thumbs as BaseTbl', 'BaseTbl.video_id = Video.video_thumb_id', 'left');
        if (!empty($searchText)) {
            $likeCriteria = "(Video.title  LIKE '%" . $searchText . "%')";
            $this->db->where($likeCriteria);
        }
        if ($test == 1) {
            //print_r($test); exit();
            $this->db->where('Video.isDeleted', $test);
        }
        $today = new DateTime('now', new DateTimeZone('UTC'));
        $day_of_week = $today->format('w');
        $today->modify('- ' . (($day_of_week - 1 + 7) % 6) . 'days');
        $sunday = clone $today;
        $sunday->modify('+ 7 days');

        $this->db->where('Video.created_at BETWEEN "' . $today->format('Y-m-d') . '" AND "' . $sunday->format('Y-m-d') . '" ');
        $this->db->where('Video.isRewardVideo', 1);
        $this->db->order_by('vid', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        //$aa = $this->db->last_query(); print_r($aa); exit();
        $result = $query->result();
        return $result;
    }

    /**
     * This function is used to get the booking listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */

    function videoListing($searchText, $page, $segment)
    {

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Video.id as vid,Video.title, Video.video, Video.created_at as vcreated_at, Video.status as vstatus,Video.isDeleted as visDeleted');
        // $this->db->where('created_at', '2023-04-22');
        $this->db->from('videos as Video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Video.user_id', 'left');
        if (!empty($searchText)) {
            $likeCriteria = "(Video.title  LIKE '%" . $searchText . "%')";
            $this->db->where($likeCriteria);
        }
        $today = new DateTime('now', new DateTimeZone('UTC'));
        $day_of_week = $today->format('w');
        $today->modify('- ' . (($day_of_week - 1 + 7) % 6) . 'days');
        $sunday = clone $today;
        $sunday->modify('+ 7 days');

        $this->db->where('Video.created_at BETWEEN "' . $today->format('Y-m-d') . '" AND "' . $sunday->format('Y-m-d') . '" ');
        $this->db->where('Video.isDeleted', 0);
        $this->db->where('Video.isRewardVideo', 1);
        // $this->db->where('BaseTbl.roleId !=', 1);
        $this->db->order_by('vid', 'DESC');
        $this->db->limit($page, $segment);
        $query = $this->db->get();
        //$aa = $this->db->last_query(); print_r($aa); exit();
        $result = $query->result();
        return $result;
    }

    function Todayrewards()
    {
        $this->db->where('created_at', date('Y-m-d'));
        $this->db->where('isRewardVideo', 1);
        $this->db->from('videos');
        $this->db->order_by("id", "desc");
        $q2 = $this->db->get();
        return $q2->result_array();
    }

    function holdUnholdVideo($userId, $userInfo)
    {
        $this->db->where('videos.id', $userId);
        $this->db->update('videos', $userInfo);
        $aa = $this->db->last_query();
        print_r($aa);
        exit();
        return $this->db->affected_rows();
    }

    function deleteVideo($userId)
    {
        $this->db->where('id', $userId);
        $this->db->delete('videos');
        // $aa = $this->db->last_query(); print_r($aa); exit();
        return $this->db->affected_rows();
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('videos');
    }

    public function getVideoDetailsByDate($fromDate, $toDate)
    {
        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Video.id as vid,Video.title, Video.video, Video.created_at as vcreated_at, Video.status as vstatus,Video.isDeleted as visDeleted');
        $this->db->from('videos as Video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Video.user_id', 'left');
        $this->db->where('Video.created_at >=', $fromDate);
        $this->db->where('Video.created_at <=', $toDate);
        $this->db->order_by('Video.id', 'DESC');
        $data = $this->db->get();
        return $data->result_array();
    }

    public function getVideoDetails($test)
    {
        /*  $this->db->where('isDeleted', $test);
          $this->db->from($tblName);
          $this->db->order_by("id", "desc");
          $data = $this->db->get();

          return $data->result_array();*/

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Video.id as vid,Video.title, Video.video, Video.created_at as vcreated_at, Video.status as vstatus,Video.isDeleted as visDeleted');

        $this->db->from('videos as Video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Video.user_id', 'left');
        $today = new DateTime('now', new DateTimeZone('UTC'));
        $day_of_week = $today->format('w');
        $today->modify('- ' . (($day_of_week - 1 + 7) % 6) . 'days');
        $sunday = clone $today;
        $sunday->modify('+ 7 days');

        $this->db->where('Video.created_at BETWEEN "' . $today->format('Y-m-d H:i:s') . '" AND "' . $sunday->format('Y-m-d H:i:s') . '" ');
        $this->db->where('Video.isRewardVideo', 1);
        $this->db->where('Video.isDeleted', $test);

        $this->db->order_by('Video.id', 'DESC');
        $data = $this->db->get();

        return $data->result();

    }

    public function getVideoDetails1()
    {
        /* /// $this->db->where('isDeleted', $test);
          $this->db->from($tblName);
          $this->db->order_by("id", "desc");
          $data = $this->db->get();
         // $aa = $this->db->last_query(); print_r($aa); exit();
          return $data->result_array();*/

        $this->db->select('BaseTbl.id, BaseTbl.email, BaseTbl.name,Video.id as vid,Video.title, Video.video, Video.created_at as vcreated_at, Video.status as vstatus,Video.isDeleted as visDeleted');
        $this->db->from('videos as Video');
        $this->db->join('users as BaseTbl', 'BaseTbl.id = Video.user_id', 'left');
        // $this->db->where('isDeleted', $test);
        $today = new DateTime('now', new DateTimeZone('UTC'));
        $day_of_week = $today->format('w');
        $today->modify('- ' . (($day_of_week - 1 + 7) % 6) . 'days');
        $sunday = clone $today;
        $sunday->modify('+ 7 days');

        $this->db->where('Video.created_at BETWEEN "' . $today->format('Y-m-d') . '" AND "' . $sunday->format('Y-m-d') . '" ');
        $this->db->where('Video.isRewardVideo', 1);
        $this->db->order_by('Video.id', 'DESC');
        $data = $this->db->get();
        // $aa = $this->db->last_query(); print_r($aa); exit();
        return $data->result();
    }

    public function getCurrentWeekPrice()
    {
        // Calculate the date of the previous Sunday
        $previousSunday = date('Y-m-d', strtotime('last Sunday'));

        // Calculate the date of the coming Sunday
        $nextSunday = date('Y-m-d', strtotime('next Sunday'));

        // Query the database to get records between previous and coming Sunday
        return $this->db->select('*')
            ->from('reward_prizes')
            ->where('created_at >=', $previousSunday)
            ->where('created_at <=', $nextSunday)
            ->order_by('id', 'desc')
            ->limit(1)
            ->get();
    }
}