
 <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic column reorder -->
                <div class="panel panel-flat">
                 
                    <div class="panel-heading">
                        <h5 class="panel-title">Videos<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li> 
                            </ul>
                        </div>
                    </div>
 

          <div class="row">
            <div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                     <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>


            <!-- Tasks options -->
                    <div class="navbar navbar-default navbar-xs navbar-component" style="padding-right:0px;">
                    <ul class="nav navbar-nav no-border visible-xs-block">
                        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
                    </ul>

                    <div class="navbar-collapse collapse" id="navbar-filter" style="width:70%; float:left">
                    <!-- <p class="navbar-text">Filter:</p> -->
                    <div class="col-md-3">
                    <select id="ddlDist" name="ddlDist" onchange="popDistrictData();" class="nav navbar-nav form-control">
                        <option value="">--Select--</option>
                        <option value="2">Show All</option>
                        <!--<option value="0">UnHold</option>-->
                        <option value="1">Hold</option>
                    </select>
                    <input type="hidden" name="hidden_country" id="hidden_country" />
                    </div>
                       
                   <!-- <p class="navbar-text"><strong>From:</strong></p>
                    <div class="col-md-3">
                      <div class="form-group">
                        <input type="date" id="from_date" required name="from_date" class="form-control" id="exampleInputPassword1" placeholder="Enter Date">
                        <p id="mbl" style="color:red"></p>
                      </div>
                    </div>
                    <p class="navbar-text"><strong>To:</strong></p>
                    <div class="col-md-3">
                      <div class="form-group">
                       
                        <input type="date" id="to_date" required name="to_date" class="form-control" id="exampleInputPassword1" placeholder="Enter Date">
                        <p id="mbl" style="color:red"></p>
                      </div>
                    </div> -->

                      <!-- <button style="margin-bottom: 10px" class="btn bg-success btn-md delete_all" data-url="/deleteAll">Delete All</button> -->
 
                    </div>
    
                    <div class="panel-body" style="width:30%; float:right;padding:0px;">
                        <form action="<?php echo base_url() ?>todaysvideo/todaysVideoListing" method="POST" class="main-search" id="searchList">
                            <div class="input-group content-group" style="margin-bottom:0px !Important">
                                <div class="has-feedback has-feedback-left">
                                    <input type="text" class="form-control input-xlg" name="searchText" value="<?php echo $searchText; ?>" placeholder="Search">
                                    <div class="form-control-feedback">
                                        <i class="icon-search4 text-muted text-size-base"></i>
                                    </div>
                                </div>

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-primary btn-xlg">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
                <!-- /tasks options -->

                <div class="box-body table-responsive no-padding">
                <table class="table datatable-reorder">
                <!-- <table class="table table-striped media-library table-lg"> -->
                        <thead>
                        <tr>    
                                <th width="5%"><button type="button" name="delete_all" id="delete_all" class="btn btn-danger btn-xs">Delete</button></th>

                                <th>Preview</th> 
                                <th>Title</th>  
                                <th>User</th> 
                                <th>Date</th>
                                <th>Status</th>
                                <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="inwTblBody">
                            <?php
                            if(!empty($videoRecords))
                            {
                                foreach($videoRecords as $record)
                                {
                                    // $uid = $record->user_id;
                                    // $sql = "select name, email, phone from `users` where id = $uid";
                                    // $query = $this->db->query($sql);
                                    // $ss = $query->result(); 
                                    //  foreach($ss as $row){
                                    //  $uname =  $row->name;
                                    //  $email =  $row->email;
                                    //  $phone =  $row->phone;
                                    //  }

                            ?>
                        <tr>
                        <!-- <td><input type="checkbox" class="sub_chk" data-id="<?php //echo $record->vid; ?>"></td> -->
                        <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record->vid; ?>" /></td>
                                <td style="font-weight:bold;">
                                <!-- <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php //echo $record->profile_photo;?>"><?php //echo $uname ?> -->

                                <video width="150" height="100" controls autoplay>
                                  <source src="<?php echo $record->video;?>" type="video/mp4">
                                  Sorry, your browser doesn't support the video element.
                                </video>
                            </td>
  
                                <td><?php echo $record->title ?></td> 
                                <td><?php echo $record->name ?></td>               
                                <td><?php echo date("d-m-Y", strtotime($record->vcreated_at)) ?></td>
                                 <td><?php echo $record->vstatus ?></td> 
                                <td style="position:relative">  
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                                  Option <span class="caret"></span></a>
                                    <ul class="dropdown-menu" style="left: -100px;"> 
                                    <li><?php if($record->visDeleted == 0){ ?>
                                        <a class="holdVideo"  href="<?php echo base_url() ;?>todaysVideoListing" data-userid="<?php echo $record->vid; ?>">Hold</a></li>
                                            <?php }elseif($record->visDeleted == 1){ ?>
                                        <a class="unholdVideo" href="<?php echo base_url() ;?>todaysVideoListing" data-userid="<?php echo $record->vid; ?>">UnHold</a>
                                    </li>
                                            <?php } ?>
                                    <li><a class="deleteVideo"  href="<?php echo base_url() ;?>todaysVideoListing" data-id="<?php echo $record->vid; ?>">Delete</a></li>
                                </ul> 
                                </td>     
                            </tr>
                            <?php
                        }
                    } else if(empty($videoRecords)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
                  <?php  }
                    ?>
                        </tbody>
                     
                    </table>
                </div>

                 <div id="hidden_div" class="box-footer clearfix">
                    <?php  
                    echo $this->pagination->create_links(); ?>
                </div>
        </div>
        <!-- Basic column reorder -->
        </div>
        <!-- Main content -->

         </div>
  </div>
<style>
.removeRow
{
 background-color: #FF0000;
 color:#FFFFFF;
}
</style>

  <script type="text/javascript">
    document.getElementById('ddlDist').addEventListener('change', function () {
    var style = this.value == 5 ? 'block' : 'none';
    //alert(style);
    document.getElementById('hidden_div').style.display = style;
    });

    document.getElementById('to_date').addEventListener('change', function () {
    var style = this.value == 5 ? 'block' : 'none';
    //alert(style);
    document.getElementById('hidden_div').style.display = style;
    });
  </script>

<!-- <script type="text/javascript" src="<?php //echo base_url(); ?>assets/js/common.js" charset="utf-8"></script> -->
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);

            jQuery("#searchList").attr("action", baseURL + "todaysVideoListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>

<script type="text/javascript">
    function popDistrictData(){
        var dist_id = document.getElementById("ddlDist").value;
       // alert(dist_id);
        $.ajax({
            type: "POST",
            url:"<?php echo site_url('filtersVideo');?>",
                data:{"dist_id" : dist_id},
                success: function (data) {
                    // alert(data);
                  $('#inwTblBody').html(data);
                }  
            });

        // $.ajax({
        //     type: "POST",
        //     url: "<?=base_url()?>artist/filterArtist/"+dist_id,
        //     dataType: "html",
        //     success: function(html){
        //         $("#inwTblBody").html(html);
        //     }
        // });
    }
</script>

<script type="text/javascript">
    $(document).on('change','#to_date',function(){
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        if(from_date=="" || to_date==""){
            die();
        }else{
            $.ajax({
            type: "POST",
            url:"<?php echo site_url('video_by_date');?>",
                data:{"from_date" : from_date,"to_date":to_date},
                success: function (data) {
                    // alert(data);
                  $('#inwTblBody').html(data);
                }  
            });
        }
    });
    
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery(document).on("click", ".deleteVideo", function(){
        var id = $(this).data("id"),
            hitURL = baseURL + "deletedVideo",
            currentRow = $(this);
        var confirmation = confirm("Are you sure to Delete this Video ?");
        
        if(confirmation)
        {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { id : id } 
            }).done(function(data){
                console.log(data);
               // alert(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Video successfully deleted"); window.location.reload(); }
                else if(data.status = false) { alert("Video deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });
    
    jQuery(document).on("click", ".searchList", function(){  
    });    
});
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery(document).on("click", ".holdVideo", function(){
        var userId = $(this).data("userid"),
            hitURL = baseURL + "holdsVideo",
            currentRow = $(this);
        var confirmation = confirm("Are you sure to Hold this Video ?");
        if(confirmation)
        {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { userId : userId } 
            }).done(function(data){
                console.log(data);
               // alert(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Video successfully Hold"); }
                else if(data.status = false) { alert("Video deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });
    
    jQuery(document).on("click", ".searchList", function(){  
    });    
});
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery(document).on("click", ".unholdsVideo", function(){
        var userId = $(this).data("userid"),
            hitURL = baseURL + "unholdVideo",
            currentRow = $(this);
        var confirmation = confirm("Are you sure to Unold this Video ?");
        if(confirmation)
        {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { userId : userId } 
            }).done(function(data){
                console.log(data);
               // alert(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Video successfully Unhold"); window.location.reload(); }
                else if(data.status = false) { alert("Video deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });
    
    jQuery(document).on("click", ".searchList", function(){  
    });    
});
</script>

<script>
$(document).ready(function(){
 
 $('.delete_checkbox').click(function(){
  if($(this).is(':checked'))
  {
   $(this).closest('tr').addClass('removeRow');
  }
  else
  {
   $(this).closest('tr').removeClass('removeRow');
  }
 });

 $('#delete_all').click(function(){
  var checkbox = $('.delete_checkbox:checked');
  if(checkbox.length > 0)
  {
   var checkbox_value = [];
   $(checkbox).each(function(){
    checkbox_value.push($(this).val());
   });
   //alert(checkbox_value);
   $.ajax({
   // url:"<?php echo base_url(); ?>multiple_delete/delete_all",
    url:"<?php echo site_url('deletedAll');?>",
    method:"POST",
    data:{checkbox_value:checkbox_value},
    success:function()
    {
     $('.removeRow').fadeOut(1500);
    }
   })
  }
  else
  {
   alert('Select atleast one records');
  }
 });

});
</script>



