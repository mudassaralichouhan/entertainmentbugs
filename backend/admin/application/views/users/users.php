 <style>
     .dropdown-menu1>.active>a, 
 {
    /* color: #fff; */
    text-decoration: none;
    outline: 0;
    /* background-color: #2196F3; */
}
 </style>
 <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic column reorder -->
                <div class="panel panel-flat">
                 
                    <div class="panel-heading">
                        <h5 class="panel-title">All Users<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li> 
                            </ul>
                        </div>
                    </div>
 
          <div class="row">
            <div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>


    

        <!-- Tasks options -->
                    <div class="navbar navbar-default navbar-xs navbar-component" style="padding-right:0px;">
                    <ul class="nav navbar-nav no-border visible-xs-block">
                        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
                    </ul>

                    <div class="navbar-collapse collapse" id="navbar-filter" style="width:60%; float:left">
                    <p class="navbar-text">Filter:</p>
                    <div class="col-md-3">
                    <select id="ddlDist" name="ddlDist" onchange="popDistrictData();" class="nav navbar-nav form-control">
                        <option value="">--Select--</option>
                        <option value="2">Show All</option>
                        <!--<option value="1">UnHold</option>-->
                        <option value="1">Hold</option>
                    </select>
                    <input type="hidden" name="hidden_country" id="hidden_country" />
                    </div>
                       
                    <p class="navbar-text"><strong>From:</strong></p>
                    <div class="col-md-3">
                      <div class="form-group">
                        <input type="date" id="from_date" required name="from_date" class="form-control" id="exampleInputPassword1" placeholder="Enter Date">
                        <p id="mbl" style="color:red"></p>
                      </div>
                    </div>
                    <p class="navbar-text"><strong>To:</strong></p>
                    <div class="col-md-3">
                      <div class="form-group">
                       
                        <input type="date" id="to_date" required name="to_date" class="form-control" id="exampleInputPassword1" placeholder="Enter Date">
                        <p id="mbl" style="color:red"></p>
                      </div>
                    </div>
                    </div>

                    <div class="panel-body" style="width:40%; float:right;padding:0px;">
                        <form action="<?php echo base_url() ?>userListing" method="POST" class="main-search" id="searchList">
                            <div class="input-group content-group" style="margin-bottom:0px !Important">
                                <div class="has-feedback has-feedback-left">
                                    <input type="text" class="form-control input-xlg" name="searchText" value="<?php echo $searchText; ?>" placeholder="Search">
                                    <div class="form-control-feedback">
                                        <i class="icon-search4 text-muted text-size-base"></i>
                                    </div>
                                </div>

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-primary btn-xlg">Search</button>
                                </div>
                            </div>
                        </form>
                         <!-- <form action="<?php //echo base_url() ?>userListing" method="POST" id="searchList">
                            <div class="input-group">
                              <input type="text" name="searchText" value="<?php echo $searchText; ?>" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div>
                        </form> -->
                    </div>
                    
                    
                </div>
                <!-- /tasks options -->

                   <div class="box-body table-responsive no-padding">
                <table class="table datatable-reorder">
                    
                        <thead>
                        <tr> 
                                <th>Photo / Nameme</th> 
                                <th>Email id</th> 
                                <th>Video</th>
                                <th>Photos</th>
                                <th>Stories</th>
                                <th>Reward</th> 
                                <th>Followers</th>
                                <th>Created At</th>
                                <th>Status</th>
                        </tr>
                        </thead>
                        <tbody id="inwTblBody">
                            <?php
                            if(!empty($userRecords))
                            {
                                foreach($userRecords as $keys => $record)
                                {
                                    
                                    $bg = '';
                                    if($record->isDeleted == 1){
                                        $bg = '#f4f4f4';
                                    }

                                $videos = $this->db->where(['user_id'=>$record->id])->from("videos")->count_all_results();
                                $stories = $this->db->where(['user_id'=>$record->id])->from("stories")->count_all_results();
                                $photos = $this->db->where(['user_id'=>$record->id])->from("upload_photos")->count_all_results(); 
                                $followers = $this->db->where(['user_id'=>$record->id])->from("followers")->count_all_results();

                                $ii = $record->id;
                              
                                $sql = "select sum(`isRewardVideo`) as isRewardVideo from `videos` where user_id = $ii";

                                $query = $this->db->query($sql);
                                $ss = $query->result(); 
                                 foreach($ss as $row){
                                 $reward =  $row->isRewardVideo;
                                 }
                                   
                            //$aa = $this->db->last_query(); print_r($aa); exit();  
                            ?>
                            <tr style="background:<?php echo $bg; ?>">
                                <td style="font-weight:bold;"><img src="<?php echo 'https://entertainmentbugs.com/'; ?>public/uploads/users/small/<?php  echo $record->photo ?>" class="img-circle img-xs" alt="" style="margin-right:10px"><?php echo $record->name ?> </td>
  
                                <td><?php echo $record->email ?></td> 
                                <td><?php echo $videos ?></td>               
                                <td><?php echo $photos ?></td>
                                <td><?php echo $stories ?></td>
                                <?php if($reward != null) { ?>           
                                <td>2 /<?php echo $reward ?></td>
                                <?php }else{ ?>
                                <td>2 /0</td> 
                                <?php } ?>     
                                <td><?php echo $followers ?></td>
                                <td><?php echo date("d-m-Y", strtotime($record->created_at)) ?></td>
                                <td style="position:relative">  
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                                  Option <span class="caret"></span></a>
                                   
                                    <ul class="dropdown-menu" style="left: -100px;"> 
                                    <li><?php if($record->isDeleted == 0){ ?>
                                        <a class="holdUser" href="javascript:void(0)" data-userid="<?php echo $record->id; ?>">Hold</a></li>
                                        <?php }elseif($record->isDeleted == 1){ ?>
                                        <a class="unholdUser" href="javascript:void(0)" data-userid="<?php echo $record->id; ?>">UnHoad</a>
                                    </li>
                                    <?php } ?>
                                    <li><a class="deleteUser"  href="<?php echo base_url() ;?>userListing" data-id="<?php echo $record->id; ?>">Delete</a></li>
                                </ul> 
                                </td>   
                           
                            </tr>
                            <?php
                        }
                    }
                    ?>
                        </tbody>
                    </table>
                </div>

                 <div id="hidden_div" class="box-footer clearfix">
                    <?php echo $this->pagination->create_links(); ?>
                </div>

        </div>
        <!-- Basic column reorder -->
        </div>
        <!-- Main content -->

         </div>
  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script> -->
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>

<script type="text/javascript">
    document.getElementById('ddlDist').addEventListener('change', function () {
    var style = this.value == 5 ? 'block' : 'none';
    //alert(style);
    document.getElementById('hidden_div').style.display = style;
    });

    document.getElementById('to_date').addEventListener('change', function () {
    var style = this.value == 5 ? 'block' : 'none';
    //alert(style);
    document.getElementById('hidden_div').style.display = style;
    });
  </script>

<script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery(document).on("click", ".deleteUser", function(){
        var id = $(this).data("id"),
            hitURL = baseURL + "deleteUser",
            currentRow = $(this);
        var confirmation = confirm("Are you sure to Delete this User ?");
        if(confirmation)
        {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { id : id } 
            }).done(function(data){
               //console.log(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("User successfully deleted"); window.location.reload(); }
                else if(data.status = false) { alert("User deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });
    
    jQuery(document).on("click", ".searchList", function(){  
    });    
});
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){
        

    jQuery(document).on("click", ".holdUser", function(){
        var userId = $(this).data("userid");
        var hitURL = baseURL + "holdUser";
        var currentRow = $(this);
        var confirmation = confirm("Are you sure to hold this User ?");
        if(confirmation)
        {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { userId : userId },
             success: function(response) {
                currentRow.parents('tr').remove();
                if(response.status = true){
                    alert("User successfully Hold");
                    window.location.reload(); 
                }else if(response.status = false) {
                    alert("User deletion failed"); 
                    
                }else {
                    alert("Access denied..!"); 
                    }
                }
            });
        }
    });
    
  
    jQuery(document).on("click", ".searchList", function(){  
    });    
});
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){
        
        
        jQuery(document).on("click", ".unholdUser", function(){
        var userId = $(this).data("userid");
        var hitURL = baseURL + "unholdUser";
        var currentRow = $(this);
        var confirmation = confirm("Are you sure to Unhold this User ?");
        if(confirmation)
        {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { userId : userId },
             success: function(response) {
                currentRow.parents('tr').remove();
                if(response.status = true){
                    alert("User successfully Unhold");
                    window.location.reload(); 
                }else if(response.status = false) {
                    alert("User Unhold failed"); 
                    
                }else {
                    alert("Access denied..!"); 
                    }
                }
            });
        }
    });
        
   
    
    
    
    jQuery(document).on("click", ".searchList", function(){  
    });    
});
</script>

<script type="text/javascript">
    function popDistrictData(){
        var dist_id = document.getElementById("ddlDist").value;
        //alert(dist_id);
        $.ajax({
            type: "POST",
            url:"<?php echo site_url('filterUser');?>",
                data:{"dist_id" : dist_id},
                success: function (data) {
                    // alert(data);
                  $('#inwTblBody').html(data);
                }  
            });
    }
</script>

<script type="text/javascript">
    $(document).on('change','#to_date',function(){
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        if(from_date=="" || to_date==""){
            die();
        }else{
            $.ajax({
            type: "POST",
            url:"<?php echo site_url('user_by_date');?>",
                data:{"from_date" : from_date,"to_date":to_date},
                success: function (data) {
                    // alert(data);
                  $('#inwTblBody').html(data);
                }  
            });
        }
    });
    
</script>



