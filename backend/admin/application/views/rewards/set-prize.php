<div class="content-wrapper">
    <!-- Basic column reorder -->
    <div class="panel panel-flat">
        
        <div class="panel-heading">
            <h5 class="panel-title">Set Prize<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li> 
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php if ($error) : ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?= $error; ?>                    
                    </div>
                <?php endif ?>
                <?php if ($success) : ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?= $success; ?>
                    </div>
                <?php endif ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?= $validationErrors; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar navbar-default navbar-xs navbar-component" style="padding-right:0px;">
            <div class="panel-body">
                <div style="width: 300px">
                    <?= form_open('rewards/save-prize', array('id' => 'save-prize-form', 'class' => 'row')) ?>
                        <?php foreach ($prizes as $key => $prize) : ?>
                            <div class="col-md-12">
                                <div style="display: flex;">
                                    <label class="form-label" style="margin-right: 10px; width: 60px"><?= $key; ?></label>
                                    <div class="form-group has-feedback has-feedback-left">
                                        <input
                                                type="number"
                                                class="form-control"
                                                step="1"
                                                min="0"
                                                placeholder="Enter Prize"
                                                name="prizes[<?= $key ?>]"
                                                value="<?= $prize ?>" required
                                        />
                                        <div class="form-control-feedback">
                                            <i class="icon-coin-dollar text-muted"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <div class="col-12">
                            <input type="hidden" name="id" value="<?= $id ?>" />
                            <input type="submit" class="btn btn-primary" value="Save Prizes" />
                        </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>