<div class="content-wrapper">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Current Week Rewards<a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if ($error) {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php
                $success = $this->session->flashdata('success');
                if ($success) {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if ($error) {
                    ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                <?php } ?>
                <?php
                $success = $this->session->flashdata('success');
                if ($success) {
                    ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="navbar navbar-default navbar-xs navbar-component" style="padding-right:0px;">
            <ul class="nav navbar-nav no-border visible-xs-block">
                <li>
                    <a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter">
                        <i class="icon-menu7"></i>
                    </a>
                </li>
            </ul>

            <div class="navbar-collapse collapse" id="navbar-filter" style="width:70%; float:left">
                <div class="col-md-3">
                    <select id="ddlDist" name="ddlDist" onchange="popDistrictData();"
                            class="nav navbar-nav form-control">
                        <option value="">--Select--</option>
                        <option value="2">Show All</option>
                        <option value="1">Hold</option>
                    </select>
                    <input type="hidden" name="hidden_country" id="hidden_country"/>
                </div>

                <p class="navbar-text"><strong>From:</strong></p>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="date" id="from_date" required name="from_date" class="form-control"
                               id="exampleInputPassword1" placeholder="Enter Date">
                        <p id="mbl" style="color:red"></p>
                    </div>
                </div>
                <p class="navbar-text"><strong>To:</strong></p>
                <div class="col-md-3">
                    <div class="form-group">

                        <input type="date" id="to_date" required name="to_date" class="form-control"
                               id="exampleInputPassword1" placeholder="Enter Date">
                        <p id="mbl" style="color:red"></p>
                    </div>
                </div>
            </div>

            <div class="panel-body" style="width:30%; float:right;padding:0px;">
                <form action="<?php echo base_url() ?>rewards/current-video-listing/" method="POST" class="main-search"
                      id="searchList">
                    <div class="input-group content-group" style="margin-bottom:0px !Important">
                        <div class="has-feedback has-feedback-left">
                            <input type="text" class="form-control input-xlg" name="searchText"
                                   value="<?php echo $searchText; ?>" placeholder="Search">
                            <div class="form-control-feedback">
                                <i class="icon-search4 text-muted text-size-base"></i>
                            </div>
                        </div>
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary btn-xlg">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="box-body table-responsive no-padding">
            <table class="table datatable-reorder">
                <thead>
                <tr>
                    <th width="5%">
                        <button type="button" name="delete_all" id="delete_all" class="btn btn-danger btn-xs">Delete
                        </button>
                    </th>
                    <th>Thumbnail</th>
                    <th>User</th>
                    <th>Video Title</th>
                    <th>Date</th>
                    <!--<th>Status</th>-->
                    <th>Action</th>
                </tr>
                </thead>

                <tbody id="inwTblBody">
                <?php foreach ($videos as $keys => $video):
                    $bg = ''; ?>
                    <tr style="background:<?= $bg; ?>">
                        <td>
                            <input type="checkbox" class="delete_checkbox"
                                   value="<?= $video->id; ?>"
                            />
                        </td>
                        <td style="font-weight:bold;">
                            <img
                                    src="https://www.entertainmentbugs.com/public/uploads/video/thumbs/<?= $video->thumb; ?>"
                                    width="60px"
                            />
                        </td>
                        <td><?= $video->name; ?></td>
                        <td><?= $video->title; ?></td>

                        <td><?= date('F j, l h:i A', strtotime($video->created_at))?></td>
                        <td style="position:relative">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"
                               style="background:#none;padding-left:0px;">
                                Option <span class="caret"></span></a>
                            <ul class="dropdown-menu" style="left: -100px;">
                                <li><?php if ($video->isDeleted == 0){ ?>
                                    <a class="holdVideo" href="javascript:void()"
                                       data-userid="<?= $video->id; ?>">Hold</a></li>
                                <?php } elseif ($video->isDeleted == 1) { ?>
                                    <a class="unholdVideo" href="javascript:void()"
                                       data-userid="<?= $video->id; ?>">UnHold</a>
                                    </li>
                                <?php } ?>
                                <li>
                                    <a class="deleteVideo"
                                       href="<?= base_url(); ?>todaysVideoListing"
                                       data-id="<?= $video->id; ?>">
                                        Delete
                                    </a>
                                </li>
                            </ul>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <?php if (empty($videos)) { ?>
                    <tr>
                        <td><?= "Record not Fount...."; ?></td>
                    </tr>
                <?php } ?>
                </tbody>

            </table>
        </div>

        <div id="hidden_div" class="box-footer clearfix">
            <?= $this->pagination->create_links(); ?>
        </div>
    </div>
    <!-- Basic column reorder -->
</div>

<style>
    .removeRow {
        background-color: #FF0000;
        color: #FFFFFF;
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

<script type="text/javascript">
    document.getElementById('ddlDist').addEventListener('change', function () {
        var style = this.value == 5 ? 'block' : 'none';
        document.getElementById('hidden_div').style.display = style;
    });

    document.getElementById('to_date').addEventListener('change', function () {
        document.getElementById('hidden_div').style.display = this.value == 5 ? 'block' : 'none';
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();
            var link = jQuery(this).get(0).href;
            var value = link.substring(link.lastIndexOf('/') + 1);

            jQuery("#searchList").attr("action", baseURL + "rewards/current-video-listing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>

<script type="text/javascript">
    function popDistrictData() {
        var dist_id = document.getElementById("ddlDist").value;
        $.ajax({
            type: "POST",
            url: "<?= site_url('filterVideorewardvideo');?>",
            data: {"dist_id": dist_id},
            success: function (data) {
                // alert(data);
                $('#inwTblBody').html(data);
            }
        });

        // $.ajax({
        //     type: "POST",
        //     url: "<?=base_url()?>artist/filterArtist/"+dist_id,
        //     dataType: "html",
        //     success: function(html){
        //         $("#inwTblBody").html(html);
        //     }
        // });
    }
</script>

<script type="text/javascript">
    $(document).on('change', '#to_date', function () {
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        if (from_date == "" || to_date == "") {
            die();
        } else {
            $.ajax({
                type: "POST",
                url: "<?= site_url('video_by_date');?>",
                data: {"from_date": from_date, "to_date": to_date},
                success: function (data) {
                    // alert(data);
                    $('#inwTblBody').html(data);
                }
            });
        }
    });

</script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(document).on("click", ".deleteVideo", function () {
            var id = $(this).data("id"),
                hitURL = baseURL + "deleteVideo",
                currentRow = $(this);
            var confirmation = confirm("Are you sure to Delete this Video ?");

            if (confirmation) {
                jQuery.ajax({
                    type: "POST",
                    dataType: "json",
                    url: hitURL,
                    data: {id: id}
                }).done(function (data) {
                    console.log(data);
                    currentRow.parents('tr').remove();
                    if (data.status = true) {
                        alert("Video successfully deleted");
                        window.location.reload();
                    } else if (data.status = false) {
                        alert("Video deletion failed");
                    } else {
                        alert("Access denied..!");
                    }
                });
            }
        });

        jQuery(document).on("click", ".searchList", function () {
        });
    });
</script>

<script>
    jQuery(document).ready(function () {
        jQuery(document).on("click", ".holdVideo", function () {
            var userId = jQuery(this).data("userid");
            var hitURL = baseURL + "holdVideo";
            var currentRow = jQuery(this);
            var confirmation = confirm("Are you sure to Hold this Video ?");
            if (confirmation) {
                jQuery.ajax({
                    type: "POST",
                    dataType: "json",
                    url: hitURL,
                    data: {userId: userId}
                }).done(function (data) {
                    console.log(data);
                    // alert(data);
                    currentRow.parents('tr').remove();
                    if (data.status = true) {
                        alert("Video successfully Hold");
                        window.location.reload();
                    } else if (data.status = false) {
                        alert("Video deletion failed");
                    } else {
                        alert("Access denied..!");
                    }
                });
            }
        });

        jQuery(document).on("click", ".searchList", function () {
        });
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(document).on("click", ".unholdVideo", function () {
            var userId = jQuery(this).data("userid");
            var hitURL = baseURL + "unholdVideo";
            var currentRow = jQuery(this);
            var confirmation = confirm("Are you sure to Unhold this Video ?");
            if (confirmation) {
                jQuery.ajax({
                    type: "POST",
                    dataType: "json",
                    url: hitURL,
                    data: {userId: userId}
                }).done(function (data) {
                    console.log(data);
                    // alert(data);
                    currentRow.parents('tr').remove();
                    if (data.status = true) {
                        alert("Video successfully Unhold");
                        window.location.reload();
                    } else if (data.status = false) {
                        alert("Video deletion failed");
                    } else {
                        alert("Access denied..!");
                    }
                });
            }
        });

        jQuery(document).on("click", ".searchList", function () {
        });
    });
</script>

<script>
    $(document).ready(function () {

        $('.delete_checkbox').click(function () {
            if ($(this).is(':checked')) {
                $(this).closest('tr').addClass('removeRow');
            } else {
                $(this).closest('tr').removeClass('removeRow');
            }
        });

        $('#delete_all').click(function () {
            var checkbox = $('.delete_checkbox:checked');
            if (checkbox.length > 0) {
                var checkbox_value = [];
                $(checkbox).each(function () {
                    checkbox_value.push($(this).val());
                });
                $.ajax({
                    // url:"<?= base_url(); ?>multiple_delete/delete_all",
                    url: "<?= site_url('deleteAll');?>",
                    method: "POST",
                    data: {checkbox_value: checkbox_value},
                    success: function () {
                        $('.removeRow').fadeOut(1500);
                    }
                })
            } else {
                alert('Select atleast one records');
            }
        });
    });
</script>



