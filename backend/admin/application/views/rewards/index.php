<div class="content-wrapper">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Rewards Videos<a class="heading-elements-toggle"><i class="icon-more"></i></a>
            </h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>
        </div>

        <div class="box-body table-responsive no-padding">
            <table class="table datatable-reorder">
                <thead>
                <tr>
                    <th>Thumbnail</th>
                    <th>User</th>
                    <th>Video Title</th>
                    <th>Date</th>
                </tr>
                </thead>

                <tbody id="inwTblBody">
                <?php
                foreach ($videos as $keys => $video): ?>
                    <tr>
                        <td style="font-weight:bold;">
                            <img
                                    src="https://entertainmentbugs.com/public/uploads/video/thumbs/<?= $video->thumb ?>"
                                    width="60px"
                            />
                        </td>
                        <td><?= $video->name; ?></td>
                        <td><?= $video->title; ?></td>
                        <td><?= date('j F g:i a', strtotime($video->created_at)); ?></td>
                    </tr>
                <?php endforeach; ?>

                <?php if (empty($videos)): ?>
                    <tr>
                        <td><?php echo "Record not Fount...."; ?></td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>