
 <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic column reorder -->
                <div class="panel panel-flat">
                
                    <div class="panel-heading">
                         <div class="text-right p-2">
                        <a href="<?php echo base_url(); ?>createSubcategory" class="btn btn-primary ">Add Subcategory</a>
                        </div>
                        <h5 class="panel-title">All Subcategories<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                        
                     
                    </div>
                            <div class="heading-elements mt-3">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li> 
                            </ul>
                        </div>

          <div class="row">
            <div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Tasks options -->
                    <div class="navbar navbar-default navbar-xs navbar-component" style="padding-right:0px;">
                    <ul class="nav navbar-nav no-border visible-xs-block">
                        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
                    </ul>

                    <div class="navbar-collapse collapse" id="navbar-filter" style="width:70%; float:left">
                    <!-- <p class="navbar-text">Filter:</p> -->
                    <!--<div class="col-md-3">-->
                    <!--<select id="ddlDist" name="ddlDist" onchange="popDistrictData();" class="nav navbar-nav form-control">-->
                    <!--    <option value="">--Select--</option>-->
                    <!--    <option value="2">Show All</option>-->
                    <!--    <option value="0">UnHold</option>-->
                    <!--    <option value="1">Hold</option>-->
                       
                    <!--</select>-->
                    <!--<input type="hidden" name="hidden_country" id="hidden_country" />-->
                    <!--</div>-->
                      

                      <!-- <button style="margin-bottom: 10px" class="btn bg-success btn-md delete_all" data-url="/deleteAll">Delete All</button> -->
 
                    </div>
    
             
                    
                </div>
                <!-- /tasks options -->
				<?php 
				
				foreach($categories AS $cat){
					
				?>
				
				<button class="accordion"><?php echo $cat->category;?></button>
 <div class="panel">

<?php 
					$parent_id=$cat->id;
					
					$this->db->select('categories.*');
        $this->db->from('categories');
    
     $this->db->where('categories.parent_id =',$parent_id);
     //$this->db->join('categories c','c.id = categories.parent_id');
     
     $query = $this->db->get();
   $subcat=$query->result();

   if(!empty($subcat)){
   foreach($subcat As $scat){
	   if($scat->category!=''){
		   
		  
	   ?>
	

	      <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       
                                    <input type="text" class="form-control required" id="name<?php echo $scat->id;?>" value="<?php echo $scat->category;?>" id="taskTitle" name="name" />
                                    </div>
                                </div>
								  <div class="col-md-2"><a href="javascript:void(0)" id="update<?php echo $scat->id;?>" data-id="<?php echo $scat->id;?>"><span class="btn btn-primary ">Update</span></a></div>
								  
								  <?php if($scat->status > 0) {
									  ?>
								  <div class="col-md-2" id="hold<?php echo $scat->id;?>"><a href="javascript:void(0)" id="delete<?php echo $scat->id;?>" data-product="<?php echo $scat->id;?>"><span class="btn btn-primary ">Hold</span></a></div>
								  <?php 
								  }
								  else
								  {
									  ?>
									 <div class="col-md-2" id="unhold<?php echo $scat->id;?>"><a href="javascript:void(0)" id="unhold<?php echo $scat->id;?>" data-product="<?php echo $scat->id;?>"><span class="btn btn-primary  ">Unhold</span></a></div> 
									  <?php 
								  }
								  ?>
								
                            </div>
							
							<script type="text/javascript">
    jQuery(document).ready(function(){
		
		
				  jQuery('#delete'+<?php echo $scat->id;?>).click(function (e) {
			  $('#delete'+<?php echo $scat->id;?>).html('<img width="30" height="30" src="https://img.icons8.com/ios-glyphs/30/40C057/spinner-frame-5.png" alt="spinner-frame-5"/>');
		
			  var product_id = $(this).data("product");
			  	  console.log(product_id);
				  
				

			  debugger;
 
         console.log(product_id);
         $.ajax({
            type: "Post",
            url: "/backend/admin/ajaxholdSubcategory",
            data: {
               id: product_id,
			   
            },
            
            success: function (res) {
				
				$('#delete'+<?php echo $scat->id;?>).html('<img width="25" height="25" src="https://img.icons8.com/color/48/ok--v1.png" alt="ok--v1"/>');
				
				
				setTimeout(function(){ $('#hold'+<?php echo $scat->id;?>).html('<a href="javascript:void(0)" id="unhold<?php echo $scat->id;?>" data-product="<?php echo $scat->id;?>"><span class="btn btn-primary ">Unhold</span></a>'); }, 2000);
				console.log(res);
				debugger;
				
			}
		 });
		  });
		  
		  
		  	
				  jQuery('#unhold'+<?php echo $scat->id;?>).click(function (e) {
			  $('#unhold'+<?php echo $scat->id;?>).html('<img width="30" height="30" src="https://img.icons8.com/ios-glyphs/30/40C057/spinner-frame-5.png" alt="spinner-frame-5"/>');
		
			  var product_id = $(this).attr("data-product");
			  	  console.log(product_id);
				  
				

			  debugger;
 
         console.log(product_id);
         $.ajax({
            type: "Post",
            url: "/backend/admin/ajaxunholdSubcategory",
            data: {
               id: product_id,
			   
            },
            
            success: function (res) {
				
				$('#unhold'+<?php echo $scat->id;?>).html('<img width="25" height="25" src="https://img.icons8.com/color/48/ok--v1.png" alt="ok--v1"/>');
				
				
				setTimeout(function(){ $('#unhold'+<?php echo $scat->id;?>).html('<a href="javascript:void(0)" id="delete<?php echo $scat->id;?>" data-id="<?php echo $scat->id;?>"><span class="btn btn-primary ">Hold</span></a>'); }, 2000);
				console.log(res);
				debugger;
				
			}
		 });
		  });
		  
		  jQuery('#update'+<?php echo $scat->id;?>).click(function (e) {
			  $('#update'+<?php echo $scat->id;?>).html('<img width="30" height="30" src="https://img.icons8.com/ios-glyphs/30/40C057/spinner-frame-5.png" alt="spinner-frame-5"/>');
		
			  var product_id = $(this).data("id");
			  	  console.log(product_id);
				  
				  var name=$('#name<?php echo $scat->id;?>').val();
				  console.log(name);
			  debugger;
 
         console.log(product_id);
         $.ajax({
            type: "Post",
            url: "/backend/admin/ajaxupdateSubcategory",
            data: {
               id: product_id,
			   name:name
            },
            
            success: function (res) {
				
				$('#update'+<?php echo $scat->id;?>).html('<img width="25" height="25" src="https://img.icons8.com/color/48/ok--v1.png" alt="ok--v1"/>');
				
				
				setTimeout(function(){ $('#update'+<?php echo $scat->id;?>).html('<span class="btn btn-primary ">update</span>'); }, 2000);
				console.log(res);
				debugger;
				
			}
		 });
		  });
       
    });
</script>


	   
	   <?php 
	   }
 
   }
   }
   else
   {
	   echo "No Subcategories";
   }
   ?>
   </div>
   <?php 
				}
				
				?>
				
				<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>
<style>
.accordion {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
}

.active, .accordion:hover {
  background-color: #ccc; 
}

.panel {
  padding: 27px 15px;

  background-color: white;
  overflow: hidden;
}
</style>

                <!--div class="box-body table-responsive no-padding">
                <table class="table datatable-reorder">
               
                        <thead>
                        <tr>    
                                 <th>Sub Category</th>
                                 <th>Parent Category</th>
                                <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="inwTblBody">
                            <?php
                            if(!empty($subcategories))
                            {
                                foreach($subcategories as $record)
                                {
                                    $bg = '';

                            ?>
                            
                            <?php if($record->status == 1){
                                $bg = '#f4f4f4';
                            }   
                            
                            ?>
                            
                            
                        <tr style="background:<?php echo $bg; ?>">
                     

                            
                                 <td style="font-weight:bold;">
                          <?php echo $record->category ?>
                        </td>
                               

                                <td><?php echo $record->cat ?></td> 
                                <td style="position:relative">  
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">
                                  Option <span class="caret"></span></a>
                                    <ul class="dropdown-menu" style="left: -100px;"> 
                                    <li><?php if($record->status ==0 ){ ?>
                                      <a class="unholdpayout" href="javascript:void(0)" data-userid="<?php echo $record->id; ?>"> Unhold</a></li>
                                            <?php }elseif($record->status == 1){ 
                                           
                                              ?><a class="holdpayout" href="javascript:void(0)" data-userid="<?php echo $record->id; ?>">Hold</a>
                                                 
                                    </li>
                                            <?php } ?>
                                   <li><a class="edit" href="<?php echo base_url()."editSubCategory/$record->id"; ?>" data-userid="<?php echo $record->id; ?>">Edit</a></li>
                                </ul> 
                                </td>     
                            </tr>
                            <?php
                        }
                    } else if(empty($subcategories)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
                  <?php  }
                    ?>
                        </tbody>
                     
                    </table>
                </div-->

                 <div id="hidden_div" class="box-footer clearfix">
                    <?php  
                    echo $this->pagination->create_links(); ?>
                </div>
        </div>
        <!-- Basic column reorder -->
        </div>
        <!-- Main content -->

         </div>
  </div>
<style>
.removeRow
{
 background-color: #FF0000;
 color:#FFFFFF;
}
</style>

  

<!-- <script type="text/javascript" src="<?php //echo base_url(); ?>assets/js/common.js" charset="utf-8"></script> -->
<script type="text/javascript">
    jQuery(document).ready(function(){
		
		  jQuery('#update').click(function (e) {
			  console.log('hi');
			  debugger;
			  var product_id = $(this).data("id");
 
         console.log(product_id);
         $.ajax({
            type: "Post",
            url: SITEURL + "updateSubcategory",
            data: {
               id: product_id
            },
            dataType: "json",
            success: function (res) {
				console.log(res);
				debugger;
				
			}
		 });
		  });
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);

            jQuery("#searchList").attr("action", baseURL + "storiesListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>




<script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery(document).on("click", ".deleteStory", function(){
        var id = $(this).data("id"),
            hitURL = baseURL + "deleteStories",
            currentRow = $(this);
        var confirmation = confirm("Are you sure to Delete this Story ?");
        
        if(confirmation)
        {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { id : id } 
            }).done(function(data){
                console.log(data);
               // alert(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Story successfully deleted"); window.location.reload(); }
                else if(data.status = false) { alert("Story deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });
    
    jQuery(document).on("click", ".searchList", function(){  
    });    
});
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery(document).on("click", ".holdpayout", function(){
        var userId = $(this).data("userid");
        var hitURL = baseURL + "holdSubcategory/"+userId;
        var currentRow = $(this);
        var confirmation = confirm("Are you sure to Hold this Subcategory ?");
        if(confirmation)
        {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { userId : userId } 
            }).done(function(data){
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Subcategory successfully Hold"); window.location.reload(); }
                else if(data.status = false) { alert("Subcategory deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });
    
    jQuery(document).on("click", ".searchList", function(){  
    });  
        jQuery(document).on("click", ".approvepayout", function(){
        var userId = $(this).data("userid");
        var hitURL = baseURL + "approvepayout";
        var currentRow = $(this);
        var confirmation = confirm("Are you sure to Hold this Payout ?");
        if(confirmation)
        {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { userId : userId } 
            }).done(function(data){
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Payout successfully Hold"); window.location.reload(); }
                else if(data.status = false) { alert("Payout deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });
    
});
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery(document).on("click", ".unholdpayout", function(){
        var userId = $(this).data("userid");
        var hitURL = baseURL + "unholdSubcategory/"+userId;
        var currentRow = $(this);
        var confirmation = confirm("Are you sure to Unhold this Category ?");
        if(confirmation)
        {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { userId : userId } 
            }).done(function(data){
                console.log(data);
               // alert(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Subcategory successfully Unhold"); window.location.reload(); }
                else if(data.status = false) { alert("Subcategory deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });
    
    jQuery(document).on("click", ".searchList", function(){  
    });    
});
</script>

<script>
$(document).ready(function(){
 
 $('.delete_checkbox').click(function(){
  if($(this).is(':checked'))
  {
   $(this).closest('tr').addClass('removeRow');
  }
  else
  {
   $(this).closest('tr').removeClass('removeRow');
  }
 });

 $('#delete_all').click(function(){
  var checkbox = $('.delete_checkbox:checked');
  if(checkbox.length > 0)
  {
   var checkbox_value = [];
   $(checkbox).each(function(){
    checkbox_value.push($(this).val());
   });
   //alert(checkbox_value);
   $.ajax({
   // url:"<?php echo base_url(); ?>multiple_delete/delete_all",
    url:"<?php echo site_url('deleteAllStories');?>",
    method:"POST",
    data:{checkbox_value:checkbox_value},
    success:function()
    {
     $('.removeRow').fadeOut(1500);
    }
   })
  }
  else
  {
   alert('Select atleast one records');
  }
 });

});
</script>



