
 <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic column reorder -->
                <div class="panel panel-flat">
                 
                    <div class="panel-heading">
                        <h5 class="panel-title">Boost Photo<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li> 
                            </ul>
                        </div>
                    </div>
 

          <div class="row">
            <div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>


        <!-- Tasks options -->
                    <div class="navbar navbar-default navbar-xs navbar-component" style="padding-right:0px;">
                    <ul class="nav navbar-nav no-border visible-xs-block">
                        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
                    </ul>

                    <div class="navbar-collapse collapse" id="navbar-filter" style="width:70%; float:left">
                    <!-- <p class="navbar-text">Filter:</p> -->
                    <div class="col-md-3">
                    <select id="ddlDist" name="ddlDist" onchange="popDistrictData();" class="nav navbar-nav form-control">
                        <option value="">--Select--</option>
                        <option value="0">Show All</option>
                        <!--<option value="0">UnHold</option>-->
                        <option value="1">Hold</option>
                    </select>
                    <input type="hidden" name="hidden_country" id="hidden_country" />
                    </div>

 
                    </div>
    
                    <div class="panel-body" style="width:30%; float:right;padding:0px;">
                        <form action="<?php echo base_url() ?>boostPhoto/boostPhotoListing" method="POST" class="main-search" id="searchList">
                            <div class="input-group content-group" style="margin-bottom:0px !Important">
                                <div class="has-feedback has-feedback-left">
                                    <input type="text" class="form-control input-xlg" name="searchText" value="<?php echo $searchText; ?>" placeholder="Search">
                                    <div class="form-control-feedback">
                                        <i class="icon-search4 text-muted text-size-base"></i>
                                    </div>
                                </div>

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-primary btn-xlg">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
                <!-- /tasks options -->

                <div class="box-body table-responsive no-padding">
                <table class="table datatable-reorder">
                <!-- <table class="table table-striped media-library table-lg"> -->
                        <thead>
                        <tr>    
                                <th width="5%"><button type="button" name="delete_all" id="delete_all" class="btn btn-danger btn-xs">Delete</button></th>
                                        
                                        <th>P Thumbnail</th> 
                                        <th>User</th> 
                                        <th>Amount</th> 
                                        <th>Post date</th>
                                        <th>Post Validity</th>
                                        <th>Action</th> 
                        </tr>
                        </thead>
                        <tbody id="inwTblBody">
                            <?php
                            $dateCheck = null;
                            if(!empty(@$boostPhotoRecords))
                            {
                                foreach(@$boostPhotoRecords as $key=>$record)
                                {
                                    
                            ?>
                            
                              <tr>
                               <?php if(!is_null($dateCheck) && $dateCheck != date("d-m-Y", strtotime($record->created_at))){ ?>
                               
                                    <td  style="width:84px;display: block;padding: 10px 0px 10px 0;background: #000;color: #fff;font-size: 11px;text-align: center;"><?php echo date("d-m-Y", strtotime($record->created_at)) ?></td> 
                                </tr>
                               <?php }else if($key==0){ ?>
                                    <td  style="width:84px;display: block;padding: 10px 0px 10px 0;background: #000;color: #fff;font-size: 11px;text-align: center;"><?php echo date("d-m-Y", strtotime($record->created_at)) ?></td> 
                              <?php } 
                              
                              $dateCheck = date("d-m-Y", strtotime($record->created_at));
                              ?>
                            <tr>
                                
                        <tr>
                        <td><input type="checkbox" class="delete_checkbox" value="<?php echo $record->id; ?>" /></td>
                                <td style="font-weight:bold;">
                                    <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php echo 'https://entertainmentbugs.com/'; ?>public/uploads/users/small/<?php  echo $record->photo ?>" />
                                </td>
  
                            
                            <td><?php echo $record->name ?></td> 
                            <td>Rs: <?php echo $record->amount ?>/-</td>

                                      
                                      
                                <!--<td><?php echo date('d-m-Y', strtotime("$dateCheck +$record->days days")); ?></td>-->
                                 <td><?php echo date("d-m-Y", strtotime($record->created_at)) ?></td>
                                 
                                 
                                 
                              <td><?php echo $record->days ?>days ( <?php echo date('d-m-Y', strtotime("$dateCheck +$record->days days")); ?> )</td> 
                                <td style="position:relative">  
                                <?php if($record->isDeleted == 0){ ?>
                                        <button class="holdBoostPhoto btn btn-primary" href="javascript:void(0)" data-userid="<?php echo $record->id; ?>">Hold</button>
                                            <?php }elseif($record->isDeleted == 1){ ?>
                                        <button class="unholdBoostPhoto btn btn-success" href="javascript:void(0)" data-userid="<?php echo $record->id; ?>">Approve</button>
                                  
                                            <?php }?>
                                <!--<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"  style="background:#none;padding-left:0px;">-->
                                <!--  Option <span class="caret"></span></a>-->
                                <!--    <ul class="dropdown-menu" style="left: -100px;"> -->
                                <!--    <li><?php if($record->isDeleted == 0){ ?>-->
                                <!--        <button class="holdBoostPhoto" href="javascript:void(0)" data-userid="<?php echo $record->id; ?>">Hold</button></li>-->
                                <!--            <?php }elseif($record->isDeleted == 1){ ?>-->
                                <!--        <a class="unholdBoostPhoto" href="javascript:void(0)" data-userid="<?php echo $record->id; ?>">UnHold</a>-->
                                <!--    </li>-->
                                <!--            <?php } ?>-->
                                    <!--<li><a class="deleteStory"  href="<?php echo base_url() ;?>BoostPhoto/boostPhotoListing" data-id="<?php echo $record->id; ?>">Delete</a></li>-->
                                <!--</ul> -->
                                </td>  
                            </tr>
                            <?php
                        }
                    } else if(empty($storyRecords)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
                  <?php  }
                    ?>
                        </tbody>
                     
                    </table>
                </div>

                 <div id="hidden_div" class="box-footer clearfix">
                    <?php  
                    echo $this->pagination->create_links(); ?>
                </div>
        </div>
        <!-- Basic column reorder -->
        </div>
        <!-- Main content -->

         </div>
  </div>
<style>
.removeRow
{
 background-color: #FF0000;
 color:#FFFFFF;
}
</style>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

  <script type="text/javascript">
    document.getElementById('ddlDist').addEventListener('change', function () {
    var style = this.value == 5 ? 'block' : 'none';
    //alert(style);
    document.getElementById('hidden_div').style.display = style;
    });

    document.getElementById('to_date').addEventListener('change', function () {
    var style = this.value == 5 ? 'block' : 'none';
    //alert(style);
    document.getElementById('hidden_div').style.display = style;
    });
  </script>

<!-- <script type="text/javascript" src="<?php //echo base_url(); ?>assets/js/common.js" charset="utf-8"></script> -->
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);

            jQuery("#searchList").attr("action", baseURL + "boostPhotoListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>

<script type="text/javascript">
    function popDistrictData(){
        var dist_id = document.getElementById("ddlDist").value;
       // alert(dist_id);
        $.ajax({
            type: "POST",
            url:"<?php echo site_url('BoostPhoto/filterBoostPhoto');?>",
                data:{"dist_id" : dist_id},
                success: function (data) {
                    // alert(data);
                  $('#inwTblBody').html(data);
                }  
            });

    }
</script>

<script type="text/javascript">
    $(document).on('change','#to_date',function(){
        var from_date = $('#from_date').val();
        var to_date = $('#to_date').val();
        if(from_date=="" || to_date==""){
            die();
        }else{
            $.ajax({
            type: "POST",
            url:"<?php echo site_url('story_by_date');?>",
                data:{"from_date" : from_date,"to_date":to_date},
                success: function (data) {
                    // alert(data);
                  $('#inwTblBody').html(data);
                }  
            });
        }
    });
    
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery(document).on("click", ".holdBoostPhoto", function(){
        var userId = $(this).data("userid"),
            hitURL = baseURL + "BoostPhoto/holdBoostPhoto",
            currentRow = $(this);
        var confirmation = confirm("Are you sure to Hold this Boost Photo ?");
       // alert(hitURL);
        if(confirmation)
        {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { userId : userId } 
            }).done(function(data){
                console.log(data);
               // alert(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Boost Photo successfully Hold"); window.location.reload(); }
                else if(data.status = false) { alert("Boost Photo deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });
    
    jQuery(document).on("click", ".searchList", function(){  
    });    
});
</script>

<script type="text/javascript">
    jQuery(document).ready(function(){
    jQuery(document).on("click", ".unholdBoostPhoto", function(){
        var userId = $(this).data("userid"),
            hitURL = baseURL + "BoostPhoto/unholdBoostPhoto",
            currentRow = $(this);
        var confirmation = confirm("Are you sure to Unhold this Boost Photo ?");
        if(confirmation)
        {
            jQuery.ajax({
            type : "POST",
            dataType : "json",
            url : hitURL,
            data : { userId : userId } 
            }).done(function(data){
                console.log(data);
               // alert(data);
                currentRow.parents('tr').remove();
                if(data.status = true) { alert("Boost Photo successfully Unhold"); window.location.reload(); }
                else if(data.status = false) { alert("Boost Photo deletion failed"); }
                else { alert("Access denied..!"); }
            });
        }
    });
    
    jQuery(document).on("click", ".searchList", function(){  
    });    
});
</script>

<script>
$(document).ready(function(){
 
 $('.delete_checkbox').click(function(){
  if($(this).is(':checked'))
  {
   $(this).closest('tr').addClass('removeRow');
  }
  else
  {
   $(this).closest('tr').removeClass('removeRow');
  }
 });

 $('#delete_all').click(function(){
  var checkbox = $('.delete_checkbox:checked');
  if(checkbox.length > 0)
  {
   var checkbox_value = [];
   $(checkbox).each(function(){
    checkbox_value.push($(this).val());
   });
   //alert(checkbox_value);
   $.ajax({
   // url:"<?php echo base_url(); ?>multiple_delete/delete_all",
    url:"<?php echo site_url('deleteAllStory');?>",
    method:"POST",
    data:{checkbox_value:checkbox_value},
    success:function()
    {
     $('.removeRow').fadeOut(1500);
    }
   })
  }
  else
  {
   alert('Select atleast one records');
  }
 });

});
</script>



