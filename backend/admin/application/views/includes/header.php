<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $pageTitle; ?></title> 
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <!-- <link href="<?php //echo base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> -->
    <!-- FontAwesome 4.3.0 -->
   <!-- <link href="<?php //echo base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> -->
    <!-- Ionicons 2.0.0 -->
   <!-- <link href="<?php //echo base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" /> -->
    <!-- Theme style -->
   <!-- <link href="<?php //echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" /> -->
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
  <!--  <link href="<?php //echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" /> --> 

    <!-- Global stylesheets -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/login/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/login/css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/login/css/core.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/login/css/components.css" rel="stylesheet" type="text/css">
  <link href="<?php echo base_url(); ?>assets/login/css/colors.css" rel="stylesheet" type="text/css">
  <!-- /global stylesheets -->

    <style>
    	.error{
    		color:red;
    		font-weight: normal;
    	}
    </style>
    <!-- <script src="<?php //echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script> -->

    <!-- Core JS files -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/login/js/plugins/loaders/pace.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/login/js/core/libraries/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/login/js/core/libraries/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/login/js/plugins/loaders/blockui.min.js"></script>
  <!-- /core JS files -->

  <!-- Theme JS files -->
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/login/js/plugins/tables/datatables/datatables.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/login/js/plugins/tables/datatables/extensions/col_reorder.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/login/js/plugins/forms/selects/select2.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/login/js/core/app.js"></script>
  <!-- /theme JS files -->

    <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
    </script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="navbar-bottom">

    <!-- Main navbar -->
  <div class="navbar navbar-inverse">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.html"><img src="<?php echo base_url(); ?>assets/login/images/logo_light.png" alt=""></a>

      <ul class="nav navbar-nav visible-xs-block">
        <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
      </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
      <ul class="nav navbar-nav">
        <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
 
      </ul>

      <p class="navbar-text"><span class="label bg-success-400">Online</span></p>

      <ul class="nav navbar-nav navbar-right">
      
        <li class="dropdown dropdown-user">
          <a class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?php echo base_url(); ?>assets/login/images/demo/users/face11.jpg" alt="">
            <span>Victoria</span>
            <i class="caret"></i>
          </a>

          <ul class="dropdown-menu dropdown-menu-right">  
        <li><a href="<?php echo base_url(); ?>changepassword"><i class="icon-key"></i> Change Password</a>
            <li><a href="<?php echo base_url(); ?>logout"><i class="icon-switch2"></i> Logout</a>

            </li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
  <!-- /main navbar -->


  <!-- Page header -->
  <div class="page-header">
    <div class="breadcrumb-line">
      <ul class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ul>

    </div>
  </div>
  <!-- /page header -->
 
  <!-- Page container -->
  <div class="page-container">

    <!-- Page content -->
    <div class="page-content">

      <!-- Main sidebar -->
     
 <!-- Main sidebar -->
      <div class="sidebar sidebar-main sidebar-default">
        <div class="sidebar-content">

          <!-- Main navigation -->
          <div class="sidebar-category sidebar-category-visible">
            <div class="category-title h6">
              <span>Main navigation</span>
              <ul class="icons-list">
                <li><a href="#" data-action="collapse"></a></li>
              </ul>
            </div>

                 
<div class="category-content no-padding">
<ul class="navigation navigation-main navigation-accordion">
<li><a href="<?php echo base_url(); ?>dashboard"><i class="icon-home4"></i> <span>Dashboard</span></a></li>


<!--<li class="navigation-header"><span> Daily Updates </span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>-->
<!--<li><a href="<?php echo base_url(); ?>todaysVideo/todaysVideoListing"><i class="icon-movie"></i> <span> Video </span></a></li> -->
<!--<li><a href="<?php echo base_url(); ?>photo/photoListing"><i class="icon-image2"></i> <span> Photo </span></a></li>-->
<!--<li><a href="<?php echo base_url(); ?>story/storyListing"><i class="icon-pencil-ruler"></i> <span>  Story</span></a></li> -->
<!--<li><a href="<?php echo base_url(); ?>rewards/Todayrewards"><i class="icon-coin-dollar"></i> <span>  Reward</span></a></li> -->




<li class="navigation-header"><span>User</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
<li><a href="<?php echo base_url(); ?>userListing"><i class="icon-user"></i> <span>  All Users</span></a></li> 
<li><a href="<?php echo base_url(); ?>artist/artistListing"><i class="icon-users"></i> <span>All Artist</span></a></li>
<li><a href="<?php echo base_url(); ?>production/productionListing"><i class="icon-users4"></i> <span>All Production House</span></a></li>
<li><a href="<?php echo base_url(); ?>auditions/auditionsListing"><i class="icon-list-numbered"></i> <span>All Auditions</span></a></li> 

<li class="navigation-header"><span> All Post Content</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>

<li><a href="<?php echo base_url(); ?>video/videoListing"><i class="icon-movie"></i> <span> Video</span></a></li> 
<li><a href="<?php echo base_url(); ?>photos/photosListing"><i class="icon-image2"></i> <span> Photo</span></a></li>
<li><a href="<?php echo base_url(); ?>stories/storiesListing"><i class="icon-pencil-ruler"></i> <span> Story</span></a></li>
<li><a href="<?php echo base_url(); ?>rewards/current-video-listing"><i class="icon-coin-dollar"></i> <span> Reward</span></a></li>

<li class="navigation-header"><span>Rewards</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
<li><a href="<?php echo base_url(); ?>rewards/current-video-listing"><i class="icon-width"></i> <span>Current Week</span></a></li>
<li><a href="<?php echo base_url(); ?>rewards/previous"><i class="icon-width"></i> <span>Previous Week</span></a></li>
<li><a href="<?php echo base_url(); ?>rewards/all"><i class="icon-width"></i> <span>All Entertainer List</span></a></li>
<li><a href="<?= base_url("rewards/set-prize") ?>"><i class="icon-width"></i> <span>Set Prize</span></a></li>

<!--<li class="navigation-header"><span>Plan</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>-->
<!--<li><a href="<?php echo base_url(); ?>plan"><i class="icon-width"></i> <span>Active Plan</span></a></li>-->
<!--<li><a href="<?php echo base_url(); ?>plan/expireplan"><i class="icon-width"></i> <span>Expire Plan</span></a></li>-->


<li class="navigation-header"><span> Extra</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
<li><a href="<?php echo base_url(); ?>flag"><i class="icon-width"></i> <span> Flag Option</span></a></li>


<br> 
<li class="navigation-header"><span>Artist Premium</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
<!--<li><a href="<?php echo base_url(); ?>plan"><i class="icon-width"></i> <span> Artist Premium theme</span></a></li>-->
<li><a href="<?php echo base_url(); ?>artistThemePlan/artistThemePlanListing"><i class="icon-width"></i> <span> Premium Profile </span></a></li>
<li><a href="<?php echo base_url(); ?>artistPremium/artistPremiumListing"><i class="icon-width"></i> <span> Theme Plan</span></a></li>

<br>

<li class="navigation-header"><span>Advertise</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
<li><a href="<?php echo base_url(); ?>boostPhoto/boostPhotoListing"><i class="icon-rocket"></i> <span> Boost - Photo </span></a></li>
<li><a href="<?php echo base_url(); ?>boostVideo/boostVideoListing"><i class="icon-rocket"></i> <span> Boost - Video</span></a></li>
<li><a href="<?php echo base_url(); ?>adsPhoto/adsPhotoListing"><i class="icon-calculator4"></i> <span> ADS - Photo</span></a></li>
<li><a href="<?php echo base_url(); ?>adsVideo/adsVideoListing"><i class="icon-calculator4"></i> <span> ADS - Video</span></a></li>
<br>
<li class="navigation-header"><span>Earning</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
<li><a href=""><i class="icon-width"></i> <span> User Withdrawal Amount</span></a></li>
<li><a href=""><i class="icon-width"></i> <span> Highest Earning</span></a></li>

<li><a href="<?php echo base_url(); ?>transaction/transactionListing"><i class="icon-width"></i> <span> Transaction</span></a></li>
<li><a href="<?php echo base_url(); ?>showpayouts"><i class="icon-width"></i> <span> Payouts</span></a></li>
<br>
<li class="navigation-header"><span>ADS SETUP:</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
<li><a href="<?php echo base_url(); ?>setvideoprice"><i class="icon-width"></i> <span> Video view and click </span></a></li>
<li><a href="<?php echo base_url(); ?>setphotoprice"><i class="icon-width"></i> <span> Photo view and click </span></a></li>
 
<li class="navigation-header"><span>Complain Requests</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
<li><a href="<?php echo base_url(); ?>complains"><i class="icon-width"></i> <span> Complains </span></a></li>
<li class="navigation-header"><span>Manage Categories</span> <i class="icon-menu" title="" data-original-title="Extensions"></i></li>
<li><a href="<?php echo base_url(); ?>subcategories"><i class="icon-width"></i> <span> Sub Categories </span></a></li>
 
 
  </div>
          </div>
          <!-- /main navigation -->

        </div>
      </div>
      <!-- /main sidebar -->

 
 

      
      