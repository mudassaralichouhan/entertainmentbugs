
 <!-- Main content -->
            <div class="content-wrapper">

                <!-- Basic column reorder -->
                <div class="panel panel-flat">
                 
                    <div class="panel-heading">
                        <h5 class="panel-title">All Transactions<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li> 
                            </ul>
                        </div>
                    </div>
 

          <div class="row">
            <div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>


        <!-- Tasks options -->
                    <div class="navbar navbar-default navbar-xs navbar-component" style="padding-right:0px;">
                    <ul class="nav navbar-nav no-border visible-xs-block">
                        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
                    </ul>

                    <div class="navbar-collapse collapse" id="navbar-filter" style="width:60%; float:left">
                    <p class="navbar-text">Filter:</p>
                    <div class="col-md-3">
                    <select id="ddlDist" name="ddlDist" onchange="popDistrictData();" class="nav navbar-nav form-control">
                        <option value="">--Select--</option>
                        <option value="2">Show All</option>
                        <!--<option value="1">UnHold</option>-->
                        <option value="1">Hold</option>
                    </select>
                    <input type="hidden" name="hidden_country" id="hidden_country" />
                    </div>
                       
                    <p class="navbar-text"><strong>From:</strong></p>
                    <div class="col-md-3">
                      <div class="form-group">
                        <input type="date" id="from_date" required name="from_date" class="form-control" id="exampleInputPassword1" placeholder="Enter Date">
                        <p id="mbl" style="color:red"></p>
                      </div>
                    </div>
                    <p class="navbar-text"><strong>To:</strong></p>
                    <div class="col-md-3">
                      <div class="form-group">
                       
                        <input type="date" id="to_date" required name="to_date" class="form-control" id="exampleInputPassword1" placeholder="Enter Date">
                        <p id="mbl" style="color:red"></p>
                      </div>
                    </div>
 
                    </div>

    
                    <div class="panel-body" style="width:40%; float:right;padding:0px;">
                        <form action="<?php echo base_url() ?>transaction/transactionListing" method="POST" class="main-search" id="searchList">
                            <div class="input-group content-group" style="margin-bottom:0px !Important">
                                <div class="has-feedback has-feedback-left">
                                    <input type="text" class="form-control input-xlg" name="searchText" value="<?php echo $searchText; ?>" placeholder="Search">
                                    <div class="form-control-feedback">
                                        <i class="icon-search4 text-muted text-size-base"></i>
                                    </div>
                                </div>

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-primary btn-xlg">Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>

                   <div class="box-body table-responsive no-padding">
                <table class="table datatable-reorder">
                    
                        <thead>
                        <tr> 
                                <th>Thumbnail</th> 
                                <th>Name</th> 
                                <th>Amount</th>  
                                <th>Transacton Type</th> 
                                <th>Date</th>
                        </tr>
                        </thead>
                        <tbody id="inwTblBody">
                            <?php
                            $dateCheck = null;
                            if(!empty($transactionRecords))
                            {
                                foreach($transactionRecords as $k=>$record)
                                {
                            ?>
                            
                         <tr>
                       <?php if(!is_null($dateCheck) && $dateCheck != date("d-m-Y", strtotime($record->created_at))){ ?>
                       
                            <td style="width:84px;display: block;padding: 10px 0px 10px 0;background: #000;color: #fff;font-size: 11px;text-align: center;"><?php echo date("d-m-Y", strtotime($record->created_at)) ?></td> 
                        </tr>
                       <?php }else if($k==0){ ?>
                            <td style="width:84px;display: block;padding: 10px 0px 10px 0;background: #000;color: #fff;font-size: 11px;text-align: center;"><?php echo date("d-m-Y", strtotime($record->created_at)) ?></td> 
                      <?php } 
                      
                      $dateCheck = date("d-m-Y", strtotime($record->created_at));
                      ?>
                        <tr>
                            
                            <tr>
                                <td style="font-weight:bold;">
                             <img class="img-circle img-xs" alt="" style="margin-right:10px" src="<?php echo 'https://entertainmentbugs.com/'; ?>public/uploads/users/small/<?php  echo $record->photo ?>" />
                            </td>
  
                                <td><?php echo $record->name ?></td> 
                                <td>Rs: <?php echo $record->amount ?>/-</td>
                                <td><?php echo $record->type ?></td>
                                <td><?php echo date("d-m-Y", strtotime($record->created_at)) ?></td>
                                    
                            </tr>
                            <?php
                        }
                    } else if(empty($transactionRecords)){ ?>
                       <tr><td><?php echo "Record not Fount...."; ?></td></tr> 
                  <?php  }
                    ?>
                        </tbody>
                     
                    </table>
                </div>

                 <div id="hidden_div" class="box-footer clearfix">
                    <?php  
                    echo $this->pagination->create_links(); ?>
                </div>
        </div>
        <!-- Basic column reorder -->
        </div>
        <!-- Main content -->

         </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

  <script type="text/javascript">
    document.getElementById('ddlDist').addEventListener('change', function () {
    var style = this.value == 5 ? 'block' : 'none';
    //alert(style);
    document.getElementById('hidden_div').style.display = style;
    });

    document.getElementById('to_date').addEventListener('change', function () {
    var style = this.value == 5 ? 'block' : 'none';
    //alert(style);
    document.getElementById('hidden_div').style.display = style;
    });
  </script>

<!-- <script type="text/javascript" src="<?php //echo base_url(); ?>assets/js/common.js" charset="utf-8"></script> -->
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);

            jQuery("#searchList").attr("action", baseURL + "transactionListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>


 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
