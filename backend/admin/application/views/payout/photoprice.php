<!-- Main content -->
<div class="content-wrapper">
   <!-- Basic column reorder -->
   <div class="panel panel-flat">
      <div class="panel-heading">
         <h5 class="panel-title">Photo View and Click Price Setup<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
         <div class="heading-elements">
            <ul class="icons-list">
               <li><a data-action="collapse"></a></li>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <?php
               $this->load->helper('form');
               $error = $this->session->flashdata('error');
               if($error)
               {
               ?>
            <div class="alert alert-danger alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <?php echo $this->session->flashdata('error'); ?>                    
            </div>
            <?php } ?>
            <?php  
               $success = $this->session->flashdata('success');
               if($success)
               {
               ?>
            <div class="alert alert-success alert-dismissable">
               <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
               <?php echo $this->session->flashdata('success'); ?>
            </div>
            <?php } ?>
            <div class="row">
               <div class="col-md-12">
                  <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
               </div>
            </div>
         </div>
      </div>
      <div class="panel panel-flat" style="
         padding: 13px  0 5px 15px;
         ">
         <!-- Tasks options -->
         <!-- /tasks options -->
         <div class="box-body table-responsive no-padding" style="
            width: 50%;
            float: left;
            ">
            <h2 style="
               margin: 15px;
               font-size: 16px;
               font-weight: 500;
               ">Photos recent post views and click amount set</h2>
            <form action="<?php echo site_url('/savephotoviewsandprice')?>" method="POST" class="main-search" id="searchList" style="
               margin-bottom: 65px;
               ">
               <label style="
                  margin: 20px;
                  float: left;
                  ">
                  1 Views  =
                  <lanel></lanel>
               </label>
               <div class="input-group content-group" style="width: 54%;float: left;">
                  <div class="has-feedback has-feedback-left">
                     <input type="text" class="form-control input-xlg" name="photoviews" value="<?php echo $data->views_price??'' ?>" placeholder="value">
                  </div>
                  <div class="input-group-btn">
                     <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
                  </div>
               </div>
            </form>
            <form action="<?php echo site_url('/savephotoviewsandprice')?>" method="POST" class="main-search" id="searchList" style="
               margin-bottom: 61px;
               clear: both;
               ">
               <label style="
                  margin: 13px 23px;
                  float: left;
                  ">
                  1 Click = 
                  <lanel></lanel>
               </label>
               <div class="input-group content-group" style="width: 54%;float: left;">
                  <div class="has-feedback has-feedback-left">
                     <input type="text" class="form-control input-xlg" name="photoclick" value="<?php echo $data->clicks_price??'' ?>"   placeholder="value">
                  </div>
                  <div class="input-group-btn">
                     <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
                  </div>
               </div>
            </form>
         </div>
         <div class="box-body table-responsive no-padding">
            <h2 style="
               margin: 15px;
               font-size: 16px;
               font-weight: 500;
               ">Photos Frontend plan date and  amount set</h2>
            <form action="<?php echo site_url('/savephotoprice')?>" method="POST" class="main-search" id="searchList" style="
               margin-bottom: 24px;
               ">
              <?php foreach($photoprices as $photoprice) { ?>
            <div style="
               clear: both;
               overflow: hidden;
               ">
               <label style="
                  margin: 15px  30px 7px 30px;
                  float: left;
                  ">
                 
                  <lanel></lanel>
               </label>
               <div class="input-group content-group" style="width: 34%;float: left;">
                  <div class="has-feedback has-feedback-left">
                     <input type="text" class="form-control input-xlg" name="price[<?php echo $photoprice->id ?>]" value="<?php echo $photoprice->price ?>" placeholder="Price">
                  </div>
               </div>
                <input style="
                  margin: 0 10px;
                  padding: 9px;
                  width: 178px;"
                  type="number"
                  min="1"
                  max="30"
                  name="days[<?php echo $photoprice->id ?>]"
                  placeholder="No of days"
                  value="<?php echo $photoprice->days ?>"
                  />
            </div>
            <?php }  ?>
               <div class="input-group-btn" style="
                  margin-left: 70px;
                  display: block;
                  margin-top: 10px;
                  ">
                  <button type="submit" class="btn btn-primary btn-xlg">Submit</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>