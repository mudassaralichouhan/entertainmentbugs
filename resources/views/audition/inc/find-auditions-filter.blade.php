<div class="casting-search">
                            <div class="casting-cat-block">
                                <h4>Category</h4>
                               <select id="category" class="extra-ma" style="width:100%;" name="category[]" multiple>
                                    <option value="Theater">Theater</option>
                                    <option value="Voiceover">Voiceover</option>
                                    <option value="Comedian">Comedian</option>
                                    <option value="Photography">Photography</option>
                                    <option value="Video Grapher">Video Grapher</option>
                                    <option value="Makeup Artrist"> Makeup Artrist</option>
            
                                    <optgroup label="Actor / Model">
                                        <option value="Actor">Actor</option>
                                        <option value="Child Actor">Child Actor</option>
                                        <option value="Comedy Actor">Comedy Actor</option>
                                        <option value="Junior Actor">Junior Actor</option>
                                        <option value="Stunt Artist">Stunt Artist</option>
                                        <option value="Drama Artist">Drama Artist</option>
                                        <option value="Mimicry Artist">Mimicry Artist</option>
                                        <option value="Background / Atmosphere">Background / Atmosphere</option>
                                    </optgroup>
                                    <optgroup label="Modeling">
                                        <option value="Model">Model</option>
                                        <option value="Child Model">Child Model</option>
                                        <option value="Pro Model">Pro Model</option>
                                    </optgroup>
                                    <optgroup label="Dancer">
                                        <option value="Dancer">Dancer</option>
                                        <option value="Lead Dancer">Lead Dancer</option>
                                        <option value="Background Dancer">Background Dancer</option>
                                    </optgroup>
                                    <optgroup label="Editor">
                                        <option value="Video Editor">Video Editor</option>
                                        <option value="Music Editor">Music Editor</option>
                                        <option value="Audio Editor">Audio Editor</option>
                                    </optgroup>
                                    <optgroup label="Writer">
                                        <option value="Dialogue Writer">Dialogue Writer</option>
                                        <option value="Script Writer">Script Writer</option>
                                        <option value="Screeplay Writer">Screeplay Writer</option>
                                        <option value="Story Writer">Story Writer</option>
                                        <option value="Lyricists">Lyricists</option>
                                    </optgroup>
                                    <optgroup label="Anchor / Presenter">
                                        <option value="Video Jockey (VJ)">Video Jockey (VJ)</option>
                                        <option value="Radio Jockey (RJ)">Radio Jockey (RJ)</option>
                                        <option value="News Reader">News Reader</option>
                                        <option value="Reporter">Reporter</option>
                                        <option value="Journalist">Journalist</option>
                                    </optgroup>
                                    <optgroup label="Off screen">
                                        <option value="Dubbing Artist">Dubbing Artist</option>
                                        <option value="Voice Over Artist">Voice Over Artist</option>
                                    </optgroup>
                                    <optgroup label="Influncer">
                                        <option value="New Influncer">New Influncer</option>
                                        <option value="Experienced Influncer">Experienced Influncer</option>
                                    </optgroup>
                                    <optgroup label="Advertising Professional">
                                        <option value="Video Advertising">Video Advertising</option>
                                        <option value="Audio Advertising">Audio Advertising</option>
                                        <option value="Banner Advertising">Banner Advertising</option>
                                    </optgroup>
                                    <optgroup label="Singer / Musician">
                                        <option value="Singer">Singer</option>
                                        <option value="Music Instrumentalist">Music Instrumentalist</option>
                                        <option value="Music Composer">Music Composer</option>
                                    </optgroup>
                                    <optgroup label="Proffessional Artist">
                                        <option value="Director Of Photography / Cinematographer">Director Of Photography /
                                            Cinematographer
                                        </option>
                                        <option value="Cameraman">Cameraman</option>
                                        <option value="Camera Operator">Camera Operator</option>
                                        <option value="Light man / Gaffer">Light man / Gaffer</option>
                                        <option value="Makeup Artist">Makeup Artist</option>
                                        <option value="Visual Effects Editor">Visual Effects Editor</option>
                                        <option value="Digital imaging technician">Digital imaging technician</option>
                                        <option value="Special Effects makeup Artist">Special Effects makeup Artist</option>
                                        <option value="Motion control technician">Motion control technician</option>
                                        <option value="Fashion Designer">Fashion Designer</option>
                                        <option value="Hair Stylist">Hair Stylist</option>
                                        <option value="Costume designer">Costume designer</option>
                                        <option value="Grip">Grip</option>
                                        <option value="Sound Designer">Sound Designer</option>
                                        <option value="Sound Grip">Sound Grip</option>
                                        <option value="Production Sound Mixer">Production Sound Mixer</option>
                                        <option value="Production Designer">Production Designer</option>
                                        <option value="Green man">Green man</option>
                                        <option value="Property master">Property master</option>
                                        <option value="Weapons master">Weapons master</option>
                                        <option value="Set Designer / Decorator">Set Designer / Decorator</option>
                                        <option value="Location Manager">Location Manager</option>
                                        <option value="StoryBoard Artist">StoryBoard Artist</option>
                                    </optgroup>
                                    <optgroup label="Others">
                                        <option value="Others">Others</option>
                                    </optgroup>
                                </select>
                            </div>
                            
                            @if(isset($request['category']))
                    <script>
                        @foreach($request['category'] as $cat)
                            $("#category option").each(function(){
                              if ($(this).text() == "{{$cat}}")
                                $(this).attr("selected","selected");
                            });
                        @endforeach

                      </script>
                    @endif
                            <div class="casting-cat-block">
                                <h4>Location</h4>
                                    <input list="list-location-cities" class="form-control" id="location-cities" name="location-cities"
                                       placeholder="city" autocomplete="off">
                                <datalist id="list-location-cities">
                                </datalist>
                            </div>
                            @if(isset($request['city']))
                            <?php echo '<script>$("#location-cities").val("'.$request['city'].'")</script>'; ?>
                            @endif
                            <div class="casting-cat-block">
                                <h4>Language</h4>
                                <select id="video-language" class="extra-ma" style="width:100%;" name="language[]" multiple>
                                     @foreach(\App\Models\Language::all() as $language)
                                        @if(isset($request['lang']))
                                            @foreach($request['lang'] as $lang)
                                                @if($lang==$language->name)
                                                    <option value="{{$language->name}}" selected>{{$language->name}}</option>
                                                @else 
                                                    <option value="{{$language->name}}">{{$language->name}}</option>
                                                @endif
                                            @endforeach
                                        @else
                                                <option value="{{$language->name}}">{{$language->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="casting-cat-block">
                                <h4>Search By </h4>
                               <input type="text" id="title-search" style="margin: 0px 14px 0 12px;width: 91%;height: 31px;" placeholder="Title">
                            </div>
                             @if(isset($request['title']))
                            <?php echo '<script>$("#title-search").val("'.$request['title'].'")</script>'; ?>
                            @endif
                        
                        
                        
                        
                        
                        
<style>
.casting-agency-main {float: none;width: 100%;margin: 0px 0 10px 0;}
.casting-agency-main {background:#fff}
.casting-agency-info h5 {text-overflow: ellipsis;white-space: nowrap;display: block;width: 100%;overflow: hidden;margin: 19px 0 2px 0;font-size: 13px;font-weight: normal;}
.casting-agency-info p {text-overflow: ellipsis;white-space: nowrap;display: block;width: 100%;overflow: hidden; font-size: 11px;line-height: 16px;}
.casting-agency-photo-block img {height: 70px;}
.casting-agency-photo-block {width: 70px;}
.casting-agency-info {float: left;width: 65%;margin-left: 4%;}
.casting-agency-main {margin: 0px 0 6px 0;}
</style>             
            
       
       
<style>
.audition-ads{padding-top:15px;background:#eaeaea}
.audition-ads #ads-banner-img{width:100% !important;}
.audition-ads #ads-banner-img-content {width: 100% !important;padding:7px 9px 32px 9px !important;}
.audition-ads .ads-trigger-btn{width:95% !Important; right:0px !Important;     bottom: 2px !Important}
.audition-ads #ads_name{bottom: 28px  !Important;right: 3%  !Important;font-size: 10px  !Important;padding: 5px 8px 0px 8px !Important;}
</style>

<div class="audition-ads">  
<div data-ad-block data-ad-type='ad' data-ad-mode='image' data-ad-size='336x280'></div>
</div>
</div>     

                  
                        
                        