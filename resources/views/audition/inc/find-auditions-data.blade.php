  <div class="casting-search-result">
      
      
<!-- Ads Start-->      
      
<style>
.audition-ads-right{padding-top:15px;background:#eaeaea}
.audition-ads-right #ads-banner-img{width:9% !important;}
.audition-ads-right #ads-banner-img-content {width: 91% !important;padding:7px 9px 2px 9px !important}
.audition-ads-right #ads-banner-img-content h6{padding: 0px 0 2px 0 !Important;}
.audition-ads-right .ads-trigger-btn{width: 11% !Important;right: 6.5% !Important;bottom: 10px !Important;}
.audition-ads-right #ads_name{ right: 1%  !Important; }
 @media  only screen and (max-width: 767px) {
.audition-ads-right #ads-banner-img{width:30% !important;}
.audition-ads-right #ads-banner-img-content {width: 70% !important;padding:7px 9px 2px 9px !important}
.audition-ads-right  p{height: 24px;overflow: hidden;margin-bottom: 7px;}
.audition-ads-right #ads-banner-img-content h6 {padding: 0px 0 4px 0 !Important;height: 34px;overflow: hidden;}
.audition-ads-right .tags_white{display:none !Important;}
.audition-ads-right .tags_language{display:none !Important;}
.audition-ads-right .ads-trigger-btn {width: 27% !Important;right: 38.5% !Important;bottom: 8px !Important;}
#find_auditions_data .casting-search-result .col-lg-12.no-psd-desk0.oad091 {font-size: 11px;margin: 4px 0 !important;width: 100% !important;padding: 14px 14px 10px 3px !important;}
#auditions .width-space{display:none;}
 }

</style>            

<div class="audition-ads-right">  
<div data-ad-block data-ad-type='ad' data-ad-mode='image' data-ad-size='1080x1080'></div>
</div>   
<!-- Ads End-->
                    
                    
                             @include('audition.inc.find-auditions-filter-info')
                            <div class="row home_page_seventh_block" style="margin:0px;">
                                <div class="col-lg-12">
                                    <!-- Popular Channels -->
                                    <div class="content-block">
                                        <div id="production-house-main">
                                            
                                            @foreach($auctions as $auction)
                                                @if(!\Carbon\Carbon::parse($auction->valid_till)->isPast() && $auction->status != 'close')
                                                    @php
                                                     $production_about = DB::table('production_about')->where(['user_id'=>$auction->user_id,'isDeleted'=>0])->get();
                                                       
                                                       
                                                    @endphp
                                                    @if(count($production_about)>0)
                                                    <div class="production-all-display">
                                                        <div class="production-hs1">
                                                            <a href="{{url('/audition/'.$auction->slug)}}">
                                                                <img src="<?php echo (count($production_about) > 0 ? (isset($production_about[0]->profile_photo) ? $production_about[0]->profile_photo : '')  : '' );?>" alt="{{$auction->title}}"></a>
                                                        </div>
                                                        <div class="production-hs2">
                                                            <h5>{{$auction->title}}</h5>
                                                            @if(\Carbon\Carbon::parse($auction->valid_till)->isPast())
                                                            <span style="text-align:right;color: #fff !important;margin-top: -44px;float:right;background: red !important;padding: 6px 15px 3px 15px;">Expired</span>
                                                            @endif
                                                            <p class="prd-info"> {!! strlen($auction->description)>200?substr($auction->description,0,200).'<a href="'.url('/audition/'.$auction->slug).'">Read
                                                                    More</a>':$auction->description !!} </p>
                                                            <div class="sp1" id="auditions">
                                                                <p><span><i class="fa fa-list-alt"></i> Category: </span>
                                                                    {{$auction->category}}</p>
                                                                <div class="width-space"></div>
                                                                <div class="line-0">|</div>
                                                                <div class="width-space"></div>
                                                                <p><span><i class="fa fa-map-marker"></i> Location: </span>
                                                                    {{$auction->city}}- {{$auction->state}} - {{$auction->country}} </p>
                                                                <div class="width-space"></div>
                                                                <div class="line-0">|</div>
                                                                <div class="width-space"></div>
                                                                <p><span><i class="fa fa-calendar"></i> Posted on: </span> {{\Carbon\Carbon::parse($auction->posted_date)->diffForHumans()}}</p>
                                                                <div class="clear"></div>
                                                                <p><span><i class="fa fa-user"></i> Posted By: </span> {{$auction->post_by_name}}</p>
                                                                <div class="width-space"></div>
                                                                <div class="line-0">|</div>
                                                                <div class="width-space"></div>
                                                                <p><span><i class="fa fa-language"></i>  Language: </span>
                                                                    {{$auction->language}}</p>
                                                                <div class="clear"></div>
                                                                <p>
                                                                    <span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span>
                                                                    {{\Carbon\Carbon::parse($auction->valid_till)->diffForHumans()}}</p>
                                                            </div>
                                                            @if( $auction->isApplied() )
                                                                <a href="javascript:void(0)" class="">Applied</a>
                                                            @endif
                                                            @if(!(\Carbon\Carbon::parse($auction->valid_till)->isPast()))
                                                                <a href="{{url('/audition/'.$auction->slug)}}" class="view-m">View Now</a>
                                                            @endif
                                                            <div class="clear"></div>
                                                        </div>
                                                        <div class="clear"></div>
                                                        
                                                        
                                                    </div>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </div>
                                        {{$auctions->links()}}
                                    </div>
                                </div>
                                <!-- /Popular Channels -->
                            </div>
                            
                  
                            
                            
                            
                            
                            
                        </div>