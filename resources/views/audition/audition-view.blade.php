@extends('layouts.home')
@section('meta_tags')
    <meta name="description" content="{{ $getIfAlready[0]->description }}" />
    <meta property="og:title" content="{{ $getIfAlready[0]->title }}" />
    <meta property="og:description" content="{{ $getIfAlready[0]->description }}" />
    <meta property="og:image" content="{{ $production_about[0]->profile_photo ?? 'https://1zhit3xkjyq3ki6yq3sav7ahhz-wpengine.netdna-ssl.com/wp-content/uploads/2018/11/Our-Lady-LTD-Season-1-Epix-400x190.png' }}"></a> 
@endsection
@section('content')
  <link href="{{ asset('public/css/casting.css') }}" rel="stylesheet">
<style>
    #live-video-user-list-home{display:none;}
</style>
<style>
    .share-option {position: absolute;right: -20px;top: -21px;width: 150px;line-height: 32px;}
    #audition_main .h-icons{float:right;}
    .share-option a{color: #fff !important;background: #f72e5e !important;width: 30px;height: 30px;margin: 0 4px 0 0;display: block;text-align: center;line-height: 35px;border-radius: 42px;float: right;}
</style>
<!-- font-family: 'Hind', sans-serif; -->
<link href='https://fonts.googleapis.com/css?family=Hind:400,300,500,600,700|Hind+Guntur:300,400,500,700' rel='stylesheet' type='text/css'>
</head>
<div class="content-wrapper" >
    <div class="audition_main_list" id="audition_main">
        <div class="container">
            @if (Session::has('success'))
                <div class="alert alert-success">
                    <span>{{ Session::get('success') }}</span>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    {!! implode('', $errors->all('<div>:message</div>')) !!}
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    <!-- Featured Videos -->
                    <div class="content-block">
                        <div class="casting-search-result">
                            <div class="row home_page_seventh_block" style="margin:0px;">
                                <div class="col-lg-12">
                                    <?php 
                                        $getUserDetail = DB::table('users')->where(['name'=>$username,'isDeleted'=>0])->get();
                                        if(count($getUserDetail) > 0)
                                        {
                                            $userid=$getUserDetail[0]->id;
                                        }else{
                                            $userid=0;
                                        }
                                        $production_about = DB::table('production_about')->where(['user_id'=>$getIfAlready[0]->user_id , 'isDeleted'=>0])->get();
                                       
                                    ?>
                                    <!-- Popular Channels -->
                                    @if(count($production_about)>0)
                                    <div class="content-block">
                                        <div id="production-house-main">
                                            <div class="production-all-display">
                                                <div class="production-hs1"> 
                                                    <a href="javascript:void(0)"> 
                                                    <img src="{{ $production_about[0]->profile_photo ?? 'https://1zhit3xkjyq3ki6yq3sav7ahhz-wpengine.netdna-ssl.com/wp-content/uploads/2018/11/Our-Lady-LTD-Season-1-Epix-400x190.png' }}"></a> 
                                                </div>
                                                <div class="production-hs2">
                                                    <div class="sp1" id="auditions">
                                                        <h5><?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->title != NULL || $getIfAlready[0]->title != '') ? $getIfAlready[0]->title : '') : '')); ?></h5>
                                                        <p><span><i class="fa fa-list-alt"></i> Category: </span> <?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->category != NULL || $getIfAlready[0]->category != '') ? $getIfAlready[0]->category : '') : '')); ?></p>
                                                        <div class="width-space"></div>
                                                        <div class="line-0">|</div>
                                                        <div class="width-space"></div>
                                                        <p><span><i class="fa fa-map-marker"></i> Location: </span>  <?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->city != NULL || $getIfAlready[0]->city != '') ? $getIfAlready[0]->city : '') : '')); ?></p>
                                                        <div class="width-space"></div>
                                                        <div class="line-0">|</div>
                                                        <div class="width-space"></div>
                                                        <p><span><i class="fa fa-calendar"></i> Posted on: </span>  <?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->posted_date != NULL || $getIfAlready[0]->posted_date != '') ? date('d F, Y',strtotime($getIfAlready[0]->posted_date)) : '') : '')); ?></p>
                                                        <div class="clear"></div>
                                                        <p><span><i class="fa fa-briefcase"></i>  Production House: </span> {{$getIfAlready[0]->productionHouse->name}}</p>
                                                        <div class="width-space"></div>
                                                        <div class="line-0">|</div>
                                                        <div class="width-space"></div>
                                                        <p><span><i class="fa fa-user"></i> Posted By: </span> {{$getIfAlready[0]->post_by_name}}</p>
                                                        <div class="clear"></div>
                                                        <p><span><i class="fa fa-language"></i>  Language: </span> <?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->language != NULL || $getIfAlready[0]->language != '') ? $getIfAlready[0]->language : '') : '')); ?></p>
                                                        <div class="width-space"></div>
                                                        <div class="line-0">|</div>
                                                        <div class="width-space"></div>
                                                        <p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> <?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->valid_till != NULL || $getIfAlready[0]->valid_till != '') ? date('d F, Y',strtotime($getIfAlready[0]->valid_till)) : '') : '')); ?></p>
                                                        <div class="width-space"></div>
                                                        <div class="line-0">|</div>
                                                        <div class="width-space"></div>
                                                        <p><span><i class="fa fa-eye"></i>Views: </span>{{ count($views) ?? '0' }}</p>
                                                    </div>
                                                    <div class="share-option">
                                                        Share:
                                                        <div class="h-icons">
                                                            <a target="_blank" href="instagram://library?AssetPath={{$getIfAlready[0]->title}}"><i class="fa fa-instagram" data-toggle="tooltip" data-placement="top" title=""></i></a>
                                                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current())."&amp;quote=".urlencode($getIfAlready[0]->title)."%0A%0A".urlencode($getIfAlready[0]->description)."%0A%0AKnow more at:" }}"><i class="fa fa-facebook" data-toggle="tooltip" data-placement="top" title=""></i></a>
                                                            <a target="_blank" href="https://api.whatsapp.com/send?text={{ urlencode($getIfAlready[0]->title)."%0A%0A".urlencode($getIfAlready[0]->description)."%0A%0AKnow more at:".urlencode(url()->current()) }}"><i class="fa fa-whatsapp" data-toggle="tooltip" data-placement="top" title=""></i></a> 
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="aud-de">
                                                <h5 style="margin-top:0px;"> INFORMATION  </h5>
                                                <p  class="prd-info"> <?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->description != NULL || $getIfAlready[0]->description != '') ? $getIfAlready[0]->description : '') : '')); ?></p>
                                                <h5> SYNOPSIS </h5>
                                                <p  class="prd-info"> <?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->synopsis != NULL || $getIfAlready[0]->synopsis != '') ? $getIfAlready[0]->synopsis : '') : '')); ?></p>
                                                <h5> ELIGIBILITY CRITERIA </h5>
                                                <p><?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->eligibility_criteria != NULL || $getIfAlready[0]->eligibility_criteria != '') ? $getIfAlready[0]->eligibility_criteria : '') : '')); ?></p>
                                                <img src="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->photo != NULL || $getIfAlready[0]->photo != '') ? $getIfAlready[0]->photo : 'https://1zhit3xkjyq3ki6yq3sav7ahhz-wpengine.netdna-ssl.com/wp-content/uploads/2018/11/Our-Lady-LTD-Season-1-Epix-400x190.png') : 'https://1zhit3xkjyq3ki6yq3sav7ahhz-wpengine.netdna-ssl.com/wp-content/uploads/2018/11/Our-Lady-LTD-Season-1-Epix-400x190.png')); ?>" style="width:50%;"> 
                                                <div class="clear"></div>
                                                @if( !$getIfAlready[0]->isApplied() )
                                                    <form method="POST" action="{{ url('apply-for-audition/'.$getIfAlready[0]->id) }}">
                                                        {{ csrf_field() }}
                                                        <input type='hidden' name='action' value='apply-audition' />
                                                        <button type='submit' name='apply' class="apply-n" style='background: #28b47e;border:#28b47e;color: #fff !important;padding: 6px 42px 4px 42px;'>Apply Now</button>
                                                        <a href="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->whatsapp_number != NULL || $getIfAlready[0]->whatsapp_number != '') ? ('https://api.whatsapp.com/send?phone='.$getIfAlready[0]->whatsapp_number) : 'javascript:void(0)') : 'javascript:void(0)')); ?>" class="apply-n" style="display: inline-block">Contact me on whatsapp</a>  
                                                    </form>
                                                @else
                                                    <a href="javascript:void(0)" class="apply-n active" style="display: inline-block">Applied</a>
                                                    <a href="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->whatsapp_number != NULL || $getIfAlready[0]->whatsapp_number != '') ? ('https://api.whatsapp.com/send?phone='.$getIfAlready[0]->whatsapp_number) : 'javascript:void(0)') : 'javascript:void(0)')); ?>" class="apply-n" style="display: inline-block">Contact me on whatsapp</a>
                                                @endif
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    @else
                                    <h4>This Content Is under Review</h4>
                                    @endif
                                </div>
                                <!-- /Popular Channels -->
                            </div>
                        </div>
                        <div class="casting-search">
                            <div class="casting-cat-block">
                                <h4>Auditions</h4>
                            </div>
                            
                            @if( $sideAuditions && count($sideAuditions) > 0 )
                                @foreach ($sideAuditions as $audition)
                                    <div class="production-all-display" id="right-side-bar">
                                        <div class="production-hs1">
                                            <a href="" class="ic"> 
                                                <img src="{{ $audition->photo ? $audition->photo : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU" }}">
                                            </a> 
                                            <h5>{{ $audition->title }}</h5>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="production-hs2">
                                            <p class="prd-info"> {{ $audition->description }}</p>
                                            <div class="sp1" id="auditions">
                                                <p><span><i class="fa fa-list-alt"></i> Category: </span> {{ $audition->category }}</p>
                                                <div class="clear"></div>
                                                <p><span><i class="fa fa-map-marker"></i> Location: </span>  {{ $audition->city.", ".$audition->state.", ".$audition->country }}</p>
                                                <div class="clear"></div>
                                                <p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> {{ \Carbon\Carbon::parse($audition->valid_till)->format("M d Y") }}</p>
                                            </div>
                                            <a href="{{ url("audition/".$audition->slug) }}" class="view-m">View Now</a> 
                                            <div class="clear"></div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                @endforeach
                            @endif
                            <div  id="view-all-auditions">
                                <a href="{{ url('') }}">View All Auditions </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--casting video END--> 
            </div>
        </div>
        <!-- /Featured Videos -->
    </div>
</div>
</div>
</div>
@endsection