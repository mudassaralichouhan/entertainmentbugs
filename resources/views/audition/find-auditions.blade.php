@extends('layouts.home')

  @section('meta_tags')
    <meta name="description" content="Search the latest open casting calls, entertainment gigs, acting auditions, Influencers and jobs for union and non-union actors.">
    <meta name="keywords" content="Find Audition, Auditions, Post Audition, Audition, Actor, Acting, Casting, Casting, Casting Director, Auditioning, Search Audition">
    <meta name="author" content="<?php echo ((isset($author) ? $author : 'Auditions - Entertainment Bugs')); ?>">
    <meta property="og:site_name" content="{{URL::to('/')}}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="<?php echo ((isset($ogtitle) ? $ogtitle : 'Auditions - Entertainment Bugs')); ?>"/>
    <meta property="og:description" content="<?php echo ((isset($ogdescription) ? $ogdescription : 'Search the latest open casting calls, entertainment gigs, acting auditions, Influencers and jobs for union and non-union actors.')); ?>"/>
    <meta property="og:url" content="<?php echo url()->current(); ?>"/>
    <meta property="og:image" content="<?php echo ((isset($ogimage) ? $ogimage : 'https://entertainmentbugs.com/balu_development/public/uploads/logo/logo-social-media.png')); ?>"/><!--profile image-->
    <meta name="keywords" content="<?php echo ((isset($keywords) ? $keywords : 'Find Audition, Auditions, Post Audition, Audition, Actor, Acting, Casting, Casting, Casting Director, Auditioning, Search Audition')); ?>">
 
   @endsection
   
   
@section('content')
  <link href="{{ asset('public/css/casting.css') }}" rel="stylesheet">
<style>



.find_audtion_active i{    color: #f72e5e !important;}
.find_audtion_active span{    color: #f72e5e !important;}

.casting-cat-block {margin-bottom: 25px;padding: 0 20px;}  
.form-control {padding: 9px 18px;height: 36px;}  
#find_auditions_data .casting-search-result .col-lg-12.no-psd-desk0.oad091{font-size: 11px; margin: 20px 20px 4px 20px !important;width: 96%;padding: 14px 14px 10px 14px;background: #eaeaea;} 
</style>



<!--<link href="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/css/example-styles.css" rel="stylesheet">-->
<!--<script src="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/js/jquery.multi-select.js"></script>-->
<!--<link href="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/css/example-styles.css" rel="stylesheet">-->
<!--<script src="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/js/jquery-2.2.4.min.js"></script>-->
<!--<script src="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/js/jquery.multi-select.js"></script>-->


<link href="https://www.entertainmentbugs.com/public/new-js/example-styles.css" rel="stylesheet">
<script src="https://www.entertainmentbugs.com/public/new-js/jquery-2.2.4.min.js"></script>
<script src="https://www.entertainmentbugs.com/public/new-js/jquery.multi-select.js"></script>


  <!--@section('meta_tags')-->
  <!--  <meta name="description" content="Be updated with the stories from around the world.">-->
  <!--  <meta name="keywords" content="Stories, News, Blog, Cars & Vehicles,  Comedy ,  Education ,  Entertainment,  Film & Animation,  Gaming ,  Music,  News & Politics ,  Non-profits & Activism ,  People & Blogs ,  Pets & Animals,  Science & Technology,  Serials,  Sports ,  Travel & Events">-->
  <!--  <meta name="author" content="<?php echo ((isset($author) ? $author : 'Stories - Entertainment Bugs')); ?>">-->
  <!--  <meta property="og:site_name" content="{{URL::to('/')}}"/>-->
  <!--  <meta property="og:locale" content="en_US"/>-->
  <!--  <meta property="og:title" content="<?php echo ((isset($ogtitle) ? $ogtitle : 'Stories - Entertainment Bugs')); ?>"/>-->
  <!--  <meta property="og:description" content="<?php echo ((isset($ogdescription) ? $ogdescription : 'Be updated with the stories from around the world.')); ?>"/>-->
  <!--  <meta property="og:url" content="<?php echo url()->current(); ?>"/>-->
  <!--  <meta property="og:image" content="<?php echo ((isset($ogimage) ? $ogimage : 'https://www.ntertainmentbugs.com/balu_development/public/uploads/logo/logo-social-media.png')); ?>"/><!--profile image-->-->
  <!--  <meta name="keywords" content="<?php echo ((isset($keywords) ? $keywords : 'Stories, News, Blog, Cars & Vehicles,  Comedy ,  Education ,  Entertainment,  Film & Animation,  Gaming ,  Music,  News & Politics ,  Non-profits & Activism ,  People & Blogs ,  Pets & Animals,  Science & Technology,  Serials,  Sports ,  Travel & Events')); ?>">-->
  <!-- @endsection-->
   
   
   

{{ HTML::style('public/css/bootstrap-tagsinput.css')}}
{{ HTML::script('public/js/bootstrap-tagsinput.js')}}
{{ HTML::script('public/js/bootstrap3-typeahead.js')}}
      
      
 
<style>
#live-video-user-list-home {display: none;}
.casting-menu ul li a {letter-spacing: 0.1px;color: #fff;font-size: 12px;padding: 16px 18px 13px 18px;display: block;}



  #rewards_main_page {
        padding: 40px 0 10px 0
    }

    #rewards_main_page ul li {
        font-size: 24px;
        line-height: 64px;
    }

    #home_page_video_new .cb-content {
        margin-bottom: 0;
    }

    #audition_main {
        padding: 20px 0 40px 0;
    }

    .audition_main_list h3 {
        font-size: 24px;
        text-transform: uppercase;
        text-align: center;
        margin: 40px 0 20px 0;
    }

    .cat-1 a {
        width: 19%;
        margin: 0.5%;
        float: left;
        background: #f72e5e !important;
        color: #fff;
        line-height: 100px;
        display: block;
        text-align: center;
        font-size: 16px;
    }

    .cat-1 {
        margin-bottom: 20px;
    }

    .cat-1 {
        border-top: solid 1px #eceff0;
        padding: 15px 10px 0px 10px;
    }

    #audition_main .content-block.head-div {
        border-top: solid 1px #eceff0;
    }

    #audition_main .home_page_second_block {
        border: 0px solid #e3e3e3;
    }

    #audition_main {
        background: #eaeaea
    }

    #casting-head {
        background: #fff
    }

    #casting-head {
        margin-bottom: 20px;
    }

    .photo-block {
        width: 14%;
        margin: 0 0%;
        padding: 0 4px;
        float: left
    }

    .cb-content {
        clear: both;
        overflow: hidden
    }

    .photo-block img {
        width: 100%;
    }

    .home_page_second_block {
        margin-top: 20px;
    }

    .middle-head {
        font-size: 22px;
        margin: 30px 0 0 -13px
    }

    .casting-home-sep {
        padding-top: 20px;
    }

    .casting-home-sep .home_page_second_block {
        margin-top: 13px;
    }

    #audition_main .casting-home-sep .content-block.head-div {
        border-top: solid 0px #eceff0;
    }

    #blog {
        padding: 0 20px
    }

    .top-banner {
        position: absolute;
        left: 100px;
        top: 25%;
        width: 45%;
    }

    .top-banner h2 {
        color: #fff;
        font-size: 44px;
    }

    .top-banner a {
        background-color: #f72e5e;
        color: white;
        padding: 7px 0;
        border: solid 1px #f72e5e;
        margin-top: 10px;
        font-size: 18px;
        border-radius: 24px;
        font-weight: 500;
        display: block;
        width: 209px;
        text-align: center;
        height: 48px;
        line-height: 36px;
    }

    .casting-menu {
        width: 100%;
    }

    .casting-menu ul {
        list-style: none;
        padding: 0px;
        text-align: center;
        background: #f72e5e;
        margin: 0px;
    }

    .casting-menu ul li {
        display: inline-block;
        text-align: center;
    }



    .casting-menu ul li a:hover {
        background: #c42047
    }

    #result-block-main .photo-block {
        width: 20%;
    }

    .casting-search {
        width: 21%;
        float: left;
        background: #fff;
        padding: 0px;
        min-height: 320px;
    }

    .casting-search-result {
        width: 78%;
        float: right
    }

    .casting-cat-block a {
        display: block;
        margin-left: 15px;
        margin-bottom: 3px;
    }

    .casting-cat-block h4 {
           background: #f72e5e !important;
    color: #fff;
    padding: 10px 10px 6px 10px;
    font-size: 16px;
    margin: 0 -20px 11px -20px;
    }

    .casting-cat-block {
        margin-bottom: 15px
    }
#title-search{margin-left:0px !Important;}
    .multi-select-menuitem {
        display: block;
        white-space: nowrap;
        font-weight: normal;
        font-size: 12px;
        padding: 8px 0 0 30px;
        margin: 0 0 7px 0;
    }

    .extra-ma {
        margin: 10px 10px -5px 10px;
    }

    #production-house-main {
        padding-top: 10px;
        padding-bottom: 20px;
    }

    .production-hs1 {
        width: 15%;
        float: left;
    }

    .production-hs1 img {
        width: 100%
    }

    .production-hs2 {
        width: 83%;
        float: right
    }

    .production-hs2 h5 {
        color: #f72e5e !important;
        font-size: 18px;
        margin: 2px 0 5px 0;
    }

    .production-hs2 p.prd-info {
        font-size: 14px;
        margin: 5px 0 8px 0
    }

    .production-hs2 .sp1 {
        display: flex;
        color: #6e6f70;
        margin-bottom: 4px;
        font-size: 14px;
    }

    .production-hs2 span {
        margin-right: 5px;
        color: #f72e5e !important;
        font-weight: 500
    }

    .width-space {
        width: 15px;
    }

    .production-hs2 a {
        color: #f72e5e !important
    }

    .production-all-display {
        border-bottom: 1px solid #e5e5e5;
        padding: 25px 10px 16px 10px;
    }

    .production-hs1 img {
        border: 2px solid #eaeaea;
    }

    #auditions {
        display: block
    }

    #auditions p {
        margin: 0 0 8px;
        float: left;
    }

    .width-space, .line-0 {
        float: left;
    }

    #auditions .width-space {
        margin: 0 0px;
        height: 1px;
    }

    .production-hs2 {
        position: relative;
    }

    .production-hs2 a.view-m {
        position: absolute;
        right: 0px;
        bottom: 7px;
        background: #28b47e !important;
        color: #fff !important;
        padding: 6px 22px 4px 22px;
    }

    .production-hs1 a {
        display: block;
        overflow: hidden;
        width: 105px;
        margin: 11px auto 0;
        height: 105px;
    }

    .production-hs1 img {
        border-radius: 500px;
        object-fit: cover;
        width: 100%;
        height: 100%;
    }

    .production-all-display:nth-child(even) {
        background: #f1efef;
    }
    
     .multi-select-button {
        border: 1px solid #e0e1e2;
        border-radius: 0px;
        box-shadow: 0 0px 0px rgb(0 0 0 / 20%);
        height: 31px;
        width: 100%;
        max-width: 95%;
    }

    .multi-select-container {
        width: 99% !important;
        border-radius: 0px !important;
        line-height: 26px;
    }

    .multi-select-menuitem {
        display: block;
        font-size: 0.875em;
        padding: 0.6em 1em 0.6em 30px;
        white-space: nowrap;
        font-weight: normal;
        padding: 1px 31px;
        margin: 1px 0 -6px 0;
    }
</style>
<style>


    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 22px 0;
        border-radius: 4px;
        width: 100%;
        text-align: center;
    }


    .pagination li a {
        color: black;
        float: none;
        padding: 5px 16px 1px 15px;
        text-decoration: none;
        transition: background-color .3s;
        text-align: center;
        font-size: 15px;
    }

    .pagination li span {
        color: black;
        float: none;
        padding: 5px 16px 1px 15px;
        text-decoration: none;
        transition: background-color .3s;
        text-align: center;
        font-size: 15px;
    }

    .pagination .active {
        background-color: #f72e5e;
        color: white;
    }

    .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
        background-color: #f72e5e;
        color: white;
        border-color: #f72e5e;
    }

    .pagination > .disable {
        pointer-events: none;
    }

    .content-block .cb-content {
        margin-bottom: 0;
    }

    .pagination a:hover:not(.active) {
        background-color: #ddd;
    }
</style>



 <div class="content-wrapper menu-top1">
    @if( Session::has("status") )
        <div class="alert alert-success">{{ Session::get('status') }}</div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            {!! implode('', $errors->all('<div>:message</div>')) !!}
        </div>
    @endif
 <?php
    
                         $userid=session()->get('user_id');
     if($userid != ''){
            $artist_about=\App\Models\ArtistAbout::where('user_id',$userid)->get();
                    $progress = 0;
                 if($artist_about->count() > 0 ){
                     $progress = 1;
                 }
                 if($progress){
                     ?>
                     <style>#user-menu{display:none}</style>
                     
                              <div class="containers" id="">
            <div class="casting-menu">
               <ul>
                    <li><a href="{{URL::to('artist-profile')}}"> Artist Profile</a></li>
                    <li><a href="{{URL::to('artist-videos')}}"> Artist Videos</a></li>
                    <li><a href="{{URL::to('artist-gallery')}}"> Artist Gallery</a></li> 
                    <li><a href="https://www.entertainmentbugs.com/artist-profile?page=1&category[]=New%20Influncer&category[]=Experienced%20Influncer&gender=Select&city=&followers=Select&min_experience=Min:&max_experience=Max:&min_age=&max_age=&min_height=&max_height=&min_weight=&max_weight=&artist_name=">Influencer</a></li>
                    <li><a href="https://www.entertainmentbugs.com/artist-profile?page=1&category[]=Actor&category[]=Child%20Actor&category[]=Comedy%20Actor&category[]=Junior%20Actor&category[]=Stunt%20Artist&category[]=Drama%20Artist&category[]=Model&category[]=Child%20Model&category[]=Pro%20Model&gender=Select&city=&followers=Select&min_experience=Min:&max_experience=Max:&min_age=&max_age=&min_height=&max_height=&min_weight=&max_weight=&artist_name=">Actor / Model</a></li>
                    <!--<li><a href="{{URL::to('production-houses')}}">Production House</a></li>-->
                    <li><a href="{{URL::to('find-auditions')}}">Auditions</a></li> 
               </ul>
            </div>
         </div>           
                <?php 
                 } 
                 
                     $product_about=\App\Models\ProductionAbout::where(['user_id'=>$userid,'isDeleted'=>0])->get();
                $progress = 0;
                if( $product_about->count() > 0){
                     $progress = 1;
                 }
                if($progress){
                ?>
                 <style>#user-menu{display:none}</style>
         
                <?php 
                }
            }//check if user login ?>   
            
            <div class="containers" id="user-menu">
            <div class="casting-menu">
               <ul>
                     <li><a href="{{URL::to('artist-profile')}}"> Artist Profile</a></li>
                    <li><a href="{{URL::to('artist-videos')}}"> Artist Videos</a></li>
                    <li><a href="{{URL::to('artist-gallery')}}"> Artist Gallery</a></li> 
                    <li><a href="">Influencer</a></li>
                    <li><a href="">Actor / Model</a></li>
                    <li><a href="{{URL::to('production-houses')}}">Production House</a></li>
                    <li><a href="{{URL::to('find-auditions')}}">Find Autions</a></li> 
                    <li><a href="{{URL::to('post-audition')}}">Post Aution</a></li> 
               </ul>
            </div>
         </div>
         </div>   













<div class="content-wrapper">
    <div class="audition_main_list" id="audition_main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12" >
                    <!-- Featured Videos -->
                   
                    <div class="content-block">
                        @include('audition.inc.find-auditions-filter')
                        <div id="find_auditions_data">
                            @include('audition.inc.find-auditions-data')   
                        </div>
                    </div>
                    <!-- Featured Videos -->
                </div>
                <!--casting video END-->
            </div>
        </div>
        <!-- /Featured Videos -->
    </div>
</div>








</div>
</div>


<script>


function getMoreUsers(page) {
      var selectedCategory = $("#category :selected").map(function(i, el) {return $(el).val();}).get();
      var selectedLanguage = $("#video-language :selected").map(function(i, el) {return $(el).val();}).get();
      var selectedLocations = $("#location-cities").val();
      var searchedTitle=$("#title-search").val();



      $.ajax({
        type: "GET",
        url: "{{ route('api.search.find-auditions.re-render') }}" + "?page=" + page,
        data: {
          'category':selectedCategory,
          'lang': selectedLanguage,
          'city': selectedLocations,
          'title': searchedTitle,
        },
        success:function(data) {
            var cat="";
            selectedCategory.forEach(function(item) {
                cat+="&category[]="+item;
            });
            var lang="";
            selectedLanguage.forEach(function(item) {
                lang+="&lang[]="+item;
            });
          $('#find_auditions_data').html(data);
          var full=window.location.href.split('?')[0];
          var finalurl=full+"?page=" + page+cat+lang+"&city="+selectedLocations+'&title='+searchedTitle;
          
          window.history.pushState(full, '{{$title}}',finalurl );

        }
      });
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    function initializefields(){
        $(document).on('click', '.pagination a', function(event) {
          event.preventDefault();
          var page = $(this).attr('href').split('page=')[1];
          getMoreUsers(page);
        });
        
        $("#video-language").on('change', function() {
          getMoreUsers(1);
        });
        
        $("#location-cities").on('change', function() {
          getMoreUsers(1);
        });
        
        $("#title-search").on('change', function() {
          getMoreUsers(1);
        });
        
        $('#category').on('change', function() {
          getMoreUsers(1);
        });


        $('#category').multiSelect();
        $('#video-language').multiSelect();
   
        $('#location-cities').keyup(function () {
            var elem = $(this);
            if (elem.val().length >= 2) {
                elem.clearQueue().stop().delay(1000).queue(function () {
                    $.ajax('{{route('api.search.cities.list')}}', {
                        type: 'GET',  // http method
                        data: {query: elem.val()},  // data to submit
                        dataType: 'json',
                        success: function (data, status, xhr) {
                            var out = '';
                            data.forEach(function (item, index, arr) {
                                out += '<option value="' + item.name + '">';
                            })
                            $('#list-location-cities').html(out);

                        },
                        error: function (jqXhr, textStatus, errorMessage) {
                            alert('Error' + errorMessage);
                        }
                    }
                )
                    ;

                });
            }
        });
    

    
    }
    
      
        initializefields();
</script>


@endsection