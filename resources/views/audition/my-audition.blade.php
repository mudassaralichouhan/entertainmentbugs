@extends('layouts.home')
@section('content')

  <link href="{{ asset('public/css/casting.css') }}" rel="stylesheet">

    <style>
        #live-video-user-list-home {
            display: none;
        }
    </style>


    <style>
        .share-option {
            position: absolute;
            right: -20px;
            top: -21px;
            width: 150px;
            line-height: 32px;
        }

        #audition_main .h-icons {
            float: right;
        }

        .share-option a {
            color: #fff !important;
            background: #f72e5e !important;
            width: 30px;
            height: 30px;
            margin: 0 4px 0 0;
            display: block;
            text-align: center;
            line-height: 35px;
            border-radius: 42px;
            float: right;
        }

        #production-house-main {
            padding-top: 10px;
            padding-bottom: 10px;
        }
    </style>

    <!-- font-family: 'Hind', sans-serif; -->
    <link href='https://fonts.googleapis.com/css?family=Hind:400,300,500,600,700|Hind+Guntur:300,400,500,700' rel='stylesheet'
        type='text/css'>
    </head>



    <div class="content-wrapper">
        <div class="audition_main_list" id="audition_main">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Featured Videos -->
                        <div class="content-block">
                            <div class="casting-search-result">
                                <div class="row home_page_seventh_block" style="margin:0px;">
                                    <div class="col-lg-12">
                                        <!-- Popular Channels -->
                                        @if ($message = Session::get('msg'))
                                            <div class="alert alert-info alert-dismissible fade in" role="alert">
                                                <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </a>
                                                <strong>Success!</strong> {{ $message }}
                                            </div>
                                        @endif {!! Session::forget('msg') !!}

                                        @if ($audtion)
                                            @foreach ($audtion as $a)
                                                <div class="content-block">
                                                    <div id="production-house-main">
                                                        <div class="production-all-display">
                                                            <div class="production-hs1">
                                                                <a href="{{ url("production-house/$a->slug") }}">
                                                                    <img src="<?php echo count($production_about) > 0 ? (isset($production_about[0]->profile_photo) ? $production_about[0]->profile_photo : '') : ''; ?>"></a>
                                                            </div>
                                                            <div class="production-hs2">
                                                                <div class="sp1" id="auditions">
                                                                    <h5>{{ $a->title }}</h5>
                                                                    <p><span><i class="fa fa-list-alt"></i> Category:
                                                                        </span>{{ $a->category }}</p>
                                                                    <div class="width-space"></div>
                                                                    <div class="line-0">|</div>
                                                                    <div class="width-space"></div>
                                                                    <p><span><i class="fa fa-map-marker"></i> Location:
                                                                        </span> {{ $a->country ."/". $a->city }}</p>
                                                                    <div class="width-space"></div>
                                                                    <div class="line-0">|</div>
                                                                    <div class="width-space"></div>
                                                                    <p><span><i class="fa fa-calendar"></i> Posted on:
                                                                        </span> {{ substr($a->posted_date, 0, 11) }}</p>
                                                                    <div class="clear"></div>
                                                                    <div class="width-space"></div>
                                                                    <div class="line-0">|</div>
                                                                    <div class="width-space"></div>
                                                                    <p><span><i class="fa fa-user"></i> Posted By: </span>
                                                                        {{ $a->post_by_name }}</p>
                                                                    <div class="clear"></div>
                                                                    <p><span><i class="fa fa-language"></i> Language:
                                                                        </span> {{ $a->language }}</p>
                                                                    <div class="width-space"></div>
                                                                    <div class="line-0">|</div>
                                                                    <div class="width-space"></div>
                                                                    <p><span><i class="fa fa-calendar-check-o"></i> Valid
                                                                            Till: </span> {{ $a->valid_till }}</p>
                                                                </div>
                                                                <div class="share-option">
                                                                    Share:
                                                                    <div class="h-icons">
                                                                        <a href="#"><i class="fa fa-instagram"
                                                                                data-toggle="tooltip" data-placement="top"
                                                                                title=""></i></a>
                                                                        <a href="#"><i class="fa fa-facebook"
                                                                                data-toggle="tooltip" data-placement="top"
                                                                                title=""></i></a>
                                                                        <a href="#"><i class="fa fa-whatsapp"
                                                                                data-toggle="tooltip" data-placement="top"
                                                                                title=""></i></a>
                                                                    </div>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="clear"></div>

                                                            <div class="aud-de" style="padding-bottom:5px;float:right">
                                                                @if (is_null($a->status) || $a->status == 'open')
                                                                    <a href="{{ url('my-audition') }}/?close={{ $a->id }}"
                                                                        class="apply-n">Close</a>
                                                                @else
                                                                    <a href="{{ url('my-audition') }}/?open={{ $a->id }}"
                                                                        class="apply-n">Reopen</a>
                                                                @endif
                                                                <a href="{{ url('audition/' . $a->slug) }}"
                                                                    class="apply-n">View</a>
                                                                <a href="{{ url("post-audition?tp=$a->id") }}"
                                                                    class="apply-n">Edit</a>
                                                            </div>
                                                            <div class="clear"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            <h4>No Audition Found..</h4>
                                        @endif
                                    </div>
                                    <!-- /Popular Channels -->
                                </div>
                            </div>
                            <div class="casting-search">
                                <div class="casting-cat-block">
                                    <h4>Auditions</h4>
                                </div>
                                <div class="production-all-display" id="right-side-bar">
                                    <div class="production-hs1">
                                        <a href="" class="ic">
                                            <img
                                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a>
                                        <h5>Looking for Male lead & co-producer | 60 days schedule in Nepal</h5>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="production-hs2">
                                        <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                        <div class="sp1" id="auditions">
                                            <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production
                                                Manager</p>
                                            <div class="clear"></div>
                                            <p><span><i class="fa fa-map-marker"></i> Location: </span> Bangalore</p>
                                            <div class="clear"></div>
                                            <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> Mar 29 2022
                                            </p>
                                        </div>
                                        <a href="" class="view-m">View Now</a>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="production-all-display" id="right-side-bar">
                                    <div class="production-hs1">
                                        <a href="" class="ic">
                                            <img
                                                src="https://1zhit3xkjyq3ki6yq3sav7ahhz-wpengine.netdna-ssl.com/wp-content/uploads/2018/11/Our-Lady-LTD-Season-1-Epix-400x190.png"></a>
                                        <h5>Looking for Male lead & co-producer | 60 days schedule in Nepal</h5>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="production-hs2">
                                        <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                        <div class="sp1" id="auditions">
                                            <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production
                                                Manager</p>
                                            <div class="clear"></div>
                                            <p><span><i class="fa fa-map-marker"></i> Location: </span> Bangalore</p>
                                            <div class="clear"></div>
                                            <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> Mar 29 2022
                                            </p>
                                        </div>
                                        <a href="" class="view-m">View Now</a>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="production-all-display" id="right-side-bar">
                                    <div class="production-hs1">
                                        <a href="" class="ic">
                                            <img
                                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a>
                                        <h5>Looking for Male lead & co-producer | 60 days schedule in Nepal</h5>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="production-hs2">
                                        <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                        <div class="sp1" id="auditions">
                                            <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production
                                                Manager</p>
                                            <div class="clear"></div>
                                            <p><span><i class="fa fa-map-marker"></i> Location: </span> Bangalore</p>
                                            <div class="clear"></div>
                                            <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> Mar 29 2022
                                            </p>
                                        </div>
                                        <a href="" class="view-m">View Now</a>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="production-all-display" id="right-side-bar">
                                    <div class="production-hs1">
                                        <a href="" class="ic">
                                            <img
                                                src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a>
                                        <h5>Looking for Male lead & co-producer | 60 days schedule in Nepal</h5>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="production-hs2">
                                        <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                        <div class="sp1" id="auditions">
                                            <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production
                                                Manager</p>
                                            <div class="clear"></div>
                                            <p><span><i class="fa fa-map-marker"></i> Location: </span> Bangalore</p>
                                            <div class="clear"></div>
                                            <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> Mar 29 2022
                                            </p>
                                        </div>
                                        <a href="" class="view-m">View Now</a>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div id="view-all-auditions">
                                    <a href="">View All Auditions </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--casting video END-->
                </div>
            </div>
            <!-- /Featured Videos -->
        </div>
    </div>
    </div>
    </div>
@endsection
