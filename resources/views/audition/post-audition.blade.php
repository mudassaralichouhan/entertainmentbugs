@extends('layouts.home')
@section('content')
<!--<link href="https://entertainmentbugs.com/dev_html/css/casting.css" rel="stylesheet">-->
<style>
    .d-none{
        display:none!important;
    }
    #live-video-user-list-home{display:none;}
    body.light .content-wrapper {background-color: #f4f3f3 !important;}
    .production-hs2 h5 {font-size: 14px;}
    .aud-de h5{font-size: 14px;}
    #create-profile{margin:40px 0}
    #top-check-field0 h2{font-size:22px; margin:0 0 0 0}
    .option-block{float:left; width:33.3%}
    .field strong{margin-top:10px;display: block;font-weight:600;font-family: 'Hind Guntur', sans-serif;}
    #input-field0 label{font-weight:600;font-family: 'Hind Guntur', sans-serif;margin:0px 0 3px 0;}
    #input-field0 .form-group {margin-bottom: 18px;}
    #input-field0 textarea {padding-top: 10px;font-size: 13px;}
    #what-we-de-bl label{font-weight:600;font-family: 'Hind Guntur', sans-serif;margin:0px 0 3px 0;}
    #what-we-de-bl .form-group {margin-bottom: 18px;}
    #what-we-de-bl  textarea {padding-top: 10px;font-size: 13px;}
    #what-we-de-bl h2{font-size:22px; margin:0 0 20px 0}
    #input-field0 h2{font-size:22px; margin:0 0 20px 0}
    #portfolio-block h2{font-size:22px; margin:0 0 20px 0}
    #portfolio-block label {font-weight: 600;font-family: 'Hind Guntur', sans-serif;margin: 0px 0 3px 0;}
    textarea {padding-top: 0;margin: 0 0 -10px 0;}
    #showreel-block .form-group {margin-bottom: 10px;}
    .two.fields{margin-bottom:20px;clear:both; overflow:hidden;}
    .btn-cv1 {padding: 11px;}
    #audition_main {background: #f4f3f3 !important;}
    .tab4{background: #eceff0; margin-bottom:0px;clear:both; overflow:hidden;} 
    .tab4 a {padding: 10px 20px;text-align: Center;color: #000;line-height: 45px;}
    .tab4 a.active{    background: #28b47e !important;color:#fff;}
    .option-block {float: left;width: 33.3%;}
    .field strong{margin-top: 9px;display: block;}
    #auditionsform input{    margin: 0 0 10px 0;}
    #auditionsform h5 {margin: 2px 0 5px 0;}
    #auditionsform {display:block}
      .casting-menu {
        width: 100%;
    }

    .casting-menu ul {
        list-style: none;
        padding: 0px;
        text-align: center;
        background: #f72e5e;
        margin: 0px;
    }

    .casting-menu ul li {
        display: inline-block;
        text-align: center;
    }

   .casting-menu ul li a {letter-spacing: 0.1px;color: #fff;font-size: 12px;padding: 16px 18px 13px 18px;display: block;}


    .casting-menu ul li a:hover {
        background: #c42047
    }
    
    
    
</style>


 <div class="content-wrapper menu-top1">
    
    @if ($errors->any())
        <div class="alert alert-danger">
            {!! implode('', $errors->all('<div>:message</div>')) !!}
        </div>
    @endif
 <?php
                         $userid=session()->get('user_id');
     if($userid != ''){
            $artist_about=\App\Models\ArtistAbout::where('user_id',$userid)->get();
                    $progress = 0;
                 if($artist_about->count() > 0 ){
                     $progress = 1;
                 }
                 if($progress){
                     ?>
                     <style>#user-menu{display:none}</style>
                     
                              <div class="containers" id="">
            <div class="casting-menu">
               <ul>
                    <li><a href="{{URL::to('artist-profile')}}"> Artist Profile</a></li>
                    <li><a href="{{URL::to('artist-videos')}}"> Artist Videos</a></li>
                    <li><a href="{{URL::to('artist-gallery')}}"> Artist Gallery</a></li> 
                    <li><a href="">Influencer</a></li>
                    <li><a href="">Actor / Model</a></li>
                    <!--<li><a href="{{URL::to('production-houses')}}">Production House</a></li>-->
                    <li><a href="{{URL::to('find-auditions')}}">Autions</a></li> 
               </ul>
            </div>
         </div>           
                <?php 
                 } 
                 
                     $product_about=\App\Models\ProductionAbout::where('user_id',$userid)->get();
                $progress = 0;
                if( $product_about->count() > 0){
                     $progress = 1;
                 }
                if($progress){
                ?>
                 <style>#user-menu{display:none}
                   
              </style>
                <?php 
                }
            }//check if user login ?>   
            
            <div class="containers" id="user-menu">
            <div class="casting-menu">
               <ul>
                     <li><a href="{{URL::to('artist-profile')}}"> Artist Profile</a></li>
                    <li><a href="{{URL::to('artist-videos')}}"> Artist Videos</a></li>
                    <li><a href="{{URL::to('artist-gallery')}}"> Artist Gallery</a></li> 
                    <li><a href="">Influencer</a></li>
                    <li><a href="">Actor / Model</a></li>
                    <li><a href="{{URL::to('production-houses')}}">Production House</a></li>
                    <li><a href="{{URL::to('find-auditions')}}">Find Autions</a></li> 
                    <li><a href="{{URL::to('post-audition')}}">Post Aution</a></li> 
               </ul>
            </div>
         </div><br>
         </div>  



<div class="audition_main_list" id="audition_main">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Featured Videos -->
                <div class="content-block">
                    <div class="casting-search-result">
                        <div class="row home_page_seventh_block" style="margin:0px;">
                            <div class="col-lg-12">
                                <div class="content-block">
                                    <div id="production-house-main">
                                        <div class="production-all-displaya">
                                            <div class="production-hs2" style="width:100%;padding-top:0px;padding-bottom:30px">
                                                <div class="sp1" id="auditionsform">
                                                    <form action="{{URL::to('save-audition-profile')}}" id="auditionform" style="margin-top:0px" enctype="multipart/form-data" method="post">
                                                        
                                                        <?php 
                                                            $categoryarr = [];
                                                            if(count($getIfAlready) > 0)
                                                            {
                                                                if($getIfAlready[0]->category != NULL)
                                                                {
                                                                    $explode = explode(',',$getIfAlready[0]->category);
                                                                    if(count($explode) > 0)
                                                                    {
                                                                        foreach($explode as $k=>$v)
                                                                        array_push($categoryarr,$v);
                                                                    }
                                                                }
                                                            }
                                                        ?>
                                                        <input type="hidden" name="cropImage" id="cropImage" value="">
                                                        <?php if(count($getIfAlready) > 0){ ?>
                                                         <input type="hidden" name="tp" value="{{$getIfAlready[0]->id}}">
                                                         <?php } ?>
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                        <div class="col-lg-12">
                                                            <h5>Title</h5>
                                                            <input type="text" required name="title" class="form-control" id="audition-title" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->title != NULL) ? $getIfAlready[0]->title : '') : '')); ?>">
                                                            <br />
                                                            <span id="audition-title-error"></span>
                                                            <h5 style="margin-top:20px">Category</h5>
                                                            <div class="field">
                                                                <div class="option-block">
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Theater" <?php echo ((count($categoryarr) > 0) ? ((in_array('Theater',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Theater
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Voiceover" <?php echo ((count($categoryarr) > 0) ? ((in_array('Voiceover',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Voiceover
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Comedian" <?php echo ((count($categoryarr) > 0) ? ((in_array('Comedian',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Comedian
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Photography" <?php echo ((count($categoryarr) > 0) ? ((in_array('Photography',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Photography
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Video Grapher" <?php echo ((count($categoryarr) > 0) ? ((in_array('Video Grapher',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Video Grapher
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Makeup Artrist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Makeup Artrist',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Makeup Artrist
                                                                    </div>
                                                                    <div class="field ">
                                                                        <strong>Actor / Model</strong>
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Actor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Actor',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Actor
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Child Actor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Child Actor',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Child Actor
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Comedy Actor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Comedy Actor',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Comedy Actor
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Junior Actor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Junior Actor',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Junior Actor
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Stunt Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Stunt Artist',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Stunt Artist
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Drama Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Drama Artist',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Drama Artist
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Mimicry Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Mimicry Artist',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Mimicry Artist
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Background / Atmosphere" <?php echo ((count($categoryarr) > 0) ? ((in_array('Background / Atmosphere',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Background / Atmosphere 
                                                                    </div>
                                                                    <div class="field ">
                                                                        <strong>Modeling</strong>
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Model" <?php echo ((count($categoryarr) > 0) ? ((in_array('Model',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Model
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Child Model" <?php echo ((count($categoryarr) > 0) ? ((in_array('Child Model',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Child Model
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Pro Model" <?php echo ((count($categoryarr) > 0) ? ((in_array('Pro Model',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Pro Model
                                                                    </div>
                                                                    <div class="field ">
                                                                        <strong> Dancer</strong>
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Dancer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Dancer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Dancer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Lead Dancer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Lead Dancer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Lead Dancer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Background Dancer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Background Dancer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Background Dancer
                                                                    </div>
                                                                    <div class="field ">
                                                                        <strong>Singer / Musician</strong>
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Singer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Singer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Singer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Music Instrumentalist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Music Instrumentalist',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Music Instrumentalist
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Music Composer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Music Composer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Music Composer
                                                                    </div>
                                                                    <div class="field ">
                                                                        <strong>Editor</strong>
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Video Editor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Video Editor',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Video Editor
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Music Editor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Music Editor',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Music Editor
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Audio Editor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Audio Editor',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Audio Editor
                                                                    </div>
                                                                </div>
                                                                <div class="option-block">
                                                                    <div class="field ">
                                                                        <strong>Writer</strong>
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Dialogue Writer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Dialogue Writer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Dialogue Writer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Script Writer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Script Writer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Script Writer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Screeplay Writer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Screeplay Writer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Screeplay Writer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Story Writer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Story Writer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Story Writer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Lyricists" <?php echo ((count($categoryarr) > 0) ? ((in_array('Lyricists',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Lyricists
                                                                    </div>
                                                                    <div class="vspace-small"></div>
                                                                    <div class="vspace-small"></div>
                                                                    <div class="vspace-small"></div>
                                                                    <div class="field ">
                                                                        <strong>Anchor / Presenter</strong>
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Video Jockey (VJ)" <?php echo ((count($categoryarr) > 0) ? ((in_array('Video Jockey (VJ)',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Video Jockey (VJ)
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Radio Jockey (RJ)" <?php echo ((count($categoryarr) > 0) ? ((in_array('Radio Jockey (RJ)',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Radio Jockey (RJ)
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="News Reader" <?php echo ((count($categoryarr) > 0) ? ((in_array('News Reader',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        News Reader
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Reporter" <?php echo ((count($categoryarr) > 0) ? ((in_array('Reporter',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Reporter
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Journalist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Journalist',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Journalist
                                                                    </div>
                                                                    <div class="field">
                                                                        <div class="ui field ">
                                                                            <strong>Off screen</strong>
                                                                        </div>
                                                                        <div class="field col-pad">
                                                                            <input name="category[]" type="checkbox" value="Dubbing Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Dubbing Artist',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                            Dubbing Artist
                                                                        </div>
                                                                        <div class="field col-pad">
                                                                            <input name="category[]" type="checkbox" value="Voice Over Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Voice Over Artist',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                            Voice Over Artist
                                                                        </div>
                                                                    </div>
                                                                    <div class="field">
                                                                        <div class="ui field ">
                                                                            <strong>Influncer</strong>
                                                                        </div>
                                                                        <div class="field col-pad">
                                                                            <input name="category[]" type="checkbox" value="New Influncer" <?php echo ((count($categoryarr) > 0) ? ((in_array('New Influncer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                            New Influncer
                                                                        </div>
                                                                        <div class="field col-pad">
                                                                            <input name="category[]" type="checkbox" value="Experienced Influncer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Experienced Influncer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                            Experienced Influncer
                                                                        </div>
                                                                    </div>
                                                                    <div class="field">
                                                                        <div class="ui field ">
                                                                            <strong>Advertising Professional	</strong>
                                                                        </div>
                                                                        <div class="field col-pad">
                                                                            <input name="category[]" type="checkbox" value="Video Advertising" <?php echo ((count($categoryarr) > 0) ? ((in_array('Video Advertising',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                            Video Advertising
                                                                        </div>
                                                                        <div class="field col-pad">
                                                                            <input name="category[]" type="checkbox" value="Audio Advertising" <?php echo ((count($categoryarr) > 0) ? ((in_array('Audio Advertising',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                            Audio Advertising
                                                                        </div>
                                                                        <div class="field col-pad">
                                                                            <input name="category[]" type="checkbox" value="Banner Advertising" <?php echo ((count($categoryarr) > 0) ? ((in_array('Banner Advertising',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                            Banner Advertising
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="option-block">
                                                                    <div class="ui field ">
                                                                        <strong>Proffessional Artist</strong>
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Director Of Photography / Cinematographer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Director Of Photography / Cinematographer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Director Of Photography / Cinematographer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Cameraman" <?php echo ((count($categoryarr) > 0) ? ((in_array('Cameraman',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Cameraman
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Camera Operator" <?php echo ((count($categoryarr) > 0) ? ((in_array('Camera Operator',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Camera Operator
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Light man / Gaffer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Light man / Gaffer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Light man / Gaffer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Makeup Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Makeup Artist',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Makeup Artist
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Visual Effects Editor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Visual Effects Editor',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Visual Effects Editor
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Digital imaging technician" <?php echo ((count($categoryarr) > 0) ? ((in_array('Digital imaging technician',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Digital imaging technician
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Special Effects makeup Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Special Effects makeup Artist',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Special Effects makeup Artist
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Motion control technician" <?php echo ((count($categoryarr) > 0) ? ((in_array('Motion control technician',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Motion control technician
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Fashion Designer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Fashion Designer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Fashion Designer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Hair Stylist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Hair Stylist',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Hair Stylist
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Costume designer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Costume designer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Costume designer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Grip" <?php echo ((count($categoryarr) > 0) ? ((in_array('Grip',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Grip
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Sound Designer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Sound Designer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Sound Designer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Sound Grip" <?php echo ((count($categoryarr) > 0) ? ((in_array('Sound Grip',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Sound Grip
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Production Sound Mixer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Production Sound Mixer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Production Sound Mixer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Production Designer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Production Designer',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Production Designer
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Green man" <?php echo ((count($categoryarr) > 0) ? ((in_array('Green man',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Green man
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Property master" <?php echo ((count($categoryarr) > 0) ? ((in_array('Property master',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Property master
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Weapons master" <?php echo ((count($categoryarr) > 0) ? ((in_array('Weapons master',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Weapons master
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Set Designer / Decorator" <?php echo ((count($categoryarr) > 0) ? ((in_array('Set Designer / Decorator',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Set Designer / Decorator
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Location Manager" <?php echo ((count($categoryarr) > 0) ? ((in_array('Location Manager',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Location Manager
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="StoryBoard Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('StoryBoard Artist',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        StoryBoard Artist
                                                                    </div>
                                                                    <div class="vspace-small"></div>
                                                                    <div class="vspace-small"></div>
                                                                    <div class="vspace-small"></div>
                                                                    <div class="ui field ">
                                                                        <strong>Others</strong>
                                                                    </div>
                                                                    <div class="field col-pad">
                                                                        <input name="category[]" type="checkbox" value="Others" <?php echo ((count($categoryarr) > 0) ? ((in_array('Others',$categoryarr)) ? 'checked' : '') : ''); ?>>
                                                                        Others
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="clear" style="height:20px"></div>
                                                        </div>
                                                        <div class="form-group col-lg-4 col-sm-6">
                                                            <label for="exampleInputEmail1">Country* </label>
                                                            <div class="clear"></div>
                                                            <select id="country" name="country" required onchange="getStates(this.value)" class="select2 input-select" style="width:100%">
                                                                <option value="">Select Country</option>
                                                                <?php
                                                                    $getCountries = DB::table('tbl_countries')->get();
                                                                    foreach($getCountries as $k=>$v)
                                                                    {
                                                                    ?>
                                                                <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->country != NULL) ? (($getIfAlready[0]->country == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-lg-4 col-sm-6">
                                                            <label for="exampleInputEmail1">Location (State)*</label>
                                                            <!--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="">-->
                                                            <select class="select2 input-select" style="width:100%" onchange="getCities(this.value)" required id="state" name="state">
                                                                <option value="">Select State</option>
                                                                <?php
                                                                    if(count($getIfAlready) > 0)
                                                                    {
                                                                        if($getIfAlready[0]->country != NULL || $getIfAlready[0]->country != '')
                                                                        {
                                                                            $getCountryId = DB::table('tbl_countries')->where('name',$getIfAlready[0]->country)->get();
                                                                            $getStates = DB::table('tbl_states')->where('country_id',$getCountryId[0]->id)->get();
                                                                            foreach($getStates as $k=>$v)
                                                                            {
                                                                    ?>
                                                                <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->state != NULL) ? (($getIfAlready[0]->state == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                                                                <?php } ?>
                                                                <?php } } ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-lg-4 col-sm-6">
                                                            <label for="exampleInputEmail1">City*</label>
                                                            <select class="select2 input-select" style="width:100%" required id="city" name="city">
                                                                <option value="">Select City</option>
                                                                <?php
                                                                    if(count($getIfAlready) > 0)
                                                                    {
                                                                        if($getIfAlready[0]->state != NULL || $getIfAlready[0]->state != '')
                                                                        {
                                                                            $getStateId = DB::table('tbl_states')->where('name',$getIfAlready[0]->state)->get();
                                                                            $getCities = DB::table('tbl_cities')->where('state_id',$getStateId[0]->id)->get();
                                                                            foreach($getCities as $k=>$v)
                                                                            {
                                                                    ?>
                                                                <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->city != NULL) ? (($getIfAlready[0]->city == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                                                                <?php } ?>
                                                                <?php } } ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <h5>Language</h5>
                                                            <input type="text" class="form-control" id ='video-language' name="language" required placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->language != NULL) ? $getIfAlready[0]->language : '') : '')); ?>">
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <h5>Post by (Name)</h5>
                                                            <input type="text" required name="post_by_name" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->post_by_name != NULL) ? $getIfAlready[0]->post_by_name : '') : '')); ?>">
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <h5>Valid till</h5>
                                                            <input type="date" class="form-control" min="<?php echo date('Y-m-d',strtotime('+1 day'));?>" name="valid_till" required id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->valid_till != NULL) ? $getIfAlready[0]->valid_till : '') : '')); ?>">
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <h5 style="margin-top:10px;">Description </h5>
                                                            <textarea id="w3review" name="description" rows="4" cols="60"><?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->description != NULL) ? $getIfAlready[0]->description : '') : '')); ?></textarea> 
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <h5 style="margin-top:20px;"> SYNOPSIS  </h5>
                                                            <textarea id="w3review" name="synopsis" rows="4" cols="60"><?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->synopsis != NULL) ? $getIfAlready[0]->synopsis : '') : '')); ?></textarea> 
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <h5 style="margin-top:20px;"> ELIGIBILITY CRITERIA   </h5>
                                                            <textarea id="w3review" name="eligibility_criteria" rows="4" cols="60"><?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->eligibility_criteria != NULL) ? $getIfAlready[0]->eligibility_criteria : '') : '')); ?></textarea> 
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <h5 style="margin-top:20px;">Email address (We will notify you on this email) </h5>
                                                            <input type="email" name="email_address" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->email_address != NULL) ? $getIfAlready[0]->email_address : '') : '')); ?>">    
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <h5 style="margin-top:20px;"> Whatsapp (Artist will direct contact on your whatsapp) </h5>
                                                            <input type="text" name="whatsapp_number" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->whatsapp_number != NULL) ? $getIfAlready[0]->whatsapp_number : '') : '')); ?>">
                                                        </div>
                                                        <div class="row" style="margin:20px 0 30px 0;padding: 0;">
                                                            <div class="col-lg-12">
                                                                <input type="file" target="myfile" id="upload_image"  class="image custom-file-input image upload-cover-image" onchange="validateThumbFile(this.files);" accept="image/x-png,image/gif,image/jpeg" />
                                                                <input type="hidden" id="myfile" name="myfile">
                                                                <?php if(count($getIfAlready) > 0){ 
                                                                    if($getIfAlready[0]->photo != NULL || $getIfAlready[0]->photo != '')
                                                                    {
                                                                    ?>
                                                                <img src="<?php echo $getIfAlready[0]->photo ?>" style="width:200px;height:200px;margin-top:15px;">
                                                                <?php } } ?>     
                                                            </div>
                                                            <div id="profile_photo_preview">
                                                                <img style="width:200px;height:200px;margin-top:15px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAAXNSR0IArs4c6QAAAD5JREFUSEvt07ENADAIBDHYf2n69A9FzAAgWUfX0fTR3XJ4TR416piAuGK072LUqGMC4orR+mNxiSsm8F9cA+n3AB9W54gZAAAAAElFTkSuQmCC" alt="previewImage">
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <div class="col-lg-2">
                                                            <button type="submit" class="apply-n"style="background: #28b47e !important;color: #fff !important;padding: 6px 42px 4px 42px;"><?php if(count($getIfAlready) == 0){ echo "Submit";}else{ echo "Update"; } ?></button>
                                                        </div>
                                                        <?php 
                                                            if(count($getIfAlready) > 0)
                                                            {
                                                        ?>
                                                        <div class="col-lg-2">
                                                            <a href="{{URL::to('audition/'.$getIfAlready[0]->slug)}}" target="_blank" class="apply-n" style="background: #28b47e !important;color: #fff !important;padding: 6px 42px 4px 42px;"> Preview</a>  
                                                        </div>
                                                        <?php } ?>
                                                    </form>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="casting-search d-none">
                        <div class="casting-cat-block">
                            <h4>My Auditions</h4>
                        </div>
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic"> 
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">
                                <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a> 
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic"> 
                                <img src="https://1zhit3xkjyq3ki6yq3sav7ahhz-wpengine.netdna-ssl.com/wp-content/uploads/2018/11/Our-Lady-LTD-Season-1-Epix-400x190.png"></a> 
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">
                                <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a> 
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic"> 
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">
                                <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a> 
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic"> 
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">
                                <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a> 
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic"> 
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">
                                <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a> 
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic"> 
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">
                                <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a> 
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic"> 
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">
                                <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a> 
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic"> 
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">
                                <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a> 
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic"> 
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">
                                <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a> 
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic"> 
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">
                                <p class="prd-info"> Hi we are looking for a male lead for...</p>
                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a> 
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div id="view-all-auditions">
                            <a href="">View All Auditions </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
{{ HTML::style('public/css/bootstrap-tagsinput.css')}}
{{ HTML::script('public/js/jquery.validate.js')}}
{{ HTML::script('public/js/bootstrap-tagsinput.js')}}
{{ HTML::script('public/js/bootstrap3-typeahead.js')}}
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    $('#video-language').tagsinput({
    	  typeahead: {
    		source: ['Afar', 'Abkhaz', 'Avestan', 'Afrikaans', 'Akan', 'Amharic','Aragonese', 'Arabic', 'Assamese', 'Avaric', 'Aymara', 'Azerbaijani','South Azerbaijani', 'Bashkir', 'Belarusian', 'Bulgarian', 'Bihari', 'Bislama',
    		'Bambara', 'Bengali', 'Tibetan Standard', 'Breton', 'Bosnian', 'Catalan; Valencian','Chechen', 'Chamorro', 'Corsican', 'Cree', 'Czech', 'Old Church Slavonic','Chuvash', 'Welsh', 'Danish', 'German', 'Maldivian', 'Dzongkha',
    		'Ewe', 'Greek', 'English', 'Esperanto', 'Spanish', 'Estonian','Basque', 'Persian ', 'Fula', 'Finnish', 'Fijian', 'Faroese',' French', 'Western Frisian', 'Irish', 'Scottish Gaelic', 'Galician', 'Guaraní',
    		'Gujarati', 'Manx', 'Hausa', 'Hebrew ', 'Hindi', 'Hiri Motu','Croatian', 'Haitian', 'Hungarian', 'Armenian', 'Herero', 'Interlingua',' Indonesian', 'Interlingue', 'Igbo', 'Nuosu', 'Inupiaq', 'Ido',
    		'Icelandic', 'Italian', 'Inuktitut', 'Japanese', 'Javanese', 'Georgian','Kongo', 'Gikuyu', 'Kwanyama', 'Kazakh', 'Kalaallisut', 'Khmer','Kannada', 'Korean', 'Kanuri', 'Kashmiri', 'Kurdish', 'Komi',
    		'Cornish', 'Kyrgyz', 'Latin', 'Luxembourgish', 'Ganda', 'Limburgish','Lingala', 'Lao', 'Lithuanian', 'Luba-Katanga', 'Latvian', 'Malagasy','Marshallese', 'Māori', 'Macedonian', 'Malayalam', 'Mongolian', 'Marathi',
    		'Malay', 'Maltese', 'Burmese', 'Nauru', 'Norwegian Bokmål', 'North Ndebele','Nepali', 'Ndonga', 'Dutch', 'Norwegian Nynorsk', 'Norwegian', 'South Ndebele','Navajo', 'Chichewa', 'Occitan', 'Ojibwe', 'Oromo', 'Oriya',
    		'Ossetian', 'Panjabi', 'Pāli', 'Polish', 'Pashto', 'Portuguese','Quechua', 'Romansh', 'Kirundi', 'Romanian', 'Russian', 'Kinyarwanda','Sanskrit', 'Sardinian', 'Sindhi', 'Northern Sami', 'Sango', 'Sinhala',
    		'Slovak', 'Slovene', 'Samoan', 'Shona', 'Somali', 'Albanian','Serbian', 'Swati', 'Southern Sotho', 'Sundanese', 'Swedish', 'Swahili','Tamil', 'Telugu', 'Tajik', 'Thai', 'Tigrinya', 'Turkmen',
    		'Tagalog', 'Tswana', 'Tonga', 'Turkish', 'Tsonga', 'Tatar','Twi', 'Tahitian', 'Uyghur', 'Ukrainian', 'Urdu	', 'Uzbek','Venda', 'Vietnamese', 'Volapük', 'Walloon', 'Wolof', 'Xhosa',
    		'Yiddish', 'Yoruba', 'Zhuang', 'Chinese', 'Zulu']
    	  },
    	  freeInput: true
    });	
    
    validateThumbFile = function(files,e){
        var flag = true;
        var imageType = /image.*/;  
        var file = files[0];
        // check file type
        if (!file.type.match(imageType)) {  
        alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
        flag = false;
        return false;	
        } 
        // check file size
        if (parseInt(file.size / 1024) > (1024*2)) {  
        alert("File \""+file.name+"\" is too big.");
        flag = false;
        return false;	
        } 
        
        if(flag == true){
            uploadImageFile(file);
        }else{
            return flag;
        }	
        
    };
    
    uploadImageFile = function(file){
        var formdata = new FormData();
        formdata.append("image", file);	
        //console.log(file);
        xhr = new XMLHttpRequest();
        //console.log(xhr);
        xhr.open("POST", "upload/temp_image");
        xhr.onload  = function() {
        var jsonResponse = xhr.response;
        console.log(jsonResponse);
        result = JSON.parse(jsonResponse);
        console.log(result);
        if(result.status == true){
            
        }else{
            alert('File is not uploading. Please try again.')
        }	  
        };	
        var csrfToken = $('#artistForm').find('input[name=_token]').val();
        xhr.setRequestHeader('X-csrf-token', csrfToken); 	
        xhr.send(formdata);		
    }
    
    $('#auditionform').on('submit',function(e)
    {
        e.preventDefault();
        const title = $("#audition-title").val();
        if( !title ){
            $("#audition-title-error").text("Title must not be empty");
            return false;
        } else if( title.length > 150 ){
            $("#audition-title-error").text("Title must not be more than 150 words");
            $('html, body').animate({
                scrollTop: $("#audition-title-error").offset().top
            }, 500);
            return false;
        } else {
            $("#audition-title-error").text("");
        }
        
        
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                console.log(res);
                if( res.status == 1 ){
                    alert(res.message);
                    location.href = res.redirectTo;
                    return;
                }
                
                alert("Failed to save data");
            }
        });
    })
    
    function getStates(value)
    {
        $('#state').empty();
        $('#state').append('<option value="">Select State</option>');
        if(value != '')
        {
            $.ajax({
                url:'{{URL::to("/get-states")}}',
                data:{id:value,'_token':'<?php echo csrf_token(); ?>'},
                type:'post',
                success:function(res)
                {
                    let parse= JSON.parse(res);
                    if(parse.length > 0)
                    {
                        for(let i=0;i<parse.length;i++)
                            $('#state').append('<option value="'+parse[i].name+'">'+parse[i].name+'</option>');
                    }else{
                        $('#state').append('<option value="">No States Found</option>');    
                    }
                }
            })
        }else{
            $('#state').append('<option value="">Select State</option>');    
        }
    }
    
    function getCities(value)
    {
        $('#city').empty();
        $('#city').append('<option value="">Select City</option>');
        if(value != '')
        {
            $.ajax({
                url:'{{URL::to("/get-cities")}}',
                data:{id:value,'_token':'<?php echo csrf_token(); ?>'},
                type:'post',
                success:function(res)
                {
                    let parse= JSON.parse(res);
                    if(parse.length > 0)
                    {
                        for(let i=0;i<parse.length;i++)
                            $('#city').append('<option value="'+parse[i].name+'">'+parse[i].name+'</option>');
                    }else{
                       $('#city').append('<option value="">No Cities Found</option>');     
                    }
                }
            })
        }else{
            $('#city').append('<option value="">Select City</option>');     
        }
    }
</script>
@endsection