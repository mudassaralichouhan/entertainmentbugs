
    <div class="eb_template_wrapper" style="">
      
      <div data-template-type="headers">
        <!--[if (gte mso 9)|(IE)]><xml><o:OfficeDocumentSettings><o:AllowPNG></o:AllowPNG><o:PixelsPerInch>96</o:PixelsPerInch></o:OfficeDocumentSettings></xml><![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="format-detection" content="telephone=no">
        <meta name="format-detection" content="date=no">
        <meta name="format-detection" content="address=no">
        <meta name="format-detection" content="email=no">
        <meta name="x-apple-disable-message-reformatting">
     
        <style data-font-primary="" type="text/css">
		
		td{outline: 0px !important;}
		
		@font-face {
     


     font-family: 'Poppins';
          font-style: italic;
          font-weight: 100;
          src: local('Poppins Thin Italic'), local('Poppins-ThinItalic'), url(https://fonts.gstatic.com/s/poppins/v9/pxiAyp8kv8JHgFVrJJLmE0tCMPI.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 200;
            src: local('Poppins ExtraLight Italic'), local('Poppins-ExtraLightItalic'), url(https://fonts.gstatic.com/s/poppins/v9/pxiDyp8kv8JHgFVrJJLmv1pVF9eO.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 300;
            src: local('Poppins Light Italic'), local('Poppins-LightItalic'), url(https://fonts.gstatic.com/s/poppins/v9/pxiDyp8kv8JHgFVrJJLm21lVF9eO.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 400;
            src: local('Poppins Italic'), local('Poppins-Italic'), url(https://fonts.gstatic.com/s/poppins/v9/pxiGyp8kv8JHgFVrJJLucHtA.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 500;
            src: local('Poppins Medium Italic'), local('Poppins-MediumItalic'), url(https://fonts.gstatic.com/s/poppins/v9/pxiDyp8kv8JHgFVrJJLmg1hVF9eO.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 600;
            src: local('Poppins SemiBold Italic'), local('Poppins-SemiBoldItalic'), url(https://fonts.gstatic.com/s/poppins/v9/pxiDyp8kv8JHgFVrJJLmr19VF9eO.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 700;
            src: local('Poppins Bold Italic'), local('Poppins-BoldItalic'), url(https://fonts.gstatic.com/s/poppins/v9/pxiDyp8kv8JHgFVrJJLmy15VF9eO.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 800;
            src: local('Poppins ExtraBold Italic'), local('Poppins-ExtraBoldItalic'), url(https://fonts.gstatic.com/s/poppins/v9/pxiDyp8kv8JHgFVrJJLm111VF9eO.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: italic;
            font-weight: 900;
            src: local('Poppins Black Italic'), local('Poppins-BlackItalic'), url(https://fonts.gstatic.com/s/poppins/v9/pxiDyp8kv8JHgFVrJJLm81xVF9eO.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 100;
            src: local('Poppins Thin'), local('Poppins-Thin'), url(https://fonts.gstatic.com/s/poppins/v9/pxiGyp8kv8JHgFVrLPTucHtA.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 200;
            src: local('Poppins ExtraLight'), local('Poppins-ExtraLight'), url(https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLFj_Z1xlFQ.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 300;
            src: local('Poppins Light'), local('Poppins-Light'), url(https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLDz8Z1xlFQ.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 400;
            src: local('Poppins Regular'), local('Poppins-Regular'), url(https://fonts.gstatic.com/s/poppins/v9/pxiEyp8kv8JHgFVrJJfecg.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 500;
            src: local('Poppins Medium'), local('Poppins-Medium'), url(https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLGT9Z1xlFQ.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 600;
            src: local('Poppins SemiBold'), local('Poppins-SemiBold'), url(https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLEj6Z1xlFQ.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 700;
            src: local('Poppins Bold'), local('Poppins-Bold'), url(https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLCz7Z1xlFQ.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 800;
            src: local('Poppins ExtraBold'), local('Poppins-ExtraBold'), url(https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLDD4Z1xlFQ.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Poppins';
            font-style: normal;
            font-weight: 900;
            src: local('Poppins Black'), local('Poppins-Black'), url(https://fonts.gstatic.com/s/poppins/v9/pxiByp8kv8JHgFVrLBT5Z1xlFQ.woff2) format('woff2');
          }
        </style>
        <style data-font-secondary="" type="text/css">@font-face {
          font-family: 'Open Sans';
          font-style: italic;
          font-weight: 300;
          src: local('Open Sans Light Italic'), local('OpenSans-LightItalic'), url(https://fonts.gstatic.com/s/opensans/v17/memnYaGs126MiZpBA-UFUKWyV9hrIqM.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 400;
            src: local('Open Sans Italic'), local('OpenSans-Italic'), url(https://fonts.gstatic.com/s/opensans/v17/mem6YaGs126MiZpBA-UFUK0Zdc0.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 600;
            src: local('Open Sans SemiBold Italic'), local('OpenSans-SemiBoldItalic'), url(https://fonts.gstatic.com/s/opensans/v17/memnYaGs126MiZpBA-UFUKXGUdhrIqM.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 700;
            src: local('Open Sans Bold Italic'), local('OpenSans-BoldItalic'), url(https://fonts.gstatic.com/s/opensans/v17/memnYaGs126MiZpBA-UFUKWiUNhrIqM.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Open Sans';
            font-style: italic;
            font-weight: 800;
            src: local('Open Sans ExtraBold Italic'), local('OpenSans-ExtraBoldItalic'), url(https://fonts.gstatic.com/s/opensans/v17/memnYaGs126MiZpBA-UFUKW-U9hrIqM.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 300;
            src: local('Open Sans Light'), local('OpenSans-Light'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN_r8OUuhp.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Open Sans Regular'), local('OpenSans-Regular'), url(https://fonts.gstatic.com/s/opensans/v17/mem8YaGs126MiZpBA-UFVZ0b.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 600;
            src: local('Open Sans SemiBold'), local('OpenSans-SemiBold'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UNirkOUuhp.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 700;
            src: local('Open Sans Bold'), local('OpenSans-Bold'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN7rgOUuhp.woff2) format('woff2');
          }
          @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 800;
            src: local('Open Sans ExtraBold'), local('OpenSans-ExtraBold'), url(https://fonts.gstatic.com/s/opensans/v17/mem5YaGs126MiZpBA-UN8rsOUuhp.woff2) format('woff2');
          }
        </style>
        <style type="text/css">.ReadMsgBody {
          width: 100%;
          background-color: #ffffff;
          }
          .ExternalClass {
            width: 100%;
            background-color: #ffffff;
          }
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
          }
          html {
            width: 100%;
          }
          body {
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
            margin: 0;
            padding: 0;
          }
          table {
            border-spacing: 0;
            table-layout: fixed;
            margin: 0 auto;
          }
          img{
            height:auto;
            outline:none;
            text-decoration:none;
            -ms-interpolation-mode:bicubic;
          }
          a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration:none !important;
            font-size:inherit !important;
            font-family:inherit !important;
            font-weight:inherit !important;
            line-height:inherit !important;
          }
          u + #body a {
            color:inherit;
            font-family:inherit;
            text-decoration:none;
            font-size:inherit;
            font-weight:inherit;
            line-height:inherit;
          }
          .appleLinks a {
            color: #c2c2c2 !important;
            text-decoration: none !important;
          }
          span.preheader {
            display:none !important;
          }
        </style>
        <style data-style-responsive="" type="text/css">@media only screen and (max-width: 699px) {
          table.hideMobile, tr.hideMobile, td.hideMobile, br.hideMobile {
            display:none!important;
          }
          table.row, div.row {
            width: 100%!important;
            max-width: 100%!important;
          }
          table.centerFloat, td.centerFloat, img.centerFloat {
            float: none!important;
            margin:0 auto!important;
          }
          table.halfRow, div.halfRow {
            width: 50%!important;
            max-width: 50%!important;
          }
          td.imgResponsive img {
            width:100%!important;
            max-width: 100%!important;
            height: auto!important;
            margin: auto;
          }
          td.menuLink a {
            display: block;
            border-top: 1px solid;
            padding-top:20px;
          }
          td.centerText{
            text-align: center!important;
          }
          td.containerPadding {
            width: 100%!important;
            padding-left: 15px!important;
            padding-right: 15px!important;
          }
          td.mo-h-0 {
            height: 0 !important;
            font-size: 0px !important;
            line-height: 0px !important;
          }
          td.spaceControl {
            height:15px!important;
            font-size:15px!important;
            line-height:15px!important;
          }
          }
        </style>
      </div>
      <div data-template-type="html" class="ui-sortable">
        <center>
            
          <table data-group="Header" data-module="Sub Header" data-bgcolor="Outer BG Color" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eaeaea" style="width:100%;max-width:100%;">
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <table class="row" width="700" border="0" align="center" cellpadding="0" cellspacing="0" style="width:700px;max-width:700px;">
                    <tbody>
                      <tr>
                        <td data-bgcolor="Inner BG Color" align="center" valign="top" bgcolor="#f72e5e">
                          <table class="row" width="700" border="0" align="center" cellpadding="0" cellspacing="0" style="width:650px;max-width:650px;">
                            <tbody>
                              <tr>
                                <td data-resizable-height="" style="font-size:10px;height:10px;line-height:10px;" class="spacer-first ui-resizable">&nbsp;
                                  <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;">
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td align="left" valign="top" style="font-size:0;padding:0">
                                  <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td valign="middle"><![endif]-->
                                  <div class="row" style="display:inline-block;max-width:390px;vertical-align:middle;width:100%">
                                    <table class="row" width="390" border="0" align="left" cellpadding="0" cellspacing="0">
                                      <tbody>
                                        <tr>
                                          <td align="center" valign="top">
                                            <table class="centerFloat" border="0" align="left" cellpadding="0" cellspacing="0">
                                              <tbody>
                                                <tr>
                                                  <td width="35" align="center" valign="middle">
                                                    <a href="https://example.com/">
                                                      <img data-image="Icon Phone" src="https://editor.liramail.com/images/aumfusion/vardo/icon-phone.png" width="25" alt="" style="border:0;width:25px;">
                                                    </a>
                                                  </td>
                                                  <td data-text="Phone" data-font="Primary" align="center" valign="middle" style="font-family:'Poppins',sans-serif;font-size:13px; font-weight:400;color:#FFFFFF; line-height:24px;" contenteditable="true" data-gramm="false">
                                                    <a href="https://example.com/" style="text-decoration:none;color:#FFFFFF;"> +91 8240369924 
                                                    </a>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <table class="row" border="0" align="left" cellpadding="0" cellspacing="0">
                                              <tbody>
                                                <tr>
                                                  <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;">&nbsp;
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                            <table class="centerFloat" border="0" align="left" cellpadding="0" cellspacing="0">
                                              <tbody>
                                                <tr>
                                                  <td width="35" align="center" valign="middle">
                                                    <a href="https://example.com/">
                                                      <img data-image="Icon Email" src="https://editor.liramail.com/images/aumfusion/vardo/icon-email.png" width="25" alt="" style="border:0;width:25px;">
                                                    </a>
                                                  </td>
                                                  <td data-text="Email" data-font="Primary" align="center" valign="middle" style="font-family:'Poppins',sans-serif;font-size:13px; font-weight:400;color:#FFFFFF; line-height:24px;" contenteditable="true" data-gramm="false">
                                                    <a href="https://example.com/" style="text-decoration:none;color:#FFFFFF;"> info@entertainmentbugs.com
                                                    </a>
                                                  </td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <!--[if (gte mso 9)|(IE)]></td><td valign="middle"><![endif]-->
                                  <div class="row" style="display:inline-block;max-width:70px;vertical-align:middle;width:100%">
                                    <table class="row" border="0" align="left" cellpadding="0" cellspacing="0">
                                      <tbody>
                                        <tr>
                                          <td height="30" align="center" valign="top" style="font-size:30px; line-height:30px;">&nbsp;
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <!--[if (gte mso 9)|(IE)]></td><td valign="middle"><![endif]-->
                                  <div class="row" style="display:inline-block;max-width:190px;vertical-align:middle;width:100%">
                                    <table class="centerFloat" width="190" border="0" align="right" cellpadding="0" cellspacing="0">
                                      <tbody>
                                            <tr>
                                          <td align="center" valign="middle" width="35">
                                            <a href="#" style="text-decoration:none;border:0">
                                              <img data-image="Icon Twitter" src="twitter.png" alt="#" border="0" width="25" style="display:block;border:0;width:25px;">
                                            </a>
                                          </td>
                                          <td align="center" valign="middle" width="35">
                                            <a href="#" style="text-decoration:none;border:0">
                                              <img data-image="Icon Facebook" src="facebook.png" alt="#" border="0" width="25" style="display:block;border:0;width:25px;">
                                            </a>
                                          </td>
                                          <td align="center" valign="middle" width="35">
                                            <a href="#" style="text-decoration:none;border:0">
                                              <img data-image="Icon Instagram" src="instagram.png" alt="#" border="0" width="25" style="display:block;border:0;width:25px;">
                                            </a>
                                          </td>
                                          <td align="center" valign="middle" width="35">
                                            <a href="#" style="text-decoration:none;border:0">
                                              <img data-image="Icon Youtube" src="youtube.png" alt="#" border="0" width="25" style="display:block;border:0;width:25px;">
                                            </a>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                              </tr>
                              <tr>
                                <td data-resizable-height="" style="font-size:10px;height:10px;line-height:10px;" class="ui-resizable">&nbsp;
                                  <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;">
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          <table data-group="Header" data-module="Header" data-bgcolor="Outer BG Color" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eaeaea" style="width:100%;max-width:100%;" class="">
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <table class="row" width="700" border="0" align="center" cellpadding="0" cellspacing="0" style="width:700px;max-width:700px;border-bottom:solid 1px #eceff0">
                    <tbody>
                      <tr>
                        <td data-bgcolor="Inner BG Color" align="center" valign="top" bgcolor="#FFFFFF">
                          <table class="row" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="width:600px;max-width:600px;">
                            <tbody>
                              <tr>
                                <td data-resizable-height="" style="font-size:30px;height:30px;line-height:30px;" class="spacer-first ui-resizable">&nbsp;
                                  <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;">
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td align="center" valign="top" style="font-size:0;padding:0">
                                  <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td valign="middle"><![endif]-->
                                  <div class="row" style="display:inline-block;max-width:260px;vertical-align:middle;width:100%">
                                    <table class="row" border="0" align="center" cellpadding="0" cellspacing="0">
                                      <tbody>
                                        <tr>
                                          <td align="center" valign="top">
                                            <a href="https://example.com/">
                                              <img data-image="Logo" src="https://entertainmentbugs.com/balu_development/public/img/entertainment_bugs_logo.png" width="260" alt="" style="display:block;border:0;width:260px;">
                                            </a>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div> 
                                </td>
                              </tr>
                              <tr>
                                <td data-resizable-height="" style="font-size:30px;height:30px;line-height:30px;" class="ui-resizable">&nbsp;
                                  <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;">
                                  </div>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
		  
		 
 
 <table data-group="Banner" data-module="CTA 1" data-bgcolor="Outer BG Color" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eaeaea" style="width:100%;max-width:100%;">
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <table class="row" width="700" border="0" align="center" cellpadding="0" cellspacing="0" style="width:700px;max-width:700px;">
                    <tbody>
                      <tr>
                        <td data-bgcolor="Inner BG Color" align="center" valign="top" bgcolor="#f72e5e">
                          <table class="row" border="0" width="100%" align="center" cellpadding="0" cellspacing="0" style="width:100%;max-width:100%;">
                            <tbody>
                              <tr>
                                <td data-bg="CTA BG" align="center" class="containerPadding" bgcolor="#f72e5e" style="background:#f72e5e;">
                                  <!--[if gte mso 9]><v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:700px;height:345px;"><v:fill type="frame" src="https://editor.liramail.com/images/aumfusion/vardo/bg-cta.png" color="#FFFFFF"></v:fill><v:textbox style="v-text-anchor:middle;" inset="0,0,0,0"><![endif]-->
                                  <table class="row" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="width:600px;max-width:600px;">
                                    <tbody>
                                      <tr>
                                        <td data-resizable-height="" style="font-size:60px;height:60px;line-height:60px;">&nbsp;
                                        </td>
                                      </tr>
                                      <tr>
                                        <td data-text="Title" data-font="Primary" class="ctaTitle" align="center" valign="middle" style="font-family:'Poppins',sans-serif;color:#FFFFFF;font-size:32px;line-height:44px;font-weight:600;letter-spacing:0px;padding:0px;padding-bottom:20px;" contenteditable="true" data-gramm="false">
										
										<strong>Forget your Password?</strong>
                                          <br>   Hi! {{ $options['name'] }}.
                                        </td>
                                      </tr>
 
										<tr>
                                        <td data-text="Description" data-font="Primary" align="center" valign="middle" style="font-family:'Poppins',sans-serif;color:#FFFFFF;font-size:14px;line-height:22px;font-weight:400;letter-spacing:0px;padding-bottom:0px" contenteditable="true" data-gramm="false">
											<p style="margin-top:0px;">Sorry to hear you’re having trouble logging into Entertainment Bugs.
											We got a message that you forgot your password. You can  reset your password now</p>
                                        </td>
                                      </tr>
									   <tr>
                                        <td data-text="Description" data-font="Primary" align="center" valign="middle" style="font-family:'Poppins',sans-serif;color:#FFFFFF;font-size:14px;line-height:22px;font-weight:400;letter-spacing:0px;padding-bottom:30px" contenteditable="true" data-gramm="false">
											<p style="margin-bottom:0px;">If you did not make this request, just ignore this email. Otherwise,</p>
											<p style="margin-top:0px;">please click the button below to change your password.</p>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td align="center" valign="middle">
                                          <table border="0" align="center" cellpadding="0" cellspacing="0" class="centerFloat">
                                            <tbody>
                                              <tr>
                                                <td data-btn="CTA Button" align="center" style="background-color:#FFFFFF;display:block;mso-padding-alt:10px 28px 10px 28px;border-radius:50px;">
                                                  <a data-font="Primary" href="{{ $options['link'] }}" style="color:#f72e5e;display:block;font-family:'Poppins',sans-serif;font-size:12px;font-weight:600;letter-spacing:1px;line-height:24px;padding:14px 28px 14px 28px;text-decoration:none;white-space:nowrap;">Reset Password
                                                  </a>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td data-resizable-height="" style="font-size:60px;height:60px;line-height:60px;">&nbsp;
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  <!--[if (gte mso 9)|(IE)]></v:textbox></v:rect><![endif]-->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
          
		 <table data-group="Footer" data-module="Footer" data-bgcolor="Outer BG Color" width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eaeaea" style="width:100%;max-width:100%;">
            <tbody>
              <tr>
                <td align="center" valign="top">
                  <table class="row" width="700" border="0" align="center" cellpadding="0" cellspacing="0" style="width:700px;max-width:700px;">
                    <tbody>
                      <tr>
                        <td data-bgcolor="Inner BG Color" align="center" valign="top" bgcolor="#fff">
                          <table class="row" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="width:600px;max-width:600px;">
                            <tbody>
                              <tr>
                                <td data-resizable-height="" style="font-size:30px;height:35px;line-height:35px;">&nbsp;
                                </td>
                              </tr>
                              <tr>
                                <td align="center" valign="top" style="font-size:0;padding:0">
                                  <!--[if (gte mso 9)|(IE)]><table border="0" cellpadding="0" cellspacing="0"><tr><td valign="middle"><![endif]-->
                                  <div class="row" style="display:inline-block;max-width:600px;vertical-align:middle;width:100%">
                                    <table class="row" align="center" border="0" cellpadding="0" cellspacing="0">
                                      <tbody>
                                        <tr>
                                          <td style="font-family:'Open Sans',Arial,helvetica,sans-serif;color:#939393;font-size:14px;line-height:22px;font-weight:400;letter-spacing:0px;" contenteditable="true" data-gramm="false"> © 2022 Entertainment Bugs. All Rights Reserved 
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <!--[if (gte mso 9)|(IE)]></td><td valign="top"><![endif]-->
                                  <div class="row" style="display:inline-block;max-width:20px;vertical-align:top;width:100%">
                                    <table class="row" border="0" align="left" cellpadding="0" cellspacing="0">
                                      <tbody>
                                        <tr>
                                          <td height="20" align="center" valign="top" style="font-size:20px;line-height:20px;">&nbsp;
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                  <!--[if (gte mso 9)|(IE)]></td><td valign="middle"><![endif]-->
                                
                                  <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                </td>
                              </tr>
                              <tr>
                                <td data-resizable-height="" style="font-size:30px;height:25px;line-height:25px;">&nbsp;
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
            </tbody>
          </table>
       
	   </center>
      </div>
    </div>