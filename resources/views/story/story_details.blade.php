@extends('layouts.home')
@if (!empty($story))
    @php
        
        $isFollowing = $story->user->isFollowedBy() ? true : false;
        $followCount = $story->user->getFollowerCount();
        $coverBasePath = STORY_COVER_UPLOAD_PATH . $story->cover_image;
        $coverUrlPath = get_story_thumb_url($story->cover_image, 'cover');
        
        $userBastPath = PROFILE_SMALL_UPLOAD_PATH . $story->user->photo;
        $userUrlPath = PROFILE_SMALL_DISPLAY_PATH . $story->user->photo;
        
        $created_at = strtotime($story->created_at);
        $curDatetime = time();
        $difference = $curDatetime - $created_at;
        
        $totalStories = \App\Models\Story::where('user_id', $story->user_id)->where('stories.isDeleted',0)->count();
        
        if (isset($user) && $user->photo != '') {
            $curuserBastPath = PROFILE_SMALL_UPLOAD_PATH . $user->photo;
            $curuserUrlPath = PROFILE_SMALL_DISPLAY_PATH . $user->photo;
            $userName = $user->name;
        }
    @endphp
    @section('meta_tags')
        <meta name="keywords" content="{{ $story->title ?? '' }}" >
        <meta name="description" content="{{ $story->about ?? '' }}" >
        <meta property="og:title" content="{{ $story->title ?? '' }} Vidoes" />
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:image" content="{{ photo_url($coverUrlPath) }}" />
        <meta property="og:description" content="{{ $story->about ?? '' }}" >
    @endsection
    @section('content')
            <style>
                .sv-views-progress {
                    top: 18px;
                }
 

                #obj-img img {
                    width: 50px;
                    height: 50px;
                    object-fit: cover;
                }

                #obj-img1 img {
                    width: 50px;
                    height: 50px;
                    object-fit: cover;
                }

                .story-title {
                    background: #fff;
                    color: #000;
                    font-size: 24px;
                    margin: 0 0 0 5px
                }

                .story-title h5 {
                    font-family: Montserrat, sans-serif;
                    font-size: 26px;
                    font-weight: 500;
                    padding: 17px 15px 14px 15px;margin: 0;line-height: 32px;
}
                
               
@media only screen and (max-width: 767px)  {
.acide-panel.acide-panel-top{display:none !Important;}
.single-video .author-head > * {width: 70px;}
.single-video .author .sv-name > div:first-child a {display: inline-block;}
.single-video .author .sv-views {position: relative;right: 0;margin-bottom: 20px;}
.color_bg .sv-name {width: 66% !Important;}
#about_mbl label {width: 21% !important;} 
.story-title h5 {font-size: 20px;line-height: 25px;}
    
    
}
                
                
                
            </style>

            <div class="color_bg" style="background:#f4f3f3">


                <div class="container" id="story_container">


                    <div class="col-lg-12 channels">
                        <div class="content-block">

                            <div class="cb-header mb-0">
                                <div class="row">
                                    <div class="col-lg-8 col-sm-8 col-xs-12">
                                        <ul class="list-inline">
                                            <li class=""><a class="color-active"> Catogies: </a></li>

                                            @if (!empty($story_categories))
                                                <span>
                                                    @php
                                                        $tc = count($story_categories);
                                                        $t = 0;
                                                    @endphp
                                                    @foreach ($story_categories as $category)
                                                        <a href="{{ URL::to('story_category/') }}/{{ $category->category }}"
                                                            target="_blank">{{ $category->category }}</a>@php
                                                                $t++;
                                                                echo $tc != $t ? ' &nbsp;' : '';
                                                            @endphp
                                                    @endforeach
                                                </span>
                                            @endif

                                        </ul>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="clear"></div>
                 
                   
                   
                    <div id="story_block_main" class="single-video row">
                     
  @if(!$story->isDeleted && !$story->user->isDeleted)
                        <div class="story_left">

                            <div class="story-title">
                                <h5>{{ $story->title }}</h5>
                            </div>


                            <div class="well" style="margin-top:0px">
                                @if (check_asset_existence($coverUrlPath))
                                    {{ HTML::image($coverUrlPath) }}
                                @else
                                    {{ HTML::image('public/img/stories.jpg', null, ['style' => 'width:100%']) }}
                                @endif

                                <div class="media">
                                    <p class="story_details">{!! $story->about !!}</p>
                                </div>
                            </div>

                            <style>
                                #story_similar_block {
                                    clear: both;
                                    overflow: hidden
                                }

                                #story_similar_block .well {
                                    width: 32.5% !Important;
                                    margin-bottom: 10px;
                                    float: left !Important
                                }

                                #story_similar_block .list-inline>li {
                                    font-size: 11px;
                                    padding: 0 1px;
                                }

                                #story_similar_block ul.list-inline li a {
                                    color: #ea2c5a !important;
                                }

                                #story_similar_block h4.media-heading {
                                    font-size: 14px;
                                    color: #ea2c5a !important;
                                }

                                .checked,
                                .fa-star-half-o {
                                    color: orange;
                                }

                                .rating_main {
                                    clear: both;
                                    overflow: hidden
                                }

                                .rating_main h4 {
                                    float: left;
                                    margin: 10px 20px 0 0
                                }

                                .rtng1 {
                                    margin-top: 11px;
                                }

                                .rtng2 {
                                    margin-top: 3px;
                                }
                            </style>



                            <div class="rating_main" style="background:#fff; padding:20px 20px 20px 10px; margin-bottom:10px;">
                                <div class="col-lg-10">
                                    <h4>Rating</h4>
                                    <span id="avgRating">@include('elements.story.rating')</span>
                                </div>

                                @if (session()->has('user_id'))
                                    <div class="col-lg-2" style="padding:0px; text-align:right">
                                        @php
                                            $rating = round($userStoryRating, 1);
                                        @endphp
                                        <div class="rtng2">
                                            @for ($i = 1; $i <= 5; $i++)
                                                @if ($i <= $rating)
                                                    <span id="{{ $i }}" class="fa fa-star checked"></span>
                                                @elseif ($rating - $i == -0.5)
                                                    <span id="{{ $i }}" class="fa fa-star-half-o"></span>
                                                @else
                                                    <span id="{{ $i }}" class="fa fa-star"></span>
                                                @endif
                                            @endfor
                                            <br>Rate this story {{ $userStoryRating }}
                                        </div>
                                    </div>
                                @endif
                            </div>


                            <div class="custom-tabs">
                                <div class="tabs-panel hidden-xs">
                                    <a href="#" class="active" data-tab="tab-1" style="display:none">
                                        <i class="cv cvicon-cv-about" data-toggle="tooltip" data-placement="top"
                                            title="About"></i>
                                        <span>About</span>
                                    </a>
                                    <!--<a href="#" data-tab="tab-2">-->
                                    <!--    <i class="cv cvicon-cv-share" data-toggle="tooltip" data-placement="top"-->
                                    <!--        title="Share"></i>-->
                                    <!--    <span>Share</span>-->
                                    <!--</a>-->
                                    @if (Session::has('user_id'))
                                        <div class="acide-panel hidden-xs">
                                            <a href="{{ $story->id }}" style="font-size:15px;line-height:25px;"
                                                id="likestory"><i class="fa fa-thumbs-up" data-toggle="tooltip"
                                                    data-placement="top" title="" style="margin-right: 5px;"
                                                    data-original-title="Liked"></i> <span
                                                    id="likedCount">{{ $storyLikeCount }}</span></a>
                                            <a href="{{ $story->id }}" style="font-size:15px;line-height:25px;"
                                                id="dislikestory"><i class="fa fa-thumbs-down" data-toggle="tooltip"
                                                    data-placement="top" title="" style="margin-right: 5px;"
                                                    data-original-title="Unlinked"></i><span
                                                    id="unLikedCount">{{ $storyDisLikeCount }}</span></a>
                                        </div>
                                    @endif
                                </div>


                                <div class="acide-panel acide-panel-top">
                                    <a href="#"><i class="cv cvicon-cv-liked" data-toggle="tooltip" data-placement="top"
                                            title="Liked"></i></a>
                                    <a href="#"><i class="cv cvicon-cv-watch-later" data-toggle="tooltip"
                                            data-placement="top" title="Watch Later"></i></a>
                                    <a href="#"><i class="cv cvicon-cv-flag" data-toggle="tooltip" data-placement="top"
                                            title="Flag"></i></a>
                                </div>

 
                                <div class="clearfix"></div>

                                <!-- BEGIN tabs-content -->
                                <div class="tabs-content" style="background:#fff">
                                    <!-- BEGIN tab-1 -->
                                    <div class="tab-1">
                                        <div class="col-xs-12" style="width:100%">

                                            <div class="author" style="box-shadow: none;border: 0px;padding: 20px 0 10px 0px;margin: 0;">
                                             
                                                
                                                <div class="author-head">
                                                    <div class="pull-left story_img" href="#">
                                                        @if (file_exists($userBastPath))
                                                            <a class="pull-left story_img" href="{{ user_url($story->user->slug) }}" id="obj-img">
                                                                {{ HTML::image($userUrlPath, null, ['class' => 'media-object']) }}
                                                            </a>
                                                        @else
                                                            <a class="pull-left story_img" href="{{ user_url($story->user->slug) }}" id="obj-img1">
                                                                {{ HTML::image('http://placekitten.com/150/150', null, ['class' => 'media-object']) }}
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <div class="sv-name">
                                                        <div><a href="{{ user_url($story->user->slug) }}">{{ $story->user->name }}</a> .
                                                            {{ $totalStories }} Stories</div>
                                                        <div class="c-sub" id="followunfollowbtn">
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="author-border"></div>
                                                 
                                               
                                                <div class="sv-views">
                                                    <div class="sv-views-count">
                                                        {{ $story->total_views_count }} views
                                                    </div>
                                                    <div class="sv-views-progress">
                                                        <div class="sv-views-progress-bar"></div>
                                                    </div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>


                                            <div id="video_up_preview">
                                                <div class="col-lg-12" id="about_mbl">
                                                    <div class="form-group">
                                                        <label for="e1">Story Tags:</label>

                                                         
                                                            @php
                                                                $tags = explode(',', $story->tags);
                                                            @endphp

                                                            @foreach ($tags as $tag)
                                                                <form action="{{ URL::to('search') }}" method="post">
                                                                    <input type="hidden" name="_token"
                                                                        value="{{ csrf_token() }}">
                                                                    <input type="hidden" class="form-control"
                                                                        name="searchdata" aria-describedby="sizing-addon2"
                                                                        value="{{ $tag }}">
                                                                    <input type="hidden" name="searchType" id="searchType"
                                                                        value="story" />
                                                                    <input type="submit" value="{{ $tag }}">
                                                                </form>
                                                            @endforeach
                                                      

                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="e3">Language:</label>
                                                        <span>{{ $story->language }}</span>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="e3">Category:</label>
                                                        @if (!empty($story_categories))
                                                            <span>
                                                                @php
                                                                    $tc = count($story_categories);
                                                                    $t = 0;
                                                                @endphp
                                                                @foreach ($story_categories as $category)
                                                                    <a href="{{ URL::to('story_category/') }}/{{ $category->category }}"
                                                                        target="_blank">{{ $category->category }}</a>@php
                                                                            $t++;
                                                                            echo $tc != $t ? ',&nbsp;' : '';
                                                                        @endphp
                                                                @endforeach
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="e3">Release Date:</label>
                                                        <span>{{ \Carbon\Carbon::parse($story->created_at)->diffForHumans() }}</span>
                                                    </div>
                                                </div>
<Style>
#video_share_option{width: 200px;float: right;margin-right: -21px; position: relative;z-index: 7;margin-top: -40px;}
.social a.whatsapp-i {background-color: #4FCE5D;}
.social a.instagram {background-color: #d62976;padding-top: 7px;}
.social a{border-radius:0}
#video_share_option a:not(:last-child) {margin-right: 6px;}


</Style>

    <div class="social" id="video_share_option">
<a href="" style="color: #000;font-size: 12px;margin: 3px 5px 0 0;"> Share:</a>
             <!-- AddToAny BEGIN -->
<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_whatsapp"></a>
</div>

<!-- AddToAny END -->   
<!--<a href="" id="facebook" class="facebook"><i class="fa fa-facebook"></i></a>-->
<!--<a href="" id="twitter" class="twitter"><i class="fa fa fa-twitter"></i></a>-->
<!--<a href="#" class="instagram"><i class="fa fa-instagram"></i></a>-->
<!--<a href="#" class="whatsapp-i"><i class="fa fa-whatsapp" style="line-height: 17px;"></i></a>               -->
                                </div>
                                





                                            </div>

                                        </div>


                                    </div>
                                    <!-- END tab-1 -->
  
                                </div>
                                <!-- END tabs-content -->
                            </div>

                            <div class="clear"></div>


    
<!--ads start-->               
<style>
.ads-cont{margin-top:15px;border:solid 1px #eceff0}
.seven_twenty_eight #ads-banner-img{width:100% !Important}
.seven_twenty_eight #ads-banner-img img{border-bottom: 1px solid #edecec;}
.seven_twenty_eight #ads-banner-img-content{width:100% !Important;padding:6px 10px !important;}
.seven_twenty_eight .ads-trigger-btn{width: 13% !Important; right: 8%  !Important;}
.seven_twenty_eight h6{padding: 0px 5px 2px 0 !important;font-size:10px !Important;}    
.seven_twenty_eight  #ads-banner-img-content p{font-size:9px !Important;}
</style>   
<div class="seven_twenty_eight">
<div data-ad-block data-ad-type='ad' data-ad-mode='image' data-ad-size='728x90'></div>
</div>
<!--ads end-->    










                            <div class="single-v-footer" id="extrra_pad1">


                                <!-- comments -->
                                <div class="comments">
                                    <div class="reply-comment">
                                        <div class="rc-header"><i class="cv cvicon-cv-comment"></i> <span class="semibold">{{ $story->total_comments_count }}</span> Comments</div>
                                        <div class="rc-ava">
                                            @if (isset($userName))
                                                <a href="{{ URL::to('userprofile/' . $userName) }}" target="_blank">

                                                    @if (file_exists($curuserBastPath))
                                                        {{ HTML::image($curuserUrlPath, null, ['class' => 'media-object']) }}
                                                    @else
                                                        {{ HTML::image('public/img/ava5.png') }}
                                                    @endif

                                                </a>
                                            @else
                                                {{ HTML::image('public/img/ava5.png') }}
                                            @endif
                                        </div>
                                        <div class="rc-comment">
                                            <form method="post" action="{{ route('comment.add') }}" id="commentfrm">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <textarea rows="3" name="comment_body" placeholder="Share what you think?"></textarea>
                                                <input type="hidden" name="story_id" value="{{ $story->id }}" />
                                                <input type="hidden" name="model" value="Story" />
                                                <button type="submit">
                                                    <i class="cv cvicon-cv-add-comment"></i>
                                                </button>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="comments-list">

                                        <div class="cl-header">
                                            <div class="c-nav">
                                                <ul class="list-inline">
                                                    <li><a href="#" class="active"> <span
                                                                class="hidden-xs">Comments</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div id="comments">
                                            @include('comment.story._comment_replies', [
                                                'comments' => $story->comments,
                                                'video_id' => $story->id,
                                            ])
                                        </div>
                                    </div>
                                </div>
                                <!-- END comments -->
                            </div>

  @else
                                    <div class="story_left">    <div class="under-review text-center">
                    <!--<h5>This content is under review</h5>-->
                    
                    <img src="https://www.entertainmentbugs.com/public/img/review_story.png" style="margin-bottom:20px;width:100%">
                </div>
     
                @endif
    
<!--ads start-->               
<style>
 .seven_twenty_six #ads-banner-img{width:14% !important;}
 .seven_twenty_six #ads-banner-img-content{width: 84% !Important}
.seven_twenty_six .ads-trigger-btn{width: 13% !Important; right: 8% !Important;}
</style>   
<div class="seven_twenty_six">
<div data-ad-block data-ad-type='ad' data-ad-mode='image' data-ad-size='336x280'></div>
</div>
<!--ads end-->    


                        </div>



                        <div id="right_full" class="extra-bg">
                            
<Style>.video-ads-right #ads-video-img{width:100% !important;}                   
#live-video-user-list-home .users a {background: none !important;}
#right_full .media p:nth-child(1) { display: none;}
.video-ads-right #ads-video-img-content {width: 100% !important;padding: 5px 9px 9px 9px !important;}
</Style>            
<div class="video-ads-right">
<div data-ad-block data-ad-type='ad' data-ad-mode='video' data-ad-size='60'></div>
</div>
        
                            
                            
                            
                            <div class="caption" style="margin-top: 10px;padding-bottom: 5px;margin-bottom: 20px;">
                                <div class="left">
                                    <a>Recomended Stories</a>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            @if ($recommended)
                                @include('elements.story.cards', ['stories' => $recommended])
                            @endif
                            <div class="clear"></div>
                            
                            
                            
                             <div data-ad-block data-ad-type='ad' data-ad-mode='image' data-ad-size='1080x1080'></div>
 
 
 
 
                        </div>

                    </div>
                 
                    <style>
                    


                        #right_full .media p:nth-child(3) {
                            display: none;
                        }

                        #right_full .media p:nth-child(4) {
                            display: none;
                        }

                        #right_full .media p:nth-child(5) {
                            display: none;
                        }

                        #right_full .media p:nth-child(6) {
                            display: none;
                        }

                        #right_full .media p:nth-child(7) {
                            display: none;
                        }

                        #right_full .media p:nth-child(8) {
                            display: none;
                        }

                        #right_full .media p:nth-child(8) {
                            display: none;
                        }

                        #right_full .media p:nth-child(10) {
                            display: none;
                        }

                        #right_full .media {
                            padding: 0px 8px 5px 12px;
                            /* marg */
                        }
                    </style>

                </div>

                <script type="text/javascript">
                    var video_key = $(this).attr('href');
                    var userID = '<?php echo Session::get('user_id'); ?>';
                    var videoID = '<?php echo $story->id; ?>';
                    var APP_URL = "<?php echo URL::to('/'); ?>";

                    $('#likestory').click(function(e) {
                        e.preventDefault();
                        if (userID) {
                            var id = $(this).attr('href');
                            $.ajax({
                                type: 'get',
                                url: 'liked-story/' + id,
                                success: function(data) {
                                    if (data.disLikeCount > 0) {
                                        $(".fa-thumbs-down").css('color', '#f72e5e');
                                    }
                                    $("#likedCount").text(data.likeCount);
                                    $("#unLikedCount").text(data.disLikeCount);
                                    $(".fa-thumbs-up").css('color', '#f72e5e');
                                    $(".fa-thumbs-down").css('color', '#637076');
                                }
                            });
                        } else {
                            window.location.replace(APP_URL + '/login');
                        }
                    });

                    $('#dislikestory').click(function(e) {
                        e.preventDefault();
                        if (userID) {
                            var id = $(this).attr('href');
                            $.ajax({
                                type: 'get',
                                url: 'disliked-story/' + id,
                                success: function(data) {
                                    if (data.likeCount > 0) {
                                        $(".fa-thumbs-up").css('color', '#f72e5e');
                                    }
                                    $("#likedCount").text(data.likeCount);
                                    $("#unLikedCount").text(data.disLikeCount);
                                    $(".fa-thumbs-down").css('color', '#f72e5e');
                                    $(".fa-thumbs-up").css('color', '#637076');
                                }
                            });
                        } else {
                            window.location.replace(APP_URL + '/login');
                        }
                    });



                    if (userID) {
                        var isLikedByUser = '<?php echo $isLikedByUser = isset($isLikedByUser) ? $isLikedByUser : ''; ?>';
                        var isDisLikedByUser = '<?php echo $isDisLikedByUser = isset($isDisLikedByUser) ? $isDisLikedByUser : ''; ?>';

                        if (isLikedByUser > 0) {
                            $(".fa-thumbs-up").css('color', '#f72e5e');
                        }

                        if (isDisLikedByUser > 0) {
                            $(".fa-thumbs-down").css('color', '#f72e5e');
                        }

                    }

                    $("#commentfrm").submit(function(e) {
                        e.preventDefault();
                        var form = $(this);
                        var url = form.attr('action');

                        $.ajax({
                            type: "POST",
                            url: url,
                            data: form.serialize(), // serializes the form's elements.
                            success: function(data) {
                                // console.log(data);
                                $('#comments').html(''); // show response from the php script.
                                $('#comments').append(data.html);
                            }
                        });
                        $(this)[0].reset();
                    });

                    function showMe(id) {
                        $(id).toggle();
                    }

                    let id = 0;
                    $('.rtng2 span').hover(function() {
                        var id = $(this).attr('id');
                        var counter = 1;
                        var salt = $(this).attr('class');

                        $('.rtng2 span').each(function(i) {
                            if (id >= counter) {
                                $('span#' + counter).addClass("checked");
                            } else {
                                $('span#' + counter).removeClass("checked");
                            }
                            counter++;
                        });
                    });

                    $(".rtng2 span").click(function() {
                        let id = $('.rtng2 .checked').length;
                        $.ajax({
                            type: 'get',
                            url: 'rating/' + videoID + '/' + id,
                            success: function(data) {
                                $('#avgRating').html('');
                                $('#avgRating').append(data.html);
                            }
                        });
                    })

                    var isFollowing = '<?php echo $isFollowing; ?>';
                    var followCount = '<?php echo $followCount; ?>';
                    follow_unfollow(isFollowing, followCount);

                    $(document).on('submit', '#follow, #unfollow', function(e) {
                        e.preventDefault();
                        if (userID) {
                            var form = $(this);
                            var url = form.attr('action');

                            $.ajax({
                                type: "POST",
                                url: url,
                                data: form.serialize(),
                                success: function(data) {
                                    if (!data.status) {
                                        follow_unfollow(data.isFollowing, data.followCount);
                                        alert(data.message);
                                    } else {
                                        alert(data.message);
                                    }
                                }
                            });
                            $(this)[0].reset();
                        } else {
                            window.location.replace(APP_URL + '/login');
                        }
                    });

                    function follow_unfollow(isFollowing, followCount) {
                        var csrf_field = '<?php echo csrf_field(); ?>';
                        var method_field = '<?php echo method_field('DELETE'); ?>';
                        var videoData_User_ID = '<?php echo $story->user->id; ?>';
                        var unfollowformAction = '<?php echo route('unfollow', ['id' => $story->user->id]); ?>';
                        var followformAction = '<?php echo route('follow', ['id' => $story->user->id]); ?>';

                        if (isFollowing) {
                            var unfollowbtn = '<form id="unfollow" action="' + unfollowformAction + '" method="POST">' +
                                csrf_field + method_field + '<button type="submit" id="delete-follow-' +
                                videoData_User_ID + '" class="c-f">Unfollow</button><div class="c-s">' +
                                followCount + '</div></form><div class="clearfix"></div>';
                            $('#followunfollowbtn').html('');
                            $('#followunfollowbtn').append(unfollowbtn);
                        } else {
                            var followbtn = '<form id="follow" action="' + followformAction + '" method="POST">' +
                                csrf_field + '<button type="submit" id="follow-user-' + videoData_User_ID +
                                '" class="c-f">follow</button><div class="c-s">' + followCount +
                                '</div></form><div class="clearfix"></div>';
                            $('#followunfollowbtn').html('');
                            $('#followunfollowbtn').append(followbtn);
                        }

                    }
                </script>
            </div>
            <script async src="https://static.addtoany.com/menu/page.js"></script>
    @endsection
@endif
