@extends('layouts.home')
@section('content')
    <div class="content-wrapper upload-page edit-page">
        <br>
        <br>
        <div class="container">
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <div class="row" id="story_block_main">
                <div class="story_left">
                    <form action="{{ url('story/' . $story->id) }}" method="post" enctype="multipart/form-data"
                        id="video-upload">

                        {{-- {!! Form::open(['route' => 'story.store','id' => 'video-upload', 'enctype'=>'multipart/form-data']) !!} --}}
                        {{ method_field('PATCH') }}
                        {{ Form::hidden('story_id', $story->id, ['id' => 'id']) }}
                        <div class="u-form">

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                        {!! Form::label('Story Title:') !!}
                                        <span class="text-danger">{{ $errors->first('title') }}</span>
                                        {{ Form::text('title', $story->title, ['id' => 'story-title', 'close' => 'form-control', 'placeholder' => 'Title']) }}
                                        <span class="text-danger" id="story-title-error">{{ $errors->first('title') }}</span>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
                                        {!! Form::label('Story Tags:') !!}
                                        {!! Form::text('tags', $story->tags, [
                                            'class' => 'form-control',
                                            'placeholder' => 'Select mulitple tags',
                                            'id' => 'story-tags',
                                        ]) !!}
                                        <span class="text-danger">{{ $errors->first('tags') }}</span>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group {{ $errors->has('language') ? 'has-error' : '' }}">
                                        {!! Form::label('Language:') !!}
                                        {!! Form::text('language', $story->language, [
                                            'class' => 'form-control',
                                            'placeholder' => 'Choose Language',
                                            'id' => 'story-language',
                                        ]) !!}
                                        <span class="text-danger">{{ $errors->first('language') }}</span>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group {{ $errors->has('about') ? 'has-error' : '' }}">
                                        {!! Form::label('About:') !!}
                                        {!! Form::textarea('about', $story->about, [
                                            'class' => 'form-control',
                                            'id' => 'about',
                                            'placeholder' => 'Description',
                                        ]) !!}
                                        <span class="text-danger">{{ $errors->first('about') }}</span>
                                    </div>
                                </div>

                            </div>

                            <div class="row ">
                                <div class="col-lg-12 u-category">Category ( you can select upto 2 categories )</div>
                            </div>

                            <div class="row">

                                @php
                                    $exploderCategory = explode(',', $story->category);
                                @endphp
                                @if (!empty($videoCategories))
                                    @foreach ($videoCategories as $catId => $catName)
                                        <div class="col-lg-3 col-xs-6">
                                            <div class="checkbox">
                                                <label>
                                                    <label class="checkbox">


                                                        @if (in_array($catId, $exploderCategory))
                                                            {{ Form::checkbox('categories[]', $catId, ['class' => 'checked']) }}
                                                        @else
                                                            {{ Form::checkbox('categories[]', $catId) }}
                                                        @endif

                                                        <span class="arrow"></span>
                                                    </label> {{ $catName }}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif

                            </div>

                            <div class="col-lg-10" id="thumbnail_image">
                                @if (!empty($story->thumb_image))
                                    @php
                                        $thumbBasePath = STORY_THUMB_UPLOAD_PATH . $story->thumb_image;
                                        $thumbUrlPath = preg_match('/^(http|https):\/\//', $story->thumb_image) ? $story->thumb_image : STORY_THUMB_DISPLAY_PATH . $story->thumb_image;
                                        
                                        $splitted = explode('/', $thumbUrlPath);
                                        $len = count($splitted);
                                        if ($splitted[$len - 1] == $splitted[$len - 2]) {
                                            array_pop($splitted);
                                            $thumbUrlPath = implode('/', $splitted);
                                        }
                                        
                                        $coverImagePathBaseUrl = STORY_COVER_DISPLAY_PATH . $story->cover_image;
                                        $thumbUrlPathCover = preg_match('/^(http|https):\/\//', $story->cover_image) ? $story->cover_image : STORY_COVER_DISPLAY_PATH . $story->cover_image;
                                        
                                        $splitted = explode('/', $thumbUrlPathCover);
                                        $len = count($splitted);
                                        if ($splitted[$len - 1] == $splitted[$len - 2]) {
                                            array_pop($splitted);
                                            $thumbUrlPathCover = implode('/', $splitted);
                                        }
                                    @endphp
                                @endif

                            </div>
                            <div class="clear"></div>
                            <div class="row" style="margin:40px 0 0 0;">
                                <div class="col-lg-3 u-category" style="line-height:60px">Cover Image </div>
                                <div class="col-lg-9" id="cover_image"><input type="hidden" name="_token"
                                        value="{{ csrf_token() }}">
                                    <div id="thumb_image_preview1">
                                        <img src="<?= $thumbUrlPathCover ?>" id="cover_preview" />
                                    </div>
                                    <input type="file" style="display: none" target="cover_image"
                                        id="croppie_upload_image1" accept="image/x-png,image/gif,image/jpeg" />
                                    <a href="javascript:void(0);" style="display:inline-block; margin-left: 0" data-croppie
                                        data-croppie-file="#croppie_upload_image1"
                                        data-croppie-input="#placeholder_cover_image" data-croppie-output="#cover_preview"
                                        data-croppie-bind="file" title="Upload Cover Image"  data-croppie-output="#cover_preview" data-croppie-bind="file" data-croppie-viewport='{"width": 850, "height": 600}' data-croppie-boundary='{"width": 850, "height": 600}' title="Upload Cover Image" class="upload-cover-image"><i
                                            class="cvicon-cv-plus"></i></a>
                                    <input type="hidden" name="cover_image" id="placeholder_cover_image" />
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="row" style="margin:40px 0 0 0;">
                                <div class="col-lg-3 u-category" style="line-height:60px">Thumb Image</div>
                                <div class="col-lg-9" id="thumb_image">
                                    <div id="thumb_image_preview">
                                        <img src="<?= $thumbUrlPath ?>" id="thumb_preview">
                                    </div>
                                    <input type="file" style="display: none" target="thumb" id="croppie_upload_image"
                                        accept="image/x-png,image/gif,image/jpeg" />
                                    <input type="hidden" name="thumb" id="placeholder_thumb_image">
                                    <a href="javascript:void(0);" style="display:inline-block; margin: 0"
                                        title="Upload Thumbnail Image" class="upload-thumb-image" data-croppie
                                        data-croppie-file="#croppie_upload_image"
                                        data-croppie-input="#placeholder_thumb_image" 
                                        data-croppie-output="#thumb_preview" 
                                        data-croppie-bind="file" data-croppie-viewport='{"width": 435, "height": 240}' data-croppie-boundary='{"width": 435, "height": 240}'><i class="cvicon-cv-plus"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="u-area mt-small">
                            <button type="submit" class="btn btn-primary u-btn">Update</button>
                        </div>
                        <div class="u-terms">
                            <p>By submitting your videos to circle, you acknowledge that you agree to circle's <a
                                    href="#">Terms
                                    of Service</a> and <a href="#">Community Guidelines</a>.</p>
                            <p class="hidden-xs">Please be sure not to violate others' copyright or privacy rights. Learn
                                more
                            </p>
                        </div>
                    </form>
                </div>
                <div id="right_full">
                    <div class="adblock2" style="padding:5px 0 15px 0">
                        {{ HTML::image('public/img/advert-728x90px.jpg', null, ['style' => 'width:100%']) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="adblock2" style="padding:0px 0 20px 0">
            {{ HTML::image('public/img/advert-728x90px-large.jpg', null, ['style' => 'margin:0 auto; display:block;']) }}
        </div>
        
        <style>
            #profile-cover-image,
            #profile-thumb-image {
                display: none;
            }

            .upload-cover-image {
                padding: 9px 46px;
                border: 2px dotted #ccc;
                font-size: 26px;
                color: #999;
                margin: 5px;
            }

            .upload-thumb-image {
                padding: 9px 46px;
                border: 2px dotted #ccc;
                font-size: 26px;
                color: #999;
                margin: 149px 0 0 0;
            }

            #cover_image img,
            #thumb_image img {
                max-width: 150px;
                max-height: 120px;
                margin: 0px;
            }

            .video-thumb-img {
                position: relative;
                display: inline-block;
                margin: 0px 5px;
            }

            .delete-video-thumb {
                position: absolute;
                right: 0px;
                color: #f72e5e;
                background-color: #fff;
            }
        </style>

        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

        {{ HTML::style('public/css/bootstrap-tagsinput.css') }}
        {{ HTML::script('public/js/bootstrap-tagsinput.js') }}
        {{ HTML::script('public/js/bootstrap3-typeahead.js') }}

        <script type="text/javascript">
            $(document).ready(function() {

                $(".upload-cover-image").click(function() {
                    $("#profile-cover-image").trigger('click');
                });

                $(".upload-thumb-image").click(function() {
                    $("#profile-thumb-image").trigger('click');
                });

                $('#story-tags').tagsinput({
                    typeahead: {
                        source: function(query) {
                            //return $.get('http://someservice.com');
                        }
                    }
                });

                $('#story-language').tagsinput({
                    typeahead: {
                        source: ['English', 'Hindi', 'Tamil', 'Bengali', 'Gujarati']
                    },
                    freeInput: true
                });
            });

            /*Validate Thumb Image File*/
            var fileCount = 0;
            validateThumbFile = function(files, e) {

                fileCount++;
                if (fileCount == 1) {

                    var flag = true;
                    var imageType = /image.*/;
                    var file = files[0];
                    // check file type
                    if (!file.type.match(imageType)) {
                        alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
                        flag = false;
                        return false;
                    }
                    // check file size
                    if (parseInt(file.size / 1024) > (1024 * 2)) {
                        alert("File \"" + file.name + "\" is too big.");
                        flag = false;
                        return false;
                    }

                    if (flag == true) {
                        uploadImageFile(file);
                    } else {
                        return flag;
                    }
                }

            };


            /*Upload Image in Temp Folder*/
            uploadImageFile = function(file) {
                var formdata = new FormData();
                formdata.append("image", file);
                xhr = new XMLHttpRequest();
                xhr.open("POST", BASE.url("upload/temp_image"));
                xhr.onload = function() {
                        var jsonResponse = xhr.response;
                        console.log(jsonResponse);
                        result = JSON.parse(jsonResponse);
                        console.log(result);
                        if (result.status == true) {
                            var fl = $('#cover_image').find('input[type=hidden]').length;
                            fl++;
                            var fNameHtml = '<input name="covers[' + fl +
                                ']" type="hidden" required="required" id="video-thumb-' + fl + '" value="' + result
                                .file + '" />';
                            var imageHtml = '<a href="javascript:void(0)" key="' + fl +
                                '" class="video-thumb-img"><img src="' + result.image +
                                '" /><span class="delete-video-thumb"><i class="cvicon-cv-cancel"></i><span></a>';
                            $('#cover_image').find('.video-thumb-img').remove();
                            $('#cover_image').append(fNameHtml);
                            $('.upload-cover-image').after(imageHtml);
                        } else {
                            alert('File is not uploading. Please try again.')
                        }
                    };
                    var csrfToken = $('#video-upload').find('input[name=_token]').val(); xhr.setRequestHeader(
                        'X-csrf-token', csrfToken); xhr.send(formdata);
                }

                /*Validate Thumb Image File*/
                validateThumbFile1 = function(files, e) {
                    var flag = true;
                    var imageType = /image.*/;
                    var file = files[0];
                    // check file type
                    if (!file.type.match(imageType)) {
                        alert("File \"" + file.name +
                            "\" is not a valid image, Please upload only jpg,png and gif image");
                        flag = false;
                        return false;
                    }
                    // check file size
                    if (parseInt(file.size / 1024) > (1024 * 2)) {
                        alert("File \"" + file.name + "\" is too big.");
                        flag = false;
                        return false;
                    }

                    if (flag == true) {
                        uploadImageFile1(file);
                    } else {
                        return flag;
                    }

                };

                /*Upload Image in Temp Folder*/
                uploadImageFile1 = function(file) {
                    var formdata = new FormData();
                    formdata.append("image", file);
                    //console.log(file);
                    xhr = new XMLHttpRequest();
                    //console.log(xhr);
                    xhr.open("POST", BASE.url("upload/temp_image"));
                    xhr.onload = function() {
                        var jsonResponse = xhr.response;
                        console.log(jsonResponse);
                        result = JSON.parse(jsonResponse);
                        console.log(result);
                        if (result.status == true) {
                            var fl = $('#thumb_image').find('input[type=hidden]').length;
                            fl++;
                            var fNameHtml = '<input name="thumbs[' + fl +
                                ']" type="hidden" required="required" id="video-thumb-' + fl + '" value="' + result
                                .file + '" />';
                            var imageHtml = '<a href="javascript:void(0)" key="' + fl +
                                '" class="video-thumb-img"><img src="' + result.image +
                                '" /><span class="delete-video-thumb"><i class="cvicon-cv-cancel"></i><span></a>';
                            $('#thumb_image').find('.video-thumb-img').remove();
                            $('#thumb_image').append(fNameHtml);
                            $('.upload-thumb-image').after(imageHtml);
                        } else {
                            alert('File is not uploading. Please try again.')
                        }
                    };
                    var csrfToken = $('#video-upload').find('input[name=_token]').val();
                    xhr.setRequestHeader('X-csrf-token', csrfToken);
                    xhr.send(formdata);
                }

                /*Delete Video Thumb Image*/
                $(document).on('click', '.delete-video-thumb', function() {
                    fileCount--;
                    var key = $(this).parent('a:first').attr('key');

                    /*Ajax Request to remove file in temp folder*/
                    $(this).parent('a:first').remove();
                    $('#video-thumb-' + key).remove();

                });
        </script>

        <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('about');
        
            $("#video-upload").on("submit", function(e){
                const title = $(this).find("[name='title']");
                const val = title.val().trim();

                if( val == "" || typeof val == "undefined" ){
                    e.preventDefault();
                    $("#story-title-error").text("Title must not be empty");
                } else if( val != "" && val.length > 100 ){
                    e.preventDefault();
                    $("#story-title-error").text("Title must not be more than 100");
                    $('html, body').animate({
                        scrollTop: $("#story-title-error").offset().top
                    }, 500)
                    return false;
                } else {
                    $("#story-title-error").text("");
                }
            })
        </script>

    @endsection