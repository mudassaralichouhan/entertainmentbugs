@extends('layouts.home')
@section('content')
    <div class="content-wrapper upload-page edit-page">
        <br>
        <br>
        <div class="container">
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif

            <div class="row" id="story_block_main">
                <div class="story_left">
                    {!! Form::open(['route' => 'story.store', 'id' => 'video-upload', 'enctype' => 'multipart/form-data']) !!}
                    {{ Form::hidden('video', null, ['id' => 'selected-video-name']) }}
                    {{ Form::hidden('selected_thumb', null, ['id' => 'video-selected-thumbnail']) }}
                    <div class="u-form">

                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                    {!! Form::label('Story Title:') !!}
                                    {!! Form::text('title', old('title'), [
                                        'class' => 'form-control',
                                        'required' => true,
                                        'placeholder' => 'Story Title',
                                    ]) !!}
                                    <span class="text-danger" id="story-title-error">{{ $errors->first('title') }}</span>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
                                    {!! Form::label('Story Tags:') !!}
                                    {!! Form::text('tags', old('tags'), [
                                        'class' => 'form-control',
                                        'required' => true,
                                        'placeholder' => 'Select mulitple tags',
                                        'id' => 'story-tags',
                                    ]) !!}
                                    <span class="text-danger">{{ $errors->first('tags') }}</span>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group {{ $errors->has('language') ? 'has-error' : '' }}">
                                    {!! Form::label('Language:') !!}
                                    {!! Form::text('language', old('language'), [
                                        'class' => 'form-control',
                                        'required' => true,
                                        'placeholder' => 'Choose Language',
                                        'id' => 'story-language',
                                    ]) !!}
                                    <span class="text-danger">{{ $errors->first('language') }}</span>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group {{ $errors->has('about') ? 'has-error' : '' }}">
                                    {!! Form::label('About:') !!}
                                    {!! Form::textarea('about', old('about'), [
                                        'class' => 'form-control',
                                        'id' => 'about',
                                        'placeholder' => 'Description',
                                    ]) !!}
                                    <span class="text-danger">{{ $errors->first('about') }}</span>
                                </div>
                            </div>

                        </div>

                        <div class="row ">
                            <div class="col-lg-12 u-category">Category ( you can select upto 2 categories )</div>
                        </div>

                        <div class="row" id="blg01">
                            @if (!empty($videoCategories))
                                @foreach ($videoCategories as $catId => $catName)
                                    <div class="col-lg-3 col-xs-6">
                                        <div class="checkbox">
                                            <label>
                                                <label class="checkbox">
                                                    {{ Form::checkbox('categories[]', $catId) }}
                                                    <span class="arrow"></span>
                                                </label> {{ $catName->category }}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                        </div>

                        <div class="row" style="margin:40px 0 0 0;">
                            <div class="col-lg-12 u-category" style="line-height:0px;padding-left:0px">Cover Image </div>
                            <div class="col-lg-12" id="cover_image" style="padding-left:0px"><input type="hidden" name="_token"
                                    value="{{ csrf_token() }}">
                                <div id="thumb_image_preview1">
                                    <img src="" id="cover_preview" />
                                </div>
                                <div class="u-progress" id="upload-photo-progress-0" style="display: none; width: 100%">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                <input type="file" style="display: none" target="cover_image" id="croppie_upload_image1"
                                    accept="image/x-png,image/gif,image/jpeg" required="required" />
                                <a href="javascript:void(0);" style="display:inline-block; margin-left: 0" data-croppie data-croppie-file="#croppie_upload_image1"
                                data-croppie-input="#placeholder_cover_image" data-croppie-progress="#upload-photo-progress-0" data-croppie-output="#cover_preview" data-croppie-bind="file" data-croppie-viewport='{"width": 850, "height": 600}' data-croppie-boundary='{"width": 850, "height": 600}' title="Upload Cover Image" class="upload-cover-image"><i class="cvicon-cv-plus"></i></a>
                                <input type="hidden" name="cover_image" id="placeholder_cover_image" />
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="row" style="margin:40px 0 0 0;">
                            <div class="col-lg-121 u-category" style="line-height:0px">Thumb Image</div>
                            <div class="col-lg-912" id="thumb_image">
                                <div class="u-progress" id="upload-photo-progress-1" style="display: none; width: 100%">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                <div id="thumb_image_preview">
                                    <img src="" id="thumb_preview">
                                </div>
                                <input type="file" style="display: none" target="thumb" id="croppie_upload_image" accept="image/x-png,image/gif,image/jpeg" required="required" />
                                <input type="hidden" name="thumb" id="placeholder_thumb_image">
                                <a href="javascript:void(0);" style="display:inline-block; margin: 0" title="Upload Thumbnail Image" class="upload-thumb-image" data-croppie data-croppie-file="#croppie_upload_image"
                                data-croppie-input="#placeholder_thumb_image" data-croppie-output="#thumb_preview" data-croppie-bind="file" data-croppie-viewport='{"width": 435, "height": 240}' data-croppie-boundary='{"width": 435, "height": 240}' data-croppie-progress="#upload-photo-progress-1"><i
                                        class="cvicon-cv-plus"></i></a>
                            </div>
                        </div>
                    </div>

                    <div class="u-area mt-small">
                        <button class="btn btn-primary u-btn">Save</button>
                    </div>
                    <div class="u-terms">
                        <p>By submitting your stories to our platform, you acknowledge that you agree to our <a
                                href="#">Terms
                                of Service</a> and <a href="#">Community Guidelines</a>.</p>
                        <p class="hidden-xs">Please be sure not to violate others' copyright or privacy rights. Learn more
                        </p>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div id="right_full">

                    @if ($stories)
                        @include('elements.story.cards')
                    @endif

                    <div class="adblock2" style="padding:5px 0 15px 0">
                        {{ HTML::image('public/img/advert-728x90px.jpg', null, ['style' => 'width:100%']) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="adblock2" style="padding:0px 0 20px 0">
            {{ HTML::image('public/img/advert-728x90px-large.jpg', null, ['style' => 'margin:0 auto; display:block;']) }}
        </div>
        <style>
 
#blg01 {
    padding: 20px 20px 10px 20px;
    background: #fff;
    margin-right: 0px;
}
        
            #profile-cover-image,
            #profile-thumb-image {
                display: none;
            }

            .upload-cover-image {
                padding: 9px 46px;
                border: 2px dotted #ccc;
                font-size: 26px;
                color: #999;
                margin: 5px;
            }

            .upload-thumb-image {
                padding: 9px 46px;
                border: 2px dotted #ccc;
                font-size: 26px;
                color: #999;
                margin: 149px 0 0 0;
            }

            #cover_image img,
            #thumb_image img {
                max-width: 120px;
                max-height: 120px;
                margin: 0px;
            }

            .video-thumb-img {
                position: relative;
                display: inline-block;
                margin: 0px 5px;
            }

            .delete-video-thumb {
                position: absolute;
                right: 0px;
                color: #f72e5e;
                background-color: #fff;
            }
  
            
            
       body.light .content-wrapper {
    background-color: #f4f3f3;
}     



.progress{height:8px;}
.progress-bar{  height:8px;  background-color: #28b47e !important;}




             @media  only screen and (max-width: 767px) {

#croppie-modal-0 .cr-viewport.cr-vp-square{width:298px !Important;height:210px !Important;}
#croppie-modal-0 .cr-boundary{width:298px !Important;height:210px !Important;}

#croppie-modal-1 .cr-viewport.cr-vp-square{width:300px !Important;height:192px !Important;}
#croppie-modal-1 .cr-boundary{width:300px !Important;height:192px !Important;}
 .u-form{padding:0 15px;}
  }
            
        </style>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        {{ HTML::style('public/css/bootstrap-tagsinput.css') }}
        {{ HTML::script('public/js/bootstrap-tagsinput.js') }}
        {{ HTML::script('public/js/bootstrap3-typeahead.js') }}

        <script type="text/javascript">
            $(document).ready(function() {

                $(".upload-cover-image").click(function() {
                    $("#profile-cover-image").trigger('click');
                });

                $(".upload-thumb-image").click(function() {
                    $("#profile-thumb-image").trigger('click');
                });


                $("#placeholder_thumb_image").on('change', function() {
                    $("#thumb_image_preview img").attr('src', $(this).val())
                });

                $("#placeholder_cover_image").on('change', function() {
                    $("#thumb_image_preview1 img").attr('src', $(this).val());
                });




                $('#story-tags').tagsinput({
                    typeahead: {
                        source: function(query) {
                            //return $.get('http://someservice.com');
                        }
                    }
                });

                $('#story-language').tagsinput({
                    typeahead: {
                       
                       
                       	source: ['Afar', 'Abkhaz', 'Avestan', 'Afrikaans', 'Akan', 'Amharic','Aragonese', 'Arabic', 'Assamese', 'Avaric', 'Aymara', 'Azerbaijani','South Azerbaijani', 'Bashkir', 'Belarusian', 'Bulgarian', 'Bihari', 'Bislama',
    		'Bambara', 'Bengali', 'Tibetan Standard', 'Breton', 'Bosnian', 'Catalan; Valencian','Chechen', 'Chamorro', 'Corsican', 'Cree', 'Czech', 'Old Church Slavonic','Chuvash', 'Welsh', 'Danish', 'German', 'Maldivian', 'Dzongkha',
    		'Ewe', 'Greek', 'English', 'Esperanto', 'Spanish', 'Estonian','Basque', 'Persian ', 'Fula', 'Finnish', 'Fijian', 'Faroese',' French', 'Western Frisian', 'Irish', 'Scottish Gaelic', 'Galician', 'Guaraní',
    		'Gujarati', 'Manx', 'Hausa', 'Hebrew ', 'Hindi', 'Hiri Motu','Croatian', 'Haitian', 'Hungarian', 'Armenian', 'Herero', 'Interlingua',' Indonesian', 'Interlingue', 'Igbo', 'Nuosu', 'Inupiaq', 'Ido',
    		'Icelandic', 'Italian', 'Inuktitut', 'Japanese', 'Javanese', 'Georgian','Kongo', 'Gikuyu', 'Kwanyama', 'Kazakh', 'Kalaallisut', 'Khmer','Kannada', 'Korean', 'Kanuri', 'Kashmiri', 'Kurdish', 'Komi',
    		'Cornish', 'Kyrgyz', 'Latin', 'Luxembourgish', 'Ganda', 'Limburgish','Lingala', 'Lao', 'Lithuanian', 'Luba-Katanga', 'Latvian', 'Malagasy','Marshallese', 'Māori', 'Macedonian', 'Malayalam', 'Mongolian', 'Marathi',
    		'Malay', 'Maltese', 'Burmese', 'Nauru', 'Norwegian Bokmål', 'North Ndebele','Nepali', 'Ndonga', 'Dutch', 'Norwegian Nynorsk', 'Norwegian', 'South Ndebele','Navajo', 'Chichewa', 'Occitan', 'Ojibwe', 'Oromo', 'Oriya',
    		'Ossetian', 'Panjabi', 'Pāli', 'Polish', 'Pashto', 'Portuguese','Quechua', 'Romansh', 'Kirundi', 'Romanian', 'Russian', 'Kinyarwanda','Sanskrit', 'Sardinian', 'Sindhi', 'Northern Sami', 'Sango', 'Sinhala',
    		'Slovak', 'Slovene', 'Samoan', 'Shona', 'Somali', 'Albanian','Serbian', 'Swati', 'Southern Sotho', 'Sundanese', 'Swedish', 'Swahili','Tamil', 'Telugu', 'Tajik', 'Thai', 'Tigrinya', 'Turkmen',
    		'Tagalog', 'Tswana', 'Tonga', 'Turkish', 'Tsonga', 'Tatar','Twi', 'Tahitian', 'Uyghur', 'Ukrainian', 'Urdu	', 'Uzbek','Venda', 'Vietnamese', 'Volapük', 'Walloon', 'Wolof', 'Xhosa',
    		'Yiddish', 'Yoruba', 'Zhuang', 'Chinese', 'Zulu']
    	  },
    	  
    	  
    	  
                    },
                    freeInput: true
                });
            });

            /*Validate Thumb Image File*/
            var fileCount = 0;
            validateThumbFile = function(files, e) {

                fileCount++;
                if (fileCount == 1) {

                    var flag = true;
                    var imageType = /image.*/;
                    var file = files[0];
                    // check file type
                    if (!file.type.match(imageType)) {
                        alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
                        flag = false;
                        return false;
                    }
                    // check file size
                    if (parseInt(file.size / 1024) > (1024 * 2)) {
                        alert("File \"" + file.name + "\" is too big.");
                        flag = false;
                        return false;
                    }

                    if (flag == true) {
                        uploadImageFile(file);
                    } else {
                        return flag;
                    }
                }

            };

            /*Upload Image in Temp Folder*/
            uploadImageFile = function(file) {
                var formdata = new FormData();
                formdata.append("image", file);
                console.log(file);
                xhr = new XMLHttpRequest();
                //console.log(xhr);
                xhr.open("POST", "upload/temp_image");
                xhr.onload = function() {
                    var jsonResponse = xhr.response;
                    console.log(jsonResponse);
                    result = JSON.parse(jsonResponse);
                    console.log(result);
                    if (result.status == true) {
                        var fl = $('#cover_image').find('input[type=hidden]').length;
                        fl++;
                        var fNameHtml = '<input name="covers[' + fl +
                            ']" type="hidden" required="required" id="video-thumb-' + fl + '" value="' + result.file +
                            '" />';
                        var imageHtml = '<a href="javascript:void(0)" key="' + fl +
                            '" class="video-thumb-img"><img src="' + result.image +
                            '" /><span class="delete-video-thumb"><i class="cvicon-cv-cancel"></i><span></a>';
                        $('#cover_image').append(fNameHtml);
                        $('.upload-cover-image').after(imageHtml);
                    } else {
                        alert('File is not uploading. Please try again.')
                    }
                };
                var csrfToken = $('#video-upload').find('input[name=_token]').val();
                xhr.setRequestHeader('X-csrf-token', csrfToken);
                xhr.send(formdata);
            }

            /*Validate Thumb Image File*/
            validateThumbFile1 = function(files, e) {
                var flag = true;
                var imageType = /image.*/;
                var file = files[0];
                // check file type
                if (!file.type.match(imageType)) {
                    alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
                    flag = false;
                    return false;
                }
                // check file size
                if (parseInt(file.size / 1024) > (1024 * 2)) {
                    alert("File \"" + file.name + "\" is too big.");
                    flag = false;
                    return false;
                }

                if (flag == true) {
                    uploadImageFile1(file);
                } else {
                    return flag;
                }

            };

            /*Upload Image in Temp Folder*/
            uploadImageFile1 = function(file) {
                var formdata = new FormData();
                formdata.append("image", file);
                //console.log(file);
                xhr = new XMLHttpRequest();
                //console.log(xhr);
                xhr.open("POST", "upload/temp_image");
                xhr.onload = function() {
                    var jsonResponse = xhr.response;
                    console.log(jsonResponse);
                    result = JSON.parse(jsonResponse);
                    console.log(result);
                    if (result.status == true) {
                        var fl = $('#thumb_image').find('input[type=hidden]').length;
                        fl++;
                        var fNameHtml = '<input name="thumbs[' + fl +
                            ']" type="hidden" required="required" id="video-thumb-' + fl + '" value="' + result.file +
                            '" />';
                        var imageHtml = '<a href="javascript:void(0)" key="' + fl +
                            '" class="video-thumb-img"><img src="' + result.image +
                            '" /><span class="delete-video-thumb"><i class="cvicon-cv-cancel"></i><span></a>';
                        $('#thumb_image').append(fNameHtml);
                        $('.upload-thumb-image').after(imageHtml);
                    } else {
                        alert('File is not uploading. Please try again.')
                    }
                };
                var csrfToken = $('#video-upload').find('input[name=_token]').val();
                xhr.setRequestHeader('X-csrf-token', csrfToken);
                xhr.send(formdata);
            }

            /*Delete Video Thumb Image*/
            $(document).on('click', '.delete-video-thumb', function() {
                fileCount--;
                var key = $(this).parent('a:first').attr('key');

                /*Ajax Request to remove file in temp folder*/
                $(this).parent('a:first').remove();
                $('#video-thumb-' + key).remove();

            });
        </script>

        <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('about');
        
            $("#video-upload").on("submit", function(e){
                const title = $(this).find("[name='title']");
                const val = title.val().trim();

                if( val == "" || typeof val == "undefined" ){
                    e.preventDefault();
                    $("#story-title-error").text("Title must not be empty");
                } else if( val != "" && val.length > 100 ){
                    e.preventDefault();
                    $("#story-title-error").text("Title must not be more than 100");
                    $('html, body').animate({
                        scrollTop: $("#story-title-error").offset().top
                    }, 500)
                    return false;
                } else {
                    $("#story-title-error").text("");
                }
            })
        </script>

    @endsection