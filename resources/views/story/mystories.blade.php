@extends('layouts.home')
@section('content')
    <section class="channel light">
        <style>
           
           
                #mobile-video-and-stories .stories_ac a{ border-bottom: 2px solid #f72e5e;
    color: #f72e5e;}
         
           
           h4.media-heading {
                color: #fff;
                margin-top: 1px;
                font-weight: normal;
                font-size: 14px;
                font-weight: normal;
            }

            .add_video {
                width: 100%;
                height: 150px;
                background-color: #eceff0;
                margin: 20px 0 0 0;
                text-align: center
            }

            .add_video a {
                padding: 52px 0 0 0;
                display: block;
                height: 100%;
                width: 100%
            }
        </style>
        @if (Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        @include('elements.header_profile')

        @php
            $loggedName = session()->get('user_name');
            $profileName = $user->name;
        @endphp

        <style>
            .plus-details {
                display: block !important;
                position: absolute;
                left: 0px;
                top: -56px;
            }

            .plus-details ul {
                list-style: none;
                margin: 0;
                padding: 2px 0 0 10px;
                width: 170px
            }

            .plus-details ul li {
                float: left;
                margin: 0 2%;
                width: 46%;
            }

            .plus-details ul li a.edt-016 {
                text-align: center !important;
                border-radius: 5px;
                background: #28b47e !important;
                line-height: 27px;
                display: block;
            }

            .media p {
                height: 13px;
                overflow: hidden;
            }

            .media p:nth-child(1) {
                display: none;
            }

            .media p:nth-child(3) {
                display: none;
            }

            .media ul.list-inline {
                padding: 0 4px;
            }
        </style>
        @if ($profileName == $loggedName)
        @else
            <style>
                .plus-details {
                    display: none !important;
                    position: absolute;
                    left: 0px;
                    top: -65px;
                }
            </style>
        @endif


        <div class="content-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- Featured Videos -->
                        <div class="content-block" style="padding:20px 0">
                            <div class="cb-content videolist">
                                <div class="row">
                                    @if (!empty($stories))
                                        @include('elements.story.cards', ['type' => 'profile', 'isLoggedInUser' => $user->id == Session::get('user_id')])
                                    @endif

                                    <div style="clear:both; overflow:hidden"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /Featured Videos -->
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
