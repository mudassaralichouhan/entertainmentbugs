@extends('layouts.home')
@section('content')
   <style>
#story_block h4{font-size: 13px;height: 31px;overflow: hidden;line-height: 16px;}
.story_img{position:absolute;right:5px;;bottom:40px;}

 @media  only screen and (max-width: 767px) {
     #categories-shown-in-all { height: 54px;overflow: hidden}
#categories-shown-in-all{display:block !Important;background: #fff; border-top: solid 1px #eceff0;padding: 11px 0 10px 0;border-bottom: solid 1px #eceff0}
#categories-shown-in-all .owl-theme .owl-controls{display: none !important;}
#no-mbl-dis-06{display:none;}
#full-width-mobile{width:100% !important;padding: 0 !important;border: 0px !important;}
#owl-demo1 .owl-item { width: auto !important;padding: 0 3px;}
#owl-demo1 a.nw2{background: #28b47e !important;padding:8px 10px 6px 10px;display: block;font-size: 12px !important;color: #fff;border-radius: 100px;line-height: 16px;}
#owl-demo1  a.color-active.nw2{background:#f72e5e !important; color:#fff !important;}
.sticky {position: fixed;top: 50px;background:#fff;z-index:99;backgroundwidth: 100%;}
#search_stories{display:none;}
#bottom-menu a i{margin-bottom:8px;}
#bottom-menu a img{margin-bottom:8px;}
#story_block  {  padding: 0 13px !important;}
#owl-demo1{display: block !important}


}
   .list-inline li.active1 a{color:#f72e5e !important;}
</style>

<div class="color_bg1" style="background:#f4f3f3">
   <div class="container" id="story_container">


         <div class="span12" id="categories-shown-in-all" style="display:none">
    		<div id="owl-demo1" class="owl-carousel">
                 @if(!empty($story_categories))
                    @php $tc = count($story_categories); $t = 0; @endphp
                    @foreach($story_categories as $cat)
                       <div class="item" >
                     <a class="nw2" href="{{ URL::to('story_category/')}}/{{$cat}}" target="">{{$cat}}</a>
                   </div>
                    @endforeach
                @endif
          </div>
        </div>


       <div class="col-lg-12 channels">
           <div class="content-block">
               <div class="cb-header mb-0">
                   <div class="row">
                       <div class="col-lg-8 col-sm-8 col-xs-12" id="no-mbl-dis-06">
                           <ul class="list-inline">
                               <li class=""><a class="color-active">  Catogies: </a></li>
                               @if(!empty($story_categories))
                                    <span>
                                    @php $tc = count($story_categories); $t = 0; @endphp
                                    @foreach($story_categories as $cat)
                                     <a href="{{ URL::to('story_category/')}}/{{$cat}}" target="">{{$cat}}</a>@php $t++; echo (($tc != $t)?',&nbsp;':""); @endphp
                                    @endforeach
                                    </span>
                                @endif
                           </ul>
                       </div>
                       @if($category)
                       <div class="col-lg-8 col-sm-8 col-xs-12">
                           <ul class="list-inline">
                               <li class=""><a class="color-active">  Search By: </a></li>
                               <li class=""><a href="#">{{ $category }}</a></li>
                           </ul>
                       </div>
                       @endif
                       <div class="col-lg-4 col-sm-4 col-xs-12" id="search_stories">
                           <div class="cb-search channels-search">
                                <form action="{{URL::to('searchstory/')}}" method="POST" role="search">
                                    {{ csrf_field() }}
                                    <label>
                                        <input type="search" name="searchStory" placeholder="Search Stories">
                                        <i class="fa fa-search"></i>
                                    </label>
                               </form>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <div id="story_block">
            @if(!empty($topOptionsStrory))
                @php
                    $stories = $topOptionsStrory;
                @endphp
                <div id="story_card">
                    @include('elements.story.cards')
                </div>

            @endif

    <!--       <div class="adblock2" style="padding:0px 0 20px 0">-->
				<!--{{HTML::image('public/img/advert-728x90px-large.jpg',null,['style'=>'margin:0 auto; display:block'])}}-->
    <!--       </div>-->

           <div class="clear"></div>
       </div>
   </div>
   <div class="container" id="story_container">
       <div class="col-lg-12 channels">
           <div class="content-block">
               <div class="cb-header mb-0">
                   <div class="row">
                       <div class="col-lg-8 col-sm-8 col-xs-10">
                           <ul class="list-inline">
                               <li class="hidden-xs"><a href="#" class="color-active">Most Popular Stories</a></li>
                           </ul>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       <div id="story_block">

        @if(!empty($mostViewedStory))
            @php
                $stories = $mostViewedStory;
            @endphp
            @include('elements.story.cards')
        @endif

           <!--<div class="adblock2" style="padding:0px 0 20px 0">-->
           <!--   {{HTML::image('public/img/advert-728x90px-large.jpg',null,['style'=>'margin:0 auto; display:block'])}}-->
           <!--</div>-->
           <div class="clear"></div>
       </div>
   </div>

    <div class="container" id="story_container">
        <div class="col-lg-12 channels">
            <div class="content-block">
                <div class="cb-header mb-0">
                    <div class="row">
                        <div class="col-lg-8 col-sm-8 col-xs-10">
                            <ul class="list-inline">
                                <li class="hidden-xs"><a href="#" class="color-active">Top Rated Stories</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="story_block">

            @if(!empty($mostRatedStory))
                @php
                    $stories = $mostRatedStory;
                @endphp
                @include('elements.story.cards')
            @endif

            <!--<div class="adblock2" style="padding:0px 0 20px 0">-->
            <!--    {{HTML::image('public/img/advert-728x90px-large.jpg',null,['style'=>'margin:0 auto; display:block'])}}-->
            <!--</div>-->
            <div class="clear"></div>
        </div>
    </div>

</div>


<script type="text/javascript">
	var page = 1;
	$(window).scroll(function() {
	    if($(window).scrollTop() + $(window).height() >= $(document).height()) {
	        page++;
	        loadMoreData(page);
	    }
	});


	function loadMoreData(page){
	  $.ajax(
	        {
	            url: '?page=' + page,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data.html == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
	            $('.ajax-load').hide();
	            $("#home_page_video").append(data.html);
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}

</script>
<style>
    #owl-demo1 .item{
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }

 #owl-demo1 .item{
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }
    .customNavigation{
      text-align: center;
    }
    .customNavigation a{
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }
    </style>

<script>
    $(document).ready(function() {

      var owl = $("#owl-demo1");

      owl.owlCarousel({

      itemsTablet: [600,4], //2 items between 600 and 0;
      itemsMobile :[600,4] // itemsMobile disabled - inherit from itemsTablet option

      });
    });
    </script>


@endsection