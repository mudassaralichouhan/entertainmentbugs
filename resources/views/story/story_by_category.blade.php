@extends('layouts.home')
@section('content')
 <style>
#story_card h4 {font-size: 13px;height: 31px;overflow: hidden;line-height: 16px;color:#fff;}
#story_card p{font-size:12px;    margin: 0 0 4px 0;}
#story_card img.media-object {position: absolute;right: 5px;bottom: 40px;width: 35px;margin: 0 5px 0 0;}
body.light .content-wrapper {background-color: #f4f3f3;}
.content-block.head-div.head-arrow .head-arrow-icon{background-color: #f4f3f3;}
 #desk-pad{padding:0 10px;}
#story_card .media h2{font-size:14px;font-weight:bold;margin: 10px 0;}
#story_card .media h3{font-size:12px;font-weight:500;margin: 10px 0;}
#story_card .media h4{font-size:13px;}
#story_card .media h5{font-size:12px;}
#story_card .media h6{font-size:12px;}
#story_card p {height: 13px;overflow: hidden;}
#story_card .media p:nth-child(2){display:block;}
#story_card .media p{display:none;}
#story_card .media h3{display:none;}
#story_card .media h4{display:none;}
#story_card .media h2{display:none;}
#story_card .media h6{display:none;}
#story_card .media h5{display:none;}
#story_card .media blockquote{display:none;}
#story_card p:nth-child(1){display:none;}
#story_card p:nth-child(3){display:none;}
#story_card ul.list-inline {  text-align:left;}
#story_card ul.list-inline li{font-size:10.80px; text-align:right;}
#story_card ul.list-inline li span i{font-size:18px; margin-left:5px;}
#story_card ul.list-inline li { padding-left: 1px;padding-right: 1px;}
#story_card ul.list-inline-block li a{color:#f72e5e !important}
#story_card ul.list-inline{margin:0px;}
.main-ban {height: 132px;}
.main-ban img {width: 100%;object-fit: cover;height: 135px;}
.v-categories.side-menu .content-block .cb-header {margin-bottom: 15px;}
.v-categories .content-block .cb-header {padding: 7px 0;
</style>



   <div class="content-wrapper" style="background:#fff !important">
    <div class="container" style="padding:0 5px">

          
                        <div class="span12" id="categories-shown-in-all" style="display:none">  
							<div id="owl-demo1" class="owl-carousel"> 
                                  
                               @php
                                        $url_segment = \Request::segment(2);
                                     @endphp
								 
                                        @if(trim($url_segment) == '')
                                            <a href="{{URL::to('story_category/')}}" class="color-active nw2">All Categories</a> 
                                        @else
                                           <a href="{{URL::to('story_category/')}}" class="nw2">All Categories</a> 
                                        @endif
                                       
                                        @foreach($categories as $category)
                                                   
                    <div class="item">  
                                            @if(trim($category->category ) == trim($url_segment))
                                               <a href="{{ URL::to('story_category/')}}/{{$category->category }}" class="color-active nw2"> {{ $category->category  }}</a> 
                                            @else
                                                <a href="{{ URL::to('story_category/')}}/{{$category->category }}" class="nw2"> {{ $category->category  }}</a> 
                                            @endif
                                            
                                           </div>      
                                            
                                        @endforeach     
                          
                                  
                         
                        </div>
       
          </div>
                   </div>
       
          </div>
       
<div class="content-wrapper">
    <div class="container">
         
        
        <div class="row">
            <div class="col-lg-12 v-categories side-menu">

                     
                      
                <!-- Popular Channels -->
                <div class="content-block">  
                        
                        
                                  <div class="cb-content">  
                        
                        
                        <div class="row">
						
						<div class="col-md-2 col-sm-2 col-xs-4" id="no-mbl-dis-06">

                                <aside class="sidebar-menu">

                                    @php

                                        $url_segment = \Request::segment(2);

                                     @endphp

									<ul>

                                        @if(trim($url_segment) == '')

                                            <li><a href="{{URL::to('story_category/')}}" class="color-active">All Categories</a></li>

                                        @else

                                            <li><a href="{{URL::to('story_category/')}}">All Categories</a></li>

                                        @endif

                                       

                                        @foreach($categories as $category)

                                            @if(trim($category->category ) == trim($url_segment))

                                                <li><a href="{{ URL::to('story_category/')}}/{{$category->category }}" class="color-active"> {{ $category->category }}</a></li>

                                            @else

                                                <li><a href="{{ URL::to('story_category/')}}/{{$category->category }}"> {{ $category->category  }}</a></li>

                                            @endif

                                        @endforeach

                                    </ul>

                                    <div class="bg-add"></div>

                                </aside>

                            </div>
                  
                            <div class="col-md-10 col-sm-10 col-xs-12"  style="padding-left:15px; padding-top:0px">
                                <div class="row">
            <div class="col-lg-12" id="desk-pad"> 
                <!-- Featured Videos -->
                <div class="content-block">
                    <div class="cb-header">
                        <div class="row">
                            <div class="col-lg-10 col-sm-10 col-xs-8">
                               
                            </div>
                            <div class="col-lg-2 col-sm-2 col-xs-4">
                                <div class="btn-group pull-right bg-clean">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Sort by <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a onclick="storyVideoSortByRecent()"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                        <li><a onclick="storyVideoSortByTopTrending()"><i class="cv cvicon-cv-view-stats"></i>Trending</a></li>
                                        <li><a onclick="storyVideoSortByTopLiked()"><i class="fa fa-thumbs-up"></i> Relenanve </a></li>
                                        <li><a onclick="storyVideoSortByTopViewed()"><i class="fa fa-heart-o"></i> Viewed </a></li>
                                        <li><a onclick="storyVideoSortByTopRated()"><i class="cv cvicon-cv-watch-later"></i> Top Rated</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="cb-content videolist" id="home_page_video">

                        @if(!empty($stories))
                            <div id="story_card">
                                 @include('elements.story.cards')   
                            </div>
                        @endif
                        
                          
                       
                    </div>
                <!-- /Featured Videos -->
				 </div>
				
				 </div>
                            </div>
                        </div>
                    </div>

                    <div class="content-block head-div head-arrow mb-40">
                        <div class="head-arrow-icon">
                            <i class="cv cvicon-cv-next"></i>
                        </div>
                    </div>
                </div>
                <!-- /Popular Channels -->

            </div>
        </div>
    </div>
</div>

<div class="ajax-load text-center" style="display:none">
	<p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>
</div>

 
<script type="text/javascript">
	var page = 1;
	$(window).scroll(function() {
	    if($(window).scrollTop() + $(window).height() >= $(document).height()) {
	        page++;
	        loadMoreData(page);
	    }
	});


	function loadMoreData(page){
	  $.ajax(
	        {
	            url: '?page=' + page,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data.html == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
	            $('.ajax-load').hide();
	            $("#home_page_video").append(data.html);
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}
	
		
		// Start story Video Sorting
    function storyVideoSortByRecent(){
        var url = '{{ $url_segment }}';
        $.ajax({
            type: "GET",
            url: "{{ URL::to('story_video_sort_by_recent') }}",
            data: {'keyword':url},
            cache: false,
            success: function(response)
            {
                $('#story_card').empty().html(response);
            }
        });
    }
    function storyVideoSortByTopViewed(){
        var url = '{{ $url_segment }}';
        $.ajax({
            type: "GET",
            url: "{{ URL::to('story_video_sort_by_viewed') }}",
            data: {'keyword':url},
            cache: false,
            success: function(response)
            {
                $('#story_card').empty().html(response);
            }
        });
    }
    function storyVideoSortByTopLiked(){
        var url = '{{ $url_segment }}';
        $.ajax({
            type: "GET",
            url: "{{ URL::to('story_video_sort_by_liked') }}",
            data: {'keyword':url},
            cache: false,
            success: function(response)
            {
                $('#story_card').empty().html(response);
            }
        });
    }
    function storyVideoSortByTopTrending(){
        var url = '{{ $url_segment }}';
        $.ajax({
            type: "GET",
            url: "{{ URL::to('story_video_sort_by_trending') }}",
            data: {'keyword':url},
            cache: false,
            success: function(response)
            {
                $('#story_video_data').empty().html(response);
            }
        });
    }
    function storyVideoSortByTopRated(){
        var url = '{{ $url_segment }}';
        $.ajax({
            type: "GET",
            url: "{{ URL::to('story_video_sort_by_top_rated') }}",
            data: {'keyword':url},
            cache: false,
            success: function(response)
            {
                $('#story_video_data').empty().html(response);
            }
        });
    }

    // End story Video Sorting
</script>

<style>
    #owl-demo1 .item{ 
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }

 #owl-demo1 .item{ 
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }
    .customNavigation{
      text-align: center;
    }
    .customNavigation a{
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }
    </style>


<script>
    $(document).ready(function() {

      var owl = $("#owl-demo1");

      owl.owlCarousel({
  
      itemsTablet: [600,4], //2 items between 600 and 0;
      itemsMobile :[600,4] // itemsMobile disabled - inherit from itemsTablet option
      
      }); 
    });
    </script>

            <style>
 @media  only screen and (max-width: 767px) {
     #desk-pad {
    padding: 0 3px;
}

     .main-ban {height: 160px  !Important;}
#categories-shown-in-all{display:block !Important;    background: #fff; border-top: solid 1px #eceff0;padding: 11px 0 10px 0;border-bottom: solid 1px #eceff0}
#categories-shown-in-all .owl-theme .owl-controls{    display: none !important;}
#no-mbl-dis-06{display:none;}
#full-width-mobile{width:100% !important;padding: 0 !important;border: 0px !important;}
#owl-demo1 .owl-item { width: auto !important;padding: 0 3px;}
#owl-demo1 a.nw2{background: #28b47e !important;padding:8px 10px 6px 10px;display: block;font-size: 12px !important;color: #fff;border-radius: 100px;line-height: 16px;}
#owl-demo1  a.color-active.nw2{background:#f72e5e !important; color:#fff !important;}
.sticky {position: fixed;top: 50px;background:#fff;z-index:99;backgroundwidth: 100%;}
#search_stories{display:none;}
#bottom-menu a i{margin-bottom:8px;}
 #bottom-menu a img{margin-bottom:8px;}    
 #story_block  {  padding: 0 13px !important;}
#owl-demo1{display: block !important}
     #categories-shown-in-all { 
    height: 54px;
    overflow: hidden;
}
     
 }
                
</style>

@endsection
