@extends('layouts.home')


  @section('meta_tags')
   
    <meta name="description" content="Be updated with the stories from around the world.">
     <meta name="keywords" content="Stories, News, Blog, Cars & Vehicles,  Comedy ,  Education ,  Entertainment,  Film & Animation,  Gaming ,  Music,  News & Politics ,  Non-profits & Activism ,  People & Blogs ,  Pets & Animals,  Science & Technology,  Serials,  Sports ,  Travel & Events">
    <meta name="author" content="<?php echo ((isset($author) ? $author : 'Stories - Entertainment Bugs')); ?>">
    <meta property="og:site_name" content="{{URL::to('/')}}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="<?php echo ((isset($ogtitle) ? $ogtitle : 'Stories - Entertainment Bugs')); ?>"/>
    <meta property="og:description" content="<?php echo ((isset($ogdescription) ? $ogdescription : 'Be updated with the stories from around the world.')); ?>"/>
    <meta property="og:url" content="<?php echo url()->current(); ?>"/>
    <meta property="og:image" content="<?php echo ((isset($ogimage) ? $ogimage : 'https://www.entertainmentbugs.com/balu_development/public/uploads/logo/logo-social-media.png')); ?>"/><!--profile image-->
    <meta name="keywords" content="<?php echo ((isset($keywords) ? $keywords : 'Stories, News, Blog, Cars & Vehicles,  Comedy ,  Education ,  Entertainment,  Film & Animation,  Gaming ,  Music,  News & Politics ,  Non-profits & Activism ,  People & Blogs ,  Pets & Animals,  Science & Technology,  Serials,  Sports ,  Travel & Events')); ?>">
  
   @endsection

@section('content')

                  
<style>
.trending-rewards-tags {margin-top:10px;}
.trending-rewards-tags ul{list-style:none;text-align: left;margin: 0 0 0px 5px;padding: 0 12px 0 0;}
.trending-rewards-tags li{display:inline-block;text-align:left;}
.trending-rewards-tags li a{   background: #e5e5e5;border: 0px;color: #000;line-height: 27px;height: 25px;padding: 2px 10px 0 10px;margin-bottom: 1px;font-size: 12px;}
#reward_tags1 .owl-item {margin-right:5px;width: auto !important;}
.tag_head{margin-right: 5px;float: left;width: 11%; line-height: 30px; color:#fff;    height: 20px;}
.btn.btn-default {padding: 12px 12px 0 12px;} 
#reward_tags1 .owl-pagination {display:block !Important;}
#reward_tags1 .owl-page span{display:none; }
#reward_tags1 .owl-page.active{opacity:0.6;}
#reward_tags1 .owl-page{ }
#reward_tags1 .owl-page:nth-child(1) {position: absolute;left: -16px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow-gray.png) no-repeat;width: 13px;height: 24px;top: 0px;background-size: contain;}  
#reward_tags1 .owl-page:nth-child(2) {position: absolute;right: -4px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow-gray.png) no-repeat;width: 13px;height: 24px;top: 0px;background-size: contain;}
#reward_tags1 .owl-page:nth-child(1):hover {position: absolute;left: -16px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow.png) no-repeat;width: 13px;height: 25px;top: 0px;background-size: contain;}  
#reward_tags1 .owl-page:nth-child(2):hover {position: absolute;right: -4px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow.png) no-repeat;width: 13px;height: 25px;top: 0px;background-size: contain;}
#reward_tags1 .owl-page:before, #reward_tags1 .owl-page:after{display:none;}
</style>    





   <style>
table{display:none;}
#story_block h4{font-size: 13px;height: 31px;overflow: hidden;line-height: 16px;}
.story_img{position:absolute;right:5px;;bottom:40px;}
.media p strong{font-weight:normal;}
.list-inline li.active1 a{color:#f72e5e !important;}
#owl-demo5 .well1 {width: auto !important;background: #f4f3f3;}
#owl-demo5 .owl-item {float: left;width: 285px !important;}   
#owl-demo5 .owl-page span{opacity:0 !Important;}
#story_block ul.list-inline li a {font-size: 13px;}
.no-pad_str{padding:0px;}





@media  only screen and (max-width: 767px) {
#categories-shown-in-all { height: 54px;overflow: hidden}
#categories-shown-in-all{display:block !Important;background: #fff; border-top: solid 1px #eceff0;padding: 11px 0 10px 0;border-bottom: solid 1px #eceff0}
#categories-shown-in-all .owl-theme .owl-controls{display: none !important;}
#no-mbl-dis-06{display:none;}
#full-width-mobile{width:100% !important;padding: 0 !important;border: 0px !important;}
#owl-demo1 .owl-item { width: auto !important;padding: 0 3px;}
#owl-demo1 a.nw2{background: #28b47e !important;padding:8px 10px 6px 10px;display: block;font-size: 12px !important;color: #fff;border-radius: 100px;line-height: 16px;}
#owl-demo1  a.color-active.nw2{background:#f72e5e !important; color:#fff !important;}
.sticky {position: fixed;top: 50px;background:#fff;z-index:99;backgroundwidth: 100%;}
#search_stories{display:none;}
#bottom-menu a i{margin-bottom:8px;}
#bottom-menu a img{margin-bottom:8px;}
#story_block  {  padding: 0 0px !important;}
#owl-demo1{display: block !important}
}

</style>

<p style="display:none">Be updated with the stories from around the world.</p>

<div class="color_bg1" style="background:#f4f3f3">
   <div class="container" id="story_container">
        @if (Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif

         <div class="span12" id="categories-shown-in-all" style="display:none">
    		<div id="owl-demo1" class="owl-carousel">
                 @if(!empty($story_categories))
                    @php $tc = count($story_categories); $t = 0; @endphp
                    @foreach($story_categories as $cat)
                       <div class="item" >
                     <a class="nw2" href="{{ URL::to('story_category/')}}/{{$cat->category}}" target="">{{$cat->category}}</a>
                   </div>
                    @endforeach
                @endif
          </div>
        </div>


     
       <div class="col-lg-12 channels">
           <div class="content-block">
               <div class="cb-header mb-0">
                   <div class="row">
                       <div class="col-lg-8 col-sm-8 col-xs-12" id="no-mbl-dis-06">
                           <ul class="list-inline">
                               <li class=""><a class="color-active">  Catogies: </a></li>
                               @if(!empty($story_categories))
                                    <span>
                                    @php $tc = count($story_categories); $t = 0; @endphp
                                    @foreach($story_categories as $cat)
                                     <a href="{{ URL::to('story_category/')}}/{{$cat->category}}" target="">{{$cat->category}}</a>@php $t++; echo (($tc != $t)?',&nbsp;':""); @endphp
                                    @endforeach
                                    </span>
                                @endif
                           </ul>
                       </div>
                       @if($category)
                       <div class="col-lg-8 col-sm-8 col-xs-12">
                           <ul class="list-inline">
                               <li class=""><a class="color-active">  Search By: </a></li>
                               <li class=""><a href="#">{{ $category }}</a></li>
                           </ul>
                       </div>
                       @endif
                       <div class="col-lg-4 col-sm-4 col-xs-12" id="search_stories">
                           <div class="cb-search channels-search">
                                <form action="{{URL::to('searchstory/')}}" method="POST" role="search">
                                    {{ csrf_field() }}
                                    <label>
                                        <input type="search" name="searchStory" placeholder="Search Stories">
                                        <i class="fa fa-search"></i>
                                    </label>
                               </form>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>  
          </div>
       
       
       
<div class="container">
    <div class="trending-rewards-tags">
      <div class="span12" id="" style="width:100%">
        <ul id="reward_tags1" class="owl-carousel" style="display:block">
            @if(isset($tages) && count($tages)>0)
                 @foreach(array_slice($tages,0,30) as $tag)   
   
        <form action="{{URL::to('search')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" class="form-control" name="searchdata" aria-describedby="sizing-addon2" value="{{ $tag }}">
            <input type="hidden" name="searchType" id="searchType" value="story"/>
        
         <li> <input type="submit" value="{{ $tag }}"/> </li>
        </form>
    @endforeach
@endif 
           
      </ul>
    </div>
  </div>                             
</div>          
       
       
       
                     <div class="container no-pad_str" >
         
       
              <div id="story_block">
                  
                         <div class=" channels">
           <div class="content-block">
               <div class="cb-header mb-0" style="padding-left: 6px;padding-top:-0px">
                   <div class="row">
                       <div class="col-lg-8 col-sm-8 col-xs-10">
                           <ul class="list-inline">
                               <li class="hidden-xs"><a href="#" class="color-active">Latest Stories</a></li>
                           </ul>
                       </div>
                   </div>
               </div>
           </div>
       </div>
       
            @if (!empty($stories))
                @include('elements.story.cards')
            @endif
            @if(!empty($topOptionsStrory))
                <div id="story_card">
                    @include('elements.story.cards', ['stories' => $topOptionsStrory])
                </div>
                {{ $topOptionsStrory->links() }}
            @endif
           <div class="clear"></div>
       </div>
         </div>
         
       
<Style>
#owl-demo5 .owl-item {margin-right: 10px !important;}
#owl-demo2 img{width:100%}
.new1{padding:0 5px;}
#owl-demo2{margin:0 -5px 5px -5px;}
#owl-demo2 .owl-page span{display:none !important;}
      
 .owl-page:before,  .owl-page:after {
   border-right: 2px solid;
    content: '';
    display: block;
    height: 15px;
    margin-top: -11px;
    position: absolute;
    -moz-transform: rotate(135deg);
    -o-transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
    transform: rotate(135deg);
    right: 17px;
    top: 45%;
    width: 0;
}

 .owl-page:after {
    margin-top: -1px;
    -moz-transform: rotate(45deg);
    -o-transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
}

 .owl-page:hover,  .owl-page:focus,
 .owl-page:hover:before,  .owl-page:hover:after,
 .owl-page:focus:before,  .owl-page:focus:after {color: #000;}
.owl-page{width: 40px;height: 40px;top: 58PX;border-radius: 3%;background-color: #f72e5e !Important;color: #fff;border: 0px;font-size: 19px;line-height: 48px;text-align: center;padding: 0;position: absolute;left: -15px;margin: 0;}
.owl-page:nth-child(2){left:auto; right:-15px;}
.new1 p{background:#fff;padding:10px;    font-size: 12px;}
.channels .content-block .cb-header .list-inline {line-height: 25px;}

</Style> 
     
 @if (isset($followers) && count($followers)>6) 

     <div class="container" id="story_container">
       <div class="col-lg-12 channels">
           <div class="content-block">
               <div class="cb-header mb-0">
                   <div class="row">
                       <div class="col-lg-8 col-sm-8 col-xs-10">
                           <ul class="list-inline">
                               <li class="hidden-xs"><a href="#" class="color-active">Top Follwing</a></li>
                           </ul>
                       </div>
                   </div>
               </div>
           </div>
       </div>
     <div id="story_block"> 
                @include('elements.story.card2',['stories'=>$followers,'class'=>'owl-demo3','var'=>'one']) 
           <div class="clear"></div>
       </div>  
   </div>
    @endif
     @if(isset($mostRatedStory) && count($mostRatedStory)>6)
   <div class="container" id="story_container">
       <div class="col-lg-12 channels">
           <div class="content-block">
               <div class="cb-header mb-0">
                   <div class="row">
                       <div class="col-lg-8 col-sm-8 col-xs-10">
                           <ul class="list-inline">
                               <li class="hidden-xs"><a href="#" class="color-active">Most tranding Stories</a></li>
                           </ul>
                       </div>
                   </div>
               </div>
           </div>
       </div>
     <div id="story_block"> 
            @include('elements.story.card2', ['stories' => $mostRatedStory, 'class'=>'owl-demo2','var'=>'two'])  
           <div class="clear"></div>
       </div>  
   </div>
@endif
 @if(isset($mostViewedStory) && count($mostViewedStory)>6)

        
    <div style="background:#fff; margin-bottom:35px;">
         <div class="container" id="story_container"> 
           <!--history based start-->
            <div class="col-lg-12 channels">
            <div class="content-block">
                <div class="cb-header mb-0">
                    <div class="row">
                        <div class="col-lg-8 col-sm-8 col-xs-10">
                            <ul class="list-inline">
                                <li class="hidden-xs"><a href="#" class="color-active"> History Based Stories</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
            <div id="story_block"> 
                @include('elements.story.card2',['stories' => $mostViewedStory, 'class'=>'owl-demo5','var'=>'three']) 
            <div class="clear"></div>
        </div>
          <!--history based end--> 
           </div>  
      </div>  
      
      
         @endif
         
             <div class="container no-pad_str"> 
                 <div id="story_block">
                @if(!empty($topOptionsStrory))
                    @php
                        $stories = $topOptionsStrory;
                    @endphp
                    <div id="story_card">
                        @include('elements.story.cards')
                    </div>
    
                @endif
    
        <!--       <div class="adblock2" style="padding:0px 0 20px 0">-->
    				<!--{{HTML::image('public/img/advert-728x90px-large.jpg',null,['style'=>'margin:0 auto; display:block'])}}-->
        <!--       </div>-->
    
               <div class="clear"></div>
       </div> 
                 <div id="story_container">
           <div class="col-lg-12 channels">
               <div class="content-block">
                   <div class="cb-header mb-0">
                       <div class="row">
                           <div class="col-lg-8 col-sm-8 col-xs-10">
                               <ul class="list-inline">
                                   <li class="hidden-xs"><a href="#" class="color-active">Most Popular Stories</a></li>
                               </ul>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
           <div id="story_block">
    
            @if(!empty($mostViewedStory))
                @include('elements.story.cards', ['stories' => $mostViewedStory])
            @endif
    
               <!--<div class="adblock2" style="padding:0px 0 20px 0">-->
               <!--   {{HTML::image('public/img/advert-728x90px-large.jpg',null,['style'=>'margin:0 auto; display:block'])}}-->
               <!--</div>-->
               <div class="clear"></div>
           </div>
   </div> 
                 <div id="story_container">
            <div class="col-lg-12 channels">
                <div class="content-block">
                    <div class="cb-header mb-0">
                        <div class="row">
                            <div class="col-lg-8 col-sm-8 col-xs-10">
                                <ul class="list-inline">
                                    <li class="hidden-xs"><a href="#" class="color-active">Top Rated Stories</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="story_block">
    
                @if(!empty($mostRatedStory))
                    @php
                        $stories = $mostRatedStory;
                    @endphp
                    @include('elements.story.cards')
                @endif
    
                <!--<div class="adblock2" style="padding:0px 0 20px 0">-->
                <!--    {{HTML::image('public/img/advert-728x90px-large.jpg',null,['style'=>'margin:0 auto; display:block'])}}-->
                <!--</div>-->
                <div class="clear"></div>
            </div>
    </div> 
             </div>
         
         
         
         
       
    </div>  
</div>
</div>

 <script>
           $(document).ready(function() {

               var owl = $("#reward_tags1");

               owl.owlCarousel({

                   items: 16, //10 items above 1000px browser width
                   itemsDesktop: [1000, 6], //5 items between 1000px and 901px
                   itemsDesktopSmall: [900, 8], // 3 items betweem 900px and 601px
                   itemsTablet: [600, 7], //2 items between 600 and 0;
                   itemsMobile: [600, 7], // itemsMobile disabled - inherit from itemsTablet option
               });

           });
           </script>
           
<script type="text/javascript">
$(document).ready(function(){
    var page = 1;
	$(window).scroll(function() {
	    
	    if($(window).scrollTop() + $(window).height() >= $(document).height()) {
	        page++;
	        loadMoreData(page);
	    }
	});
});
	


	function loadMoreData(page){
	   
	  $.ajax(
	        {
	            url: '?page=' + page,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data.html == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
	            $('.ajax-load').hide();
	            $("#home_page_video").append(data.html);
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}

</script>
<style>
    #owl-demo1 .item{
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }

 #owl-demo1 .item{
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }
    .customNavigation{
      text-align: center;
    }
    .customNavigation a{
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }
    .owl-carousel .nav-btn{
  height: 47px;
  position: absolute;
  width: 26px;
  cursor: pointer;
  top: 100px !important;
}

.owl-carousel .owl-prev.disabled,
.owl-carousel .owl-next.disabled{
pointer-events: none;
opacity: 0.2;
}

.owl-carousel .prev-slide{
  background: url(nav-icon.png) no-repeat scroll 0 0;
  left: -33px;
}
.owl-carousel .next-slide{
  background: url(nav-icon.png) no-repeat scroll -24px 0px;
  right: -33px;
}
.owl-carousel .prev-slide:hover{
 background-position: 0px -53px;
}
.owl-carousel .next-slide:hover{
background-position: -24px -53px;
}  
    </style>

<script>
    $(document).ready(function() {

      var owl = $("#owl-demo1");

      owl.owlCarousel({

      itemsTablet: [600,4], //2 items between 600 and 0;
      itemsMobile :[600,4] // itemsMobile disabled - inherit from itemsTablet option

      });
    });
    </script>

@endsection