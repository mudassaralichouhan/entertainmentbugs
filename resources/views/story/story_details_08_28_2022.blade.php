@extends('layouts.home')
@section('content')
@if(!empty($story))	
    @php

        $coverBasePath = STORY_COVER_UPLOAD_PATH.$story->cover_image;
        $coverUrlPath = STORY_COVER_DISPLAY_PATH.$story->cover_image;

        $userBastPath = PROFILE_SMALL_UPLOAD_PATH.$story->user->photo;
        $userUrlPath = PROFILE_SMALL_DISPLAY_PATH.$story->user->photo;

        $created_at = strtotime($story->created_at);
        $curDatetime = time();
        $difference = $curDatetime - $created_at;

        $totalStories = count($stories);

        if(isset($user) && $user->photo != ''){
            $curuserBastPath = PROFILE_SMALL_UPLOAD_PATH.$user->photo;
            $curuserUrlPath = PROFILE_SMALL_DISPLAY_PATH.$user->photo;
            $userName = $user->name;
        }
    @endphp
<style>

.sv-views-progress {top: 18px;}
.single-video .author .sv-views {position: relative;}
#obj-img img{width: 50px;height: 50px;object-fit: cover;}
#obj-img1 img{width: 50px;height: 50px;object-fit: cover;}

.story-title{background:#fff; color:#000; font-size:24px;margin:0 0 0 5px}
.story-title h5{ font-family: Montserrat,sans-serif;
    font-size: 26px;
    font-weight: 500;
    padding: 17px 15px 14px 15px;
    margin: 0;
    line-height: 32px;
} 
</style>
 
 <div class="color_bg" style="background:#f4f3f3">
 

<div class="container" id="story_container"> 
 
 
  <div class="col-lg-12 channels">
	<div class="content-block">
	
	<div class="cb-header mb-0">
                        <div class="row">
                            <div class="col-lg-8 col-sm-8 col-xs-12">
                                <ul class="list-inline"> 
									<li class=""><a class="color-active">  Catogies: </a></li>
									
									 @if(!empty($story_categories))
                                        <span>
                                        @php $tc = count($story_categories); $t = 0; @endphp
                                        @foreach($story_categories as $category)
                                         <a href="{{ URL::to('category/')}}/{{$category->category}}" target="_blank">{{$category->category}}</a>@php $t++; echo (($tc != $t)?' &nbsp;':""); @endphp
                                        @endforeach
                                        </span>
                                    @endif
                                
                                </ul>
                            </div>
                            
                        </div>
						 
                    </div>
	     </div>
	   </div>
 
 <div class="clear"></div>
 

  <div  id="story_block_main" class="single-video row">
  
				    <div class="story_left" >
				        
					  <div class="story-title">  
							<h5>{{$story->title}}</h5>  
					 </div> 
					 
					 
				    <div class="well" style="margin-top:0px">		
                    @if(file_exists($coverBasePath))
                        {{HTML::image($coverUrlPath)}}
                    @else
                        {{HTML::image('public/img/stories.jpg',null,['style'=>'width:100%'])}}
                    @endif		
					
					  <div class="media"> 
						  <p class="story_details">{!! $story->about !!}</p> 
					 </div>  
				  </div> 

				  <style>
				#story_similar_block{clear:both; overflow:hidden}
				#story_similar_block .well{width:32.5% !Important;margin-bottom: 10px ; float:left !Important}
				#story_similar_block  .list-inline>li{font-size:11px; padding: 0 1px;}
				#story_similar_block  ul.list-inline li a {color: #ea2c5a !important;}
				#story_similar_block h4.media-heading{font-size:14px; color: #ea2c5a !important;}
			 .checked, .fa-star-half-o{
  color: orange;
}
.rating_main {clear:both; overflow:hidden}
.rating_main h4{float:left; margin:10px 20px 0 0}
.rtng1{margin-top:11px;}
.rtng2{margin-top:3px;}
				 </style>	
				     
								
				  	 
				  <div class="rating_main" style="background:#fff; padding:20px 20px 20px 10px; margin-bottom:10px;">
                            <div class="col-lg-10">
                                <h4>Rating</h4>
                                <span id="avgRating">@include('elements.story.rating')</span>
                            </div>
                            
                            @if (session()->has('user_id'))
                            <div class="col-lg-2" style="padding:0px; text-align:right">
                                @php
                                $rating = round($userStoryRating, 1);
                                @endphp
                                <div class="rtng2">
                                    @for ($i = 1; $i <= 5; $i++)
                                        @if ($i <= $rating)
                                            <span id="{{ $i }}" class="fa fa-star checked"></span>
                                        @elseif (($rating - $i) == -0.5)
                                            <span id="{{ $i }}" class="fa fa-star-half-o"></span>
                                        @else
                                            <span id="{{ $i }}" class="fa fa-star"></span>
                                        @endif
                                    @endfor
                                    <br>Rate this story {{ $userStoryRating }}
                                </div>
                            </div>
                            @endif
                        </div>
				  
				  
				   <div class="custom-tabs">
                        <div class="tabs-panel hidden-xs">
                            <a href="#" class="active" data-tab="tab-1">
                                <i class="cv cvicon-cv-about" data-toggle="tooltip" data-placement="top" title="About"></i>
                                <span>About</span>
                            </a>
                            <a href="#" data-tab="tab-2">
                                <i class="cv cvicon-cv-share" data-toggle="tooltip" data-placement="top" title="Share"></i>
                                <span>Share</span>
                            </a> 
                            @if(Session::has('user_id'))
                                <div class="acide-panel hidden-xs">
                                    <a href="{{$story->id}}" style="font-size:15px;line-height:25px;" id="likevideo"><i class="fa fa-thumbs-up" data-toggle="tooltip" data-placement="top" title="" style="margin-right: 5px;" data-original-title="Liked"></i> <span id="likedCount">{{ $storyLikeCount }}</span></a>
                                    <a href="{{$story->id}}" style="font-size:15px;line-height:25px;" id="dislikevideo"><i class="fa fa-thumbs-down" data-toggle="tooltip" data-placement="top" title="" style="margin-right: 5px;" data-original-title="Unlinked"></i><span id="unLikedCount">{{ $storyDisLikeCount }}</span></a>
                                </div>
                            @endif
                        </div>
                        
                        
                            <div class="acide-panel acide-panel-top">
                                <a href="#"><i class="cv cvicon-cv-liked" data-toggle="tooltip" data-placement="top" title="Liked"></i></a>
                                <a href="#"><i class="cv cvicon-cv-watch-later" data-toggle="tooltip" data-placement="top" title="Watch Later"></i></a> 
                                <a href="#"><i class="cv cvicon-cv-flag" data-toggle="tooltip" data-placement="top" title="Flag"></i></a> 
                            </div>
                       
				
				
				 <div class="tabs-panel only_mobile"> 
                            <a href="#" data-tab="tab-2">
                                <i class="cv cvicon-cv-share" data-toggle="tooltip" data-placement="top" title="Share"></i>
                                <span>Share</span>
                            </a> 
                        </div>
				
                        <div class="clearfix"></div>

                        <!-- BEGIN tabs-content -->
                        <div class="tabs-content" style="background:#fff">
                            <!-- BEGIN tab-1 -->
                            <div class="tab-1">
                                <div class="col-xs-12" style="width:100%">
                                 
								 <div class="author" style="box-shadow: none;border: 0px;padding: 20px 0 10px 0px;margin: 0;">
                    <div class="author-head">
                        	<a class="pull-left story_img" href="#" style="width:60px">
                            @if(file_exists($userBastPath))
                                <a class="pull-left story_img" href="#" id="obj-img">
                                    {{HTML::image($userUrlPath,null,['class'=>'media-object'])}}
                                </a>
                            @else
                                <a class="pull-left story_img" href="#" id="obj-img1">
                                {{HTML::image('http://placekitten.com/150/150',null,['class'=>'media-object'])}}
                                </a>
                            @endif
						</a> 
                        <div class="sv-name">
                            <div><a href="#">{{$story->user->name}}</a> . {{ $totalStories }} Stories</div>
                            <div class="c-sub hidden-xs">
                                <div class="c-f">
                                    Followers
                                </div>
                                <div class="c-s">
                                    {{ $followers }}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                     
                    </div>
                    <div class="author-border"></div>
                    <div class="sv-views">
                        <div class="sv-views-count">
                            {{ $story->total_views_count }} views
                        </div>
                        <div class="sv-views-progress">
                            <div class="sv-views-progress-bar"></div>
                        </div>
                         
                    </div>
                    <div class="clearfix"></div>
                </div>
				
				
				<div id="video_up_preview"> 
                        <!--<div class="col-lg-12" id="about_mbl">-->
                        <!--    <div class="form-group">-->
                        <!--        <label for="e2">About:</label>-->
                        <!--        <span>{{ $story->about }}</span>-->
                        <!--    </div>-->
                        <!--</div>-->
						
						 <div class="col-lg-12" id="about_mbl">
                            <div class="form-group">
                                <label for="e1">Story Tags:</label>
                                
                                <p class="sv-tags">
                                    @php
                                        $tags = explode(',', $story->tags);
                                    @endphp

                                    @foreach ($tags as $tag)
                                     <form action="{{URL::to('search')}}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" class="form-control" name="searchdata" aria-describedby="sizing-addon2" value="{{ $tag }}">
                                        <input type="hidden" name="searchType" id="searchType" value="story"/>
                                        <input type="submit" value="{{ $tag }}">
                                    </form>
                                    @endforeach
                                </p>
									
                            </div>
                        </div>
						
						 <div class="col-lg-12">
                            <div class="form-group">
                                <label for="e3">Language:</label> 
								<span>{{ $story->language }}</span>
                            </div>
                        </div> 
                        
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="e3">Category:</label>
                                @if(!empty($story_categories))
                                    <span>
                                    @php $tc = count($story_categories); $t = 0; @endphp
                                    @foreach($story_categories as $category)
                                     <a href="{{ URL::to('category/')}}/{{$category->category}}" target="_blank">{{$category->category}}</a>@php $t++; echo (($tc != $t)?',&nbsp;':""); @endphp
                                    @endforeach
                                    </span>
                                @endif
                            </div>
                        </div>
						
				 	    <div class="col-lg-12">
                            <div class="form-group">
                                <label for="e3">Release Date:</label> 
								<span>{{ round($difference / 86400) }} Days ago</span>
                            </div>
                        </div>  
				 
 
 
                </div> 
                                    
                                </div>
                              
                            
                            </div>
                            <!-- END tab-1 -->

                            <!-- BEGIN tab-2 -->
                            <div class="tab-2" style="padding:10px;">
                                <h4>Share:</h4>
                                <div class="social">
                                    <a href="https://www.facebook.com/sharer/sharer.php?u={{URL::to('story/display', str_replace(' ', '-', $story->title))}}" class="facebook">
                                    <i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="https://twitter.com/intent/tweet?text=Default+share+text&amp;url={{URL::to('story/display', str_replace(' ', '-', $story->title))}}" class="twitter">
                                    <i class="fa fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url={{URL::to('story/display', str_replace(' ', '-', $story->title))}}" class="tumblr">
                                    <i class="fa fa-linkedin"></i></a>
                                    <a target="_blank" href="https://wa.me/?text={{URL::to('story/display', str_replace(' ', '-', $story->title))}}" class="reddit">
                                    <i class="fa fa-whatsapp"></i></a>
                                </div>
                                <div class="row">
                                    <div class="col-md-9">
                                        <h4>Link:</h4>
                                        <label class="clipboard">
                                            <input type="text" name="#" class="share-link" value="https://entertainmentbugs.com./DwGgdfe-C6c" readonly>
                                            <div class="btn-copy" data-clipboard-target=".share-link">Copy</div>
                                        </label>
                                    </div>
                                 
                                </div>
                                <div class="tab-popup popup-share">
                                    <div class="tab-popup-head">
                                        <i class="cv cvicon-cv-share"></i>
                                        <span>Share this video</span>
                                        <a href="#" class="tab-popup-close"><i class="cv cvicon-cv-cancel"></i></a>
                                    </div>
                                    <div class="tab-popup-content">
                                        <h4>Copy Link:</h4>
                                        <label class="clipboard">
                                            <input type="text" name="#" class="share-link" value="https://entertainmentbugs.com./DwGgdfe-C6c" readonly>
                                            <div class="btn-copy" data-clipboard-target=".share-link">Copy</div>
                                        </label>
                                    </div>
                                    <div class="tab-popup-content">
                                        <div class="popup-share-social">
                                            <a href="#" class="facebook">
                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                                <span>Facebook</span>
                                            </a>
                                            <a href="#" class="twitter">
                                                <i class="fa fa fa-twitter" aria-hidden="true"></i>
                                                <span>Twitter</span>
                                            </a>
                                            <a href="#" class="google">
                                                <i class="fa fa-google-plus" aria-hidden="true"></i>
                                                <span>Google Plus</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END tab-2 -->

                       

                             
                            <!-- END tab-5 -->
                        </div>
                        <!-- END tabs-content -->
                    </div>

				   <div class="clear"></div>
				  
				  <div class="single-v-footer"  id="extrra_pad1">
                        

                        <!-- comments -->
                        <div class="comments">
                            <div class="reply-comment">
                                <div class="rc-header"><i class="cv cvicon-cv-comment"></i> <span class="semibold">{{ $story->total_comments_count }}</span> Comments</div>
                                <div class="rc-ava">
                                    @if(isset($userName))
                                        <a href="{{URL::to('userprofile/'.$userName)}}" target="_blank">

                                            @if(file_exists($curuserBastPath))
                                                    {{HTML::image($curuserUrlPath,null,['class'=>'media-object'])}}
                                            @else
                                                {{HTML::image('public/img/ava5.png')}}
                                            @endif 
                                        
                                        </a> 
                                    @else 
                                        {{HTML::image('public/img/ava5.png')}}
                                    @endif                       
                                </div>
                                <div class="rc-comment">
                                    <form method="post" action="{{ route('comment.add') }}" id="commentfrm">
                                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                       <textarea rows="3" name="comment_body" placeholder="Share what you think?"></textarea>
                                       <input type="hidden" name="story_id" value="{{ $story->id }}" />
                                       <input type="hidden" name="model" value="Story" />
                                       <button type="submit">
                                           <i class="cv cvicon-cv-add-comment"></i>
                                       </button>
                                    </form>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comments-list">

                                <div class="cl-header">
                                    <div class="c-nav">
                                        <ul class="list-inline">
                                            <li><a href="#" class="active">  <span class="hidden-xs">Comments</span></a></li> 
                                        </ul>
                                    </div>
                                </div>
                                <div id="comments">
                                @include('comment.story._comment_replies', ['comments' => $story->comments, 'video_id' => $story->id])                                                          
                                </div>
                            </div>
                        </div>
                        <!-- END comments -->
                    </div>
				  
				  

				   
     </div> 
	 
	 
	 
	       <div id="right_full" class="extra-bg">
			 <div class="caption" style="margin-top: 10px;padding-bottom: 5px;margin-bottom: 20px;">
                   <div class="left">
                       <a>Recomended Stories</a>
                   </div>
             
                   <div class="clearfix"></div>
               </div>
            @if($stories)
                @include('elements.story.cards')
            @endif
		    <div class="clear"></div>   
     </div> 
  
</div>

<Style>
#right_full .media p:nth-child(1){display:none;}
#right_full .media p:nth-child(3){display:none;}
#right_full .media p:nth-child(4){display:none;}
#right_full .media p:nth-child(5){display:none;}
#right_full .media p:nth-child(6){display:none;}
#right_full .media p:nth-child(7){display:none;}
#right_full .media p:nth-child(8){display:none;}
#right_full .media p:nth-child(8){display:none;}
#right_full .media p:nth-child(10){display:none;}
     #right_full .media {
    padding: 0px 8px 5px 12px;
    marg
</Style>
 					
                </div>

<script type="text/javascript">

    var video_key = $(this).attr('href');
    var userID = '<?php echo Session::get('user_id'); ?>';
    var videoID = '<?php echo $story->id; ?>';
    var APP_URL  = '<?php echo URL::to('/'); ?>';

    $('#likevideo').click(function(e){
        e.preventDefault();
        if(userID){
            var id = $(this).attr('href');
            $.ajax({
                type:'get',
                url:'liked-story/'+id,
                success:function(data) {
                    if(data.disLikeCount > 0)
                    {
                        $(".fa-thumbs-down").css('color','#f72e5e');
                    }
                    $("#likedCount").text(data.likeCount);
                    $("#unLikedCount").text(data.disLikeCount);
                    $(".fa-thumbs-up").css('color','#f72e5e');
                    $(".fa-thumbs-down").css('color','#637076');
                }
            });
        } else {  
            window.location.replace(APP_URL+'/login');
        }
    });

    $('#dislikevideo').click(function(e){
        e.preventDefault();
        if(userID){
            var id = $(this).attr('href');
            $.ajax({
                type:'get',
                url:'disliked-story/'+id,
                success:function(data) {
                    if(data.likeCount > 0)
                    {
                        $(".fa-thumbs-up").css('color','#f72e5e'); 
                    }
                    $("#likedCount").text(data.likeCount);
                    $("#unLikedCount").text(data.disLikeCount);
                    $(".fa-thumbs-down").css('color','#f72e5e');
                    $(".fa-thumbs-up").css('color','#637076');
                }
            });
        } else {  
            window.location.replace(APP_URL+'/login');
        }
    });

   

    if(userID){
        var isLikedByUser = '<?php echo $isLikedByUser = isset($isLikedByUser) ? $isLikedByUser : ''; ?>';
        var isDisLikedByUser = '<?php echo $isDisLikedByUser = isset($isDisLikedByUser) ? $isDisLikedByUser : ''; ?>';

        if(isLikedByUser > 0) {
            $(".fa-thumbs-up").css('color','#f72e5e');
        }

        if(isDisLikedByUser > 0) {
            $(".fa-thumbs-down").css('color','#f72e5e');
        }

    }

    $("#commentfrm").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                console.log(data);
                $('#comments').html(''); // show response from the php script.
                $('#comments').append(data.html);
            }
        });
        $(this)[0].reset();
    });

    function showMe(id){
        $(id).toggle();
    }
    
    $('.rtng2 span').hover(function() {

        var id = $(this).attr('id');
        var counter = 1;
        var salt = $(this).attr('class');

        $('.rtng2 span').each(function(i) {
            if (id >= counter) {
                $('span#' + counter).addClass("checked");
            } else {
                $('span#' + counter).removeClass("checked");
            }
            counter++;
        });

        $.ajax({
            type: 'get',
            url: 'rating/' + videoID + '/' + id,
            success: function(data) {
                $('#avgRating').html('');
                $('#avgRating').append(data.html);
            }
        });
    });

    
</script>
</div>
@endif
@endsection
