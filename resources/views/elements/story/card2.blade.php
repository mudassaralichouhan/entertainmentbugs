 <Style>
        #{{$class}} img{width:100%}
       .new1{padding:0 5px;}
       #{{$class}}{margin:0 -5px 5px -5px;}
        #{{$class}}.owl-page span{display:none !important;}
        .well1{width: auto !important;}
 .owl-page:before,  .owl-page:after {
   border-right: 2px solid;
    content: '';
    display: block;
    height: 15px;
    margin-top: -11px;
    position: absolute;
    -moz-transform: rotate(135deg);
    -o-transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
    transform: rotate(135deg);
    right: 17px;
    top: 45%;
    width: 0;
}

 .owl-page:after {
    margin-top: -1px;
    -moz-transform: rotate(45deg);
    -o-transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
}

 .owl-page:hover,  .owl-page:focus,
 .owl-page:hover:before,  .owl-page:hover:after,
 .owl-page:focus:before,  .owl-page:focus:after {
    color: #000;
}



     .owl-page{width: 40px;
    height: 40px;
    top: 58PX;
    border-radius: 3%;
    background-color: #f72e5e !Important;
    color: #fff;
    border: 0px;
    font-size: 19px;
    line-height: 48px;
    text-align: center;
    padding: 0;
    position: absolute;
    left: -15px;
    margin: 0;}
       .owl-page:nth-child(2){left:auto; right:-15px;}
     
       
       </Style>
     <div class="container">
           <div class="pro-teams gallery">    
             <div class="pro-team-slide-ind">  
              <div class="span12" id="crousal_wraps">
              <div id="{{$class}}" class="owl-carousel" style="display:block">
                   <style>
        .plus-details a {color:#fff;}
        .v-img {float: left; margin: -70px 0 0 0;} 
        .plus {display: block !important;}
    </style>

@if($stories && count($stories) > 0)
   
    @foreach ($stories as $key => $story)
      
        @if(!empty($story) && count($story->user->toArray())>0)
          @if( ($key+1)%8 == 0 )
            <div class="well1 no-mar1" data-ad-type="story" data-ad-style='listing' data-ad-block></div>
        @endif
            @php
                $created_at = strtotime($story->created_at);
                $curDatetime = time();
                $difference = $curDatetime - $created_at;
                $userBasePath = '';
                $userUrlPath = '';
                $thumbBasePath = '';
                $thumbUrlPath = '';
            @endphp
        
            @if(!empty($story->thumb_image))
                @php
                    $thumbBasePath =  STORY_THUMB_UPLOAD_PATH.$story->thumb_image;
                    $thumbUrlPath = get_story_thumb_url($story->thumb_image);
                @endphp
            @endif
            @if(!empty($story->user->photo))
                @php
                    $userBasePath = PROFILE_SMALL_UPLOAD_PATH.$story->user->photo;
                    $userUrlPath = PROFILE_SMALL_DISPLAY_PATH.$story->user->photo;
                @endphp
            @endif
            
            @if( !$story->isAd )
                <div class="well1 no-mar1">
            @endif
                <div class="main-ban">
                    @if(check_asset_existence($thumbUrlPath))
                        {{ HTML::image($thumbUrlPath) }}
                    @else
                        {{HTML::image('public/img/story_thumb.jpg',null,['style'=>'width:100%'])}}
                    @endif
                    <div class="stories-head">
                        <div class="v-img" id="">
                            @if (!empty($story->user) && $story->user->id == Session::get('user_id'))
                                <div class="plus-details">
                                    <ul>
                                        <li><a href="edit-story/{{$story->id}}" class="edt-016"><i class="cvicon-cv-watch-later" aria-hidden="true"></i> Edit</a></li>
                                        <li>
                                            <form action="{{ url('/story/delete/'.$story->id)}}" method="POST" id="deletestory">
                                                {{ csrf_field() }} 
                                                <button class="btn btn-danger btn-xs" type="submit">Delete</button>
                                            </form>	
                                        </li>
                                    </ul>
                                </div>
                            @endif  
                        </div>
                        <a class="pull-left story_img" href="{{URL::to('userprofile/'.@$story->user->slug)}}" target="_blank">
                            @if(file_exists($userBasePath))
                                {{ HTML::image($userUrlPath,null,['class'=>'media-object']) }}
                            @else
                                {{HTML::image('http://placekitten.com/150/150',null,['class'=>'media-object'])}}
                            @endif
                        </a>
                        <div class="media-body">
                            <div class="story_list_star">
                                @php
                                    $storyRating = $story->avgRating;
                                @endphp
                                @include('elements.story.rating')
                            </div> 
                            <a href="{{ to_story(@$story->slug) }}" style="display:block"> 
                            <h4 class="media-heading">{{$story->title}}</h4></a>
                        </div>
                    </div>
                </div>
                <a href="{{ user_url(@$story->user->slug) }}" target="_blank">
                    <h5 class="media-heading" style="margin:10px 0 -14px 13px;color:#000 !important; font-size:13px">{{@$story->user->name}}</h5></a>
                <div class="media">
                    <p>{!! $story->about !!}</p>
                    <ul class="list-inline list-unstyled">
                    <li>
                        <span class="v-percent"><span class="v-circle"></span> {{ \Carbon\Carbon::parse($story->created_at)->diffForHumans() }} </span>
                        </li>
                        <li>|</li>
                        <li><span> {{ $story->total_views_count }} Views </span></li>
                        <li>|</li>
                        <li><span> {{ $story->total_comments_count }} comments</span> </li>
                        <li>|</li>
                    <li>
                        <span><a href="{{ to_story(@$story->slug) }}"> Read More</a></span>
                    </li>
                    <li class="social_icons12">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ to_story(@$story->slug) }}" target="_blank"><span><i class="fa fa-facebook-square"></i></span></a>
                        <a href="https://twitter.com/intent/tweet?text=Default+share+text&amp;url={{ to_story(@$story->slug) }}" target="_blank"><span><i class="fa fa-twitter-square"></i></span></a>
                        <a href="https://wa.me/?text={{ to_story(@$story->slug) }}" target="_blank"><span><i class="fa fa-whatsapp"></i></span></a>
                    </li>
                    @if( isset($isLoggedInUser) && $isLoggedInUser && !$story->is_promoted && isset($type) && $type == 'profile' && ($key == 0 || $key == 2) )
                        <li style="display: block">
                            <a href="{{ url("promote-post/story/".@$story->slug) }}" style="width:100%; padding: 5px; 10px; text-align:center;background-color: #f72e5e;color:white; margin-top: 10px; display: inline-block; text-decoration:none">Promote your post</a>
                        </li>
                    @endif
                </ul>
            </div>
            @if( $story->isAd )
                <div>
                    <button type='button' data-ad-button='{{ $story->button_url }}'>{{ $story->button_text }}</button>
                </div>
            @else
                </div>
            @endif
        @endif
    @endforeach
@endif
     </div>
                    </div> 
                 </div> 
              </div>
            </div>
            
   <script>
    $(document).ready(function(){
        @if($class)
        
        var {{$var}} = $('#{{$class}}');
     {{$var}}.owlCarousel({
    
      itemsTablet: [600,4], //2 items between 600 and 0;
      itemsMobile :[600,2] // itemsMobile disabled - inherit from itemsTablet option

      });
      
      {{$var}}.next()   // Go to next slide
{{$var}}.prev()  

@endif
    });
</script>         