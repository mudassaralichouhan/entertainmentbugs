@if($stories && count($stories) > 0)
    <style>
        .plus-details a {color:#fff;}
        .v-img {float: left; margin: -70px 0 0 0;} 
        .plus {display: block !important;}
    </style>
    @foreach ($stories as $key => $story)
        @if( ($key+1)%8 == 0 )
            <div class="well no-mar1" data-ad-type="story" data-ad-style='listing' data-ad-block></div>
        @endif
        @if(!empty($story) && count($story->user->toArray())>0)
            @php
                $created_at = strtotime($story->created_at);
                $curDatetime = time();
                $difference = $curDatetime - $created_at;
                $userBasePath = '';
                $userUrlPath = '';
                $thumbBasePath = '';
                $thumbUrlPath = '';
            @endphp
        
            @if(!empty($story->thumb_image))
                @php
                    $thumbBasePath =  STORY_THUMB_UPLOAD_PATH.$story->thumb_image;
                    $thumbUrlPath = get_story_thumb_url($story->thumb_image);
                @endphp
            @endif
            @if(!empty($story->user->photo))
                @php
                    $userBasePath = PROFILE_SMALL_UPLOAD_PATH.$story->user->photo;
                    $userUrlPath = PROFILE_SMALL_DISPLAY_PATH.$story->user->photo;
                @endphp
            @endif
            
            @if( !$story->isAd )
                <div class="well no-mar1">
            @endif
                <div class="main-ban">
                    @if(check_asset_existence($thumbUrlPath))
                        {{ HTML::image($thumbUrlPath) }}
                    @else
                        {{HTML::image('public/img/story_thumb.jpg',null,['style'=>'width:100%'])}}
                    @endif
                    <div class="stories-head">
                        <div class="v-img" id="">
                            @if (!empty($story->user) && $story->user->id == Session::get('user_id'))
                                <div class="plus-details">
                                    <ul>
                                        <li><a href="edit-story/{{$story->id}}" class="edt-016"><i class="cvicon-cv-watch-later" aria-hidden="true"></i> Edit</a></li>
                                        <li>
                                            <form action="{{ url('/story/delete/'.$story->id)}}" method="POST" id="deletestory">
                                                {{ csrf_field() }} 
                                                <button class="btn btn-danger btn-xs" type="submit">Delete</button>
                                            </form>	
                                        </li>
                                    </ul>
                                </div>
                            @endif  
                        </div>
                        <a class="pull-left story_img" href="{{URL::to('userprofile/'.@$story->user->slug)}}" target="_blank">
                            @if(file_exists($userBasePath))
                                {{ HTML::image($userUrlPath,null,['class'=>'media-object']) }}
                            @else
                                {{HTML::image('http://placekitten.com/150/150',null,['class'=>'media-object'])}}
                            @endif
                        </a>
                        <div class="media-body">
                            <div class="story_list_star">
                                @php
                                    $storyRating = $story->avgRating;
                                @endphp
                                @include('elements.story.rating')
                            </div> 
                            
                            <a href="{{ to_story(@$story->slug) }}" style="display:block"> 
                            <h4 class="media-heading">{{$story->title}}</h4></a>
                        </div>
                    </div>
                </div>
                <a href="{{ user_url(@$story->user->slug) }}" target="_blank">
                    <h5 class="media-heading" style="margin:10px 0 -14px 13px;color:#000 !important; font-size:13px">{{@$story->user->name}}</h5></a>
                <div class="media">
                    <p>{!! $story->about !!}</p>
                    <ul class="list-inline list-unstyled">
                    <li>
                        <span class="v-percent"><span class="v-circle"></span> {{ \Carbon\Carbon::parse($story->created_at)->diffForHumans() }} </span>
                        </li>
                        <li>|</li>
                        <li><span> {{ $story->total_views_count }} Views </span></li>
                        <li>|</li>
                        <li><span> {{ $story->total_comments_count }} comments</span> </li>
                        <li>|</li>
                    <li>
                        <span><a href="{{ to_story(@$story->slug) }}"> Read More</a></span>
                    </li>
                    <li class="social_icons12">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ to_story(@$story->slug) }}" target="_blank"><span><i class="fa fa-facebook-square"></i></span></a>
                        <a href="https://twitter.com/intent/tweet?text=Default+share+text&amp;url={{ to_story(@$story->slug) }}" target="_blank"><span><i class="fa fa-twitter-square"></i></span></a>
                        <a href="https://wa.me/?text={{ to_story(@$story->slug) }}" target="_blank"><span><i class="fa fa-whatsapp"></i></span></a>
                    </li>
                    @if( isset($isLoggedInUser) && $isLoggedInUser && !$story->is_promoted && isset($type) && $type == 'profile' && ($key == 0 || $key == 2) )
                        <li style="display: block">
                            <a href="{{ url("promote-post/story/".@$story->slug) }}" style="width:100%; padding: 5px; 10px; text-align:center;background-color: #f72e5e;color:white; margin-top: 10px; display: inline-block; text-decoration:none">Promote your post</a>
                        </li>
                    @endif
                </ul>
            </div>
            @if( $story->isAd )
                <div>
                    <button type='button' data-ad-button='{{ $story->button_url }}'>{{ $story->button_text }}</button>
                </div>
            @else
                </div>
            @endif
        @endif
    @endforeach
@endif