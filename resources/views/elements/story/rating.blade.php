@php
    $rating = round($storyRating, 1);
@endphp
<div class="rtng1">
    @for ($i = 1; $i <= 5; $i++)
        @if ($i <= $rating)
            <span class="fa fa-star checked"></span>
        @elseif ($rating - $i == -0.5)
            <span class="fa fa-star-half-o"></span>
        @else
            <span class="fa fa-star" style="color: black"></span>
        @endif
    @endfor
    {{ $rating }}
</div>
