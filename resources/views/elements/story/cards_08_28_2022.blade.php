@foreach ($stories as $story)
    @if(!empty($story))	
    
            @php
                $created_at = strtotime($story->created_at);
                $curDatetime = time();
                $difference = $curDatetime - $created_at;
                $userBasePath = '';
                $userUrlPath = '';
            @endphp
        
            @if(!empty($story->thumb_image))
                @php
                    $thumbBasePath = STORY_THUMB_UPLOAD_PATH.$story->thumb_image;
                    $thumbUrlPath = STORY_THUMB_DISPLAY_PATH.$story->thumb_image;
                @endphp
            @endif
            @if(!empty($story->user->photo))
                @php
                    $userBasePath = PROFILE_SMALL_UPLOAD_PATH.$story->user->photo;
                    $userUrlPath = PROFILE_SMALL_DISPLAY_PATH.$story->user->photo;
                @endphp
            @endif
            
        <div class="well no-mar1">
            <div class="main-ban">
           
			
                @if(!empty($thumbBasePath))
                    {{HTML::image($thumbUrlPath)}}
                @else
                    {{HTML::image('public/img/story_thumb.jpg',null,['style'=>'width:100%'])}}
                @endif
                <div class="stories-head">
                    <div class="v-img" id="">
                     
                  <!--<div class="plus"><i class="cvicon-cv-plus" aria-hidden="true"></i></div>-->
			<div class="plus-details">
				<ul>
					<li><a href="edit-story/{{$story->id}}" class="edt-016"><i class="cvicon-cv-watch-later" aria-hidden="true"></i> Edit</a></li>
					<li>
							<form action="{{ url('/story/delete/'.$story->id)}}" method="POST" id="deletestory">
								{{ csrf_field() }} 
								<button class="btn btn-danger btn-xs" type="submit">Delete</button>
							</form>	
					</li>
				</ul>
			</div>
			</div>
                    <a class="pull-left story_img" href="{{URL::to('userprofile/'.$story->user->name)}}" target="_blank">
                        @if(file_exists($userBasePath))
                                {{HTML::image($userUrlPath,null,['class'=>'media-object'])}}
                        @else
                            {{HTML::image('http://placekitten.com/150/150',null,['class'=>'media-object'])}}
                        @endif
                    </a>
                    
                    <div class="media-body">
                        <div class="story_list_star">
                            @php
                                $storyRating = $story->avgRating;
                            @endphp
                            @include('elements.story.rating')
                        </div> 
                        <a href="{{URL::to('story/display', str_replace(' ', '-', $story->title))}}" style="display:block"> 
                        <h4 class="media-heading">{{$story->title}}</h4></a>
                     

                       
                    </div>
                </div>
            </div>   
            <a href="{{URL::to('userprofile/'.$story->user->name)}}" target="_blank">                               
                        <h5 class="media-heading" style="margin:10px 0 -14px 13px;color:#000 !important; font-size:13px">{{$story->user->name}}</h5></a>
            <div class="media">
                <p>{!! $story->about !!}</p>
                <ul class="list-inline list-unstyled">
                <li><span> {{ \Carbon\Carbon::parse($story->created_at)->diffForHumans() }} </span></li>
                    <li>|</li>
                    <li><span> {{ $story->total_views_count }} Views </span></li>
                    <li>|</li>
                    <li><span> {{ $story->total_comments_count }} comments</span> </li>
                    <li>|</li>
                <li><span><a href="{{URL::to('story/display', str_replace(' ', '-', $story->title))}}"> Read More</a>
                    
                </span> </li>
                    <li class="social_icons12">
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{URL::to('story/display', str_replace(' ', '-', $story->title))}}" target="_blank"><span><i class="fa fa-facebook-square"></i></span></a>
                        <a href="https://twitter.com/intent/tweet?text=Default+share+text&amp;url={{URL::to('story/display', str_replace(' ', '-', $story->title))}}" target="_blank"><span><i class="fa fa-twitter-square"></i></span></a>
                        <a href="https://wa.me/?text={{URL::to('story/display', str_replace(' ', '-', $story->title))}}" target="_blank"><span><i class="fa fa-whatsapp"></i></span></a>
                    </li>
                </ul>
            </div>
        </div>
        <style>
        
        .plus-details a {color:#fff;}
           .v-img {float: left; margin: -70px 0 0 0;} 
            .plus {display: block !important;}
            
        </style>
        
    @endif
@endforeach