{{ HTML::script('public/js/facebox.js')}}
{{ HTML::style('public/css/facebox.css')}}
<script type="text/javascript">
    $(document).ready(function ($) {
        $('.close_image').hide();
        $('a[rel*=facebox]').facebox({
            closeImage: '{!! HTTP_PATH !!}/public/img/close.png'
        });
    });
</script>
<div class="admin_loader" id="loaderID">{{HTML::image("public/img/website_load.svg", SITE_TITLE)}}</div>
@if(!$allrecrds->isEmpty())
    <div class="panel-body marginzero">
        <div class="ersu_message">@include('elements.admin.errorSuccessMessage')</div>
        {{ Form::open(array('method' => 'post', 'id' => 'actionFrom')) }}
            <section id="no-more-tables" class="lstng-section">
                <div class="topn">
                    <div class="topn_left">Users List</div>
                    <div class="topn_rightd ddpagingshorting" id="pagingLinks" align="right">
                        <div class="panel-heading" style="align-items:center;">
                            {{$allrecrds->appends(Input::except('_token'))->render()}}
                        </div>
                    </div>                
                </div>
                <div class="tbl-resp-listing">
                <table class="table table-bordered table-striped table-condensed cf">
                    <thead class="cf ddpagingshorting">
                        <tr>
                            <th style="width:5%">#</th>
                            <th class="sorting_paging">@sortablelink('first_name', 'First Name')</th>
                            <th class="sorting_paging">@sortablelink('email_address', 'Email Address')</th>
                            <th class="sorting_paging">@sortablelink('contact', 'Contact Number')</th>
                            <th class="sorting_paging">@sortablelink('created_at', 'Date')</th>
                            <th class="action_dvv"> Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($allrecrds as $allrecrd)
                            <tr>
                                <th style="width:5%"><input type="checkbox" onclick="javascript:isAllSelect(this.form);" name="chkRecordId[]" value="{{$allrecrd->id}}" /></th>
                                <td data-title="Full Name">{{$allrecrd->first_name}}</td>
                                <td data-title="Email Address">{{$allrecrd->email_address}}</td>
                                <td data-title="Contact Number">{{$allrecrd->contact}}</td>
                                <td data-title="Date">{{$allrecrd->created_at->format('M d, Y')}}</td>
                                <td data-title="Action">
                                    <div id="loderstatus{{$allrecrd->id}}" class="right_action_lo">{{HTML::image("public/img/loading.gif", SITE_TITLE)}}</div>
                                    <span class="right_acdc" id="status{{$allrecrd->id}}">
                                        @if($allrecrd->status == '1')
                                            <a href="{{ URL::to( 'admin/users/deactivate/'.$allrecrd->slug)}}" title="Deactivate" class="deactivate"><button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button></a>
                                        @else
                                            <a href="{{ URL::to( 'admin/users/activate/'.$allrecrd->slug)}}" title="Activate" class="activate"><button class="btn btn-danger btn-xs"><i class="fa fa-ban"></i></button></a>
                                        @endif
                                    </span>
                                    <a href="{{ URL::to( 'admin/users/edit/'.$allrecrd->slug)}}" title="Edit" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                                    <a href="{{ URL::to( 'admin/users/delete/'.$allrecrd->slug)}}" title="Delete" class="btn btn-danger btn-xs action-list delete-list" onclick="return confirm('Are you sure you want to delete this record?')"><i class="fa fa-trash-o"></i></a>
                                    <a href="#info{!! $allrecrd->id !!}" title="View" class="btn btn-primary btn-xs" rel='facebox'><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="search_frm">
                    <button type="button" name="chkRecordId" onclick="checkAll(true);"  class="btn btn-info">Select All</button>
                    <button type="button" name="chkRecordId" onclick="checkAll(false);" class="btn btn-info">Unselect All</button>
                    <?php global $accountStatus;?>
                    <div class="list_sel">{{Form::select('action', $accountStatus,null, ['class' => 'small form-control','placeholder' => 'Action for selected record', 'id' => 'action'])}}</div>
                <button type="submit" class="small btn btn-success btn-cons btn-info" onclick="return ajaxActionFunction();" id="submit_action">OK</button>
                </div>    
            </div>
        </section>
        {{ Form::close()}}
        </div>         
    </div>
@else 
    <div id="listingJS" style="display: none;" class="alert alert-success alert-block fade in"></div>
    <div class="admin_no_record">No record found.</div>
@endif

@if(!$allrecrds->isEmpty())
    @foreach($allrecrds as $allrecrd)
        <div id="info{!! $allrecrd->id !!}" style="display: none;">
            <div class="nzwh-wrapper">
                <fieldset class="nzwh">
                    <legend class="head_pop">{!! $allrecrd->first_name.' '.$allrecrd->last_name !!}</legend>
                    <div class="drt">
                    <div class="admin_pop"><span>Name: </span>  <label>{!! $allrecrd->first_name.' '.$allrecrd->last_name !!}</label></div>
                    <div class="admin_pop"><span>Email Address: </span>  <label>{!! $allrecrd->email_address !!}</label></div>
                    <div class="admin_pop"><span>Gender: </span>  <label>{!! $allrecrd->gender !!}</label></div>
                    <div class="admin_pop"><span>Contact Number: </span>  <label>{!! $allrecrd->contact !!}</label></div>
                    <div class="admin_pop"><span>Country Name: </span>  <label>@isset($allrecrd->Country->name) {{$allrecrd->Country->name}} @endisset</label></div>
                    <div class="admin_pop"><span>Address: </span>  <label>{!! nl2br($allrecrd->address) !!}</label></div>
                    @if($allrecrd->profile_image != '')
                        <div class="admin_pop"><span></span> <label>{{HTML::image(PROFILE_SMALL_DISPLAY_PATH.$allrecrd->profile_image, SITE_TITLE,['style'=>"max-width: 200px"])}}</label></div>
                    @endif
                </fieldset>
            </div>
        </div>
    @endforeach
@endif