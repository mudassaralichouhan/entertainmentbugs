<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="treeview @if(isset($actdashboard)){{'active'}}@endif">
                <a href="{{HTTP_PATH}}/admin/admins/dashboard">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview @if(isset($actchangeusername) || isset($actchangepassword) || isset($actchangeemail) || isset($actsitesetting)){{'active'}}@endif">
                <a href="javascript:void(0)">
                    <i class="fa fa-gears"></i> <span>Configuration</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(isset($actchangeusername)){{'active'}}@endif"><a href="{{HTTP_PATH}}/admin/admins/change-username"><i class="fa fa-circle-o"></i> Change Username</a></li>
                    <li class="@if(isset($actchangepassword)){{'active'}}@endif"><a href="{{HTTP_PATH}}/admin/admins/change-password"><i class="fa fa-circle-o"></i> Change Password</a></li>
                    <li class="@if(isset($actchangeemail)){{'active'}}@endif"><a href="{{HTTP_PATH}}/admin/admins/change-email"><i class="fa fa-circle-o"></i> Change Email</a></li>
                    <li class="@if(isset($actsitesetting)){{'active'}}@endif"><a href="{{HTTP_PATH}}/admin/admins/site-settings"><i class="fa fa-circle-o"></i> Site Settings</a></li>
                </ul>
            </li>   
            <li class="treeview @if(isset($actrest)){{'active'}}@endif">
                <a href="javascript:void(0)">
                    <i class="fa fa-user"></i> <span>Manage Restaurants</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(isset($actrest)){{'active'}}@endif"><a href="{{HTTP_PATH}}/admin/restaurants"><i class="fa fa-circle-o"></i>Restaurants List</a></li>
                </ul>
            </li>
            <li class="treeview @if(isset($actusers)){{'active'}}@endif">
                <a href="javascript:void(0)">
                    <i class="fa fa-users"></i> <span>Manage Users</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(isset($actusers)){{'active'}}@endif"><a href="{{HTTP_PATH}}/admin/users"><i class="fa fa-circle-o"></i>Users List</a></li>
                </ul>
            </li>  
            <li class="treeview @if(isset($actreviews)){{'active'}}@endif">
                <a href="javascript:void(0)">
                    <i class="fa fa-comments"></i> <span>Manage Reviews/Ratings</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(isset($actreviews)){{'active'}}@endif"><a href="{{HTTP_PATH}}/admin/reviews"><i class="fa fa-circle-o"></i>Reviews/Ratings List</a></li>
                </ul>
            </li>
            <li class="treeview @if(isset($actcountries)){{'active'}}@endif">
                <a href="javascript:void(0)">
                    <i class="fa fa-globe"></i> <span>Manage Countries</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(isset($actcountries)){{'active'}}@endif"><a href="{{HTTP_PATH}}/admin/countries"><i class="fa fa-circle-o"></i>Countries List</a></li>
                </ul>
            </li>

            <li class="treeview @if(isset($actpages)){{'active'}}@endif">
                <a href="javascript:void(0)">
                    <i class="fa fa-file-text-o"></i> <span>Manage Pages</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="@if(isset($actpages)){{'active'}}@endif"><a href="{{HTTP_PATH}}/admin/pages"><i class="fa fa-circle-o"></i>Page List</a></li>
                </ul>
            </li>            

        </ul>
    </section>
</aside>