@if( $notifications )
    @php $now = \Carbon\Carbon::now(); @endphp
    @foreach ($notifications as $notice)
        @php
            $user = null;
            $message = null;
            $link = get_notification_link($notice['data']);
            if( !$notice['read_at'] ){
                $link .= "?read=".$notice['id'];
            }
        @endphp
        @if($notice['data']['type'] == 'following-post')
            @php
                $user = \App\Models\User::find($notice['data']['following_id']);
                $message = $user->name.", ".$notice['data']['message'];
            @endphp
        @elseif($notice['data']['type'] == 'follows')
            @if( isset($notice['data']['user_id']) )
                @php
                    $user = \App\Models\User::find($notice['data']['user_id']);
                @endphp
            @elseif( isset($notice['data']['follower_id']) )
                @php
                    $user = \App\Models\User::find($notice['data']['follower_id']);
                @endphp
            @endif
            @if(trim($notice['data']['message']) == 'followed successfully')
                @php
                    $message = "You followed ".($user ? $user->name : "");
                @endphp
            @else
                @if( !$user )
                    @php $user = \App\Models\User::find(session()->get('user_id')); @endphp
                @endif
                @php
                    $message = ($user ? $user->name : "")." started following you";
                @endphp
            @endif
        @elseif($notice['data']['type'] == 'photo-uploaded' || $notice['data']['type'] == 'video-uploaded' || $notice['data']['type'] == 'story-uploaded')
            @if( isset($notice['data']['user_id']) )
                @php
                    $user = \App\Models\User::find($notice['data']['user_id']);
                @endphp
            @endif
            @php
                $type = str_replace("-uploaded", "", $notice['data']['type']);
            @endphp
            @if( isset($user) && $user )
                @if( $user->id != session()->get('user_id') )
                    @php $message = "$user->name uploaded $type."; @endphp
                @else 
                    @php $message = "$user->name, Your $type uploaded successfully."; @endphp
                @endif
            @endif
        @else
            @php $message = (isset($notice['data']['user_name']) ? $notice['data']['user_name'] : '')." ".$notice['data']['message']; @endphp
        @endif

        @if( !$user )
            @if( isset($notice['data']['user_id']) )
                @php $user = \App\Models\User::find($notice['data']['user_id']); @endphp
            @else
                @php
                    $user = new stdClass();
                    $user->slug = "#";
                    $user->photo = "";
                    $user->name = "";
                @endphp
            @endif
        @endif
        <div class="row" class="br_all">
            <div class="col-lg-2" id="new-profile">
                <div class="c-avatar">
                    <a href="{{ $link }}">
                        {!! get_avatar($user->photo, $user->name) !!}
                    </a>
                </div>
            </div>
            <div class="col-lg-10  col-xs-10" id="new-profile-details">
                <div class="channel-details">
                    <div class="row">
                        <a href="{{ $link }}" style="display:block;">
                            <h5>{!! $message." ".get_notification_icon($notice['data']['type']) !!}</h5>
                            @if($notice['data']['type'] == 'story-rating')
                                <div class="rtng1" style="display:inline-block">
                                    @if(array_key_exists('rating', $notice['data']))
                                        @for($i = 1; $i <= 5; $i++)
                                            @if($i <= $notice['data']['rating'])
                                                <span class="fa fa-star checked"></span>
                                            @else
                                                <span class="fa fa-star"></span>
                                            @endif
                                        @endfor
                                    @endif
                                </div>
                            @endif
                            @if($notice['data']['type'] == 'video-comment' || $notice['data']['type'] == 'story-comment' || $notice['data']['type'] == 'photo-comment')
                                {{ $notice['data']['comment'] }}
                            @elseif($notice['data']['type'] == 'apply-for-audition' || $notice['data']['type'] == 'applied-for-audition')
                                @php
                                    $id = isset($notice['data']['action_id']) ? $notice['data']['action_id'] : "";
                                    if( $id ){
                                        $audition = App\Models\PostAudtion::find($id);
                                        if( $audition ){
                                            echo $audition->title;
                                        }
                                    }
                                @endphp
                            @endif
                            <p>{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $notice['created_at'])->diffForHumans($now) }}</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif