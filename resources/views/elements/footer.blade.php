<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="container padding-def">
                <div class="col-lg-1  col-sm-2 col-xs-12 footer-logo">
                    <!--<a class="navbar-brand" href="index.html"><img src="images/logo.svg" alt="Project name" class="logo" /></a>-->
                    <a class="navbar-brand" href="https://www.entertainmentbugs.com/terms-and-conditions">
                        {{HTML::image('public/img/logo_footer.png', SITE_TITLE, ['class' => 'footer_logo'])}}
                    </a>
                </div>
                <div class="col-lg-7 col-sm-6 col-xs-12">
                    <div class="f-links">
                        <ul class="list-inline">
                            <li class="about_highlight"><a href="{{URL::to('about')}}">About</a></li>
                            <li class="faq_highlight"><a href="{{URL::to('faq')}}">FAQ</a></li>
                            <li class="hidden-xs contact_hig"><a href="{{URL::to('contact')}}">Contact</a></li>
                            <li class="hidden-xs complain-request"><a href="{{URL::to('complain-request')}}">Complain
                                    Request</a></li>
                        </ul>
                    </div>
                    <div class="delimiter"></div>
                </div>
                <div class="col-lg-7 col-sm-6 col-xs-12">
                    <div class="f-copy">
                        <ul class="list-inline">
                            <li><a href="{{URL::to('advertise-with-us')}}">Advertise with us</a></li>
                            <li><a href="{{URL::to('terms-and-conditions')}}">Terms and Conditions</a></li>
                            <li><a href="{{URL::to('privacy')}}">Privacy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-offset-1 col-lg-3 col-sm-4 col-xs-12">
                    <div class="f-last-line">

                        <div class="f-lang pull-right">
                            <div class="btn-group dropup pull-right">
                                <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Language
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" id="language_chs">
                                    <li><a href="#">English</a></li>
                                    <li><a href="#">Hindi</a></li>
                                    <li><a href="#">Tamil</a></li>
                                    <li><a href="#">Spanish</a></li>
                                    <li><a href="#">Gujrati</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="delimiter visible-xs"></div>
                </div>
            </div>
            <div class="f-icon pull-left" style="float:none;width:100%; text-align:center; padding: 23px 0 8px 0; border-top: solid 1px #eceff0;">
                <a href="#"><i class="fa fa-facebook-square"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-google-plus"></i></a>
            </div>
            <p class="e-developed_technology" style="text-align:center; ">
                <?= date('Y') ?> Entertainment Bugs All Right Reserved
                <br>
                Designed by
                <a target="_blank" href="http://e-developedtechnology.com">
                    <span> E-developed Technology </span>
                </a>
            </p>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            var owl = $("#photo_tag_slide");
            owl.owlCarousel({
                items: 6, //10 items above 1000px browser width
                itemsDesktop: [1000, 5], //5 items between 1000px and 901px
                itemsDesktopSmall: [900, 5], // 3 items betweem 900px and 601px
                itemsTablet: [600, 5], //2 items between 600 and 0;
                itemsMobile: [600, 2], // itemsMobile disabled - inherit from itemsTablet option
            });

        });
    </script>

    <script>
        $(document).ready(function () {

            var owl = $("#reward_tags");

            owl.owlCarousel({

                items: 15, //10 items above 1000px browser width
                itemsDesktop: [1000, 6], //5 items between 1000px and 901px
                itemsDesktopSmall: [900, 8], // 3 items betweem 900px and 601px
                itemsTablet: [600, 7], //2 items between 600 and 0;
                itemsMobile: [600, 7], // itemsMobile disabled - inherit from itemsTablet option
            });

        });
    </script>

    <style>
#allRewardVideos .shortname {border-radius: 100px;width: 25px;height: 25px;line-height: 31px;font-size: 12px;}
.notification_all .col-lg-2 .c-avatar img{    width: 55px;border-radius: 100px;height: 55px;    margin-bottom: 7px;object-fit: cover;}
.profile-sec .d-flex.flex-column img{width: 46px;margin: 3px auto 6px;border-radius: 100px;}


.subcat {
    background: #fff;
    height: 28px;
    padding: 3px 0 0 10px;
}
.edit-page .u-form .subcat .checkbox {
    margin-top: 0px !important;
    margin-bottom: 14px !important;
}
.u-form .subcat div.checkbox .subcat.11label.checkbox {
    position: absolute;
    margin-left: -36px;
    margin-top: -2px;
    padding: 0 !important;
}



#owl-demo .item {
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            text-align: center;
        }
        .customNavigation {
            text-align: center;
        }

        .customNavigation a {
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }

        @media only screen and (max-width: 767px) {
            .modal {
                top: 46px;
            }
            .modal-title {
                font-size: 12px;
            }
            .modal-header .close {
                margin-top: -24px;
            }

            .modal-body {
                padding: 10px 9px 0 9px;
            }

            .modal-footer {
                padding: 10px 15px;
            }

            .modal-content {
                border-radius: 0;
                box-shadow: 0 3px 9px rgba(0, 0, 0, 0.1);
            }
        }
    </style>
    <script>
        $(document).ready(function () {
            var owl = $("#owl-demo");
            owl.owlCarousel({
                items: 18, //10 items above 1000px browser width
                itemsDesktop: [1000, 9], //5 items between 1000px and 901px
                itemsDesktopSmall: [900, 7], // 3 items betweem 900px and 601px
                itemsTablet: [600, 4], //2 items between 600 and 0;
                itemsMobile: [600, 5], // itemsMobile disabled - inherit from itemsTablet option

            });
            $(".next").click(function () {
                owl.trigger('owl.next');
            })
            $(".prev").click(function () {
                owl.trigger('owl.prev');
            })
            $(".play").click(function () {
                owl.trigger('owl.play', 1000);
            })
            $(".stop").click(function () {
                owl.trigger('owl.stop');
            })
        });
        $('.pending_profile_class').click(function () {
            var div_id = this.id;
            console.log(div_id);
            $.ajax({
                type: "POST",
                url: "{{ route('later.complete.profile') }}",
                data: {id: div_id, '_token': '<?php echo csrf_token(); ?>'},
                dataType: "JSON",
                //  delay: 250,
                success: function (data) {
                    console.log(data);
                    //incomplete_process_div
                    document.getElementById('incomplete_process_div').style.display = 'none';
                },
                error: function () {
                    console.log('Data Not Found');
                }
            });
        });
    </script>
</footer>
