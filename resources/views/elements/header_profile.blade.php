<style>

    .content-wrapper .profile .col-lg-3.col-sm-6.videoitem .v-img {
        height: auto;
    }

    .photo-block:nth-child(8) {
        width: 16.2%;
        height: 174px;
    }

    .photo-block:nth-child(17) {
        width: 16.2%;
        height: 174px;
    }

    .photo-block:nth-child(23) {
        width: 16.2%;
        height: 174px;
    }

    .photo-block:nth-child(24) {
        width: 16.2%;
        height: 174px;
    }

    .photo-block:nth-child(25) {
        width: 16.2%;
        height: 174px;
    }

    .photo-block:nth-child(26) {
        width: 16.2%;
        height: 174px;
    }

    .photo-block:nth-child(41) {
        width: 16.2%;
        height: 174px;
    }

    .photo-block:nth-child(47) {
        width: 16.2%;
        height: 174px;
    }

    .photo-block:nth-child(48) {
        width: 16.2%;
        height: 174px;
    }

    .photo-block:nth-child(49) {
        width: 16.2%;
        height: 174px;
    }

    .photo-block:nth-child(50) {
        width: 16.2%;
        height: 174px;
    }

    .photo-block:nth-child(59) {
        width: 16.2%;
        height: 174px;
    }


    #mobile-video-and-stories a {
        border-bottom: 2px solid #fff;
    }

    .shane_tm_hero .main-profile-default {
        width: 100%;
        height: 100%;
        line-height: 266px;
        e
        margin-top: 0;
        border-radius: 1000px;
        font-size: 134px;
    }

    .videoitem .v-img a img {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }

    #profile_social a {
        display: inline-block;
        padding-top: 8px;
        padding-left: 3px;
        margin-top: 10px;
        width: 30px;
        height: 30px;
        border-radius: 50%;
        color: #ffffff;
        font-size: 14px;
        margin-right: 2px;
        margin-left: 2px;
    }

    #profile_social .fb {
        background-color: #3b5998;
    }

    #profile_social .tw {
        background-color: #55acee;
        padding-top: 7px;
        padding-left: 4px;
    }

    #profile_social .gp {
        background-color: #dd4b39;
        padding-top: 7px;
        padding-left: 5px;
    }

    .reward-all-dis {
        width: 65px;
        float: left;
        margin-right: 7px;
    }

    .reward-all-dis img {
        margin: 0px 0 4px 0;
        border: solid 2px #f72e5e;
        padding: 2px;
        border-radius: 100px;
        width: 60px;
    }

    .reward-all-dis.right-rew img {
        border-radius: 100px;
        object-fit: cover;
        margin: 0 auto;
        display: block;
        transition: 0.3s;
        width: 60px;
        height: 60px;
        border: solid 2px #f72e5e;
        padding: 2px;
    }

    .reward-all-dis h4 {
        text-align: center;
        font-size: 11px;
        margin-top: 3px;
    }

    .reward-all-dis p {
        color: #7e7e7e;
        text-align: center;
        font-size: 12px;
        margin-top: 7px;
        line-height: 14px;
    }

    .reward-all-dis.right-rew {
        position: relative;
        margin: 0px;
        width: 68px;
    }

    .reward-all-dis.right-rew strong {
        width: 4px;
        height: 2px;
        display: inline-block;
    }

    .reward-all-dis.right-rew span {
        position: relative;
        top: 3px;
        right: 1px;
        color: #000;
        display: block;
        width: 100%;
        text-align: center;
        line-height: 11px;
        border-radius: 00;
        font-size: 10px;
        padding: 4px 5px;
        height: 28px;
        overflow: hidden;
        white-space: nowrap;
        text-overflow: ellipsis;
        -webkit-line-clamp: 2;
        text-align: center;
    }

    .zoom_img1 {
        border: solid 0px #f72e5e;
        transition: 0.3s;
        margin: 0 auto;
        padding: 0;
        overflow: hidden;
        border-radius: 0;
    }

    .zoom_img1:hover img {
        transition: 0.3s;
    }

    .zoom_img1:hover {
        transition: 0.3s;
    }

    .reward-all-dis.right-rew a:hover span {
        transition: 0.3s;
    }

    .channel .c-details {
        border-bottom: solid 0px #eceff0;
    }

    #mobile-video-and-stories {
        border-bottom: solid 1px #eceff0;
        margin-bottom: 14px;
    }

    #mobile-video-and-stories {
        width: 100%;
    }

    #profile_social a {
        display: inline-block;
        padding-top: 8px;
        padding-left: 3px;
        margin-top: 10px;
        width: 30px;
        height: 30px;
        border-radius: 50%;
        color: #ffffff;
        font-size: 14px;
        margin-right: 2px;
        margin-left: 2px;
    }

    #profile_social .fb {
        background-color: #3b5998;
    }

    #profile_social .tw {
        background-color: #55acee;
        padding-top: 7px;
        padding-left: 4px;
    }

    #profile_social .gp {
        background-color: #dd4b39;
        padding-top: 7px;
        padding-left: 5px;
    }

    .checked,
    .fa-star-half-o {
        color: orange;
    }

    #new-profile {
        width: 90px;
        float: left
    }

    #new-profile-details {
        width: 89%;
        float: left;
        margin-left: 2%
    }

    .channel .c-details .c-nav {
        padding-top: 0px
    }

    #social-icons-profile {
        width: 119px;
        position: absolute;
        right: -10px;
        top: 0;
    }

    .channel .c-details {
        height: auto;
    }

    .channel .c-details .c-nav .list-inline li a {
        line-height: 47px;
    }

    #mobile-video-and-stories ul {
        padding: 0;
        margin: 0;
    }

    #mobile-video-and-stories {
        border-bottom: solid 1px #eceff0;
    }

    .channel .c-details .c-nav .list-inline li a {
        display: block;
    }

    #about-new-01 {
        margin: 0px;
        padding: 23px 0 10px 0
    }

    #new-profile-details h5 {
        margin-bottom: 0px;
        font-size: 17px;
        color: #000;
        margin-top: 26px;
    }

    #profile_social a {
        width: 27px;
        margin-right: 0px;
        margin-left: 2px;
        height: 27px;
        font-size: 11px;
    }

    #new-profile-details a {
        font-size: 12px;
    }

    #mobile-video-and-stories a.active {
        border-bottom: 2px solid #f72e5e;
        color: #f72e5e
    }

    #mobile-video-and-stories a:hover {
        border-bottom: 2px solid #f72e5e;
        color: #f72e5e
    }

    #follower-main1 p {
        float: left;
        width: 33.3%;
        border-top: solid 1px #eceff0;
        text-align: Center;
        line-height: 15px;
        padding: 10px 0;
        margin: 0px;
        border-bottom: solid 1px #eceff0;
    }

    #follower-main1 .btn-01765 a {
        background: #28b47e !important;
        color: #fff;
        display: block;
        height: 32px;
        line-height: 34px;
    }

    #follower-main1 {
        margin-top: 10px
    }

    #follower-main2 p {
        float: left;
        width: 50%;
        border-top: solid 1px #eceff0;
        text-align: Center;
        line-height: 16px;
        padding: 10px 0;
        margin: 0px;
        border-bottom: solid 1px #eceff0;
    }

    #follower-main2 a {
        background: #28b47e !important;
        color: #fff;
        display: block;
        height: 32px;
        line-height: 34px;
    }

    #follower-main2 {
        margin-top: 10px
    }

    .channel .c-details .c-nav .list-inline li a:hover:before {
        opacity: 0;
    }

    #profile_social .gp {
        padding-left: 1px;
    }

    #follower-main1 .btn-01765 a {
        width: 175px;
        float: right;
    }

    .user-search-individual {
        border-bottom: solid 1px #eceff0;
    }

    #user-search #new-profile {
        width: 6%;
        float: left
    }

    #user-search #new-profile-details {
        width: 92%;
        float: left;
        margin-left: 2%
    }

    #user-search h5 {
        font-size: 16px;
        margin-top: 18px;
        letter-spacing: 0.2px;
    }

    #user-search p {
        font-size: 12px;
    }

    #user-search a.flw {
        float: right;
        margin: 29px 0 0 0;
        background: #28b47e !important;
        color: #fff;
        width: 90px;
        text-align: center;
        z-index: 99;
        position: relative;
        line-height: 27px;
        height: 25px;
    }

    .main-profile-default {
        width: 80px;
        display: block;
        height: 80px;
        text-align: center;
        line-height: 88px;
        margin-top: 10px;
        background: #28b47e !important;
        color: #fff;
        border-radius: 100px;
        font-size: 32px;
        font-weight: bold;
        text-transform: uppercase;
    }

    .main-profile-default:hover {
        color: #fff;
    }

    .channel .c-details .c-nav {
        margin-top: 0px !important;
    }

    b,
    strong {
        font-weight: bold;
        font-family: arial;
        color: #7e7e7e;
    }


    #raising-star-mobile {
        display: none;
    }

    @media (min-width: 100px) and (max-width: 767px) {
        #raising-star-mobile {
            display: block;
        }

        #live-video-user-list-home {
            display: none
        }

        .reward-all-dis.new_mbl40972 {
            width: 18% !Important;
            float: left !important;
            margin-right: 0 !important;
        }


        .new_circle_width_set .owl-item {
            width: 66px !important;
        }

        #img_cutting_modal .cr-viewport.cr-vp-square {
            width: 280px !Important;
            height: 280px !Important;
        }

        #img_cutting_modal .cr-boundary {
            width: 280px !Important;
            height: 280px !Important;
        }

        #follower-main1 .btn-01765 a {
            width: 100%;
            float: none;
        }

        .content-wrapper .profile .col-lg-3.col-sm-6.videoitem {
            width: 50%;
            padding: 0 2px;
            float: left;
        }

        .shane_tm_hero {
            display: none;
        }

        #no0dek {
            display: block !important;
        }

        .abt-mbl-sml {
            font-size: 12px;
        }

        .mobile-pad1 {
            display: block !important;
        }

        .main-profile-default {
            width: 75px;
            height: 75px;
            line-height: 88px;
            margin-top: 2px;
            font-size: 32px;
        }

        .btn-cv1 {
            width: 90%;
            margin: 0 auto !important;
            display: block;
        }

        .cb-content.videolist {
            margin: 0px;
        }

        #new-profile-details .channel-details {
            display: block;
        }

        #follower-main1 a {
            float: none;
            width: auto
        }

        .videolist .videoitem {
            width: 50%;
            float: left;
        }

        #new-profile {
            width: 25%;
            float: left
        }

        #new-profile-details {
            width: 72%;
            float: left;
            margin-left: 3%
        }

        .channel .c-details .c-nav {
            padding-top: 0px
        }

        #social-icons-profile {
            width: 105px !important;
            position: absolute;
            right: -10px;
            top: 0;
        }

        #mobile-video-and-stories li {
            width: 25%;
            padding: 0;
            margin: 0;
            text-align: center;
            float: left;
        }

        #mobile-video-and-stories {
            width: 100%;
            display: block;
        }

        .channel .c-details .c-nav .list-inline li a {
            line-height: 47px;
        }

        #mobile-video-and-stories ul {
            padding: 0;
            margin: 0;
        }

        #mobile-video-and-stories {
            border-top: solid 1px #eceff0;
        }

        .channel .c-details .c-nav .list-inline li a {
            display: block;
        }

        #about-new-01 {
            margin: 0px;
            padding: 18px 0 13px 0
        }

        #new-profile-details h5 {
            margin-bottom: 0px;
            font-size: 17px;
            color: #000;
            margin-top: 12px;
        }

        #profile_social a {
            width: 27px;
            margin-right: 0px;
            margin-left: 2px;
            height: 27px;
            font-size: 11px;
        }

        #new-profile-details a {
            font-size: 12px;
        }

        #mobile-video-and-stories a.active {
            border-bottom: 2px solid #f72e5e;
            color: #f72e5e
        }

        #mobile-video-and-stories a:hover {
            border-bottom: 2px solid #f72e5e;
            color: #f72e5e
        }

        #follower-main1 p {
            float: left;
            width: 33.3%;
            border-top: solid 1px #eceff0;
            text-align: Center;
            line-height: 15px;
            padding: 10px 0;
            margin: 0px;
            border-bottom: solid 1px #eceff0;
        }

        #follower-main1 .btn-01765 a {
            background: #28b47e !important;
            color: #fff;
            display: block;
            height: 32px;
            line-height: 34px;
        }

        #follower-main1 {
            margin-top: 10px
        }

        #follower-main2 p {
            float: left;
            width: 50%;
            border-top: solid 1px #eceff0;
            text-align: Center;
            line-height: 16px;
            padding: 10px 0;
            margin: 0px;
            border-bottom: solid 1px #eceff0;
        }

        #follower-main2 a {
            background: #28b47e !important;
            color: #fff;
            display: block;
            height: 32px;
            line-height: 34px;
        }

        #follower-main2 {
            margin-top: 10px
        }

        .channel .c-details .c-nav .list-inline li a:hover:before {
            opacity: 0;
        }

        #profile_social .gp {
            padding-left: 1px;
        }

        .user-search-individual {
            border-bottom: solid 1px #eceff0;
        }

        #user-search #new-profile {
            width: 20%;
            float: left
        }

        #user-search #new-profile-details {
            width: 78%;
            float: left;
            margin-left: 2%
        }

        #user-search h5 {
            font-size: 16px;
            margin-top: 18px;
            letter-spacing: 0.2px;
        }

        #user-search p {
            font-size: 12px;
        }

        #user-search a.flw {
            float: right;
            margin: 29px 0 0 0;
            background: #28b47e !important;
            color: #fff;
            width: 90px;
            text-align: center;
            z-index: 99;
            position: relative;
            line-height: 27px;
            height: 25px;
        }

        #profile_social .tw {
            padding-left: 1px;
        }

        #about-new-01 p {
            margin-bottom: -1px;
            font-size: 12px;
        }

        .mobile-pad1 {
            padding: 18px 15px 0px 15px;
            clear: both;
            overflow: hidden;
        }

    }

    .channel.light .c-avatar a img {
        object-fit: cover;
    }
</style>
@php
    $follow = DB::table('followers')
        ->where([['user_id', session()->get('user_id')], ['follows_id', $user->id]])
        ->count();

@endphp


<div class="container" style=" border-top: solid 1px #eceff0;padding:0px" id="pr-0101">
    <!--desktop-->
    @php
        $loggedName = session()->get('user_name');
        $profileName = ucwords($user->name);
        $artist = \App\Models\ArtistAbout::where('user_id', $user->id)->first();
        $productionHouse = \App\Models\ProductionAbout::where('user_id', $user->id)->first();
    @endphp
    @if ($profileName != $loggedName)
        <style>
            .shane_tm_hero #social-icons-profile {
                right: 35px;
                top: 26px;
            }

            .shane_tm_hero {
                width: 100%;
                height: auto;
                margin-bottom: 10px;
                float: left;
                clear: both;
                position: relative;
            }

            .shane_tm_hero .image {
                position: absolute;
                right: 0;
                top: 0;
                bottom: 0;
                left: 0;
                background-repeat: no-repeat;
                background-position: center;
                background-size: cover;
            }

            .content_all h3 {
                line-height: 59px;
                color: #f72e5e;
                font-weight: bold;
                font-size: 64px;
                position: relative;
            }

            .content_all p.top1 {
                width: 100%;
                font-size: 16px;
            }

            .content_all {
                padding: 40px 0 0 0;
                float: right;
                width: 66%;
            }

            .background {
                float: left;
                width: 34%
            }

            .background-insde {
                width: 250px;
                margin: 55px auto 0;
                display: block;
                border-radius: 1000px;
                overflow: inherit;
                height: 250px;
                border: 2px solid #f72e5e;
                padding: 5px
            }

            .background img {
                width: 100%;
                border-radius: 1000px;
                height: 100%;
                object-fit: cover
            }

            .gallery-b {
                width: 16.666%;
                float: left;
                padding: 3px;
                float: left;
            }

            .gallery-b img {
                width: 100%;
            }

            .mobile-pad1 {
                display: none;
            }

            #no0dek {
                display: none;
            }

            .content_all p {
                margin-bottom: 2px;
                padding-top: 2px;
            }
        </style>
        @php

            $rewardVideos = App\Models\Video::where(['isRewardVideo' => 1])
                ->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'created_at')
                ->orderBy('total_views_count', 'desc')
                ->with([
                    'thumb' => function ($thumb) {
                        $thumb->select('id', 'thumb');
                    },
                ])
                ->with([
                    'user' => function ($user) {
                        $user->select('id', 'name', 'photo', 'slug');
                    },
                ])
            
                ->with([
                    'totalViews' => function ($query) {
                        $query
                            ->select('video_id')
                            ->groupBy('video_id', 'video_id')
                            ->orderBY(DB::raw('count(video_id)'), 'DESC');
                    },
                ])
                ->withCount(['totalViews'])
                ->with('watchTime')
                ->paginate(10);

            if (!empty($user->photo)) {
                $userPhotoPath = asset('public/uploads/users/small/' . $user->photo);
            }
        @endphp
        <div class="shane_tm_hero" id="home" data-style="three">
            <div class="container">
                <div class="background">
                    <div class="background-insde">
                        @if (!empty($userPhotoPath))
                            {{ HTML::image($userPhotoPath, null, ['style' => 'display:block;border-radius:1000px']) }}
                        @else
                            <a class="main-profile-default">
                                <div class="shortnamepr">
                                    {{ name_to_pic($user->name) }}
                                </div>
                            </a>
                        @endif

                        @if($user->isPremium() || $user->hasThemePlan())
                            <img src="https://entertainmentbugs.com/public/img/star.gif" id="raising-star-desk"/>
                        @endif
                        @if($artist)
                            <div style="float: none;clear: both;overflow: hidden;color: #fff;width: 145px;font-size: 12px;background: #f72e5e;text-align: center;padding:5px 0 4px 0;margin: 35px auto 1px;display: block;">
                                <a href="{{ $artist->username ? url('artist/'.$artist->username) : '' }}"><i
                                            class="fas fa-theater-masks" aria-hidden="true" style="color: #fff;"></i>
                                    <span style=" color: #fff;font-size: 12px;"> Artist Portfolio</span></a>
                            </div>
                        @elseif($productionHouse)
                            <div style="float: none;clear: both;overflow: hidden;color: #fff;width: 145px;font-size: 12px;background: #f72e5e;text-align: center;padding:5px 0 4px 0;margin: 35px auto 1px;display: block;">
                                <a href="{{ url('production-house/'.$productionHouse->username) }}"><i
                                            class="fa fa-film" aria-hidden="true" style="color: #fff;"></i> <span
                                            style=" color: #fff;font-size: 12px;">Production Profile</span></a>
                            </div>

                        @endif
                    </div>
                </div>

                <div class="content_all">

                    <div class="c-name" style="text-align:center; font-size:14px;" id="social-icons-profile">
                        <div class="c-social" id="profile_social">
                            <a href="{{ $user->facebook }}" target="_blank" class="fb"><i
                                        class="fa fa-facebook"></i></a>
                            <a href="{{ $user->twitter }}" target="_blank" class="tw"><i
                                        class="fa fa-twitter"></i></a>
                            <a href="{{ $user->instagram }}" target="_blank" class="gp"><i
                                        class="fa fa-instagram"></i></a>
                        </div>
                    </div>

                    <style>
                        #raising-star-desk {
                            width: 75px !important;
                            float: left;
                            height: auto !Important;
                            margin: -55px 1px 0 0;
                            -webkit-transform: rotate(272deg);
                            -moz-transform: rotate(272deg);
                            -ms-transform: rotate(272deg);
                            -o-transform: rotate(272deg);
                            transform: rotate(272deg);
                        }

                        #raising-star-mobile {
                            width: 38px !important;
                            float: left;
                            height: auto !Important;
                            margin: -36px 1px 0 -6px;
                            -webkit-transform: rotate(272deg);
                            -moz-transform: rotate(272deg);
                            -ms-transform: rotate(272deg);
                            -o-transform: rotate(272deg);
                            transform: rotate(272deg);
                        }

                    </style>

                    <h3>Hello I’m <bR>{{ $user->name }} </h3>
                    <div>
                        <strong style="margin-bottom: 0px;display: block;float:left; margin-right:3px;"> User
                            Name:</strong>
                        <p>{{$user->slug}}</p>
                        <div style="clear:both; overflow:hidden"></div>


                        <strong style="float:left; margin-right:3px;"> About me:</strong> {!! $user->about ?? '' !!}
                        <div style="clear:both; overflow:hidden"></div>
                        @if ($user->email_status == 1)
                            <strong> Contact:</strong> {{ $user->email ?? '' }}
                        @endif
                        <div style="clear:both; overflow:hidden"></div>
                        <strong> Website:</strong>
                        <a href="{{ $user->website }}" style="color: #337ab7;display:inline-block" target="_blank">
                            {{ $user->website ?? '' }} </a>


                        <div style="clear:both; overflow:hidden"></div>
                        <strong style="float:left; margin-right:3px;"> Location:</strong> {{ @$user->country->name }}
                        / {{ @$user->state->name }}
                        <div style="clear:both; overflow:hidden"></div>


                        <div style="clear:both;overflow:hidden;height: 3px;width: 100%;"></div>
                        <strong style="float:left; margin-right:3px;"> Language:</strong>

                            <?php
                            $languageList = new \Illuminate\Support\Collection(explode(",", @$user->language));

                            $languageList->each(function ($language) {

                                echo "<span class='tag label label-info'>" . $language . "</span>";
                            });

                            ?>
                    </div>
                    <Style>
                        .tag.label.label-info {
                            background: #fff;
                            color: #000;
                            font-weight: normal;
                            margin-left: 6px;
                            line-height: 5px;
                            padding: 2px 10px 0 10px;
                            display: inline
                        }

                        ;
                    </Style>

                    <div style="clear:both; overflow:hidden"></div>

                    <div class="containers" id="follower-main1">
                        <p>
                            <a href="{{ URL::to($user->slug . '/followers') }}"><strong> {{ $user->getFollowerCount() }}</strong>
                                <br> Followers</a></p>
                        <p>
                            <a href="{{ URL::to($user->slug . '/following') }}"><strong>{{ $user->getFollowingCount() }}</strong><br>
                                Following</a></p>
                        @if ($follow > 0)
                            <div class="btn-01765"><p><a data-do='unfollow' data-user-id='{{ $user->id }}'
                                                         style="background:#f72e5e !important"><i
                                                class="fa fa-solid fa-heart"></i> Following</a></p></div>
                        @else
                            <div class="btn-01765"><p><a data-do='follow' data-user-id='{{ $user->id }}'><i
                                                class="fa fa-heart-o"></i> Follow</a></p></div>
                        @endif
                    </div>
                    <div class="reward_container" style="background:#fafafa !important">
                        <div class="col-xs-12 col-sm-12" id="about-new-01" style="border-top:solid 0px #eceff0">
                            <div class="reward-all-dis">
                                <img src="https://www.entertainmentbugs.com/public/img/announcment.png">
                                <h4>Latest Post</h4>
                            </div>

                            @if (isset($topMedia) && count($topMedia) > 0)

                                @foreach ($topMedia as $key => $value)
                                    @php
                                        $url = '';
                                        $image = $value->image;
                                        $username = '';
                                    @endphp
                                    <div class="users">
                                        @if ($value->tag == 'photo')
                                            @php
                                                $images = unserialize($value->image);
                                                $image = "uploads/photo/$images[0]";
                                                $url = url("single-photo/$value->slug");
                                            @endphp
                                        @elseif ($value->tag == 'story')
                                            @php
                                                $image = preg_match('/^(http|https):\/\//', $value->image) ? $value->image : STORY_THUMB_DISPLAY_PATH . $value->image;
                                                $splitted = explode('/', $image);
                                                $len = count($splitted);
                                                if ($splitted[$len - 1] == $splitted[$len - 2]) {
                                                    array_pop($splitted);
                                                    $image = implode('/', $splitted);
                                                }
                                                
                                                if (!empty($value->image) && file_exists(preg_replace('/^(http|https):\/\/(.+)\/public\//', public_path('/'), $image))) {
                                                    $image = preg_replace('/^(http|https):\/\/(.+)\/public\//', '', $image);
                                                } else {
                                                    $image = 'img/story_thumb.jpg';
                                                }
                                                
                                                $url = to_story($value->slug);
                                            @endphp
                                        @else
                                            @php
                                                if (!empty($value->image)) {
                                                    $video = \App\Models\VideoThumb::select('thumb')
                                                        ->where('id', $value->image)
                                                        ->first();
                                                
                                                    if ($video) {
                                                        $image = str_replace(HTTP_PATH . '/public/', '', VIDEO_THUMB_DISPLAY_PATH) . $video->thumb;
                                                    } else {
                                                        $image = 'img/video1-1.png';
                                                    }
                                                } else {
                                                    $image = 'img/video1-1.png';
                                                }
                                               
                                                if ($value->tag == 'reward_video') {
                                                    $url = url("watch-reward/$value->slug");
                                                } else {
                                                    $url = url("watch/$value->slug");
                                                }

                                            @endphp
                                        @endif

                                        <div class="reward-all-dis right-rew">
                                            <a href="{{ $url }}">
                                                <div class="zoom_img1">
                                                    <img src="{{ asset('public/' . $image) }}" alt="{{$value->_slug}}">
                                                    <span>{{$value->_slug}}</span>
                                                </div>
                                            </a>

                                        </div>

                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <!--desktop-->
    <div class="mobile-pad1">

        <div class="col-lg-2" style="padding:0px;" id="new-profile">

            <div class="c-avatar">
                @php
                    if (!empty($user->photo)) {
                        $userPhotoPath = asset('public/uploads/users/small/' . $user->photo);
                    }
                @endphp
                @if (!empty($userPhotoPath))
                    <a href="{{ URL::to('profile') }}">
                        {{ HTML::image($userPhotoPath, null, ['style' => 'width:80px;height:80px; margin:10px auto 10px; display:block;border-radius:100px']) }}</a>
                @else
                    <a href="{{ URL::to('profile') }}" class="main-profile-default">
                        @php$words = explode(' ', Session::get('user_name'));
                            echo '<div class="shortnamepr">' . strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1)) . '</div>';
                        @endphp
                    </a>
                @endif
                <img src="https://entertainmentbugs.com/public/img/star.gif" id="raising-star-mobile">
            </div>
            @php
                $loggedName = session()->get('user_name');
                $profileName = $user->name;
            @endphp
        </div>
        <div class="col-lg-10  col-xs-10" id="new-profile-details">

            <div class="channel-details">
                <div class="row">

                    @php
                        $loggedName = session()->get('user_name');
                        $profileName = ucwords($user->name);
                    @endphp


                    @if ($profileName == $loggedName)
                        <h5>     {{$user->slug}}
                            <!--{{ $user->name }}-->

                            {{ $user->username }} <br> <a href="#">

                                <!--<a href="#">-->
                                <!--    <span class="fa fa-star checked"></span>-->
                                <!--</a>-->
                                <!--<a href="#">-->
                                <!--    <span class="fa fa-star checked"></span>-->
                                <!--</a>-->
                                <!--<a href="#">-->
                                <!--    <span class="fa fa-star checked"></span>-->
                                <!--</a>-->
                                <!--<a href="#">-->
                                <!--    <span class="fa fa-star checked"></span>-->
                                <!--</a>-->
                                <!--<a href="#">-->
                                <!--    <span class="fa fa-star-o"></span>-->
                                <!--</a> -->
                            </a></h5>
                        <p style="margin:0">
                            @if ($user->email_status == 1)
                                {{ $user->email ?? '' }}
                            @endif
                        </p>
                        <span style="   font-size: 11px;margin: 1px 0 6px 0;display: inline-block;">{{@$user->country->name}} / {{@$user->state->name}}</span></br>
                        <Style>
                            .tag.label.label-info {
                                background: #f4f3f3 !Important;
                                color: #000;
                                font-weight: normal;
                                margin-right: 3px;
                                line-height: 5px;
                                padding: 2px 8px 0 8px;
                                display: inline;
                            }

                            #new-profile-details h5 {
                                margin-top: 13px;
                            }
                        </Style>

                            <?php
                            $languageList = new \Illuminate\Support\Collection(explode(",", @$user->language));
                            $languageList->each(function ($language) {
                                echo "<span class='tag label label-info'>" . $language . "</span>";
                            });

                            ?>



                        <div class="c-name" style="text-align:center; font-size:14px;" id="social-icons-profile">
                            <div class="c-social" id="profile_social">
                                <a href="{{ $user->facebook }}" target="_blank" class="fb"><i
                                            class="fa fa-facebook"></i></a>
                                <a href="{{ $user->twitter }}" target="_blank" class="tw"><i
                                            class="fa fa-twitter"></i></a>
                                <a href="{{ $user->instagram }}" target="_blank" class="gp"><i
                                            class="fa fa-instagram"></i></a>
                            </div>

                        </div>


                        <!--My PF-->
                        <style>#raising-star-mobile {
                                display: none
                            }</style>
                        <div style="float: right;padding: 3px 8px  1px 8px;color: #f72e5e !important;margin-right:7px;">
                            <a href="{{ $artist && $artist->username ? url('artist/'.$artist->username) : "#" }}"><i
                                        class="fas fa-theater-masks" aria-hidden="true"
                                        style="color: #f72e5e;font-size: 14px;"></i> <span
                                        style="color: #f72e5e;font-size: 13px;">Artist Profile</span></a>
                        </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>


    <div class="clear"></div>

    <div class="container" id="follower-main1">
        <p><a href="{{ URL::to($user->slug . '/followers') }}"><strong> {{ $user->getFollowerCount() }}</strong> <br>
                Followers</a><br></p>
        <p><a href="{{ URL::to($user->slug . '/following') }}"><strong>{{ $user->getFollowingCount() }}</strong> <br>
                Following</a></p>
        <p class="btn-01765"><a href="{{ URL::to('edit-profile/' . $user->slug) }}">Edit Profile</a></p>
    </div>


    <div class="container">
        <div class="col-xs-12 col-sm-12" id="about-new-01">
            <h5 style="margin:0 0 4px 0;font-weight:bold">{{ $user->name }}
                <img src="http://entertainmentbugs.com/public/img/verified-profile-icon.png">
            </h5>

            <!--<strong style="float:left; margin-right:10px;display-inline-block;">About:</strong>-->
            {!! $user->about ?? '' !!} </p>
            <div style="clear:both;overflow:hidden"></div>

            <!--<strong> Website : </strong> -->

            <a href="{{ $user->website }}" style="color: #337ab7;" target="_blank"> {{ $user->website ?? '' }} </a>


        </div>

    </div>
</div>
</div>

<!--My profile css Start-->
<style>
    #live-video-user-list-home {
        display:
    }

    #other_user_profile .content-wrapper {
        background: #f4f3f3 !Important;
    }

    #other_user_profile .c-nav {
        margin-bottom: 0px;
    }

</style>

<!--My profile css End-->
@else
    <!--Other profile css on top-->

    <h5>{{ $user->name }} <br> <a href="#">
            <span class="fa fa-star checked"></span>
        </a>
        <a href="#">
            <span class="fa fa-star checked"></span>
        </a>
        <a href="#">
            <span class="fa fa-star checked"></span>
        </a>
        <a href="#">
            <span class="fa fa-star checked"></span>
        </a>
        <a href="#">
            <span class="fa fa-star-o"></span>
        </a>
    </h5>
    <p>
        @if ($user->email_status == 1)
            {{ $user->email ?? '' }}
        @endif
    </p>
    </div>
    <div class="c-name" style="text-align:center; font-size:14px;" id="social-icons-profile">
        <div class="c-social" id="profile_social">
            <a href="{{ $user->facebook }}" target="_blank" class="fb"><i class="fa fa-facebook"></i></a>
            <a href="{{ $user->twitter }}" target="_blank" class="tw"><i class="fa fa-twitter"></i></a>
            <a href="{{ $user->instagram }}" target="_blank" class="gp"><i class="fa fa-instagram"></i></a>
        </div>
    </div>
    </div>
    </div>
    </div>


    <div class="clear"></div>
    <div id="no0dek">
        <div class="container" id="follower-main1">
            <p><a href="{{ URL::to($user->slug . '/followers') }}"><strong> {{ $followers }}</strong>
                    <br>Followers</a><br></p>
            <p><a href="{{ URL::to($user->slug . '/following') }}"><strong>{{ $following ?? '' }}</strong> <br>Following</a>
            </p>
            @if ($follow > 0)
                <div class="btn-01765"><p><a data-do='unfollow' data-user-id='{{ $user->id }}'
                                             style="background:#f72e5e !important"><i class="fa fa-solid fa-heart"></i>
                            Following</a></p></div>
            @else
                <div class="btn-01765"><p><a data-do='follow' data-user-id='{{ $user->id }}'><i
                                    class="fa fa-heart-o"></i> Follow</a></p></div>
            @endif
        </div>
    </div>
    <div class="container" id="no0dek">
        <div class="col-xs-12 col-sm-12" id="about-new-01">
            <h5 style="margin:0 0 1px 0;font-weight:bold">{{$user->slug}}
                <!--{{ $user->name }}
                <img src="https://entertainmentbugs.com/public/img/verified-profile-icon.png">-->
            </h5>
            <a href="{{ $user->website }}" style="color: #337ab7;" target="_blank"> {{ $user->website ?? '' }} </a>
            {!! $user->about ?? '' !!}
            <div style="float: none;clear: both;overflow: hidden;color: #fff;width: 145px;font-size: 12px;background: #f72e5e;text-align: center;padding:5px 0 4px 0;margin: 8px auto 1px;display: block;">
                <a href="{{ $artist && $artist->username ? url('artist/'.$artist->username) : "#" }}"><i
                            class="fas fa-theater-masks" aria-hidden="true" style="color: #fff;"></i> <span
                            style=" color: #fff;font-size: 12px;"> Artist Portfolio</span></a>
            </div>
        </div>
        <a href="" style="color: #337ab7;" target="_blank">
        </a>


        <style>
            #reward_tags1 {
                margin: 0px;
                padding: 0px;
                list-style: none;
            }
        </style>


        <div class="reward_container mnuy8" style="background:#fafafa !important">
            <div class="col-xs-12 col-sm-12" id="about-new-01" style="border-top:solid 0px #eceff0">
                <div class="reward-all-dis new_mbl40972">
                    <img src="https://www.entertainmentbugs.com/public/img/announcment.png">
                    <h4>Latest Post</h4>
                </div>
                <div class="span12" id="crousal_wrap" style="width:100%">

                    <ul id="reward_tags1" class="users owl-carousel new_circle_width_set" style="display:block">

                        @if (isset($topMedia) && count($topMedia) > 0)
                            @foreach ($topMedia as $key => $value)
                                @php
                                    $url = '';
                                    $image = $value->image;
                                    $username = '';
                                @endphp


                                <li class="item">
                                    @if ($value->tag == 'photo')
                                        @php
                                            $images = unserialize($value->image);
                                            $image = "uploads/photo/$images[0]";
                                            $url = url("single-photo/$value->slug");
                                        @endphp
                                    @elseif ($value->tag == 'story')
                                        @php
                                            $image = preg_match('/^(http|https):\/\//', $value->image) ? $value->image : STORY_THUMB_DISPLAY_PATH . $value->image;
                                            $splitted = explode('/', $image);
                                            $len = count($splitted);
                                            if ($splitted[$len - 1] == $splitted[$len - 2]) {
                                                array_pop($splitted);
                                                $image = implode('/', $splitted);
                                            }

                                            if (!empty($value->image) && file_exists(preg_replace('/^(http|https):\/\/(.+)\/public\//', public_path('/'), $image))) {
                                                $image = preg_replace('/^(http|https):\/\/(.+)\/public\//', '', $image);
                                            } else {
                                                $image = 'img/story_thumb.jpg';
                                            }
                                            $url = to_story($value->slug);
                                        @endphp
                                    @else
                                        @php
                                            if (!empty($value->image)) {
                                                $video = \App\Models\VideoThumb::select('thumb')
                                                    ->where('id', $value->image)
                                                    ->first();

                                                if ($video) {
                                                    $image = str_replace(HTTP_PATH . '/public/', '', VIDEO_THUMB_DISPLAY_PATH) . $video->thumb;
                                                } else {
                                                    $image = 'img/video1-1.png';
                                                }
                                            } else {
                                                $image = 'img/video1-1.png';
                                            }

                                            if ($value->tag == 'reward_video') {
                                                $url = url("watch-reward/$value->slug");
                                            } else {
                                                $url = url("watch/$value->slug");
                                            }

                                        @endphp
                                    @endif


                                    <div class="reward-all-dis right-rew">
                                        <a href="{{ $url }}">
                                            <div class="zoom_img1">
                                                <img src="{{ asset('public/' . $image) }}" alt="{{$value->_slug}}">
                                                <span>{{$value->_slug}}</span>
                                            </div>
                                        </a>

                                    </div>


                                </li>

                            @endforeach
                        @endif</ul>
                </div>

            </div>
        </div>

    </div>

    @endif

    </div>
    </div>
    </div>
    </div>
    <div class="userprofile_contetn-display" id="other_user_profile" style="background:#fff">
        <div class="container">

            <div class="c-details" style="padding:0px;">

                <div class="c-nav" id="mobile-video-and-stories">
                    <ul class="list-inline">

                        @php
                            $loggedName = session()->get('user_name');
                            $profileName = ucwords($user->name);
                            $userSlug = $user->slug;
                        @endphp

                        @if ($userSlug == session()->get('slug'))
                            <!--<li><a href="{{ URL::to('profile/' . $userSlug) }}" style="margin-left:10px;">{{ $userSlug }}  &nbsp {{ HTML::image('public/img/verified-profile-icon.png') }}</a></li>-->
                            <li class="video_ac"><a href="{{ URL::to('profile/' . $userSlug) }}"
                                                    class="{{ request()->is('profile') ? 'color-active' : '' }}"><i
                                            class="fa fa-video-camera" title="Video"></i> Video</a></li>
                            <li class="photo_ac"><a href="{{ URL::to('myphoto/' . $userSlug) }}"
                                                    class="{{ request()->is('profile') ? 'color-active' : '' }}"><i
                                            class="fa fa-image"
                                            title="Video"></i> Photos</a></li>
                            <!-- <li><a href="{{ URL::to('edit-profile/' . $userSlug) }} " class="{{ request()->is('videos') ? 'color-active' : '' }}"><i class="fa fa-video-camera" title="Video"></i> Profile</a></li> -->
                            <li class="stories_ac"><a href="{{ URL::to('mystories/' . $userSlug) }}"
                                                      class="{{ request()->is('mystories') ? 'color-active' : '' }}"><i
                                            class="cv cvicon-cv-view-stats" title="Video"></i> Stories</a></li>
                            <li class="reward_ac"><a href="{{ URL::to('myrewards') }}"
                                                     class="{{ request()->is('rewards') ? 'color-active' : '' }}"><i
                                            class="fa fa-trophy" title="Video"></i> Rewards</a></li>
                            <li class="hidden-xs"><a
                                        href="{{ $artist && $artist->username ? url('artist/'.$artist->username) : "#" }}"><i
                                            class="fa fa-user" title=""></i> My Artist Portfolio</a></li>
                            <li class="hidden-xs">
                                <a href="{{ URL::to('earnings') }}"
                                   class="{{ request()->is('earnings') ? 'color-active' : '' }}"><i class="fa fa-money"
                                                                                                    title="Video"></i>
                                    Earning</a>
                            </li>
                            <li class="hidden-xs">
                                <a href="{{ URL::to('profile/' . $userSlug) }}">
                                    <i class="fa fa-dashboard" title=""></i>
                                    Dashboard
                                </a>
                            </li>
                            <li class="hidden-xs">
                                <a href="{{ URL::to('promote-recent-post') }}" class="{{ request()->is('promote-recent-post') ? '' : '' }}">
                                    <i class="fa fa-rocket" title=""></i>
                                    Promote Recent Post
                                </a>
                            </li>
                            <li class="hidden-xs">
                                <a href="{{ URL::to('promote-recent-post') }}" class="{{ request()->is('promote-recent-post') ? '' : '' }}">
                                    <i class="fa-solid fa-bullhorn" title=""></i>
                                    Create Ads Campaign
                                </a>
                            </li>
                        @else
                            <style>
                                .channel.light {
                                    background: #f4f3f3;
                                }
                            </style>
                            <li class="video_ac"><a href="{{ URL::to('videos/' . $userSlug) }}"
                                                    class="{{ request()->is('videos') ? 'color-active' : '' }}"><i
                                            class="fa fa-video-camera" title="Video"></i> Videos</a></li>
                            <!--videocolor*1-->
                            <li class="photo_ac"><a href="{{ url($user->slug."/photo") }}"
                                                    class="{{ request()->is('videos') ? 'color-active' : '' }}"><i
                                            class="fa fa-image" title="Video"></i> Photos</a></li>
                            <!--videocolor*1-->
                            <li class="stories_ac"><a href="{{ URL::to('mystories/' . $userSlug) }}"
                                                      class="{{ request()->is('mystories') ? 'color-active' : '' }}"><i
                                            class="cv cvicon-cv-view-stats" title="Video"></i> Stories</a></li>
                            <li class="reward_ac"><a href="{{ URL::to($user->slug . '/rewards') }}"
                                                     class="{{ request()->is('mystories') ? 'color-active' : '' }}"><i
                                            class="fa fa-trophy" title="Video"></i> Reward</a></li>
                        @endif
                        <div class="clear"></div>
                    </ul>
                    <div class="btn-group dropup pull-right">
                        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Discussion <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#"><i class="cv cvicon-cv-relevant"></i> Relevant</a></li>
                            <li><a href="#"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                            <li><a href="#"><i class="cv cvicon-cv-view-stats"></i> Viewed</a></li>
                            <li><a href="#"><i class="cv cvicon-cv-star"></i> Top Rated</a></li>
                            <li><a href="#"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                var owl = $("#reward_tags1");
                owl.owlCarousel({
                    items: 4, //10 items above 1000px browser width
                    itemsDesktop: [1000, 6], //5 items between 1000px and 901px
                    itemsDesktopSmall: [900, 6], // 3 items betweem 900px and 601px
                    itemsTablet: [600, 5], //2 items between 600 and 0;
                    itemsMobile: [600, 5], // itemsMobile disabled - inherit from itemsTablet option
                });
            });
        </script>