@if(!empty($video))	
	
	@php
		$videoUrl = $video->video_key;
		 
		$created_at = strtotime($video->created_at);
		$curDatetime = time();
		$difference = $curDatetime - $created_at;
	@endphp

<div class="col-lg-12  col-sm-12 col-sm-12 videoitem" id="c">
   <div class="b-video last-row">
	   <div class="v-img">
		   @if(!empty($video->thumb))
				
                @if(!empty($video->thumb))
            
                    @php
                        $vthumbBastPath = $video->thumb->thumb;
                    @endphp
                @endif
               @if(!empty($vthumbBastPath))
                    <img src="{{ asset('public/uploads/video/thumbs/'.$vthumbBastPath) }}" />
                @else
    			<a href="{{URL::to('watch-reward/'.$videoUrl)}}">{{HTML::image('public/img/video1-1.png')}}</a>	
    			@endif
			
			@endif
			<div class="time">{{$video->video_time}}</div>
	   </div>
    
   
	   <div class="v-desc">
		   <a href="{{URL::to('watch-reward/'.$videoUrl)}}">{{$video->title}}</a>
	   </div>
	   <div class="v-views">
		{{$video->total_views_count}}  views
	   
 
	    <span class="v-percent">
	   
	   <span class="v-circle"></span> {{ \Carbon\Carbon::parse($video->created_at)->diffForHumans() }} </span></div>
   </div>
   <div class="clearfix"></div>
</div>	

@endif