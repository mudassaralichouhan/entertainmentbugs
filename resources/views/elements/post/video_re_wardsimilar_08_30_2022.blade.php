@if(!empty($video))	
	@php
		$videoUrl = $video->video_key;
	@endphp
	<div class="col-lg-3 col-sm-6 col-xs-12">
	   <div class="h-video row">
		   <div class="col-sm-12 col-xs-6">
			   <div class="v-img">
                    @if(!empty($video->thumb))
        				
                        @if(!empty($video->thumb))
                    
                            @php
                                $vthumbBastPath = $video->thumb->thumb;
                            @endphp
                        @endif
                       @if(!empty($vthumbBastPath))
                            <img src="{{ asset('public/uploads/video/thumbs/'.$vthumbBastPath) }}" />
                        @else
            			<a href="{{URL::to('watch-reward/'.$videoUrl)}}">{{HTML::image('public/img/video1-1.png')}}</a>	
            			@endif
        			
        			@endif
					<div class="time">{{$video->video_time}}</div>
			   </div>
		   </div>
		   <div class="col-sm-12 col-xs-6">
			   <div class="v-desc">
				   <a href="{{URL::to('watch-reward/'.$videoUrl)}}">{{$video->title}}</a>
			   </div>
			   <div class="v-views">
				    views
			   </div>
		   </div>
	   </div>
	</div>	

@endif