@if(!empty($video))	
	@php
		  $videoUrl = $video->video_key;
	@endphp
<div class="col-lg-3 col-sm-6 videoitem"> 							 
	<div class="b-video">
		<div class="v-img" id="{{ $video->video_key}}">
			@if(!empty($video->thumb->thumb))
				
			@php
				$thumbBastPath = VIDEO_THUMB_UPLOAD_PATH.$video->thumb->thumb;
				$thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$video->thumb->thumb;
			
			@endphp
			
			@if(file_exists($thumbBastPath))
			@php echo $thumbBastPath @endphp
			<a href="{{URL::to('watch/'.$videoUrl)}}">{{HTML::image($thumbUrlPath)}}</a>
			@else
		
			<a href="{{URL::to('watch/'.$videoUrl)}}">{{HTML::image('public/img/video1-1.png')}}</a>	
			@endif
			
			@endif
			<div class="time">{{$video->video_time}}</div>
			@if($video->user_id == session()->get('user_id'))
			<div class="plus"><i class="cvicon-cv-plus" aria-hidden="true"></i></div>
			@endif
			<div class="plus-details">
				<ul >
					<li><a href="{{ URL::to('videos/edit-video/'.$video->video_key) }}"><i class="cvicon-cv-watch-later" aria-hidden="true"></i> Edit</a></li>
					<li>
							<form action="{{ url('/videos/delete/'.$video->video_key)}}" method="POST" id="deleteVideo">
								{{ csrf_field() }} 
								<button class="btn btn-danger btn-xs" type="submit">Delete</button>
							</form>	
					</li>
				</ul>
			</div>
		</div>
		<div class="v-desc">
			<a href="{{URL::to('watch/'.$videoUrl)}}">{{ $video->title }}</a>
		</div>
		<div class="v-views">
			 {{$video->views}} views <span class="v-percent"><span class="v-circle"></span> 78%</span>
		</div>
	</div>
</div>
@endif