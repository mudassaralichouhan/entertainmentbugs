@if(!empty($video))	
	@php
    	$videoUrl = $video->video_key; 
		$created_at = strtotime($video->created_at);
		$curDatetime = time();
		$difference = $curDatetime - $created_at;
		$watchInUrl = $video->isRewardVideo == 1 ? "watch-reward" : "watch";
	@endphp
<div class="col-lg-3 col-sm-6 videoitem"> 							 
	<div class="b-video">
		<div class="v-img" id="{{ $video->video_key}}">
			@if(!empty($video->thumb->thumb))
    			@php
    				$thumbBastPath = VIDEO_THUMB_UPLOAD_PATH.$video->thumb->thumb;
    				$thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$video->thumb->thumb;
    			
    			@endphp
    			@if(file_exists($thumbBastPath)) 
    			    <a href="{{URL::to($watchInUrl."/".$videoUrl)}}">{{HTML::image($thumbUrlPath)}}</a>
    			@else
        			<a href="{{URL::to($watchInUrl."/".$videoUrl)}}">{{HTML::image('public/img/video1-1.png')}}</a>	
    			@endif
			@endif
			<div class="time">{{ seconds_to_duration($video->video_time??0) }}</div>
			@if($video->user_id == session()->get('user_id'))
				<div class="plus"><i class="cvicon-cv-plus" aria-hidden="true"></i></div>
			@endif
			<div class="plus-details">
				<ul >
					<li><a href="{{ URL::to('videos/edit-video/'.$video->video_key) }}"><i class="cvicon-cv-watch-later" aria-hidden="true"></i> Edit</a></li>
					<li>
						<form action="{{ url('/videos/delete/'.$video->video_key)}}" method="POST" id="deleteVideo">
							{{ csrf_field() }} 
							<button class="btn btn-danger btn-xs" type="submit">Delete</button>
						</form>	
					</li>
				</ul>
			</div>
		</div>
		<div class="v-desc">
			<a href="{{URL::to($watchInUrl."/".$videoUrl)}}">{{ $video->title }}</a>
		</div>
		<div class="v-views">
			{{ $video->getUniqueUsers() }} views <span class="v-percent"><span class="v-circle"></span> {{ \Carbon\Carbon::parse($video->created_at)->diffForHumans() }}</span>
			<div id="pm-post">
				@if( isset($isLoggedInUser) && $isLoggedInUser && !$video->is_promoted && isset($key) && ($key == 0 || $key == 2) )
					<a href="{{ url("promote-post/video/".$video->video_key) }}" style="width:100%; padding: 5px 10px; text-align:center;background-color: #f72e5e;color:white; margin-top: 10px; display: inline-block; text-decoration:none">Promote your post</a>
				@endif
			</div>
		</div>
	</div>
</div>
 
<Style>
#mobile-video-and-stories .video_ac a{ border-bottom: 2px solid #f72e5e;color: #f72e5e;}
         </Style>
@endif