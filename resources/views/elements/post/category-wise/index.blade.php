@if( is_object($categoryWiseVideo) && $categoryWiseVideo->count() > 0 )
    @foreach( $categoryWiseVideo as $video )
        @if(!empty($video))	
            @php
                $videoUrl = $video->video_key;
                $userphotoPath = PROFILE_SMALL_DISPLAY_PATH.$video->user['photo'];
        
                $created_at = strtotime($video->created_at);
                $curDatetime = time();
                $difference = $curDatetime - $created_at;
                $watchStatus = 0;
                if(!empty($video->watchTime->watchTime)){
                    $watchStatus = ($video->watchTime->watchTime / $video->video_time) * 100;
                }
            @endphp
            
            <div class="col-lg-12  col-sm-12 col-sm-12 videoitem">
                <div class="user">
                    
                    @if(!empty($video->user['photo']))
                        <a href="{{URL::to('userprofile/'.$video->user['name'])}}" class="user_img" target="">{{HTML::image($userphotoPath)}}</a>
                    @else
                        <a href="{{URL::to('userprofile/'.$video->user['name'])}}" class="user_heading-title-bg" target="">  
                    
                    @php $words = explode(' ', $video->user['name']);
            echo '<div class="shortnamevd">'. strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1)).'</div>';
        @endphp
                        </a>
                    @endif
                    
                    <a href="{{URL::to('userprofile/'.$video->user['name'])}}" target=""> {{$video->user['name']}}</a>
                </div>
                <div class="b-video last-row">
                    <div class="v-img">
                        @if(!empty($video->thumb->thumb))
                        
                            @php
                                $thumbBastPath = VIDEO_THUMB_UPLOAD_PATH.$video->thumb->thumb;
                                $thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$video->thumb->thumb;
                                
                            @endphp
                                @if(file_exists($thumbBastPath))
                                    <a href="{{URL::to('watch/'.$videoUrl)}}">
                                        {{HTML::image($thumbUrlPath)}}
                                    
                                        @if(!empty($video->is_watched_video_count) && $video->is_watched_video_count > 0)
                                            <div class="watched-mask"></div>
                                            <div class="watched">WATCHED</div>
                                        @endif
                                        <div class="time">{{ $video->video_time}}</div>
                                    </a>
                                @else
                                    <a href="{{URL::to('watch/'.$videoUrl)}}">
                                        {{HTML::image('public/img/video1-1.png')}}
                                        @if(!empty($video->is_watched_video_count) && $video->is_watched_video_count > 0)
                                            <div class="watched-mask"></div>
                                            <div class="watched">WATCHED</div>
                                        @endif
                                        <div class="time">{{ $video->video_time}}</div>
                                    </a>
                                @endif
                    
                        @endif


                    </div>

                        @if($watchStatus != '')
                            <div class="sv-views-progress">
                            <div class="sv-views-progress-bar" style="width:{{ $watchStatus }}%"></div>
                            </div>
                        @endif


                    <div class="v-desc">
                        <a href="{{URL::to('watch/'.$videoUrl)}}">{{$video->title}}</a>
                    </div>
                    <div class="v-views">
                        {{ $video->total_views_count }} views. <span class="v-percent"><span class="v-circle"></span> {{ \Carbon\Carbon::parse($video->created_at)->diffForHumans() }}</span>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
    @if($categoryWiseVideo->toArray()['next_page_url'])
        <div id="load-more-videos"></div>
    @endif
@endif