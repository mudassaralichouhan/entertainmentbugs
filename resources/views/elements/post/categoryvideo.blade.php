@if( $videoData && count($videoData) > 0 )
        
        <?php
       // echo "<pre>";print_r($videoData); exit;
        ?>
        
    @foreach ($videoData as $key => $video)
          
        @if (!empty($video) && count($video->user->toArray())>0)

            @if( ($key+1)%8 == 0 )
                <div class="col-lg-2 col-sm-6 video-item ads_block_contatiner" data-ad-type="video" data-ad-style='listing' data-ad-block>

                </div>
            @endif
            @php
            
                $videoId = $video->id;
                $videoUrl = $video->video_key;
                $videoSrc = $video->video;
                $userphotoPath = PROFILE_SMALL_DISPLAY_PATH . $video->user['photo'];
                $created_at = strtotime($video->created_at);
                $curDatetime = time();
                $difference = $curDatetime - $created_at;
                $init = $video->video_time;
                $day = floor($init / 86400);
                $hours = floor(($init -($day*86400)) / 3600);
                $minutes = floor(($init / 60) % 60);
                $seconds = $init % 60;
            @endphp
            @if( !$video->isAd )
                <div class="col-lg-2 col-sm-6 videoitem" >
            @endif
                <div class="b-video mouse-hover-container" id="{{$videoUrl}}" data-value="video_div{{$videoUrl}}" data-video="{{$videoSrc}}" data-append-div="append_{{$videoId}}" data-video-poster="{{$video->thumb->thumb}}">
                 <div class="thumb_video">
                    <div class="image-container" >     
            
                        <div class="v-img member">
                        @if (!empty($video->user) && !empty($video->thumb))
                            @php
                                $thumbBastPath = VIDEO_THUMB_UPLOAD_PATH . $video->thumb->thumb;
                                $thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH . $video->thumb->thumb;
                            @endphp

                            @if (file_exists($thumbBastPath))
                                <a href="{{ URL::to('watch/' . $videoUrl) }}">{{ HTML::image($thumbUrlPath) }}
                                  
                                @if (!empty($video->user) && !empty($video->is_watched_video_count) && $video->is_watched_video_count > 0)
                                     <div class="watched-mask"></div>
                                     <div class="watched">WATCHED</div>
                                 @endif
                                 </a>
                            @else
                                <a href="{{ URL::to('watch/' . $videoUrl) }}">{{ HTML::image('public/img/video1-1.png') }}</a>
                            @endif
                        @endif  
                        
                        <div class="time"><?php echo "$hours:$minutes:$seconds"; ?></div>
                    </div>
                     </div> 
                     
                    
                    <div class="v-img video-container" id="append_{{$videoId}}" data-value="{{$videoUrl}}"></div>   
                    
                    <!--<div class="v-img video-container" id="videoSrc_{{$videoId}}" data-value="{{$videoUrl}}"></div>                    -->
                </div>
                
               
                
                       <div class="user">
                        @if(!empty($video->user['photo']))
                            <a href="{{ user_url($video->user['slug']) }}" class="user_img" target="">{{HTML::image($userphotoPath)}}</a>
                     @else
                            <a href="{{ user_url($video->user['slug']) }}" class="user_img" target="">
                                <div class="shortnamevd">{{ name_to_pic($video->user['name']) }}</div>
                            </a>  
                        @endif
                        
                    </div>
                    
                    
                    @if(!empty($video->user))
                    <div class="v-desc">
                        <a href="{{ URL::to('watch/' . $videoUrl) }}">{{ $video->title }}</a>
                    </div>
                   





                    <div class="v-views">{{ $video->total_views_count }} views <span class="v-percent"><span class="v-circle"></span> {{ \Carbon\Carbon::parse($video->created_at)->diffForHumans() }} </span>
                  <div class="tag_click_main">
                      
                    <div class="tag_click">
              <ul style="list-style:none; margin:0px;padding:0px;">   
        @foreach($video->video_tag as $tags)
        <form action="{{URL::to('search')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" class="form-control" name="searchdata" aria-describedby="sizing-addon2" value="{{ $tags->tag->tag }}">
            <input type="hidden" name="searchType" id="searchType" value="video"/>
            <li><input  class="tag_nme12" type="submit" value="{{ $tags->tag->tag }}"/> </li>
       
        </form>
         
        @endforeach
                                    
              </ul>                          
          
                      </div>
                       </div>
                    </div>
                 
                    @endif
                </div>
                

            @if( $video->isAd )
                <div>
                    <button type='button' data-ad-button='{{ $video->button_url }}'>{{ $video->button_text }}</button>
                </div>
            @else
                </div>
                
             
                
            @endif
        @endif
    @endforeach
@endif

<style>
.tag_nme12 {background: #e5e5e5 !important;padding: 6px 6px 3px 6px;color:#000 !important;font-size: 10px;line-height: 8px;margin-bottom: 10px;display: block;float: left;border: 0px !important;margin-right: 0px;}
.tag_click {overflow: hidden;width: 123%;height: 18px;font-size: 12px;}
.tag_click_main{overflow:hidden; width:100%;margin-top:7px;}

.v-img a::before {transition: 0.3s;background: url(https://www.entertainmentbugs.com/public/uploads/logo/favicon.png) no-repeat;position: absolute;width: 43px;height: 43px;top: 50%;margin-left:-21.5px;margin-top:-21.5px;z-index: 999;display: block;content: "";left: 50%;display:none;background-size: contain;}
.v-img a::after {transition: 0.3s;display:none;background: rgba(255,255,255,0.7);position: absolute;width: 100%;height: 100%;top: 0;z-index: 99; content: "";left: 0;}
.v-img a:hover::before{display:block;transition: 0.3s;}
.v-img a:hover::after{display:block;transition: 0.3s;}
</style>

<script>


    $('.mouse-hover-container').mouseover(function() {
     

        var base_url = window.location.origin;
        
        var current_element = $(this);
        var video_div_id = current_element.attr('data-value');
        var video_poster = current_element.attr('data-video-poster');
        var src_video = current_element.attr('data-video');
        var append_div = current_element.attr('data-append-div');
        
        var element = document.getElementById(append_div);
       
        var allChildNodes = element.childNodes;
        var childNodesLength = element.childNodes.length;
        
       if(childNodesLength > 0){
           for(var i=0; i< childNodesLength; i++)
           {
               if(allChildNodes[i])
                    allChildNodes[i].remove();
            }
    	}
        
        
        var video_url = base_url+"/public/uploads/video/"+src_video;
        const video = document.createElement('video');
        video.poster =base_url+"/public/uploads/video/thumbs/"+video_poster;
        
        video.src =video_url;
        video.autoplay = true;
        video.controls = false;
        video.loop = true;
        video.muted = true;
        video.id = video_div_id;
        setTimeout(function(){
    video.pause(); 
},5000);
        const box = document.getElementById(append_div);
        box.appendChild(video);
     
  /*
   		var video_key = this.id;
   		   $.ajax({
              type: "GET",
                url:"{{ route('get.video') }}",
                 data:{id:video_key,'_token':'<?php echo csrf_token(); ?>'},
                dataType: "JSON",
              //  delay: 250,
              success: function (data){
                  var video_id = "video_"+data.id;
                  var video_src = "videoSrc_"+data.id;
                  var video_tag = "video_tag_"+data.video_key;
                  //document.getElementById(data.video_key).style.display = 'none';
                   var video_url = base_url+"/public/uploads/video/";
                  // var video_url = "https://www.entertainmentbugs.com/public/uploads/video/";
                   video_url += data.video;
                   
                  //console.log(video_url);
                  
                  if (document.getElementById(video_tag) !== null){
                       document.getElementById(video_tag).remove();
                  }
                   
                  const video = document.createElement('video');

               
                video.src =video_url;
                
                // video.poster =
                //   'https://peach.blender.org/wp-content/uploads/title_anouncement.jpg?x11217';
                
                video.autoplay = true;
                video.controls = false;
                video.loop = true;
                video.muted = true;
                video.id = "video_tag_"+data.video_key;
                const box = document.getElementById(video_src);
                box.appendChild(video);
               },
               error: function() {
                    console.log('Data Not Found');
                }
                   });
                   
                   
                   */
   		
	});
	

	
	$('.video-container').mouseout(function() {

    var div_id = this.id;
    var element = document.getElementById(div_id);
    var allChildNodes = element.childNodes;
    var childNodesLength = element.childNodes.length;
    if(childNodesLength > 0){
       for(var i=0; i< childNodesLength; i++)
       {
           if(allChildNodes[i])
                allChildNodes[i].remove();
       }
	}
     
	});

	
	</script>
