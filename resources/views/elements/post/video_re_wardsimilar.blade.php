@if(!empty($video))	
	@php
		$videoUrl = ($video->isRewardVideo ? "watch-reward/" : "watch/"). $video->video_key;
	@endphp
	<div class="col-lg-3 col-sm-6 col-xs-12">
		
	   <div class="h-video row">
		   <div class="col-sm-12 col-xs-6">
			   <div class="v-img">
                    @if(!empty($video->thumb))
                        @if(!empty($video->thumb))
                            @php
                                $vthumbBastPath = $video->thumb->thumb;
                            @endphp
                        @endif
                       @if(!empty($vthumbBastPath))
                            <img data-testing src="{{ asset('public/uploads/video/thumbs/'.$vthumbBastPath) }}" />
                        @else
            				<a data-testing href="{{ URL::to($videoUrl )}}">{{ HTML::image('public/img/video1-1.png') }}</a>	
            			@endif
        			
					@else
						<a data-testing href="{{ URL::to($videoUrl )}}">{{ HTML::image('public/img/video1-1.png') }}</a>
        			@endif
					<div class="time">{{ $video->video_time }}</div>
			   </div>
		   </div>
		   <div class="col-sm-12 col-xs-6">
			   <div class="v-desc">
				   <a href="{{ user_url(@$video->user->slug) }}">{{ $video->title }}</a>
			   </div>
			   <div class="v-views">
				{{$video->total_views_count}}  views
			   </div>
			   <div class="v-percent"><span class="v-circle"></span> {{ \Carbon\Carbon::parse($video->created_at)->diffForHumans() }} </div>
		   </div>
	   </div>
	</div>	

@endif