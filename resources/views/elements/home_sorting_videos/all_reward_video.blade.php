@foreach ($rewardVideos as $video)
	@if(!empty($video) && !empty($video->user))
		@php
			$videoUrl = $video->video_key;
		@endphp

	<style>
		@media only screen and (max-width: 767px){
			.cb-header .list-inline>li {
				display: inline-block;
				padding-left: 0;
				padding-right: 0;
				width: 48%;
				margin: 0 0.7%;
			}
			#reward_1 .videolist .videoitem .b-video .v-desc {
				height: 38px;
				overflow: hidden;
				margin-bottom: 5px;
			}
			#home_page_video .v-desc a {
				font-size: 12px;
				line-height: 15px;
			}

		}
		.user-photo-reward{z-index:99;    position: absolute;right: 6px;bottom: 20px;}
		
	</style>
	<div class="col-lg-2 col-sm-2 videoitem">
	   <div class="user-photo-reward" style="padding:0">
                            <a href="{{ url($video->user->slug) }}">
                              @if( !empty($video->user->photo) )
                                <img style="width:25px;height:25px;border-radius:100px; margin:0 auto;display:block;" src="{{ asset('public/uploads/users/small/' . $video->user->photo) }}">
                              @else
                                <span class="shortname" style="display:inline-block; ">{{ name_to_pic($video->user->name) }}</span>
                              @endif
                                <!--<span style="font-size:11px;text-align: center;display: block;margin-top: 5px;">{{ $video->user->name }}</span>-->
                            </a>
                        </div>
 



<div class="b-video">
			<div class="v-img" id="{{ $video->video_key}}">
				@if(!empty($video->thumb->thumb))
					@php
						$thumbBastPath = VIDEO_THUMB_UPLOAD_PATH.$video->thumb->thumb;
						$thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$video->thumb->thumb;
					@endphp
				
					@if(file_exists($thumbBastPath))
						<a href="{{URL::to('watch-reward/'.$videoUrl)}}">{{HTML::image($thumbUrlPath)}}</a>
					@else
						<a href="{{URL::to('watch-reward/'.$videoUrl)}}">{{HTML::image('public/img/video1-1.png')}}</a>	
					@endif
				@endif
				<div class="time">{{$video->video_time}}</div>
			</div>
			<div class="v-desc">
				<a href="{{URL::to('watch-reward/'.$videoUrl)}}">{{$video->title}}</a>
			</div>
			<div class="v-views">
					{{$video->total_views_count}} views <span class="v-percent"><span class="v-circle"></span> {{ \Carbon\Carbon::parse($video->created_at)->diffForHumans() }} </span>
			</div>
		</div>
	</div>
	@endif
@endforeach
<div style="clear:both; overflow:hidden"></div>
{!! $rewardVideos->links() !!}