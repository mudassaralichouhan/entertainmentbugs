 @foreach ($videoData as $videos)
     @if (!empty($videos))
         @php
             $videosUrl = $videos->video_key;
             $userphotoPath = PROFILE_SMALL_DISPLAY_PATH . $videos->user['photo'];
             
             $created_at = strtotime($videos->created_at);
             $curDatetime = time();
             $difference = $curDatetime - $created_at;
             
             $watchStatus = 0;
             
             if (!empty($videos->watchTime->watchTime)) {
                 $watchStatus = ($videos->watchTime->watchTime / $videos->video_time) * 100;
             }
         @endphp
         <div class="col-lg-2  col-sm-2 col-sm-6 videoitem">
             <div class="user">

                 @if (!empty($videos->user['photo']))
                     <a href="{{ URL::to('userprofile/' . $videos->user['slug']) }}" class="user_img" target="">{{ HTML::image($userphotoPath) }}</a>
                 @else
                     <a href="{{ URL::to('userprofile/' . $videos->user['slug']) }}" class="user_heading-title-bg" target="">

                         @php$words = explode(' ', $videos->user['name']);
                             echo '<div class="shortnamevd">' . strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1)) . '</div>';
                         @endphp
                     </a>
                 @endif

                 <a href="{{ URL::to('userprofile/' . $videos->user['slug']) }}" target="">{{ $videos->user['name'] }}</a>
             </div>
             <div class="b-video last-row">
                 <div class="v-img">
                     @if (!empty($videos->thumb->thumb))
                         @php
							$thumbBastPath = VIDEO_THUMB_UPLOAD_PATH . $videos->thumb->thumb;
							$thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH . $videos->thumb->thumb;
                         @endphp
                         @if (file_exists($thumbBastPath))
                             <a href="{{ URL::to('watch/' . $videosUrl) }}">
                                 {{ HTML::image($thumbUrlPath) }}

                                 @if (!empty($videos->is_watched_video_count) && $videos->is_watched_video_count > 0)
                                     <div class="watched-mask"></div>
                                     <div class="watched">WATCHED</div>
                                 @endif

                                 <div class="time">{{ $videos->video_time }}</div>
                             </a>
                         @else
                             <a href="{{ URL::to('watch/' . $videosUrl) }}">
                                 {{ HTML::image('public/img/video1-1.png') }}
                                 @if (!empty($videos->is_watched_video_count) && $videos->is_watched_video_count > 0)
                                     <div class="watched-mask"></div>
                                     <div class="watched">WATCHED</div>
                                 @endif
                                 <div class="time">{{ $videos->video_time }}</div>
                             </a>
                         @endif
                     @endif


                 </div>

                 @if ($watchStatus != '')
                     <div class="sv-views-progress">
                         <div class="sv-views-progress-bar" style="width:{{ $watchStatus }}%"></div>
                     </div>
                 @endif


                 <div class="v-desc">
                     <a href="{{ URL::to('watch/' . $videosUrl) }}">{{ $videos->title }}</a>
                 </div>
                 <div class="v-views">
                     {{ $videos->total_views_count }} views. <span class="v-percent"><span class="v-circle"></span>
                         {{ \Carbon\Carbon::parse($videos->created_at)->diffForHumans() }} </span>
                 </div>
             </div>
         </div>
     @endif
     @if ($loop->iteration % 6 == 0)
         <div style="clear:both; overflow:hidden"></div>
     @endif
 @endforeach
