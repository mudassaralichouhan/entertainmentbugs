@foreach ($stories as $story)
    @if(!is_null($story))
        @php
            $created_at = strtotime($story->created_at);
            $curDatetime = time();
            $difference = $curDatetime - $created_at;
            $userBasePath = '';
            $userUrlPath = '';
        @endphp
        
        @if(!empty($story->thumb_image))
            @php
                $thumbBasePath =  STORY_THUMB_UPLOAD_PATH.$story->thumb_image;
                $thumbUrlPath = get_story_thumb_url($story->thumb_image);
            @endphp
        @endif
        
        @if(!empty($story->user->photo))
            @php
               $userBasePath = PROFILE_SMALL_UPLOAD_PATH.$story->user->photo;
               $userUrlPath = PROFILE_SMALL_DISPLAY_PATH.$story->user->photo;
            @endphp
        @endif
        
        
        

        
    <div class="well">
        <div class="main-ban">
            <a href="{{ to_story(@$story->slug) }}">
                @if(check_asset_existence($thumbUrlPath))
                    {{ HTML::image($thumbUrlPath) }}
                @else
                    {{ HTML::image('public/img/story_thumb.jpg', null, ['style'=>'width:100%']) }}
                @endif
            </a>
    
    
            <div class="stories-head">
                <a class="pull-left story_img" href="{{URL::to('userprofile/'.@$story->user->slug)}}" target="_blank">
                    @if(file_exists($userBasePath))
                            {{HTML::image($userUrlPath,null,['class'=>'media-object'])}}
                    @else
                        {{HTML::image('http://placekitten.com/150/150',null,['class'=>'media-object'])}}
                    @endif
                </a>
    
                <div class="media-body">
                    <div class="story_list_star">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star-half-o"></span>
                        &nbsp; 4.5
                    </div>
                    <a href="{{URL::to('userprofile/'.@$story->user->slug)}}" target="_blank"><h4 class="media-heading">{{@$story->user->name}}</h4></a>
                    <a href="{{ to_story(@$story->slug) }}"> 
                    <h5 class="media-heading">{{@$story->title}}</h5></a>
                </div>
    
            </div>
        </div>
        
        
        <div class="media">
            <!--<p>{!! @$story->about !!}</p>--> 
            <ul class="list-inline list-unstyled">
                <li><span> {{ \Carbon\Carbon::parse($video->created_at)->diffForHumans() }} </span></li>
                <li>|</li>
                <li><span> {{ @$story->total_views_count }} Views </span></li>
                <li>|</li>
                <li><span> {{ @$story->total_comments_count }} comments</span> </li>
                <li>|</li>
                <li><span><a href="{{ to_story(@$story->slug) }}"> Read More</a></span> </li>
                <li class="social_icons12">
                    <a href="{{@$story->user->facebook}}" target="_blank"><span><i class="fa fa-facebook-square"></i></span></a>
                    <a href="{{@$story->user->twitter}}" target="_blank"><span><i class="fa fa-twitter-square"></i></span></a>
                    <a href="{{@$story->user->instagram}}" target="_blank"><span><i class="fa fa-google-plus-square"></i></span></a>
                </li>
            </ul>
        </div>
    </div>
    @endif
    
<Style>
#latestStories .main-ban img {height: auto;object-fit: cover;}
#latestStories .well{width: 24.2%;}
#latestStories .main-ban {height: auto;}
.home_page_third_block .stories-head {padding: 9px 10px 0px 10px;position: relative;background: #fff;margin-bottom: -4px;}
.home_page_third_block #story_block h4 {color:#000;}
#latestStories h5.media-heading {font-size: 12px;color: #000 !important;line-height: 14px;}
.home_page_third_block .story_img {position: absolute;right: 0;top: -14px;}
#latestStories .media {margin-top: 2px;}
</Style>

@endforeach  
