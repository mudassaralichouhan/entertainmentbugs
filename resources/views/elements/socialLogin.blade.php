<div class="row" id="social_log">
   <br>
   <div class="col-lg-6 col-xs-6">                               
	<button type="submit" class="btn btn-cv1" style="background:#4267b2; border:1px solid #4267b2" onclick="return loginsignup('redirecttoacebook');">Facebook</button>                          
   </div>
   <div class="col-lg-6 col-xs-6">                               
	<button type="submit" onclick="return loginsignup('redirecttogoogle');" class="btn btn-cv1" style="background:#d74937; border:1px solid #d74937">Google Plus</button>                           
   </div>
</div>
<script type="text/javascript">
    var newwindow;
    var intId;
    function loginsignup(type) {
        var screenX = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft,
                screenY = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop,
                outerWidth = typeof window.outerWidth != 'undefined' ? window.outerWidth : document.body.clientWidth,
                outerHeight = typeof window.outerHeight != 'undefined' ? window.outerHeight : (document.body.clientHeight - 22),
                width = 800,
                height = 500,
                left = parseInt(screenX + ((outerWidth - width) / 2), 10),
                top = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
                features = (
                        'width=' + width +
                        ',height=' + height +
                        ',left=' + left +
                        ',top=' + top
                        );

        newwindow = window.open('{!! HTTP_PATH !!}/users/'+type, 'Social Login', features);
        if (window.focus) {
            newwindow.focus()
        }
        return false;
    }
</script>