@if(!empty($photo))	
    @php
        $created_at = strtotime($photo->created_at);
        $curDatetime = time();
        $difference = $curDatetime - $created_at;
        
    @endphp
    <div class="cb-content videolist">
        <div class="col-lg-12 videoitem">
            <div class="h-video">
                <div class="col-lg-1 col-sm-1 col-xs-12" style="padding:0">
                    
                    <a href="{{ to_story($photo->slug) }}" target="_blank" style="width:100%">
                        {{ HTML::image('public/img/story_thumb.jpg') }}
                    </a>
                </div>

                <div class="col-lg-3 col-sm-4 col-xs-12">
                    <div class="new_right">
                        <div class="v-desc">
                            <a href="{{ to_story($photo->slug) }}" target="_blank">{{$photo->title}}</a>
                        </div>
                        <div class="v-views" style="font-size:12px;padding:0px;">
                            {{ $photo->total_views_count }} views. {{ round($difference / 86400) }} years ago
                        </div>

                        <div class="acide-panel search_page_like">
                            <a href="{{ to_story($photo->slug) }}" target="_blank" style="font-size:12px;line-height:25px;"><i
                                    class="fa fa-thumbs-up" data-toggle="tooltip"
                                    data-placement="top" title=""
                                    style="margin-right: 5px;"
                                    data-original-title="Liked"></i> {{ $photo->likes->count() }} like</a>
                            <a href="{{ to_story($photo->slug) }}" target="_blank" class="dislk"
                                style="font-size:12px;line-height:25px;"><i
                                    class="fa fa-thumbs-down" data-toggle="tooltip"
                                    data-placement="top" title=""
                                    style="margin-right: 5px;"
                                    data-original-title="Unlinked"></i> {{ $photo->story_dislikes_count }} Dislike</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                    <div class="col-lg-2 col-sm-3 col-xs-12">
                    <div class="v-desc">
                        <a> Total Traffic</a>
                    </div>
                    <div class="v-views" style="font-size:12px;">
                           22
                    </div>
                </div>
                  
                      
                <div class="col-lg-2 col-sm-3 col-xs-12">
                    <div class="v-desc">
                        <a> Earning</a>
                    </div>
                    <div class="v-views" style="font-size:12px;">
                        Rs: 0/-
                    </div>
                </div>
                
                
                <div class="col-lg-2 col-sm-3 col-xs-12">
                    <div class="v-desc">
                        <a>Total Earning</a>
                    </div>
                    <div class="v-views" style="font-size:12px;">
                        Rs: 0/-
                    </div>
                </div>
                
                
                
                
                            <div class="col-lg-1 col-sm-3 col-xs-12" style="padding:0px">
                    <a class="view_graph" id="{{ $photo->id}}"  style="line-height: 30px;">View Graph</a>
                </div>
                <div class="h-divider"></div>
                <Br>
            </div>
        </div>
        <div class="clear"></div>

        <div class="show_graph" id="show_graph_{{ $photo->id}}" style="display:none">
            <div class="v-desc"> <a> Most Viewed In:</a> </div>
            <div id="chart-{{ $photo->id}}" style="height: 370px; width: 100%;"></div>
        </div>

    </div>
    <div class="clear"></div>

@endif