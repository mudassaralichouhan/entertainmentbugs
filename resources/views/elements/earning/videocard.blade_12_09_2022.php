@if(!empty($video))	
    @php
        $videoUrl = $video->video_key;
       

        $created_at = strtotime($video->created_at);
		$curDatetime = time();
		$difference = $curDatetime - $created_at;
		
		$totalWatchTime = 0;
		if(!empty($video->watchTime->watchTime)){
			//$totalWatchTime += gmdate('H', $video->totalViews[0]->watchTime);
			$totalWatchTime +=  $video->totalViews[0]->watchTime;
        }
        $totalWatchTime = round($totalWatchTime, 3);
        
    @endphp
    <?php


//            echo date('YYYY-MM-DD HH:mm:ss',1483381800000);
//            exit();
    $thumbBasePath='';

    $next_year = strtotime('+1 month');
    $current_time = time();

    $dataPoints=[];
    while($current_time < $next_year){
        $current_time =strtotime('+1 day', $current_time);
        $nextCurrent  = strtotime('+1 day', $current_time);

        $from= date('Y-m-d', $current_time);
        $to=date('Y-m-d', $nextCurrent);

        $videoData =  \App\Models\Video::where('id', '=', $video->id)
            ->whereBetween('created_at', [$from, $to])
            ->select('id','video_key','title','video_thumb_id','video_time', 'user_id', 'created_at')
            ->orderBy('created_at','desc')
            ->with(['totalViews' => function($query){
                $query->select('video_id','watchTime')->where('user_id', Session::get('user_id'));
            }])
            ->withCount(['totalViews'])
            ->with(['totalViews' => function($time){
                $time->sum('watchTime');
            }])->get();

        $datanew=DateTime::createFromFormat('Y-m-d', date($from))->format('U')*1000;

        $view=0;
         if (count($videoData)>0){
             $view=$videoData->totalViews;
         }

        $dataPoints[]= array("x" =>$datanew , "y" => $view);
    }


    ?>
    @if(!empty($video->thumb->thumb))
        @php
            $thumbBasePath = VIDEO_THUMB_UPLOAD_PATH.$video->thumb->thumb;
            $thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$video->thumb->thumb;
        @endphp
    @endif
    <div class="cb-content videolist">
        <div class="col-lg-12 videoitem">
            <div class="h-video">
                <div class="col-lg-1 col-sm-1 col-xs-12" style="padding:0">
                    
                        <a href="{{URL::to('watch/'.$videoUrl)}}" target="_blank" style="width:100%">
                            @if(file_exists($thumbBasePath))
                                {{HTML::image($thumbUrlPath)}}
                            @else
                                {{HTML::image('public/img/video1-1.png')}}
                            @endif
                        </a>
                </div>

                <div class="col-lg-4 col-sm-4 col-xs-12">
                    <div class="new_right">
                        <div class="v-desc">
                            <a href="{{URL::to('watch/'.$videoUrl)}}"  target="_blank">{{$video->title}}</a>
                        </div>
                        <div class="v-views" style="font-size:12px;padding:0px;">
                            {{ $video->total_views_count }} views. {{ round($difference / 86400) }} years ago
                        </div>

                        <div class="acide-panel search_page_like">
                            <a href="{{URL::to('watch/'.$videoUrl)}}"  target="_blank" style="font-size:12px;line-height:25px;"><i
                                    class="fa fa-thumbs-up" data-toggle="tooltip"
                                    data-placement="top" title=""
                                    style="margin-right: 5px;"
                                    data-original-title="Liked"></i> {{ $video->video_likes_count }} like</a>
                            <a href="{{URL::to('watch/'.$videoUrl)}}" target="_blank" class="dislk"
                                style="font-size:12px;line-height:25px;"><i
                                    class="fa fa-thumbs-down" data-toggle="tooltip"
                                    data-placement="top" title=""
                                    style="margin-right: 5px;"
                                    data-original-title="Unlinked"></i> {{ $video->video_dislikes_count }} Dislike</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-3 col-xs-12">
                    <div class="v-desc">
                        <a>Total Hours Viewed</a>
                    </div>
                    <div class="v-views" style="font-size:12px;">
                        {{ $totalWatchTime }} Hours
                    </div>
                </div>
                <div class="col-lg-2 col-sm-3 col-xs-12">
                    <div class="v-desc">
                        <a>Total Earning</a>
                    </div>
                    <div class="v-views" style="font-size:12px;">
                        Rs: 0/-
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-xs-12">
                    <a class="view_graph" id="{{ $video->id}}">View Graph</a>
                </div>
                <div class="h-divider"></div>
                <Br>
            </div>
        </div>
        <div class="clear"></div>

        <div class="show_graph" id="show_graph_{{ $video->id}}" style="display:none">
            <div id="chartContainer-{{ $video->id}}" style="height: 370px; width: 100%;"></div>
            <div style="display: none" id="show_graph_data_{{ $video->id}}"><?php echo json_encode($dataPoints);?></div>
        </div>

    </div>
    <div class="clear"></div>

@endif