@if (!empty($story))
	<div class="cb-content videolist">
		<div class="col-lg-12 videoitem">
			<div class="h-video">
				<div class="col-lg-1 col-sm-1 col-xs-12" style="padding:0">
					<a href="{{ $story->getURL() }}" target="_blank" style="width:100%">
						{{ HTML::image($story->getThumbnailURL()) }}
					</a>
				</div>
				<div class="col-lg-3 col-sm-4 col-xs-12">
					<div class="new_right">
						<div class="v-desc">
							<a href="{{ $story->getURL() }}"
								target="_blank">{{ $story->title }}</a>
						</div>
						<div class="v-views" style="font-size:12px;padding:0px;">
							{{ $story->getTimesAgo() }}
						</div>
						<div class="acide-panel search_page_like">
							<a href="{{ $story->getURL() }}" target="_blank" style="font-size:12px;line-height:25px;">
								<i class="fa fa-thumbs-up" data-toggle="tooltip" data-placement="top" title="" style="margin-right: 5px;" data-original-title="Liked"></i> {{ $story->story_likes_count }} like
							</a>
							<a href="{{ $story->getURL() }}" target="_blank" class="dislk" style="font-size:12px;line-height:25px;">
								<i class="fa fa-thumbs-down" data-toggle="tooltip" data-placement="top" title="" style="margin-right: 5px;" data-original-title="Unlinked"></i> {{ $story->story_dislikes_count }} Dislike
							</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-lg-2 col-sm-3 col-xs-12">
					<div class="v-desc">
						<a>Current Reaction's</a>
					</div>
					<div class="v-views" style="font-size:12px;">
						{{ $story->current_reaction_count }}
					</div>
				</div>
				<div class="col-lg-2 col-sm-3 col-xs-12">
					<div class="v-desc">
						<a> Earning</a>
					</div>
					<div class="v-views" style="font-size:12px;">
						Rs: {{ $story->getUnpaidReactionEarning() }}/-
					</div>
				</div>
				<div class="col-lg-2 col-sm-3 col-xs-12">
					<div class="v-desc">
						<a>Total Earning</a>
					</div>
					<div class="v-views" style="font-size:12px;">
						Rs: {{ $story->getTotalReactionEarning() }}/-
					</div>
				</div>

				<div class="col-lg-1 col-sm-3 col-xs-12" style="padding:0px">
					<a class="view_graph" id="{{ $story->id }}" data-chart="#show_graph_stories_{{ $story->id }}" style="line-height: 30px;">View Graph</a>
				</div>
				<div class="h-divider"></div>
				<Br>
			</div>
		</div>
		<div class="clear"></div>
		<div class="show_graph_period" style="display: none">
			<div style="margin: 8px 0">
				<select class="graph_period">
					<option value="7">Last 7 days</option>
					<option value="30">Last 30 days</option>
				</select>
			</div>
		</div>
		<div class="show_graph" id="show_graph_stories_{{ $story->id }}" style="display:none; background-color: white; border: 1px solide #f1f1f1">
			{!! $story->getGraphElement() !!}
		</div>

	</div>
	<div class="clear"></div>

@endif