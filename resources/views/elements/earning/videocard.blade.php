@if(!empty($video))
    <style>
        .v-desc a {
            font-size: 12px;
        }
    </style>
    <div class="cb-content videolist">
        <div class="col-lg-12 videoitem">
            <div class="h-video">
                <div class="col-lg-1 col-sm-1 col-xs-12" style="padding:0">
                    <a href="{{ $video->getURL() }}" target="_blank" style="width:100%">
                        {{ HTML::image($video->getThumbnailURL()) }}
                    </a>
                </div>

                <div class="col-lg-2 col-sm-3 col-xs-12">
                    <div class="new_right">
                        <div class="v-desc">
                            <a href="{{ $video->getURL() }}" target="_blank">{{ $video->title }}</a>
                        </div>
                        <div class="v-views" style="font-size:12px;padding:0px;">
                            {{ $video->totalHistory->count() }} views. {{ $video->getTimesAgo() }}
                        </div>
                        <div class="acide-panel search_page_like">
                            <a href="{{ $video->getURL() }}" target="_blank" style="font-size:12px;line-height:25px;">
                                <i class="fa fa-thumbs-up" data-toggle="tooltip" data-placement="top"
                                   style="margin-right: 5px;" data-original-title="Liked"></i> {{ $video->likes() }}
                                like
                            </a>
                            <a href="{{ $video->getURL() }}" target="_blank" class="dislk"
                               style="font-size:12px;line-height:25px;">
                                <i class="fa fa-thumbs-down" data-toggle="tooltip" data-placement="top" title=""
                                   style="margin-right: 5px;"
                                   data-original-title="Unlinked"></i> {{ $video->dislikes() }} Dislike
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-3 col-xs-12">
                    <div class="v-desc">
                        <a>Unpaid Hours Viewed</a>
                    </div>
                    <div class="v-views" style="font-size:12px;">
                        {{ $video->track->getUnpaidWatchTime() }} Hours
                    </div>
                </div>
                <div class="col-lg-2 col-sm-3 col-xs-12">
                    <div class="v-desc">
                        <a>Total Hours Viewed</a>
                    </div>
                    <div class="v-views" style="font-size:12px;">
                        {{ $video->track->getTotalWatchTime() }} Hours
                    </div>
                </div>

                <div class="col-lg-1 col-sm-3 col-xs-12">
                    <div class="v-desc">
                        <a>Unpaid Earning</a>
                    </div>
                    <div class="v-views" style="font-size:12px;">
                        Rs: {{ $video->track->getUnpaidEarning() }}/-
                    </div>
                </div>

                <div class="col-lg-2 col-sm-3 col-xs-12">
                    <div class="v-desc">
                        <a>Total Earning</a>
                    </div>
                    <div class="v-views" style="font-size:12px;">
                        Rs: {{ $video->track->getTotalEarning() }}/-
                    </div>
                </div>
                <div class="col-lg-1 col-sm-3 col-xs-12" style="padding:0px">
                    <a class="view_graph"
                       data-chart="#show_graph_{{ $video->isRewardVideo ? "reward_videos" : "videos" }}_{{ $video->id }}"
                       id="{{ $video->id}}" style="line-height: 30px;">View Graph</a>
                </div>
                <div class="h-divider"></div>
                <Br>
            </div>
        </div>
        <div class="clear"></div>
        <div class="show_graph_period" style="display: none">
            <div style="margin: 8px 0">
                <select class="graph_period">
                    <option value="7">Last 7 days</option>
                    <option value="30">Last 30 days</option>
                </select>
            </div>
        </div>
        <div class="show_graph"
             id="show_graph_{{ $video->isRewardVideo ? "reward_videos" : "videos" }}_{{ $video->id }}"
             style="display:none; background-color: white; border: 1px solide #f1f1f1">
            {!! $video->getGraphElement() !!}
        </div>
    </div>
    <div class="clear"></div>

@endif