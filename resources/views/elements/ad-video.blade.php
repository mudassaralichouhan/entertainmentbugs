<div id="ads-video-60" style="background:#fff;margin-bottom:15px">
    <div id="ads-video-img" style="width: 40%;float:left;">
        <a href=""data-ad-button="{{ $ad->website_link }}" target="_blank">
            <video controls loop autoplay muted style="width:100%; height:auto!important" poster="{{ $ad->thumb }}" preload="none">
                <source src="{{ asset("public/ads/videos/$ad->file") }}" type="video/mp4" />
                <source src="{{ asset("public/ads/videos/$ad->file") }}" type="video/webm" />
                <source src="{{ asset("public/ads/videos/$ad->file") }}" type="video/ogg" />
            </video>
        </a>
    </div>
    <div id="ads-video-img-content" style="width: 58%;float:right;padding:8px 4px 0 4px;">
        <h6 style="font-size: 12px!important; margin: 0px!important;padding: 2px 5px 7px 0 !important;font-family: arial;font-weight: bold;line-height: 13px;">
            {{ $ad->title }}
        </h6>
        <p style="font-size: 10.5px;line-height: 12px;padding-right: 10px;margin: 0 0 8px 0;font-family: arial;">
            {{ $ad->description }}
        </p>
        <div class="ads-video-tag">
            <!--<span style="border:1px solid  #edecec;padding: 5px 6px 1px 6px;font-size: 10px;margin-bottom: 7px;display: inline-block;">{{ $ad->categories }}</span>-->
            <div class="clearfix"></div>
            <span style="background: #edecec;padding: 2px 6px 0px 6px; font-size: 10px; margin-right:2px;display:none">{{ $ad->language }}</span>
            <span style="background: #edecec;padding: 2px 6px 0px 6px; font-size: 10px; margin-right:2px;">{{ $ad->tags }}</span>
            <small id="ads_name" style="border:1px solid #edecec;border-radius:5px;position: absolute;top: 15px;font-size: 10px;padding: 5px 13px 0px 13px;background: #edecec;right: 13px;">Ad</small>
            <a href="" class="ne21" target="_blank" data-ad-button="{{ $ad->website_link }}" style="float: right;font-size: 11px;color: #f72e5e;margin: 5px 5px 0 0;">Shop Now <i class="fa fa-external-link" aria-hidden="true"></i></a>
        
        </div>
    </div>
    <div class="clearfix"></div>
</div>