@php
    use Carbon\Carbon;
    $carbon = new Carbon();
    $days = $carbon->now()->subDay(3)->format("Y-m-d 00:00:00");
    $tempvideo = DB::table('temp_files')->where(['user_id'=>session()->get('user_id')])->latest()->first();
@endphp

@if($tempvideo && ($tempvideo->updated_at >= $days || $tempvideo->created_at >= $days) && !request()->routeIs('complatevideo'))
    <div class="uploaded_process" style="display:block;">
        <img src="{{asset('public/'.$tempvideo->thumb_path)}}" alt=""/>
        <h2>Your video is uploaded now. <i class="fa fa-check" aria-hidden="true" style="color:#fff !important"></i>
        </h2>
        <a href="" class="cross_btn_uploaded"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
        <p>Just complete the next step to make the video live successfully.</p>
        <a href="{{'complatevideo/'.$tempvideo->id}}" class="complete-pr">Complete Process</a>
        <a href="{{url('deletetemp',$tempvideo->id)}}" class="complete-pr">Delete Now</a>
    </div>
@endif

<link href="/public/css/header.css" rel="stylesheet" type="text/css">

<div style="position:fixed" id="bottom-menu">
    <a href="{{URL::to('/')}}" style="padding: 8.5px 0 3px 0;">
        <i class="fa fa-home" title="Home" style="font-size: 24px;"></i>
        <span style="margin-top: -5.9px;">Home</span>
    </a>
    <a href="{{URL::to('photos')}}" style="font-size:20px;margin-top: 1px;">
        <i class="fa fa-camera" title="Video"></i>
        <span style="
	margin-top: -4.7px;
">Photos</span>
    </a>
    @if(!empty(Session::get('user_id')))
        @if(empty(Session::get('profile_pic')))
            <a href="{{URL::to('videos')}}" style="font-size:24px;margin-top: -2px;">
                <i class="fa fa-play-circle" title="Video"></i>
                <span style="margin-top: -7.6px;">Video</span>
            </a>
        @else
            <a href="{{URL::to('videos')}}" style="font-size:24px;margin-top: -2px;">
                <i class="fa fa-play-circle" title="Video"></i>
                <span style="margin-top: -7.6px;">Video</span>
            </a>
        @endif
    @else
        <a href="{{URL::to('login')}}" style="font-size:24px;margin-top: -2px;">
            <i class="fa fa-play-circle" title="Video"></i>
            <span style="margin-top: -7.6px;">Video</span>
        </a>
    @endif <a href="{{URL::to('stories')}}">
        <i class="cv cvicon-cv-view-stats" title="Video"></i>
        <span>Stories</span>
    </a>
    @if(!empty(Session::get('user_id')))
        @if(empty(Session::get('profile_pic')))
            <a href="{{URL::to('artist-profile')}}" style="margin-top: -4px;">
                <i style="font-size:22px;" class="fas fa-theater-masks" title="Artist"></i>
                <span style="margin-top: -5.6px;">Artist</span>
            </a>
        @else
            <a href="{{URL::to('artist-profile')}}" style="margin-top: -4px;">
                <i style="font-size:22px;" class="fas fa-theater-masks" title="Artist"></i>
                <span style="margin-top: -5.6px;">Artist</span>
            </a>
        @endif
    @else
        <a href="{{URL::to('artist')}}" style="margin-top: -4px;">
            <i style="font-size:22px;" class="fas fa-theater-masks" title="Artist"></i>
            <span style="margin-top: -5.6px;">Artist</span>
        </a>
    @endif @if(!empty(Session::get('user_id')))
        @if(empty(Session::get('profile_pic')))
            <a href="{{URL::to('profile')}}"> @php $words = explode(' ', Session::get('user_name')); echo ' <div class="shortname">'. strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1)).'</div> ';
                @endphp
                <span> @php $nameLen = strlen(session()->get('user_name')); @endphp {{($nameLen > 6) ? substr(session()->get('user_name'), 0, 6).'...':session()->get('user_name')}}</span>
            </a>
        @else
            <a href="{{URL::to('profile')}}" class="" style="border:0px;">
                <img src="https://www.entertainmentbugs.com/public/uploads/users/small/{{Session::get('profile_pic')}}"
                     alt="" style="width:25px; margin-top:-1px;border-radius:100px;" style="margin-top: 1px;">
                <span style="width: 90%;margin: -2px auto 0;white-space: nowrap;text-overflow: ellipsis;overflow: hidden;">{{Session ::get('user_name')}}</span>
            </a>
        @endif
    @else
        <a href="{{URL::to('login')}}" style="border:0px;margin:margin-top: -1px;;">
            <i class="fa fa-sign-in" title="Video"></i>
            <span>Login</span>
        </a>
    @endif
</div>

<div class="header" id="myHeader">
    <div class="container-fluid" id="mbl-fix1">
        <div class="row">
            <div class="navbar-container">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-sm-3 col-xs-5" id="logo">
                            <a class="navbar-brand" href="{{URL::to('/')}}">
                                {{HTML::image('public/img/entertainment_bugs_logo.png', SITE_TITLE, ['class' => 'logo', 'style' => 'width: 89%;margin-top: 1px;'])}}
                            </a>
                        </div> @if(session()->has('user_id'))
                            <div class="col-lg-3 col-sm-3 hidden-xs header_small_menu">
                                <ul class="list-inline menu">
                                    <li class="active0">
                                        <a href="{{URL::to('')}}">Home</a>
                                    </li>
                                    /
                                    <li class="active">
                                        <a href="{{URL::to('videos')}}">Videos</a>
                                    </li>
                                    /
                                    <li class="active4">
                                        <a href="{{URL::to('photos')}}" target=""> Photos</a>
                                    </li>
                                    /
                                    <li class="active1">
                                        <a href="{{URL::to('stories')}}">Stories</a>
                                    </li>
                                    /
                                    <li class="active3">
                                        <a href="{{URL::to('all-top-entertainer')}}">Top Entertainer</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-4 col-sm-4 col-xs-2" id="search_mob">
                                <form action="{{URL::to('search')}}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="topsearch">
                                        <i class="cv cvicon-cv-cancel topsearch-close"></i>
                                        <div class="input-group">
                                          <span class="input-group-addon" id="sizing-addon2">
                                            <i class="fa fa-search"></i>
                                          </span>
                                            <input type="text" class="form-control"
                                                   placeholder="Search by: Title, Tags and Category" name="searchdata"
                                                   aria-describedby="sizing-addon2">
                                            <input type="hidden" name="searchType" id="searchType" value="video"/>
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default dropdown-toggle"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                    <i class="cv cvicon-cv-video-file" id="iconType"></i>&nbsp;&nbsp;&nbsp;
                                                    <span class="caret"></span>
                                                </button>
                                                /
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="#" id="videoType">
                                                            <i class="cv cvicon-cv-upload-video"></i> Video </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" id="storyType">
                                                            <i class="cv cvicon-cv-view-stats"></i> Stories </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- /btn-group -->
                                        </div>
                                    </div>
                                    <a href="#" class="btn-menu-toggle" id="after-login-menu"
                                       style="border:0px;margin:0px;">
                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                    </a>
                                </form>
                                <script>
                                    $('#videoType').click(function () {
                                        $('#searchType').val('video');
                                        $("#iconType").removeClass("cvicon-cv-view-stats");
                                        $("#iconType").addClass("cvicon-cv-video-file");
                                    });
                                    $('#storyType').click(function () {
                                        $('#searchType').val('story');
                                        $("#iconType").removeClass("cvicon-cv-video-file");
                                        $("#iconType").addClass("cvicon-cv-view-stats");
                                    });
                                    var APP_URL = "{{ url('/') }}";
                                    var userID = "{{ Session::get('user_id') }}";
                                    var userName = "{{ Session::get('user_name') }}";

                                    function UrlExists(url) {
                                        var http = new XMLHttpRequest();
                                        http.open('HEAD', url, false);
                                        http.send();
                                        return http.status != 404;
                                    }
                                </script>
                            </div>
                            <div class="col-lg-1 col-sm-1 col-xs-3" id="chat_noti">
                                <a href="javaScript:void(0)" class="chat-message" style="">
                                    <i class="fas fa-comments" aria-hidden="true"></i>
                                    <span id="chat_count" class="chat_count">0</span>
                                </a>
                                <div id="chat_show">
                                    <div class="notification_main_all">
                                        <a class="notification_main_img1" href="#">
                                            <img src="http://placekitten.com/150/150">
                                            <div class="media-body_notifcation1">
                                                <h4 class="media-heading">Rohit Jaiswal</h4>
                                                <p>you: The Whole Nine Yards consectetur adipiscing elit. consectetur
                                                    adipiscing elit. </p>
                                            </div>
                                        </a>
                                        <a class="notification_main_img1" href="#">
                                            <img src="http://placekitten.com/150/150">
                                            <div class="media-body_notifcation1">
                                                <h4 class="media-heading">Rohit Jaiswal</h4>
                                                <p>The Whole Nine Yards consectetur adipiscing elit. consectetur
                                                    adipiscing elit. </p>
                                            </div>
                                        </a>
                                        <a class="notification_main_img" href="{{URL::to('messages')}}"
                                           style="padding:4px 0 !important;background: #f4f3f3;border-radius: 0px;">
                                            <h4 class="media-heading" style="text-align:center">View All Messages</h4>
                                        </a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                @php
                                    $user = \App\Models\User::find(Session::get('user_id'));

                                    $all_notices = $user->notifications()
                                        ->orderBy('created_at', 'desc')
                                        ->limit(10)
                                        ->get();
                                @endphp
                                <a href="javaScript:void(0)" class="notification_icon">
                                    <i class="fa fa fa-bell" aria-hidden="true"></i>
                                    <span id="notifications_count">{{ $user->total_unread_notifications }}</span>
                                </a>

                                <div id="notification_show">
                                    <div class="notification_main">
                                        <ul class="list-unstyled panel-heading">
                                            @include('elements.notifications', ['notifications' => $all_notices])
                                        </ul>
                                        <a href="{{ url('/user/all-notifications') }}"
                                           style="display: block; text-align: center;">All Notifications</a>
                                    </div>
                                </div>

                                <a id="rewards-mobile" href="{{URL::to('rewards')}}"
                                   style="float: right;display:none;margin-right: -4px;margin-top: 0px;font-size: 20px;border-radius: 0px; width: auto;color: #f72e5e !important;margin-left: 10px;padding: 1px 6px;line-height: 17px;height: 15px;">
                                    <i class="fa fa-trophy" aria-hidden="true"></i>
                                </a>
                                <a id="rewards-mobile" class="no-atist-dis" href="{{URL::to('follow')}}"
                                   style="float: right;display:none;margin-right: -8px;margin-top: 0.5px;font-size: 19px;border-radius: 0px;width: auto;color: #f72e5e !important;margin-left: 0;padding: 1px 6px;line-height: 25px;height: 23px;">
                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="col-lg-1 col-sm-1 hidden-xs" id="name_log">
                                <div class="selectuser pull-left">
                                    <div class="btn-group pull-right dropdown">
                                        <button class="btn btn-default" onclick="openNav131()"
                                                class="btn btn-default"> @php $nameLen = strlen(session()->get('user_name')); @endphp {{($nameLen>8)?substr(session()->get('user_name'), 0, 8).'...':session()->get('user_name')}}
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="{{URL::to('profile/'.session()->get('slug'))}}">Profile</a>
                                            </li>
                                            <li>
                                                <a href="{{URL::to('videos/'.session()->get('slug'))}}">My Video</a>
                                            </li>
                                            <li>
                                                <a href="{{URL::to('upload-video')}}">Video Upload </a>
                                            </li>
                                            <li>
                                                <a href="{{URL::to('photos/create')}}">Photo Upload </a>
                                            </li>
                                            <li>
                                                <a href="{{ route('story.create') }}">Write Stories</a>
                                            </li>
                                            <li>
                                                <a href="#">Change Language</a>
                                            </li>
                                            <li>
                                                <a href="{{URL::to('edit-profile/'.session()->get('slug'))}}">Edit
                                                    Profile</a>
                                            </li>
                                            <li>
                                                <a href="{{URL::to('logout')}}">Logout</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="Sidenav11" class="sidenav">
                                <a href="javascript:void(0)" class="closebtn" onclick="closeNav131()">&times;</a>
                                <div id="new-search">
                                    <form action="{{URL::to('search')}}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="topsearch">
                                            <i class="cv cvicon-cv-cancel topsearch-close"></i>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="sizing-addon2">
                                                  <i class="fa fa-search"></i>
                                                </span>
                                                <input type="text" class="form-control" placeholder="Search: Video"
                                                       name="searchdata" aria-describedby="sizing-addon2">
                                                <input type="hidden" name="searchType" id="searchType" value="video"/>
                                                <div class="input-group-btn">
                                                    <button type="button" class="btn btn-default dropdown-toggle"
                                                            data-toggle="dropdown" aria-haspopup="true"
                                                            aria-expanded="false">
                                                        <i class="cv cvicon-cv-video-file" id="iconType"></i>&nbsp;&nbsp;&nbsp;
                                                    </button>
                                                    /
                                                </div>
                                                <!-- /btn-group -->
                                            </div>
                                        </div>
                                        <a href="#" class="btn-menu-toggle" id="after-login-menu"
                                           style="border:0px;margin:0px;">
                                            <i class="fa fa-bars" aria-hidden="true"></i>
                                        </a>
                                    </form>
                                </div>
                                <div class="sidenav1">
                                    <div style="width:31%;float:left">
                                        <h2>Entertainment</h2>
                                        <a href="{{URL::to('profile/'.session()->get('slug'))}}"><i
                                                    class="fas fa-user"></i> My Profile</a>
                                        <a href="{{URL::to('edit-profile/'.session()->get('slug'))}}">Edit Profile</a>
                                        <h2>Prize Money</h2>
                                        <a href="{{URL::to('rewards')}}">Rewards</a>
                                        <a href="{{URL::to('participate-video')}}">Reward Participate</a>
                                        <!--<a href="">Earning</a>-->
                                        <h2>Menu</h2>
                                        <a href="{{URL::to('dashboard')}}">Dashboard</a>
                                        <a href="{{URL::to('myhistory')}}">History</a>
                                        <a href="#"> Information <a href="#">Location & Language</a>
                                            <a href="#">Privacy Setting</a>
                                            <a href="{{URL::to('create-new-ads-campaign')}}"
                                               style="background: #f72e5e;color: #fff;padding: 3px 0 0 0;width: 145px;text-align: center;line-height: 29px;font-size: 12px;margin-top: 7px;">Create
                                                ADS Campaign
                                            </a>
                                    </div>
                                    <div style="width:30%;float:left">
                                        <h2>Earning</h2>
                                        <a href="{{URL::to('total-earning')}}">Total Earning</a>
                                        <a href="{{URL::to('payment-withdraw')}}">Payment Withdraw</a>

                                        <h2>Post Promote </h2>
                                        <a href="{{URL::to('promote-recent-post')}}"><i class="fa fa-rocket"></i>
                                            Promote Recent Post</a>
                                        <a href="{{URL::to('promoted-posts')}}">Promoted Posts Lists</a>


                                        <h2>Promote Ads</h2>
                                        <a href="{{URL::to('ads-overview')}}">Overview</a>
                                        <a href="{{URL::to('active-ads-campaign')}}">Active Ads Campaign</a>
                                        <a href="{{URL::to('create-new-ads-campaign')}}">Create new ads</a>
                                    </div>
                                    <div style="width:38%;float:left">
                                            <?php
                                            $userid = session()->get('user_id');
                                        if ($userid != ''){
                                            $artist_about = \App\Models\ArtistAbout::where('user_id', $userid)->first();
                                        if ($artist_about){
                                            ?>
                                        <h2>My Artist Profile </h2>
                                        <a href="{{ $artist_about->username ? url('artist/'.$artist_about->username) : "#" }}"><i
                                                    class="fas fa-theater-masks"></i> Artist profile</a>
                                        <a href="{{URL::to('create-an-artist-profile')}}">Edit Artist Profile</a>
                                        <h2>Search </h2>
                                        <a href="{{URL::to('production-houses')}}">Production House</a>
                                        <a href="{{URL::to('create-new-ads-campaign')}}">Find Auditions</a>
                                        <h2>Quick Links</h2>
                                        <a href="{{ URL::to('artist-videos') }}">Artist Video</a>
                                        <a href="{{ URL::to('artist-gallery') }}">Artist Galley</a>
                                        <a href="{{ URL::to('artist-profile') }}">Artist Profile</a>
                                        <a href="{{URL::to('theme-plan')}}"
                                           style="background: #f72e5e;color: #fff;padding: 3px 0 0 0;width: 194px;text-align: center;line-height: 29px;font-size: 12px;margin-top: 11px;">Purchase
                                            Artist Premium Theme</a>
                                            <?php
                                        }

                                            $product_about = \App\Models\ProductionAbout::where('user_id', $userid)->get();
                                        if ($product_about && count($product_about) > 0){
                                            ?>
                                        <h2>Production </h2>
                                        <a href="{{URL::to('production-houses')}}"></a>
                                            <?php
                                            $username = session()->get('user_name');
                                            $userId = session()->get('user_id');
                                            $getIfAlready = DB::select('select * from production_about where user_id=' . $userId);
                                            ?>
                                        <a href="{{URL::to('production-house/')}}/{{ $product_about[0]->username ?? '' }}">My
                                            Production Profile</a>
                                        <a href="{{URL::to('create-an-production-house-profile')}}">Edit Production
                                            Profile</a>
                                        <h2>Auditions</h2>
                                        <a href="{{ URL::to('my-audition') }}">My Post Audition</a>
                                        <a href="{{ URL::to('post-audition') }}">Post New Audition</a>
                                        <h2>Quick Search</h2>
                                        <a href="{{URL::to('artist-profile')}}">Artist Profile</a>
                                        <a href="{{URL::to('artist-videos')}}">Artist Video</a>
                                        </a>
                                        <a href="{{URL::to('artist-gallery')}}">Artist Gallery</a>
                                        <a href="{{URL::to('production-houses')}}">Production Houses</a>
                                            <?php
                                        }
                                        } ?>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="menu-bottom">

                                        <div class="small-thumb">

                                            <a href="{{ route('photos.create') }}">
                                                <div class="upload-button1"
                                                     style="position:relative !important; background: none !important;border: 0px !important;box-shadow: none !important;">
                                                    <i class="cv cvicon-cv-upload-videos">
                                                        <img src="https://www.entertainmentbugs.com/public/img/upload-photo-main-color.png"
                                                             style=" width:50px; margin:0 auto 2px;display:block">
                                                        <span style="color:#7e7e7e;font-family:'Hind Guntur', sans-serif;font-size:12px;"> Upload Photo</span>
                                                    </i>
                                                </div>
                                            </a>
                                            <a href="{{URL::to('upload-video')}}">
                                                <div class="upload-button1"
                                                     style="position:relative !important;background: none !important;border: 0px !important;box-shadow: none !important;">
                                                    <i class="cv cvicon-cv-upload-videos">
                                                        <img src="https://www.entertainmentbugs.com/public/img/play-icons-main-color.png"
                                                             style=" width:50px;margin:0 auto 2px;display:block">
                                                        <span style="color:#7e7e7e;font-family:'Hind Guntur', sans-serif;font-size:12px;"> Upload Video</span>
                                                    </i>
                                                </div>
                                            </a>
                                            <a href="{{ route('story.create') }}">
                                                <div class="upload-button1"
                                                     style="position:relative !important;background: none !important;border: 0px !important;box-shadow: none !important;">
                                                    <i class="cv cvicon-cv-upload-videos">
                                                        <img src="https://www.entertainmentbugs.com/public/img/upload-stories-main-color.png"
                                                             style=" width:50px; margin:0 auto 2px;display:block">
                                                        <span style="color:#7e7e7e;font-family:'Hind Guntur', sans-serif;font-size:12px;"> Upload Story</span>
                                                    </i>
                                                </div>
                                            </a>
                                            <a href="{{URL::to('participate-video')}}">
                                                <div class="upload-button1"
                                                     style="position:relative !important; background: none !important;border: 0px !important;box-shadow: none !important;">
                                                    <i class="cv cvicon-cv-upload-videos">
                                                        <img src="https://www.entertainmentbugs.com/public/img/reward-icons.png"
                                                             style=" width:50px; margin:0 auto 2px;display:block">
                                                        <span style="color:#7e7e7e;font-family:'Hind Guntur', sans-serif;font-size:12px;"> Reward Video</span>
                                                    </i>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="profile-sec"
                                             style="float:left; width:300px"> @if(!empty(Session::get('user_id')))
                                                @if(empty(Session::get('profile_pic')))
                                                    <a href="{{URL::to('profile')}}"> @php $words = explode(' ', Session::get('user_name')); echo ' <div class="shortname">'. strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1)).' </div> '; @endphp
                                                        <span> @php $nameLen = strlen(session()->get('user_name')); @endphp Entertainment Profile </span>
                                                    </a>
                                                @else
                                                    <a href="{{URL::to('profile')}}" class="" style="border:0px;">
                                                        <img src="https://www.entertainmentbugs.com/public/uploads/users/small/{{Session::get('profile_pic')}}"
                                                             alt=""
                                                             style="width: 46px;margin: 3px auto  6px;border-radius:100px;display: block;"
                                                             style="margin-top: 1px;">
                                                        <span style="    color: #7e7e7e;font-family: 'Hind Guntur', sans-serif;font-size: 12px;width: 89px;display: block;text-align: center;line-height: 15px;">Entertainment Profile</span>
                                                    </a>
                                                @endif
                                            @else @endif
                                            @if($artist_about)
                                                <a href="{{ $artist_about->username ? url('artist'.$artist_about->username) : '#' }}"
                                                   class='d-flex flex-column'>
                                                    {!! get_avatar($artist_about->profile_photo, $artist_about->profile_name) !!}
                                                    <span style="color: #7e7e7e;font-family: 'Hind Guntur', sans-serif;font-size: 12px;width: 64px;display: block;text-align: center;line-height: 15px;">
                                                        Artist Profile
                                                      </span>
                                                </a>
                                            @endif

                                            @if(!empty(Session::get('user_id')) && $product_about && count($product_about) > 0)
                                                @if(empty($product_about[0]->profile_photo))
                                                    <a href="{{URL::to('production-house/'.$product_about[0]->username)}}">
                                                        <div class="shortname">{{ name_to_pic(Session::get('user_name')) }}</div>
                                                        <span> @php $nameLen = strlen(session()->get('user_name')); @endphp Production House Profile </span>
                                                    </a>
                                                @else
                                                    @php
                                                        $username = session()->get('user_name');
                                                        $userId = session()->get('user_id');
                                                    @endphp
                                                    <a href="{{URL::to('production-house/'.($product_about[0]->username ?? '')) }}">
                                                        <img src="{{ $product_about[0]->profile_photo }}" alt=""
                                                             style="width: 46px;margin: 3px auto  6px;border-radius:100px;display: block;"
                                                             style="margin-top: 1px;">
                                                        <span style="color: #7e7e7e;font-family: 'Hind Guntur', sans-serif;font-size: 12px;width: 89px;display: block;text-align: center;line-height: 15px;">
                                                            Production Profile
                                                        </span>
                                                    </a>
                                                @endif
                                            @endif
                                        </div>
                                        <div class="clear"></div>
                                        <div id="bottom-m3">
                                            <a href="{{URL::to('about-us')}}">About us</a>
                                            <a href="">Terms and Condition</a>
                                            <a href="">Privacy Policy</a>
                                            <a href="{{URL::to('contact-us')}}">Contact Us</a>
                                            <a href="{{URL::to('logout')}}">Logout</a>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        @else
                            <div class="col-lg-5 col-sm-4 col-xs-7">
                                <form action="{{URL::to('search')}}" method="post">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="topsearch">
                                        <i class="cv cvicon-cv-cancel topsearch-close"></i>
                                        <div class="input-group">
                                          <span class="input-group-addon" id="sizing-addon2">
                                            <i class="fa fa-search"></i>
                                          </span>
                                            <input type="text" class="form-control" placeholder="Search"
                                                   name="searchdata" aria-describedby="sizing-addon2">
                                            <input type="hidden" name="searchType" id="searchType" value="video"/>
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default dropdown-toggle"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                    <i class="cv cvicon-cv-video-file" id="iconType"></i>&nbsp;&nbsp;&nbsp;
                                                    <span class="caret"></span>
                                                </button>
                                                /
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a href="#" id="videoType">
                                                            <i class="cv cvicon-cv-upload-video"></i> Video </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" id="storyType">
                                                            <i class="cv cvicon-cv-view-stats"></i> Stories </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <script>
                                    $('#videoType').click(function () {
                                        $('#searchType').val('video');
                                        $("#iconType").removeClass("cvicon-cv-view-stats");
                                        $("#iconType").addClass("cvicon-cv-video-file");
                                    });
                                    $('#storyType').click(function () {
                                        $('#searchType').val('story');
                                        $("#iconType").removeClass("cvicon-cv-video-file");
                                        $("#iconType").addClass("cvicon-cv-view-stats");
                                    });
                                </script>
                            </div>
                            <div class="col-lg-3 col-sm-3 hidden-xs" id="small-wd" style="padding-right:0px">
                                <ul class="list-inline menu">
                                    <li class="active0">
                                        <a href="{{URL::to('')}}">Home</a>
                                    </li>
                                    /
                                    <li>
                                        <a href="{{URL::to('videos')}}">Videos</a>
                                    </li>
                                    /
                                    <li>
                                        <a href="{{URL::to('stories')}}">Stories</a>
                                    </li>
                                    /
                                    <li>
                                        <a href="{{URL::to('photos')}}">Photos</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-1 col-sm-2 col-sm-12 mobile_right-menu" style="padding:0px;display:none"
                                 id="mobile-register-and-login">
                                <div class="loginsignup pull-right">


                                    <a href="{{URL::to('production-house')}}"
                                       style="color: #f72e5e !important;color:#000 !important;background: #fff !important;padding:0 5px 0 0">Production</a>


                                    <a href="{{URL::to('rewards')}}"
                                       style="color: #f72e5e !important;color:#000 !important;background: #fff !important;padding:0 5px 0 0">Rewards</a>
                                    <a href="{{URL::to('register')}}">Signup </a>
                                </div>
                            </div>
                            <div class="col-lg-1 col-sm-2 col-sm-12 desktop_right-menu" style="padding:0px;"
                                 id="mobile-register-and-login">
                                <div class="loginsignup pull-right">
                                    <a href="{{URL::to('login')}}">Login </a>
                                    <span>/</span>
                                    <a href="{{URL::to('register')}}">Signup </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endif
                    </div> @if(session()->has('user_id'))
                        <div class="hidden-xs" style="">
                            <a class="right-up-btn" onclick="openNav131()" style="cursor: pointer;">
                                <div class="upload-button"
                                     style="height: 58px; top:auto; right: -1px;bottom:-3px;background: none !important;border: 0px !important;box-shadow: none !important;">
                                    <i class="fa fa-bars" aria-hidden="true"
                                       style="background: #f72e5e;color: #fff !important;margin: 0;padding: 10px;"></i>
                                </div>
                            </a>
                        </div>
                        <div class="hidden-xs" style="display:none">
                            <a class="right-up-btn" href="{{ route('story.create') }}">
                                <!--<div class="upload-button">-->
                                <!--    <i class="cv cvicon-cv-upload-video"></i>-->
                                <!--</div>-->
                                <div class="upload-button"
                                     style="top:auto; right: -1px;bottom:-3px;background: none !important;border: 0px !important;box-shadow: none !important;">
                                    <i class="cv cvicon-cv-upload-videos">
                                        <img src="https://www.entertainmentbugs.com/public/img/upload-stories-main-color.png"
                                             style=" width:50px; margin:0 auto -3px;display:block">
                                        <span style="color:#302f30;font-family:'Hind Guntur', sans-serif;font-size:10px;"> Upload Content</span>
                                    </i>
                                </div>
                            </a>
                        </div>
                        <div class="hidden-xs" style="display:none">
                            <a class="right-up-btn" href="{{URL::to('upload-video')}}">
                                <!--<div class="upload-button">-->
                                <!--    <i class="cv cvicon-cv-upload-video"></i>-->
                                <!--</div>-->
                                <div class="upload-button"
                                     style="top:auto; right: -1px;bottom:  75px;background: none !important;border: 0px !important;box-shadow: none !important;">
                                    <i class="cv cvicon-cv-upload-videos">
                                        <img src="https://www.entertainmentbugs.com/public/img/play-icons-main-color.png"
                                             style=" width:50px;margin:0 auto -3px;display:block">
                                        <span style="color:#302f30;font-family:'Hind Guntur', sans-serif;font-size:10px;"> Upload Video</span>
                                    </i>
                                </div>
                            </a>
                        </div>
                        <div class="hidden-xs" style="display:none">
                            <a class="right-up-btn" href="{{ route('photos.create') }}">
                                <!--<div class="upload-button">-->
                                <!--    <i class="cv cvicon-cv-upload-video"></i>-->
                                <!--</div>-->
                                <div class="upload-button"
                                     style="top:auto; right: -1px;bottom: 151px;background: none !important;border: 0px !important;box-shadow: none !important;">
                                    <i class="cv cvicon-cv-upload-videos">
                                        <img src="https://www.entertainmentbugs.com/public/img/upload-photo-main-color.png"
                                             style=" width:50px; margin:0 auto -3px;display:block">
                                        <span style="color:#302f30;font-family:'Hind Guntur', sans-serif;font-size:10px;"> Upload Photo</span>
                                    </i>
                                </div>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div id="spacer_blanl-mb"></div>
@if(session()->has('user_id'))
    <div class="mobile-menu">
        <div class="mobile-menu-head">
            <a href="#" class="mobile-menu-close"></a>
            <a class="navbar-brand" href="index.html">
                {{HTML::image('public/img/final-logo2.png', SITE_TITLE, ['class' => 'logo', 'style' => 'width:80%'])}}
            </a>
            <!--<a href="{{URL::to('logout')}}" style="float:right;margin-top: 9px;">Logout</a>-->
        </div>
        <div class="mobile-menu-content">
            <div class="mobile-menu-list" style="cleat:both; overflow:hidden">
                <ul style="width:45%; float:left">
                    <li>
                        <h2 style="color: #f72e5e;font-size: 14px;margin: 18px 0 4px 0px;padding: 0px;">
                            Entertainment</h2>
                    </li>
                    <li>
                        <a href="{{URL::to('profile/'.session()->get('slug'))}}">My Profile</a>
                    </li>
                    <li>
                        <a href="{{URL::to('dashboard')}}">Dashboard</a>
                    </li>
                    <li>
                        <h2 style="color: #f72e5e;font-size: 14px;margin:18px 0 4px 0px;padding: 0px;">Menu</h2>
                    </li>
                    <li>
                        <a href="{{URL::to('rewards')}}">Rewards</a>
                    </li>
                    <li>
                        <a href="{{URL::to('participate-video')}}">Reward Participate</a>
                    </li>
                    <li>
                        <a href="{{URL::to('all-top-entertainer')}}">Top Entertainers</a>
                    </li>
                    <li>
                        <h2 style="color: #f72e5e;font-size: 14px;margin:18px 0 4px 0px;padding: 0px;">Quick Links</h2>
                    <li>
                        <a href="{{URL::to('watch/watch-later')}}">Watch Later </a>
                    </li>
                    <li>
                        <a href="{{URL::to('myhistory')}}">History </a>
                    </li>
                    <li>
                        <a href="{{URL::to('profile/'.session()->get('slug'))}}">Edit Profile</a>
                    </li>
                    <li>
                        <h2 style="color: #f72e5e;font-size: 14px;margin:18px 0 4px -0px;padding: 0px;">Earning</h2>
                    </li>
                    <li>
                        <a href="{{URL::to('total-earning')}}">Total Earning</a>
                    </li>
                    <li>
                        <a href="{{URL::to('payment-withdraw')}}">Payment withdraw</a>
                    </li>
                </ul>


                <ul style="width:55%; float:left">
                        <?php
                        if (!$userid) {
                            $userid = session()->get('user_id');
                        }

                    if ($userid){
                        if (!$artist_about) {
                            $artist_about = \App\Models\ArtistAbout::where('user_id', $userid)->first();
                        }

                    if ($artist_about){
                        ?>
                    <li>
                        <h2 style="color: #f72e5e;font-size: 14px;margin:18px 0 4px -0px;padding: 0px;">My Artist
                            Profile </h2>
                    </li>
                    <li>
                        <a href="{{ $artist_about->username ? url('artist/'.$artist_about->username) : "#" }}">Artist
                            profile</a>
                    </li>
                    <li>
                        <a href="{{URL::to('create-an-artist-profile')}}">Edit Artist Profile</a>
                    </li>
                    <li>
                        <h2 style="color: #f72e5e;font-size: 14px;margin:18px 0 4px -0px;padding: 0px;">Search </h2>
                    </li>
                    <li>
                        <a href="{{URL::to('production-houses')}}">Production House</a>
                    </li>
                    <li>
                        <a href="{{URL::to('find-auditions')}}">Find Auditions</a>
                    </li>
                    <li>
                        <h2 style="color: #f72e5e;font-size: 14px;margin:18px 0 4px -0px;padding: 0px;">Quick Links</h2>
                    </li>
                    <li>
                        <a href="{{ URL::to('artist-videos') }}">Artist Video</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('artist-gallery') }}">Artist Galley</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('artist-profile') }}">Artist Profile</a>
                    </li>
                    <a href="{{URL::to('theme-plan')}}"
                       style="background: #f72e5e;color: #fff;padding: 5px 9px 2px 9px;width: 194px;text-align: center;line-height: 31px;font-size: 10px;margin-top: 11px;">Purchase
                        Artist Premium Theme</a>
                        <?php
                    }

                        if (!$product_about) {
                            $product_about = \App\Models\ProductionAbout::where('user_id', $userid)->get();
                        }

                    if ($product_about && count($product_about) > 0){
                        ?>
                    <li>
                        <h2 style="color: #f72e5e;font-size: 14px;margin:18px 0 4px -0px;padding: 0px;">Production </h2>
                            <?php
                            $username = session()->get('user_name');
                            $userId = session()->get('user_id');
                            $getIfAlready = DB::select('select * from production_about where user_id=' . $userId);
                            ?>
                        <a href="{{URL::to('production-house/')}}/{{ $product_about[0]->username ?? '' }}">My Production
                            Profile</a>
                    </li>
                    <li>
                        <a href="{{URL::to('create-an-production-house-profile')}}">Edit Production Profile</a>
                    </li>
                    <li>
                        <h2 style="color: #f72e5e;font-size: 14px;margin:18px 0 4px -0px;padding: 0px;">Auditions</h2>
                    </li>
                    <li>
                        <a href="{{ URL::to('my-audition') }}">My Post Audition</a>
                    </li>
                    <li>
                        <a href="{{ URL::to('post-audition') }}">Post New Audition</a>
                    </li>
                    <li>
                        <h2 style="color: #f72e5e;font-size: 14px;margin:18px 0 4px -0px;padding: 0px;">Quick
                            Search</h2>
                    </li>
                    <li>
                        <a href="{{URL::to('artist-profile')}}">Artist Profile</a>
                    </li>
                    <li>
                        <a href="{{URL::to('artist-videos')}}">Artist Video</a>
                        </a>
                    </li>
                    <li>
                        <a href="{{URL::to('artist-gallery')}}">Artist Gallery</a>
                    </li>
                    <li>
                        <a href="{{URL::to('production-houses')}}">Production Houses</a>
                    </li>
                        <?php
                    }
                    }
                        ?>
                    <li>
                        <h2 style="color: #f72e5e;font-size: 14px;margin:18px 0 4px -0px;padding: 0px;">Promote Ads</h2>
                    </li>
                    <li>
                        <a href="{{URL::to('ads-overview')}}">Ads Overview </a>
                    </li>
                    <li>
                        <a href="{{URL::to('create-new-ads-campaign')}}">Create Ads </a>
                    </li>
                    <li>
                        <a href="{{URL::to('logout')}}">Logout</a>
                    </li>
                </ul>
            </div>
            <div class="menu-bottom">
                <div class="small-thumb">

                    <a href="{{ route('photos.create') }}">
                        <div class="upload-button1"
                             style="position:relative !important; background: none !important;border: 0px !important;box-shadow: none !important;">
                            <i class="cv cvicon-cv-upload-videos">
                                <img src="https://www.entertainmentbugs.com/public/img/upload-photo-main-color.png"
                                     style=" width:50px; margin:0 auto 2px;display:block">
                                <span style="color:#7e7e7e;font-family:'Hind Guntur', sans-serif;font-size:12px;"> Upload Photo</span>
                            </i>
                        </div>
                    </a>
                    <a href="{{URL::to('upload-video')}}">
                        <div class="upload-button1"
                             style="position:relative !important;background: none !important;border: 0px !important;box-shadow: none !important;">
                            <i class="cv cvicon-cv-upload-videos">
                                <img src="https://www.entertainmentbugs.com/public/img/play-icons-main-color.png"
                                     style=" width:50px;margin:0 auto 2px;display:block">
                                <span style="color:#7e7e7e;font-family:'Hind Guntur', sans-serif;font-size:12px;"> Upload Video</span>
                            </i>
                        </div>
                    </a>
                    <a href="{{ route('story.create') }}">
                        <div class="upload-button1"
                             style="position:relative !important;background: none !important;border: 0px !important;box-shadow: none !important;">
                            <i class="cv cvicon-cv-upload-videos">
                                <img src="https://www.entertainmentbugs.com/public/img/upload-stories-main-color.png"
                                     style=" width:50px; margin:0 auto 2px;display:block">
                                <span style="color:#7e7e7e;font-family:'Hind Guntur', sans-serif;font-size:12px;"> Upload Story</span>
                            </i>
                        </div>
                    </a>
                    <a href="{{URL::to('participate-video')}}">
                        <div class="upload-button1"
                             style="position:relative !important; background: none !important;border: 0px !important;box-shadow: none !important;">
                            <i class="cv cvicon-cv-upload-videos">
                                <img src="https://www.entertainmentbugs.com/public/img/reward-icons.png"
                                     style=" width:50px; margin:0 auto 2px;display:block">
                                <span style="color:#7e7e7e;font-family:'Hind Guntur', sans-serif;font-size:12px;"> Reward Video</span>
                            </i>
                        </div>
                    </a>
                </div>
                <div class="clear"></div>
            </div>
            <!--<a href="{{URL::to('logout')}}" class="btn mobile-menu-logout">Log out</a>-->
        </div>
    </div>
@endif

<div class="container-fluid">
    <div class="row">
        <div class="navbar-container2">
            <div class="container">
                <div class="rows">
                    <div class="col-lg-6 col-sm-6 col-xs-12 no_pad">
                        <div class="h-resume" style="padding:3px 0 5px 10px">
                            <div class="play-icon" style="position:relative; left:0px">
                                <div class="all-s">
                                    <a class="casting_active" href="{{URL::to('casting')}}"
                                       style="float: left;font-size: 22px;    margin-top: -4px;">
                                        <i class="fa fa-home"></i>
                                        <span class="sml" style="font-size: 12px;margin: 4px 0 0 0px;"> Casting</span>
                                    </a>
                                    <a href="{{URL::to('artist')}}"
                                       style="font-size: 16px;margin: 0px 0 0 14px;display: block;float: left;"
                                       class="pr-artist">
                                        <i class="fa fa-users"></i>
                                        <span class="sml" style="font-size: 12px;margin: 4px 0 0 2px;"> Artists</span>
                                    </a>
                                    <a href="{{URL::to('production-house')}}" class="pr-act"
                                       style="float: left;font-size: 17px;margin-left: 15px;margin-top: 0px;">
                                        <i class=" fa fa-film" style="float: left;"></i>
                                        <span class="sml pr-act"
                                              style="font-size: 12px;margin: 4px 0 0 5px;display: block;float: left;">Production House
                                    </a>
                                    <a class="find_audtion_active" href="{{URL::to('find-auditions')}}"
                                       style="float: left;font-size: 25px;margin-left: 14px;margin-top: -2px;">
                                        <i class="fa fa-audio-description" style="float: left;"></i>
                                        <span class="sml" style="font-size: 12px;margin: 6px 0 0 8px;float: left;"> Find Auditions</span>
                                    </a>
                                </div>
                                <?php
                                $userid = session()->get('user_id');
                                if ($userid != ''){
                                    $artist_about = \App\Models\ArtistAbout::where('user_id', $userid)->first();
                                    $progress = 0;
                                if ($artist_about){
                                    ?>
                                <a href="{{URL::to('casting')}}"
                                   style="float: left;font-size: 22px;    margin-top: -4px;" class="casting_active">
                                    <i class="fa fa-home"></i>
                                    <span class="sml" style="font-size: 12px;margin: 4px 0 0 0px;"> Casting</span>
                                </a>
                                <a href="{{URL::to('production-houses')}}" class="pr-act"
                                   style="float: left;font-size: 17px;margin-left: 15px;margin-top: 0px;">
                                    <i class=" fa fa-film" style="float: left;"></i>
                                    <span class="sml pr-act"
                                          style="font-size: 12px;margin: 4px 0 0 5px;display: block;float: left;">Production House
                                </a>
                                <a class="find_audtion_active" href="{{URL::to('find-auditions')}}"
                                   style="float: left;font-size: 25px;margin-left: 14px;margin-top: -2px;">
                                    <i class="fa fa-audio-description" style="float: left;"></i>
                                    <span class="sml" style="font-size: 12px;margin: 6px 0 0 8px;float: left;"> Find Auditions</span>
                                </a>
                                <a class="all_artist_profile_active" href="{{URL::to('artist-profile')}}"
                                   style="float: left;font-size: 20px;margin-left: 14px;margin-top: -1px;">
                                    <i class="fas fa-theater-masks"
                                       style="float:left;font-size: 19px;margin-top: -2px;"></i>
                                    <span class="sml"
                                          style="font-size: 12px;margin: 5px 0 0 7px;display: block;float: left;">All Artist Profile</span>
                                </a> <?php
                                     }

                                         $product_about = \App\Models\ProductionAbout::where('user_id', $userid)->get();
                                         $progress = 0;
                                         if ($product_about && count($product_about) > 0) {
                                             $progress = 1;
                                         }
                                     if ($progress){
                                         ?>

                                <a href="{{URL::to('casting')}}"
                                   style="float: left;font-size: 22px;    margin-top: -4px;">
                                    <i class="fa fa-home"></i>
                                    <span class="sml" style="font-size: 12px;margin: 4px 0 0 0px;"> Casting</span>
                                </a>
                                <a href="{{URL::to('artist-profile')}}"
                                   style="float: left;font-size: 20px;margin-left: 14px;margin-top: -1px;">
                                    <i class="fa fa-user" style="float:left"></i>
                                    <span class="sml"
                                          style="font-size: 12px;margin: 4px 0 0 7px;display: block;float: left;"> Artist Profiles</span>
                                </a>
                                <a href="{{URL::to('artist-videos')}}"
                                   style="float: left;font-size: 20px;margin-left: 14px;margin-top: -1px;">
                                    <i class="fa fa-play-circle-o" style="float:left"></i>
                                    <span class="sml"
                                          style="font-size: 12px;margin: 4px 0 0 7px;display: block;float: left;"> Artist Videos</span>
                                </a>
                                <a href="{{URL::to('artist-gallery')}}"
                                   style="float: left;font-size: 20px;margin-left: 14px;margin-top: -1px;">
                                    <i class="fa fa-picture-o" style="float:left"></i>
                                    <span class="sml"
                                          style="font-size: 12px;margin: 4px 0 0 7px;display: block;float: left;"> Artist Gallery</span>
                                </a>
                                <a href="{{URL::to('find-auditions')}}"
                                   style="float: left;font-size: 25px;margin-left: 14px;margin-top: -2px;">
                                    <i class="fa fa-audio-description" style="float: left;"></i>
                                    <span class="sml" style="font-size: 12px;margin: 6px 0 0 8px;float: left;"> Find Auditions</span>
                                </a> <?php
                                     }
                                     }//check if user login ?>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-lg-1 col-sm-2 pull-right hidden-xs" style="padding:0px;width: 12%;">

                    <a href="{{URL::to('promote-recent-post')}}" class="promote_your_video" target="_blank"
                       style="font-size:12px;font-weight: normal;margin: 6px 0 0 0;padding: 10px 5px 6px 5px;"> Promote
                        Recent Post </span>
                        </i>
                    </a>
                </div>


                @if(!empty(Session::get('user_id')))
                    @if(empty(Session::get('profile_pic')))
                        @php $words = explode(' ', Session::get('user_name')); echo ''; @endphp @php $nameLen = strlen(session()->get('user_name')); @endphp


                                <!-- {{($nameLen > 6)?substr(session()->get('user_name'),0,6).'...':session()->get('user_name')}} -->
                    @else @endif
                @else
                    <div class="col-lg-2 col-sm-2 hidden-xs" style="float:right;width:235px;padding-right:0px">
                        <div class="h-resume" style="padding:3px 0 5px 10px">
                            <div class="play-icon" style="position:relative; left:0px">
                                <a href="{{URL::to('rewards')}}"
                                   style="font-size: 16px;margin: 0px 0 0 0;display: block;float: left;"
                                   class="pr-artist">
                                    <i class="fa fa-trophy"></i>
                                    <span class="sml" style="font-size: 12px;margin: 4px 0 0 2px;"> Rewards</span>
                                </a>
                                <a href="{{URL::to('all-top-entertainer')}}"
                                   style="font-size: 16px;margin: 0px 0 0 20px;display: block;float: left;"
                                   class="pr-artist">
                                    <i class="fa fa-user-secret"></i>
                                    <span class="sml"
                                          style="font-size: 12px;margin: 4px 0 0 2px;"> Top Entertainers</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif @if(session()->has('user_id'))
                    @include('elements.header_inner')
                @endif
                @php $countFollowerVideo = 25;
		if( session()->get('user_id') )
		{
		$getFollowers = DB::table('followers')->where('user_id',session()->get('user_id'))->pluck('follows_id')->toArray();
		$getLatestUserVideoId = App\Models\Video::orderByDesc('id')->whereIn('user_id',$getFollowers)->where('user_id','!=',session()->get('user_id'))->pluck('user_id','id')->toArray();
		$getLatestUserVideoId = array_unique($getLatestUserVideoId);
		$Data_array = $getLatestUserVideoId;
		$output_array = array();
		foreach($Data_array as $key => $a)
		{ array_push($output_array,$key);
		}
		$latestFollowerVideo = App\Models\Video::whereIn('videos.id',$output_array)->distinct()
								->select('videos.id','video_key','title','video_thumb_id','video_time', 'user_id', 'videos.created_at', 'isRewardVideo')
								->whereNull('videos.deleted_at')
								->orderBy('created_at','desc')
								->with(['thumb' => function($thumb){
									$thumb->select('id','thumb');
								}])
								->with(['user' => function($user){
									$user->select('id','name', 'photo')->where('users.isDeleted', 0);
									}])
								->holdUser()
								->withCount(['totalViews'])
								->with('watchTime')
								->take(25) ->get();
								}
								if(!empty($latestFollowerVideo)){
									$countFollowerVideo = count($latestFollowerVideo);
								if( $countFollowerVideo < 25 ){
									$countFollowerVideo=25 - $countFollowerVideo;
									}
								}
								$getLatestUserVideoId=App\Models\Video::orderByDesc('id')->pluck('user_id','id')->toArray();
								$getLatestUserVideoId = array_unique($getLatestUserVideoId);
								$Data_array = $getLatestUserVideoId; $output_array = array();
								foreach($Data_array as $key => $a) {
								    array_push($output_array,$key);
								}
									$LatestVideos = App\Models\Video::whereIn('videos.id',$output_array)
												->select('videos.id','video_key','title','video_thumb_id','video_time', 'user_id', 'videos.created_at', 'isRewardVideo')
												->with(['thumb' => function($thumb)
												{
													$thumb->select('id','thumb');
												}])
												->with(['user' => function($user)
													{
														$user->select('id','name', 'photo')->where('users.isDeleted',0);
													}])
												->holdUser()
												->withCount(['totalViews'])
												->with('watchTime')
												->take(20)
												->orderByDesc('videos.id')
												->get();
                @endphp
            </div>
        </div>
    </div>
</div>

<div id="show-desktop">
    <div class="container">
        <div class="small-thumb1">
            <a href="{{ url('photos/create') }}">
                <div class="upload-button1"
                     style="position:relative !important; background: none !important;border: 0px !important;box-shadow: none !important;">
                    <i class="cv cvicon-cv-upload-videos">
                        <img class="color-img"
                             src="https://www.entertainmentbugs.com/public/img/upload-photo-main-color.png">
                        <img class="white-img"
                             src="https://www.entertainmentbugs.com/public/img/upload-photo-main-white.png">
                        <span> Upload Photo</span>
                    </i>
                </div>
            </a>
            <a href="https://www.entertainmentbugs.com/upload-video">
                <div class="upload-button1"
                     style="position:relative !important;background: none !important;border: 0px !important;box-shadow: none !important;">
                    <i class="cv cvicon-cv-upload-videos">
                        <img class="color-img"
                             src="https://www.entertainmentbugs.com/public/img/play-icons-main-color.png">
                        <img class="white-img"
                             src="https://www.entertainmentbugs.com/public/img/play-icons-main-white.png">
                        <span> Upload Video</span>
                    </i>
                </div>
            </a>
            <a href="https://www.entertainmentbugs.com/story/create">
                <div class="upload-button1"
                     style="position:relative !important;background: none !important;border: 0px !important;box-shadow: none !important;">
                    <i class="cv cvicon-cv-upload-videos">
                        <img class="color-img"
                             src="https://www.entertainmentbugs.com/public/img/upload-stories-main-color.png">
                        <img class="white-img"
                             src="https://www.entertainmentbugs.com/public/img/upload-stories-main-white.png">
                        <span> Upload Story</span>
                    </i>
                </div>
            </a>
            <a href="https://www.entertainmentbugs.com/participate-video">
                <div class="upload-button1"
                     style="position:relative !important; background: none !important;border: 0px !important;box-shadow: none !important;">
                    <i class="cv cvicon-cv-upload-videos">
                        <img class="color-img" src="https://www.entertainmentbugs.com/public/img/reward-icons.png">
                        <img class="white-img"
                             src="https://www.entertainmentbugs.com/public/img/reward-icons-white.png">
                        <span> Reward Video</span>
                    </i>
                </div>
            </a>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div id="top-circle-new-user">
    <div class="container mobile-extra_fea" style="border-bottom:solid 1px #eceff0;">
        <div class="row" id="live-video-user-list-home">
            <div class="col-lg-12">
                <div class="content-block">
                    <div class="cb-header">
                        <div class="rows">
                            <div class="users" id="n12">
                                <strong>New Videos</strong>
                            </div>
                            <div class="users" id="only-desktop" style="display:none">
                                <!--<a href="{{URL::to('upload-video')}}">-->
                                <a href="javascript:void(0)" class="upload-options">
                                    <strong><i class="cv cvicon-cv-upload-video" style="color:#fff !Important"></i>
                                        Upload </strong>
                                </a>
                            </div>
                            <div class="span12" id="crousal_wrap">
                                <div id="owl-demo" class="owl-carousel">
                                    @php
                                        $topBar = new \stdClass;
                                        $topBar->videos = \App\Models\Video::selectRaw("'video' as tag, isRewardVideo as type, title, videos.id, video_key as slug, video_thumb_id as image, videos.created_at, user_id, videos.deleted_at as is_deleted")->leftJoin("users", "users.id", "videos.user_id")->where("users.isDeleted", 0);
                                        $topBar->photos = \App\UploadPhotos::selectRaw("'photo' as tag, '' as type, photo_title as title, upload_photos.id, upload_photos.id as slug, images as image, upload_photos.created_at, user_id, upload_photos.isDeleted as is_deleted")->leftJoin("users", "users.id", "upload_photos.user_id")->where("users.isDeleted", 0);
                                        $topBar->stories = \App\Models\Story::selectRaw("'story' as tag, '' as type, title, stories.id, slug, thumb_image as image, stories.created_at, user_id, stories.isDeleted as is_deleted")
                                          ->unionAll($topBar->videos)
                                          ->unionAll($topBar->photos);

                                        $topBarPosts = DB::table(DB::raw("({$topBar->stories->toSql()}) as x"))
                                          ->setBindings($topBar->stories->getBindings())
                                          ->orderByDesc("created_at")
                                          ->limit(28)
                                          ->get();

                                    @endphp
                                    @if(!empty($topBarPosts))
                                        @foreach($topBarPosts as $post)

                                                <?php
                                                $user = \App\Models\User::find($post->user_id);
                                                $artist_about = \App\Models\ArtistAbout::whereUserId($post->user_id)->first();

                                                //  if($post->is_deleted==1)
                                                //    continue;
                                                ?>

                                            <div class="item">
                                                <div class="users">
                                                    @if( $post->tag == 'photo' )
                                                        @php
                                                            $url = url('single-photo/'.$post->slug);
                                                            $icon = "";
                                                        @endphp
                                                    @elseif( $post->tag == 'story' )
                                                        @php
                                                            if($post->is_deleted == 1)
                                                                $url = url('/'.@$user->slug);
                                                            else
                                                                $url = url('story/display/'.$post->slug);
                                                            $icon = "";
                                                        @endphp
                                                    @elseif( $post->tag == 'video' )
                                                        @if( $post->type == 1 )
                                                            @php
                                                                $url = url('watch-reward/'.$post->slug);
                                                                $icon = "";
                                                            @endphp
                                                        @else
                                                            @php
                                                                $url = url('watch/'.$post->slug);
                                                                $icon = "";
                                                            @endphp
                                                        @endif

                                                        @php if($post->is_deleted == 1)
											$url = url('/'.@$user->slug);
										else
											$url = $url;
                                                        @endphp

                                                    @endif

                                                    @php
                                                        $user_photo = null;

                                                        if(@$user){
                                                        if($post->is_deleted==1 && $post->tag == 'photo')
                                                            $user_photo = null;
                                                        else
                                                            $user_photo = @$user->photo;
                                                    @endphp
                                                    <a href="{{ $url }}" class="user_img">
                                                        {!! get_avatar(@$user_photo, @$user->name) !!}
                                                        <small>{{ $user->name ?? '' }}</small>
                                                    </a>
                                                    @php } @endphp
                                                    <span class="green">{{ $icon }}</span>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php


if (Session::get('is_profile_completed') == true) { ?>
<div class="incomplete_process" id="incomplete_process_div">
    <h2>Incomplete Profile Process !!!</h2>
    <a href="javascript:void(0)" class="cross_btn_uploaded pending_profile_class" id="{{$user->id}}"><i
                class="fa fa-times-circle" aria-hidden="true"></i></a>
    <p>Please complete your profile it is pending now</p>
    <a href="{{route('edit.profile', $user->slug)}}" class="complete-pr">Complete Now</a>
    <a href="javascript:void(0)" class="complete-pr pending_profile_class" id="{{$user->id}}">Later</a>
</div>
    <?php
}

?>


<script>
    $(document).ready(function () {
        $(".upload-options").click(function () {
            $("#show-desktop").slideToggle();
        });
    });
</script>

<script>
    $(".chat-message").click(function () {
        $("#chat_show").toggle("100");
    });
</script>
<script>
    function openNav131() {
        document.getElementById("Sidenav11").style.width = "55%";
    }

    function closeNav131() {
        document.getElementById("Sidenav11").style.width = "0";
    }
</script>