@if(!empty($category))
    @php 
        $videoCount = DB::table('video_categories')->where('category_id',$category->id)->where('deleted_at',NULL)->distinct('video_id')->get();
    @endphp
    <div class="col-lg-2 col-xs-6 col-sm-3">
        <div class="b-category">
            <a href="{{ URL::to('category/')}}/{{$category->category}}" class="name" target=""><img src="public/img/categories/channel-1.png" alt="">
            {{ $category->category}}</a>
            <p class="desc">{{ count($videoCount) }} Videos</p>
        </div>
    </div>

@endif