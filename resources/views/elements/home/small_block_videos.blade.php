@if(!empty($video))

    @php
		$videoUrl = $video->video_key;
		$created_at = strtotime($video->created_at);
		$curDatetime = time();
		$difference = $curDatetime - $created_at;
	    $watchStatus = 0;
		if(!empty($video->watchTime->watchTime)){
			$watchStatus = ($video->watchTime->watchTime / $video->video_time) * 100;
		}

	@endphp
	
	@if(!empty($video->user->photo))
        @php
           $userBasePath = PROFILE_SMALL_UPLOAD_PATH.$video->user->photo;
		   $userUrlPath = PROFILE_SMALL_DISPLAY_PATH.$video->user->photo;
        @endphp
    @endif

    <div class="small_block videoitem">
        <div class="b-video">
            <div class="v-img">
                @if(!empty($video->thumb->thumb))
				
					@php
						$thumbBastPath = VIDEO_THUMB_UPLOAD_PATH.$video->thumb->thumb;
						$thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$video->thumb->thumb;
						
					@endphp
						@if(file_exists($thumbBastPath))
							<a href="{{URL::to('watch/'.$videoUrl)}}">
								{{HTML::image($thumbUrlPath)}}
							
								@if(!empty($video->is_watched_video_count) && $video->is_watched_video_count > 0)
									<div class="watched-mask"></div>
									<div class="watched">WATCHED</div>
								@endif
								<div class="time">{{ $video->video_time}}</div>
							</a>
						@else
							<a href="{{URL::to('watch/'.$videoUrl)}}">
								{{HTML::image('public/img/video1-1.png')}}
								@if(!empty($video->is_watched_video_count) && $video->is_watched_video_count > 0)
									<div class="watched-mask"></div>
									<div class="watched">WATCHED</div>
								@endif
								<div class="time">{{ $video->video_time}}</div>
							</a>
						@endif
			
				@endif
                <div class="time">{{ $video->video_time}}</div>
            </div>
            @if($watchStatus != '')
					<div class="sv-views-progress">
					<div class="sv-views-progress-bar" style="width:{{ $watchStatus }}%"></div>
					</div>
				@endif
            <div class="v-desc">
                <a href="{{URL::to('watch/'.$videoUrl)}}">{{$video->title}}</a>
            </div>
            <div class="v-views">
                {{ $video->total_views_count }} views.<span class="v-percent" style="display:inline-block;font-size:12px;"><span class="v-circle"></span> {{ \Carbon\Carbon::parse($video->created_at)->diffForHumans() }} </span>
            </div>
        </div>
    </div>


@endif