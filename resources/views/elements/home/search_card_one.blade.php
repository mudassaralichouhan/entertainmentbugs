@foreach ($videoData as $criteria)
    @if(!empty($criteria))

    @php
        $videoUrl = $criteria->video_key;
        $created_at = strtotime($criteria->created_at);
        $curDatetime = time();
        $difference = $curDatetime - $created_at;
    @endphp
    
    @if(!empty($criteria->thumb->thumb))
        @php
            $thumbBasePath = VIDEO_THUMB_UPLOAD_PATH.$criteria->thumb->thumb;
            $thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$criteria->thumb->thumb;
        @endphp
    @endif
   

<div class="col-lg-6 videoitem">
    <div class="h-video">
        <div class="col-lg-4 col-sm-5 col-xs-12">
            <div class="v-img">
                @if(file_exists($thumbBasePath))
                <a href="{{URL::to('watch/'.$videoUrl)}}" class="user_img">{{HTML::image($thumbUrlPath)}}</a>
                @else
                    <a href="{{URL::to('watch/'.$videoUrl)}}" class="user_img"><img src="{{url('/public/img/video1-1.png')}}" alt=""></a>
                @endif
                <div class="time">{{$criteria->video_time}}</div>
            </div>
        </div>

        <div class="col-lg-8 col-sm-5 col-xs-12" style="padding:0 5px">
            <div class="new_right">
                <div class="v-desc">
                    <a href="{{URL::to('watch/'.$videoUrl)}}">{{$criteria->title}}</a>
                </div>
                <div class="v-views">
                    {{ $criteria->total_views_count }} views. {{ round($difference / 3.6e6) }} hours ago
                </div>

                <div class="acide-panel search_page_like">

 

                    <a style="font-size:13px;line-height:25px;">
                        <i class="fa fa-thumbs-up" data-toggle="tooltip" data-placement="top"
                            title="" style="margin-right: 5px;" data-original-title="Liked"></i>
                        {{ $criteria->video_likes_count }} like</a>
                    <a class="dislk" style="font-size:13px;line-height:25px;"><i
                            class="fa fa-thumbs-down" data-toggle="tooltip" data-placement="top"
                            title="" style="margin-right: 5px;"
                            data-original-title="Unlinked"></i> {{ $criteria->video_dislikes_count }} Dislike</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="h-divider"></div>
    </div>
</div>

   <style>
.v-desc a {font-size: 15px;margin: 8px 0 0 0;display: block;}
.v-history .h-video .v-views {font-size: 13px;}
.v-history .h-video .v-views {font-size: 13px;padding: 0;margin: 1px 0 -1px 0;}
@media  only screen and (max-width: 767px) {
.col-lg-4.col-sm-5.col-xs-12{padding:0px;}
.v-desc a {margin: -4px 0 0 0;}
.v-history .h-video {padding-top: 0px;}
.search_page_main_block .divider {height: 0px;}
 }
   </style>
   
   
   
   
@endif
@endforeach