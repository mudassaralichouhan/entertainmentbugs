@if(!empty($user))

    @php
        $video_count = \App\Models\Video::where([['user_id',$user->id], ['isRewardVideo', 0]])->count();
        $followers=DB::table('followers')->where('follows_id',$user->id)->get();
        if(!empty($user->photo)){
            $userBasePath = PROFILE_SMALL_UPLOAD_PATH.$user->photo;
            $userUrlPath = PROFILE_SMALL_DISPLAY_PATH.$user->photo;
        } else {
            $userBasePath = '';
            $userUrlPath = '';
        }
        

        $created_at = strtotime($user->created_at);
        $curDatetime = time();
        $difference = $curDatetime - $created_at;
        
    @endphp
    <div class="col-lg-2 col-sm-4 col-xs-4">
        <div class="channels-card">
            <div class="channels-card-image">
                <a target="_blank" href="{{ user_url($user->slug) }}">
                    @if(file_exists($userBasePath))
                            {{HTML::image($userUrlPath,null,['class'=>'img-fluid'])}}
                    @else
                        <img class="img-fluid" src="{{url('/public/img/ava2.png')}}" alt="">
                    @endif
                </a>
                @if( Session::get('user_id') && $user->isFollowedBy() )
                    <div class="channels-card-image-btn" id="suggested_unfollow_user{{ $user->id }}">
                        <a onclick="unFollowUser({{ $user->id }})" class="btn-0flw" style="background:#f72e5e !important"> <i class="fa fa-solid fa-heart"></i> 
                            Following 
                        </a>
                    </div>
                @else
                    <div class="channels-card-image-btn" id="suggested_follow_user{{ $user->id }}">
                        <a onclick="followUser({{ $user->id }})" class="btn-0flw"> <i class="fa fa-heart-o"></i>
                        Follow
                    </div>
                @endif
            </div>
            <div class="channels-card-body">
                <div class="channels-title">
                    <a target="_blank" href="{{ user_url($user->slug) }}">{{ $user->name }}</a>
                </div>
                <div class="channels-view">
                    {{ $video_count }} videos
                </div>
            </div>
        </div>
    </div>
@endif