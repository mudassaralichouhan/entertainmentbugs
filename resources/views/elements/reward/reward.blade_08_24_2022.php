@if(!empty($video))	
	@php
		$videoUrl = $video->video_key;
	@endphp
	
	<style>
	    @media only screen and (max-width: 767px){

.cb-header .list-inline>li {
    display: inline-block;
    padding-left: 0;
    padding-right: 0;
    width: 48%;
    margin: 0 0.7%;
}
#reward_1 .videolist .videoitem .b-video .v-desc {
    height: 38px;
    overflow: hidden;
    margin-bottom: 5px;
}
#home_page_video .v-desc a {
    font-size: 12px;    line-height: 15px;}
    
}

	    
	</style>
	
	
<div class="col-lg-2 col-sm-6 videoitem"> 							 
	<div class="b-video">
		<div class="v-img" id="{{ $video->video_key}}">
			@if(!empty($video->thumb->thumb))
				
			@php
				$thumbBastPath = VIDEO_THUMB_UPLOAD_PATH.$video->thumb->thumb;
				$thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$video->thumb->thumb;
				
			@endphp
			
			@if(file_exists($thumbBastPath))
			<a href="{{URL::to('watch-reward/'.$videoUrl)}}">{{HTML::image($thumbUrlPath)}}</a>
			@else
			<a href="{{URL::to('watch-reward/'.$videoUrl)}}">{{HTML::image('public/img/video1-1.png')}}</a>	
			@endif
			
			@endif
			<div class="time">{{$video->video_time}}</div>
			
		</div>
		<div class="v-desc">
			<a href="{{URL::to('watch-reward/'.$videoUrl)}}">{{$video->title}}</a>
		</div>
		<div class="v-views">
			 {{$video->total_views_count}} views <span class="v-percent"><span class="v-circle"></span> 78%</span>
		</div>
	</div>
</div>
@endif