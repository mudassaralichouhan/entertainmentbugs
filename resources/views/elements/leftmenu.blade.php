<div class="col-sm-4 col-md-4">
    <div class="porfile-menu">
        <ul data-spy="affix" data-offset-top="200" id="navbar">
            <li><a href="#title1" id="next">Reservations</a>
                <ul>
                    <li><a href="#title2" id="next">Points</a></li>
                    <li><a href="#title3" id="next">Upcoming Reservations</a></li>
                    <li><a href="#title4" id="next">Past Reservations</a></li>
                </ul>
            </li>
            <li><a href="#">Saved Restaurants</a></li>
            <li><a href="{{URL::to('users/edit')}}">Account Details</a>
                <ul>
                    <li><a href="#profile-section" id="next">Profile</a></li>
                    <li><a href="#password-section" id="next">Password</a></li>
                    <li><a href="#address-section" id="next">Address</a></li>
                    <li><a href="#public-diner-profile-section" id="next">Public Profile Details</a></li>
                    <li><a href="#dining-preferences-section" id="next">Dining Preferences</a></li>
                    <li><a href="#email-preferences-section" id="next">Communication Preferences</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>