<div class="ads-cont" style="background:#fff;position:relative; margin-bottom:10px;">
    <div id="ads-banner-img" style="width: 40%;float:left;">
        <a href="" data-ad-button="{{ $ad->website_link }}" target="_blank">
            <img src="{{ asset('public/ads/'.$ad->type.'s/'.$ad->file) }}" style="width:100%" />
        </a>
    </div>
    <div id="ads-banner-img-content" style="width: 58%;float:right;padding:8px 4px 0 4px;">
        <h6 style="font-size: 12px ;margin:0px  !important;padding: 2px 5px 7px 0;font-family: arial;font-weight: bold;line-height: 13px;">
            {{ $ad->title }}
        </h6>
        <p style="font-size: 10.5px;line-height: 12px;padding-right: 10px;margin: 0 0 8px 0;font-family: arial;    height: 25px;
    overflow: hidden;">
            {{ $ad->description }}
        </p>
        <div class="ads-banner-tag">
            <!--<span style="border:1px solid  #edecec;padding: 5px 6px 1px 6px;font-size: 8px;margin-bottom: 7px;display: inline-block;">{{ $ad->categories }}</span>-->
            <div class="clearfix"></div>
            <span class="tags_language" style="background: #edecec;padding: 2px 6px 0px 6px;font-size: 10px;margin-right:2px;">{{ $ad->language }}</span>
            <span class="tags_white" style="background: #edecec;padding: 2px 6px 0px 6px;font-size: 9.5px;margin-right:2px;">{{ $ad->tags }}</span>
            <small id="ads_name" style="border:1px solid #edecec;border-radius:5px;position: absolute;bottom: 11px;right: 3%;font-size: 10px;padding: 5px 13px 0px 13px;background: #edecec;">Ad</small>
        </div>
    </div>
    <a href="" class="ads-trigger-btn" data-ad-button="{{ $ad->website_link }}" style="float: none;font-size: 10px;color: #fff;margin: 5px 5px 0 0;background: #f72e5e;position: absolute;bottom: 10px;width: 25%;right: 31%;text-align: center;line-height: 17px;padding: 5px 0 0 0;">{{ $ad->button_name }} <i class="fa fa-external-link" aria-hidden="true"></i></a>
    <div class="clearfix"></div>
</div>