@extends('layouts.home')
@section('style')
    <style>
        .b-video .v-img {
            height: 92px;
            overflow: hidden;
        }

        .b-video .v-img a {
            display: block;
            width: 100%;
            height: 100%;
        }

        #home_page_video .v-img img {
            width: 100%;
            object-fit: cover;
            height: 100%;
            display: block;
        }

        .b-video .v-desc {
            padding-top: 7px;
        }

        .add_video {
            width: 100%;
            height: 150px;
            background-color: #eceff0;
            margin: 20px 0 0 0;
            text-align: center
        }

        .add_video a {
            padding: 52px 0 0 0;
            display: block;
            height: 100%;
            width: 100%
        }


    </style>
    <style>
        #rewards_main_page {
            padding: 40px 0 10px 0
        }

        body.light .content-wrapper {
            background-color: #f4f3f3;
            padding: 20px 0 0 0;
        }

        #rewards_main_page ul li {
            font-size: 24px;
            line-height: 64px;
        }

        #home_page_video .v-desc a {
            font-size: 12px;
        }
    </style>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="container">
            @if (Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
            @endif
            <div class="row">
                <div class="col-lg-12">
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="#" class="color-active">
                                                <span class="visible-xs"> All video </span>
                                                <span class="hidden-xs"> All video </span>
                                            </a>
                                        </li>
                                        <li class="hidden-xs"><a href="#"> </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content videolist" id="home_page_video">
                            <div class="col-lg-2 col-sm-6 videoitem">
                                <div class="add_video" style="margin-top:0px">
                                    <a href="{{URL::to('participate-video')}}">
                                        <i class="cvicon-cv-plus" aria-hidden="true"></i> <br>Add Video
                                    </a>
                                </div>
                            </div>
                            @if (!empty($rewardVideos))
                                @foreach ($rewardVideos as $video)
                                    <div class="col-lg-2 col-sm-6 videoitem" style="margin-top: 20px">
                                        @include('elements.reward.rewardcard')
                                        @if ($loop->iteration % 6 == 0)
                                            <div style="clear:both; overflow:hidden"></div>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="v-pagination">
                <ul class="list-inline">
                    <li class="v-pagination-prev">
                        <a href="#">
                            <i class="cv cvicon-cv-previous"></i>
                        </a>
                    </li>
                    <li class="v-pagination-first"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">...</a></li>
                    <li><a href="#">10</a></li>
                    <li class="v-pagination-skin visible-xs"><a href="#">Skip 5 Pages</a></li>
                    <li class="v-pagination-next"><a href="#"><i class="cv cvicon-cv-next"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection
