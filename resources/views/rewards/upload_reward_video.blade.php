@extends('layouts.home')
@section('content')
<!--Start Upload Video Part-->
<div class="content-wrapper" id="upload-video-step1">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 upload-page">
				<div class="u-area" >
					{{ Form::open(array('method' => 'post', 'id' => 'video-upload', 'enctype'=>'multipart/form-data')) }} 
					<input name="video" type="file" id="uploadVideoFile" onchange="validateFile(this.files);" /> 
					<i class="cv cvicon-cv-upload-video"></i>
					<div id="dropbox">
					<p class="u-text1">Select Video files to upload</p>
                    <p class="u-text2">or drag and drop video files</p>
					</div>
					{{ Form::button('Upload Video', ['type' => 'button', 'id'=>'submit-video', 'class' => 'btn btn-primary u-btn'])}}{{Form::close()}}
				</div>
				<div class="u-terms">
                    <p>By submitting your videos to out platform, you acknowledge that you agree to our <a href="#">Terms of Service</a> and <a href="#">Community Guidelines</a>.</p>
                    <p class="hidden-xs">Please be sure not to violate others' copyright or privacy rights. Learn more</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Upload Video Part -->

<!--Start Update Video Part-->

<div class="content-wrapper upload-page edit-page" id="upload-video-step2" style="display:none;">
{{ Form::open(array('method' => 'post', 'id' => 'uploadVideo')) }}
{{ Form::hidden('video', null, ['id' => 'selected-video-name']) }}
{{ Form::hidden('selected_thumb', null, ['id' => 'video-selected-thumbnail']) }}
	
    <div class="container-fluid u-details-wrap">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
						<div class="error-message">@include('elements.errorSuccessMessage')</div> 
                        <div class="u-details">
                            <div class="row">
                                <div class="col-lg-12 ud-caption">Upload Details</div>
                                <div class="col-lg-2"><div class="imgplace"></div></div>
                                <div class="col-lg-10">
                                    <div class="u-title"></div>
									<div id="upload-progress">
										<div class="u-size"></div>
										<div class="u-progress">
											<div class="progress">
												<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
													<span class="sr-only">0% Complete</span>
												</div>
											</div>
											<div class="u-close">
												<a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
											</div>
										</div>
									</div>
                                    
									
                                    <div class="u-desc"><span class="percent-process"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                </div>
                            </div>
                        </div>
                    </div>
					
					
					 </div>
					 
					 <div class="row" style="margin:40px 0 0 0;"> 
						   <div class="col-lg-2 ud-caption" style="line-height:90px"> Thumbnails Video</div>
						   <div class="col-lg-10" id="thumbnail_image">	
							<input name="thumb_image" type="file" id="video-thumb-image" onchange="validateThumbFile(this.files);" accept="image/x-png,image/gif,image/jpeg" /> 
							<a href="javascript:void(0);" title="Upload Video Thumbnail Image" class="upload-thumb-image"><i class="cvicon-cv-plus"></i></a>
											 
						   </div>					
				    </div>	
			</div>
        </div>
    </div>
	
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="u-form">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
								<label for="video-title">Video Title</label>
								{{ Form::text('title', null, ['id' => 'video-title', 'close' => 'form-control', 'placeholder' => 'Rocket League Pro Championship Gameplay (36 characters remaining)', 'required' => true]) }}
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="video-description">About</label>
								{{ Form::textarea('description', null, ['id' => 'video-description', 'class' => 'form-control', 'rows' => '3', 'required' => true, 'placeholder' => 'Description']) }}
                            </div>
                        </div>
						
						 <div class="col-lg-12">
                            <div class="form-group">
                                <label for="e1">Video Tags</label>
								{{ Form::text('tags', null, ['id' => 'video-tags', 'close' => 'form-control', 'required' => true, 'placeholder' => 'Select mulitple tags']) }}
                            </div>
                        </div>
					</div>
												
					  <div class="row">
							<div class="col-lg-3">
								<div class="form-group">
									<label for="video-feature">Features</label>
									{{ Form::select('feature', unserialize(VIDEO_FEATURES), null, ['class' => 'form-control', 'id' => 'video-feature', 'required' => true]) }}
									
								</div>
                            </div>
							<div class="col-lg-3">
								<div class="form-group">
									<label for="video-subtitle">Subtitles</label>
									{{ Form::select('sub_title', unserialize(VIDEO_SUBTITLES), null, ['class' => 'form-control', 'id' => 'video-subtitle', 'required' => true]) }}
								</div>
                            </div>
				 
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="video-language">Language</label>
								{{ Form::text('language', null,['class' => 'form-control', 'placeholder' => 'Choose Language', 'id' => 'video-language'])}}                                 
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="video-ps">Privacy Settings</label>
                                {{ Form::select('privacy_setting', unserialize(VIDEO_PRIVACY_SETTINGS), null, ['class' => 'form-control', 'id' => 'video-ps', 'required' => true]) }}
                            </div>
                        </div>                        
                    </div>
                    
                </div>
				<div class="u-area mt-small">
				{{ Form::button('Save', ['type' => 'submit', 'class' => 'btn btn-primary u-btn'])}}
                </div>
                <div class="u-terms">
                    <p>By submitting your videos to circle, you acknowledge that you agree to circle's <a href="#">Terms of Service</a> and <a href="#">Community Guidelines</a>.</p>
                    <p class="hidden-xs">Please be sure not to violate others' copyright or privacy rights. Learn more</p>
                </div>
            </div>
        </div>
    </div>

{{ Form::close()}}	
</div>


<!--End Update Video Part-->

<style>

   
   #progress{
	 height: 6px;
	 display:block;
	 width: 0%;
	 border-radius: 2px;
	 background: -moz-linear-gradient(center top , #13DD13 20%, #209340 80%) repeat scroll 0 0 transparent; /* IE hack */ 
	 background: -ms-linear-gradient(bottom, #13DD13, #209340); /* chrome hack */ 
	 background-image: -webkit-gradient(linear, 20% 20%, 20% 100%, from(#13DD13), to(#209340)); /* safari hack */ 
	 background-image: -webkit-linear-gradient(top, #13DD13, #209340); /* opera hack */ 
	 background-image: -o-linear-gradient(#13DD13,#209340);
	 box-shadow:3px 3px 3px #888888;
   }
   .preview{
	 border: 1px solid #CDCDCD;
	 width: 450px;
	 padding: 10px;
	 height:auto; 
	 overflow: auto;
	 color: #4D4D4D;
	 float: left;
	 box-shadow:3px 3px 3px #888888;
	 border-radius: 2px;
   }
   .percents{
	 float: right;
   }   
   .upload-progress{
	 display: none;
   }
   
   #video-thumb-image{ display:none;}
   
   .upload-thumb-image{
	  padding: 22px 46px;
	  border: 2px dotted #ccc;
	  font-size: 38px;
	  color:#999;
	  margin:5px;
   }
   #thumbnail_image img{ max-width:150px; max-height:120px; margin:0px;}
   
   .video-thumb-img{position:relative; display:inline-block; margin:0px 5px;}
   .delete-video-thumb{position:absolute; right:0px; color:#f72e5e; background-color:#fff;}
   
   
</style>
{{ HTML::style('public/css/bootstrap-tagsinput.css')}}
{{ HTML::script('public/js/jquery.validate.js')}}
{{ HTML::script('public/js/bootstrap-tagsinput.js')}}
{{ HTML::script('public/js/bootstrap3-typeahead.js')}}
{{ HTML::script('public/js/script.js')}}
<!-- Owl Carousel -->
{{ HTML::script('public/js/jquery.easytabs.min.js') }}
{{ HTML::script('public/js/owl.carousel.js') }}
{{ HTML::script('public/js/only_slider.js') }}
<!-- Owl Carousel -->
<script type="text/javascript">
    $(document).ready(function () {
		$("i.cvicon-cv-upload-video").click(function () {
		  $("#uploadVideoFile").trigger('click');
		  $('input[type="file"]').change(function(e){
				var fileName = e.target.files[0].name;
				$('#dropbox').prepend(fileName);
				$('.cvicon-cv-upload-video').css('color', '#f72e5e');
           });
		});

		$('#submit-video').on('click', function(e){
			var fileData = $('#uploadVideoFile').prop('files');
			if(validateFile(fileData)){
			  $('#upload-video-step1').hide(0);
			  $('#upload-video-step2').show(0);
			  handleFiles(fileData);
		    }						
		});
		
		$("#uploadVideo").validate();
		
		$(".upload-thumb-image").click(function () {
		  $("#video-thumb-image").trigger('click');
		});
		
		$('#video-tags').tagsinput({
			typeahead: {
				source: function(query) {
				  //return $.get('http://someservice.com');
				}
			  }
		});
			
		$('#video-language').tagsinput({
			  typeahead: {
				source: ['English', 'Hindi', 'Tamil', 'Bengali', 'Gujarati']
			  },
			  freeInput: true
		});	
    });
</script> 
@endsection