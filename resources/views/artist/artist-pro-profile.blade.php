@extends('layouts.home') 

<!--code from everywhere -->
@php
  $getUserDetail = \App\Models\User::where('id',$id)->get();
  $hasThemePlan = $getUserDetail[0]->hasThemePlan();
  $userid = count($getUserDetail) > 0 ? $getUserDetail[0]->id : 0;

  $artist_about = \App\Models\ArtistAbout::where('user_id', $userid)->get();
  $artist_what_we_do=DB::select('select * from artist_what_we_do where user_id='.$userid);
  $artist_achievment=DB::select('select * from artist_achievment where user_id='.$userid);
  $artist_showreel=DB::select('select * from artist_showreel where user_id='.$userid);
  $artist_gallery=DB::select('select * from artist_gallery where user_id='.$userid);
  $artist_video=DB::select('select * from artist_video where user_id='.$userid);
  $artist_project=DB::select('select * from artist_project where user_id='.$userid);
  $artist_contact=DB::select('select * from artist_contact where user_id='.$userid);
  $artist_follower=DB::select('select count(id_key) as follower from followers where user_id='.$userid);
@endphp
 
@section('meta_tags')
  <meta name="keywords" content="{{ $artist_about[0]->profile_name ?? '' }}" >
  <meta name="description" content="{{ $artist_about[0]->about_me ?? '' }}" >
  <meta property="og:title" content="{{ $artist_about[0]->profile_name ?? '' }} Vidoes" />
  <meta property="og:url" content="{{ url()->current() }}" />
  <meta property="og:image" content="{{ photo_url($artist_about[0]->profile_photo) }}" />
  <meta property="og:description" content="{{ $artist_about[0]->about_me ?? '' }}" >
@endsection

@section('content') 
<style>
  #live-video-user-list-home {
    display: none
  }
</style>


    <link media="all" type="text/css" rel="stylesheet" href="https://entertainmentbugs.com/public/css/owl.carousel.css">
    <link media="all" type="text/css" rel="stylesheet" href="https://entertainmentbugs.com/public/css/owl.theme.css">
    <link media="all" type="text/css" rel="stylesheet" href="https://entertainmentbugs.com/public/css/owl.transitions.css">



<style>

.casting-menu {
    display: none !Important;
}


.project-work-extra-block:nth-child(7){display:;}
.project-work-extra-block:nth-child(4){display:;}
.project-work-extra-block:nth-child(5){display:;}
.project-work-extra-block:nth-child(6){display:;}
#bottom-banner{position:relative;background:url(https://ui-themez.smartinnovates.net/items/bemax/img/1.jpg) no-repeat fixed;background-position:fixed;width:100%; height:380px;background-size:Cover;}
#bottom-banner:before{
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0; z-index:9;
    left: 0;
    background:#f72e5e; opacity:0.8;
    z-index: 1;}
#bottom-banner h2{width: 53%;
    margin: 0 auto;
    text-align: center;
    color: #fff;
    position: relative;
    z-index: 99;
    font-weight: 400;
    font-size: 22px;
    font-style: italic;
    padding: 130px 0 0 0;
    line-height: 35px;}
</style>


<style>
#mobile-set1{
position: absolute;
    right: 0;
    bottom: 0;
    min-width: 100%;
    min-height: 100%;
    /*transform: translateX(calc((100% - 100vw) / 2));*/
    
}
.show-real{height:95vh;position:relative;overflow:hidden}
.ach-le{float:left;width:98%;padding:0 0;margin: 0 1% 40px 1%; background:#fff;}
.ach-le img{width:100%;}
#what_we_do_all .owl-controls {margin-top: 20px !important;}


.pro-team-slide-ind a img{ transition: transform .2s;}
.pro-team-slide-ind a:hover img{
-ms-transform: scale(1.1); /* IE 9 */
-webkit-transform: scale(1.1); /* Safari 3-8 */transform: scale(1.1); }
.sl-wrapper .sl-image .sl-caption {font-size: 15px;}   
.sl-overlay {opacity: 0.95;}

     
.owl-carousel {display: block;}
    #gallery_all .item{
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }
    
     #achievement_all .item{
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }
    
      #what_we_do_all .item{
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }
    
    .customNavigation{
      text-align: center;
    }
    .customNavigation a{
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }

.cd-011 {width: 32.3%;float: left;margin: 0 0.5% 20px 0.5%;height: 356px;overflow: hidden;}
.cd-011 video{width:100%;height:205px;}
.video-bg-bottom{padding:62px 0 40px 0;}
.inside{padding:20px; background:#f4f3f3;}
.inside h6{margin:0px; padding:7px 0 3px 0;font-size:17px;  color: #f72e5e; font-weight: 500;}

  .containers.sticky{position:fixed;top:0px; width:100%;z-index:9999;}
  
  
  
  #contact-and-email {
    width: 420px;
    margin: 0 auto;
}  
.contact-new-last {background: #f72e5e;padding: 2px 0 60px 0;}

.contact-new-last h3 {line-height: 51px;font-weight: bold;color: #fff;font-size: 54px;text-align: Center;position: relative;margin: 72px 0 15px 0;}         
.contact-new-last a {    background: #fff;
width: 200px;
display: block;
text-align: center;
line-height: 40px;
padding: 14px 0 9px 0;
float:left;
font-size: 20px;
margin: 0 5px;
color: #f72e5e;}

.contact-new-last p {color: #fff;
width: 55%;
margin: 0 auto 29px;
font-size: 18px;
text-align: center;}



.project-work-extra-block {height: auto;margin: -12px 0 40px 0;padding: 0;float: left;width: 100%;}
.project-work-extra-block p {  position: relative;float:right;width:44%;  font-size: 15px;color: #2e2e2e;margin-bottom: 22px;margin-top: 0px;float: right;
padding: 0 30px 10px 0;line-height: 22px;font-weight:400;}
.project-work-extra-block strong{display: block;font-size: 20px; line-height:27px; margin:105px 0 8px 0; font-weight:500;color: #f72e5e;}
.project-work-extra-block {position:relative;   border: 2px solid #f4f3f3;}
.project-work-extra-block img{position: relative;float:left;width:50%;object-fit:cover;}
.produciton-project{background: #f4f3f3;padding: 62px 0 22px 0;}
.produciton-project h3 {line-height: 51px;font-weight: bold;color: #000;font-size: 38px;position: relative;margin: 0 0 50px 0;text-transform:uppercase;text-align:Center; }
.project-work-extra-block a{width: 120px;
    line-height: 34px;
    display: block;
    margin: 15px 37% 0 0;
    background: #f72e5e;
    color: #fff;
    text-align: Center;
    padding-top: 4px;
    height: 38px;
    font-size: 13px;}

.project-work-extra-block:nth-child(3) img{float:right}
.project-work-extra-block:nth-child(3) p{float:left}


.background img {border-radius: 250px;}
.background-insde:after{border-radius: 0 250px 250px 0 !important;}
.background-insde:before{border-radius: 250px 0 0 250px !important;}

.video-bg-bottom .slide-ind{padding: 0 0 0px}
.bg-all{clear:both;  }
.container2{padding:80px 0 130px 0;    overflow: hidden; background:url(https://images.pexels.com/photos/5978370/pexels-photo-5978370.jpeg) no-repeat; 
background-position: center center !important; background-size:cover !important;}

.container2{position:relative;}
.container2:before {
   position: absolute;
    content: "";
    left: 20%;
    top: 26.5%;
    width: 80%;
    height: 48%;
    background: rgba(0,0,0,0.2);

}





#home:befodre {content: '';display: block;position: absolute;left: 0;top: 0;width: 100%;height: 100%;z-index: 1;background: rgba(0,0,0,.5);background: rgba(0,0,0,0.65);background-color: rgba(0,0,0,0.65);} 
#home .container{position:relative;z-index:2;}
.content_all h3{ line-height: 50px;font-weight: bold;color: #fff;font-size: 58px;margin: 112px 0 3px 0;position: relative;}
.content_all p.top1 {width: 100%;font-size: 15px;color: #fff;margin-top: 17px;}
.content_all h5{width:100%; margin:0 0 9px 0;font-size:15px; color: #fff;}
ul.social-menu {position: relative;overflow: hidden;padding: 0;margin:0px 0  3px 0;float:left;}
.landing-section ul.social-menu{ float:none; margin:0 auto; display:inline-block;}
ul.social-menu li {display: inline;list-style-type: none;position: relative;line-height:1;float: left;margin: 0px 2px 10px 0;padding:0 0 0 0;overflow: hidden;text-align:center;
-moz-transition: opacity 0.3s ease-in;
-webkit-transition: opacity 0.3s ease-in; 	-o-transition: opacity 0.3s ease-in;}
ul.social-menu li a {margin: 0 0 0 0;position: relative;font-size: 14px;height: 46px;text-align: center;background:#fff;padding: 13px 25px 13px 24px;float: left;border: none !important;}
ul.social-menu li a i{margin-right:10px;}
.content_all{position:relative}
.trigger_button {position:absolute;right:0px;top:60px;z-index: 9999;}
.trigger_button a{color:#fff;background: #28b47e !important;color:#fff; padding:7px 15px; margin:0 0 0 10px;}

.pro-team{padding:62px 0 50px 0;clear:both; overflow:hidden;background: #f4f3f3;   }
.pro-team h3 {line-height: 51px;font-weight: bold;color: #f72e5e;font-size: 38px; text-transform:uppercase;text-align:Center;  position: relative;margin: 0 0 25px 0;}     
.pro-team-slide-ind a {width: 96%;float: left; margin: 0 2% 13px 2%;} 
.pro-team-slide-ind a img{width:100%;}   
.pro-team h6 {text-align:center;color: #fff;font-size: 20px;position: relative;margin: -30px 0 10px 0;} 
.pro-team span{ width: 100%;font-size: 14px;color: #fff;margin: 0 0 23px 0;text-align:center;display:block;}

.slide-ind img{width:23%; float:left; margin:0 1% 20px 1%;}
.content_alls{clear:both; overflow:hidden;}
.video-about{clear:both; overflow:hidden;padding:50px 0;}
.video-about-left{width: 38%;float: left;background: rgba(255,255,255,0.3);padding: 45px 20px 15px 25px;margin: 21% 0 0 0;}
.video-about-left h3{line-height: 21px;font-weight: bold;text-transform: uppercase;font-size: 38px;margin: 0;position: relative;}
.video-about-left p {width: 100%;font-size:16px;color: #fff;margin-top: 12px;}
.video-about-right{width:57%; float:right}
.content_alls h3{line-height: 51px;font-weight: bold;color: #f72e5e;font-size: 38px;text-transform: uppercase;text-align: Center;position: relative;margin: 0 0 25px 0;}
.about-me{clear:both; overflow:hidden;margin:0px 0;position:relative;   }

.about-me h3{text-align: left; line-height: 51px; font-weight:bold; color: #fff;font-size: 68px;margin:0px 0 20px 10px;position: relative}
.about-me-left p{width: 100%;font-size:20px;color: #fff;margin-top: 17px;}
.about-me-right{width: 38%;float: right;padding: 24px 43px 20px 43px;background: #000;position: absolute;right: 61px;top: 36px;}
.about-me-right p {width: 100%;font-size: 16px;color: #fff;margin-top: 9px;}

.shane_tm_hero {width: 100%; float: left;clear: both;position: relative;}
.shane_tm_hero .image {position: absolute;right: 0;top: 0;bottom: 0;left: 0;background-repeat: no-repeat;background-position: center;background-size: cover;}
#follower-main1{width:100%;clear:both; overflowh:hidden}
#follower-main1 p {float: left;width: 33.3%;border-top: solid 1px #eceff0;text-align: Center;line-height: 16px;padding: 10px 0;margin: 0px;border-bottom: solid 1px #eceff0;}
#follower-main1 a {width: 175px;float: right;}
#follower-main1 a {background: #28b47e !important;color: #fff;display: block;height: 32px;line-height: 34px;}
.content_all{ padding: 7px 0 0 0;  
float:right;width: 69%;}
.content_all p{margin-bottom:4px}

.background{float:left;width:27%}

.background-insde {width: 100%;margin: 46px auto 0;display: block;    border-radius: 1000px;overflow: inherit;}
.background img{width:100%; height:100%; object-fit:cover;z-index: 3;position:relative;}
 
.content_all strong{color: #7e7e7e;}
.background-insde{position:relative;}
.background-insde:before {content: '';
    display: block;
    position: absolute;
    left: -13px;
    top: -9px;
    width: 55%;
    height: 106%;
    background: #f72e5e;
    z-index: 1;}
.background-insde:after {content: '';
    display: block;
    position: absolute;
    right: -13px;
    width: 55%;
    height: 106%;
    background: #fff;
    z-index: 1;
    top: -9px;}
.casting-menu{width:100%;}
.casting-menu ul{list-style:none;padding:0px;text-align:center;background:#f72e5e;margin:0px;}
.casting-menu ul li{display:inline-block;text-align:center;  }
.casting-menu ul li a{    letter-spacing: 0.3px; color:#fff; font-size:12px;  padding: 16px 18px 10px 18px;display:block;}
.casting-menu ul li a:hover{background:#c42047}
.extra-work-extra-block{height: auto;margin: 55px 0 45px 0;padding: 0 0px 0px 0px;float: left;width: 100%;}

.ach-le{position:relative}
#crousal_wrap  .owl-controls{display:block!important;}
#crousal_wrap .owl-pagination{display:block!important;}
.extra-work-extra-block a {   
 width: 120px;
    line-height: 33px;
    height:38px;
    display: block;font-size: 13px;
    margin: 0px 37% 0 0;
    background: #f72e5e;
    color: #fff;
    position: absolute;
    right: 0px;
    text-align: Center;
    padding-top: 4px;}
.extra-work-extra-block h3 {text-align: center;text-transform: uppercase;line-height: 51px;font-weight: bold;color: #f72e5e;font-size: 38px;margin: 14px 0 27px 2px;position: relative;}
.extra-work-extra-block strong { 
 color: #fff;
    background: rgba(0,0,0,0.5);
    margin-top: -59px;
    padding: 12px 20px 7px 20px;
    width: 100%;
    font-weight: 500;
    position: relative;
    z-index: 99;
    height: 59px;
  }
 .extra-work-extra-block p {
font-size: 14px;
    color: #2e2e2e;
    padding: 7px 14px 9px 14px;
    width: 100%;
    line-height: 22px;
    margin:0px;
  }
 
.extra-work-extra-block strong{    display: block;font-size: 16px;margin-bottom: 8px;line-height: 20px;}
.container2 {display: flex;justify-content: center;align-items: center; }
.followers-block{ width: 295px;clear: both;margin-left: 20px;display: block;}  
.followers-block img{float: left;width: 44px;border: 2px solid #fff;border-radius: 100px;margin-left: -25px;}
.followers-block p{margin-left:7px;float: left;color: #fff;font-size: 13px;padding-top: 15px;}
.followers-block a {background: #28b47e;padding: 7px 16px 3px 14px;float: right;color: #fff;margin-top: 8px;font-size: 13px;} 
.followers-block a:hover {background: #f72e5e; color: #fff;  text-decoration:none;}     


.what-we-do-main{background: #f4f3f3;padding: 70px 0 50px 0;}    
.what-we-do-block h3{text-align:center; text-transform:uppercase; line-height: 51px;font-weight: bold;color: #f72e5e;font-size: 38px;margin:10px 0 10px 2px;position: relative;} 
.what-we-do-main p.btm02{text-align: center;font-size: 16px;padding: 0 0px 10px 0;width: 44%;line-height: 23px;margin: -7px auto 15px;}          
    
.what-we-do-block-left {    min-height: 176px; float: left;width: 97%;margin: 0 1.5%;padding: 20px 11px;background: #fff;margin-top: 25px;}
.what-we-do-block-left h4{text-align:center;color: #f72e5e;font-size: 17px;     font-weight: 500; position: relative;} 
.what-we-do-block-left p{text-align:center;font-size: 14px; padding:0 0px 10px 0;width:100%; line-height: 23px;margin:0}
   
   
#what_we_do_all .what-we-do-block-left span{width: 35px;height: 35px;border-radius: 100px;text-align: center;line-height: 39px;background: #f72e5e;margin: -39px auto 20px;display: block;color: #fff;font-size: 20px;font-weight: 600;}

  #raising-star{width: 55px;
    float: left;
    margin: -14px 1px 0 0;-webkit-transform:rotate(272deg);
  -moz-transform: rotate(272deg);
  -ms-transform: rotate(272deg);
  -o-transform: rotate(272deg);
  transform: rotate(272deg);}
 
@media only screen and (max-width: 767px) {
    
    .casting-menu ul li a {
    letter-spacing: 0.3px;
    color: #fff;
    font-size: 10px;
    padding: 13px 7px 6px 10px;
    display: block;
}
#branding h3{    line-height: 26px !important;
    font-size: 22px !important;
    margin: 0 6px 25px 6px !important;}
.project-work-extra-block strong {margin: 10px 0 8px 0;}
.pro-team h3, .produciton-project h3{    line-height: 26px !important;
    font-size: 22px !important;
    margin: 0 6px 25px 6px !important;}
.content_alls h3{    line-height: 26px !important;
    font-size: 22px !important;
    margin: 0 6px 25px 6px !important;}
#premium-about .about-me h3{    line-height: 26px !important;
    font-size: 22px !important;
    margin: 0 6px 25px 6px !important;}

.contact-new-last h3 {line-height: 31px;font-size: 33px;margin: 72px 0 6px 0;}
.container2 {padding: 10px 0 50px 0;}
.background {float: none !important; width: 75% !important;padding:0 20px;margin:0 auto} 
.content_all {float: none !important; width: 100% !important;padding:0 10px;}   
.content_all h3 {text-align: center; font-size: 38px;margin: 48px 0 6px 0;position: relative;}
ul.social-menu {;float: none;text-align: center;}
ul.social-menu li a {font-size: 12px;  text-align: center; height: 40px;padding: 13px 11px 9px 9px;}
ul.social-menu li a.entertainment_icon-1{padding:10px 14px 9px 12px;}

ul.social-menu li {display: inline-block; line-height: 1;float: none;margin: 0px 5px -1px 0;}
.content_all h5{text-align: center;}
.followers-block{margin:0 auto}   
#premium-about .about-me p {font-size: 14px  !important; padding: 10px 0px 10px 0  !important;line-height: 23px  !important;margin: -7px auto 4px  !important;text-align: center;}
#premium-about .about-me-right p {float: left;width: 48% !important;margin: 0 1% 9px 1% !important;}
.about-me {margin: 0px 0 62px 0 !important;padding:0px !important;}
.about-me-left{float: none !important; width: 100% !important;padding: 49px 5px 15px 5px !important;height: auto; }  
.about-me-right{    float: none !important;width: 90% !important;padding: 14px 20px;height: auto;position: relative;right: 0;top: -37px;margin: 0 auto;}  
.about-me h3 {font-size: 50px;}
.extra-work-extra-block p {width: 100%;margin-left: 0px !important;position: relative;display: block;}
.extra-work-extra-block {    margin: 0;
    padding: 29px 0px 0px 0px;
    width: 100%;
    background: none;}   
.extra-work-extra-block h3 {margin: 15px 0 22px 0px;}
.video-about-left {margin: 38% 0 0 0 !important;}

.video-about-left{float: none !important; width: 100% !important;padding:27px 15px 1px 15px;}   
.video-about-right {float: none !important; width: 100% !important;padding:0 10px;}   
.video-about-left h3 {margin: 25px 0 20px 0;position: relative;}
.video-about-right    video {height: 210px !important;}

.what-we-do-main p.btm02 {font-size: 16px;width: 98%;line-height: 21px;}
.what-we-do-block-left {min-height: 176px;float: left;width: 98%;padding: 20px 0.5% 0 0.5%;}
.what-we-do-block-left p {font-size: 14px;
    padding: 0 24px 10px 24px;
    line-height: 23px;
    margin: 0;}
.ach-le {
   width: 100% !important;
    padding: 0 0 0px 0;
    margin: 0 0% !important;}
.extra-work-extra-block a {margin: 0 auto !important;position: relative;}
.pro-team h6 {font-size: 16px;margin: -24px 0 10px 0;}

.pro-team-slide-ind a {width: 96%;
    float: left;
    margin: 0 2% 7px 2%;  }   
.pro-teams.gallery   {margin:0 -15px;}
.cd-011 {width: 100% !Important;float: none !Important;margin: 0 0% 24px 0% !Important;height: auto !Important;overflow: hidden !Important;}
.video-bg-bottom .slide-ind {padding:0 4px 0 5px !Important;}  
.project-work-extra-block p {font-size: 14px !Important;color: #000 !Important;margin-bottom: 22px !Important;margin-top: 0px !Important;float: right !Important;padding: 20px 0px 10px 0px !Important;width: 100% !Important;line-height: 22px !Important;} 
.project-work-extra-block img {position: relative !Important;top: 0 !Important;left: 0 !Important; width: 100% !important;margin: 0 auto !Important;}
.project-work-extra-block {height: auto !Important;margin: -29px 0 5px 0 !Important;padding: 33px 5px 0px 5px !Important;}
.contact-new-last p {width: 86% !Important;}



 
#contact-and-email {width: 90% !Important;}
.contact-new-last a {width: 48% !Important;margin: 0 1%  !Important;font-size:14px !important;}
.extra-work-extra-block h3{ line-height: 26px;font-size: 22px;}
 .what-we-do-block h3 {line-height: 26px;font-size: 22px;margin: 0 6px 25px 6px;}   
.video-about-left h3{ line-height: 26px;font-size: 22px;}
#crousal_wrap{    width: 100% !Important;}
  .show-real {
    height: 390px;
    position: relative;
    overflow: hidden;
}
.video-about-left p {width: 100%;font-size: 14px;color: #fff;margin: -14px 0 23px 0;}
#mobile-set1{    position: absolute;height: 354px;object-fit: cover;}
.show-real {margin-top: 40px;}
#bottom-banner {height: auto;padding: 0 0 116px 0;}
#bottom-banner h2 {
    width: 95%;
    font-size: 22px;
    padding: 100px 0 0 0;
    line-height: 35px;
}
}
</style>
  @if(!$artist->isDeleted)  
 
 <div class="containers" id="artist-menu">
    <div class="casting-menu">
        <ul>
          <li><a href="#banner-section-top">Home</a></li>
          <li><a href="#premium-about">About</a></li>
          <li><a href="#services">Services</a></li> 
          <li><a href="#achievements">Achievements</a></li>
          <li><a href="#show-real">Showreel</a></li>
          <li><a href="#gallery">Gallery</a></li>
          <li><a href="#video">Video</a></li>
          <li><a href="#latest-project">Latest Project</a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
    </div>
  </div>
         
         

 <div id="banner-section-top">
 <div class="container2" style="background: url(<?php echo (count($artist_about) > 0 ? (isset($artist_about[0]->profile_bkg) ? $artist_about[0]->profile_bkg : '')  : '' );?>) no-repeat;">
  <div class="container">
    <div class="rowbg-all">
      <div class="background">
        <div class="background-insde">
          <img src="
						<?php echo (count($artist_about) > 0 ? (isset($artist_about[0]->profile_photo) ? $artist_about[0]->profile_photo : '')  : '' );?>">
        </div>
      </div>
       <div class="content_all">
        <h3>
          @if( $getUserDetail[0]->isPremium() || $getUserDetail[0]->hasThemePlan() )
			 
            <img src="https://www.entertainmentbugs.com/public/img/white_star.gif" id="raising-star">
          @endif
          <?php echo (count($artist_about) > 0 ? (isset($artist_about[0]->profile_name) ? $artist_about[0]->profile_name : '')  : '' ); ?> </h3>
          <h5> Category: <?php echo (count($artist_about) > 0 ? (isset($artist_about[0]->multiple_category) ? $artist_about[0]->multiple_category : '')  : '' ); ?> </h5>
          <h5>Tags: {{ is_var_exist($artist_about, 'tags') ? $artist_about[0]->tags : '' }}</h5>
        <ul class="social-menu tranz">
          <li class="sprite-imdb">
            <a target="_blank" title="" href="
									<?php echo (count($artist_contact) > 0 ? (isset($artist_contact[0]->facebook) ? $artist_contact[0]->facebook : '')  : '' ); ?>">
              <i class="fa fa-facebook"></i>
              <span>Facebook</span>
            </a>
          </li>
          <li class="sprite-instagram">
            <a target="_blank" href="
                
									<?php echo (count($artist_contact) > 0 ? (isset($artist_contact[0]->instagram) ? $artist_contact[0]->instagram : '')  : '' ); ?>">
              <i class="fa fa-instagram"></i>
              <span>Instagram</span>
            </a>
          </li>
          <li>
            <a target="_blank" title="" href="
                
									<?php echo (count($artist_contact) > 0 ? (isset($artist_contact[0]->youtube) ? $artist_contact[0]->youtube : '')  : '' ); ?>
                ">
              <i class="fa fa-youtube-play"></i>
              <span>Youtube</span>
            </a>
          </li>
          <li>
            <a target="_blank" title="" href="" class="entertainment_icon-1">
              <img style="width:20px;margin:-2px 5px 0 -4px;float: left;" src="http://www.entertainmentbugs.com/public/img/logo_footer.png">
              <span style="line-height: 14px;display: inline-block;padding: 5px 0 0 0;">Entertainment Profile</span>
            </a>
          </li>
          <li>
            <a target="_blank" title="" href="mailto:
                 
										<?php echo (count($artist_contact) > 0 ? (isset($artist_contact[0]->email) ? $artist_contact[0]->email : '')  : '' ); ?>
                ">
              <i class="fa fa-inbox"></i>
              <span> Contact Me</span>
            </a>
          </li>
        </ul>
        <div class="followers-block">
          <img src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-1-400x400.jpg">
          <img src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-2-400x400.jpg">
          <img src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-3-400x400.jpg">
          <img src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-4-400x400.jpg">
          <img src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-5-400x400.jpg">
          <p> <?php echo (count($artist_follower) > 0 ? (isset($artist_follower[0]->follower) ? $artist_follower[0]->follower : '')  : '' ); ?> Followers
            <!--212k Followers-->
          </p>
          <a href="">
            <i class="fa fa-heart-o"></i> Follow me </a>
        </div>
 


    </div>
  </div>
</div>
</div>

      </div>
<style>
#premium-about{}   
#premium-about .about-me-left {float: left;width: 100%;padding: 27px 120px 0px 120px;   height: auto;}
#premium-about .about-me h3{text-align: center;text-transform: uppercase;line-height: 51px;font-weight: bold;color: #f72e5e;font-size: 38px;margin: 10px 0 10px 2px;position: relative;}
#premium-about .about-me p{   font-size: 16px;padding: 0 0px 10px 0;line-height: 23px;margin: -7px auto 4px;color: #000;text-align: center;}
#premium-about .about-me-right {width:100%;float: right;padding: 0px 0px 10px 0px;background: none;position: relative;right: 0;top: 0;}
#premium-about .about-me-right p{ color: #000;border: 1px solid #f72e5e;padding-top: 10px;font-size: 14px;}
#premium-about .about-me-right p strong{    display: block;font-weight: 500;margin-bottom: -3px;}
#premium-about .about-me-right p {float: left;width: 13.8%;margin: 0.2%;}
#premium-about .about-me {padding: 80px 0 135px 0;}
#premium-about{position:relative;}
#premium-about:before{content: "ARTIST";position: absolute;left: 0px;top: 0px;width: 100%;font-size: 429px;opacity: 0.05;overflow: hidden;text-align: center;line-height: 610px;}
</style>



<div id="premium-about">
  @if( $artist_about && count($artist_about) > 0 )
    <div class="container">
      <div class="content_alls">
        <div class="about-me">
          <div class="about-me-left">
            <h3>About Me</h3>
            <p> <?php echo (count($artist_about) > 0 ? (isset($artist_about[0]->about_me) ? $artist_about[0]->about_me : '')  : '' ); ?> </p>
          </div>
          <div class="about-me-right">
            <p>
              <strong>Age: </strong> <?php echo (count($artist_about) > 0 ? (isset($artist_about[0]->age) ? $artist_about[0]->age : '')  : '' ); ?> years
            <p>
              <strong>Gender: </strong> <?php echo (count($artist_about) > 0 ? (isset($artist_about[0]->gender) ? $artist_about[0]->gender : '')  : '' ); ?>
            </p>
            <p>
              <strong>Location: </strong> <?php echo (count($artist_about) > 0 ? (isset($artist_about[0]->city) ? $artist_about[0]->city : '')  : '' ); ?>
            </p>
            <p>
              <strong>Language: </strong> <?php echo (count($artist_about) > 0 ? (isset($artist_about[0]->language) ? $artist_about[0]->language : '')  : '' ); ?>
            </p>
            <p>
              <strong>Height: </strong> <?php echo (count($artist_about) > 0 ? (isset($artist_about[0]->height) ? $artist_about[0]->height : '')  : '' ); ?>
            </p>
            <p>
              <strong>Weight: </strong> <?php echo (count($artist_about) > 0 ? (isset($artist_about[0]->weight) ? $artist_about[0]->weight : '')  : '' ); ?> Kg"
            </p>
            <p>
              <strong>Experience: </strong> <?php echo (count($artist_about) > 0 ? (isset($artist_about[0]->exp) ? $artist_about[0]->exp : '')  : '' ); ?> years
            </p>
          </div>
        </div>
      </div>
    </div>
  @endif
    </div>
    </div>

    @if( $artist_what_we_do && count($artist_what_we_do) > 0 && isset($artist_what_we_do[0]) && !empty($artist_what_we_do[0]->what_we_do) )
       <div id="services"> 
            <div class="what-we-do-main"> 
             <div class="container"> 
               <div class="what-we-do-block"> 
                 <h3> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->what_we_do) ? $artist_what_we_do[0]->what_we_do : '')  : '' ); ?> </h3>
                 <p class="btm02"> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->description) ? $artist_what_we_do[0]->description : '')  : '' ); ?> </p>  
                       <div class="span12">
              <div id="what_we_do_all" class="owl-carousel">
               
                   
                   
    			  		  
    			  		  
       <div class="item"> <div class="what-we-do-block-left"><span>1</span>
        <h4> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_1) ? $artist_what_we_do[0]->title_1 : '')  : '' ); ?> </h4>
        <p> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_1) ? $artist_what_we_do[0]->des_1 : '')  : '' ); ?> </p>
      </div></div> 
      
       <div class="item"> <div class="what-we-do-block-left"><span>2</span>
        <h4> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_2) ? $artist_what_we_do[0]->title_2 : '')  : '' ); ?> </h4>
        <p> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_2) ? $artist_what_we_do[0]->des_2 : '')  : '' ); ?> </p>
      </div></div>  
      
      <div class="item"> <div class="what-we-do-block-left"><span>3</span>
        <h4> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_3) ? $artist_what_we_do[0]->title_3 : '')  : '' ); ?> </h4>
        <p> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_3) ? $artist_what_we_do[0]->des_3 : '')  : '' ); ?> </p>
      </div></div>   
      <div class="item">  <div class="what-we-do-block-left"><span>4</span>
        <h4> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_4) ? $artist_what_we_do[0]->title_4 : '')  : '' ); ?> </h4>
        <p> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_4) ? $artist_what_we_do[0]->des_4 : '')  : '' ); ?> </p>
      </div></div>   
      
      <?php if((count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_5) ? $artist_what_we_do[0]->title_5 : '')  : '' ) !=null || 
        (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_5) ? $artist_what_we_do[0]->des_5 : '')  : '' )!=null){ ?> 
        <div class="item">  <div class="what-we-do-block-left"><span>5</span>
        <h4> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_5) ? $artist_what_we_do[0]->title_5 : '')  : '' ); ?> </h4>
        <p> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_5) ? $artist_what_we_do[0]->des_5 : '')  : '' ); ?> </p>
      </div></div>    <?php } ?> <?php if(
        (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_6) ? $artist_what_we_do[0]->title_6 : '')  : '' ) !=null || 
        (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_6) ? $artist_what_we_do[0]->des_6 : '')  : '' )!=null){ ?>  <div class="item"> <div class="what-we-do-block-left"><span>6</span>
        <h4> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_6) ? $artist_what_we_do[0]->title_6 : '')  : '' ); ?> </h4>
        <p> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_6) ? $artist_what_we_do[0]->des_6 : '')  : '' ); ?> </p>
      </div></div>    <?php } ?> <?php if((count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_7) ? $artist_what_we_do[0]->title_7 : '')  : '' ) !=null || (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_7) ? $artist_what_we_do[0]->des_7 : '')  : '' ) !=null){ ?>  <div class="item"> <div class="what-we-do-block-left"><span>7</span>
        <h4> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_7) ? $artist_what_we_do[0]->title_7 : '')  : '' ); ?> </h4>
        <p> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_7) ? $artist_what_we_do[0]->des_7 : '')  : '' ); ?> </p>
      </div></div>    <?php } ?> <?php if((count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_8) ? $artist_what_we_do[0]->title_8 : '')  : '' ) !=null || (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_8) ? $artist_what_we_do[0]->des_8 : '')  : '' ) !=null){ ?>  <div class="item"> <div class="what-we-do-block-left"><span>8</span>
        <h4> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_8) ? $artist_what_we_do[0]->title_8 : '')  : '' ); ?> </h4>
        <p> <?php echo (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_8) ? $artist_what_we_do[0]->des_8 : '')  : '' ); ?> </p>
      </div></div>   <?php } ?>
              </div>
            </div>
          </div>
        </div> 
      </div> 
    </div>
  @endif

  @if( $artist_achievment && count($artist_achievment) > 0 )
    <div id="achievements" style="background:#fff">
      <div class="container">
        <div class="extra-work-extra-block">
          <h3>Achievements</h3>
          <div class="span12" id="crousal_wrap">
            <div id="achievement_all" class="owl-carousel">
              <?php
        foreach($artist_achievment as $ac) {
      ?> 
      
          <div class="item">  
          <div class="ach-le">
     <img src="<?= $ac->photo_1 ?>" style="height:240px; object-fit:cover;">
      <strong> <?php echo (count($artist_achievment) > 0 ? (isset($ac->title_1) ? $ac->title_1 : '')  : '' ); ?>   <small style="display:block;font-size:12px;"><?php
            if($ac->date_1 != '' || $ac->date_1 != NULL)
            {
                echo date('d F, Y', strtotime(
                   
                     (count($artist_achievment) > 0 ? (isset($ac->date_1) ? $ac->date_1 : '')  : '' )
                    )); 
            }
            ?> </small>  </strong> 
            
            <p><?php echo (count($artist_achievment) > 0 ? (isset($ac->des_1) ? $ac->des_1 : '')  : '' ); ?> <?php 
            if($ac->link_1 != '' || $ac->link_1 != NULL)
            {
         ?> 
                     </p>
         <a href="
     <?php echo (count($artist_achievment) > 0 ? (isset($ac->link_1) ? $ac->link_1 : '')  : '' ); ?>
        ">View Now </a> <?php } ?>
    </p> 
    </div>
  </div>
    <?php } ?>          
          <div style="clear:both; overflow:hidden"></div>
        </div>
      </div>
    </div>
  </div>
</div>
@endif

@if( $artist_showreel && count($artist_showreel) > 0 )
    <div id="show-real" class="show-real" style="background:none">       
      <div class="container" style="background:none">
        <div class="video-about-left" style="position:relative; z-index:99;">
          <h3 style="color:#fff;"> Showreel</h3>
          <p> <?php echo (count($artist_showreel) > 0 ? (isset($artist_showreel[0]->description) ? $artist_showreel[0]->description : '')  : '' ); ?> </p>
        </div>
      </div>
      <video id="mobile-set1" autoplay="autoplay"loop muted poster="<?php echo count($artist_showreel) > 0 ? (isset($artist_showreel[0]->video_thumb) ? $artist_showreel[0]->video_thumb : '') : ''; ?>">
        <source src="<?php echo count($artist_showreel) > 0 ? (isset($artist_showreel[0]->video) ? $artist_showreel[0]->video : '') : ''; ?>" type="video/mp4" />
        <source src="<?php echo count($artist_showreel) > 0 ? (isset($artist_showreel[0]->video) ? $artist_showreel[0]->video : '') : ''; ?>" type="video/ogg" /> Your browser does not support the video tag.
      </video>
    </div>
@endif         
<link href='https://www.jqueryscript.net/demo/Responsive-Touch-enabled-jQuery-Image-Lightbox-Plugin/dist/simple-lightbox.min.css' rel='stylesheet' type='text/css'>

<style>
    #crousal_wrap{width:100%;}
</style>

@if( $artist_gallery && count($artist_gallery) > 0 )
<div id="gallery" class="pro-team">
  <div class="containers">
    <h3>Gallery</h3>
    <div class="containers">
           <div class="pro-teams gallery"> 
                 
             <div class="pro-team-slide-ind">  
            <div class="span12" id="crousal_wrap">
              <div id="gallery_all" class="owl-carousel">
              
              
                                                      
                         <?php 
                foreach($artist_gallery as $ag){
            ?>   <div class="item">  <a href="
            
            
																	<?php echo (count($artist_gallery) > 0 ? (isset($ag->photo) ? $ag->photo : '')  : '' ); ?>
            ">
            <img src="
              
              
																		<?php echo (count($artist_gallery) > 0 ? (isset($ag->photo) ? $ag->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')  : '' ); ?>
              " alt="
              
              
																		<?php echo (count($artist_gallery) > 0 ? (isset($ag->photo_title) ? $ag->photo_title : '')  : '' ); ?>
              " title="
              
              
																		<?php echo (count($artist_gallery) > 0 ? (isset($ag->photo_title) ? $ag->photo_title : '')  : '' ); ?>
              ">
            <h6> <?php echo (count($artist_gallery) > 0 ? (isset($ag->category) ? $ag->category : '')  : '' ); ?> </h6>
          </a>     </div> <?php } ?>
          
          
                    
              </div>
              
                    </div> 
                    </div> 
        
    
      </div>
    </div>
  </div>
</div>
@endif

@if( $artist_video && count($artist_video) > 0 )
<div class="video-bg-bottom" id="video" style="background:#fff">
  <div class="container">
    <div class="rows">
      <div class="content_alls">
        <h3> Video</h3>
         <div class="slide-ind">

                        <?php 
              $count=0;
              foreach($artist_video as $av) { ?>
                        <div class="cd-011 <?php if($count%3==0) {?> margin-right-off <?php } ?>">
                            <video class="" poster="
              
              <?php echo count($artist_video) > 0 ? (isset($av->thumb) ? $av->thumb : '') : ''; ?>
              "
                                controls>
                                <source src="
                
                <?php echo count($artist_video) > 0 ? (isset($av->video) ? $av->video : '') : ''; ?>
                "
                                    type="video/mp4">
                                <source src="
                
                <?php echo count($artist_video) > 0 ? (isset($av->video) ? $av->video : '') : ''; ?>
                "
                                    type="video/ogg"> Your browser does not support the video tag.
                            </video>
                            <div class="inside">
                                <small>

                                    <?php echo count($artist_video) > 0 ? (isset($av->category) ? $av->category : '') : ''; ?>
                                </small>
                                <h6>

                                    <?php echo count($artist_video) > 0 ? (isset($av->video_title) ? $av->video_title : '') : ''; ?>
                                </h6>
                                <p>

                                    <?php echo count($artist_video) > 0 ? (isset($av->description) ? $av->description : '') : ''; ?>
                                </p>
                            </div>
                        </div>


                        <?php 
            $count=$count+1;
            } ?>
                    </div>
      </div>
    </div>
  </div>
</div>
@endif

@if( $hasThemePlan && is_var_exist($artist_about, 'quote') )

	@if(!empty($artist_about[0]->quote->quote ))
  <div id="bottom-banner">
      <!-- 100 word limit-->
      <h2><i class="fa-quote-left"></i>{{ $artist_about[0]->quote->quote }}<i class="fa-quote-right"></i></h2>
    <p style="color:#fff;text-align:center;position:relative;z-index:99;font-size: 19px;padding: 10px 0 0 0;">-{{ $artist_about[0]->quote->name }}</p>
  </div>
@endif
@endif

<style>
#branding h3{float:left;ine-height: 51px;font-weight: bold;color: #000;font-size: 38px;position: relative;margin: 40px 0 20px 0;text-transform: uppercase;text-align: Center;}
#branding img{width: 100%;}
.branding {margin:32px 0 20px 0;float:right;}
.branding a{float:left;margin-left:5px; width:115px;}
.product-main{clear: both;display: block;padding:0 5px;}
.product-main-block {text-align: center;vertical-align: middle;display: inline;width: 100%;padding: 0;margin: 0px 0.5% 7px 0%;}
.content-b h2{color:#fff;text-align:left;font-size:16px;margin:20px 0 5px 0;}
.shop-btn{z-index: 99; position: absolute;right: 15px;bottom: 13px;background: #fff;color: #000;line-height: 17px;padding: 9px 12px 6px 12px;font-size: 12px;text-align: center;display: block;}
.product-main-block{position:relative;}

.product-main-block:before{background: #000;opacity: 0.2;left: 0px;bottom: 0px;height: 57px;width: 100%;content: '';z-index: 9;display: block;position: absolute;}
.content-b {width: 92%;position: absolute;z-index: 9;top: 245px;color: #fff;left: 10px;}


.product-main-block .product_img img {transition: transform .2s;}

.product-main-block:hover .product_img img {
  -ms-transform: scale(1.1); /* IE 9 */
  -webkit-transform: scale(1.1); /* Safari 3-8 */
  transform: scale(1.1); }
</style>

@php
  $brands = $artist_about[0]->brand_collaboration;
  $widthInPercent = [25, 24, 49.5, 49.5, 24.5, 24.5];
  $visibleProducts = 0;
@endphp


 @if(is_var_exist($artist_about, 'brand_collaboration'))
	<?php 

	
echo $count=count($artist_about[0]['brand_collaboration']);	
if($count > 0){ ?>
<div id="branding" style="background:#fff;padding: 20px 30px 60px 30px;clear:both;overflow:hidden;position: relative;">
  <div class="product-main" style="width: 1180px;margin: 0 auto;clear: both;overflow: hidden;">
    <h3 style="color:#f72e5e;text-align: left;padding-left: 0px;">Brand Collaborations</h3>
    <div class="branding">
      @if( $hasThemePlan )
        <img src="https://www.entertainmentbugs.com/public/img/purple_star.gif" id="raising-star" style="width: 48px;margin-top: -4px;" />
      @endif
    <a href="">
      <img src="{{ is_var_exist($brands, 'brand_logo') ? $brands[0]->brand_logo : '' }}" />
    </a>
    <a href="">
      <img src="{{ is_var_exist($brands, 'brand_logo', 1) ? $brands[1]->brand_logo : '' }}" />
    </a>
    </div>
	
    <div style="clear:both; overflow:hidden"></div>
	
    @if(is_var_exist($artist_about, 'brand_collaboration'))
        @for($i = 0; $i < 6; $i++)
            @php
                $product = null;
            @endphp
            @if( is_var_exist($brands, 'brand_products') )
                @if(isset($brands[0]->brand_products[$i]) && $brands[0]->brand_products[$i]->active == 1)
                    @php $product = $brands[0]->brand_products[$i]; @endphp
                @endif
            @endif
            
            @if( !$product && is_var_exist($brands, 'brand_products', 1) )
                @if(isset($brands[1]->brand_products[$i]) && $brands[1]->brand_products[$i]->active == 1)
                    @php $product = $brands[1]->brand_products[$i]; @endphp
                @endif
            @endif
            @php
                if(!$product){
                    $product = new stdClass();
                    $product->button_label = '';
                    $product->title = '';
                    $product->banner = '';
                    $product->url = '';
                }
            @endphp
            
            @if(empty($product->banner))
                @php
                    // add here, if you want some image on blank part; $product->banner = <IMAGE_URL>;
                @endphp
            @endif
			
			 @if( ($product->button_label!='') || ($product->title!='' )  || ($product->title!='' ) )
          <div class="product-main-block" style="width: {{ $widthInPercent[$visibleProducts++] }}%;float: left; height: 300px;overflow: hidden;">
            @if( $product->button_label )
              <a href="{{ $product->url }}" class="shop-btn">{{ $product->button_label }}</a>
            @endif

            <a href="{{ $product->url }}" class="product_img">
              <img src="{{ $product->banner }}" />
            </a>
            <div class="content-b">
              <h2>{{ $product->title }}</h2>
            </div>
          </div>
		  @endif
        @endfor
    @endif
  </div>
</div>
 <?php   
 }
 ?>
    @endif
@if( $artist_project && count($artist_project) > 0 )
<div id="latest-project" class="produciton-project">
  <div class="container">
    <h3 style="color:#f72e5e">Latest Projects</h3>
    
    <?php 
      foreach($artist_project as $ap) {
      ?> 
      
      <div class="project-work-extra-block">
      <img src="<?php echo (count($artist_project) > 0 ? (isset($ap->photo) ? $ap->photo : '')  : '' ); ?>" >
      <p> <?php if(count($artist_project) > 0){
            if($ap->date != '')
            {
        ?> <strong><span style="font-size:12px;display:block;color:#2e2e2e"><?php
            echo date('d F, Y', strtotime(
                
                 (count($artist_project) > 0 ? (isset($ap->date) ? $ap->date : '')  : '' )
                )); 
            ?> </span>  <?php echo (count($artist_project) > 0 ? (isset($ap->title) ? $ap->title : '')  : '' ); ?>   </strong>
            <?php } ?> 
            <?php } ?> <?php echo (count($artist_project) > 0 ? (isset($ap->description) ? $ap->description : '')  : '' ); ?> 
			<?php if($ap->button_name!='') {?>
			
			<a href="
			<?php echo (count($artist_project) > 0 ? (isset($ap->link) ? $ap->link : '')  : '' ); ?>"> <?php echo (count($artist_project) > 0 ? (isset($ap->button_name) ? $ap->button_name : '')  : '' ); ?> </a><?php } ?>
      </p> </div> 
      
      
      
      
   <?php } ?>
  </div>
</div>
@endif

<div id="contact" class="contact-new-last">
  <div class="container">
    <h3> <?php echo (count($artist_contact) > 0 ? (isset($artist_contact[0]->title) ? $artist_contact[0]->title : '')  : '' ); ?> </h3>
    <p> <?php echo (count($artist_contact) > 0 ? (isset($artist_contact[0]->description) ? $artist_contact[0]->description : '')  : '' ); ?> </p>
    <div id="contact-and-email">
      <a href="mailto:<?php echo (count($artist_contact) > 0 ? (isset($artist_contact[0]->email) ? $artist_contact[0]->email : '')  : '' ); ?>">
        <i class="fa fa-envelope"></i> Email me </a>
      <a href="https://wa.me/<?php echo (count($artist_contact) > 0 ? (isset($artist_contact[0]->phone) ? $artist_contact[0]->phone : '')  : '' ); ?>">
        <i class="fa fa-commenting-o"></i> Chat with me </a>
    </div>
  </div>
</div>
</div>
@else
<div class="under-review"><h5>This artist is under review</h5></div>

@endif
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<script type="text/javascript" src="https://www.jqueryscript.net/demo/Responsive-Touch-enabled-jQuery-Image-Lightbox-Plugin/dist/simple-lightbox.jquery.min.js"></script>
<script>
  $(function(){
    var gallery = $('.gallery a').simpleLightbox({navText:    ['&lsaquo;','&rsaquo;']});
  });
</script>
<script src="https://www.entertainmentbugs.com/public/js/owl.carousel.js"></script>
<script src="https://www.entertainmentbugs.com/public/js/only_slider.js"></script>
<script type="text/javascript" src="https://www.jqueryscript.net/demo/Responsive-Touch-enabled-jQuery-Image-Lightbox-Plugin/dist/simple-lightbox.jquery.min.js"></script>
<style>
#gallery_all .owl-item {
 width:315px !Important;   
}
</style>
<script>
$(document).ready(function() {
  var owl = $("#gallery_all");
  owl.owlCarousel({
  items : 5, //10 items above 1000px browser width
  itemsDesktop : [1000,5], //5 items between 1000px and 901px
  itemsDesktopSmall : [900,4], // 3 items betweem 900px and 601px
  itemsTablet: [600,3], //2 items between 600 and 0;
  itemsMobile :[600,1], // itemsMobile disabled - inherit from itemsTablet option
  
  });
});
</script>
<script>
  $(document).ready(function() {
    var owl = $("#what_we_do_all");
    owl.owlCarousel({
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,4], // 3 items betweem 900px and 601px
      itemsTablet: [600,3], //2 items between 600 and 0;
      itemsMobile :[600,1], // itemsMobile disabled - inherit from itemsTablet option
    });
  });
</script>

<script>
$(document).ready(function() {
  var owl = $("#achievement_all");
  owl.owlCarousel({
    items : 3, //10 items above 1000px browser width
    itemsDesktop : [1000,3], //5 items between 1000px and 901px
    itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
    itemsTablet: [600,3], //2 items between 600 and 0;
    itemsMobile :[600,1], // itemsMobile disabled - inherit from itemsTablet option
  });
});
</script>

<script>
  window.onscroll = function() {myFunction()};
  var header = document.getElementById("artist-menu");
  var sticky = header.offsetTop;

  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("sticky");
    } else {
      header.classList.remove("sticky");
    }
  }
</script>
<script>
$('.casting-menu a').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top - 42
    }, 500);
    return false;
});
 
</script>
<script>
  $(function() {
    var gallery = $('.gallery a').simpleLightbox({
      navText: ['&lsaquo;', '&rsaquo;']
    });
  });
</script>
@endsection