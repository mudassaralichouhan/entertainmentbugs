@extends('layouts.home')

<!--code from everywhere -->
@php
    $user = \App\Models\User::find($id);
    $hasThemePlan = $user->hasThemePlan();

    $artist_about = DB::select("select * from artist_about where user_id=?", [$id]);
    $artist_what_we_do = DB::select('select * from artist_what_we_do where user_id=? limit ?', [$id, $hasThemePlan ? \App\Models\ArtistWhatWeDo::$premiumBlocks : \App\Models\ArtistWhatWeDo::$blocks]);
    $artist_achievment = DB::select('select * from artist_achievment where user_id=? limit ?', [$id, $hasThemePlan ? \App\Models\ArtistAchivement::$premiumBlocks : \App\Models\ArtistAchivement::$blocks]);
    $artist_showreel = DB::select('select * from artist_showreel where user_id=?', [$id]);
    $artist_gallery = DB::select('select * from artist_gallery where user_id=? limit ?', [$id, $hasThemePlan ? \App\Models\ArtistGallery::$premiumBlocks : \App\Models\ArtistGallery::$blocks]);
    $artist_video = DB::select('select * from artist_video where user_id=? limit ?', [$id, $hasThemePlan ? \App\Models\ArtistVideo::$premiumBlocks : \App\Models\ArtistVideo::$blocks]);
    $artist_project = DB::select('select * from artist_project where user_id=? limit ?', [$id, $hasThemePlan ? \App\Models\ArtistProject::$premiumBlocks : \App\Models\ArtistProject::$blocks]);
    $artist_contact = DB::select('select * from artist_contact where user_id=?', [$id]);
@endphp

@section('meta_tags')
    <meta name="keywords" content="{{ $artist_about[0]->profile_name ?? '' }}" >
    <meta name="description" content="{{ $artist_about[0]->about_me ?? '' }}" >
    <meta property="og:title" content="{{ $artist_about[0]->profile_name ?? '' }} Vidoes" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:image" content="{{ photo_url($artist_about[0]->profile_photo) }}" />
    <meta property="og:description" content="{{ $artist_about[0]->about_me ?? '' }}" >
@endsection

@section('content')
    <style>
        #live-video-user-list-home {
            display: none
        }
  
  
  

        .pro-team-slide-ind a img {
            transition: transform .2s;
        }

        .pro-team-slide-ind a:hover img {
            -ms-transform: scale(1.1);
            /* IE 9 */
            -webkit-transform: scale(1.1);
            /* Safari 3-8 */
            transform: scale(1.1);
        }

        .sl-wrapper .sl-image .sl-caption {
            font-size: 15px;
        }

        .sl-overlay {
            opacity: 0.95;
        }

        .video-bg-bottom .slide-ind {
            padding: 0 0 0 15px
        }

        .bg-all {
            clear: both;
        } 

        .container2 {
            padding: 50px 0 85px 0;
            overflow: hidden;
            background: url(https://www.entertainmentbugs.com/public/img/artist-bg-cover.jpg) no-repeat;
            background-position: center center;
            background-size: cover;
        }

        #home:befodre {
            content: '';
            display: block;
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            z-index: 1;
            background: rgba(0, 0, 0, .5);
            background: rgba(0, 0, 0, 0.65);
            background-color: rgba(0, 0, 0, 0.65);
        }

        #home .container {
            position: relative;
            z-index: 2;
        }

        .content_all h3 {
            line-height: 21px;
            font-weight: bold;
            color: #fff;
            font-size: 68px;
            margin: 130px 0 20px 0;
            position: relative;
        }

        .content_all p.top1 {
            width: 100%;
            font-size: 15px;
            color: #fff;
            margin-top: 17px;
        }

        .content_all h5 {
            width: 100%;
            font-size: 16px;
            color: #fff;
            margin-bottom: 0px;
        }

        ul.social-menu {
            position: relative;
            overflow: hidden;
            padding: 0;
            margin: 0px 0 10px 0;
            float: left;
        }

        .landing-section ul.social-menu {
            float: none;
            margin: 0 auto;
            display: inline-block;
        }

        ul.social-menu li {
            display: inline;
            list-style-type: none;
            position: relative;
            line-height: 1;
            float: left;
            margin: 0px 5px 10px 0;
            padding: 0 0 0 0;
            overflow: hidden;
            text-align: center;
            -moz-transition: opacity 0.3s ease-in;
            -webkit-transition: opacity 0.3s ease-in;
            -o-transition: opacity 0.3s ease-in;
        }

        ul.social-menu li a {
            margin: 0 0 0 0;
            position: relative;
            font-size: 14px;
            height: 46px;
            text-align: center;
            background: #fff;
            padding: 13px 25px 13px 24px;
            float: left;
            border: none !important;
        }

        ul.social-menu li a i {
            margin-right: 10px;
        }

        .content_all {
            position: relative
        }

        .trigger_button {
            position: absolute;
            right: 0px;
            top: 60px;
            z-index: 9999;
        }

        .trigger_button a {
            color: #fff;
            background: #28b47e !important;
            color: #fff;
            padding: 7px 15px;
            margin: 0 0 0 10px;
        }

        .pro-team {
            padding: 50px 0 35px 0;
            clear: both;
            overflow: hidden
        }

        .pro-team h3 {
            line-height: 51px;
            font-weight: bold;
            color: #f72e5e;
            font-size: 38px;
            text-transform: uppercase;
            text-align: Center;
            position: relative;
            margin: 0 0 25px 0;
        }

        .pro-team-slide-ind a {
            width: 23%;
            float: left;
            margin: 0 1% 20px 1%;
        }

        .pro-team-slide-ind a img {
            width: 100%;
        }

        .pro-team h6 {
            text-align: center;
            color: #fff;
            font-size: 20px;
            position: relative;
            margin: -30px 0 10px 0;
        }

        .pro-team span {
            width: 100%;
            font-size: 14px;
            color: #fff;
            margin: 0 0 23px 0;
            text-align: center;
            display: block;
        }

        .slide-ind img {
            width: 23%;
            float: left;
            margin: 0 1% 20px 1%;
        }

        .content_alls {
            clear: both;
            overflow: hidden;
        }

        .video-about {
            clear: both;
            overflow: hidden;
            padding: 50px 0;
        }

        .video-about-left {
            width: 37%;
            float: left
        }

        .video-about-left h3 {
            line-height: 21px;
            font-weight: bold;
            color: #f72e5e;
            font-size: 52px;
            margin: 100px 0 20px 0;
            position: relative;
        }

        .video-about-left p {
            width: 100%;
            font-size: 18px;
            color: #000;
            margin-top: 17px;
        }

        .video-about-right {
            width: 57%;
            float: right
        }

        .content_alls h3 {
            line-height: 51px;
            font-weight: bold;
            color: #f72e5e;
            font-size: 38px;
            text-transform: uppercase;
            text-align: Center;
            position: relative;
            margin: 0 0 25px 0;
        }

        .about-me {
            clear: both;
            overflow: hidden;
            margin: 80px 0;
            position: relative;
        }

        .about-me-left {
            height: 375px;
            padding: 80px 120px 60px 70px;
            float: left;
            width: 65%;
            background: #f72e5e;
        }

        .about-me h3 {
            text-align: left;
            line-height: 51px;
            font-weight: bold;
            color: #fff;
            font-size: 68px;
            margin: 0px 0 20px 10px;
            position: relative
        }

        .about-me-left p {
            width: 100%;
            font-size: 20px;
            color: #fff;
            margin-top: 17px;
        }

        .about-me-right {
            width: 38%;
            float: right;
            padding: 24px 43px 20px 43px;
            background: #000;
            position: absolute;
            right: 61px;
            top: 36px;
        }

        .about-me-right p {
            width: 100%;
            font-size: 16px;
            color: #fff;
            margin-top: 9px;
        }

        .shane_tm_hero {
            width: 100%;
            float: left;
            clear: both;
            position: relative;
        }

        .shane_tm_hero .image {
            position: absolute;
            right: 0;
            top: 0;
            bottom: 0;
            left: 0;
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }

        #follower-main1 {
            width: 100%;
            clear: both;
            overflowh: hidden
        }

        #follower-main1 p {
            float: left;
            width: 33.3%;
            border-top: solid 1px #eceff0;
            text-align: Center;
            line-height: 16px;
            padding: 10px 0;
            margin: 0px;
            border-bottom: solid 1px #eceff0;
        }

        #follower-main1 a {
            width: 175px;
            float: right;
        }

        #follower-main1 a {
            background: #28b47e !important;
            color: #fff;
            display: block;
            height: 32px;
            line-height: 34px;
        }

        .content_all {
            padding: 7px 0 0 0;
            float: right;
            width: 72%;
        }

        .content_all p {
            margin-bottom: 4px
        }

        .background {
            float: left;
            width: 24%
        }

        .background-insde {
            width: 100%;
            margin: 46px auto 0;
            display: block;
            border-radius: 1000px;
            overflow: inherit;
        }

        .background img {
            width: 100%;
            height: 100%;
            object-fit: cover;
            z-index: 3;
            position: relative;
        }

        .content_all strong {
            color: #7e7e7e;
        }

        .background-insde {
            position: relative;
        }

        .background-insde:before {
            border-radius: 10px 0 0 10px;
            content: '';
            display: block;
            position: absolute;
            left: -13px;
            top: -9px;
            width: 56%;
            height: 107%;
            background: #f72e5e;
            z-index: 1;
        }

        .background-insde:after {
            border-radius: 10px 0 0 10px;
            border-radius: 0 10px 10px 0;
            content: '';
            display: block;
            position: absolute;
            right: -13px;
            top: -9px;
            width: 56%;
            height: 107%;
            background: #fff;
            z-index: 1;
        }

        .casting-menu {
            width: 100%;
        }

        .casting-menu ul {
            list-style: none;
            padding: 0px;
            text-align: center;
            background: #f72e5e;
            margin: 0px;
        }

        .casting-menu ul li {
            display: inline-block;
            text-align: center;
        }

        .casting-menu ul li a {
            color: #fff;
            font-size: 15px;
            padding: 20px 18px 20px 18px;
            display: block;
        }

        .casting-menu ul li a:hover {
            background: #c42047
        }

        .extra-work-extra-block {
            height: auto;
            margin: 65px 0 60px 0;
            padding: 45px 20px 0px 70px;
            float: left;
            width: 100%;
            background: #f72e5e;
        }

        .extra-work-extra-block h3 {
            line-height: 51px;
            font-weight: bold;
            color: #fff;
            font-size: 31px;
            margin: 25px 0 22px -101px;
            background: #000;
            display: inline-block;
            padding: 15px 25px 3px 24px !important;
            position: relative;
        }

        .extra-work-extra-block p {
            font-size: 16px;
            color: #fff;
            padding-bottom: 40px;
            margin-top: 0px;
            float: right;
            width: 82%;
            line-height: 24px;
            position: relative;
            margin-bottom: 0px;
        }

        .extra-work-extra-block strong:before {
            content: "";
            position: absolute;
            width: 10px;
            height: 10px;
            background: #fff;
            border-radius: 100px;
            left: -31px;
        }

        .extra-work-extra-block p:after {
            content: "";
            position: absolute;
            left: -27px;
            width: 2px;
            height: 80%;
            background: #fff;
            top: 0;
        }

        .extra-work-extra-block strong {
            display: block;
            font-size: 23px;
            margin-bottom: 8px;
        }

        .container2 {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .followers-block {
            width: 295px;
            clear: both;
            margin-left: 20px;
            display: block;
        }

        .followers-block img {
            float: left;
            width: 44px;
            border: 2px solid #fff;
            border-radius: 100px;
            margin-left: -25px;
        }

        .followers-block p {
            margin-left: 7px;
            float: left;
            color: #fff;
            font-size: 13px;
            padding-top: 15px;
        }
.followers-block a:hover {background: #f72e5e; color: #fff;  border:0px !important; text-decoration:none;}     

        .followers-block a {
            background: #28b47e;
            padding: 7px 14px 3px 14px;
            float: right;
            text-align:center;
            color: #fff;
            border:0px !important;
            margin-top: 8px;
            font-size: 13px;
            user-select: none;
            cursor: pointer;
        }

        /* .followers-block a:hover {
            background: #fff;
            color: #f72e5e;
            text-decoration: none;
        } */

        .what-we-do-main {
            background: #f4f3f3;
            padding: 50px 0 70px 0;
        }

        .what-we-do-block h3 {
            text-align: center;
            text-transform: uppercase;
            line-height: 51px;
            font-weight: bold;
            color: #f72e5e;
            font-size: 38px;
            margin: 10px 0 10px 2px;
            position: relative;
        }

        .what-we-do-main p.btm02 {
            text-align: center;
            font-size: 18px;
            padding: 0 0px 10px 0;
            width: 49%;
            line-height: 23px;
            margin: -7px auto 4px;
        }

        .what-we-do-block-left {
            min-height: 176px;
            float: left;
            width: 24%;
            margin: 0 0.5%;
            padding: 20px 11px;
            background: #fff;
            margin-top: 25px;
        }

        .what-we-do-block-left h4 {
            text-align: center;
            color: #f72e5e;
            font-size: 24px;
            position: relative;
        }
        .inside p{   font-size: 14px;}

        .what-we-do-block-left p {
            text-align: center;
            font-size: 14px;
            padding: 0 0px 10px 0;
            width: 100%;
            line-height: 23px;
            margin: 0
        }


        .cd-011 {
            width: 31.6%;
            float: left;
            margin: 0 1% 24px 1%;
            height: 356px;
            overflow: hidden;
        }

        .cd-011 video {
            width: 100%;
            height: 205px;
        }

        .video-bg-bottom {
            padding: 50px 0 30px 0;
        }

        .inside {
            padding: 20px;
            background: #fff;
        }

        .inside h6 {
            margin: 0px;
            padding: 7px 0 3px 0;
            font-size: 17px;
            color: #f72e5e;
        }

        .cd-011.margin-right-off {
            margin-right: 0px !important;
        }

        #contact-and-email {
            width: 420px;
            margin: 0 auto
        }

        .contact-new-last {
            background: #f72e5e;
            padding: 2px 0 60px 0;
        }

        .contact-new-last h3 {
            line-height: 51px;
            font-weight: bold;
            color: #fff;
            font-size: 54px;
            text-align: Center;
            position: relative;
            margin: 72px 0 15px 0;
        }

        .contact-new-last a {
            background: #fff;
            width: 200px;
            display: block;
            text-align: center;
            line-height: 40px;
            padding: 14px 0 9px 0;
            float: left;
            font-size: 20px;
            margin: 0 5px;
            color: #f72e5e;
        }

        .contact-new-last p {
            color: #fff;
            width: 55%;
            margin: 0 auto 29px;
            font-size: 18px;
            text-align: center;
        }

        .project-work-extra-block {
            height: auto;
            margin: -29px 0 60px 0;
            padding: 33px 20px 0px 70px;
            float: left;
            width: 100%;
            background: #f72e5e;
        }

        .project-work-extra-block p {
            font-size: 14px;
            color: #000;
            margin-bottom: 22px;
            margin-top: 0px;
            float: right;
            padding: 0 30px 10px 0;
            width: 82%;
            line-height: 23px;
        }

        .project-work-extra-block strong {
            display: block;
            font-size: 23px;
            margin-bottom: 8px;
            color: #000;
        }

        .project-work-extra-block {
            position: relative;
            background: #f4f3f3;
            border: 2px solid #f4f3f3;
        }

        .project-work-extra-block img {
            position: absolute;
            top: 10px;
            left: -36px;
        }

        .produciton-project {
            background: #fff;
            padding: 50px 0 8px 0;
        }

        .produciton-project h3 {
            line-height: 51px;
            font-weight: bold;
            color: #000;
            font-size: 38px;
            position: relative;
            margin: 0 0 50px 0;
            text-transform: uppercase;
            text-align: Center;
        }

        .project-work-extra-block a {
            margin-top: 10px;
            width: 150px;
            line-height: 46px;
            display: block;
            color: #fff;
            background: #f72e5e;
            text-align: Center;
        }


        @media only screen and (max-width: 767px) {
            .container2 {
                padding: 10px 0 50px 0;
            }

            .background {
                float: none !important;
                width: 85% !important;
                padding: 0 20px;
                margin: 0 auto
            }

            .content_all {
                float: none !important;
                width: 100% !important;
                padding: 0 10px;
            }

            .content_all h3 {
                text-align: center;
                font-size: 38px;
                margin: 48px 0 6px 0;
                position: relative;
            }

            ul.social-menu {
                ;
                float: none;
                text-align: center;
            }

            ul.social-menu li a {
                font-size: 12px;
                text-align: center;
                height: 40px;
                padding: 13px 14px 9px 12px;
            }

            ul.social-menu li a.entertainment_icon-1 {
                padding: 10px 14px 9px 12px;
            }

            .container2 {
                overflow: hidden;
                background: url(https://www.entertainmentbugs.com/public/img/artist-bg-cover.jpg) no-repeat;
                background-position: center center;
                background-size: cover;
            }

            ul.social-menu li {
                display: inline-block;
                line-height: 1;
                float: none;
                margin: 0px 5px -1px 0;
            }

            .content_all h5 {
                text-align: center;
            }

            .followers-block {
                margin: 0 auto
            }

            .about-me {
                margin: 80px 0 55px 0;
            }

            .about-me-left {
                float: none !important;
                width: 100% !important;
                padding: 49px 20px;
                height: auto;
            }

            .about-me-right {
                float: none !important;
                width: 90% !important;
                padding: 14px 20px;
                height: auto;
                position: relative;
                right: 0;
                top: -37px;
                margin: 0 auto;
            }

            .about-me h3 {
                font-size: 50px;
            }

            .extra-work-extra-block p {
                width: 94%;
                margin-left: 61px !important;
                position: relative;
                display: block;
            }

            .extra-work-extra-block {
                margin: -28px 0 60px 0;
                padding: 12px 20px 0px 20px;
                width: 100%;
                background: #f72e5e;
            }

            .extra-work-extra-block h3 {
                margin: 15px 0 22px -31px;
            }

            .video-about-left {
                float: none !important;
                width: 100% !important;
                padding: 0 10px;
            }

            .video-about-right {
                float: none !important;
                width: 100% !important;
                padding: 0 10px;
            }

            .video-about-left h3 {
                margin: 25px 0 20px 0;
                position: relative;
            }

            .video-about-left p {
                margin-bottom: 36px;
            }

            .video-about-right video {
                height: 210px !important;
            }

            .what-we-do-main p.btm02 {
                font-size: 19px;
                width: 98%;
                line-height: 27px;
            }

            .what-we-do-block-left {
                min-height: 176px;
                float: left;
                width: 99%;
                padding: 20px 11px 0 11px;
            }

            .what-we-do-block-left p {
                font-size: 17px;
                padding: 0 15px 10px 15px;
                line-height: 23px;
                margin: 0;
            }


            .pro-team h6 {
                font-size: 16px;
                margin: -24px 0 10px 0;
            }

            .pro-team-slide-ind a {
                width: 48%;
                float: left;
                margin: 0 1% 7px 1%;
            }

            .pro-teams.gallery {
                margin: 0 -15px;
            }

            .cd-011 {
                width: 100% !Important;
                float: none !Important;
                margin: 0 0% 24px 0% !Important;
                height: auto !Important;
                overflow: hidden !Important;
            }

            .video-bg-bottom .slide-ind {
                padding: 0 4px 0 5px !Important;
            }

            .project-work-extra-block p {
                font-size: 16px !Important;
                color: #000 !Important;
                margin-bottom: 22px !Important;
                margin-top: 0px !Important;
                float: right !Important;
                padding: 20px 10px 10px 10px !Important;
                width: 100% !Important;
                line-height: 26px !Important;
            }

            .project-work-extra-block img {
                position: relative !Important;
                top: 0 !Important;
                left: 0 !Important;
                width: 100% !important;
                margin: 0 auto !Important;
            }

            .project-work-extra-block {
                height: auto !Important;
                margin: -29px 0 60px 0 !Important;
                padding: 33px 20px 0px 20px !Important;
            }

            .contact-new-last p {
                width: 86% !Important;
            }

            .extra-work-extra-block a {
                margin-top: 10px;
                width: 146px;
                line-height: 41px;
                display: block;
                color: #f72e5e;
                background: #fff;
                text-align: Center;
                padding-top: 4px;
            }

            #contact-and-email {
                width: 90% !Important;
            }

            .contact-new-last a {
                width: 48% !Important;
                margin: 0 1% !Important;
                font-size: 14px !important;
            }

        }

        .row-all {
            padding: 0 15px;
            margin: 0 15px;
            clear: both;
            overflow: hidden;
        }

        .extra-work-extra-block a {
            margin-top: 10px;
    width: 146px;
    line-height: 41px;
    display: flow-root;
    color: #f72e5e;
    background: #fff;
    text-align: Center;
    padding-top: 4px;
        }
        
        .content_all h5 {
    text-transform: capitalize;
}
    </style>
      @if(!$artist->isDeleted)  
    <div class="container2">
        <div class="container">
            <div class="row-all">
                <div class="background">
                    <div class="background-insde">
                        <img src="<?php echo count($artist_about) > 0 ? (isset($artist_about[0]->profile_photo) ? $artist_about[0]->profile_photo : '') : ''; ?>">
                    </div>
                </div>
                <div class="content_all">
                    <h3><?php echo count($artist_about) > 0 ? (isset($artist_about[0]->profile_name) ? $artist_about[0]->profile_name : '') : ''; ?></h3>
                    <h5> Category: <?php echo count($artist_about) > 0 ? (isset($artist_about[0]->multiple_category) ? $artist_about[0]->multiple_category : '') : ''; ?></h5>
                    <h5>Tags: {{ is_var_exist($artist_about, 'tags') ? $artist_about[0]->tags : '' }}</h5>
                    <br>
                    <ul class="social-menu tranz">
                        <li class="sprite-imdb">
                            <a target="_blank" title="" href="<?php echo count($artist_contact) > 0 ? (isset($artist_contact[0]->facebook) ? $artist_contact[0]->facebook : '') : ''; ?>">
                                <i class="fa fa-facebook"></i>
                                <span>Facebook</span>
                            </a>
                        </li>
                        <li class="sprite-instagram">
                            <a target="_blank" href="
              <?php echo count($artist_contact) > 0 ? (isset($artist_contact[0]->instagram) ? $artist_contact[0]->instagram : '') : ''; ?>">
                                <i class="fa fa-instagram"></i>
                                <span>Instagram</span>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" title="" href="
              <?php echo count($artist_contact) > 0 ? (isset($artist_contact[0]->youtube) ? $artist_contact[0]->youtube : '') : ''; ?>
              ">
                                <i class="fa fa-youtube-play"></i>
                                <span>Youtube</span>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" title="" href="{{ user_url($user->slug) }}" class="entertainment_icon-1">
                                <img style="width:20px;margin:-2px 5px 0 -4px;float: left;" src="http://entertainmentbugs.com/public/img/logo_footer.png">
                                <span style="line-height: 14px;display: inline-block;padding: 5px 0 0 0;">Entertainment Profile</span>
                            </a>
                        </li>
                        <li>
                            <a target="_blank" title="" href="mailto:<?php echo count($artist_contact) > 0 ? (isset($artist_contact[0]->email) ? $artist_contact[0]->email : '') : ''; ?>">
                                <i class="fa fa-inbox"></i>
                                <span> Contact Me</span>
                            </a>
                        </li>
                    </ul>
                    <div class="followers-block">
                        <img
                            src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-1-400x400.jpg">
                        <img
                            src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-2-400x400.jpg">
                        <img
                            src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-3-400x400.jpg">
                        <img
                            src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-4-400x400.jpg">
                        <img
                            src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-5-400x400.jpg">
                        <p>
                            <span data-follower-count data-user-id="{{ $user->id }}" data-template="with-count">{{ $user->getFollowerCount() }}</span> Followers
                        </p>

                        @if ( Session::get("user_id") && $user->isFollowedBy() )
                            <div class="channels-card-image-btn" id="suggested_unfollow_user{{ $user->id }}">
                                <a data-do="unfollow" data-user-id="{{ $user->id }}" data-template="with-count" style="background-color: #f72e5e!important;"><i class="fa fa-solid fa-heart"></i> Following </a>
                            </div>
                        @else
                            <div class="channels-card-image-btn" id="suggested_follow_user{{ $user->id }}">
                                <a data-do="follow" data-user-id="{{ $user->id }}" data-template="with-count" class="flw"><i class="fa fa-heart-o"></i> Follow me </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/85188/jquery.ripples-min.js"></script>
    <Script>
        $('.container2').ripples({
            resolution: 400
        });
    </script>
    <div class="container">
        <div class="content_alls">
            <div class="about-me">
                <div class="about-me-left">
                    <h3>About Me</h3>

                    <p>

                        <?php echo count($artist_about) > 0 ? (isset($artist_about[0]->about_me) ? $artist_about[0]->about_me : '') : ''; ?>
                    </p>
                </div>
                <div class="about-me-right">
                    <p>
                        <strong>Age: </strong>

                        <?php echo count($artist_about) > 0 ? (isset($artist_about[0]->age) ? $artist_about[0]->age : '') : ''; ?>
                        years
                    <p>
                        <strong>Gender: </strong>

                        <?php echo count($artist_about) > 0 ? (isset($artist_about[0]->gender) ? $artist_about[0]->gender : '') : ''; ?>
                    </p>
                    <p>
                        <strong>Location: </strong>
                        <?php echo count($artist_about) > 0 ? (isset($artist_about[0]->city) ? $artist_about[0]->city : '') : ''; ?>

                    </p>
                    <p>
                        <strong>Language: </strong>
                        <?php echo count($artist_about) > 0 ? (isset($artist_about[0]->language) ? $artist_about[0]->language : '') : ''; ?>

                    </p>
                    <p>
                        <strong>Height: </strong>
                        <?php echo count($artist_about) > 0 ? (isset($artist_about[0]->height) ? $artist_about[0]->height : '') : ''; ?>

                    </p>
                    <p>
                        <strong>Weight: </strong>
                        <?php echo count($artist_about) > 0 ? (isset($artist_about[0]->weight) ? $artist_about[0]->weight : '') : ''; ?>
                        Kg"
                    </p>
                    <p>
                        <strong>Experience: </strong>
                        <?php echo count($artist_about) > 0 ? (isset($artist_about[0]->exp) ? $artist_about[0]->exp : '') : ''; ?>
                        years
                    </p>
                </div>
            </div>
        </div>
    </div>
    </div>
    @if( $artist_what_we_do && count($artist_what_we_do) > 0 && isset($artist_what_we_do[0]) && !empty($artist_what_we_do[0]->what_we_do) )
        <div class="what-we-do-main">
            <div class="container">
                <div class="what-we-do-block">
                    <h3>{{ $artist_what_we_do[0]->what_we_do }}</h3>
                    <p class="btm02">{{ $artist_what_we_do[0]->description }}</p>
                    <div class="what-we-do-block-left">
                        <h4>{{ $artist_what_we_do[0]->title_1 }}</h4>
                        <p>{{ $artist_what_we_do[0]->des_1 }}</p>
                    </div>
                    <div class="what-we-do-block-left">
                        <h4>{{ $artist_what_we_do[0]->title_2 }}</h4>
                        <p>{{ $artist_what_we_do[0]->des_2 }}</p>
                    </div>
                    <div class="what-we-do-block-left">
                        <h4>{{ $artist_what_we_do[0]->title_3 }}</h4>
                        <p>{{ $artist_what_we_do[0]->des_3 }}</p>
                    </div>
                    <div class="what-we-do-block-left">
                        <h4>{{ $artist_what_we_do[0]->title_4 }}</h4>
                        <p>{{ $artist_what_we_do[0]->des_4 }}</p>
                    </div>
                    <?php if((count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_5) ? $artist_what_we_do[0]->title_5 : '')  : '' ) !=null || 
            (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_5) ? $artist_what_we_do[0]->des_5 : '')  : '' )!=null){ ?>
                    <div class="what-we-do-block-left">
                        <h4>

                            <?php echo count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_5) ? $artist_what_we_do[0]->title_5 : '') : ''; ?>
                        </h4>

                        <p>

                            <?php echo count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_5) ? $artist_what_we_do[0]->des_5 : '') : ''; ?>
                        </p>
                    </div>
                    <?php } ?>
                    <?php if(
            (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_6) ? $artist_what_we_do[0]->title_6 : '')  : '' ) !=null || 
            (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_6) ? $artist_what_we_do[0]->des_6 : '')  : '' )!=null){ ?>
                    <div class="what-we-do-block-left">
                        <h4>

                            <?php echo count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_6) ? $artist_what_we_do[0]->title_6 : '') : ''; ?>
                        </h4>
                        <p>

                            <?php echo count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_6) ? $artist_what_we_do[0]->des_6 : '') : ''; ?>
                        </p>
                    </div>
                    <?php } ?>
                    <?php if((count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_7) ? $artist_what_we_do[0]->title_7 : '')  : '' ) !=null || (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_7) ? $artist_what_we_do[0]->des_7 : '')  : '' ) !=null){ ?>
                    <div class="what-we-do-block-left">
                        <h4>

                            <?php echo count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_7) ? $artist_what_we_do[0]->title_7 : '') : ''; ?>
                        </h4>
                        <p>

                            <?php echo count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_7) ? $artist_what_we_do[0]->des_7 : '') : ''; ?>
                        </p>
                    </div>
                    <?php } ?>
                    <?php if((count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_8) ? $artist_what_we_do[0]->title_8 : '')  : '' ) !=null || (count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_8) ? $artist_what_we_do[0]->des_8 : '')  : '' ) !=null){ ?>
                    <div class="what-we-do-block-left">
                        <h4>

                            <?php echo count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->title_8) ? $artist_what_we_do[0]->title_8 : '') : ''; ?>
                        </h4>
                        <p>

                            <?php echo count($artist_what_we_do) > 0 ? (isset($artist_what_we_do[0]->des_8) ? $artist_what_we_do[0]->des_8 : '') : ''; ?>
                        </p>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    @endif
    @if( $artist_achievment && count($artist_achievment) > 0 )
        <div class="container">
            <div class="extra-work-extra-block">
                <h3>Achievements</h3>
                @foreach($artist_achievment as $ac)
          <p>
                  <strong>   
                        <span style="display: block; margin-bottom: 5px; float:left;margin-right:20px;">
                        @if( isset( $ac->photo_1 ) && !(empty($ac->photo_1)) )
                            <img src="<?= $ac->photo_1 ?>" style="width: 150px; height: 150px" />
                        @endif
                    </span>    
                    
                    
                   <?php echo count($artist_achievment) > 0 ? (isset($ac->title_1) ? $ac->title_1 : '') : ''; ?> - <?php
                        if ($ac->date_1 != '' || $ac->date_1 != null) {
                        echo date('d F, Y', strtotime(count($artist_achievment) > 0 ? (isset($ac->date_1) ? $ac->date_1 : '') : ''));
                        }
                        ?>
                    </strong>
                  
                    <?php echo count($artist_achievment) > 0 ? (isset($ac->des_1) ? $ac->des_1 : '') : ''; ?>
                    <?php if($ac->link_1 != '' || $ac->link_1 != NULL) { ?>
                    <a href="<?= (isset($ac->link_1) ? $ac->link_1 : ''); ?>">View Now</a>
                    <?php } ?>
                </p>
                @endforeach
            </div>
        </div>
    @endif
    @if( $artist_showreel && count($artist_showreel) > 0 )
        <div class="show-real" style="background:#f4f3f3">
            <div class="container">
                <div class="video-about">
                    <div class="video-about-left">
                        <h3> Showreel</h3>
                        <p><?php echo count($artist_showreel) > 0 ? (isset($artist_showreel[0]->description) ? $artist_showreel[0]->description : '') : ''; ?>
                        </p>
                    </div>
                    <div class="video-about-right">
                        <video width="100%" height="340" poster="<?php echo count($artist_showreel) > 0 ? (isset($artist_showreel[0]->video_thumb) ? $artist_showreel[0]->video_thumb : '') : ''; ?>" controls>
                            <source src="<?php echo count($artist_showreel) > 0 ? (isset($artist_showreel[0]->video) ? $artist_showreel[0]->video : '') : ''; ?>" type="video/mp4">
                            <source src="<?php echo count($artist_showreel) > 0 ? (isset($artist_showreel[0]->video) ? $artist_showreel[0]->video : '') : ''; ?>" type="video/ogg"> Your browser does not support the video tag.
                        </video>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <link href='https://www.jqueryscript.net/demo/Responsive-Touch-enabled-jQuery-Image-Lightbox-Plugin/dist/simple-lightbox.min.css' rel='stylesheet' type='text/css'>
    @if( $artist_gallery && count($artist_gallery) > 0 )
        <div class="pro-team">
            <div class="container">
                <h3>Gallery</h3>
                <div class="container">
                    <div class="pro-teams gallery">
                        <div class="pro-team-slide-ind">
                            <?php 
                    foreach($artist_gallery as $ag){
                ?>
                <a href="<?php echo count($artist_gallery) > 0 ? (isset($ag->photo) ? $ag->photo : '') : ''; ?>
                "><img src="<?php echo count($artist_gallery) > 0 ? (isset($ag->photo) ? $ag->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : ''; ?>
                " alt="<?php echo count($artist_gallery) > 0 ? (isset($ag->photo_title) ? $ag->photo_title : '') : ''; ?>
                " title="<?php echo count($artist_gallery) > 0 ? (isset($ag->photo_title) ? $ag->photo_title : '') : ''; ?>">
                <h6><?php echo count($artist_gallery) > 0 ? (isset($ag->category) ? $ag->category : '') : ''; ?></h6></a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if( $artist_video && count($artist_video) > 0 )
        <div class="video-bg-bottom" style="background:#f4f3f3">
            <div class="container">
                <div class="rows">
                    <div class="content_alls">
                        <h3> Video</h3>
                        <div class="slide-ind">
                            <?php
                            $count=0;
                foreach($artist_video as $av) { ?>
                            <div class="cd-011 <?php if($count%3==0) {?> margin-right-off <?php } ?>">
                                <video class="" poster="<?php echo count($artist_video) > 0 ? (isset($av->thumb) ? $av->thumb : '') : ''; ?>" controls>
                                    <source src="<?php echo count($artist_video) > 0 ? (isset($av->video) ? $av->video : '') : ''; ?>" type="video/mp4">
                                    <source src="<?php echo count($artist_video) > 0 ? (isset($av->video) ? $av->video : '') : ''; ?>" type="video/ogg"> Your browser does not support the video tag.
                                </video>
                                <div class="inside">
                                    <small>
                                        <?php echo count($artist_video) > 0 ? (isset($av->category) ? $av->category : '') : ''; ?>
                                    </small>
                                    <h6>
                                        <?php echo count($artist_video) > 0 ? (isset($av->video_title) ? $av->video_title : '') : ''; ?>
                                    </h6>
                                    <p>
                                        <?php echo count($artist_video) > 0 ? (isset($av->description) ? $av->description : '') : ''; ?>
                                    </p>
                                </div>
                            </div>
                            <?php 
                $count=$count+1;
                } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if( $artist_project && count($artist_project) > 0 )
        <div class="produciton-project">
            <div class="container">
                <h3 style="color:#f72e5e">Latest Projects</h3>
                <?php 
			
        foreach($artist_project as $ap) {
        ?>
                <div class="project-work-extra-block">
				<?php if($ap->photo!=''){ ?>
                    <img src="<?php echo count($artist_project) > 0 ? (isset($ap->photo) ? $ap->photo : '') : ''; ?>" style="width:250px">
					<?php 
				}
				else
				{
					?>
					<style>
					.project-work-extra-block p{width:100%}
					</style>
					<?php 
				}
				?>
                    <p ><?php if(count($artist_project) > 0){
                if($ap->date != '')
                {
            ?>
                        <strong>
                            <?php echo count($artist_project) > 0 ? (isset($ap->title) ? $ap->title : '') : ''; ?>
                            - <?php
                            echo date('d F, Y', strtotime(count($artist_project) > 0 ? (isset($ap->date) ? $ap->date : '') : ''));
                            ?>
                        </strong>
                        <?php } ?>
                        <?php } ?>
                        <?php echo count($artist_project) > 0 ? (isset($ap->description) ? $ap->description : '') : ''; ?>

                        @if( isset($ap->link) && !(empty($ap->link)) )
                            <a href="<?php echo count($artist_project) > 0 ? (isset($ap->link) ? $ap->link : '') : ''; ?>"><?php echo count($artist_project) > 0 ? (isset($ap->button_name) ? $ap->button_name : '') : ''; ?></a>
                        @endif
                    </p>
                </div>
                <?php } ?>
            </div>
        </div>
    @endif
    <div class="contact-new-last">
        <div class="container">
            <h3>
                <?php echo count($artist_contact) > 0 ? (isset($artist_contact[0]->title) ? $artist_contact[0]->title : '') : ''; ?>
            </h3>
            <p><?php echo count($artist_contact) > 0 ? (isset($artist_contact[0]->description) ? $artist_contact[0]->description : '') : ''; ?></p>
            <div id="contact-and-email">
                <a href="mailto:<?php echo count($artist_contact) > 0 ? (isset($artist_contact[0]->email) ? $artist_contact[0]->email : '') : ''; ?>">
                    <i class="fa fa-envelope"></i> Email me </a>
                <a href="https://wa.me/<?php echo count($artist_contact) > 0 ? (isset($artist_contact[0]->phone) ? $artist_contact[0]->phone : '') : ''; ?>">
                    <i class="fa fa-commenting-o"></i> Chat with me </a>
            </div>
        </div>
    </div>
    </div>
    @else
    <div class="under-review text-center"><h5>This artist is under review</h5></div>

    @endif
    <script type="text/javascript" src="https://www.jqueryscript.net/demo/Responsive-Touch-enabled-jQuery-Image-Lightbox-Plugin/dist/simple-lightbox.jquery.min.js">
    </script>
    <script>
        $(function() {
            var gallery = $('.gallery a').simpleLightbox({
                navText: ['&lsaquo;', '&rsaquo;']
            });
        });
    </script>
@endsection
