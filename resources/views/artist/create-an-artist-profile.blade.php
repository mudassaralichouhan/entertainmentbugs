@extends('layouts.home')
@section('content')
<style>
#brand_collaboration h2 {font-size: 22px;margin: 0px 0 20px 0;}
#quote  h2 {font-size: 22px;margin: 0px 0 20px 0;}
.modal-header .close {margin-top: -30px;}
.modal-body {position: relative;padding-bottom:0px;}
#profile_only_mobile {padding: 25px 25px !important;background: #eaeaea !important;width: 45%;margin-right: 5%;}
.cover-photo-mobile-show {padding: 25px 25px !important;background: #eaeaea !important;}
.video-p .u-close{    float: right;margin: -43px -32px 0px 0;}
.modal-footer {padding: 6px 15px 11px 15px;}

#option-select{    clear: both;overflow: hidden;padding: 20px 30px;background: rgb(244, 243, 243);margin-bottom: 25px;}
#option-select input[type="checkbox"] {margin: 2px 0 0;}


#live-video-user-list-home{display:none;}
#contact h2 {font-size: 22px;margin: 0 0 20px 0;}
.form-group input, .form-group select {height: 42px;font-size: 12px;border: solid 1px #e0e1e2;}
#create-profile{margin:40px 0}
#top-check-field0 h2{font-size:22px; margin:0 0 0 0}
.option-block{float:left; width:33.3%}
.field strong{margin-top:10px;display: block;font-weight:600;font-family: 'Hind Guntur', sans-serif;}
#about label{font-weight:500;font-family: 'Hind Guntur', sans-serif;margin:0px 0 3px 0;}
#about .form-group {margin-bottom: 18px;}
#about textarea {padding-top: 10px;font-size: 13px;}
#achievements-blocks0 label{font-weight:600;font-family: 'Hind Guntur', sans-serif;margin:0px 0 3px 0;}
#achievements-blocks0  .form-group {margin-bottom: 18px;}
#achievements-blocks0  textarea {padding-top: 10px;font-size: 13px;}
#achievements-blocks0 h2{font-size:22px; margin:0 0 20px 0}
#about h2{font-size:22px; margin:0 0 20px 0}
#portfolio-block h2{font-size:22px; margin:0 0 20px 0}
#portfolio-block label {font-weight: 600;font-family: 'Hind Guntur', sans-serif;margin: 0px 0 3px 0;}
.btn-cv1 {padding: 11px;}
#what-we-de-bl label{font-weight:600;font-family: 'Hind Guntur', sans-serif;margin:0px 0 3px 0;}
#what-we-de-bl .form-group {margin-bottom: 18px;}
#what-we-de-bl  textarea {padding-top: 10px;font-size: 13px;}
#what-we-de-bl h2{font-size:22px; margin:0 0 20px 0}
.tab4{background: #eceff0; margin-bottom:0px;clear:both; overflow:hidden;} 
.tab4 a {padding: 8px 20px 12px 20px;text-align: Center;color: #000;line-height: 43px;}
.tab4 a.active{    background: #28b47e !important;color:#fff;}
.w3-green, .w3-hover-green:hover {color: #fff!important;background-color: #f72e5e !important;text-align: Center;line-height: 17px;padding: 3px 0 0 0;}
.w3-light-grey {color: #000!important;margin-top:8px;background-color: #eceff0 !important;}
.edit-page .u-area {text-align: center;padding-top: 12px;padding-bottom: 20px;}
#select-p i{color: #637076;font-size: 36px;text-align: center;display: block;padding: 32px 0 0 0;}
.upload-page .u-area i {font-size: 90px;color: #637076;}
.upload-page .u-area .u-text1 {margin-top: 10px;margin-bottom: 9px;font-size: 14px;}
#portfolio-block .browse-photo{width:100px; height:100px; background:#eceff0}
#portfolio-block .form-group {margin-bottom: 10px;}
#portfolio-block .col-lg-6{margin:5px 0 20px 0;}
#portfolio-block .btn-save{margin-top:40px;}
#showreel-block h2{font-size:22px; margin:0 0 20px 0}
#showreel-block label {font-weight: 600;font-family: 'Hind Guntur', sans-serif;margin: 0px 0 3px 0;}
#portfolio-block .progress {width: 90%;margin-bottom: 0;height: 8px;background-color: #eceff0;
-webkit-box-shadow: none;box-shadow: none;}
    .progress-bar1, .progress-bar2,.progress-bar3,.progress-bar4,.progress-bar5,.progress-bar6{
    float: left;
    width: 0%;
    height: 100%;
    font-size: 14px;
    line-height: 22px;
    color: #fff;
    text-align: center;
    background-color: #337ab7;
    -webkit-box-shadow: inset 0 -1px 0 rgb(0 0 0 / 15%);
    box-shadow: inset 0 -1px 0 rgb(0 0 0 / 15%);
    -webkit-transition: width .6s ease;
    -o-transition: width .6s ease;
    transition: width .6s ease;
    }
   #portfolio-block label {
    font-size: 13px;
}
    #portfolio-block .col-lg-6.top_block {
    border: solid 1px #e0e1e2;
    width: 32.3%;
    float: left;
    padding: 20px 15px 2px 18px;
    margin: 0 1% 11px 0;
    background: rgb(244, 243, 243);
    border-radius: 10px;
}

#portfolio-block .col-lg-6.new_video_block31{
    width: 49%;
    float: left;
    padding: 20px 15px 10px 18px;
    margin: 0 1% 11px 0 !important;
    background: rgb(244, 243, 243);
    border-radius: 10px;
}

#what-we-de-bl .col-lg-3{
    width: 24%;
    float: left;
    padding: 20px 15px 10px 18px;
    margin: 0 1% 11px 0;
    background: rgb(244, 243, 243);
    border-radius: 10px;
}

    #portfolio-block .col-lg-6.top_block input{padding-top: 15px;}

    #portfolio-block .cgt{position: absolute;top: -6px;right: 21px;}
    .u-progress{position:relative; margin-top:10px;}
    .tab {overflow: hidden;border: 1px solid #ccc;background-color: #f1f1f1;}
    .tab button {background-color: inherit;float: left;border: none;outline: none;cursor: pointer;padding: 16px 18px 10px 18px;transition: 0.3s;font-size: 12px;}
    .tab button:hover {background-color: #ddd;}
    .tab button.active {background-color: #ccc;}
    .tabcontent {padding: 6px 12px;border:px solid #ccc;border-top: none;display:none;}
    .tabcontent{margin-top:10px;}
    label {font-weight: 500;}
    
    
  
  @media  only screen and (max-width: 767px) {
      
      
      
      
      
.tabcontent {padding: 6px 0px !Important;}
#about .form-group {   margin-bottom: 18px;padding: 0 2px !important;}
#what-we-de-bl .form-group {padding: 0;}
#achievements-blocks0  .form-group{padding: 0;}
#showreel-block .form-group{padding: 0;}
.option-block {width: 100%;}
.photo-up01{padding: 10px 0 !Important;}
.tb_mobile_scroll{width: 100%;overflow: scroll;}
.tab.tab4{width: 1320px;}
#what-we-de-bl .col-lg-3 {width: 100%  !Important;float: none !Important;}
#portfolio-block .col-lg-6.top_block {width: 100% !Important;    margin: 0 0% 11px 0} 
      
      .logo-01:nth-child(odd), .logo-01:nth-child(even) {
    width: 100% !important;margin-bottom:10px;
    float: none;
}
      
      
  }
</style>
<div class="content-wrapper">
    <div class="audition_main_list" id="auditions" style="background:#f4f3f3">
        <div id="create-profile-tab">
            <div class="container">
                @php
                    $username = session()->get('user_name');
                    $userId = session()->get('user_id');
                    $user = \App\Models\User::find($userId);
                    $slug = null;
                    $getIfAlready = DB::select('select * from artist_about where user_id='.$userId);
                    if( $getIfAlready && isset($getIfAlready[0]) ){
                        $slug = $getIfAlready[0]->username;
                    }
                    $hasThemePlan = $user->hasThemePlan();
                    
                    $whatWeDoLength = 4;
                    $whatWeDoPremiumLength = 8;
                    $achievementLength = 2;
                    $achievementPremiumLength = 5;
                    $galleryLength = 4;
                    $galleryPremiumLength = 12;
                    $videoLength = 3;
                    $videoPremiumLength = 6;
                    $latestProjectLength = 2;
                    $latestProjectPremiumLength = 3;
                @endphp
                <div class="rows tb_mobile_scroll" style="background:#fff;padding:0px; margin-top:10px">
                    <div class="tab tab4">
                        <button class="tablinks active" onclick="openCity(event, 'about')">About</button>
                        <button class="tablinks" onclick="openCity(event, 'what-we-de-bl')">What we do</button>
                        <button class="tablinks" onclick="openCity(event, 'achievements-blocks0')">Achiements</button>
                        <button class="tablinks" onclick="openCity(event, 'showreel-block')">Showreel</button>
                        <button class="tablinks" onclick="openCity(event, 'portfolio-block')">Gallery Image</button>
                        <button class="tablinks" onclick="openCity(event, 'video')">Video</button>
                        <button class="tablinks" onclick="openCity(event, 'latest-project')">Latest project</button>
                        <button class="tablinks" onclick="openCity(event, 'contact')">Contact</button>
                        @if( $hasThemePlan )
                            <button class="tablinks" onclick="openCity(event, 'brand_collaboration')">Brand Collboration</button>
                            <button class="tablinks" onclick="openCity(event, 'quote')">Quote</button>
                        @else
                            <a href="{{URL::to('theme-plan')}}" target="_blank" style="float: right;width: 160px;margin: 0;padding: 0 10px;margin-right:4px;font-size:12px;background: #eceff0 !important;color: #000;">Purchase Premium Theme</a>                   
                        @endif
                        <a href="{{URL::to('artist/'.$slug) }}" style="float: right;width: 130px;margin: 0;padding: 0 10px;font-size:12px; background: #28b47e !important;color: #fff;">Preview Profile</a>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="w3-light-grey">
                    <?php
                        function limit_text($text, $a, $limit) {
                            if (str_word_count($text, 0) > $limit) {
                                $words = str_word_count($text, 2);
                                $pos   = array_keys($words);
                                $text  = substr($text, 0, $pos[$limit]);
                            }
                            return $text;
                        }

                        $userid=session()->get('user_id');
                        $artist_about=DB::select('select * from artist_about where user_id='.$userid);
                        $artist_what_we_do=DB::select('select * from artist_what_we_do where user_id='.$userid);
                        $artist_achievment=DB::select('select * from artist_achievment where user_id='.$userid);
                        $artist_showreel=DB::select('select * from artist_showreel where user_id='.$userid);
                        $artist_gallery=DB::select('select * from artist_gallery where user_id='.$userid);
                        $artist_video=DB::select('select * from artist_video where user_id='.$userid);
                        $artist_project=DB::select('select * from artist_project where user_id='.$userid);
                        $artist_contact=DB::select('select * from artist_contact where user_id='.$userid);
                        $artist_brand= \App\Models\ArtistBrandCollaboration::with('brand_products')->where('user_id', $userid)->get();
                        $artist_quote = \App\Models\ArtistQuote::where('user_id', $userid)->first();
                        $progress = 0;
                        if( count($artist_about) > 0){
                        $progress += 12.5;
                        }
                        if(count($artist_what_we_do) > 0){
                        $progress += 12.5;
                        }
                        if(count($artist_achievment) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_showreel ) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_gallery ) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_video ) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_project ) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_contact ) >0){
                        $progress += 12.5;
                        }
                        ?>
                    <div class="w3-container w3-green w3-center" style="width: <?php echo $progress; ?>%">
                        <?php echo $progress; ?>%
						<?php if($progress > 80) {?>
						<style>
						#incomplete_process_div_a{display:none;}
						</style>
						<?php 
						}
						?>
                    </div>
                </div>
            </div>
            <div id="about"  class="tabcontent" style="display:block; margin-top:10px">
                <div class="container">
                    <div class="rows" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <h2>About <br> <small style="font-size:16px;">Create your profile as a artist</small></h2>
                        @if($message = Session::get('success')) <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </a>
                        <strong>Success!</strong> {{ $message }}
                      </div> @endif {!! Session::forget('success') !!}
                        <?php 
                            $userId = session()->get('user_id');
                            $getIfAlready = DB::select('select * from artist_about where user_id='.$userId);
                        ?>
                    <form action="{{URL::to('save-artist-profile')}}" id="artistForm" style="margin-top:0px" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="cropImage" id="cropImage" value="">
                            
                            <div class="form-group  col-lg-3 col-sm-6 ">
                                <label for="exampleInputEmail1">Profile Name*</label>
                                <input type="text" required name="profile_name" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->profile_name != NULL) ? $getIfAlready[0]->profile_name : '') : '')); ?>">
                            </div>
                            
                               <div class="form-group  col-lg-3 col-sm-6 ">
                                <label for="exampleInputEmail1">User name* </label>
                                <input type="text" required name="username" class="form-control" id="" placeholder="This username will be your profile url." value="{{ $getIfAlready[0]->username ?? '' }}">
                                <input type="hidden" name="current_user" value="{{ $getIfAlready[0]->username ?? '' }}" /> 

                            </div>
                            
                            <div class="form-group  col-lg-6 col-sm-6 ">
                                <label for="exampleInputEmail1">Profile Title (Tag Line)* - 25 words</label>
                                <input type="text" required name="profile_title" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->profile_title != NULL) ? limit_text($getIfAlready[0]->profile_title,0,25) : '') : '')); ?>">
                            </div>
                            
                             <div class="form-group col-lg-2 col-sm-6">
                                <label for="exampleInputEmail1">Gender*</label>
                                <select required style="width:100%;padding:0 10px" name="gender">
                                    <option value="Male" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->gender != NULL) ? (($getIfAlready[0]->gender == 'Male') ? 'selected' : '') : '') : ''); ?>>Male</option>
                                    <option value="Female" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->gender != NULL) ? (($getIfAlready[0]->gender == 'Female') ? 'selected' : '') : '') : ''); ?>>Female</option>
                                    <option value="Kids-Male" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->gender != NULL) ? (($getIfAlready[0]->gender == 'Kids-Male') ? 'selected' : '') : '') : ''); ?>>Kids - Male</option>
                                    <option value="Kids-Female" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->gender != NULL) ? (($getIfAlready[0]->gender == 'Kids-Female') ? 'selected' : '') : '') : ''); ?>>Kids - Female</option>
                                    <option value="Trans-Female" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->gender != NULL) ? (($getIfAlready[0]->gender == 'Trans-Female') ? 'selected' : '') : '') : ''); ?>>Trans Female</option>
                                    <option value="Trans-Male" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->gender != NULL) ? (($getIfAlready[0]->gender == 'Trans-Male') ? 'selected' : '') : '') : ''); ?>>Trans Male</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-sm-6">
                                <label for="exampleInputEmail1">Date Of Birth*</label>
                                <input type="date" class="form-control" name="dob" required id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->dob != NULL) ? $getIfAlready[0]->dob : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-2 col-sm-6">
                                <label for="exampleInputEmail1">Age*</label> 
                                <input type="number" name="age" min="1" class="form-control" required id="exampleInputEmail1" required value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->age != NULL) ? $getIfAlready[0]->age : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-2 col-sm-6">
                                <label for="exampleInputEmail1">Experience*</label>
                                <select name="exp" class="input-select" style="width:100%" required style="width:100%;padding:0 10px">
                                    <option value="">Select</option>
                                    <option value="new" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == 'new') ? 'selected' : '') : '') : ''); ?>>New</option>
                                    <option value="1" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '1') ? 'selected' : '') : '') : ''); ?>>1 years</option>
                                    <option value="2" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '2') ? 'selected' : '') : '') : ''); ?>>2 years</option>
                                    <option value="3" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '3') ? 'selected' : '') : '') : ''); ?>>3 years</option>
                                    <option value="4" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '4') ? 'selected' : '') : '') : ''); ?>>4 years</option>
                                    <option value="5" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '5') ? 'selected' : '') : '') : ''); ?>>5 years</option>
                                    <option value="6" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '6') ? 'selected' : '') : '') : ''); ?>>6 years</option>
                                    <option value="7" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '7') ? 'selected' : '') : '') : ''); ?>>7 years</option>
                                    <option value="8" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '8') ? 'selected' : '') : '') : ''); ?>>8 years</option>
                                    <option value="9" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '9') ? 'selected' : '') : '') : ''); ?>>9 years</option>
                                    <option value="10" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '10') ? 'selected' : '') : '') : ''); ?>>10 years</option>
                                    <option value="11" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '11') ? 'selected' : '') : '') : ''); ?>>11 years</option>
                                    <option value="12" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '12') ? 'selected' : '') : '') : ''); ?>>12 years</option>
                                    <option value="13" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '13') ? 'selected' : '') : '') : ''); ?>>13 years</option>
                                    <option value="14" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '14') ? 'selected' : '') : '') : ''); ?>>14 years</option>
                                    <option value="15" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '15') ? 'selected' : '') : '') : ''); ?>>15 years</option>
                                    <option value="16" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '16') ? 'selected' : '') : '') : ''); ?>>16 years</option>
                                    <option value="17" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '17') ? 'selected' : '') : '') : ''); ?>>17 years</option>
                                    <option value="18" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '18') ? 'selected' : '') : '') : ''); ?>>18 years</option>
                                    <option value="19" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '19') ? 'selected' : '') : '') : ''); ?>>19 years</option>
                                    <option value="20" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '20') ? 'selected' : '') : '') : ''); ?>>20 years</option>
                                    <option value="21" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '21') ? 'selected' : '') : '') : ''); ?>>21 years</option>
                                    <option value="22" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '22') ? 'selected' : '') : '') : ''); ?>>22 years</option>
                                    <option value="23" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '23') ? 'selected' : '') : '') : ''); ?>>23 years</option>
                                    <option value="24" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '24') ? 'selected' : '') : '') : ''); ?>>24 years</option>
                                    <option value="25" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '25') ? 'selected' : '') : '') : ''); ?>>25 years</option>
                                    <option value="26" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '26') ? 'selected' : '') : '') : ''); ?>>26 years</option>
                                    <option value="27" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '27') ? 'selected' : '') : '') : ''); ?>>27 years</option>
                                    <option value="28" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '28') ? 'selected' : '') : '') : ''); ?>>28 years</option>
                                    <option value="29" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '29') ? 'selected' : '') : '') : ''); ?>>29 years</option>
                                    <option value="30" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '30') ? 'selected' : '') : '') : ''); ?>>30 years +</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-sm-6">
                                <label for="exampleInputEmail1">Height* in ft</label>
                                <input type="text" name="height" required class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->height != NULL) ? $getIfAlready[0]->height : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-2 col-sm-6">
                                <label for="exampleInputEmail1">Weightin  kg</label>
                                <input type="text" name="weight" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->weight != NULL) ? $getIfAlready[0]->weight : '') : '')); ?>">
                            </div>
                            
                            

<div class="form-group col-lg-6 col-sm-6">
    <label> Language* (Multiple) all global language </label>   
    <input type="text" class="form-control" id ='video-language' name="language" required placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->language != NULL) ? $getIfAlready[0]->language : '') : '')); ?>">
</div>
   <div class="form-group col-lg-6 col-sm-6">
    <label>Tags / Skills</label>
    <input type="text" class="form-control" id ='about_tags' name="tags" required placeholder="" value="{{ is_var_exist($getIfAlready, 'tags') ? $getIfAlready[0]->tags : '' }}">
</div>         
                            
                            
                            <div class="form-group col-lg-4 col-sm-6">
                                <label for="exampleInputEmail1">Country* </label>
                                <div class="clear"></div>
                                <select id="country" name="country" required onchange="getStates(this.value)" class="select2 input-select" style="width:100%">
                                    <option value="">Select Country</option>
                                    <?php
                                        $getCountries = DB::table('tbl_countries')->get();
                                        foreach($getCountries as $k=>$v)
                                        {
                                        ?>
                                    <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->country != NULL) ? (($getIfAlready[0]->country == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-sm-6">
                                <label for="exampleInputEmail1">Location (State)*</label>
                                <!--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="">-->
                                <select class="select2 input-select" style="width:100%" onchange="getCities(this.value)" required id="state" name="state">
                                    <option value="">Select State</option>
                                    <?php
                                        if(count($getIfAlready) > 0)
                                        {
                                            if($getIfAlready[0]->country != NULL || $getIfAlready[0]->country != '')
                                            {
                                                $getCountryId = DB::table('tbl_countries')->where('name',$getIfAlready[0]->country)->get();
                                                $getStates = DB::table('tbl_states')->where('country_id',$getCountryId[0]->id)->get();
                                                foreach($getStates as $k=>$v)
                                                {
                                        ?>
                                    <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->state != NULL) ? (($getIfAlready[0]->state == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                                    <?php } ?>
                                    <?php } } ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-sm-6">
                                <label for="exampleInputEmail1">City*</label>
                                <select class="select2 input-select" style="width:100%" required id="city" name="city">
                                    <option value="">Select City</option>
                                    <?php
                                        if(count($getIfAlready) > 0)
                                        {
                                            if($getIfAlready[0]->state != NULL || $getIfAlready[0]->state != '')
                                            {
                                                $getStateId = DB::table('tbl_states')->where('name',$getIfAlready[0]->state)->get();
                                                $getCities = DB::table('tbl_cities')->where('state_id',$getStateId[0]->id)->get();
                                                foreach($getCities as $k=>$v)
                                                {
                                        ?>
                                    <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->city != NULL) ? (($getIfAlready[0]->city == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                                    <?php } ?>
                                    <?php } } ?>
                                </select>
                            </div>
                           
                         
                            <div class="form-group col-lg-12">
                                <label for="exampleInputEmail1"> About Me* (100 words)</label>
                                <textarea id="w3review" required onKeyPress="return check(event,value)" onInput="checkLength(100,this)" name="about_me" rows="4" cols="60"><?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->about_me != NULL) ? limit_text($getIfAlready[0]->about_me,0,100) : '') : '')); ?></textarea> 
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <p style="padding:0 15px"> Select upto 5 <strong>multiple options,</strong> which  represents to your profile.</p>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="two fields" id="option-select">
                                <div class="field">
                                    <div class="option-block">
                                        <?php 
                                            $categoryarr = [];
                                            if(count($getIfAlready) > 0)
                                            {
                                                if($getIfAlready[0]->multiple_category != NULL)
                                                {
                                                    $explode = explode(',',$getIfAlready[0]->multiple_category);
                                                    if(count($explode) > 0)
                                                    {
                                                        foreach($explode as $k=>$v)
                                                        array_push($categoryarr,$v);
                                                    }
                                            ?>
                                        <?php    
                                            }
                                            }
                                            ?>
                                        <div class="field "> <input name="artistcheck[]" type="checkbox" value="Theater" <?php echo ((count($categoryarr) > 0) ? ((in_array('Theater',$categoryarr)) ? 'checked' : '') : ''); ?>> Theater </div>
                                        <div class="field ">  <input name="artistcheck[]" type="checkbox" value="Voiceover" <?php echo ((count($categoryarr) > 0) ? ((in_array('Voiceover',$categoryarr)) ? 'checked' : '') : ''); ?>> Voiceover   </div>
                                        <div class="field "> <input name="artistcheck[]" type="checkbox" value="Comedian" <?php echo ((count($categoryarr) > 0) ? ((in_array('Comedian',$categoryarr)) ? 'checked' : '') : ''); ?>> Comedian   </div>
                                        <div class="field ">  <input name="artistcheck[]" type="checkbox" value="Photography" <?php echo ((count($categoryarr) > 0) ? ((in_array('Photography',$categoryarr)) ? 'checked' : '') : ''); ?>> Photography   </div>
                                        <div class="field "> <input name="artistcheck[]" type="checkbox" value="Video Grapher" <?php echo ((count($categoryarr) > 0) ? ((in_array('Video Grapher',$categoryarr)) ? 'checked' : '') : ''); ?>> Video Grapher </div>
                                        <div class="field ">   <input name="artistcheck[]" type="checkbox" value="Makeup Artrist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Makeup Artrist',$categoryarr)) ? 'checked' : '') : ''); ?>> Makeup Artrist  </div>
                                        <div class="field ">
                                            <strong>Actor / Model</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Actor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Actor',$categoryarr)) ? 'checked' : '') : ''); ?>> Actor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Child Actor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Child Actor',$categoryarr)) ? 'checked' : '') : ''); ?>> Child Actor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Comedy Actor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Comedy Actor',$categoryarr)) ? 'checked' : '') : ''); ?>> Comedy Actor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Junior Actor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Junior Actor',$categoryarr)) ? 'checked' : '') : ''); ?>> Junior Actor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Stunt Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Stunt Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Stunt Artist
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Drama Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Drama Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Drama Artist
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Mimicry Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Mimicry Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Mimicry Artist
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Background / Atmosphere" <?php echo ((count($categoryarr) > 0) ? ((in_array('Background / Atmosphere',$categoryarr)) ? 'checked' : '') : ''); ?>> Background / Atmosphere 
                                        </div>
                                        <div class="field ">
                                            <strong>Modeling</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Model" <?php echo ((count($categoryarr) > 0) ? ((in_array('Model',$categoryarr)) ? 'checked' : '') : ''); ?>> Model
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Child Model" <?php echo ((count($categoryarr) > 0) ? ((in_array('Child Model',$categoryarr)) ? 'checked' : '') : ''); ?>> Child Model
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Pro Model" <?php echo ((count($categoryarr) > 0) ? ((in_array('Pro Model',$categoryarr)) ? 'checked' : '') : ''); ?>> Pro Model
                                        </div>
                                        <div class="field ">
                                            <strong> Dancer</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Dancer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Dancer',$categoryarr)) ? 'checked' : '') : ''); ?>> Dancer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Lead Dancer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Lead Dancer',$categoryarr)) ? 'checked' : '') : ''); ?>> Lead Dancer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Background Dancer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Background Dancer',$categoryarr)) ? 'checked' : '') : ''); ?>> Background Dancer
                                        </div>
                                        <div class="field ">
                                            <strong>Editor</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Video Editor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Video Editor',$categoryarr)) ? 'checked' : '') : ''); ?>> Video Editor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Music Editor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Music Editor',$categoryarr)) ? 'checked' : '') : ''); ?>> Music Editor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Audio Editor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Audio Editor',$categoryarr)) ? 'checked' : '') : ''); ?>> Audio Editor
                                        </div>
                                    </div>
                                    <div class="option-block">
                                        <div class="field ">
                                            <strong>Writer</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Dialogue Writer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Dialogue Writer',$categoryarr)) ? 'checked' : '') : ''); ?>> Dialogue Writer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Script Writer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Script Writer',$categoryarr)) ? 'checked' : '') : ''); ?>> Script Writer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Screeplay Writer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Screeplay Writer',$categoryarr)) ? 'checked' : '') : ''); ?>> Screeplay Writer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Story Writer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Story Writer',$categoryarr)) ? 'checked' : '') : ''); ?>> Story Writer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Lyricists" <?php echo ((count($categoryarr) > 0) ? ((in_array('Lyricists',$categoryarr)) ? 'checked' : '') : ''); ?>> Lyricists
                                        </div>
                                        <div class="vspace-small"></div>
                                        <div class="vspace-small"></div>
                                        <div class="vspace-small"></div>
                                        <div class="field ">
                                            <strong>Anchor / Presenter</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Video Jockey (VJ)" <?php echo ((count($categoryarr) > 0) ? ((in_array('Video Jockey (VJ)',$categoryarr)) ? 'checked' : '') : ''); ?>> Video Jockey (VJ)
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Radio Jockey (RJ)" <?php echo ((count($categoryarr) > 0) ? ((in_array('Radio Jockey (RJ)',$categoryarr)) ? 'checked' : '') : ''); ?>> Radio Jockey (RJ)
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="News Reader" <?php echo ((count($categoryarr) > 0) ? ((in_array('News Reader',$categoryarr)) ? 'checked' : '') : ''); ?>> News Reader
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Reporter" <?php echo ((count($categoryarr) > 0) ? ((in_array('Reporter',$categoryarr)) ? 'checked' : '') : ''); ?>> Reporter
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Journalist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Journalist',$categoryarr)) ? 'checked' : '') : ''); ?>> Journalist
                                        </div>
                                        <div class="field">
                                            <div class="ui field ">
                                                <strong>Off screen</strong>
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Dubbing Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Dubbing Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Dubbing Artist
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Voice Over Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Voice Over Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Voice Over Artist
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="ui field ">
                                                <strong style="background: #f72e5e;display: block;color: #fff;padding: 4px 10px 0 8px;margin: 4px 0 0 0;width: 78px;">Influncer</strong>
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="New Influncer" <?php echo ((count($categoryarr) > 0) ? ((in_array('New Influncer',$categoryarr)) ? 'checked' : '') : ''); ?>> New Influncer
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Experienced Influncer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Experienced Influncer',$categoryarr)) ? 'checked' : '') : ''); ?>> Experienced Influncer
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="ui field ">
                                                <strong>Advertising Professional	</strong>
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Video Advertising" <?php echo ((count($categoryarr) > 0) ? ((in_array('Video Advertising',$categoryarr)) ? 'checked' : '') : ''); ?>> Video Advertising
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Audio Advertising" <?php echo ((count($categoryarr) > 0) ? ((in_array('Audio Advertising',$categoryarr)) ? 'checked' : '') : ''); ?>>  Audio Advertising
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Banner Advertising" <?php echo ((count($categoryarr) > 0) ? ((in_array('Banner Advertising',$categoryarr)) ? 'checked' : '') : ''); ?>>  Banner Advertising
                                            </div>
                                            <div class="field ">
                                                <strong>Singer / Musician</strong>
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Singer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Singer',$categoryarr)) ? 'checked' : '') : ''); ?>> Singer
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Music Instrumentalist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Music Instrumentalist',$categoryarr)) ? 'checked' : '') : ''); ?>> Music Instrumentalist
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Music Composer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Music Composer',$categoryarr)) ? 'checked' : '') : ''); ?>> Music Composer
                                            </div>
                                        </div>
                                    </div>
                                    <div class="option-block">
                                        <div class="ui field ">
                                            <strong>Proffessional Artist</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Director Of Photography / Cinematographer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Director Of Photography / Cinematographer',$categoryarr)) ? 'checked' : '') : ''); ?>> Director Of Photography / Cinematographer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Cameraman" <?php echo ((count($categoryarr) > 0) ? ((in_array('Cameraman',$categoryarr)) ? 'checked' : '') : ''); ?>> Cameraman
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Camera Operator" <?php echo ((count($categoryarr) > 0) ? ((in_array('Camera Operator',$categoryarr)) ? 'checked' : '') : ''); ?>> Camera Operator
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Light man / Gaffer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Light man / Gaffer',$categoryarr)) ? 'checked' : '') : ''); ?>> Light man / Gaffer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Makeup Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Makeup Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Makeup Artist
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Visual Effects Editor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Visual Effects Editor',$categoryarr)) ? 'checked' : '') : ''); ?>> Visual Effects Editor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Digital imaging technician" <?php echo ((count($categoryarr) > 0) ? ((in_array('Digital imaging technician',$categoryarr)) ? 'checked' : '') : ''); ?>> Digital imaging technician
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Special Effects makeup Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Special Effects makeup Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Special Effects makeup Artist
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Motion control technician" <?php echo ((count($categoryarr) > 0) ? ((in_array('Motion control technician',$categoryarr)) ? 'checked' : '') : ''); ?>> Motion control technician
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Fashion Designer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Fashion Designer',$categoryarr)) ? 'checked' : '') : ''); ?>> Fashion Designer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Hair Stylist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Hair Stylist',$categoryarr)) ? 'checked' : '') : ''); ?>> Hair Stylist
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Costume designer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Costume Designer',$categoryarr)) ? 'checked' : '') : ''); ?>> Costume designer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Grip" <?php echo ((count($categoryarr) > 0) ? ((in_array('Grip',$categoryarr)) ? 'checked' : '') : ''); ?>> Grip
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Sound Designer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Sound Designer',$categoryarr)) ? 'checked' : '') : ''); ?>> Sound Designer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Sound Grip" <?php echo ((count($categoryarr) > 0) ? ((in_array('Sound Grip',$categoryarr)) ? 'checked' : '') : ''); ?>> Sound Grip
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Production Sound Mixer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Production Sound Mixer',$categoryarr)) ? 'checked' : '') : ''); ?>> Production Sound Mixer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Production Designer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Production Designer',$categoryarr)) ? 'checked' : '') : ''); ?>> Production Designer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Green man" <?php echo ((count($categoryarr) > 0) ? ((in_array('Green man',$categoryarr)) ? 'checked' : '') : ''); ?>> Green man
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Property master" <?php echo ((count($categoryarr) > 0) ? ((in_array('Property master',$categoryarr)) ? 'checked' : '') : ''); ?>> Property master
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Weapons master" <?php echo ((count($categoryarr) > 0) ? ((in_array('Weapons master',$categoryarr)) ? 'checked' : '') : ''); ?>> Weapons master
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Set Designer / Decorator" <?php echo ((count($categoryarr) > 0) ? ((in_array('Set Designer / Decorator',$categoryarr)) ? 'checked' : '') : ''); ?>> Set Designer / Decorator
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Location Manager" <?php echo ((count($categoryarr) > 0) ? ((in_array('Location Manger',$categoryarr)) ? 'checked' : '') : ''); ?>> Location Manager
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="StoryBoard Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('StoryBoard Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> StoryBoard Artist
                                        </div>
                                        <div class="vspace-small"></div>
                                        <div class="vspace-small"></div>
                                        <div class="vspace-small"></div>
                                        <div class="ui field ">
                                            <strong>Others</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Others" <?php echo ((count($categoryarr) > 0) ? ((in_array('Others',$categoryarr)) ? 'checked' : '') : ''); ?>> Others
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <p style="color:red">Select any one the category</p>
                            <div class="clear"></div>
                        
                            <div class="col-lg-6 photo-up01" id="profile_only_mobile">
                                <label for="exampleInputEmail1"> Upload profile photo <!-- 400px x 400px  Crop--> </label>
                                <input type="file" id="data-croppie-about-profile" class="image custom-file-input image upload-myfile-image" accept="image/x-png,image/gif,image/jpeg" data-croppie-size='{"width": 400, "height": 400}'
                                data-croppie data-croppie-file="#data-croppie-about-profile" data-croppie-input="#data-croppie-about-profile-input" data-croppie-output="#data-croppie-about-profile-preview"
                                data-croppie-progress="#artist-about-profile-progress" />
                                <input type="hidden" id="data-croppie-about-profile-input" name="myfile">

                                @php $source = asset("public/img/sv-5.jpg") @endphp
                                @if( count( $getIfAlready ) )
                                    @if( !empty($getIfAlready[0]->profile_photo) )
                                        @php $source = $getIfAlready[0]->profile_photo @endphp
                                    @endif
                                @endif
                                <img data-about-profile src="{{ $source }}" style="width: 200px; height: 200px; margin-top: 15px" id="data-croppie-about-profile-preview" />
                                <div class="u-progress" id="artist-about-profile-progress" style="display: none; width: 200px">
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
											<span class="sr-only" data-progress="text">0% Complete</span>
										</div>
									</div>
								</div>
                            </div>
 @if( $hasThemePlan )
                            <div class="col-lg-6 cover-photo-mobile-show   cover-photo-hidden" style="margin:20px 0 30px 0;padding: 0;">
                                <div class="" id="cover_only_mobile" style="padding:0px">
                                    <label for="exampleInputEmail1"> Upload Cover Banner 1350px x 570px </label>
                                    <input type="file" id="data-croppie-about-profile_bkg" accept="image/x-png,image/gif,image/jpeg"
                                    data-croppie data-croppie-store="public/profile_bkg/" data-croppie-file="#data-croppie-about-profile_bkg" data-croppie-input="#data-croppie-about-profile_bkg-input" data-croppie-output="#data-croppie-about-profile_bkg-preview"
                                    data-croppie-viewport='{"width": 1350, "height": 570}'
                                    data-croppie-boundary='{"width": 1350, "height": 570}'
                                    data-croppie-size='{"width": 1350, "height": 570}'
                                    data-croppie-progress="#artist-about-cover-progress" />
                                    <input type="hidden" name="profile_bkg" id="data-croppie-about-profile_bkg-input" />
                                      <!--File size: 5mb max-->
                                </div>
                                @php $source = asset('public/img/sv-5.jpg'); @endphp
                                @if ( !empty($getIfAlready[0]->profile_bkg) )
                                    @php $source = $getIfAlready[0]->profile_bkg @endphp
                                @endif
                                <br>
                                <img src="{{ $source }}" style="width: 225px; height: 95px;"id="data-croppie-about-profile_bkg-preview" />
                                <div class="u-progress" id="artist-about-cover-progress" style="display: none; width: 225px">
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
											<span class="sr-only" data-progress="text">0% Complete</span>
										</div>
									</div>
								</div>
                            </div>
							@endif
                            <div style="clear:both"></div>
                            <div class="row" style="margin: 20px 0px 0 0;">
                                <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlready) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                            </div>
                    </div>
                    <div class="clear"></div>
                    </form>
                </div>
            </div>
        </div>
            <div id="what-we-de-bl" class="tabcontent" style="margin-top:10px">
            <div class="container">
                <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                    <?php 
                        $userId = session()->get('user_id');
                        $getIfAlreadyWhatWedo = DB::select('select * from artist_what_we_do where user_id='.$userId);
                        ?>
                    <form action="{{URL::to('save-what-we-do-profile')}}" id="whatwedoform" style="margin-top:0px" enctype="multipart/form-data" method="post" novalidate>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group achievemets">
                            <h2>What we do</h2>
                            <div class="form-group col-lg-12">
                                <label for="exampleInputEmail1">What we do *</label>
                                <input type="text" name="what_we_do" class="form-control" required id="exampleInputEmail1" placeholder="What we do" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->what_we_do != NULL) ? $getIfAlreadyWhatWedo[0]->what_we_do : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="exampleInputEmail1"> Description (50 words) *</label>
                                <textarea id="w3review" name="description" onKeyPress="return check(event,value)" onInput="checkLength(50,this)" rows="4" cols="50" required><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->description != NULL) ? limit_text($getIfAlreadyWhatWedo[0]->description,0,50) : '') : '')); ?></textarea> 
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div id="copydiv">
                                @for($i = 0; $i < ($hasThemePlan ? $whatWeDoPremiumLength : $whatWeDoLength); $i++)
                                    @php
                                        $objTitle = "title_".($i+1);
                                        $objDesc = "des_".($i+1);
                                    @endphp
                                    <div class="col-lg-3">
                                        <div class="form-group"> <label for="exampleInputEmail{{ $i + 1 }}">Title *</label>
                                            <input type="text" required class="form-control" name="title[]" id="exampleInputEmail{{ $i + 1 }}" placeholder="eg: CINEMAS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->{$objTitle} != NULL) ? $getIfAlreadyWhatWedo[0]->{$objTitle} : '') : '')); ?>">
                                        </div>
                                        <div class="form-group">  
                                            <label for="exampleInputEmail{{ $i + 1 }}"> Description (25 words) *</label>
                                            <textarea name="desc[]" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" required rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->{$objDesc} != NULL) ? limit_text($getIfAlreadyWhatWedo[0]->{$objDesc}, 0, 25) : '') : '')); ?></textarea>
                                        </div>
                                    </div>
                                @endfor
                            </div>
                            <br><br>
                            Note: Minimum 4 you will add 4 services,  to add more purchase premium profile <a href="" style="text-decoration:underline">Purchase Premium Theme</a>
                        </div>
                        <div style="clear:both; overflow:hidden"></div>
                        <div class="row">
                            <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyWhatWedo) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
            <div id="achievements-blocks0" class="tabcontent" style="margin-top:10px">
                <div class="container">
                    <div class="row"  style="background:#fff;padding: 30px 15px;margin: 0;">
                        <div class="form-group achievemets">
                            @php $getIfAlreadyAchivement = DB::select('select * from artist_achievment where user_id='.$userId); @endphp
                            <form action="{{URL::to('save-achivement')}}" id="achivementForm" style="margin-top:0px" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <h2>Achievements</h2>
                                <p>You can add <strong>5</strong> Achievements | Image size <strong>450 x 300</strong></p>
                              
                                
                                @for($i = 0; $i < ($hasThemePlan ? $achievementPremiumLength : $achievementLength); $i++)
                                    @php
                                        $j = $i + 1;
                                    @endphp
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="form-group col-lg-6">
                                                <label for="exampleInputEmail1">Title</label>
                                                <input type="text" name="titleachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 && isset($getIfAlreadyAchivement[$i]) ? (($getIfAlreadyAchivement[$i]->title_1 != NULL) ? $getIfAlreadyAchivement[$i]->title_1 : '') : '')); ?>">
                                            </div>
                                            <input type="hidden" name="id[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 && isset($getIfAlreadyAchivement[$i]) ? (($getIfAlreadyAchivement[$i]->id != NULL) ? $getIfAlreadyAchivement[$i]->id : '') : '')); ?>">  
                                            <input type="hidden" name="oldFile[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 && isset($getIfAlreadyAchivement[$i]) ? (($getIfAlreadyAchivement[$i]->photo_1 != NULL) ? $getIfAlreadyAchivement[$i]->photo_1 : '') : '')); ?>">  
                                            <div class="form-group col-lg-3">
                                                <label for="exampleInputEmail1">Link - If any</label>
                                                <input type="text" name="linkachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 && isset($getIfAlreadyAchivement[$i]) ? (($getIfAlreadyAchivement[$i]->link_1 != NULL) ? $getIfAlreadyAchivement[$i]->link_1 : '') : '')); ?>">  
                                            </div>
                                            <div class="form-group col-lg-3">
                                                <label for="exampleInputEmail1">Date</label>
                                                <input type="date" name="date[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 && isset($getIfAlreadyAchivement[$i]) ? (($getIfAlreadyAchivement[$i]->date_1 != NULL) ? date('Y-m-d',strtotime($getIfAlreadyAchivement[$i]->date_1)) : '') : '')); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <label for="exampleInputEmail1"> Description (100 words)</label>
                                        <textarea id="w3review" onKeyPress="return check(event,value)" onInput="checkLength(100,this)" name="descachieve[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyAchivement) >= 1 && isset($getIfAlreadyAchivement[$i]) ? (( $getIfAlreadyAchivement[$i]->des_1 != NULL) ? limit_text($getIfAlreadyAchivement[$i]->des_1,0,100) : '') : '')); ?></textarea> 
                                    </div>
                                    <div class="clear"></div>
                                    <div class="form-group col-lg-12">  
                                        <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <a href="javascript:void(0)"
                                                data-croppie data-croppie-file="#fileachivement{{ $j }}"
                                                data-croppie-input="#uploaded_achieve_image{{ $j }}" data-croppie-output="#previewachievesec{{ $j }}" data-croppie-bind="file"
                                                data-croppie-viewport='{"width": 450, "height": 300}'
                                                data-croppie-boundary='{"width": 450, "height": 300}'
                                                data-croppie-progress='#artist-achievement-progress-{{ $j }}' style="background: #637076;color: #fff;width:auto;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">Browse Achievement Photo</a>
                                            <input type="file" class="d-none" target="fileachive{{ $j }}" id="fileachivement{{ $j }}" class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                            <input type="hidden" id="uploaded_achieve_image{{ $j }}" name="fileachive{{ $j }}">
                                        </div>
                                        <div class="u-close" style="float:left; margin-right:20px;">
                                            <img id="previewachievesec{{ $j }}" src="<?php echo ((count($getIfAlreadyAchivement) >= 1 && isset($getIfAlreadyAchivement[$i]) ? (($getIfAlreadyAchivement[0]->photo_1 != NULL || $getIfAlreadyAchivement[0]->photo_1 != '') ? $getIfAlreadyAchivement[$i]->photo_1 : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:70px;object-fit:cover;margin-right:2px;">
                                            <div class="u-progress{{ $j }}" id="artist-achievement-progress-{{ $j }}" style="display: none; width: 100px">
            									<div class="progress">
            										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
            											<span class="sr-only" data-progress="text">0% Complete</span>
            										</div>
            									</div>
            								</div>
                                            <?php 
                                                if(count($getIfAlreadyAchivement) >= 1 && isset($getIfAlreadyAchivement[$i]))
                                                {
                                                    if($getIfAlreadyAchivement[$i]->photo_1 != NULL || $getIfAlreadyAchivement[$i]->photo_1 != '')
                                                    {
                                                ?>
                                            <a id="crossachive{{ $j }}" href="<?php echo URL::to('/delete-achivement-image/'.$getIfAlreadyAchivement[$i]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                            <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                        </div>
                                    </div>
                                    <Br>
                                @endfor
                                </div>
                                <!-- End Of Hidden Achivements -->
                                </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="row">
                                <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyAchivement) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        
        <style>
            .d-none{
            display:none!important;
            }
            textarea {font-size: 14px;}
            #latest-project h2{font-size:22px;margin:0px;}
        </style>
        <div id="showreel-block" class="tabcontent"  style="padding-top: 15px; background: rgb(244, 243, 243); margin-top: 0px;">
            <div class="container">
                <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                    <h2>Showreel</h2>
                    <?php 
                        $userId = session()->get('user_id');
                        $getIfAlreadyReel = DB::select('select * from artist_showreel where user_id='.$userId);
                        ?>
                    <form action="{{URL::to('/save-reel')}}" method="post" enctype="multipart/form-data" id="reelform">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group col-lg-12" style="margin-bottom:10px">
                            <label for="exampleInputEmail1">Description 50 words</label>
                            <textarea id="w3review" name="description" onKeyPress="return check(event,value)" onInput="checkLength(50,this)" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyReel) > 0 ? (($getIfAlreadyReel[0]->description != NULL) ? limit_text($getIfAlreadyReel[0]->description,0,50) : '') : '')); ?></textarea>
                        </div>	<div class="clear"></div>
                        <div class="row" style="margin-top:0px;margin-left:20px;">
                           <div class="video-p"> <input type="file" class="d-none" id="reelvideo" accept="video/*">
                            <a href="javascript:void(0)" onclick="openReelVideo()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;width: 105px;float: left;text-align: center;margin-right: 4%;">Upload Video</a>
                           
                          
                            <div class="u-details" style="display:none;     float: left;
    width: 84%;">
                                <div class="row">
                                    <div class="col-lg-12 ud-caption">Upload Details</div>
                                    <div class="col-lg-10">
                                        <div class="u-title"></div>
                                        <div id="upload-progress">
                                            <div class="u-size"></div>
                                            <div class="u-progress">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                        <span class="sr-only">0% Complete</span>
                                                    </div>
                                                </div>
                                                <div class="u-close">
                                                    <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="u-desc"><span class="percent-process1"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                    </div>
                                </div>
                            </div>      </div>
                              <p id="reelvideolabel" class="text-success" style="color:green;font-weight:bold;margin-left:15px;margin-top:15px;"></p>
                             
                            <div>
                            <input type="hidden" name="video" id="videouploaded">
                            <input type="file" class="d-none" data-target="video_thumb" target="video_thumb" id="upload_image1"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                            <input type="hidden" id="generated_video_thumb" name="generated_video_thumb">  
                            <input type="hidden" id="video_thumb" name="video_thumb">  
                                  <p id="reelvideothumblabel" class="text-success" style="color:green;font-weight:bold;margin-left:20px;margin-top:15px;"></p>
                            <div style="clear:both; overflow:hidden"></div>
                            
                            
                            <br>
                            <a href="javascript:void(0)" data-croppie data-croppie-file="input[data-target='video_thumb']"
                            data-croppie-input="#video_thumb" data-croppie-output="#reelThumbGenerated" data-croppie-bind="file"
                            data-croppie-viewport='{"width": 665, "height": 340}'
                            data-croppie-boundary='{"width": 665, "height": 340}'
                            data-croppie-progress='#artist-showreel-thumb-progress'
                            style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Browse Thumbnail</a>
                            <br>
                      
                            </div>
                            <!--Image size ( width:665px height:372px;) -->
                            <div class="u-close" style="margin-right:20px;margin-top:20px;">
                                <img id="reelThumbGenerated" src="<?php echo ((count($getIfAlreadyReel) > 0 ? (($getIfAlreadyReel[0]->video_thumb != NULL || $getIfAlreadyReel[0]->video_thumb != '') ? $getIfAlreadyReel[0]->video_thumb : url('public/img/sv-5.jpg')) : ('public/img/sv-5.jpg'))); ?>" style="background:rgb(244, 243, 243);width:200px;height:200px;object-fit:cover;margin-right:2px;float:left;"><?php 
                                    if(count($getIfAlreadyReel) > 0)
                                    {
                                        if($getIfAlreadyReel[0]->video_thumb != NULL || $getIfAlreadyReel[0]->video_thumb != '')
                                        {
                                    ?>
                                <a href="<?php echo URL::to('/delete-reel-image/'.$getIfAlreadyReel[0]->id) ?>" style="margin-top: 2px;display: block;float: left;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div id="profile_photo_preview">
                                <img src="<?= url('public/img/sv-5.jpg'); ?>" style="width:200px;height:200px;object-fit:cover;margin-left:15px;" alt="previewImage">
                                <div class="u-progress" id="artist-showreel-thumb-progress" style="display: none; width: 200px">
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
											<span class="sr-only" data-progress="text">0% Complete</span>
										</div>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="row" style="margin-top:20px;">
                            <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyReel) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="portfolio-block" class="tabcontent" style="padding-top: 15px; background: rgb(244, 243, 243); margin-top: 0px;">
            <div class="container">
                <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                    <h2>Gallery</h2>
                    <p>You can add <Strong>12</Strong> Photos  |  Image size <Strong>600px x 600px</Strong></p>
                   
                    <div class="col-lg-12">
                        <div class="u-form">
                            <?php 
                                $userId = session()->get('user_id');
                                $getIfAlreadyGallery = DB::select('select * from artist_gallery where user_id='.$userId);
                                ?>
                            <form action="{{URL::to('/save-gallery')}}" method="post" enctype="multipart/form-data" id="galleryform">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 1 ? (($getIfAlreadyGallery[0]->id != NULL) ? $getIfAlreadyGallery[0]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 1 ? (($getIfAlreadyGallery[0]->photo != NULL) ? $getIfAlreadyGallery[0]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 2 ? (($getIfAlreadyGallery[1]->id != NULL) ? $getIfAlreadyGallery[1]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 2 ? (($getIfAlreadyGallery[1]->photo != NULL) ? $getIfAlreadyGallery[1]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[2]->id != NULL) ? $getIfAlreadyGallery[2]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[2]->photo != NULL) ? $getIfAlreadyGallery[2]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 4 ? (($getIfAlreadyGallery[3]->id != NULL) ? $getIfAlreadyGallery[3]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 4 ? (($getIfAlreadyGallery[3]->photo != NULL) ? $getIfAlreadyGallery[3]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 5 ? (($getIfAlreadyGallery[4]->id != NULL) ? $getIfAlreadyGallery[4]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 5 ? (($getIfAlreadyGallery[4]->photo != NULL) ? $getIfAlreadyGallery[4]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 6 ? (($getIfAlreadyGallery[5]->id != NULL) ? $getIfAlreadyGallery[5]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 6 ? (($getIfAlreadyGallery[5]->photo != NULL) ? $getIfAlreadyGallery[5]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 7 ? (($getIfAlreadyGallery[6]->id != NULL) ? $getIfAlreadyGallery[6]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 7 ? (($getIfAlreadyGallery[6]->photo != NULL) ? $getIfAlreadyGallery[6]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 8 ? (($getIfAlreadyGallery[7]->id != NULL) ? $getIfAlreadyGallery[7]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 8 ? (($getIfAlreadyGallery[7]->photo != NULL) ? $getIfAlreadyGallery[7]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 9 ? (($getIfAlreadyGallery[8]->id != NULL) ? $getIfAlreadyGallery[8]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 9 ? (($getIfAlreadyGallery[8]->photo != NULL) ? $getIfAlreadyGallery[8]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 10 ? (($getIfAlreadyGallery[9]->id != NULL) ? $getIfAlreadyGallery[9]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 10 ? (($getIfAlreadyGallery[9]->photo != NULL) ? $getIfAlreadyGallery[9]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 11 ? (($getIfAlreadyGallery[10]->id != NULL) ? $getIfAlreadyGallery[10]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 11 ? (($getIfAlreadyGallery[10]->photo != NULL) ? $getIfAlreadyGallery[10]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 12 ? (($getIfAlreadyGallery[11]->id != NULL) ? $getIfAlreadyGallery[11]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 12 ? (($getIfAlreadyGallery[11]->photo != NULL) ? $getIfAlreadyGallery[11]->photo : '') : '')); ?>" />
                                <div class="row" id="portfolio1">
                                    @for( $i = 0; $i < ($hasThemePlan ? $galleryPremiumLength : $galleryLength); $i++ )
                                        @php
                                            $j = $i + 1;
                                        @endphp
                                        <div class="col-lg-6 top_block">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title {{ $j }} - (3 words)</label>
                                                <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i]) ? (($getIfAlreadyGallery[$i]->photo_title != NULL) ? limit_text($getIfAlreadyGallery[$i]->photo_title,0,3) : '') : '')); ?>">  
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Select Category</strong>
                                                    </option>
                                                    <option value="Theater" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Theater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Theater</strong>
                                                    </option>
                                                    <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voiceover</strong>
                                                    </option>
                                                    <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedian</strong>
                                                    </option>
                                                    <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Photography</strong>
                                                    </option>
                                                    <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Grapher</strong>
                                                    </option>
                                                    <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artrist</strong>
                                                    </option>
                                                    <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Actor</strong>
                                                    </option>
                                                    <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Child Actor</strong>
                                                    </option>
                                                    <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedy Actor</strong>
                                                    </option>
                                                    <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Junior Actor</strong>
                                                    </option>
                                                    <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Stunt Artist</strong>
                                                    </option>
                                                    <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Drama Artist</strong>
                                                    </option>
                                                    <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Mimicry Artist</strong>
                                                    </option>
                                                    <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Background / Atmosphere</strong>
                                                    </option>
                                                    <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Model</strong>
                                                    </option>
                                                    <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Child Model</strong>
                                                    </option>
                                                    <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Pro Model</strong>
                                                    </option>
                                                    <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dancer</strong>
                                                    </option>
                                                    <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lead Dancer</strong>
                                                    </option>
                                                    <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Background Dancer</strong>
                                                    </option>
                                                    <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Editor</strong>
                                                    </option>
                                                    <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Editor</strong>
                                                    </option>
                                                    <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Editor</strong>
                                                    </option>
                                                    <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dialogue Writer</strong>
                                                    </option>
                                                    <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Script Writer</strong>
                                                    </option>
                                                    <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Screeplay Writer</strong>
                                                    </option>
                                                    <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Story Writer</strong>
                                                    </option>
                                                    <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lyricists</strong>
                                                    </option>
                                                    //video vj
                                                    <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Video Jockey (VJ)</strong>
                                                    </option>
                                                    <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Radio Jockey (RJ)</strong>
                                                    </option>
                                                    <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> News Reader</strong>
                                                    </option>
                                                    <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Reporter</strong>
                                                    </option>
                                                    <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Journalist</strong>
                                                    </option>
                                                    <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dubbing Artist</strong>
                                                    </option>
                                                    <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voice Over Artist</strong>
                                                    </option>
                                                    <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>New Influncer</strong>
                                                    </option>
                                                    <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Experienced Influncer</strong>
                                                    </option>
                                                    <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Advertising</strong>
                                                    </option>
                                                    <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Advertising</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Banner Advertising</strong>
                                                    </option>
                                                    <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Singer</strong>
                                                    </option>
                                                    <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Instrumentalist</strong>
                                                    </option>
                                                    <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Composer</strong>
                                                    </option>
                                                    <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Director Of Photography / Cinematographer</strong>
                                                    </option>
                                                    <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Cameraman</strong>
                                                    </option>
                                                    <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Camera Operator</strong>
                                                    </option>
                                                    <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Light man / Gaffer</strong>
                                                    </option>
                                                    <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artist</strong>
                                                    </option>
                                                    <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Visual Effects Editor</strong>
                                                    </option>
                                                    <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Digital imaging technician</strong>
                                                    </option>
                                                    <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Special Effects makeup Artist</strong>
                                                    </option>
                                                    <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Motion control technician</strong>
                                                    </option>
                                                    <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Fashion Designer</strong>
                                                    </option>
                                                    <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Hair Stylist</strong>
                                                    </option>
                                                    <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Costume designer</strong>
                                                    </option>
                                                    <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Grip</strong>
                                                    </option>
                                                    <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Designer</strong>
                                                    </option>
                                                    <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Grip</strong>
                                                    </option>
                                                    <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Sound Mixer</strong>
                                                    </option>
                                                    <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Designer</strong>
                                                    </option>
                                                    <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Green man</strong>
                                                    </option>
                                                    <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Property master</strong>
                                                    </option>
                                                    <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Weapons master</strong>
                                                    </option>
                                                    <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Set Designer / Decorator</strong>
                                                    </option>
                                                    <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Location Manager</strong>
                                                    </option>
                                                    <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>StoryBoard Artist</strong>
                                                    </option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i])) ? (($getIfAlreadyGallery[$i]->category != NULL) ?
                                                        (($getIfAlreadyGallery[$i]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Others</strong>
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo"
                                                        data-croppie data-croppie-file="#photosectionphoto{{ $j }}"
                                                        data-croppie-input="#uploaded_gal_image{{ $j }}"
                                                        data-croppie-output="#gal{{ $j }}"
                                                        data-croppie-bind="file"
                                                        data-croppie-viewport='{"width": 600, "height": 600}'
                                                        data-croppie-boundary='{"width": 600, "height": 600}'
                                                        data-croppie-progress='#artist-gallery-progress-{{ $j }}'><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;font-size: 10px;">Browse photo</a>
                                                    </div>
                                                    
                                                    <input type="file" class="d-none" target="photosectionphoto{{ $j }}" id="photosectionphoto{{ $j }}"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                    <input type="hidden" id="uploaded_gal_image{{ $j }}" name="photosectionphoto{{ $j }}">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img id="gal{{ $j }}" src="<?php echo ((count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i]) ? (($getIfAlreadyGallery[$i]->photo != NULL || $getIfAlreadyGallery[$i]->photo != '') ? $getIfAlreadyGallery[$i]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <div class="u-progress" id="artist-gallery-progress-{{ $j }}" style="display: none; width: 100px;">
                    									<div class="progress">
                    										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                    											<span class="sr-only" data-progress="text">0% Complete</span>
                    										</div>
                    									</div>
                    								</div>
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 1 && isset($getIfAlreadyGallery[$i]))
                                                        {
                                                            if($getIfAlreadyGallery[$i]->photo != NULL || $getIfAlreadyGallery[$i]->photo != '')
                                                            {
                                                        ?>
                                                    <a id="crossgal{{ $j }}" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[$i]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                                <div class="btn-save">
                                    <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyGallery) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="video" class="tabcontent" style="margin-top: -6px;">
            <div id="portfolio-block"  style="padding-top: 15px; background: rgb(244, 243, 243); margin-top: 0px;">
                <div class="container">
                    <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <h2>Video</h2>
                        <p>You can add 6 Videos (Set of 3) 
                        <!--|  Image <strong>width:665px height:372px-->
                        
                        </Strong> </p>
                       
                        <div class="col-lg-12">
                            <form action="{{URL::to('/save-video')}}" method="post" enctype="multipart/form-data" id="videoform">
                                <?php 
                                    $userId = session()->get('user_id');
                                    $getIfAlreadyVideo = DB::select('select * from artist_video where user_id='.$userId);
                                    ?>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->id != NULL) ? $getIfAlreadyVideo[0]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->video != NULL) ? $getIfAlreadyVideo[0]->video : '') : '')); ?>" />
                                <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->thumb != NULL) ? $getIfAlreadyVideo[0]->thumb : '') : '')); ?>" />
                                <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->id != NULL) ? $getIfAlreadyVideo[1]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->video != NULL) ? $getIfAlreadyVideo[1]->video : '') : '')); ?>" />
                                <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->thumb != NULL) ? $getIfAlreadyVideo[1]->thumb : '') : '')); ?>" />
                                <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->id != NULL) ? $getIfAlreadyVideo[2]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->video != NULL) ? $getIfAlreadyVideo[2]->video : '') : '')); ?>" />
                                <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->thumb != NULL) ? $getIfAlreadyVideo[2]->thumb : '') : '')); ?>" />
                                <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->id != NULL) ? $getIfAlreadyVideo[3]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->video != NULL) ? $getIfAlreadyVideo[3]->video : '') : '')); ?>" />
                                <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->thumb != NULL) ? $getIfAlreadyVideo[3]->thumb : '') : '')); ?>" />
                                <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->id != NULL) ? $getIfAlreadyVideo[4]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->video != NULL) ? $getIfAlreadyVideo[4]->video : '') : '')); ?>" />
                                <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->thumb != NULL) ? $getIfAlreadyVideo[4]->thumb : '') : '')); ?>" />
                                <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->id != NULL) ? $getIfAlreadyVideo[5]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->video != NULL) ? $getIfAlreadyVideo[5]->video : '') : '')); ?>" />
                                <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->thumb != NULL) ? $getIfAlreadyVideo[5]->thumb : '') : '')); ?>" />
                                <div class="u-form">
                                    <div class="row">
                                        @for( $i = 0; $i < ($hasThemePlan ? $videoPremiumLength : $videoLength); $i++ )
                                            @php
                                                $j = $i + 1;
                                            @endphp
                                            <div class="col-lg-6 new_video_block31">
                                                <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                                    <label for="e1">Video Title - Position {{ $j }}</label>
                                                    <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i]) ? (($getIfAlreadyVideo[$i]->video_title != NULL) ? $getIfAlreadyVideo[$i]->video_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6" style="padding:0px;">
                                                    <label for="e1">Select category</label>
                                                    <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                                        <option value="" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Select Category</strong>
                                                        </option>
                                                        <option value="Theater" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Theater") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Theater</strong>
                                                        </option>
                                                        <option value="Voiceover" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Voiceover</strong>
                                                        </option>
                                                        <option value="Comedian" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Comedian</strong>
                                                        </option>
                                                        <option value=" Photography" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Photography</strong>
                                                        </option>
                                                        <option value="Video Grapher" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Video Grapher</strong>
                                                        </option>
                                                        <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Makeup Artrist</strong>
                                                        </option>
                                                        <option value="Actor" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Actor</strong>
                                                        </option>
                                                        <option value="Child Actor" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Child Actor</strong>
                                                        </option>
                                                        <option value="Comedy Actor" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Comedy Actor</strong>
                                                        </option>
                                                        <option value=" Junior Actor" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Junior Actor</strong>
                                                        </option>
                                                        <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Stunt Artist</strong>
                                                        </option>
                                                        <option value=" Drama Artist" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Drama Artist</strong>
                                                        </option>
                                                        <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Mimicry Artist</strong>
                                                        </option>
                                                        <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Background / Atmosphere</strong>
                                                        </option>
                                                        <option value="Model" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Model</strong>
                                                        </option>
                                                        <option value=" Child Model" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Child Model</strong>
                                                        </option>
                                                        <option value="Pro Model" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Pro Model</strong>
                                                        </option>
                                                        <option value="Dancer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Dancer</strong>
                                                        </option>
                                                        <option value="Lead Dancer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Lead Dancer</strong>
                                                        </option>
                                                        <option value="Background Dancer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Background Dancer</strong>
                                                        </option>
                                                        <option value="Video Editor" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Video Editor</strong>
                                                        </option>
                                                        <option value="Music Editor" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Music Editor</strong>
                                                        </option>
                                                        <option value="Audio Editor" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Audio Editor</strong>
                                                        </option>
                                                        <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Dialogue Writer</strong>
                                                        </option>
                                                        <option value="Script Writer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Script Writer</strong>
                                                        </option>
                                                        <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Screeplay Writer</strong>
                                                        </option>
                                                        <option value="Story Writer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Story Writer</strong>
                                                        </option>
                                                        <option value="Lyricists" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Lyricists</strong>
                                                        </option>
                                                        //video vj
                                                        <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Video Jockey (VJ)</strong>
                                                        </option>
                                                        <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Radio Jockey (RJ)</strong>
                                                        </option>
                                                        <option value=" News Reader" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> News Reader</strong>
                                                        </option>
                                                        <option value="Reporter" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Reporter</strong>
                                                        </option>
                                                        <option value="Journalist" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Journalist</strong>
                                                        </option>
                                                        <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Dubbing Artist</strong>
                                                        </option>
                                                        <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Voice Over Artist</strong>
                                                        </option>
                                                        <option value="New Influncer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>New Influncer</strong>
                                                        </option>
                                                        <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Experienced Influncer</strong>
                                                        </option>
                                                        <option value="Video Advertising" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Video Advertising</strong>
                                                        </option>
                                                        <option value="Audio Advertising" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Audio Advertising</strong>
                                                        </option>
                                                        <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Threater</strong>
                                                        </option>
                                                        <option value="Banner Advertising" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Banner Advertising</strong>
                                                        </option>
                                                        <option value="Singer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Singer</strong>
                                                        </option>
                                                        <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Music Instrumentalist</strong>
                                                        </option>
                                                        <option value="Music Composer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Music Composer</strong>
                                                        </option>
                                                        <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Director Of Photography / Cinematographer</strong>
                                                        </option>
                                                        <option value="Cameraman" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Cameraman</strong>
                                                        </option>
                                                        <option value="Camera Operator" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Camera Operator</strong>
                                                        </option>
                                                        <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Light man / Gaffer</strong>
                                                        </option>
                                                        <option value="Makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Makeup Artist</strong>
                                                        </option>
                                                        <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Visual Effects Editor</strong>
                                                        </option>
                                                        <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Digital imaging technician</strong>
                                                        </option>
                                                        <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Special Effects makeup Artist</strong>
                                                        </option>
                                                        <option value="Motion control technician" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Motion control technician</strong>
                                                        </option>
                                                        <option value="Fashion Designer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Fashion Designer</strong>
                                                        </option>
                                                        <option value="Hair Stylist" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Hair Stylist</strong>
                                                        </option>
                                                        <option value="Costume designer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Costume designer</strong>
                                                        </option>
                                                        <option value="Grip" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Grip</strong>
                                                        </option>
                                                        <option value="Sound Designer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Sound Designer</strong>
                                                        </option>
                                                        <option value="Sound Grip" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Sound Grip</strong>
                                                        </option>
                                                        <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Production Sound Mixer</strong>
                                                        </option>
                                                        <option value="Production Designer" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Production Designer</strong>
                                                        </option>
                                                        <option value="Green man" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Green man</strong>
                                                        </option>
                                                        <option value="Property master" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Property master</strong>
                                                        </option>
                                                        <option value="Weapons master" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Weapons master</strong>
                                                        </option>
                                                        <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Set Designer / Decorator</strong>
                                                        </option>
                                                        <option value="Location Manager" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Location Manager</strong>
                                                        </option>
                                                        <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>StoryBoard Artist</strong>
                                                        </option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i])) ? (($getIfAlreadyVideo[$i]->category != NULL) ?
                                                            (($getIfAlreadyVideo[$i]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Others</strong>
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="e1">Video Description </label>
                                                    <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i]) ? (($getIfAlreadyVideo[$i]->description != NULL) ? $getIfAlreadyVideo[$i]->description : '') : '')); ?></textarea>
                                                </div>
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="uploadVideo('{{ $j }}')"><i class="fa fa-upload" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                    <input type="file" class="d-none" id="videoFile{{ $j }}">
                                                    <input type="hidden" name="uploadedvideosec{{ $j }}" id="uploadedvideosec{{ $j }}">
                                                    <input type="hidden" id="generatedvideosecthumb{{ $j }}" name="generatedvideosecthumb{{ $j }}">
                                                </div>
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                     <!--onclick="uploadVideoThumbnail('1')"-->
                                                    <div class="browse-photo"
                                                        data-croppie data-croppie-file="#videoThumb{{ $j }}"
                                                        data-croppie-input="#uploaded_vid_image{{ $j }}"
                                                        data-croppie-output="#previewvideosec{{ $j }}"
                                                        data-croppie-bind="file"
                                                        data-croppie-viewport='{"width": 365, "height": 205}'
                                                        data-croppie-boundary='{"width": 365, "height": 205}'
                                                        data-croppie-progress='#artist-video-progress-thumb-{{ $j }}'><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                    <input type="file" class="d-none" target="videoThumbFile{{ $j }}" id="videoThumb{{ $j }}"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                    <input type="hidden" id="uploaded_vid_image{{ $j }}" name="videoThumbFile{{ $j }}">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img id="previewvideosec{{ $j }}" src="<?php echo ((count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i]) ? (($getIfAlreadyVideo[$i]->thumb != NULL || $getIfAlreadyVideo[$i]->thumb != '') ? $getIfAlreadyVideo[$i]->thumb : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:170px;height:117px;object-fit:cover;margin-right:2px;">
                                                    <div class="u-progress{{ $j }}" id="artist-video-progress-thumb-{{ $j }}" style="display: none; width: 170px">
                    									<div class="progress">
                    										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                    											<span class="sr-only" data-progress="text">0% Complete</span>
                    										</div>
                    									</div>
                    								</div>
                                                    <?php 
                                                        if(count($getIfAlreadyVideo) >= 1 && isset($getIfAlreadyVideo[$i]))
                                                        {
                                                            if($getIfAlreadyVideo[$i]->thumb != NULL || $getIfAlreadyVideo[$i]->thumb != '')
                                                            {
                                                        ?>
                                                    <a id="crossvid{{ $j }}" href="<?php echo URL::to('/delete-video-image/'.$getIfAlreadyVideo[$i]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                                <div class="u-progress<?php echo $j;?>" style="display:none;">
                                                    <div class="row">
                                                        <div class="col-lg-12 ud-caption">Upload Details</div>
                                                        <div class="col-lg-10">
                                                            <div class="u-title"></div>
                                                            <div id="upload-progress{{ $j }}">
                                                                <div class="u-size{{ $j }}"></div>
                                                                <div class="u-progress{{ $j }}">
                                                                    <div class="progress{{ $j }}">
                                                                        <div class="progress-bar{{ $j }}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                            <span class="sr-only{{ $j }}">0% Complete</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="u-close">
                                                                        <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="u-desc{{ $j }}"><span class="percent-process{{ $j }}"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endfor
                                    </div>
                                </div>
                                <div class="row col-md-12 btn-save">
                                    <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyVideo) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="latest-project" class="tabcontent" style="padding-top: 15px; background: rgb(244, 243, 243); margin-top: 0px;">
            <div class="container">
                <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                    <h2>Latest Projects - add upto 3</h2>
                    <form action="{{URL::to('/save-project')}}" method="post" enctype="multipart/form-data" id="projectform">
                        <?php 
                            $getIfAlreadyProject = DB::select('select * from artist_project where user_id='.$userId);
                            $countInWords = ['First', 'Second', 'Third'];
                        ?>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        @for( $i = 0; $i < ($hasThemePlan ? $latestProjectPremiumLength : $latestProjectLength); $i++ )
                            @php
                                $j = $i + 1;
                            @endphp
                            <input type="hidden" name="oldProjid[]" value="<?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i]) ? (($getIfAlreadyProject[$i]->id != NULL) ? $getIfAlreadyProject[$i]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldProjFile[]" value="<?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i]) ? (($getIfAlreadyProject[$i]->photo != NULL) ? $getIfAlreadyProject[$i]->photo : '') : '')); ?>" />
                            <div class="col-lg-12" style="background:rgb(244, 243, 243);margin-top:10px;    border-radius: 5px;">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>{{ $countInWords[$i] }}</h6>
                                    </div>
                                     <div class="form-group col-lg-4">
                                        <label for="exampleInputEmail1">Title</label>  
                                        <input type="text" name="projecttitle[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i]) ? (($getIfAlreadyProject[$i]->title != NULL) ? $getIfAlreadyProject[$i]->title : '') : '')); ?>">  
                                    </div>
                                    
                                    <div class="form-group col-lg-2">
                                        <label for="exampleInputEmail1">Select category</label>  
                                        <select class="input-select" style="width:100%" name="categoryproject[]" >
                                            <option value="Threater" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Threater</strong>
                                            </option>
                                            <option value="Voiceover" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Voiceover</strong>
                                            </option>
                                            <option value="Comedian" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Comedian</strong>
                                            </option>
                                            <option value=" Photography" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Photography</strong>
                                            </option>
                                            <option value="Video Grapher" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Video Grapher</strong>
                                            </option>
                                            <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Makeup Artrist</strong>
                                            </option>
                                            <option value="Actor" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Actor</strong>
                                            </option>
                                            <option value="Child Actor" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Child Actor</strong>
                                            </option>
                                            <option value="Comedy Actor" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Comedy Actor</strong>
                                            </option>
                                            <option value=" Junior Actor" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Junior Actor</strong>
                                            </option>
                                            <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Stunt Artist</strong>
                                            </option>
                                            <option value=" Drama Artist" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Drama Artist</strong>
                                            </option>
                                            <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Mimicry Artist</strong>
                                            </option>
                                            <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Background / Atmosphere</strong>
                                            </option>
                                            <option value="Model" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Model</strong>
                                            </option>
                                            <option value=" Child Model" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Child Model</strong>
                                            </option>
                                            <option value="Pro Model" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Pro Model</strong>
                                            </option>
                                            <option value="Dancer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Dancer</strong>
                                            </option>
                                            <option value="Lead Dancer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Lead Dancer</strong>
                                            </option>
                                            <option value="Background Dancer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Background Dancer</strong>
                                            </option>
                                            <option value="Video Editor" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Video Editor</strong>
                                            </option>
                                            <option value="Music Editor" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Music Editor</strong>
                                            </option>
                                            <option value="Audio Editor" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Audio Editor</strong>
                                            </option>
                                            <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Dialogue Writer</strong>
                                            </option>
                                            <option value="Script Writer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Script Writer</strong>
                                            </option>
                                            <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Screeplay Writer</strong>
                                            </option>
                                            <option value="Story Writer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Story Writer</strong>
                                            </option>
                                            <option value="Lyricists" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Lyricists</strong>
                                            </option>
                                            //video vj
                                            <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Video Jockey (VJ)</strong>
                                            </option>
                                            <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Radio Jockey (RJ)</strong>
                                            </option>
                                            <option value=" News Reader" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> News Reader</strong>
                                            </option>
                                            <option value="Reporter" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Reporter</strong>
                                            </option>
                                            <option value="Journalist" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Journalist</strong>
                                            </option>
                                            <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Dubbing Artist</strong>
                                            </option>
                                            <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Voice Over Artist</strong>
                                            </option>
                                            <option value="New Influncer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>New Influncer</strong>
                                            </option>
                                            <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Experienced Influncer</strong>
                                            </option>
                                            <option value="Video Advertising" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Video Advertising</strong>
                                            </option>
                                            <option value="Audio Advertising" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Audio Advertising</strong>
                                            </option>
                                            <option value="Threater" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Threater</strong>
                                            </option>
                                            <option value="Banner Advertising" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Banner Advertising</strong>
                                            </option>
                                            <option value="Singer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Singer</strong>
                                            </option>
                                            <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Music Instrumentalist</strong>
                                            </option>
                                            <option value="Music Composer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Music Composer</strong>
                                            </option>
                                            <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Director Of Photography / Cinematographer</strong>
                                            </option>
                                            <option value="Cameraman" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Cameraman</strong>
                                            </option>
                                            <option value="Camera Operator" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Camera Operator</strong>
                                            </option>
                                            <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Light man / Gaffer</strong>
                                            </option>
                                            <option value="Makeup Artist" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Makeup Artist</strong>
                                            </option>
                                            <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Visual Effects Editor</strong>
                                            </option>
                                            <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Digital imaging technician</strong>
                                            </option>
                                            <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Special Effects makeup Artist</strong>
                                            </option>
                                            <option value="Motion control technician" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Motion control technician</strong>
                                            </option>
                                            <option value="Fashion Designer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Fashion Designer</strong>
                                            </option>
                                            <option value="Hair Stylist" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Hair Stylist</strong>
                                            </option>
                                            <option value="Costume designer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Costume designer</strong>
                                            </option>
                                            <option value="Grip" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Grip</strong>
                                            </option>
                                            <option value="Sound Designer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Sound Designer</strong>
                                            </option>
                                            <option value="Sound Grip" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Sound Grip</strong>
                                            </option>
                                            <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Production Sound Mixer</strong>
                                            </option>
                                            <option value="Production Designer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Production Designer</strong>
                                            </option>
                                            <option value="Green man" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Green man</strong>
                                            </option>
                                            <option value="Property master" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Property master</strong>
                                            </option>
                                            <option value="Weapons master" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Weapons master</strong>
                                            </option>
                                            <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Set Designer / Decorator</strong>
                                            </option>
                                            <option value="Location Manager" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Location Manager</strong>
                                            </option>
                                            <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>StoryBoard Artist</strong>
                                            </option>
                                            <option value="Others" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->category != NULL) ?
                                                (($getIfAlreadyProject[$i]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Others</strong>
                                            </option>
                                        </select>
                                    </div>
                                   
                                    <div class="form-group col-lg-2">
                                        <label for="exampleInputEmail1">Date</label>  
                                        <input type="date" name="projectdate[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i]) ? (($getIfAlreadyProject[$i]->date != NULL) ? $getIfAlreadyProject[$i]->date : '') : '')); ?>">  
                                    </div>
                                   
                                    <div class="form-group col-lg-2">
                                        <label for="exampleInputEmail1">Button Name</label>  
                                        <select class="form-control" name="projectbuttonname[]" style="padding: 10px;">
                                            <option value="" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->button_name != NULL) ? (($getIfAlreadyProject[$i]->button_name == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                            <option value="View Video" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->button_name != NULL) ? (($getIfAlreadyProject[$i]->button_name == "View Video") ? 'selected' : '') : '') : ''); ?>>View Video</option>
                                            <option value="View Now" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->button_name != NULL) ? (($getIfAlreadyProject[$i]->button_name == "View Now") ? 'selected' : '') : '') : ''); ?>>View Now</option>
                                            <option value="Official Trailer" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->button_name != NULL) ? (($getIfAlreadyProject[$i]->button_name == "Official Trailer") ? 'selected' : '') : '') : ''); ?>> Official Trailer</option>
                                            <option value="View Profile" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->button_name != NULL) ? (($getIfAlreadyProject[$i]->button_name == "View Profile") ? 'selected' : '') : '') : ''); ?>>View Profile</option>
                                            <option value="View More" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->button_name != NULL) ? (($getIfAlreadyProject[$i]->button_name == "View More") ? 'selected' : '') : '') : ''); ?>>View More</option>
                                            <option value="No Button" <?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i])) ? (($getIfAlreadyProject[$i]->button_name != NULL) ? (($getIfAlreadyProject[$i]->button_name == "No Button") ? 'selected' : '') : '') : ''); ?>>No Button</option>
                                        </select>
                                    </div>
                                     <div class="form-group col-lg-2">
                                        <label for="exampleInputEmail1">Add Link - If any</label>  
                                        <input type="text" name="projectlink[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i]) ? (($getIfAlreadyProject[$i]->link != NULL) ? $getIfAlreadyProject[$i]->link : '') : '')); ?>">  
                                    </div>
                                    
                                    <div class="form-group col-lg-12">  
                                        <label for="exampleInputEmail1">Description 200 words</label>
                                        <textarea id="w3review" name="projectdesc[]" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i]) ? (($getIfAlreadyProject[$i]->description != NULL) ? limit_text($getIfAlreadyProject[$i]->description,0,200) : '') : '')); ?></textarea>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="form-group col-lg-12">  
                                        <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <a href="javascript:void(0)"
                                                data-croppie data-croppie-file="#fileproj{{ $j }}"
                                                data-croppie-input="#uploaded_proj_image{{ $j }}"
                                                data-croppie-output="#previewprojsec{{ $j }}"
                                                data-croppie-bind="file"
                                                data-croppie-viewport='{"width": 583, "height": 400}'
                                                data-croppie-boundary='{"width": 583, "height": 400}'
                                                data-croppie-progress="#artist-latest-project-progress-{{ $j }}"
                                                style="background: #637076;color: #fff;width:145px;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">Browse Banner Photo</a>
                                            <input type="file" class="d-none" target="fileproj{{ $j }}" id="fileproj{{ $j }}"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                            <input type="hidden" id="uploaded_proj_image{{ $j }}" name="fileproj{{ $j }}">
                                        </div>
                                        <div class="u-close" style="float:left; margin-right:20px;">
                                            <img id="previewprojsec{{ $j }}" src="<?php echo ((count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i]) ? (($getIfAlreadyProject[$i]->photo != NULL || $getIfAlreadyProject[$i]->photo != '') ? $getIfAlreadyProject[$i]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:170px;height:117px;object-fit:cover;margin-right:2px;">
                                            <div class="u-progress" id="artist-latest-project-progress-{{ $j }}" style="display: none; width: 170px">
            									<div class="progress">
            										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
            											<span class="sr-only" data-progress="text">0% Complete</span>
            										</div>
            									</div>
            								</div>
                                            <?php 
                                                if(count($getIfAlreadyProject) >= 1 && isset($getIfAlreadyProject[$i]))
                                                {
                                                    if($getIfAlreadyProject[$i]->photo != NULL || $getIfAlreadyProject[$i]->photo != '')
                                                    {
                                                ?>
                                            <a id="crossproj{{ $j }}" href="<?php echo URL::to('/delete-proj-image/'.$getIfAlreadyProject[$i]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        @endfor
                        <div class="row" style="margin-top:20px;margin-bottom:20px">
                            <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyProject) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                        </div>
                    </form>
                    @if( $hasThemePlan )
                        <a href="javascript:void(0)" id="addProj" class="btn btn-dark" onclick="addProj()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Add More Projects</a>
                        <a href="javascript:void(0)" class="d-none btn btn-dark" id="removeProj" onclick="removeProj()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">
                            Remove Projects
                        </a>
                    @endif
                </div>
            </div>
        </div>
        <div id="contact" class="tabcontent" style="padding-top: 15px; background: rgb(244, 243, 243);margin-top: 0px;">
            <div class="container">
                <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                    <div class="form-group achievemets">
                        <h2>Contact Us</h2>
                        <?php 
                            $userId = session()->get('user_id');
                            $getIfAlreadyContact = DB::select('select * from artist_contact where user_id='.$userId);
                            ?>
                        <form action="{{URL::to('save-contact')}}" id="contactform" style="margin-top:0px" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="form-group col-lg-12">
                                <label for="exampleInputEmail1">Title eg: Contact me to know more!!!</label>
                                <input type="text" name="titlecontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->title != NULL) ? $getIfAlreadyContact[0]->title : '') : '')); ?>">  
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="exampleInputEmail1"> Description (50 words)</label>
                                <textarea id="w3review" name="descriptioncontact" onKeyPress="return check(event,value)" onInput="checkLength(50,this)" rows="4" cols="50"><?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->description != NULL) ? limit_text($getIfAlreadyContact[0]->description,0,50) : '') : '')); ?></textarea> 
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="col-lg-3">
                                <div class="form-group"> <label for="exampleInputEmail1">Number  / Whatsapp No </label>  
                                    <input type="text" name="phonecontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->phone != NULL) ? $getIfAlreadyContact[0]->phone : '') : '')); ?>">  
                                </div>
                                <input type="checkbox" name="tick_whatsapp_feature" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->tick_whatsapp_feature != NULL) ? $getIfAlreadyContact[0]->tick_whatsapp_feature : '') : '')); ?>" style="height: 16px;margin: -1px 5px 0 0;float: left;" <?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->tick_whatsapp_feature != NULL && $getIfAlreadyContact[0]->tick_whatsapp_feature != 'no') ? 'checked' : '') : '')); ?>> Check tick for whatsaps features
                                <br> <small style="font-size: 11px;font-weight: bold;">Note: Do not type number if you have privacy issue</small>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group"> 
                                    <label for="exampleInputEmail1">Email id</label>
                                    <input type="email" name="emailcontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->email != NULL) ? $getIfAlreadyContact[0]->email : '') : '')); ?>">    
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group"> 
                                    <label for="exampleInputEmail1">Facebook</label>
                                    <input type="text" name="facebookcontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->facebook != NULL) ? $getIfAlreadyContact[0]->facebook : '') : '')); ?>">    
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <label for="exampleInputEmail1">Instagram</label>
                                <input type="text" name="instacontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->instagram != NULL) ? $getIfAlreadyContact[0]->instagram : '') : '')); ?>">    
                            </div>
                            <div class="col-lg-3">
                                <label for="exampleInputEmail1">Youtube</label>
                                <input type="text" name="youtubecontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->youtube != NULL) ? $getIfAlreadyContact[0]->youtube : '') : '')); ?>">    
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                    </div>
                    <div style="clear:both; overflow:hidden"></div>
                    <div class="row">
                    <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyContact) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        @include('artist.inc.brand-collaborations')
        @include('artist.inc.quote')
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<style>
    .d-none{
    display:none;
    }
    .success{
    color:green!important;
    }
  
 
 
 
#croppie-modal-1 .cr-viewport.cr-vp-square{width:900px !Important;height:380px !Important;}
#croppie-modal-1 .cr-boundary{width:900px !Important;height:380px !Important;}


  
  @media  only screen and (max-width: 767px) {
      
     #profile_only_mobile {   width:100% !important;margin-right:0 !Important; padding: 25px 25px !important;
    background: #eaeaea !important;}
      .cover-photo-mobile-show{    padding: 25px 25px !important;
    background: #eaeaea !important;}
      
 #portfolio-block .col-lg-6.new_video_block31 {width: 100% !Important;}


#croppie-modal-1 .cr-viewport.cr-vp-square{width:337px !Important;height:142px !Important;}
#croppie-modal-1 .cr-boundary{width:337px !Important;height:142px !Important;}

#croppie-modal-0 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-0 .cr-boundary{width:300px !Important;height:300px !Important;}
   
   

#croppie-modal-2 .cr-viewport.cr-vp-square{width:337px !Important;height:225px !Important;}
#croppie-modal-2 .cr-boundary{width:337px !Important;height:225px !Important;}      
#croppie-modal-3 .cr-viewport.cr-vp-square{width:337px !Important;height:225px !Important;}
#croppie-modal-3 .cr-boundary{width:337px !Important;height:225px !Important;}      
#croppie-modal-4 .cr-viewport.cr-vp-square{width:337px !Important;height:225px !Important;}
#croppie-modal-4 .cr-boundary{width:337px !Important;height:225px !Important;}      
#croppie-modal-5 .cr-viewport.cr-vp-square{width:337px !Important;height:225px !Important;}
#croppie-modal-5 .cr-boundary{width:337px !Important;height:225px !Important;}      
#croppie-modal-6 .cr-viewport.cr-vp-square{width:337px !Important;height:225px !Important;}
#croppie-modal-6 .cr-boundary{width:337px !Important;height:225px !Important;}      

   
   
#croppie-modal-7 .cr-viewport.cr-vp-square{width:333px !Important;height:187px !Important;}
#croppie-modal-7 .cr-boundary{width:333px !Important;height:187px !Important;}      

    
#croppie-modal-8 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-8 .cr-boundary{width:300px !Important;height:300px !Important;}      
#croppie-modal-9 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-9 .cr-boundary{width:300px !Important;height:300px !Important;}      
#croppie-modal-10 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-10 .cr-boundary{width:300px !Important;height:300px !Important;}      
#croppie-modal-11 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-11 .cr-boundary{width:300px !Important;height:300px !Important;}      
#croppie-modal-12 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-12 .cr-boundary{width:300px !Important;height:300px !Important;}      
#croppie-modal-13 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-13 .cr-boundary{width:300px !Important;height:300px !Important;}      
#croppie-modal-14 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-14 .cr-boundary{width:300px !Important;height:300px !Important;}      
#croppie-modal-15 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-15 .cr-boundary{width:300px !Important;height:300px !Important;}      
#croppie-modal-16 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-16 .cr-boundary{width:300px !Important;height:300px !Important;}      
#croppie-modal-17 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-17 .cr-boundary{width:300px !Important;height:300px !Important;}      
#croppie-modal-18 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-18 .cr-boundary{width:300px !Important;height:300px !Important;}      
#croppie-modal-19 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-19 .cr-boundary{width:300px !Important;height:300px !Important;}      

#croppie-modal-20 .cr-viewport.cr-vp-square{width:292px !Important;height:194px !Important;}
#croppie-modal-20 .cr-boundary{width:292px !Important;height:194px !Important;}      
#croppie-modal-21 .cr-viewport.cr-vp-square{width:292px !Important;height:194px !Important;}
#croppie-modal-21 .cr-boundary{width:292px !Important;height:194px !Important;}      
#croppie-modal-22 .cr-viewport.cr-vp-square{width:292px !Important;height:194px !Important;}
#croppie-modal-22 .cr-boundary{width:292px !Important;height:194px !Important;}      
#croppie-modal-23 .cr-viewport.cr-vp-square{width:292px !Important;height:194px !Important;}
#croppie-modal-23 .cr-boundary{width:292px !Important;height:194px !Important;}      
#croppie-modal-24 .cr-viewport.cr-vp-square{width:292px !Important;height:194px !Important;}
#croppie-modal-24 .cr-boundary{width:292px !Important;height:194px !Important;}      
#croppie-modal-25 .cr-viewport.cr-vp-square{width:292px !Important;height:194px !Important;}
#croppie-modal-25 .cr-boundary{width:292px !Important;height:194px !Important;}      

#croppie-modal-26 .cr-viewport.cr-vp-square{width:300px !Important;height:200px !Important;}
#croppie-modal-26 .cr-boundary{width:292px !Important;height:194px !Important;}      
#croppie-modal-27 .cr-viewport.cr-vp-square{width:300px !Important;height:200px !Important;}
#croppie-modal-27 .cr-boundary{width:292px !Important;height:194px !Important;}      
#croppie-modal-28 .cr-viewport.cr-vp-square{width:300px !Important;height:200px !Important;}
#croppie-modal-28 .cr-boundary{width:292px !Important;height:194px !Important;}      

  
   }
    
    
    
    
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
{{ HTML::style('public/css/bootstrap-tagsinput.css')}}
{{ HTML::script('public/js/jquery.validate.js')}}
{{ HTML::script('public/js/bootstrap-tagsinput.js')}}
{{ HTML::script('public/js/bootstrap3-typeahead.js')}}
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    function getStates(value)
    {
        $('#state').empty();
        $('#state').append('<option value="">Select State</option>');
        if(value != '')
        {
            $.ajax({
                url:'{{URL::to("/get-states")}}',
                data:{id:value,'_token':'<?php echo csrf_token(); ?>'},
                type:'post',
                success:function(res)
                {
                    let parse= JSON.parse(res);
                    if(parse.length > 0)
                    {
                        for(let i=0;i<parse.length;i++)
                            $('#state').append('<option value="'+parse[i].name+'">'+parse[i].name+'</option>');
                    }else{
                        $('#state').append('<option value="">No States Found</option>');    
                    }
                }
            })
        }else{
            $('#state').append('<option value="">Select State</option>');    
        }
    }
    
    function getCities(value)
    {
        $('#city').empty();
        $('#city').append('<option value="">Select City</option>');
        if(value != '')
        {
            $.ajax({
                url:'{{URL::to("/get-cities")}}',
                data:{id:value,'_token':'<?php echo csrf_token(); ?>'},
                type:'post',
                success:function(res)
                {
                    let parse= JSON.parse(res);
                    if(parse.length > 0)
                    {
                        for(let i=0;i<parse.length;i++)
                            $('#city').append('<option value="'+parse[i].name+'">'+parse[i].name+'</option>');
                    }else{
                       $('#city').append('<option value="">No Cities Found</option>');     
                    }
                }
            })
        }else{
            $('#city').append('<option value="">Select City</option>');     
        }
    }
    
    $('#artistForm').on('submit',function(e)
    {
        e.preventDefault();
        let arr = $(this).serializeArray();
        let count = 0;
        for(let i =0 ;i<arr.length;i++)
        {
            if(arr[i].name == 'artistcheck[]')
            {
                count++;
            }
        }
        if(count > 5)
        {
            alert('Please Select Upto 5 Multiple Options');
        }
        else if(count == 0) {
            alert('Please Select At Least One Option');
        } else {
            let data = new FormData(this);
            $.ajax({
                url:$(this).attr('action'),
                data:data,
                contentType: false,
                cache: false,
                processData:false,
                type:'post',
                success:function(res)
                {
                    if(res == 'exists') {
                        alert('Username already exists.');
                    } 
                    else {                        
                        alert('Data Saved Successfully');
                        location.reload();
                    }
                }
            });
        }
    })
    
    $('#achivementForm').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#reelform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#videoform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#whatwedoform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#projectform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        console.log(data);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                // location.reload();
            }
        });
    })
    
    function showPhotoUploader(num)
    {
        $('#photosectionphoto'+num).click();
    }
    
    $('#galleryform').on('submit',function(e)
    {
        e.preventDefault();
        let oldData = new FormData(this);

        $.ajax({
            url:$(this).attr('action'),
            data:oldData,
            headers: {
                "X-CSRF-TOKEN": "{{ csrf_token() }}"
            },
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#contactform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";
    }
    
    function showAchivementUploader(num)
    {
        $('#fileachive'+num).click();
    }
    
    function showFileLabel(num)
    {
        $('#fileachivelabel'+num).html('1 File Selected <i class="fa fa-times" onclick="removeAchivementFile('+num+')"></i>');
    }
    
    function removeAchivementFile(num)
    {
        const file = document.querySelector('#fileachive'+num);
        file.value = '';
        $('#fileachivelabel'+num).html('');
    }
    
    function openReelVideo()
    {
        $('#reelvideo').click();
    }
    
    function openReelThumb(){
        let fileinput = $('input[data-target="video_thumb"]');
        fileinput.click();
    }
    
    function addfourmore(num)
    {
        if(num == 2)
        {
            $('#portfolio3').removeClass('d-none');
            $('#addPhotoThreeMore').removeClass('d-none');
            $('#removePhotoTwoMore').removeClass('d-none');
            $('#addPhotoTwoMore').addClass('d-none');
        }
        if(num == 3)
        {
            $('#portfolio4').removeClass('d-none');
            $('#addPhotoThreeMore').addClass('d-none');
            $('#removePhotoThreeMore').removeClass('d-none');
            $('#addPhotoTwoMore').addClass('d-none');
        }
    }
    
    function removefourmore(num)
    {
        if(num == 2)
        {
            $('#portfolio3').addClass('d-none');
            $('#removePhotoTwoMore').addClass('d-none');
            $('#addPhotoTwoMore').removeClass('d-none');
            $('#addPhotoThreeMore').addClass('d-none');
        }
        if(num == 3)
        {
            $('#portfolio4').addClass('d-none');
            if($('#portfolio3').hasClass('d-none'))
            {
                $('#addPhotoThreeMore').addClass('d-none');
            }else{
                $('#addPhotoThreeMore').removeClass('d-none');
            }
            $('#removePhotoThreeMore').addClass('d-none');
        }
    }
    
    async function getThumbnailForVideo(videoUrl) {
      const video = document.createElement("video");
      const canvas = document.createElement("canvas");
      video.style.display = "none";
      canvas.style.display = "none";
    
      // Trigger video load
      await new Promise((resolve, reject) => {
        video.addEventListener("loadedmetadata", () => {
          video.width = video.videoWidth;
          video.height = video.videoHeight;
          canvas.width = '665';
          canvas.height = '372';
          // Seek the video to 25%
          video.currentTime = video.duration * 0.25;
        });
        video.addEventListener("seeked", () => resolve());
        video.src = videoUrl;
      });
    
      // Draw the thumbnailz
      canvas
        .getContext("2d")
        .drawImage(video, 0, 0, '672', '372');
      const imageUrl = canvas.toDataURL("image/png");
      return imageUrl;
    }
    
    const videoFile1 = document.querySelector('#videoFile1');
    
    if( videoFile1 ){
        videoFile1.addEventListener("change", async e => {
            var flag = true;
        	var imageType = /video.*/;  
        	var file = e.target.files[0];
        	// check file type
        	if (!file.type.match(imageType)) {  
        	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
        	  flag = false;
        	  return false;	
        	} 
        	// check file size
        	if (parseInt(file.size / 1024) > (1024*50)) {  
        	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
        	  flag = false;
        	  return false;	
        	} 
        	if(flag)
        	{
        	    const fileUrl = URL.createObjectURL(file);
                const thumbUrl = await getThumbnailForVideo(fileUrl);
                $('#generatedvideosecthumb1').val(thumbUrl);
                $('#previewvideosec1').attr('src',thumbUrl);
                $('.u-progress1').show();
        	    handleFiles1(file,'#uploadedvideosec1','1');
        	}
        });
    }
    
    const videoFile2 = document.querySelector('#videoFile2');
    if( videoFile2 ){
        videoFile2.addEventListener("change", async e => {
            var flag = true;
        	var imageType = /video.*/;  
        	var file = e.target.files[0];
        	// check file type
        	if (!file.type.match(imageType)) {  
        	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
        	  flag = false;
        	  return false;	
        	} 
        	// check file size
        	if (parseInt(file.size / 1024) > (1024*50)) {  
        	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
        	  flag = false;
        	  return false;	
        	} 
        	if(flag)
        	{
        	    const fileUrl = URL.createObjectURL(file);
                const thumbUrl = await getThumbnailForVideo(fileUrl);
                $('#generatedvideosecthumb2').val(thumbUrl);
                $('#previewvideosec2').attr('src',thumbUrl);
                $('.u-progress2').show();
        	    handleFiles1(file,'#uploadedvideosec2','2');
        	}
        });
    }
    
    const videoFile3 = document.querySelector('#videoFile3');
    if( videoFile3 ){
        videoFile3.addEventListener("change", async e => {
            var flag = true;
        	var imageType = /video.*/;  
        	var file = e.target.files[0];
        	// check file type
        	if (!file.type.match(imageType)) {  
        	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
        	  flag = false;
        	  return false;	
        	} 
        	// check file size
        	if (parseInt(file.size / 1024) > (1024*50)) {  
        	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
        	  flag = false;
        	  return false;	
        	} 
        	if(flag)
        	{
        	    const fileUrl = URL.createObjectURL(file);
                const thumbUrl = await getThumbnailForVideo(fileUrl);
                $('#generatedvideosecthumb3').val(thumbUrl);
                $('#previewvideosec3').attr('src',thumbUrl);
                $('.u-progress3').show();
        	    handleFiles1(file,'#uploadedvideosec3','3');
        	}
        });        
    }
    const videoFile4 = document.querySelector('#videoFile4');
    if( videoFile4 ){
        videoFile4.addEventListener("change", async e => {
            var flag = true;
        	var imageType = /video.*/;  
        	var file = e.target.files[0];
        	// check file type
        	if (!file.type.match(imageType)) {  
        	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
        	  flag = false;
        	  return false;	
        	} 
        	// check file size
        	if (parseInt(file.size / 1024) > (1024*50)) {  
        	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
        	  flag = false;
        	  return false;	
        	} 
        	if(flag)
        	{
        	    const fileUrl = URL.createObjectURL(file);
                const thumbUrl = await getThumbnailForVideo(fileUrl);
                $('#generatedvideosecthumb4').val(thumbUrl);
                $('#previewvideosec4').attr('src',thumbUrl);
                $('.u-progress4').show();
        	    handleFiles1(file,'#uploadedvideosec4','4');
        	}
        });
    }
    
    const videoFile5 = document.querySelector('#videoFile5');
    if( videoFile5 ){
        videoFile5.addEventListener("change", async e => {
            var flag = true;
        	var imageType = /video.*/;  
        	var file = e.target.files[0];
        	// check file type
        	if (!file.type.match(imageType)) {  
        	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
        	  flag = false;
        	  return false;	
        	} 
        	// check file size
        	if (parseInt(file.size / 1024) > (1024*50)) {  
        	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
        	  flag = false;
        	  return false;	
        	} 
        	if(flag)
        	{
        	    const fileUrl = URL.createObjectURL(file);
                const thumbUrl = await getThumbnailForVideo(fileUrl);
                $('#generatedvideosecthumb5').val(thumbUrl);
                $('#previewvideosec5').attr('src',thumbUrl);
                $('.u-progress5').show();
        	    handleFiles1(file,'#uploadedvideosec5','5');
        	}
        });
    }    
    
    const videoFile6 = document.querySelector('#videoFile6');
    if( videoFile6 ){
        videoFile6.addEventListener("change", async e => {
            var flag = true;
        	var imageType = /video.*/;  
        	var file = e.target.files[0];
        	// check file type
        	if (!file.type.match(imageType)) {  
        	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
        	  flag = false;
        	  return false;	
        	} 
        	// check file size
        	if (parseInt(file.size / 1024) > (1024*50)) {  
        	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
        	  flag = false;
        	  return false;	
        	} 
        	if(flag)
        	{
        	    const fileUrl = URL.createObjectURL(file);
                const thumbUrl = await getThumbnailForVideo(fileUrl);
                $('#generatedvideosecthumb6').val(thumbUrl);
                $('#previewvideosec6').attr('src',thumbUrl);
                $('.u-progress6').show();
        	    handleFiles1(file,'#uploadedvideosec6','6');
        	}
        });
    }    
    
    const fileInput = document.querySelector("#reelvideo");
    
    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), 
            n = bstr.length, 
            u8arr = new Uint8Array(n);
            
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        
        return new File([u8arr], filename, {type:mime});
    }
    
    if( fileInput ){
        fileInput.addEventListener("change", async e => {
            var flag = true;
        	var imageType = /video.*/;  
        	var file = e.target.files[0];
        	// check file type
        	if (!file.type.match(imageType)) {  
        	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
        	  flag = false;
        	  return false;	
        	} 
        	// check file size
        	if (parseInt(file.size / 1024) > (1024*50)) {  
        	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
        	  flag = false;
        	  return false;	
        	} 
        	if(flag)
        	{
        	    const fileUrl = URL.createObjectURL(file);
                const thumbUrl = await getThumbnailForVideo(fileUrl);
                $('#generated_video_thumb').val(thumbUrl);
                $('#profile_photo_preview img').attr('src',thumbUrl);
                $('.u-details').show();
        	    handleFiles1(file,'#videouploaded','');
        	}
        });
    }
    
    handleFiles1 = function (files,selector,num,e){
    		
    	var fileName = files.name;
    	fileName.split('.').slice(0, -1).join('.');
    	$('.u-title').html(fileName);
    	console.log(num);
		debugger;
    	var info = '<div class="u-size'+num+'">'+(parseInt(parseInt(files.size / 1024))/1024).toFixed(2)+' MB / <span class="up-done'+num+'"></span> MB</div><div class="u-progress'+num+'"><div class="progress'+num+'"><div class="progress-bar'+num+'" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span class="sr-only'+num+'">0% Complete</span></div></div><div class="u-close"><a href="#"><i class="cvicon-cv-cancel"></i></a></div></div>';
    			
    	$("#upload-progress"+num).show("fast",function(){
    		$("#upload-progress"+num).html(info); 
    		uploadVideoFile1(files,selector,num);
    	});
    		
      }
      /*Upload Video File*/
      uploadVideoFile1 = function(file,target,num){
    	 console.log(file);
    	 
    	// check if browser supports file reader object 
        	if (typeof FileReader !== "undefined"){
        	//alert("uploading "+file.name);  
        	reader = new FileReader();
        	reader.onload = function(e){
        	    console.log(e);
        		//alert(e.target.result);
        		//$('.preview img').attr('src',e.target.result).css("width","70px").css("height","70px");
        	}
        	reader.readAsDataURL(file);
        	
        	var formdata = new FormData();
        	formdata.append("video", file);
        	
        	xhr = new XMLHttpRequest();
        	xhr.open("POST", "upload/video");
        	
        	xhr.upload.addEventListener("progress", function (event) {
        		console.log(event);
				debugger;
        		if (event.lengthComputable) {
        			$(".progress-bar"+num).css("width",(event.loaded / event.total) * 100 + "%");
        			$(".progress-bar"+num).css("aria-valuenow",(event.loaded / event.total) * 100);
        			$(".sr-only"+num).html(" "+((event.loaded / event.total) * 100).toFixed() + "%");
        			$(".up-done"+num).html((parseInt(parseInt(event.loaded / 1024))/1024).toFixed(2));
        			$('.percent-process'+num).html('<strong>'+parseInt((event.loaded / event.total) * 100).toFixed(0)+'%</strong>');
        		}
        		else {
        			alert("Failed to compute file upload length");
        		}
        	}, false);
        
        	xhr.onreadystatechange = function (oEvent) {  
        	  if (xhr.readyState === 4) {  
        		if (xhr.status === 200) {  
        		  $(".progress-bar"+num).css("width","100%");
        		  $(".progress-bar"+num).attr("aria-valuenow","100");
        		  $(".sr-only"+num).html("100%");
        		  $(".up-done").html((parseInt(parseInt(file.size / 1024))/1024).toFixed(2));
        		  $('.u-desc'+num).html("<strong>100% Completed.</strong> Your video has been successfully upload.");
        		  $('.u-desc'+num).addClass('text-success');
        		} else {  
        		  alert("Error"+ xhr.statusText);  
        		}  
        	  }  
        	}; 
        	
        	xhr.onload  = function() {
        	   var jsonResponse = xhr.response;
        	   result = JSON.parse(jsonResponse);
        	   if(result.status == true){
                    $(target).val(result.file);
        	   }else{
        		   alert('File is not uploading. Please try again.')
        	   }	  
        	};
        
        	
        	var csrfToken = $('#reelform').find('input[name=_token]').val();
        	xhr.setRequestHeader('X-csrf-token', csrfToken); 	
        	
        	xhr.send(formdata);
        
        	}else{
        		alert("Your browser doesnt support FileReader object");
        	} 		
          }
    
    function showReelThumbLabel()
    {
        $('#reelvideothumblabel').html('1 File Selected <i class="fa fa-times" onclick="removeVideoThumbFile()"></i>');
    }
    
    function removeVideoFile()
    {
        const file = document.querySelector('#reelvideolabel');
        file.value = '';
        $('#reelvideolabel').html('');
    }
    
    function removeVideoThumbFile()
    {
        const file = document.querySelector('#reelvideothumblabel');
        file.value = '';
        $('#reelvideothumblabel').html('');
    }
    
    function removeAchivementFile(num)
    {
        const file = document.querySelector('#fileachive'+num);
        file.value = '';
        $('#fileachivelabel'+num).html('');
    }
    
    function addMoreAchivement()
    {
        $('#addMoreAchivement').addClass('d-none');
        $('#removeMoreAchivement').removeClass('d-none');
        $('#cloneachivement').removeClass('d-none');
    }
    
    function removeMoreAchivement()
    {
        $('#addMoreAchivement').removeClass('d-none');
        $('#removeMoreAchivement').addClass('d-none');
        $('#cloneachivement').addClass('d-none');
    }
    
    function removeWhatWeDo()
    {
        for(let i=5;i<=8;i++)
        {
            $('#title'+i).val('');
            $('#desc'+i).val('');
        }
        $('#addMore').removeClass('d-none');
        $('#removeMore').addClass('d-none');
        $('#clonediv').addClass('d-none');
    }
    
    function addProj()
    {
        $('#addProj').addClass('d-none');
        $('#removeProj').removeClass('d-none');
        $('#cloneprojdiv').removeClass('d-none');    
    }
    
    function removeProj()
    {
        $('#addProj').removeClass('d-none');
        $('#removeProj').addClass('d-none');
        $('#cloneprojdiv').addClass('d-none');
    }
    
    function removeMoreVideo()
    {
        $('#videoAdd').removeClass('d-none');
        $('#videoRemove').addClass('d-none');
        $('#clonevideodiv').addClass('d-none');
    }
    
    function addMoreVideo()
    {
        $('#videoAdd').addClass('d-none');
        $('#videoRemove').removeClass('d-none');
        $('#clonevideodiv').removeClass('d-none');    
    }
    
    function addMoreWhatWeDo()
    {
        $('#addMore').addClass('d-none');
        $('#removeMore').removeClass('d-none');
        $('#clonediv').removeClass('d-none');
    }
    
    function openProjUploader(num)
    {
        $('#fileproj'+num).click();
    }
    
    function uploadVideo(num)
    {
        $('#videoFile'+num).click();
    }
    
    function uploadVideoThumbnail(num)
    {
        $('#videoThumb'+num).click();
    }
    
    function showProjFileLabel(num)
    {
        $('#fileprojlabel'+num).html('1 File Selected <i class="fa fa-times" onclick="removeProjFile('+num+')"></i>');
    }
    
    function removeProjFile(num)
    {
        const file = document.querySelector('#fileproj'+num);
        file.value = '';
        $('#fileprojlabel'+num).html('');
    }

    $("#about_tags").tagsinput({
        typehead: {
            source: []
        },
        freeInput: true,
        maxTags: 5
    });

    $(document).on('keypress', '.bootstrap-tagsinput input', function(e){
        if (e.keyCode == 13){
            e.keyCode = 188;
            e.preventDefault();
        };
    });
    
    $('#video-language').tagsinput({
    	  typeahead: {
    		source: ['Afar', 'Abkhaz', 'Avestan', 'Afrikaans', 'Akan', 'Amharic','Aragonese', 'Arabic', 'Assamese', 'Avaric', 'Aymara', 'Azerbaijani','South Azerbaijani', 'Bashkir', 'Belarusian', 'Bulgarian', 'Bihari', 'Bislama',
    		'Bambara', 'Bengali', 'Tibetan Standard', 'Breton', 'Bosnian', 'Catalan; Valencian','Chechen', 'Chamorro', 'Corsican', 'Cree', 'Czech', 'Old Church Slavonic','Chuvash', 'Welsh', 'Danish', 'German', 'Maldivian', 'Dzongkha',
    		'Ewe', 'Greek', 'English', 'Esperanto', 'Spanish', 'Estonian','Basque', 'Persian ', 'Fula', 'Finnish', 'Fijian', 'Faroese',' French', 'Western Frisian', 'Irish', 'Scottish Gaelic', 'Galician', 'Guaraní',
    		'Gujarati', 'Manx', 'Hausa', 'Hebrew ', 'Hindi', 'Hiri Motu','Croatian', 'Haitian', 'Hungarian', 'Armenian', 'Herero', 'Interlingua',' Indonesian', 'Interlingue', 'Igbo', 'Nuosu', 'Inupiaq', 'Ido',
    		'Icelandic', 'Italian', 'Inuktitut', 'Japanese', 'Javanese', 'Georgian','Kongo', 'Gikuyu', 'Kwanyama', 'Kazakh', 'Kalaallisut', 'Khmer','Kannada', 'Korean', 'Kanuri', 'Kashmiri', 'Kurdish', 'Komi',
    		'Cornish', 'Kyrgyz', 'Latin', 'Luxembourgish', 'Ganda', 'Limburgish','Lingala', 'Lao', 'Lithuanian', 'Luba-Katanga', 'Latvian', 'Malagasy','Marshallese', 'Māori', 'Macedonian', 'Malayalam', 'Mongolian', 'Marathi',
    		'Malay', 'Maltese', 'Burmese', 'Nauru', 'Norwegian Bokmål', 'North Ndebele','Nepali', 'Ndonga', 'Dutch', 'Norwegian Nynorsk', 'Norwegian', 'South Ndebele','Navajo', 'Chichewa', 'Occitan', 'Ojibwe', 'Oromo', 'Oriya',
    		'Ossetian', 'Panjabi', 'Pāli', 'Polish', 'Pashto', 'Portuguese','Quechua', 'Romansh', 'Kirundi', 'Romanian', 'Russian', 'Kinyarwanda','Sanskrit', 'Sardinian', 'Sindhi', 'Northern Sami', 'Sango', 'Sinhala',
    		'Slovak', 'Slovene', 'Samoan', 'Shona', 'Somali', 'Albanian','Serbian', 'Swati', 'Southern Sotho', 'Sundanese', 'Swedish', 'Swahili','Tamil', 'Telugu', 'Tajik', 'Thai', 'Tigrinya', 'Turkmen',
    		'Tagalog', 'Tswana', 'Tonga', 'Turkish', 'Tsonga', 'Tatar','Twi', 'Tahitian', 'Uyghur', 'Ukrainian', 'Urdu	', 'Uzbek','Venda', 'Vietnamese', 'Volapük', 'Walloon', 'Wolof', 'Xhosa',
    		'Yiddish', 'Yoruba', 'Zhuang', 'Chinese', 'Zulu']
    	  },
    	  freeInput: true
    });	
    
    validateThumbFile = function(files,e){
        var flag = true;
        var imageType = /image.*/;  
        var file = files[0];
        // check file type
        if (!file.type.match(imageType)) {  
        alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
        flag = false;
        return false;	
        } 
        // check file size
        if (parseInt(file.size / 1024) > (1024*2)) {  
        alert("File \""+file.name+"\" is too big.");
        flag = false;
        return false;	
        } 
        
        if(flag == true){
            uploadImageFile(file);
        }else{
            return flag;
        }	
        
    };

    validateThumbFile2 = function(files,e){
        var flag = true;
        var imageType = /image.*/;  
        var file = files[0];
        // check file type
        if (!file.type.match(imageType)) {  
        alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
        flag = false;
        return false;	
        } 
        // check file size
        if (parseInt(file.size / 1024) > (1024*2)) {  
        alert("File \""+file.name+"\" is too big.");
        flag = false;
        return false;	
        } 
        
        if(flag == true){
            uploadImageFile(file);
        }else{
            return flag;
        }	
        
    };
    
    uploadImageFile = function(file){
        var formdata = new FormData();
        formdata.append("image", file);	
        //console.log(file);
        xhr = new XMLHttpRequest();
        //console.log(xhr);
        xhr.open("POST", "upload/temp_image");
        xhr.onload  = function() {
        var jsonResponse = xhr.response;
        result = JSON.parse(jsonResponse);
        console.log(result);
        if(result.status == true){
            
        }else{
            alert('File is not uploading. Please try again.')
        }	  
        };	
        var csrfToken = $('#artistForm').find('input[name=_token]').val();
        xhr.setRequestHeader('X-csrf-token', csrfToken); 	
        xhr.send(formdata);		
    }
    
    function showGalThumb(input,index)
    {
        var flag = true;
        var imageType = /image.*/;  
        var file = input[0];
        // check file type
        if (!file.type.match(imageType)) {  
            alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
            flag = false;
            return false;	
        } 
        // check file size
        if (parseInt(file.size / 1024) > (1024*2)) {  
            alert("File \""+file.name+"\" is too big.");
            flag = false;
            return false;	
        } 
        
        if(flag == true){
            var formdata = new FormData();
            formdata.append("image", file);	
            //console.log(file);
            xhr = new XMLHttpRequest();
            //console.log(xhr);
            xhr.open("POST", "upload/temp_image");
            xhr.onload  = function() {
                var jsonResponse = xhr.response;
                result = JSON.parse(jsonResponse);
                if(result.status == true){
                    $('#gal'+index).attr('src',result.image);
                    $('#crossgal'+index).hide();
                }else{
                    alert('File is not uploading. Please try again.')
                }
            };	
            var csrfToken = $('#artistForm').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken); 	
            xhr.send(formdata);		
        }else{
            return flag;
        }
    }
    
    function check(e,value)
    {
        //Check Charater
        var unicode=e.charCode? e.charCode : e.keyCode;
        if (value.indexOf(".") != -1)if( unicode == 46 )return false;
    }
    
    function checkLength(len,ele){
      var fieldLength = ele.value.length;
      let split = ele.value.split(' ');
      if(split.length <= len)
      {
          return true;
      }
      else
      {
        var str = ele.value;
        let finalstr = '';
        for(let i=0;i<len;i++)
        {
            finalstr += split[i] + ' ';
        }
        ele.value = finalstr;
      }
    }
    
</script>
@endsection