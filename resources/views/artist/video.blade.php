

@extends('layouts.home')
@section('content')
    <style>
      .shortname{
    background: #28b47e !important;
    border-radius: 100px;
    width: 35px;
    color: #fff;
    height: 35px;
    text-align: center;
    margin-bottom: 7px;
    line-height: 38px;
    font-weight: bold;
    font-size: 16px;
  }
  
  #live-video-user-list-home{display:none;}
  
#home_page_video .b-video .v-desc {height: 56px;margin-bottom:  0px;padding: 10px;background: #fff;}
#home_page_video .v-desc p {margin: 0px;padding: 0px;font-size: 12px;}
#home_page_video .v-desc a {font-size: 12.5px;line-height: 18px;   margin: px 0 0 0;display: block;}
 
 
     .home_page_second_block {
    background:#eaeaea !important;
 
    border: 0px solid #e3e3e3;
}

 

        .layout .content .chat .chat-footer {
            padding-top: 0;
        }

        body:not(.form-membership) {
            overflow: auto;
        }

        .content {
            background: #eaeaea !important;
        }

        .chat {
            background: #fff;
        }

        .container .content {
            padding-left: 0px;
            padding-right: 0;
        }

        .navigation {
            display: none
        }
        
        #desktop09124{padding:0 10px 0 0;}

       @media only screen and (max-width: 767px) {
           
      #desktop09124{padding:0 15px;}      
           
           #ml09198{padding:0 10px !Important;}
           
.col-lg-7.col-sm-7.pad-01 p {
    font-size: 11px;
    line-height: 13px;
    margin: -3px 10px 14px 0;
    height: auto;
    overflow: hidden;
    padding: 0 0 10px 0;
}

#new_block--0991{width:79% !important; margin-left:0px !important;}
.list{clear: both;overflow: hidden;width: 100%;}
.single-video .h-video .v-img {height: auto !Important;}
.pad-01 {
    background: #eaeaea !important;
    padding: 0px 10px 0 10px !important;;
    margin-bottom: 0px;
}

.single-video .h-video .v-views {
    font-size: 9px;}
.cb-content.videolist .videoitem {    margin-bottom: 0px !important;}

.mobile-artist-gallery {
    background: #c42047;
}
#home_page_video .user a {
    float: none;
    line-height: initial;
    width: 35px;
    padding: 0;
    overflow: hidden;
}
 #sm71 .user {
    bottom: 55px !Important
}
#sm71 .user {
    width: 33px;
    clear: both;
    overflow: hidden;
    position: absolute;
    bottom: 40px;
    right: 10px;
    z-index: 1;
}

.single-video .author .sv-views .sv-views-count {text-align: left;}

.pad-01 {
    float: right;
    width: 58%;
    background: #fff !important;
}


.col-lg-5.col-sm-5 {
    padding: 0;
    width: 42%;
    float: left;
}


.h-video.row{    margin: 0px 0%;
    width: 100%;
    float: left;}
    
   .col-lg-5.col-sm-5 {padding: 0;}
    
            nav.navigation {
                background: #fff;
                width: 40px;
                position: absolute;
                left: 0px;
                height: 40px;
            }

            .layout .content .chat .chat-header .chat-header-action {
                margin-top: 15px;
                width: 65px;
                float: right;
                margin: -38px 0 0 13px;
            }

            .layout .content .sidebar-group .sidebar .list-group-item .users-list-body h5 {
                font-size: 14px !important;
            }

            .layout .content {
                margin-top: 40px;
            }
        }

        .layout .content .chat .chat-body.no-message {
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            height: 473px;
            overflow: scroll;
            background: #eaeaea;
            margin: 0;
        }

        footer {
            display: none;
        }

        .messages {
            padding: 20px;
        }

        .right_big_video {
            float: right;
            width: 92%
        }

        .icon_small {
            float: left;
            width: 8%;
        }

        .icon_small a {
            font-size: 11px;
            display: block;
            float: left;
            width: 48px;
            height: 48px;
            overflow: hidden;
            margin-bottom: 5px
        }

        .icon_small a.user_imgs {
            border-radius: 1px;
            width: 50px;
            height: 30px;
            text-align: center;
            line-height: 13px;
            margin-bottom: 1px
        }

        .icons_b.first1 {
            width: 50px;
            height: 30px;
            text-align: center;
            line-height: 13px;
            margin-bottom: 1px
        }

        .icon_small span {
            -webkit-transition: all 500ms ease;
            -ms-transition: all 500ms ease;
            -o-transition: all 500ms ease;
            transition: all 500ms ease;
            opacity: 0;
            width: max-content;
            top: 6px;
            position: absolute;
            left: 50.9px;
            color: #fff;
            padding: 11px 15px 9px 15px;
            background: #f72e5e;
            z-index: -9;
            font-size: 12px;
        }

        .icons_b {
            position: relative;
            float: left;
            width: 48px;
            height: 48px;
            margin-bottom: 5px
        }

        .icon_small a img {
            width: 50px;
            height: 50px;
        }

        .icon_small a img {
            transition: transform .5s;
        }

        .icon_small a:hover img {
            transform: scale(1.5);
        }

        .icons_b {
            -webkit-transition: all 500ms ease;
            -ms-transition: all 500ms ease;
            -o-transition: all 500ms ease;
            transition: all 500ms ease;
        }

        .first2:hover span {
            opacity: 1;
            z-index: 999;
        }

        .first3:hover span {
            opacity: 1;
            z-index: 999;
        }

        .first4:hover span {
            opacity: 1;
            z-index: 999;
        }

        .first5:hover span {
            opacity: 1;
            z-index: 999;
        }

        .first6:hover span {
            opacity: 1;
            z-index: 999;
        }

        .first7:hover span {
            opacity: 1;
            z-index: 999;
        }

        .single-video .h-video {
            margin-bottom: 2px;
        }

        .single-video .h-video .v-img {
            border-radius: 2px;
            position: relative;
            height: 80px;
            overflow: hidden;
        }

        .pad-01 {
            padding: 0px;
        }
        .middle-head {
            font-size: 22px;
            margin: 30px 0 0 -13px;
        }

        #sm71 {
            padding-bottom: 40px;
        }

              #sm71 .user {
               width: 36px;
    clear: both;
    overflow: hidden;
    position: absolute;
    bottom:40px;
    right: 10px;
    z-index: 1;
}
    #sm71  .shortname {

    margin-bottom: -7px;}  
 

  </Style>
   
   
 <link href="https://www.entertainmentbugs.com/public/css/casting.css" rel="stylesheet">
  
    
   <div class="content-wrapper menu-top1">
 <?php
                         $userid=session()->get('user_id');
     if($userid != ''){
            $artist_about=\App\Models\ArtistAbout::where('user_id',$userid)->get();
                    $progress = 0;
                 if($artist_about->count() > 0 ){
                     $progress = 1;
                 }
                 if($progress){
                     ?>
                     <style>#user-menu{display:none}</style>
                     
                              <div class="containers" id="">
            <div class="casting-menu">
               <ul>
                    <li><a href="{{ URL::to('artist-profile') }}" class="mobile-artist-profile"> Artist Profile</a></li>
                    <li><a href="{{ URL::to('artist-videos') }}" class="mobile-artist-videos"> Artist Videos</a></li>
                    <li><a href="{{ URL::to('artist-gallery') }}" class="mobile-artist-gallery"> Artist Gallery</a></li>
                    <li><a href="">Influencer</a></li>
                    <li><a href="">Actor / Model</a></li>
                    <!--<li><a href="{{URL::to('production-houses')}}">Production House</a></li>-->
                    <li><a href="{{URL::to('find-auditions')}}">Autions</a></li> 
               </ul>
            </div>
         </div>           
                <?php 
                 } 
                 
                     $product_about=\App\Models\ProductionAbout::where('user_id',$userid)->get();
                $progress = 0;
                if( $product_about->count() > 0){
                     $progress = 1;
                 }
                if($progress){
                ?>
                 <style>#user-menu{display:none}</style>
          
                <?php 
                }
            }//check if user login ?>   
            
            <div class="containers" id="user-menu">
            <div class="casting-menu">
               <ul>
                     <li><a href="{{URL::to('artist-profile')}}"> Artist Profile</a></li>
                    <li><a href="{{URL::to('artist-videos')}}"> Artist Videos</a></li>
                    <li><a href="{{URL::to('artist-gallery')}}"> Artist Gallery</a></li> 
                    <li><a href="">Influencer</a></li>
                    <li><a href="">Actor / Model</a></li>
                    <li><a href="{{URL::to('production-houses')}}">Production House</a></li>
                    <li><a href="{{URL::to('find-auditions')}}">Find Autions</a></li> 
                    <li><a href="{{URL::to('post-audition')}}">Post Aution</a></li> 
               </ul>
            </div>
         </div>
         </div>   
         
        

    <!--artist video display start-->
    <div class="single-video light" style="padding-top:15px;padding-bottom: 15px; background:#eaeaea !important;">
        <div class="container">
            <div class="row" style="background: #fff;padding:15px;border-bottom: solid 2px #f72e5e;">
                <div class="row">
                    <div class="col-lg-8 col-xs-12 col-sm-12">
                        <div class="sv-video">
                            <video poster="{{ $video->thumb }}" id="" width="100%" controls=""
                                style="width:100%;height:100%;">
                                <source
                                    src="{{ $video->video }}"
                                    type="video/mp4">
                            </video>
                        </div>
                        <div class="clear"></div>
                        <div class="author" style="box-shadow: none;border: 0px;padding: 0px 0 10px 0px;margin: 0;">
                            <div class="author-head">
                                <h1 style="padding-bottom:0px">{{ $video->video_title }}</h1>
                                <p>{{ $video->description }}</p>
                                <div class="sv-name" style="padding-left:0px; margin-top:-16px">
                                    <div class="c-sub" id="followunfollowbtn"></div>
                                </div>
                                <!--<div class="sv-views" style="margin-top:-7px">-->
                                <!--    <div class="sv-views-count">-->
                                <!--        Share-->
                                <!--    </div>-->
                                <!--</div>-->
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <!-- right column -->
                    <div class="col-lg-4 col-xs-12 col-sm-12" id="desktop09124">
                                          
<style>
.ads-cont{background:#eaeaea !important;}
.tags_white{background:#fff !important;}
#ads_name{background:#fff !Important;}
.col-lg-7.col-sm-7.pad-01 p {font-size: 11px;line-height: 13px;margin: -3px 10px 14px 0;height: 38px;overflow: hidden;}
.single-video .h-video .v-views {padding-top: 0;}


</style>
<div class="ads"> <div data-ad-block data-ad-type='ad' data-ad-mode='image' data-ad-size='1080x1080'></div></div>
 

<!-- up next -->
                        <a href="{{ url("artist/$slug") }}" style="clear:both;overflow:hidden;display:block;width:100%;border-bottom:1px solid #eaeaea;padding: 8px 10px 6px 11px;margin-bottom: 10px;background:#0abb87 !important;border-radius: 0;">
                            @if( $video->profile_photo )
                                <img src="{{ $video->profile_photo }}" style="width: 55px;margin-top: 10px;float: left;border-radius:100px;height: 55px;object-fit: cover;">
                            @else
                                <span class="shortname" style="display: inline-block">{{ name_to_pic($video->profile_name) }}</span>
                            @endif
                            <span id="new_block--0991"
                                style="margin-left:15px;float: right;line-height: 16px;color: #fff;display:block;margin-top: 7px;font-size: 15px;width: 80%;margin-bottom: 4px;">
                                <small style="font-size: 15px;font-weight: bold;"> {{ $video->profile_name }} - {{ $video->state }}</small>

                                <small style="display:block;font-size: 12px;color: #fff;margin-top: 0px;">Category: {{ $video->multiple_category }}</small>
                                <small class="p0" style="display:block;font-size: 11px;color: #fff;margin-top: 4px;height: 25px;overflow: hidden;width: 100%;line-height: 13px;">{{ $video->about_me }}</small>
                            </span>
                        </a>

                        <div class="list">
                            @if( $videos && $videos->count() > 0 )
                                @foreach( $videos as $v )
                                    <div class="h-video row">
                                        <div class="col-lg-5 col-sm-5">
                                            <div class="v-img">
                                                <a href="{{ url("$slug/video/$v->slug") }}">
                                                    <img src="{{ $v->thumb }}" alt="{{ $v->video_title }}" />
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-sm-7 pad-01">
                                            <div class="v-desc">
                                                <a href="{{ url("$slug/video/$v->slug") }}">{{ $v->video_title }}</a>
                                            </div>
                                            <div class="v-views">
                                                Category: {{ $v->category }}
                                            </div>
                                            <p>{{ $v->description }}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--artist video display end-->

    <div id="ml09198" style="background:#eaeaea !important">


        <div class="container" id="sm71" style="position:relative">
            <h2 class="middle-head">Find Latest Artist videos</h2>
            <a href="./artist-videos"
                style="position:absolute;right: 2px;top: 22px;color:#fff;padding:4px 15px 0 15px;background: #f72e5e !important;">View
                all</a> <!-- casting video start-->
            <div class="row home_page_second_block" style=" padding: 20px 0 15px 0;t">
                 
                    <div class="cb-content videolist" id="home_page_video" >
                        @if( $latestVideos && $latestVideos->count() > 0 )
                            @foreach( $latestVideos as $v )
                                <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                        <a href="{{ url("artist/".$v->aboutAuthor->username) }}">
                                            @if( $v->aboutAuthor->profile_bkg )
                                                <img src="{{ $v->aboutAuthor->profile_bkg }}" alt="">
                                            @else
                                                <span class="shortname" style="display: inline-block">{{ name_to_pic($v->aboutAuthor->profile_name) }}</span>
                                            @endif
                                        </a>
                                    </div>
                                    <div class="b-video">
                                        <div class="v-img">
                                            <a href="{{ url($v->aboutAuthor->username."/video/$v->slug") }}">
                                                <img src="{{ $v->thumb }}" />
                                            </a>
                                        </div>
                                        <div class="v-desc">
                                            <p>{{ $v->category }}</p>
                                            <a href="{{ url($v->aboutAuthor->username."/video/$v->slug") }}">{{ explode(",", $v->aboutAuthor->multiple_category)[0] }}</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                
                <!-- /Featured Videos -->
            </div>
        </div>
  <div class="container" id="sm71" style="position:relative">
            <h2 class="middle-head">Find Latest Artist Photos</h2>
            <a href="{{URL::to('artist-gallery')}}"
                style="position:absolute;right: 17px;top: 22px;color:#fff;padding:4px 15px 0 15px;background: #f72e5e !important;">View
                all</a> <!-- casting video start-->
            <div class="row home_page_second_block" style="">
                <div class="col-lg-12">

                    <div class="cb-content videolist" id="home_page_video"  >
                        @if( $latestPhotos && $latestPhotos->count() > 0 )
                            @foreach( $latestPhotos as $v )
                                <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                        <a href="{{ url("artist/".$v->aboutAuthor->username) }}">
                                            @if( $v->aboutAuthor->profile_bkg )
                                                <img src="{{ $v->aboutAuthor->profile_bkg }}" alt=""  />
                                            @else
                                                <span class="shortname" style="display: inline-block; width: 30px; height: 30px; line-height: 1.9">{{ name_to_pic($v->aboutAuthor->profile_name) }}</span>
                                            @endif
                                        </a>
                                    </div>
                                    <div class="b-video">
                                        <div class="v-img">
                                            <a href="{{ url($v->aboutAuthor->username."/gallery/$v->slug") }}">
                                                <img src="{{ $v->photo }}" />
                                            </a>
                                        </div>
                                        <div class="v-desc">
                                            <p>{{ $v->category }}</p>
                                            <a href="{{ url($v->aboutAuthor->username."/gallery/$v->slug") }}">{{ explode(",", $v->aboutAuthor->multiple_category)[0] }}</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <!-- /Featured Videos -->
            </div>
        </div>
    </div>
    <!---similar-->
    <section class="portfolio section">
        <div class="container">
            <div class="col-12 filters-content" id="result-block-main">
                <div class="row" style="padding: 15px 20px;">
                    <div class="col-lg-12">
                        <ul class="list-inline" style="margin-top: 15px;">

                            <li><a href="#">Similar Category Profile</a></li>

                        </ul>
                    </div>
                </div>
                <div class="grid">
                    @if( $similarCategoryProfiles && $similarCategoryProfiles->count() > 0 )
                        @foreach( $similarCategoryProfiles as $artist )
                            <div class="col-sm-3 photo-block">
                                <a href="{{ url("artist/".$artist->user->slug) }}" class="profile-p">
                                    @if( $artist->profile_bkg )
                                        <img src="{{ $artist->profile_bkg }}" height="150">
                                    @else
                                        <span class="shortname" style="display: inline-block">{{ name_to_pic($artist->profile_name) }}</span>
                                    @endif
                                </a>
                                <div class="info-p p-inner">
                                    <h5>{{ $artist->profile_name }}</h5>
                                    <div class="cat">{{ $artist->city." ".$artist->state." ".$artist->country }}<br>
                                        Age: {{ $artist->age }}<br>
                                        {{ $artist->language }}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <div class="clear"></div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    <script>
        @php
            $isFollowing = $video->user->isFollowedBy() ? true : false;
        @endphp
        var isFollowing = "{{ $isFollowing }}";
        var followCount = '<?php echo DB::table('followers')->where('follows_id', '=', $video->user->id)->count(); ?>';
        follow_unfollow(isFollowing, followCount);
        
        $("#followunfollowbtn").on('submit','#follow, #unfollow',function(e){
            e.preventDefault();
             if(userID){
                var form = $(this);
                var url = form.attr('action');
        
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(),
                    success: function(data)
                    {
                       if(!data.status){
                            follow_unfollow(data.isFollowing, data.followCount);
                            alert(data.message);
                        } else {
                            alert(data.message);
                        }
                    }
                });
                $(this)[0].reset();
            } else {  
                window.location.replace(APP_URL+'/login');
            }
        });
        
        function follow_unfollow(isFollowing, followCount){        
            var csrf_field = '<?php echo csrf_field(); ?>';
            var method_field = '<?php echo method_field("DELETE"); ?>';
            var videoData_User_ID = '<?php echo $video->user->id; ?>';
            var unfollowformAction = '<?php echo route("unfollow", ["id" => $video->user->id]);?>';
            var followformAction = '<?php echo route("follow", ["id" => $video->user->id]);?>';
        
            if(isFollowing){
                var unfollowbtn = '<form id="unfollow" action="'+unfollowformAction+'" method="POST">'
                            +csrf_field+method_field+'<button type="submit" id="delete-follow-'
                            +videoData_User_ID+'" class="c-f">Unfollow</button><div class="c-s">'
                            +followCount+'</div></form><div class="clearfix"></div>';
                $('#followunfollowbtn').html('');
                $('#followunfollowbtn').append(unfollowbtn);
            } else {
                var followbtn = '<form id="follow" action="'+followformAction+'" method="POST">'
                +csrf_field+'<button type="submit" id="follow-user-'+videoData_User_ID+'" class="c-f">follow</button><div class="c-s">'+followCount+'</div></form><div class="clearfix"></div>';
                $('#followunfollowbtn').html('');
                $('#followunfollowbtn').append(followbtn);
            }
        }
    </script>
@endsection
