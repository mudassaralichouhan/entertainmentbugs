@extends('layouts.home')
@section('content')
<style>
    #live-video-user-list-home{display:none;}
    #contact h2 {font-size: 22px;margin: 0 0 20px 0;}
    .form-group input, .form-group select {height: 42px;font-size: 13px;border: solid 1px #e0e1e2;}
    #create-profile{margin:40px 0}
    #top-check-field0 h2{font-size:22px; margin:0 0 0 0}
    .option-block{float:left; width:33.3%}
    .field strong{margin-top:10px;display: block;font-weight:600;font-family: 'Hind Guntur', sans-serif;}
    #about label{font-weight:500;font-family: 'Hind Guntur', sans-serif;margin:0px 0 3px 0;}
    #about .form-group {margin-bottom: 18px;}
    #about textarea {padding-top: 10px;font-size: 13px;}
    #achievements-blocks0 label{font-weight:600;font-family: 'Hind Guntur', sans-serif;margin:0px 0 3px 0;}
    #achievements-blocks0  .form-group {margin-bottom: 18px;}
    #achievements-blocks0  textarea {padding-top: 10px;font-size: 13px;}
    #achievements-blocks0 h2{font-size:22px; margin:0 0 20px 0}
    #about h2{font-size:22px; margin:0 0 20px 0}
    #portfolio-block h2{font-size:22px; margin:0 0 20px 0}
    #portfolio-block label {font-weight: 600;font-family: 'Hind Guntur', sans-serif;margin: 0px 0 3px 0;}
    .btn-cv1 {padding: 11px;}
    #what-we-de-bl label{font-weight:600;font-family: 'Hind Guntur', sans-serif;margin:0px 0 3px 0;}
    #what-we-de-bl .form-group {margin-bottom: 18px;}
    #what-we-de-bl  textarea {padding-top: 10px;font-size: 13px;}
    #what-we-de-bl h2{font-size:22px; margin:0 0 20px 0}
    .tab4{background: #eceff0; margin-bottom:0px;clear:both; overflow:hidden;} 
    .tab4 a {padding: 10px 20px;text-align: Center;color: #000;line-height: 45px;}
    .tab4 a.active{    background: #28b47e !important;color:#fff;}
    .w3-green, .w3-hover-green:hover {color: #fff!important;background-color: #f72e5e !important;text-align: Center;line-height: 17px;padding: 3px 0 0 0;}
    .w3-light-grey {color: #000!important;margin-top:8px;background-color: #eceff0 !important;}
    .edit-page .u-area {text-align: center;padding-top: 12px;padding-bottom: 20px;}
    #select-p i{color: #637076;font-size: 36px;text-align: center;display: block;padding: 32px 0 0 0;}
    .upload-page .u-area i {font-size: 90px;color: #637076;}
    .upload-page .u-area .u-text1 {margin-top: 10px;margin-bottom: 9px;font-size: 14px;}
    #portfolio-block .browse-photo{width:100px; height:100px; background:#eceff0}
    #portfolio-block .form-group {margin-bottom: 10px;}
    #portfolio-block .col-lg-6{margin:5px 0 20px 0;}
    #portfolio-block .btn-save{margin-top:40px;}
    #showreel-block h2{font-size:22px; margin:0 0 20px 0}
    #showreel-block label {font-weight: 600;font-family: 'Hind Guntur', sans-serif;margin: 0px 0 3px 0;}
    #portfolio-block .progress {width: 90%;margin-bottom: 0;height: 8px;background-color: #eceff0;
    -webkit-box-shadow: none;box-shadow: none;}
    .progress-bar1, .progress-bar2,.progress-bar3,.progress-bar4,.progress-bar5,.progress-bar6{
    float: left;
    width: 0%;
    height: 100%;
    font-size: 14px;
    line-height: 22px;
    color: #fff;
    text-align: center;
    background-color: #337ab7;
    -webkit-box-shadow: inset 0 -1px 0 rgb(0 0 0 / 15%);
    box-shadow: inset 0 -1px 0 rgb(0 0 0 / 15%);
    -webkit-transition: width .6s ease;
    -o-transition: width .6s ease;
    transition: width .6s ease;
    }
    #portfolio-block .cgt{position: absolute;top: -6px;right: 21px;}
    .u-progress{position:relative; margin-top:10px;}
    .tab {overflow: hidden;border: 1px solid #ccc;background-color: #f1f1f1;}
    .tab button {background-color: inherit;float: left;border: none;outline: none;cursor: pointer;padding: 16px 20px 10px 20px;transition: 0.3s;font-size: 14px;}
    .tab button:hover {background-color: #ddd;}
    .tab button.active {background-color: #ccc;}
    .tabcontent {padding: 6px 12px;border:px solid #ccc;border-top: none;display:none;}
    .tabcontent{margin-top:10px;}
    label {font-weight: 500;}
</style>
<div class="content-wrapper">
    <div class="audition_main_list" id="auditions" style="background:#f4f3f3">
        <div id="create-profile-tab">
            <div class="container">
                <div class="rows" style="background:#fff;padding:0px; margin-top:10px">
                    <div class="tab tab4">
                        <button class="tablinks active" onclick="openCity(event, 'about')">About</button>
                        <button class="tablinks" onclick="openCity(event, 'what-we-de-bl')">What we do</button>
                        <button class="tablinks" onclick="openCity(event, 'achievements-blocks0')">Achiements</button>
                        <button class="tablinks" onclick="openCity(event, 'showreel-block')">Showreel</button>
                        <button class="tablinks" onclick="openCity(event, 'portfolio-block')">Gallery Image</button>
                        <button class="tablinks" onclick="openCity(event, 'video')">Video</button>
                        <button class="tablinks" onclick="openCity(event, 'latest-project')">Latest project</button>
                        <button class="tablinks" onclick="openCity(event, 'contact')">Contact</button>
                        <a href="{{URL::to('artist-profile')}}" style="float: right;width: 150px;margin: 0;padding: 0 10px;background: #28b47e !important;color: #fff;">Preview Profile</a>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="w3-light-grey">
                    <?php
                        $userid=session()->get('user_id');
                        
                        $artist_about=DB::select('select * from artist_about where user_id='.$userid);
                        $artist_what_we_do=DB::select('select * from artist_what_we_do where user_id='.$userid);
                        $artist_achievment=DB::select('select * from artist_achievment where user_id='.$userid);
                        $artist_showreel=DB::select('select * from artist_showreel where user_id='.$userid);
                        $artist_gallery=DB::select('select * from artist_gallery where user_id='.$userid);
                        $artist_video=DB::select('select * from artist_video where user_id='.$userid);
                        $artist_project=DB::select('select * from artist_project where user_id='.$userid);
                        $artist_contact=DB::select('select * from artist_contact where user_id='.$userid);
                        $progress = 0;
                        if( count($artist_about) > 0){
                        $progress += 12.5;
                        }
                        if(count($artist_what_we_do) > 0){
                        $progress += 12.5;
                        }
                        if(count($artist_achievment) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_showreel ) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_gallery ) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_video ) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_project ) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_contact ) >0){
                        $progress += 12.5;
                        }
                        ?>
                    <div class="w3-container w3-green w3-center" style="width: <?php echo $progress; ?>%">
                        <?php echo $progress; ?>%
                    </div>
                </div>
            </div>
            <div id="about"  class="tabcontent" style="display:block; margin-top:10px">
                <div class="container">
                    <div class="rows" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <h2>About <br> <small style="font-size:16px;">Create your profile as a artist</small></h2>
                        <?php 
                            $userId = session()->get('user_id');
                            $getIfAlready = DB::select('select * from artist_about where user_id='.$userId);
                            ?>
                        <form action="{{URL::to('save-artist-profile')}}" id="artistForm" style="margin-top:0px" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="cropImage" id="cropImage" value="">
                            <div class="form-group  col-lg-6 col-sm-6 ">
                                <label for="exampleInputEmail1">Profile Name*</label>
                                <input type="text" required name="profile_name" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->profile_name != NULL) ? $getIfAlready[0]->profile_name : '') : '')); ?>">
                            </div>
                            <div class="form-group  col-lg-6 col-sm-6 ">
                                <label for="exampleInputEmail1">Profile Title (Tag Line)* - 25 words</label>
                                <input type="text" required name="profile_title" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->profile_title != NULL) ? substr($getIfAlready[0]->profile_title,0,25) : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-4 col-sm-6">
                                <label for="exampleInputEmail1">Country* </label>
                                <div class="clear"></div>
                                <select id="country" name="country" required onchange="getStates(this.value)" class="select2 input-select" style="width:100%">
                                    <option value="">Select Country</option>
                                    <?php
                                        $getCountries = DB::table('tbl_countries')->get();
                                        foreach($getCountries as $k=>$v)
                                        {
                                        ?>
                                    <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->country != NULL) ? (($getIfAlready[0]->country == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-sm-6">
                                <label for="exampleInputEmail1">Location (State)*</label>
                                <!--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="">-->
                                <select class="select2 input-select" style="width:100%" onchange="getCities(this.value)" required id="state" name="state">
                                    <option value="">Select State</option>
                                    <?php
                                        if(count($getIfAlready) > 0)
                                        {
                                            if($getIfAlready[0]->country != NULL || $getIfAlready[0]->country != '')
                                            {
                                                $getCountryId = DB::table('tbl_countries')->where('name',$getIfAlready[0]->country)->get();
                                                $getStates = DB::table('tbl_states')->where('country_id',$getCountryId[0]->id)->get();
                                                foreach($getStates as $k=>$v)
                                                {
                                        ?>
                                    <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->state != NULL) ? (($getIfAlready[0]->state == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                                    <?php } ?>
                                    <?php } } ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-4 col-sm-6">
                                <label for="exampleInputEmail1">City*</label>
                                <select class="select2 input-select" style="width:100%" required id="city" name="city">
                                    <option value="">Select City</option>
                                    <?php
                                        if(count($getIfAlready) > 0)
                                        {
                                            if($getIfAlready[0]->state != NULL || $getIfAlready[0]->state != '')
                                            {
                                                $getStateId = DB::table('tbl_states')->where('name',$getIfAlready[0]->state)->get();
                                                $getCities = DB::table('tbl_cities')->where('state_id',$getStateId[0]->id)->get();
                                                foreach($getCities as $k=>$v)
                                                {
                                        ?>
                                    <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->city != NULL) ? (($getIfAlready[0]->city == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                                    <?php } ?>
                                    <?php } } ?>
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-sm-6">
                                <label for="exampleInputEmail1">Gender*</label>
                                <select required style="width:100%;padding:0 10px" name="gender">
                                    <option value="Male" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->gender != NULL) ? (($getIfAlready[0]->gender == 'Male') ? 'selected' : '') : '') : ''); ?>>Male</option>
                                    <option value="Female" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->gender != NULL) ? (($getIfAlready[0]->gender == 'Female') ? 'selected' : '') : '') : ''); ?>>Female</option>
                                    <option value="Kids-Male" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->gender != NULL) ? (($getIfAlready[0]->gender == 'Kids-Male') ? 'selected' : '') : '') : ''); ?>>Kids - Male</option>
                                    <option value="Kids-Female" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->gender != NULL) ? (($getIfAlready[0]->gender == 'Kids-Female') ? 'selected' : '') : '') : ''); ?>>Kids - Female</option>
                                    <option value="Trans-Female" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->gender != NULL) ? (($getIfAlready[0]->gender == 'Trans-Female') ? 'selected' : '') : '') : ''); ?>>Trans Female</option>
                                    <option value="Trans-Male" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->gender != NULL) ? (($getIfAlready[0]->gender == 'Trans-Male') ? 'selected' : '') : '') : ''); ?>>Trans Male</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-sm-6">
                                <label for="exampleInputEmail1">Date Of Birth*</label>
                                <input type="date" class="form-control" name="dob" required id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->dob != NULL) ? $getIfAlready[0]->dob : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-2 col-sm-6">
                                <label for="exampleInputEmail1">Age*</label> 
                                <input type="text" name="age" class="form-control" required id="exampleInputEmail1" required value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->age != NULL) ? $getIfAlready[0]->age : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-2 col-sm-6">
                                <label for="exampleInputEmail1">Experience*</label>
                                <select name="exp" class="input-select" style="width:100%" required style="width:100%;padding:0 10px">
                                    <option value="">Select</option>
                                    <option value="new" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == 'new') ? 'selected' : '') : '') : ''); ?>>New</option>
                                    <option value="1" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '1') ? 'selected' : '') : '') : ''); ?>>1 years</option>
                                    <option value="2" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '2') ? 'selected' : '') : '') : ''); ?>>2 years</option>
                                    <option value="3" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '3') ? 'selected' : '') : '') : ''); ?>>3 years</option>
                                    <option value="4" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '4') ? 'selected' : '') : '') : ''); ?>>4 years</option>
                                    <option value="5" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '5') ? 'selected' : '') : '') : ''); ?>>5 years</option>
                                    <option value="6" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '6') ? 'selected' : '') : '') : ''); ?>>6 years</option>
                                    <option value="7" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '7') ? 'selected' : '') : '') : ''); ?>>7 years</option>
                                    <option value="8" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '8') ? 'selected' : '') : '') : ''); ?>>8 years</option>
                                    <option value="9" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '9') ? 'selected' : '') : '') : ''); ?>>9 years</option>
                                    <option value="10" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '10') ? 'selected' : '') : '') : ''); ?>>10 years</option>
                                    <option value="11" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '11') ? 'selected' : '') : '') : ''); ?>>11 years</option>
                                    <option value="12" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '12') ? 'selected' : '') : '') : ''); ?>>12 years</option>
                                    <option value="13" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '13') ? 'selected' : '') : '') : ''); ?>>13 years</option>
                                    <option value="14" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '14') ? 'selected' : '') : '') : ''); ?>>14 years</option>
                                    <option value="15" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '15') ? 'selected' : '') : '') : ''); ?>>15 years</option>
                                    <option value="16" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '16') ? 'selected' : '') : '') : ''); ?>>16 years</option>
                                    <option value="17" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '17') ? 'selected' : '') : '') : ''); ?>>17 years</option>
                                    <option value="18" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '18') ? 'selected' : '') : '') : ''); ?>>18 years</option>
                                    <option value="19" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '19') ? 'selected' : '') : '') : ''); ?>>19 years</option>
                                    <option value="20" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '20') ? 'selected' : '') : '') : ''); ?>>20 years</option>
                                    <option value="21" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '21') ? 'selected' : '') : '') : ''); ?>>21 years</option>
                                    <option value="22" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '22') ? 'selected' : '') : '') : ''); ?>>22 years</option>
                                    <option value="23" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '23') ? 'selected' : '') : '') : ''); ?>>23 years</option>
                                    <option value="24" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '24') ? 'selected' : '') : '') : ''); ?>>24 years</option>
                                    <option value="25" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '25') ? 'selected' : '') : '') : ''); ?>>25 years</option>
                                    <option value="26" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '26') ? 'selected' : '') : '') : ''); ?>>26 years</option>
                                    <option value="27" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '27') ? 'selected' : '') : '') : ''); ?>>27 years</option>
                                    <option value="28" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '28') ? 'selected' : '') : '') : ''); ?>>28 years</option>
                                    <option value="29" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '29') ? 'selected' : '') : '') : ''); ?>>29 years</option>
                                    <option value="30" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->exp != NULL) ? (($getIfAlready[0]->exp == '30') ? 'selected' : '') : '') : ''); ?>>30 years +</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-sm-6">
                                <label for="exampleInputEmail1">Height* in ft</label>
                                <input type="text" name="height" required class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->height != NULL) ? $getIfAlready[0]->height : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-2 col-sm-6">
                                <label for="exampleInputEmail1">Weightin  kg</label>
                                <input type="text" name="weight" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->weight != NULL) ? $getIfAlready[0]->weight : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-4 col-sm-6">
                                <label> Language* (Multiple) all global language </label>   
                                <input type="text" class="form-control" id ='video-language' name="language" required placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->language != NULL) ? $getIfAlready[0]->language : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="exampleInputEmail1"> About Me* (200 words)</label>
                                <textarea id="w3review" required onKeyPress="return check(event,value)" onInput="checkLength(200,this)" name="about_me" rows="4" cols="60"><?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->about_me != NULL) ? substr($getIfAlready[0]->about_me,0,200) : '') : '')); ?></textarea> 
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <p style="padding:0 15px"> Select upto 5 <strong>multiple options,</strong> which  represents to your profile.</p>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="two fields">
                                <div class="field">
                                    <div class="option-block">
                                        <?php 
                                            $categoryarr = [];
                                            if(count($getIfAlready) > 0)
                                            {
                                                if($getIfAlready[0]->multiple_category != NULL)
                                                {
                                                    $explode = explode(',',$getIfAlready[0]->multiple_category);
                                                    if(count($explode) > 0)
                                                    {
                                                        foreach($explode as $k=>$v)
                                                        array_push($categoryarr,$v);
                                                    }
                                            ?>
                                        <?php    
                                            }
                                            }
                                            ?>
                                        <div class="field "><strong> <input name="artistcheck[]" type="checkbox" value="Theater" <?php echo ((count($categoryarr) > 0) ? ((in_array('Theater',$categoryarr)) ? 'checked' : '') : ''); ?>> Theater </strong> </div>
                                        <div class="field "><strong> <input name="artistcheck[]" type="checkbox" value="Voiceover" <?php echo ((count($categoryarr) > 0) ? ((in_array('Voiceover',$categoryarr)) ? 'checked' : '') : ''); ?>> Voiceover </strong> </div>
                                        <div class="field "><strong> <input name="artistcheck[]" type="checkbox" value="Comedian" <?php echo ((count($categoryarr) > 0) ? ((in_array('Comedian',$categoryarr)) ? 'checked' : '') : ''); ?>> Comedian </strong> </div>
                                        <div class="field "><strong> <input name="artistcheck[]" type="checkbox" value="Photography" <?php echo ((count($categoryarr) > 0) ? ((in_array('Photography',$categoryarr)) ? 'checked' : '') : ''); ?>> Photography </strong> </div>
                                        <div class="field "><strong> <input name="artistcheck[]" type="checkbox" value="Video Grapher" <?php echo ((count($categoryarr) > 0) ? ((in_array('Video Grapher',$categoryarr)) ? 'checked' : '') : ''); ?>> Video Grapher </strong> </div>
                                        <div class="field "><strong>  <input name="artistcheck[]" type="checkbox" value="Makeup Artrist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Makeup Artrist',$categoryarr)) ? 'checked' : '') : ''); ?>> Makeup Artrist </strong> </div>
                                        <div class="field ">
                                            <strong>Actor / Model</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Actor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Actor',$categoryarr)) ? 'checked' : '') : ''); ?>> Actor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Child Actor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Child Actor',$categoryarr)) ? 'checked' : '') : ''); ?>> Child Actor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Comedy Actor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Comedy Actor',$categoryarr)) ? 'checked' : '') : ''); ?>> Comedy Actor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Junior Actor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Junior Actor',$categoryarr)) ? 'checked' : '') : ''); ?>> Junior Actor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Stunt Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Stunt Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Stunt Artist
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Drama Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Drama Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Drama Artist
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Mimicry Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Mimicry Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Mimicry Artist
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Background / Atmosphere" <?php echo ((count($categoryarr) > 0) ? ((in_array('Background / Atmosphere',$categoryarr)) ? 'checked' : '') : ''); ?>> Background / Atmosphere 
                                        </div>
                                        <div class="field ">
                                            <strong>Modeling</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Model" <?php echo ((count($categoryarr) > 0) ? ((in_array('Model',$categoryarr)) ? 'checked' : '') : ''); ?>> Model
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Child Model" <?php echo ((count($categoryarr) > 0) ? ((in_array('Child Model',$categoryarr)) ? 'checked' : '') : ''); ?>> Child Model
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Pro Model" <?php echo ((count($categoryarr) > 0) ? ((in_array('Pro Model',$categoryarr)) ? 'checked' : '') : ''); ?>> Pro Model
                                        </div>
                                        <div class="field ">
                                            <strong> Dancer</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Dancer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Dancer',$categoryarr)) ? 'checked' : '') : ''); ?>> Dancer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Lead Dancer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Lead Dancer',$categoryarr)) ? 'checked' : '') : ''); ?>> Lead Dancer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Background Dancer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Background Dancer',$categoryarr)) ? 'checked' : '') : ''); ?>> Background Dancer
                                        </div>
                                        <div class="field ">
                                            <strong>Editor</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Video Editor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Video Editor',$categoryarr)) ? 'checked' : '') : ''); ?>> Video Editor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Music Editor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Music Editor',$categoryarr)) ? 'checked' : '') : ''); ?>> Music Editor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Audio Editor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Audio Editor',$categoryarr)) ? 'checked' : '') : ''); ?>> Audio Editor
                                        </div>
                                    </div>
                                    <div class="option-block">
                                        <div class="field ">
                                            <strong>Writer</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Dialogue Writer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Dialogue Writer',$categoryarr)) ? 'checked' : '') : ''); ?>> Dialogue Writer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Script Writer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Script Writer',$categoryarr)) ? 'checked' : '') : ''); ?>> Script Writer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Screeplay Writer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Screeplay Writer',$categoryarr)) ? 'checked' : '') : ''); ?>> Screeplay Writer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Story Writer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Story Writer',$categoryarr)) ? 'checked' : '') : ''); ?>> Story Writer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Lyricists" <?php echo ((count($categoryarr) > 0) ? ((in_array('Lyricists',$categoryarr)) ? 'checked' : '') : ''); ?>> Lyricists
                                        </div>
                                        <div class="vspace-small"></div>
                                        <div class="vspace-small"></div>
                                        <div class="vspace-small"></div>
                                        <div class="field ">
                                            <strong>Anchor / Presenter</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Video Jockey (VJ)" <?php echo ((count($categoryarr) > 0) ? ((in_array('Video Jockey (VJ)',$categoryarr)) ? 'checked' : '') : ''); ?>> Video Jockey (VJ)
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Radio Jockey (RJ)" <?php echo ((count($categoryarr) > 0) ? ((in_array('Radio Jockey (RJ)',$categoryarr)) ? 'checked' : '') : ''); ?>> Radio Jockey (RJ)
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="News Reader" <?php echo ((count($categoryarr) > 0) ? ((in_array('News Reader',$categoryarr)) ? 'checked' : '') : ''); ?>> News Reader
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Reporter" <?php echo ((count($categoryarr) > 0) ? ((in_array('Reporter',$categoryarr)) ? 'checked' : '') : ''); ?>> Reporter
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Journalist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Journalist',$categoryarr)) ? 'checked' : '') : ''); ?>> Journalist
                                        </div>
                                        <div class="field">
                                            <div class="ui field ">
                                                <strong>Off screen</strong>
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Dubbing Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Dubbing Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Dubbing Artist
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Voice Over Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Voice Over Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Voice Over Artist
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="ui field ">
                                                <strong style="background: #f72e5e;display: block;color: #fff;padding: 4px 10px 0 8px;margin: 4px 0 0 0;width: 78px;">Influncer</strong>
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="New Influncer" <?php echo ((count($categoryarr) > 0) ? ((in_array('New Influncer',$categoryarr)) ? 'checked' : '') : ''); ?>> New Influncer
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Experienced Influncer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Experienced Influncer',$categoryarr)) ? 'checked' : '') : ''); ?>> Experienced Influncer
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="ui field ">
                                                <strong>Advertising Professional	</strong>
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Video Advertising" <?php echo ((count($categoryarr) > 0) ? ((in_array('Video Advertising',$categoryarr)) ? 'checked' : '') : ''); ?>> Video Advertising
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Audio Advertising" <?php echo ((count($categoryarr) > 0) ? ((in_array('Audio Advertising',$categoryarr)) ? 'checked' : '') : ''); ?>>  Audio Advertising
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Banner Advertising" <?php echo ((count($categoryarr) > 0) ? ((in_array('Banner Advertising',$categoryarr)) ? 'checked' : '') : ''); ?>>  Banner Advertising
                                            </div>
                                            <div class="field ">
                                                <strong>Singer / Musician</strong>
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Singer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Singer',$categoryarr)) ? 'checked' : '') : ''); ?>> Singer
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Music Instrumentalist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Music Instrumentalist',$categoryarr)) ? 'checked' : '') : ''); ?>> Music Instrumentalist
                                            </div>
                                            <div class="field col-pad">
                                                <input name="artistcheck[]" type="checkbox" value="Music Composer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Music Composer',$categoryarr)) ? 'checked' : '') : ''); ?>> Music Composer
                                            </div>
                                        </div>
                                    </div>
                                    <div class="option-block">
                                        <div class="ui field ">
                                            <strong>Proffessional Artist</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Director Of Photography / Cinematographer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Director Of Photography / Cinematographer',$categoryarr)) ? 'checked' : '') : ''); ?>> Director Of Photography / Cinematographer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Cameraman" <?php echo ((count($categoryarr) > 0) ? ((in_array('Cameraman',$categoryarr)) ? 'checked' : '') : ''); ?>> Cameraman
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Camera Operator" <?php echo ((count($categoryarr) > 0) ? ((in_array('Camera Operator',$categoryarr)) ? 'checked' : '') : ''); ?>> Camera Operator
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Light man / Gaffer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Light man / Gaffer',$categoryarr)) ? 'checked' : '') : ''); ?>> Light man / Gaffer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Makeup Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Makeup Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Makeup Artist
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Visual Effects Editor" <?php echo ((count($categoryarr) > 0) ? ((in_array('Visual Effects Editor',$categoryarr)) ? 'checked' : '') : ''); ?>> Visual Effects Editor
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Digital imaging technician" <?php echo ((count($categoryarr) > 0) ? ((in_array('Digital imaging technician',$categoryarr)) ? 'checked' : '') : ''); ?>> Digital imaging technician
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Special Effects makeup Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Special Effects makeup Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> Special Effects makeup Artist
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Motion control technician" <?php echo ((count($categoryarr) > 0) ? ((in_array('Motion control technician',$categoryarr)) ? 'checked' : '') : ''); ?>> Motion control technician
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Fashion Designer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Fashion Designer',$categoryarr)) ? 'checked' : '') : ''); ?>> Fashion Designer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Hair Stylist" <?php echo ((count($categoryarr) > 0) ? ((in_array('Hair Stylist',$categoryarr)) ? 'checked' : '') : ''); ?>> Hair Stylist
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Costume designer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Costume Designer',$categoryarr)) ? 'checked' : '') : ''); ?>> Costume designer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Grip" <?php echo ((count($categoryarr) > 0) ? ((in_array('Grip',$categoryarr)) ? 'checked' : '') : ''); ?>> Grip
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Sound Designer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Sound Designer',$categoryarr)) ? 'checked' : '') : ''); ?>> Sound Designer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Sound Grip" <?php echo ((count($categoryarr) > 0) ? ((in_array('Sound Grip',$categoryarr)) ? 'checked' : '') : ''); ?>> Sound Grip
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Production Sound Mixer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Production Sound Mixer',$categoryarr)) ? 'checked' : '') : ''); ?>> Production Sound Mixer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Production Designer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Production Designer',$categoryarr)) ? 'checked' : '') : ''); ?>> Production Designer
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Green man" <?php echo ((count($categoryarr) > 0) ? ((in_array('Green man',$categoryarr)) ? 'checked' : '') : ''); ?>> Green man
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Property master" <?php echo ((count($categoryarr) > 0) ? ((in_array('Property master',$categoryarr)) ? 'checked' : '') : ''); ?>> Property master
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Weapons master" <?php echo ((count($categoryarr) > 0) ? ((in_array('Weapons master',$categoryarr)) ? 'checked' : '') : ''); ?>> Weapons master
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Set Designer / Decorator" <?php echo ((count($categoryarr) > 0) ? ((in_array('Set Designer / Decorator',$categoryarr)) ? 'checked' : '') : ''); ?>> Set Designer / Decorator
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Location Manager" <?php echo ((count($categoryarr) > 0) ? ((in_array('Location Manger',$categoryarr)) ? 'checked' : '') : ''); ?>> Location Manager
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="StoryBoard Artist" <?php echo ((count($categoryarr) > 0) ? ((in_array('StoryBoard Artist',$categoryarr)) ? 'checked' : '') : ''); ?>> StoryBoard Artist
                                        </div>
                                        <div class="vspace-small"></div>
                                        <div class="vspace-small"></div>
                                        <div class="vspace-small"></div>
                                        <div class="ui field ">
                                            <strong>Others</strong>
                                        </div>
                                        <div class="field col-pad">
                                            <input name="artistcheck[]" type="checkbox" value="Others" <?php echo ((count($categoryarr) > 0) ? ((in_array('Others',$categoryarr)) ? 'checked' : '') : ''); ?>> Others
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <p style="color:red">Select any one the category</p>
                            <div class="clear"></div>
                            <div class="row" style="margin:20px 0 30px 0;padding: 0;">
                                <div class="col-lg-3">
                                    <label for="exampleInputEmail1"> Upload profile photo 400px x 400px  Crop</label>
                                    <input type="file" target="myfile" id="upload_image"  class="image custom-file-input image upload-cover-image" onchange="validateThumbFile(this.files);" accept="image/x-png,image/gif,image/jpeg" />
                                    <input type="hidden" id="myfile" name="myfile">
                                    <?php if(count($getIfAlready) > 0){ 
                                        if($getIfAlready[0]->profile_photo != NULL || $getIfAlready[0]->profile_photo != '')
                                        {
                                        ?>
                                    <img src="<?php echo $getIfAlready[0]->profile_photo ?>" style="width:200px;height:200px;margin-top:15px;">
                                    <?php } } ?>     
                                </div>
                                <div id="profile_photo_preview">
                                    <img style="width:200px;height:200px;margin-top:15px;" alt="previewImage">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlready) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                            </div>
                    </div>
                    <div class="clear"></div>
                    </form>
                </div>
            </div>
        </div>
            <div id="what-we-de-bl" class="tabcontent" style="margin-top:10px">
            <div class="container">
                <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                    <?php 
                        $userId = session()->get('user_id');
                        $getIfAlreadyWhatWedo = DB::select('select * from artist_what_we_do where user_id='.$userId);
                        ?>
                    <form action="{{URL::to('save-what-we-do-profile')}}" id="whatwedoform" style="margin-top:0px" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group achievemets">
                            <h2>What we do</h2>
                            <div class="form-group col-lg-12">
                                <label for="exampleInputEmail1">What we do *</label>
                                <input type="text" name="what_we_do" class="form-control" required id="exampleInputEmail1" placeholder="What we do" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->what_we_do != NULL) ? $getIfAlreadyWhatWedo[0]->what_we_do : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="exampleInputEmail1"> Description (50 words) *</label>
                                <textarea id="w3review" name="description" onKeyPress="return check(event,value)" onInput="checkLength(50,this)" rows="4" cols="50" required><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->description != NULL) ? substr($getIfAlreadyWhatWedo[0]->description,0,50) : '') : '')); ?></textarea> 
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div id="copydiv">
                                <div class="col-lg-3">
                                    <div class="form-group"> <label for="exampleInputEmail1">Title *</label>  
                                        <input type="text" required class="form-control" name="title[]" id="exampleInputEmail1" placeholder="eg: CINEMAS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_1 != NULL) ? $getIfAlreadyWhatWedo[0]->title_1 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group">  
                                        <label for="exampleInputEmail1"> Description (25 words) *</label>
                                        <textarea name="desc[]" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" required rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_1 != NULL) ? substr($getIfAlreadyWhatWedo[0]->des_1,0,25) : '') : '')); ?></textarea> 
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Title *</label>  
                                        <input type="text" required name="title[]" class="form-control" id="exampleInputEmail1" placeholder="eg: PRODUCTION" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_2 != NULL) ? $getIfAlreadyWhatWedo[0]->title_2 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group">  
                                        <label for="exampleInputEmail1"> Description (25 words) *</label>
                                        <textarea id="w3review" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" required name="desc[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_2 != NULL) ? substr($getIfAlreadyWhatWedo[0]->des_2,0,25) : '') : '')); ?></textarea> 
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Title *</label>
                                        <input type="text" required name="title[]" class="form-control" id="exampleInputEmail1" placeholder="eg: THEATRE" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_3 != NULL) ? $getIfAlreadyWhatWedo[0]->title_3 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group">  
                                        <label for="exampleInputEmail1"> Description (25 words) *</label>
                                        <textarea id="w3review" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" required name="desc[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_3 != NULL) ? substr($getIfAlreadyWhatWedo[0]->des_3,0,25) : '') : '')); ?></textarea> 
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Title *</label>  
                                        <input type="text" required name="title[]" class="form-control" id="exampleInputEmail1" placeholder="eg: BLOCK BUSTERS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_4 != NULL) ? $getIfAlreadyWhatWedo[0]->title_4 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"> Description (25 words) *</label>
                                        <textarea id="w3review" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" required name="desc[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_4 != NULL) ? substr($getIfAlreadyWhatWedo[0]->des_4,0,25) : '') : '')); ?></textarea> 
                                    </div>
                                </div>
                            </div>
                            <style>
                                .d-none{
                                display:none;
                                }
                            </style>
                            <a href="javascript:void(0)" id="addMore" onclick="addMoreWhatWeDo(this)" style="background: #f72e5e;color: #fff;padding: 0 10px;" class="<?php if(count($getIfAlreadyWhatWedo) > 0){ if($getIfAlreadyWhatWedo[0]->title_5 != NULL || $getIfAlreadyWhatWedo[0]->title_6 != NULL || $getIfAlreadyWhatWedo[0]->title_7 != NULL || $getIfAlreadyWhatWedo[0]->title_8 != NULL){ echo "d-none"; } }else{ echo "d-none"; } ?>">+ Add More</a>
                            <a href="javascript:void(0)" id="removeMore" onclick="removeWhatWeDo(this)" style="background: #f72e5e;color: #fff;padding: 0 10px;" class="<?php if(count($getIfAlreadyWhatWedo) > 0){ if($getIfAlreadyWhatWedo[0]->title_5 == NULL && $getIfAlreadyWhatWedo[0]->title_6 == NULL && $getIfAlreadyWhatWedo[0]->title_7 == NULL && $getIfAlreadyWhatWedo[0]->title_8 == NULL){ echo "d-none"; } }else{ echo "d-none"; } ?>">- Remove</a>
                            <br> <br>
                            <div id="clonediv" class="<?php if(count($getIfAlreadyWhatWedo) > 0){ if($getIfAlreadyWhatWedo[0]->title_5 == NULL && $getIfAlreadyWhatWedo[0]->title_6 == NULL && $getIfAlreadyWhatWedo[0]->title_7 == NULL && $getIfAlreadyWhatWedo[0]->title_8 == NULL){ echo "d-none"; } } ?>">
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Title</label>  
                                        <input type="text" name="title[]" id="title5" class="form-control" placeholder="eg: BLOCK BUSTERS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_5 != NULL) ? $getIfAlreadyWhatWedo[0]->title_5 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"> Description (25 words)</label>
                                        <textarea id="desc5" name="desc[]" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_5 != NULL) ? substr($getIfAlreadyWhatWedo[0]->des_5,0,25) : '') : '')); ?></textarea> 
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Title</label>  
                                        <input type="text" name="title[]" class="form-control" id="title6" placeholder="eg: BLOCK BUSTERS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_6 != NULL) ? $getIfAlreadyWhatWedo[0]->title_6 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"> Description (25 words)</label>
                                        <textarea id="desc6" name="desc[]" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_6 != NULL) ? substr($getIfAlreadyWhatWedo[0]->des_6,0,25) : '') : '')); ?></textarea> 
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Title</label>  
                                        <input type="text" name="title[]" class="form-control" id="title7" placeholder="eg: BLOCK BUSTERS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_7 != NULL) ? $getIfAlreadyWhatWedo[0]->title_7 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"> Description (25 words)</label>
                                        <textarea id="desc7" name="desc[]" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_7 != NULL) ? substr($getIfAlreadyWhatWedo[0]->des_7,0,25) : '') : '')); ?></textarea> 
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Title</label>  
                                        <input type="text" name="title[]" class="form-control" id="title8" placeholder="eg: BLOCK BUSTERS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_8 != NULL) ? $getIfAlreadyWhatWedo[0]->title_8 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"> Description (25 words)</label>
                                        <textarea id="desc8" name="desc[]" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_8 != NULL) ? substr($getIfAlreadyWhatWedo[0]->des_8,0,25) : '') : '')); ?></textarea> 
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            Note: Minimum 4 you have to insert and max 8
                        </div>
                        <div style="clear:both; overflow:hidden"></div>
                        <div class="row">
                            <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyWhatWedo) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
            <div id="achievements-blocks0" class="tabcontent" style="margin-top:10px">
                <div class="container">
                    <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <div class="form-group achievemets">
                            <?php 
                                $userId = session()->get('user_id');
                                $getIfAlreadyAchivement = DB::select('select * from artist_achievment where user_id='.$userId);
                            ?>
                            <form action="{{URL::to('save-achivement')}}" id="achivementForm" style="margin-top:0px" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <h2>Achievemets</h2>
                                <p>You can add 5 Achievements</p>
                                <div class="form-group col-lg-6">
                                    <label for="exampleInputEmail1">Title</label>
                                    <input type="text" name="titleachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->title_1 != NULL) ? $getIfAlreadyAchivement[0]->title_1 : '') : '')); ?>">  
                                </div>
                                <input type="hidden" name="id[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->id != NULL) ? $getIfAlreadyAchivement[0]->id : '') : '')); ?>">  
                                <input type="hidden" name="oldFile[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->photo_1 != NULL) ? $getIfAlreadyAchivement[0]->photo_1 : '') : '')); ?>">  
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Link - If any</label>
                                    <input type="text" name="linkachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->link_1 != NULL) ? $getIfAlreadyAchivement[0]->link_1 : '') : '')); ?>">  
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Date</label>
                                    <input type="date" name="date[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->date_1 != NULL) ? date('Y-m-d',strtotime($getIfAlreadyAchivement[0]->date_1)) : '') : '')); ?>">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="exampleInputEmail1"> Description (200 words)</label>
                                    <textarea id="w3review" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" name="descachieve[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyAchivement) >= 1 ? (( $getIfAlreadyAchivement[0]->des_1 != NULL) ? substr($getIfAlreadyAchivement[0]->des_1,0,200) : '') : '')); ?></textarea> 
                                </div>
                                <div class="clear"></div>
                                <a href="javascript:void(0)" onclick="showAchivementUploader('1')" style="background: #637076;color: #fff;width:195px;margin:10px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                                Browse Achievement  Photo
                                </a>  
                                <input type="file" style="display:none;" name="fileachive[]" id="fileachive1" onchange="showFileLabel('1')">
                                <br>
                                <p id="fileachivelabel1" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                                <div style="clear:both; overflow:hidden"></div>
                                <Br>
                                <div class="u-close" style="float:left; margin-right:20px;">
                                    <img src="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->photo_1 != NULL || $getIfAlreadyAchivement[0]->photo_1 != '') ? $getIfAlreadyAchivement[0]->photo_1 : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                    <?php 
                                        if(count($getIfAlreadyAchivement) >= 1)
                                        {
                                            if($getIfAlreadyAchivement[0]->photo_1 != NULL || $getIfAlreadyAchivement[0]->photo_1 != '')
                                            {
                                        ?>
                                    <a href="<?php echo URL::to('/delete-achivement-image/'.$getIfAlreadyAchivement[0]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                    <i class="cvicon-cv-cancel"></i>
                                    </a>
                                    <?php } } ?>
                                </div>
                                <br> Image size 300 x 300  <br>  <br>
                                <div style="clear:both; overflow:hidden"></div>
                                <Br>   
                                <div class="form-group col-lg-6">
                                    <label for="exampleInputEmail1">Title</label>
                                    <input type="text" name="titleachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->title_1 != NULL) ? $getIfAlreadyAchivement[1]->title_1 : '') : '')); ?>">  
                                </div>
                                <input type="hidden" name="id[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->id != NULL) ? $getIfAlreadyAchivement[1]->id : '') : '')); ?>">  
                                <input type="hidden" name="oldFile[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->photo_1 != NULL) ? $getIfAlreadyAchivement[1]->photo_1 : '') : '')); ?>">  
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Link - If any</label>
                                    <input type="text" name="linkachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->link_1 != NULL) ? $getIfAlreadyAchivement[1]->link_1 : '') : '')); ?>">  
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Date</label>
                                    <input type="date" name="date[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->date_1 != NULL) ? date('Y-m-d',strtotime($getIfAlreadyAchivement[1]->date_1)) : '') : '')); ?>">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="exampleInputEmail1"> Description (200 words)</label>
                                    <textarea id="w3review" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" name="descachieve[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->des_1 != NULL) ? substr($getIfAlreadyAchivement[1]->des_1,0,200) : '') : '')); ?></textarea> 
                                </div>
                                <div class="clear"></div>
                                <a href="javascript:void(0)"  onclick="showAchivementUploader('2')" style="background: #637076;color: #fff;width:195px;margin:10px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                                Browse Achievement  Photo
                                </a>
                                <input type="file" style="display:none;" name="fileachive[]" id="fileachive2" onchange="showFileLabel('2')">
                                <br>
                                <p id="fileachivelabel2" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                                <div style="clear:both; overflow:hidden"></div>
                                <Br>
                                <div class="u-close" style="float:left; margin-right:20px;">
                                    <img src="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->photo_1 != NULL || $getIfAlreadyAchivement[1]->photo_1 != '') ? $getIfAlreadyAchivement[1]->photo_1 : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                    <?php 
                                        if(count($getIfAlreadyAchivement) >= 2)
                                        {
                                            if($getIfAlreadyAchivement[1]->photo_1 != NULL || $getIfAlreadyAchivement[1]->photo_1 != '')
                                            {
                                        ?>
                                    <a href="<?php echo URL::to('/delete-achivement-image/'.$getIfAlreadyAchivement[1]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                    <i class="cvicon-cv-cancel"></i>
                                    </a>
                                    <?php } } ?>
                                </div>
                                <div class="clear"></div>
                                <div style="clear:both; overflow:hidden"></div>
                                <Br>   
                                <div class="clear"></div>
                                <a href="javascript:void(0)" id="addMoreAchivement" onclick="addMoreAchivement(this)" style="background: #f72e5e;color: #fff;padding: 0 10px;" class="<?php if(count($getIfAlreadyAchivement) >= 3){ echo "d-none"; }else{ } ?>">+ Add More</a>
                                <a href="javascript:void(0)" id="removeMoreAchivement" onclick="removeMoreAchivement(this)" style="background: #f72e5e;color: #fff;padding: 0 10px;" class="<?php if(count($getIfAlreadyAchivement) <= 2){ echo "d-none"; } ?>">- Remove</a>
                                <div style="clear:both; overflow:hidden"></div>
                                <Br>   
                                <div class="clear"></div>
                                <div id="cloneachivement" class="<?php if(count($getIfAlreadyAchivement) <= 2){ echo "d-none"; } ?>">
                                    <div class="form-group col-lg-6">
                                        <label for="exampleInputEmail1">Title</label>
                                        <input type="text" name="titleachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->title_1 != NULL) ? $getIfAlreadyAchivement[2]->title_1 : '') : '')); ?>">  
                                    </div>
                                    <input type="hidden" name="id[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->id != NULL) ? $getIfAlreadyAchivement[2]->id : '') : '')); ?>">  
                                    <input type="hidden" name="oldFile[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->photo_1 != NULL) ? $getIfAlreadyAchivement[2]->photo_1 : '') : '')); ?>">  
                                    <div class="form-group col-lg-3">
                                        <label for="exampleInputEmail1">Link - If any</label>
                                        <input type="text" name="linkachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->link_1 != NULL) ? $getIfAlreadyAchivement[2]->link_1 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="exampleInputEmail1">Date</label>
                                        <input type="date" name="date[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->date_1 != NULL) ? date('Y-m-d',strtotime($getIfAlreadyAchivement[2]->date_1)) : '') : '')); ?>">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <label for="exampleInputEmail1"> Description (200 words)</label>
                                        <textarea id="w3review" name="descachieve[]" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" rows="4" cols="50"><?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->des_1 != NULL) ? substr($getIfAlreadyAchivement[2]->des_1,0,200) : '') : '')); ?></textarea> 
                                    </div>
                                    <div class="clear"></div>
                                    <br>
                                    <a href="javascript:void(0)"  onclick="showAchivementUploader('3')" style="background: #637076;color: #fff;width:195px;margin:10px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                                    Browse Achievement  Photo
                                    </a>
                                    <input type="file" style="display:none;" name="fileachive[]" id="fileachive3" onchange="showFileLabel('3')">
                                    <br>
                                    <p id="fileachivelabel3" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                                    <div style="clear:both; overflow:hidden"></div>
                                    <Br>
                                    <div class="u-close" style="float:left; margin-right:20px;">
                                        <img src="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->photo_1 != NULL || $getIfAlreadyAchivement[2]->photo_1 != '') ? $getIfAlreadyAchivement[2]->photo_1 : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                        <?php 
                                            if(count($getIfAlreadyAchivement) >= 3)
                                            {
                                                if($getIfAlreadyAchivement[2]->photo_1 != NULL || $getIfAlreadyAchivement[2]->photo_1 != '')
                                                {
                                            ?>
                                        <a href="<?php echo URL::to('/delete-achivement-image/'.$getIfAlreadyAchivement[2]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                        <i class="cvicon-cv-cancel"></i>
                                        </a>
                                        <?php } } ?>
                                    </div>
                                    <br> Image size 300 x 300  <br>  <br>
                                    <div style="clear:both; overflow:hidden"></div>
                                    <Br>   
                                    <div class="form-group col-lg-6">
                                        <label for="exampleInputEmail1">Title</label>
                                        <input type="text" name="titleachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 4 ? (($getIfAlreadyAchivement[3]->title_1 != NULL) ? $getIfAlreadyAchivement[3]->title_1 : '') : '')); ?>">  
                                    </div>
                                    <input type="hidden" name="id[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 4 ? (($getIfAlreadyAchivement[3]->id != NULL) ? $getIfAlreadyAchivement[3]->id : '') : '')); ?>">  
                                    <input type="hidden" name="oldFile[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 4 ? (($getIfAlreadyAchivement[3]->photo_1 != NULL) ? $getIfAlreadyAchivement[3]->photo_1 : '') : '')); ?>">  
                                    <div class="form-group col-lg-3">
                                        <label for="exampleInputEmail1">Link - If any</label>
                                        <input type="text" name="linkachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 4 ? (($getIfAlreadyAchivement[3]->link_1 != NULL) ? $getIfAlreadyAchivement[3]->link_1 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="exampleInputEmail1">Date</label>
                                        <input type="date" name="date[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 4 ? (($getIfAlreadyAchivement[3]->date_1 != NULL) ? date('Y-m-d',strtotime($getIfAlreadyAchivement[3]->date_1)) : '') : '')); ?>">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <label for="exampleInputEmail1"> Description (200 words)</label>
                                        <textarea id="w3review" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" name="descachieve[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyAchivement) >= 4 ? (($getIfAlreadyAchivement[3]->des_1 != NULL) ? substr($getIfAlreadyAchivement[3]->des_1,0,200) : '') : '')); ?></textarea> 
                                    </div>
                                    <div class="clear"></div>
                                    <br>
                                    <a href="javascript:void(0)"  onclick="showAchivementUploader('4')" style="background: #637076;color: #fff;width:195px;margin:10px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                                    Browse Achievement  Photo
                                    </a>
                                    <input type="file" style="display:none;" name="fileachive[]" id="fileachive4" onchange="showFileLabel('4')">
                                    <br>
                                    <p id="fileachivelabel4" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                                    <div style="clear:both; overflow:hidden"></div>
                                    <Br>  
                                    <div class="u-close" style="float:left; margin-right:20px;">
                                        <img src="<?php echo ((count($getIfAlreadyAchivement) >= 4 ? (($getIfAlreadyAchivement[3]->photo_1 != NULL || $getIfAlreadyAchivement[3]->photo_1 != '') ? $getIfAlreadyAchivement[3]->photo_1 : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                        <?php 
                                            if(count($getIfAlreadyAchivement) >= 4)
                                            {
                                                if($getIfAlreadyAchivement[3]->photo_1 != NULL || $getIfAlreadyAchivement[3]->photo_1 != '')
                                                {
                                            ?>
                                        <a href="<?php echo URL::to('/delete-achivement-image/'.$getIfAlreadyAchivement[3]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                        <i class="cvicon-cv-cancel"></i>
                                        </a>
                                        <?php } } ?>
                                    </div>
                                    <br> Image size 300 x 300  <br>  <br>
                                    <div style="clear:both; overflow:hidden"></div>
                                    <Br>   
                                    <div class="form-group col-lg-6">
                                        <label for="exampleInputEmail1">Title</label>
                                        <input type="text" name="titleachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 5 ? (($getIfAlreadyAchivement[4]->title_1 != NULL) ? $getIfAlreadyAchivement[4]->title_1 : '') : '')); ?>">  
                                    </div>
                                    <input type="hidden" name="id[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 5 ? (($getIfAlreadyAchivement[4]->id != NULL) ? $getIfAlreadyAchivement[4]->id : '') : '')); ?>">  
                                    <input type="hidden" name="oldFile[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 5 ? (($getIfAlreadyAchivement[4]->photo_1 != NULL) ? $getIfAlreadyAchivement[4]->photo_1 : '') : '')); ?>">  
                                    <div class="form-group col-lg-3">
                                        <label for="exampleInputEmail1">Link - If any</label>
                                        <input type="text" name="linkachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 5 ? (($getIfAlreadyAchivement[4]->link_1 != NULL) ? $getIfAlreadyAchivement[4]->link_1 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="exampleInputEmail1">Date</label>
                                        <input type="date" name="date[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 5 ? (($getIfAlreadyAchivement[4]->date_1 != NULL) ? date('Y-m-d',strtotime($getIfAlreadyAchivement[4]->date_1)) : '') : '')); ?>">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <label for="exampleInputEmail1"> Description (200 words)</label>
                                        <textarea id="w3review" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" name="descachieve[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyAchivement) >= 5 ? (($getIfAlreadyAchivement[4]->des_1 != NULL) ? substr($getIfAlreadyAchivement[4]->des_1,0,200) : '') : '')); ?></textarea> 
                                    </div>
                                    <div class="clear"></div>
                                    <br>
                                    <a href="javascript:void(0)"  onclick="showAchivementUploader('5')" style="background: #637076;color: #fff;width:195px;margin:10px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                                    Browse Achievement  Photo
                                    </a>
                                    <input type="file" style="display:none;" name="fileachive[]" id="fileachive5" onchange="showFileLabel('5')">
                                    <br>
                                    <p id="fileachivelabel5" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                                    <div style="clear:both; overflow:hidden"></div>
                                    <Br>  
                                    <div class="u-close" style="float:left; margin-right:20px;">
                                        <img src="<?php echo ((count($getIfAlreadyAchivement) >= 5 ? (($getIfAlreadyAchivement[4]->photo_1 != NULL || $getIfAlreadyAchivement[4]->photo_1 != '') ? $getIfAlreadyAchivement[4]->photo_1 : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                        <?php 
                                            if(count($getIfAlreadyAchivement) >= 5)
                                            {
                                                if($getIfAlreadyAchivement[4]->photo_1 != NULL || $getIfAlreadyAchivement[4]->photo_1 != '')
                                                {
                                            ?>
                                        <a href="<?php echo URL::to('/delete-achivement-image/'.$getIfAlreadyAchivement[4]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                        <i class="cvicon-cv-cancel"></i>
                                        </a>
                                        <?php } } ?>
                                    </div>
                                    <br> Image size 300 x 300  <br>  <br>
                                    <div style="clear:both; overflow:hidden"></div>
                                    <Br>   
                                </div>
                                <div style="clear:both; overflow:hidden"></div>
                                <div class="row">
                                    <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyAchivement) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .d-none{
            display:none!important;
            }
        </style>
        <div id="showreel-block" class="tabcontent"  style="margin-top:10px">
            <div class="container">
                <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                    <h2>Showreel</h2>
                    <?php 
                        $userId = session()->get('user_id');
                        $getIfAlreadyReel = DB::select('select * from artist_showreel where user_id='.$userId);
                        ?>
                    <form action="{{URL::to('/save-reel')}}" method="post" enctype="multipart/form-data" id="reelform">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group col-lg-12">
                            <label for="exampleInputEmail1">Description 50 words</label>
                            <textarea id="w3review" name="description" onKeyPress="return check(event,value)" onInput="checkLength(50,this)" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyReel) > 0 ? (($getIfAlreadyReel[0]->description != NULL) ? substr($getIfAlreadyReel[0]->description,0,50) : '') : '')); ?></textarea>
                        </div>
                        <div class="row" style="margin-top:20px;margin-left:20px;">
                            <input type="file" class="d-none" id="reelvideo" accept="video/*">
                            <a href="javascript:void(0)" onclick="openReelVideo()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Upload Video</a>
                            <br>
                            <p id="reelvideolabel" class="text-success" style="color:green;font-weight:bold;margin-left:15px;margin-top:15px;"></p>
                            <div style="clear:both; overflow:hidden"></div>
                            <Br>
                            <div class="u-details" style="display:none;">
                                <div class="row">
                                    <div class="col-lg-12 ud-caption">Upload Details</div>
                                    <div class="col-lg-10">
                                        <div class="u-title"></div>
                                        <div id="upload-progress">
                                            <div class="u-size"></div>
                                            <div class="u-progress">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                        <span class="sr-only">0% Complete</span>
                                                    </div>
                                                </div>
                                                <div class="u-close">
                                                    <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="u-desc"><span class="percent-process1"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="video" id="videouploaded">
                            <input type="file" class="d-none" data-target="video_thumb" target="video_thumb" id="upload_image1"  class="image custom-file-input image upload-cover-image" onchange="validateThumbFile(this.files);" accept="image/x-png,image/gif,image/jpeg" />
                            <input type="hidden" id="generated_video_thumb" name="generated_video_thumb">  
                            <input type="hidden" id="video_thumb" name="video_thumb">  
                            <br>
                            <a href="javascript:void(0)" onclick="openReelThumb()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Browse Thumbnail</a>
                            <br>
                            <p id="reelvideothumblabel" class="text-success" style="color:green;font-weight:bold;margin-left:20px;margin-top:15px;"></p>
                            <div style="clear:both; overflow:hidden"></div>
                            <Br>
                            Image size ( width:665px height:372px;) 
                            <div class="u-close" style="margin-right:20px;margin-top:20px;">
                                <img id="reelThumbGenerated" src="<?php echo ((count($getIfAlreadyReel) > 0 ? (($getIfAlreadyReel[0]->video_thumb != NULL || $getIfAlreadyReel[0]->video_thumb != '') ? $getIfAlreadyReel[0]->video_thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:200px;height:200px;object-fit:cover;margin-right:2px;float:left;"><?php 
                                    if(count($getIfAlreadyReel) > 0)
                                    {
                                        if($getIfAlreadyReel[0]->video_thumb != NULL || $getIfAlreadyReel[0]->video_thumb != '')
                                        {
                                    ?>
                                <a href="<?php echo URL::to('/delete-reel-image/'.$getIfAlreadyReel[0]->id) ?>" style="margin-top: 2px;display: block;float: left;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div id="profile_photo_preview">
                                <img style="width:200px;height:200px;margin-left:15px;" alt="previewImage">
                            </div>
                        </div>
                        <div class="row" style="margin-top:20px;">
                            <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyReel) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="portfolio-block" class="tabcontent" style="margin-top:10px">
            <div class="container">
                <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                    <h2>Gallery</h2>
                    <p>You can add 12 Photos</p>
                    Image size 600 x 600
                    <div class="col-lg-12">
                        <div class="u-form">
                            <?php 
                                $userId = session()->get('user_id');
                                $getIfAlreadyGallery = DB::select('select * from artist_gallery where user_id='.$userId);
                                ?>
                            <form action="{{URL::to('/save-gallery')}}" method="post" enctype="multipart/form-data" id="contactform">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 1 ? (($getIfAlreadyGallery[0]->id != NULL) ? $getIfAlreadyGallery[0]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 1 ? (($getIfAlreadyGallery[0]->photo != NULL) ? $getIfAlreadyGallery[0]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 2 ? (($getIfAlreadyGallery[1]->id != NULL) ? $getIfAlreadyGallery[1]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 2 ? (($getIfAlreadyGallery[1]->photo != NULL) ? $getIfAlreadyGallery[1]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[2]->id != NULL) ? $getIfAlreadyGallery[2]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[2]->photo != NULL) ? $getIfAlreadyGallery[2]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 4 ? (($getIfAlreadyGallery[3]->id != NULL) ? $getIfAlreadyGallery[3]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 4 ? (($getIfAlreadyGallery[3]->photo != NULL) ? $getIfAlreadyGallery[3]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 5 ? (($getIfAlreadyGallery[4]->id != NULL) ? $getIfAlreadyGallery[4]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 5 ? (($getIfAlreadyGallery[4]->photo != NULL) ? $getIfAlreadyGallery[4]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 6 ? (($getIfAlreadyGallery[5]->id != NULL) ? $getIfAlreadyGallery[5]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 6 ? (($getIfAlreadyGallery[5]->photo != NULL) ? $getIfAlreadyGallery[5]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 7 ? (($getIfAlreadyGallery[6]->id != NULL) ? $getIfAlreadyGallery[6]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 7 ? (($getIfAlreadyGallery[6]->photo != NULL) ? $getIfAlreadyGallery[6]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 8 ? (($getIfAlreadyGallery[7]->id != NULL) ? $getIfAlreadyGallery[7]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 8 ? (($getIfAlreadyGallery[7]->photo != NULL) ? $getIfAlreadyGallery[7]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 9 ? (($getIfAlreadyGallery[8]->id != NULL) ? $getIfAlreadyGallery[8]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 9 ? (($getIfAlreadyGallery[8]->photo != NULL) ? $getIfAlreadyGallery[8]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 10 ? (($getIfAlreadyGallery[9]->id != NULL) ? $getIfAlreadyGallery[9]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 10 ? (($getIfAlreadyGallery[9]->photo != NULL) ? $getIfAlreadyGallery[9]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 11 ? (($getIfAlreadyGallery[10]->id != NULL) ? $getIfAlreadyGallery[10]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 11 ? (($getIfAlreadyGallery[10]->photo != NULL) ? $getIfAlreadyGallery[10]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 12 ? (($getIfAlreadyGallery[11]->id != NULL) ? $getIfAlreadyGallery[11]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 12 ? (($getIfAlreadyGallery[11]->photo != NULL) ? $getIfAlreadyGallery[11]->photo : '') : '')); ?>" />
                                <div class="row" id="portfolio1">
                                    <div class="col-lg-6">
                                        <div class="form-group col-lg-6" style="padding-left:0px">
                                            <label for="e1">Photo Title 1 - (3 words)</label>
                                            <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 1 ? (($getIfAlreadyGallery[0]->photo_title != NULL) ? substr($getIfAlreadyGallery[0]->photo_title,0,3) : '') : '')); ?>">  
                                        </div>
                                        <div class="form-group col-lg-6"  style="padding:0px">
                                            <label for="e1">Select category</label>
                                            <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Select Category</strong>
                                                </option>
                                                <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Threater</strong>
                                                </option>
                                                <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Voiceover</strong>
                                                </option>
                                                <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Comedian</strong>
                                                </option>
                                                <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Photography</strong>
                                                </option>
                                                <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Grapher</strong>
                                                </option>
                                                <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Makeup Artrist</strong>
                                                </option>
                                                <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Actor</strong>
                                                </option>
                                                <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Child Actor</strong>
                                                </option>
                                                <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Comedy Actor</strong>
                                                </option>
                                                <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Junior Actor</strong>
                                                </option>
                                                <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Stunt Artist</strong>
                                                </option>
                                                <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Drama Artist</strong>
                                                </option>
                                                <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Mimicry Artist</strong>
                                                </option>
                                                <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Background / Atmosphere</strong>
                                                </option>
                                                <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Model</strong>
                                                </option>
                                                <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Child Model</strong>
                                                </option>
                                                <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Pro Model</strong>
                                                </option>
                                                <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dancer</strong>
                                                </option>
                                                <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Lead Dancer</strong>
                                                </option>
                                                <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Background Dancer</strong>
                                                </option>
                                                <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Editor</strong>
                                                </option>
                                                <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Editor</strong>
                                                </option>
                                                <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Audio Editor</strong>
                                                </option>
                                                <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dialogue Writer</strong>
                                                </option>
                                                <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Script Writer</strong>
                                                </option>
                                                <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Screeplay Writer</strong>
                                                </option>
                                                <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Story Writer</strong>
                                                </option>
                                                <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Lyricists</strong>
                                                </option>
                                                //video vj
                                                <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Video Jockey (VJ)</strong>
                                                </option>
                                                <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Radio Jockey (RJ)</strong>
                                                </option>
                                                <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> News Reader</strong>
                                                </option>
                                                <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Reporter</strong>
                                                </option>
                                                <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Journalist</strong>
                                                </option>
                                                <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dubbing Artist</strong>
                                                </option>
                                                <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Voice Over Artist</strong>
                                                </option>
                                                <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>New Influncer</strong>
                                                </option>
                                                <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Experienced Influncer</strong>
                                                </option>
                                                <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Advertising</strong>
                                                </option>
                                                <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Audio Advertising</strong>
                                                </option>
                                                <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Threater</strong>
                                                </option>
                                                <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Banner Advertising</strong>
                                                </option>
                                                <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Singer</strong>
                                                </option>
                                                <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Instrumentalist</strong>
                                                </option>
                                                <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Composer</strong>
                                                </option>
                                                <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Director Of Photography / Cinematographer</strong>
                                                </option>
                                                <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Cameraman</strong>
                                                </option>
                                                <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Camera Operator</strong>
                                                </option>
                                                <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Light man / Gaffer</strong>
                                                </option>
                                                <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Makeup Artist</strong>
                                                </option>
                                                <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Visual Effects Editor</strong>
                                                </option>
                                                <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Digital imaging technician</strong>
                                                </option>
                                                <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Special Effects makeup Artist</strong>
                                                </option>
                                                <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Motion control technician</strong>
                                                </option>
                                                <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Fashion Designer</strong>
                                                </option>
                                                <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Hair Stylist</strong>
                                                </option>
                                                <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Costume designer</strong>
                                                </option>
                                                <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Grip</strong>
                                                </option>
                                                <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Sound Designer</strong>
                                                </option>
                                                <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Sound Grip</strong>
                                                </option>
                                                <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Production Sound Mixer</strong>
                                                </option>
                                                <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Production Designer</strong>
                                                </option>
                                                <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Green man</strong>
                                                </option>
                                                <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Property master</strong>
                                                </option>
                                                <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Weapons master</strong>
                                                </option>
                                                <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Set Designer / Decorator</strong>
                                                </option>
                                                <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Location Manager</strong>
                                                </option>
                                                <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>StoryBoard Artist</strong>
                                                </option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ?
                                                    (($getIfAlreadyGallery[0]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Others</strong>
                                                </option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                <div class="browse-photo" onclick="showPhotoUploader('1')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                </div>
                                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                <input type="file" class="d-none" id="photosectionphoto1" name="photosectionphoto0">
                                            </div>
                                            <div class="u-close" style="float:left; margin-right:20px;">
                                                <img src="<?php echo ((count($getIfAlreadyGallery) >= 1 ? (($getIfAlreadyGallery[0]->photo != NULL || $getIfAlreadyGallery[0]->photo != '') ? $getIfAlreadyGallery[0]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                <?php 
                                                    if(count($getIfAlreadyGallery) >= 1)
                                                    {
                                                        if($getIfAlreadyGallery[0]->photo != NULL || $getIfAlreadyGallery[0]->photo != '')
                                                        {
                                                    ?>
                                                <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[0]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                                </a>
                                                <?php } } ?>
                                            </div>
                                            <div style="clear:both; overflow:hidden"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group col-lg-6" style="padding-left:0px">
                                            <label for="e1">Photo Title 2 - (3 words)</label>
                                            <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 2 ? (($getIfAlreadyGallery[1]->photo_title != NULL) ? substr($getIfAlreadyGallery[1]->photo_title,0,3) : '') : '')); ?>">
                                        </div>
                                        <div class="form-group col-lg-6"  style="padding:0px">
                                            <label for="e1">Select category</label>
                                            <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Select Category</strong>
                                                </option>
                                                <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Threater</strong>
                                                </option>
                                                <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Voiceover</strong>
                                                </option>
                                                <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Comedian</strong>
                                                </option>
                                                <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Photography</strong>
                                                </option>
                                                <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Grapher</strong>
                                                </option>
                                                <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Makeup Artrist</strong>
                                                </option>
                                                <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Actor</strong>
                                                </option>
                                                <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Child Actor</strong>
                                                </option>
                                                <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Comedy Actor</strong>
                                                </option>
                                                <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Junior Actor</strong>
                                                </option>
                                                <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Stunt Artist</strong>
                                                </option>
                                                <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Drama Artist</strong>
                                                </option>
                                                <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Mimicry Artist</strong>
                                                </option>
                                                <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Background / Atmosphere</strong>
                                                </option>
                                                <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Model</strong>
                                                </option>
                                                <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Child Model</strong>
                                                </option>
                                                <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Pro Model</strong>
                                                </option>
                                                <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dancer</strong>
                                                </option>
                                                <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Lead Dancer</strong>
                                                </option>
                                                <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Background Dancer</strong>
                                                </option>
                                                <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Editor</strong>
                                                </option>
                                                <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Editor</strong>
                                                </option>
                                                <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Audio Editor</strong>
                                                </option>
                                                <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dialogue Writer</strong>
                                                </option>
                                                <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Script Writer</strong>
                                                </option>
                                                <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Screeplay Writer</strong>
                                                </option>
                                                <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Story Writer</strong>
                                                </option>
                                                <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Lyricists</strong>
                                                </option>
                                                //video vj
                                                <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Video Jockey (VJ)</strong>
                                                </option>
                                                <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Radio Jockey (RJ)</strong>
                                                </option>
                                                <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> News Reader</strong>
                                                </option>
                                                <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Reporter</strong>
                                                </option>
                                                <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Journalist</strong>
                                                </option>
                                                <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dubbing Artist</strong>
                                                </option>
                                                <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Voice Over Artist</strong>
                                                </option>
                                                <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>New Influncer</strong>
                                                </option>
                                                <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Experienced Influncer</strong>
                                                </option>
                                                <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Advertising</strong>
                                                </option>
                                                <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Audio Advertising</strong>
                                                </option>
                                                <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Threater</strong>
                                                </option>
                                                <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Banner Advertising</strong>
                                                </option>
                                                <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Singer</strong>
                                                </option>
                                                <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Instrumentalist</strong>
                                                </option>
                                                <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Composer</strong>
                                                </option>
                                                <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Director Of Photography / Cinematographer</strong>
                                                </option>
                                                <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Cameraman</strong>
                                                </option>
                                                <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Camera Operator</strong>
                                                </option>
                                                <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Light man / Gaffer</strong>
                                                </option>
                                                <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Makeup Artist</strong>
                                                </option>
                                                <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Visual Effects Editor</strong>
                                                </option>
                                                <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Digital imaging technician</strong>
                                                </option>
                                                <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Special Effects makeup Artist</strong>
                                                </option>
                                                <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Motion control technician</strong>
                                                </option>
                                                <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Fashion Designer</strong>
                                                </option>
                                                <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Hair Stylist</strong>
                                                </option>
                                                <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Costume designer</strong>
                                                </option>
                                                <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Grip</strong>
                                                </option>
                                                <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Sound Designer</strong>
                                                </option>
                                                <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Sound Grip</strong>
                                                </option>
                                                <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Production Sound Mixer</strong>
                                                </option>
                                                <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Production Designer</strong>
                                                </option>
                                                <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Green man</strong>
                                                </option>
                                                <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Property master</strong>
                                                </option>
                                                <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Weapons master</strong>
                                                </option>
                                                <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Set Designer / Decorator</strong>
                                                </option>
                                                <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Location Manager</strong>
                                                </option>
                                                <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>StoryBoard Artist</strong>
                                                </option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ?
                                                    (($getIfAlreadyGallery[1]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Others</strong>
                                                </option>
                                                          
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                <div class="browse-photo" onclick="showPhotoUploader('2')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                </div>
                                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                <input type="file" class="d-none" id="photosectionphoto2" name="photosectionphoto1">
                                            </div>
                                            <div class="u-close" style="float:left; margin-right:20px;">
                                                <img src="<?php echo ((count($getIfAlreadyGallery) >= 2 ? (($getIfAlreadyGallery[1]->photo != NULL || $getIfAlreadyGallery[1]->photo != '') ? $getIfAlreadyGallery[1]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                <?php 
                                                    if(count($getIfAlreadyGallery) >= 2)
                                                    {
                                                        if($getIfAlreadyGallery[1]->photo != NULL || $getIfAlreadyGallery[1]->photo != '')
                                                        {
                                                    ?>
                                                <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[1]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                                </a>
                                                <?php } } ?>
                                            </div>
                                            <div style="clear:both; overflow:hidden"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group col-lg-6" style="padding-left:0px">
                                            <label for="e1">Photo Title 3 - (3 words)</label>
                                            <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[2]->photo_title != NULL) ? substr($getIfAlreadyGallery[2]->photo_title,0,3) : '') : '')); ?>">
                                        </div>
                                        <div class="form-group col-lg-6"  style="padding:0px">
                                            <label for="e1">Select category</label>
                                            <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Select Category</strong>
                                                </option>
                                                <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Threater</strong>
                                                </option>
                                                <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Voiceover</strong>
                                                </option>
                                                <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Comedian</strong>
                                                </option>
                                                <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Photography</strong>
                                                </option>
                                                <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Grapher</strong>
                                                </option>
                                                <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Makeup Artrist</strong>
                                                </option>
                                                <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Actor</strong>
                                                </option>
                                                <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Child Actor</strong>
                                                </option>
                                                <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Comedy Actor</strong>
                                                </option>
                                                <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Junior Actor</strong>
                                                </option>
                                                <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Stunt Artist</strong>
                                                </option>
                                                <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Drama Artist</strong>
                                                </option>
                                                <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Mimicry Artist</strong>
                                                </option>
                                                <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Background / Atmosphere</strong>
                                                </option>
                                                <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Model</strong>
                                                </option>
                                                <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Child Model</strong>
                                                </option>
                                                <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Pro Model</strong>
                                                </option>
                                                <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dancer</strong>
                                                </option>
                                                <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Lead Dancer</strong>
                                                </option>
                                                <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Background Dancer</strong>
                                                </option>
                                                <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Editor</strong>
                                                </option>
                                                <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Editor</strong>
                                                </option>
                                                <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Audio Editor</strong>
                                                </option>
                                                <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dialogue Writer</strong>
                                                </option>
                                                <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Script Writer</strong>
                                                </option>
                                                <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Screeplay Writer</strong>
                                                </option>
                                                <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Story Writer</strong>
                                                </option>
                                                <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Lyricists</strong>
                                                </option>
                                                //video vj
                                                <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Video Jockey (VJ)</strong>
                                                </option>
                                                <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Radio Jockey (RJ)</strong>
                                                </option>
                                                <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> News Reader</strong>
                                                </option>
                                                <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Reporter</strong>
                                                </option>
                                                <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Journalist</strong>
                                                </option>
                                                <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dubbing Artist</strong>
                                                </option>
                                                <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Voice Over Artist</strong>
                                                </option>
                                                <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>New Influncer</strong>
                                                </option>
                                                <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Experienced Influncer</strong>
                                                </option>
                                                <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Advertising</strong>
                                                </option>
                                                <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Audio Advertising</strong>
                                                </option>
                                                <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Threater</strong>
                                                </option>
                                                <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Banner Advertising</strong>
                                                </option>
                                                <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Singer</strong>
                                                </option>
                                                <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Instrumentalist</strong>
                                                </option>
                                                <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Composer</strong>
                                                </option>
                                                <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Director Of Photography / Cinematographer</strong>
                                                </option>
                                                <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Cameraman</strong>
                                                </option>
                                                <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Camera Operator</strong>
                                                </option>
                                                <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Light man / Gaffer</strong>
                                                </option>
                                                <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Makeup Artist</strong>
                                                </option>
                                                <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Visual Effects Editor</strong>
                                                </option>
                                                <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Digital imaging technician</strong>
                                                </option>
                                                <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Special Effects makeup Artist</strong>
                                                </option>
                                                <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Motion control technician</strong>
                                                </option>
                                                <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Fashion Designer</strong>
                                                </option>
                                                <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Hair Stylist</strong>
                                                </option>
                                                <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Costume designer</strong>
                                                </option>
                                                <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Grip</strong>
                                                </option>
                                                <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Sound Designer</strong>
                                                </option>
                                                <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Sound Grip</strong>
                                                </option>
                                                <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Production Sound Mixer</strong>
                                                </option>
                                                <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Production Designer</strong>
                                                </option>
                                                <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Green man</strong>
                                                </option>
                                                <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Property master</strong>
                                                </option>
                                                <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Weapons master</strong>
                                                </option>
                                                <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Set Designer / Decorator</strong>
                                                </option>
                                                <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Location Manager</strong>
                                                </option>
                                                <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>StoryBoard Artist</strong>
                                                </option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ?
                                                    (($getIfAlreadyGallery[2]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Others</strong>
                                                </option>
                                                          
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                <div class="browse-photo" onclick="showPhotoUploader('3')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                </div>
                                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                <input type="file" class="d-none" id="photosectionphoto3" name="photosectionphoto2">
                                            </div>
                                            <div class="u-close" style="float:left; margin-right:20px;">
                                                <img src="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[2]->photo != NULL || $getIfAlreadyGallery[2]->photo != '') ? $getIfAlreadyGallery[2]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                <?php 
                                                    if(count($getIfAlreadyGallery) >= 3)
                                                    {
                                                        if($getIfAlreadyGallery[2]->photo != NULL || $getIfAlreadyGallery[2]->photo != '')
                                                        {
                                                    ?>
                                                <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[2]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                                </a>
                                                <?php } } ?>
                                            </div>
                                            <div style="clear:both; overflow:hidden"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group col-lg-6" style="padding-left:0px">
                                            <label for="e1">Photo Title 4 - (3 words)</label>
                                            <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 4 ? (($getIfAlreadyGallery[3]->photo_title != NULL) ? substr($getIfAlreadyGallery[3]->photo_title,0,3) : '') : '')); ?>">
                                        </div>
                                        <div class="form-group col-lg-6"  style="padding:0px">
                                            <label for="e1">Select category</label>
                                            <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Select Category</strong>
                                                </option>
                                                <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Threater</strong>
                                                </option>
                                                <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Voiceover</strong>
                                                </option>
                                                <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Comedian</strong>
                                                </option>
                                                <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Photography</strong>
                                                </option>
                                                <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Grapher</strong>
                                                </option>
                                                <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Makeup Artrist</strong>
                                                </option>
                                                <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Actor</strong>
                                                </option>
                                                <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Child Actor</strong>
                                                </option>
                                                <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Comedy Actor</strong>
                                                </option>
                                                <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Junior Actor</strong>
                                                </option>
                                                <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Stunt Artist</strong>
                                                </option>
                                                <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Drama Artist</strong>
                                                </option>
                                                <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Mimicry Artist</strong>
                                                </option>
                                                <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Background / Atmosphere</strong>
                                                </option>
                                                <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Model</strong>
                                                </option>
                                                <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Child Model</strong>
                                                </option>
                                                <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Pro Model</strong>
                                                </option>
                                                <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dancer</strong>
                                                </option>
                                                <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Lead Dancer</strong>
                                                </option>
                                                <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Background Dancer</strong>
                                                </option>
                                                <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Editor</strong>
                                                </option>
                                                <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Editor</strong>
                                                </option>
                                                <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Audio Editor</strong>
                                                </option>
                                                <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dialogue Writer</strong>
                                                </option>
                                                <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Script Writer</strong>
                                                </option>
                                                <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Screeplay Writer</strong>
                                                </option>
                                                <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Story Writer</strong>
                                                </option>
                                                <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Lyricists</strong>
                                                </option>
                                                //video vj
                                                <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Video Jockey (VJ)</strong>
                                                </option>
                                                <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Radio Jockey (RJ)</strong>
                                                </option>
                                                <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> News Reader</strong>
                                                </option>
                                                <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Reporter</strong>
                                                </option>
                                                <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Journalist</strong>
                                                </option>
                                                <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dubbing Artist</strong>
                                                </option>
                                                <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Voice Over Artist</strong>
                                                </option>
                                                <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>New Influncer</strong>
                                                </option>
                                                <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Experienced Influncer</strong>
                                                </option>
                                                <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Advertising</strong>
                                                </option>
                                                <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Audio Advertising</strong>
                                                </option>
                                                <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Threater</strong>
                                                </option>
                                                <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Banner Advertising</strong>
                                                </option>
                                                <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Singer</strong>
                                                </option>
                                                <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Instrumentalist</strong>
                                                </option>
                                                <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Composer</strong>
                                                </option>
                                                <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Director Of Photography / Cinematographer</strong>
                                                </option>
                                                <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Cameraman</strong>
                                                </option>
                                                <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Camera Operator</strong>
                                                </option>
                                                <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Light man / Gaffer</strong>
                                                </option>
                                                <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Makeup Artist</strong>
                                                </option>
                                                <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Visual Effects Editor</strong>
                                                </option>
                                                <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Digital imaging technician</strong>
                                                </option>
                                                <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Special Effects makeup Artist</strong>
                                                </option>
                                                <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Motion control technician</strong>
                                                </option>
                                                <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Fashion Designer</strong>
                                                </option>
                                                <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Hair Stylist</strong>
                                                </option>
                                                <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Costume designer</strong>
                                                </option>
                                                <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Grip</strong>
                                                </option>
                                                <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Sound Designer</strong>
                                                </option>
                                                <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Sound Grip</strong>
                                                </option>
                                                <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Production Sound Mixer</strong>
                                                </option>
                                                <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Production Designer</strong>
                                                </option>
                                                <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Green man</strong>
                                                </option>
                                                <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Property master</strong>
                                                </option>
                                                <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Weapons master</strong>
                                                </option>
                                                <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Set Designer / Decorator</strong>
                                                </option>
                                                <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Location Manager</strong>
                                                </option>
                                                <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>StoryBoard Artist</strong>
                                                </option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ?
                                                    (($getIfAlreadyGallery[3]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Others</strong>
                                                </option>
                                                          
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                <div class="browse-photo" onclick="showPhotoUploader('4')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                </div>
                                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                <input type="file" class="d-none" id="photosectionphoto4" name="photosectionphoto3">
                                            </div>
                                            <div class="u-close" style="float:left; margin-right:20px;">
                                                <img src="<?php echo ((count($getIfAlreadyGallery) >= 4 ? (($getIfAlreadyGallery[3]->photo != NULL || $getIfAlreadyGallery[3]->photo != '') ? $getIfAlreadyGallery[3]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                <?php 
                                                    if(count($getIfAlreadyGallery) >= 4)
                                                    {
                                                        if($getIfAlreadyGallery[3]->photo != NULL || $getIfAlreadyGallery[3]->photo != '')
                                                        {
                                                    ?>
                                                <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[3]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                                </a>
                                                <?php } } ?>
                                            </div>
                                            <div style="clear:both; overflow:hidden"></div>
                                        </div>
                                    </div>
                                </div>
                                <a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;" id="addPhotoTwoMore" onclick="addfourmore('2')">Add More 4 photos grid</a>
                                <a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;" class="d-none" id="removePhotoTwoMore" onclick="removefourmore('2')">Remove 4 photos grid</a>
                                <br>
                                <div id="portfolio2">
                                    <div id="portfolio3" class="d-none">
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title 5 - (3 words)</label>
                                                <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 5 ? (($getIfAlreadyGallery[4]->photo_title != NULL) ? substr($getIfAlreadyGallery[4]->photo_title,0,3) : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Select Category</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voiceover</strong>
                                                    </option>
                                                    <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedian</strong>
                                                    </option>
                                                    <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Photography</strong>
                                                    </option>
                                                    <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Grapher</strong>
                                                    </option>
                                                    <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artrist</strong>
                                                    </option>
                                                    <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Actor</strong>
                                                    </option>
                                                    <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Child Actor</strong>
                                                    </option>
                                                    <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedy Actor</strong>
                                                    </option>
                                                    <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Junior Actor</strong>
                                                    </option>
                                                    <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Stunt Artist</strong>
                                                    </option>
                                                    <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Drama Artist</strong>
                                                    </option>
                                                    <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Mimicry Artist</strong>
                                                    </option>
                                                    <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Background / Atmosphere</strong>
                                                    </option>
                                                    <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Model</strong>
                                                    </option>
                                                    <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Child Model</strong>
                                                    </option>
                                                    <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Pro Model</strong>
                                                    </option>
                                                    <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dancer</strong>
                                                    </option>
                                                    <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lead Dancer</strong>
                                                    </option>
                                                    <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Background Dancer</strong>
                                                    </option>
                                                    <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Editor</strong>
                                                    </option>
                                                    <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Editor</strong>
                                                    </option>
                                                    <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Editor</strong>
                                                    </option>
                                                    <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dialogue Writer</strong>
                                                    </option>
                                                    <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Script Writer</strong>
                                                    </option>
                                                    <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Screeplay Writer</strong>
                                                    </option>
                                                    <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Story Writer</strong>
                                                    </option>
                                                    <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lyricists</strong>
                                                    </option>
                                                    //video vj
                                                    <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Video Jockey (VJ)</strong>
                                                    </option>
                                                    <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Radio Jockey (RJ)</strong>
                                                    </option>
                                                    <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> News Reader</strong>
                                                    </option>
                                                    <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Reporter</strong>
                                                    </option>
                                                    <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Journalist</strong>
                                                    </option>
                                                    <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dubbing Artist</strong>
                                                    </option>
                                                    <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voice Over Artist</strong>
                                                    </option>
                                                    <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>New Influncer</strong>
                                                    </option>
                                                    <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Experienced Influncer</strong>
                                                    </option>
                                                    <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Advertising</strong>
                                                    </option>
                                                    <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Advertising</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Banner Advertising</strong>
                                                    </option>
                                                    <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Singer</strong>
                                                    </option>
                                                    <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Instrumentalist</strong>
                                                    </option>
                                                    <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Composer</strong>
                                                    </option>
                                                    <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Director Of Photography / Cinematographer</strong>
                                                    </option>
                                                    <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Cameraman</strong>
                                                    </option>
                                                    <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Camera Operator</strong>
                                                    </option>
                                                    <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Light man / Gaffer</strong>
                                                    </option>
                                                    <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artist</strong>
                                                    </option>
                                                    <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Visual Effects Editor</strong>
                                                    </option>
                                                    <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Digital imaging technician</strong>
                                                    </option>
                                                    <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Special Effects makeup Artist</strong>
                                                    </option>
                                                    <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Motion control technician</strong>
                                                    </option>
                                                    <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Fashion Designer</strong>
                                                    </option>
                                                    <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Hair Stylist</strong>
                                                    </option>
                                                    <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Costume designer</strong>
                                                    </option>
                                                    <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Grip</strong>
                                                    </option>
                                                    <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Designer</strong>
                                                    </option>
                                                    <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Grip</strong>
                                                    </option>
                                                    <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Sound Mixer</strong>
                                                    </option>
                                                    <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Designer</strong>
                                                    </option>
                                                    <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Green man</strong>
                                                    </option>
                                                    <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Property master</strong>
                                                    </option>
                                                    <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Weapons master</strong>
                                                    </option>
                                                    <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Set Designer / Decorator</strong>
                                                    </option>
                                                    <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Location Manager</strong>
                                                    </option>
                                                    <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>StoryBoard Artist</strong>
                                                    </option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ?
                                                        (($getIfAlreadyGallery[4]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Others</strong>
                                                    </option>
                                                              
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="showPhotoUploader('5')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                    <input type="file" class="d-none" id="photosectionphoto5" name="photosectionphoto4">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img src="<?php echo ((count($getIfAlreadyGallery) >= 5 ? (($getIfAlreadyGallery[4]->photo != NULL || $getIfAlreadyGallery[4]->photo != '') ? $getIfAlreadyGallery[4]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 5)
                                                        {
                                                            if($getIfAlreadyGallery[4]->photo != NULL || $getIfAlreadyGallery[4]->photo != '')
                                                            {
                                                        ?>
                                                    <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[4]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title 6 - (3 words)</label>
                                                <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 6 ? (($getIfAlreadyGallery[5]->photo_title != NULL) ? substr($getIfAlreadyGallery[5]->photo_title,0,3) : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Select Category</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voiceover</strong>
                                                    </option>
                                                    <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedian</strong>
                                                    </option>
                                                    <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Photography</strong>
                                                    </option>
                                                    <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Grapher</strong>
                                                    </option>
                                                    <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artrist</strong>
                                                    </option>
                                                    <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Actor</strong>
                                                    </option>
                                                    <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Child Actor</strong>
                                                    </option>
                                                    <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedy Actor</strong>
                                                    </option>
                                                    <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Junior Actor</strong>
                                                    </option>
                                                    <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Stunt Artist</strong>
                                                    </option>
                                                    <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Drama Artist</strong>
                                                    </option>
                                                    <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Mimicry Artist</strong>
                                                    </option>
                                                    <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Background / Atmosphere</strong>
                                                    </option>
                                                    <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Model</strong>
                                                    </option>
                                                    <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Child Model</strong>
                                                    </option>
                                                    <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Pro Model</strong>
                                                    </option>
                                                    <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dancer</strong>
                                                    </option>
                                                    <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lead Dancer</strong>
                                                    </option>
                                                    <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Background Dancer</strong>
                                                    </option>
                                                    <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Editor</strong>
                                                    </option>
                                                    <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Editor</strong>
                                                    </option>
                                                    <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Editor</strong>
                                                    </option>
                                                    <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dialogue Writer</strong>
                                                    </option>
                                                    <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Script Writer</strong>
                                                    </option>
                                                    <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Screeplay Writer</strong>
                                                    </option>
                                                    <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Story Writer</strong>
                                                    </option>
                                                    <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lyricists</strong>
                                                    </option>
                                                    //video vj
                                                    <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Video Jockey (VJ)</strong>
                                                    </option>
                                                    <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Radio Jockey (RJ)</strong>
                                                    </option>
                                                    <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> News Reader</strong>
                                                    </option>
                                                    <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Reporter</strong>
                                                    </option>
                                                    <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Journalist</strong>
                                                    </option>
                                                    <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dubbing Artist</strong>
                                                    </option>
                                                    <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voice Over Artist</strong>
                                                    </option>
                                                    <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>New Influncer</strong>
                                                    </option>
                                                    <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Experienced Influncer</strong>
                                                    </option>
                                                    <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Advertising</strong>
                                                    </option>
                                                    <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Advertising</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Banner Advertising</strong>
                                                    </option>
                                                    <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Singer</strong>
                                                    </option>
                                                    <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Instrumentalist</strong>
                                                    </option>
                                                    <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Composer</strong>
                                                    </option>
                                                    <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Director Of Photography / Cinematographer</strong>
                                                    </option>
                                                    <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Cameraman</strong>
                                                    </option>
                                                    <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Camera Operator</strong>
                                                    </option>
                                                    <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Light man / Gaffer</strong>
                                                    </option>
                                                    <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artist</strong>
                                                    </option>
                                                    <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Visual Effects Editor</strong>
                                                    </option>
                                                    <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Digital imaging technician</strong>
                                                    </option>
                                                    <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Special Effects makeup Artist</strong>
                                                    </option>
                                                    <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Motion control technician</strong>
                                                    </option>
                                                    <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Fashion Designer</strong>
                                                    </option>
                                                    <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Hair Stylist</strong>
                                                    </option>
                                                    <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Costume designer</strong>
                                                    </option>
                                                    <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Grip</strong>
                                                    </option>
                                                    <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Designer</strong>
                                                    </option>
                                                    <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Grip</strong>
                                                    </option>
                                                    <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Sound Mixer</strong>
                                                    </option>
                                                    <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Designer</strong>
                                                    </option>
                                                    <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Green man</strong>
                                                    </option>
                                                    <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Property master</strong>
                                                    </option>
                                                    <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Weapons master</strong>
                                                    </option>
                                                    <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Set Designer / Decorator</strong>
                                                    </option>
                                                    <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Location Manager</strong>
                                                    </option>
                                                    <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>StoryBoard Artist</strong>
                                                    </option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ?
                                                        (($getIfAlreadyGallery[5]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Others</strong>
                                                    </option>
                                                              
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="showPhotoUploader('6')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                    <input type="file" class="d-none" id="photosectionphoto6" name="photosectionphoto5">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img src="<?php echo ((count($getIfAlreadyGallery) >= 6 ? (($getIfAlreadyGallery[5]->photo != NULL || $getIfAlreadyGallery[5]->photo != '') ? $getIfAlreadyGallery[5]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 6)
                                                        {
                                                            if($getIfAlreadyGallery[5]->photo != NULL || $getIfAlreadyGallery[5]->photo != '')
                                                            {
                                                        ?>
                                                    <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[5]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title 7 - (3 words)</label>
                                                <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 7 ? (($getIfAlreadyGallery[6]->photo_title != NULL) ? substr($getIfAlreadyGallery[6]->photo_title,0,3) : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Select Category</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voiceover</strong>
                                                    </option>
                                                    <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedian</strong>
                                                    </option>
                                                    <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Photography</strong>
                                                    </option>
                                                    <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Grapher</strong>
                                                    </option>
                                                    <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artrist</strong>
                                                    </option>
                                                    <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Actor</strong>
                                                    </option>
                                                    <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Child Actor</strong>
                                                    </option>
                                                    <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedy Actor</strong>
                                                    </option>
                                                    <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Junior Actor</strong>
                                                    </option>
                                                    <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Stunt Artist</strong>
                                                    </option>
                                                    <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Drama Artist</strong>
                                                    </option>
                                                    <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Mimicry Artist</strong>
                                                    </option>
                                                    <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Background / Atmosphere</strong>
                                                    </option>
                                                    <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Model</strong>
                                                    </option>
                                                    <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Child Model</strong>
                                                    </option>
                                                    <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Pro Model</strong>
                                                    </option>
                                                    <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dancer</strong>
                                                    </option>
                                                    <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lead Dancer</strong>
                                                    </option>
                                                    <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Background Dancer</strong>
                                                    </option>
                                                    <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Editor</strong>
                                                    </option>
                                                    <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Editor</strong>
                                                    </option>
                                                    <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Editor</strong>
                                                    </option>
                                                    <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dialogue Writer</strong>
                                                    </option>
                                                    <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Script Writer</strong>
                                                    </option>
                                                    <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Screeplay Writer</strong>
                                                    </option>
                                                    <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Story Writer</strong>
                                                    </option>
                                                    <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lyricists</strong>
                                                    </option>
                                                    //video vj
                                                    <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Video Jockey (VJ)</strong>
                                                    </option>
                                                    <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Radio Jockey (RJ)</strong>
                                                    </option>
                                                    <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> News Reader</strong>
                                                    </option>
                                                    <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Reporter</strong>
                                                    </option>
                                                    <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Journalist</strong>
                                                    </option>
                                                    <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dubbing Artist</strong>
                                                    </option>
                                                    <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voice Over Artist</strong>
                                                    </option>
                                                    <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>New Influncer</strong>
                                                    </option>
                                                    <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Experienced Influncer</strong>
                                                    </option>
                                                    <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Advertising</strong>
                                                    </option>
                                                    <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Advertising</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Banner Advertising</strong>
                                                    </option>
                                                    <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Singer</strong>
                                                    </option>
                                                    <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Instrumentalist</strong>
                                                    </option>
                                                    <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Composer</strong>
                                                    </option>
                                                    <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Director Of Photography / Cinematographer</strong>
                                                    </option>
                                                    <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Cameraman</strong>
                                                    </option>
                                                    <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Camera Operator</strong>
                                                    </option>
                                                    <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Light man / Gaffer</strong>
                                                    </option>
                                                    <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artist</strong>
                                                    </option>
                                                    <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Visual Effects Editor</strong>
                                                    </option>
                                                    <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Digital imaging technician</strong>
                                                    </option>
                                                    <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Special Effects makeup Artist</strong>
                                                    </option>
                                                    <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Motion control technician</strong>
                                                    </option>
                                                    <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Fashion Designer</strong>
                                                    </option>
                                                    <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Hair Stylist</strong>
                                                    </option>
                                                    <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Costume designer</strong>
                                                    </option>
                                                    <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Grip</strong>
                                                    </option>
                                                    <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Designer</strong>
                                                    </option>
                                                    <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Grip</strong>
                                                    </option>
                                                    <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Sound Mixer</strong>
                                                    </option>
                                                    <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Designer</strong>
                                                    </option>
                                                    <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Green man</strong>
                                                    </option>
                                                    <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Property master</strong>
                                                    </option>
                                                    <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Weapons master</strong>
                                                    </option>
                                                    <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Set Designer / Decorator</strong>
                                                    </option>
                                                    <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Location Manager</strong>
                                                    </option>
                                                    <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>StoryBoard Artist</strong>
                                                    </option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ?
                                                        (($getIfAlreadyGallery[6]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Others</strong>
                                                    </option>
                                                              
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="showPhotoUploader('7')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                    <input type="file" class="d-none" id="photosectionphoto7" name="photosectionphoto6">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img src="<?php echo ((count($getIfAlreadyGallery) >= 7 ? (($getIfAlreadyGallery[6]->photo != NULL || $getIfAlreadyGallery[6]->photo != '') ? $getIfAlreadyGallery[6]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 7)
                                                        {
                                                            if($getIfAlreadyGallery[6]->photo != NULL || $getIfAlreadyGallery[6]->photo != '')
                                                            {
                                                        ?>
                                                    <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[6]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title 8 - (3 words)</label>
                                                <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 8 ? (($getIfAlreadyGallery[7]->photo_title != NULL) ? substr($getIfAlreadyGallery[7]->photo_title,0,3) : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Select Category</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voiceover</strong>
                                                    </option>
                                                    <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedian</strong>
                                                    </option>
                                                    <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Photography</strong>
                                                    </option>
                                                    <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Grapher</strong>
                                                    </option>
                                                    <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artrist</strong>
                                                    </option>
                                                    <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Actor</strong>
                                                    </option>
                                                    <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Child Actor</strong>
                                                    </option>
                                                    <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedy Actor</strong>
                                                    </option>
                                                    <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Junior Actor</strong>
                                                    </option>
                                                    <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Stunt Artist</strong>
                                                    </option>
                                                    <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Drama Artist</strong>
                                                    </option>
                                                    <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Mimicry Artist</strong>
                                                    </option>
                                                    <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Background / Atmosphere</strong>
                                                    </option>
                                                    <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Model</strong>
                                                    </option>
                                                    <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Child Model</strong>
                                                    </option>
                                                    <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Pro Model</strong>
                                                    </option>
                                                    <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dancer</strong>
                                                    </option>
                                                    <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lead Dancer</strong>
                                                    </option>
                                                    <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Background Dancer</strong>
                                                    </option>
                                                    <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Editor</strong>
                                                    </option>
                                                    <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Editor</strong>
                                                    </option>
                                                    <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Editor</strong>
                                                    </option>
                                                    <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dialogue Writer</strong>
                                                    </option>
                                                    <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Script Writer</strong>
                                                    </option>
                                                    <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Screeplay Writer</strong>
                                                    </option>
                                                    <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Story Writer</strong>
                                                    </option>
                                                    <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lyricists</strong>
                                                    </option>
                                                    //video vj
                                                    <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Video Jockey (VJ)</strong>
                                                    </option>
                                                    <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Radio Jockey (RJ)</strong>
                                                    </option>
                                                    <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> News Reader</strong>
                                                    </option>
                                                    <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Reporter</strong>
                                                    </option>
                                                    <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Journalist</strong>
                                                    </option>
                                                    <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dubbing Artist</strong>
                                                    </option>
                                                    <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voice Over Artist</strong>
                                                    </option>
                                                    <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>New Influncer</strong>
                                                    </option>
                                                    <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Experienced Influncer</strong>
                                                    </option>
                                                    <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Advertising</strong>
                                                    </option>
                                                    <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Advertising</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Banner Advertising</strong>
                                                    </option>
                                                    <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Singer</strong>
                                                    </option>
                                                    <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Instrumentalist</strong>
                                                    </option>
                                                    <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Composer</strong>
                                                    </option>
                                                    <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Director Of Photography / Cinematographer</strong>
                                                    </option>
                                                    <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Cameraman</strong>
                                                    </option>
                                                    <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Camera Operator</strong>
                                                    </option>
                                                    <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Light man / Gaffer</strong>
                                                    </option>
                                                    <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artist</strong>
                                                    </option>
                                                    <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Visual Effects Editor</strong>
                                                    </option>
                                                    <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Digital imaging technician</strong>
                                                    </option>
                                                    <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Special Effects makeup Artist</strong>
                                                    </option>
                                                    <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Motion control technician</strong>
                                                    </option>
                                                    <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Fashion Designer</strong>
                                                    </option>
                                                    <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Hair Stylist</strong>
                                                    </option>
                                                    <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Costume designer</strong>
                                                    </option>
                                                    <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Grip</strong>
                                                    </option>
                                                    <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Designer</strong>
                                                    </option>
                                                    <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Grip</strong>
                                                    </option>
                                                    <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Sound Mixer</strong>
                                                    </option>
                                                    <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Designer</strong>
                                                    </option>
                                                    <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Green man</strong>
                                                    </option>
                                                    <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Property master</strong>
                                                    </option>
                                                    <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Weapons master</strong>
                                                    </option>
                                                    <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Set Designer / Decorator</strong>
                                                    </option>
                                                    <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Location Manager</strong>
                                                    </option>
                                                    <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>StoryBoard Artist</strong>
                                                    </option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ?
                                                        (($getIfAlreadyGallery[7]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Others</strong>
                                                    </option>
                                                              
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="showPhotoUploader('8')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                    <input type="file" class="d-none" id="photosectionphoto8" name="photosectionphoto7">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img src="<?php echo ((count($getIfAlreadyGallery) >= 8 ? (($getIfAlreadyGallery[7]->photo != NULL || $getIfAlreadyGallery[7]->photo != '') ? $getIfAlreadyGallery[7]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 8)
                                                        {
                                                            if($getIfAlreadyGallery[7]->photo != NULL || $getIfAlreadyGallery[7]->photo != '')
                                                            {
                                                        ?>
                                                    <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[7]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;" class="d-none" id="addPhotoThreeMore" onclick="addfourmore('3')">Add More 4 photos grid</a>
                                    <a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;" class="d-none" id="removePhotoThreeMore" onclick="removefourmore('3')">Remove 4 photos grid</a>
                                    <div id="portfolio4" class="d-none">
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title 9 - (3 words)</label>
                                                <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 9 ? (($getIfAlreadyGallery[8]->photo_title != NULL) ? substr($getIfAlreadyGallery[8]->photo_title,0,3) : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Select Category</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voiceover</strong>
                                                    </option>
                                                    <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedian</strong>
                                                    </option>
                                                    <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Photography</strong>
                                                    </option>
                                                    <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Grapher</strong>
                                                    </option>
                                                    <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artrist</strong>
                                                    </option>
                                                    <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Actor</strong>
                                                    </option>
                                                    <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Child Actor</strong>
                                                    </option>
                                                    <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedy Actor</strong>
                                                    </option>
                                                    <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Junior Actor</strong>
                                                    </option>
                                                    <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Stunt Artist</strong>
                                                    </option>
                                                    <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Drama Artist</strong>
                                                    </option>
                                                    <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Mimicry Artist</strong>
                                                    </option>
                                                    <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Background / Atmosphere</strong>
                                                    </option>
                                                    <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Model</strong>
                                                    </option>
                                                    <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Child Model</strong>
                                                    </option>
                                                    <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Pro Model</strong>
                                                    </option>
                                                    <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dancer</strong>
                                                    </option>
                                                    <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lead Dancer</strong>
                                                    </option>
                                                    <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Background Dancer</strong>
                                                    </option>
                                                    <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Editor</strong>
                                                    </option>
                                                    <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Editor</strong>
                                                    </option>
                                                    <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Editor</strong>
                                                    </option>
                                                    <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dialogue Writer</strong>
                                                    </option>
                                                    <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Script Writer</strong>
                                                    </option>
                                                    <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Screeplay Writer</strong>
                                                    </option>
                                                    <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Story Writer</strong>
                                                    </option>
                                                    <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lyricists</strong>
                                                    </option>
                                                    //video vj
                                                    <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Video Jockey (VJ)</strong>
                                                    </option>
                                                    <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Radio Jockey (RJ)</strong>
                                                    </option>
                                                    <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> News Reader</strong>
                                                    </option>
                                                    <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Reporter</strong>
                                                    </option>
                                                    <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Journalist</strong>
                                                    </option>
                                                    <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dubbing Artist</strong>
                                                    </option>
                                                    <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voice Over Artist</strong>
                                                    </option>
                                                    <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>New Influncer</strong>
                                                    </option>
                                                    <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Experienced Influncer</strong>
                                                    </option>
                                                    <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Advertising</strong>
                                                    </option>
                                                    <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Advertising</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Banner Advertising</strong>
                                                    </option>
                                                    <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Singer</strong>
                                                    </option>
                                                    <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Instrumentalist</strong>
                                                    </option>
                                                    <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Composer</strong>
                                                    </option>
                                                    <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Director Of Photography / Cinematographer</strong>
                                                    </option>
                                                    <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Cameraman</strong>
                                                    </option>
                                                    <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Camera Operator</strong>
                                                    </option>
                                                    <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Light man / Gaffer</strong>
                                                    </option>
                                                    <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artist</strong>
                                                    </option>
                                                    <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Visual Effects Editor</strong>
                                                    </option>
                                                    <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Digital imaging technician</strong>
                                                    </option>
                                                    <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Special Effects makeup Artist</strong>
                                                    </option>
                                                    <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Motion control technician</strong>
                                                    </option>
                                                    <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Fashion Designer</strong>
                                                    </option>
                                                    <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Hair Stylist</strong>
                                                    </option>
                                                    <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Costume designer</strong>
                                                    </option>
                                                    <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Grip</strong>
                                                    </option>
                                                    <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Designer</strong>
                                                    </option>
                                                    <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Grip</strong>
                                                    </option>
                                                    <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Sound Mixer</strong>
                                                    </option>
                                                    <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Designer</strong>
                                                    </option>
                                                    <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Green man</strong>
                                                    </option>
                                                    <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Property master</strong>
                                                    </option>
                                                    <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Weapons master</strong>
                                                    </option>
                                                    <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Set Designer / Decorator</strong>
                                                    </option>
                                                    <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Location Manager</strong>
                                                    </option>
                                                    <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>StoryBoard Artist</strong>
                                                    </option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ?
                                                        (($getIfAlreadyGallery[8]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Others</strong>
                                                    </option>
                                                              
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="showPhotoUploader('9')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                    <input type="file" class="d-none" id="photosectionphoto9" name="photosectionphoto8">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img src="<?php echo ((count($getIfAlreadyGallery) >= 9 ? (($getIfAlreadyGallery[8]->photo != NULL || $getIfAlreadyGallery[8]->photo != '') ? $getIfAlreadyGallery[8]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 9)
                                                        {
                                                            if($getIfAlreadyGallery[8]->photo != NULL || $getIfAlreadyGallery[8]->photo != '')
                                                            {
                                                        ?>
                                                    <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[8]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title 10 - (3 words)</label>
                                                <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 10 ? (( $getIfAlreadyGallery[9]->photo_title != NULL) ? substr($getIfAlreadyGallery[9]->photo_title,0,3) : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Select Category</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voiceover</strong>
                                                    </option>
                                                    <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedian</strong>
                                                    </option>
                                                    <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Photography</strong>
                                                    </option>
                                                    <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Grapher</strong>
                                                    </option>
                                                    <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artrist</strong>
                                                    </option>
                                                    <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Actor</strong>
                                                    </option>
                                                    <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Child Actor</strong>
                                                    </option>
                                                    <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedy Actor</strong>
                                                    </option>
                                                    <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Junior Actor</strong>
                                                    </option>
                                                    <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Stunt Artist</strong>
                                                    </option>
                                                    <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Drama Artist</strong>
                                                    </option>
                                                    <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Mimicry Artist</strong>
                                                    </option>
                                                    <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Background / Atmosphere</strong>
                                                    </option>
                                                    <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Model</strong>
                                                    </option>
                                                    <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Child Model</strong>
                                                    </option>
                                                    <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Pro Model</strong>
                                                    </option>
                                                    <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dancer</strong>
                                                    </option>
                                                    <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lead Dancer</strong>
                                                    </option>
                                                    <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Background Dancer</strong>
                                                    </option>
                                                    <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Editor</strong>
                                                    </option>
                                                    <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Editor</strong>
                                                    </option>
                                                    <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Editor</strong>
                                                    </option>
                                                    <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dialogue Writer</strong>
                                                    </option>
                                                    <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Script Writer</strong>
                                                    </option>
                                                    <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Screeplay Writer</strong>
                                                    </option>
                                                    <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Story Writer</strong>
                                                    </option>
                                                    <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lyricists</strong>
                                                    </option>
                                                    //video vj
                                                    <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Video Jockey (VJ)</strong>
                                                    </option>
                                                    <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Radio Jockey (RJ)</strong>
                                                    </option>
                                                    <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> News Reader</strong>
                                                    </option>
                                                    <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Reporter</strong>
                                                    </option>
                                                    <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Journalist</strong>
                                                    </option>
                                                    <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dubbing Artist</strong>
                                                    </option>
                                                    <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voice Over Artist</strong>
                                                    </option>
                                                    <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>New Influncer</strong>
                                                    </option>
                                                    <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Experienced Influncer</strong>
                                                    </option>
                                                    <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Advertising</strong>
                                                    </option>
                                                    <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Advertising</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Banner Advertising</strong>
                                                    </option>
                                                    <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Singer</strong>
                                                    </option>
                                                    <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Instrumentalist</strong>
                                                    </option>
                                                    <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Composer</strong>
                                                    </option>
                                                    <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Director Of Photography / Cinematographer</strong>
                                                    </option>
                                                    <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Cameraman</strong>
                                                    </option>
                                                    <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Camera Operator</strong>
                                                    </option>
                                                    <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Light man / Gaffer</strong>
                                                    </option>
                                                    <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artist</strong>
                                                    </option>
                                                    <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Visual Effects Editor</strong>
                                                    </option>
                                                    <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Digital imaging technician</strong>
                                                    </option>
                                                    <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Special Effects makeup Artist</strong>
                                                    </option>
                                                    <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Motion control technician</strong>
                                                    </option>
                                                    <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Fashion Designer</strong>
                                                    </option>
                                                    <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Hair Stylist</strong>
                                                    </option>
                                                    <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Costume designer</strong>
                                                    </option>
                                                    <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Grip</strong>
                                                    </option>
                                                    <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Designer</strong>
                                                    </option>
                                                    <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Grip</strong>
                                                    </option>
                                                    <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Sound Mixer</strong>
                                                    </option>
                                                    <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Designer</strong>
                                                    </option>
                                                    <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Green man</strong>
                                                    </option>
                                                    <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Property master</strong>
                                                    </option>
                                                    <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Weapons master</strong>
                                                    </option>
                                                    <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Set Designer / Decorator</strong>
                                                    </option>
                                                    <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Location Manager</strong>
                                                    </option>
                                                    <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>StoryBoard Artist</strong>
                                                    </option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ?
                                                        (($getIfAlreadyGallery[9]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Others</strong>
                                                    </option>
                                                              
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="showPhotoUploader('10')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                    <input type="file" class="d-none" id="photosectionphoto10" name="photosectionphoto9">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img src="<?php echo ((count($getIfAlreadyGallery) >= 10 ? (($getIfAlreadyGallery[9]->photo != NULL || $getIfAlreadyGallery[9]->photo != '') ? $getIfAlreadyGallery[9]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 10)
                                                        {
                                                            if($getIfAlreadyGallery[9]->photo != NULL || $getIfAlreadyGallery[9]->photo != '')
                                                            {
                                                        ?>
                                                    <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[9]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title 11 - (3 words)</label>
                                                <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 11 ? (( $getIfAlreadyGallery[10]->photo_title != NULL) ? substr($getIfAlreadyGallery[10]->photo_title,0,3) : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Select Category</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voiceover</strong>
                                                    </option>
                                                    <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedian</strong>
                                                    </option>
                                                    <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Photography</strong>
                                                    </option>
                                                    <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Grapher</strong>
                                                    </option>
                                                    <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artrist</strong>
                                                    </option>
                                                    <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Actor</strong>
                                                    </option>
                                                    <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Child Actor</strong>
                                                    </option>
                                                    <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedy Actor</strong>
                                                    </option>
                                                    <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Junior Actor</strong>
                                                    </option>
                                                    <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Stunt Artist</strong>
                                                    </option>
                                                    <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Drama Artist</strong>
                                                    </option>
                                                    <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Mimicry Artist</strong>
                                                    </option>
                                                    <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Background / Atmosphere</strong>
                                                    </option>
                                                    <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Model</strong>
                                                    </option>
                                                    <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Child Model</strong>
                                                    </option>
                                                    <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Pro Model</strong>
                                                    </option>
                                                    <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dancer</strong>
                                                    </option>
                                                    <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lead Dancer</strong>
                                                    </option>
                                                    <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Background Dancer</strong>
                                                    </option>
                                                    <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Editor</strong>
                                                    </option>
                                                    <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Editor</strong>
                                                    </option>
                                                    <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Editor</strong>
                                                    </option>
                                                    <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dialogue Writer</strong>
                                                    </option>
                                                    <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Script Writer</strong>
                                                    </option>
                                                    <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Screeplay Writer</strong>
                                                    </option>
                                                    <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Story Writer</strong>
                                                    </option>
                                                    <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lyricists</strong>
                                                    </option>
                                                    //video vj
                                                    <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Video Jockey (VJ)</strong>
                                                    </option>
                                                    <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Radio Jockey (RJ)</strong>
                                                    </option>
                                                    <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> News Reader</strong>
                                                    </option>
                                                    <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Reporter</strong>
                                                    </option>
                                                    <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Journalist</strong>
                                                    </option>
                                                    <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dubbing Artist</strong>
                                                    </option>
                                                    <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voice Over Artist</strong>
                                                    </option>
                                                    <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>New Influncer</strong>
                                                    </option>
                                                    <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Experienced Influncer</strong>
                                                    </option>
                                                    <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Advertising</strong>
                                                    </option>
                                                    <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Advertising</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Banner Advertising</strong>
                                                    </option>
                                                    <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Singer</strong>
                                                    </option>
                                                    <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Instrumentalist</strong>
                                                    </option>
                                                    <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Composer</strong>
                                                    </option>
                                                    <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Director Of Photography / Cinematographer</strong>
                                                    </option>
                                                    <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Cameraman</strong>
                                                    </option>
                                                    <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Camera Operator</strong>
                                                    </option>
                                                    <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Light man / Gaffer</strong>
                                                    </option>
                                                    <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artist</strong>
                                                    </option>
                                                    <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Visual Effects Editor</strong>
                                                    </option>
                                                    <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Digital imaging technician</strong>
                                                    </option>
                                                    <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Special Effects makeup Artist</strong>
                                                    </option>
                                                    <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Motion control technician</strong>
                                                    </option>
                                                    <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Fashion Designer</strong>
                                                    </option>
                                                    <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Hair Stylist</strong>
                                                    </option>
                                                    <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Costume designer</strong>
                                                    </option>
                                                    <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Grip</strong>
                                                    </option>
                                                    <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Designer</strong>
                                                    </option>
                                                    <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Grip</strong>
                                                    </option>
                                                    <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Sound Mixer</strong>
                                                    </option>
                                                    <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Designer</strong>
                                                    </option>
                                                    <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Green man</strong>
                                                    </option>
                                                    <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Property master</strong>
                                                    </option>
                                                    <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Weapons master</strong>
                                                    </option>
                                                    <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Set Designer / Decorator</strong>
                                                    </option>
                                                    <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Location Manager</strong>
                                                    </option>
                                                    <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>StoryBoard Artist</strong>
                                                    </option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ?
                                                        (($getIfAlreadyGallery[10]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Others</strong>
                                                    </option>
                                                              
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="showPhotoUploader('11')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                    <input type="file" class="d-none" id="photosectionphoto11" name="photosectionphoto10">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img src="<?php echo ((count($getIfAlreadyGallery) >= 11 ? (($getIfAlreadyGallery[10]->photo != NULL || $getIfAlreadyGallery[10]->photo != '') ? $getIfAlreadyGallery[10]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 11)
                                                        {
                                                            if($getIfAlreadyGallery[10]->photo != NULL || $getIfAlreadyGallery[10]->photo != '')
                                                            {
                                                        ?>
                                                    <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[10]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title 12 - (3 words)</label>
                                                <input type="text" name="photo_title[]" onKeyPress="return check(event,value)" onInput="checkLength(3,this)" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 12 ? (($getIfAlreadyGallery[11]->photo_title != NULL) ? substr($getIfAlreadyGallery[11]->photo_title,0,3) : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Select Category</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Voiceover" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voiceover</strong>
                                                    </option>
                                                    <option value="Comedian" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedian</strong>
                                                    </option>
                                                    <option value=" Photography" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Photography</strong>
                                                    </option>
                                                    <option value="Video Grapher" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Grapher</strong>
                                                    </option>
                                                    <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artrist</strong>
                                                    </option>
                                                    <option value="Actor" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Actor</strong>
                                                    </option>
                                                    <option value="Child Actor" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Child Actor</strong>
                                                    </option>
                                                    <option value="Comedy Actor" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedy Actor</strong>
                                                    </option>
                                                    <option value=" Junior Actor" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Junior Actor</strong>
                                                    </option>
                                                    <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Stunt Artist</strong>
                                                    </option>
                                                    <option value=" Drama Artist" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Drama Artist</strong>
                                                    </option>
                                                    <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Mimicry Artist</strong>
                                                    </option>
                                                    <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Background / Atmosphere</strong>
                                                    </option>
                                                    <option value="Model" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Model</strong>
                                                    </option>
                                                    <option value=" Child Model" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Child Model</strong>
                                                    </option>
                                                    <option value="Pro Model" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Pro Model</strong>
                                                    </option>
                                                    <option value="Dancer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dancer</strong>
                                                    </option>
                                                    <option value="Lead Dancer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lead Dancer</strong>
                                                    </option>
                                                    <option value="Background Dancer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Background Dancer</strong>
                                                    </option>
                                                    <option value="Video Editor" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Editor</strong>
                                                    </option>
                                                    <option value="Music Editor" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Editor</strong>
                                                    </option>
                                                    <option value="Audio Editor" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Editor</strong>
                                                    </option>
                                                    <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dialogue Writer</strong>
                                                    </option>
                                                    <option value="Script Writer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Script Writer</strong>
                                                    </option>
                                                    <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Screeplay Writer</strong>
                                                    </option>
                                                    <option value="Story Writer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Story Writer</strong>
                                                    </option>
                                                    <option value="Lyricists" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lyricists</strong>
                                                    </option>
                                                    //video vj
                                                    <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Video Jockey (VJ)</strong>
                                                    </option>
                                                    <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Radio Jockey (RJ)</strong>
                                                    </option>
                                                    <option value=" News Reader" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> News Reader</strong>
                                                    </option>
                                                    <option value="Reporter" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Reporter</strong>
                                                    </option>
                                                    <option value="Journalist" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Journalist</strong>
                                                    </option>
                                                    <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dubbing Artist</strong>
                                                    </option>
                                                    <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voice Over Artist</strong>
                                                    </option>
                                                    <option value="New Influncer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>New Influncer</strong>
                                                    </option>
                                                    <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Experienced Influncer</strong>
                                                    </option>
                                                    <option value="Video Advertising" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Advertising</strong>
                                                    </option>
                                                    <option value="Audio Advertising" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Advertising</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Banner Advertising" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Banner Advertising</strong>
                                                    </option>
                                                    <option value="Singer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Singer</strong>
                                                    </option>
                                                    <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Instrumentalist</strong>
                                                    </option>
                                                    <option value="Music Composer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Composer</strong>
                                                    </option>
                                                    <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Director Of Photography / Cinematographer</strong>
                                                    </option>
                                                    <option value="Cameraman" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Cameraman</strong>
                                                    </option>
                                                    <option value="Camera Operator" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Camera Operator</strong>
                                                    </option>
                                                    <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Light man / Gaffer</strong>
                                                    </option>
                                                    <option value="Makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artist</strong>
                                                    </option>
                                                    <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Visual Effects Editor</strong>
                                                    </option>
                                                    <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Digital imaging technician</strong>
                                                    </option>
                                                    <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Special Effects makeup Artist</strong>
                                                    </option>
                                                    <option value="Motion control technician" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Motion control technician</strong>
                                                    </option>
                                                    <option value="Fashion Designer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Fashion Designer</strong>
                                                    </option>
                                                    <option value="Hair Stylist" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Hair Stylist</strong>
                                                    </option>
                                                    <option value="Costume designer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Costume designer</strong>
                                                    </option>
                                                    <option value="Grip" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Grip</strong>
                                                    </option>
                                                    <option value="Sound Designer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Designer</strong>
                                                    </option>
                                                    <option value="Sound Grip" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Grip</strong>
                                                    </option>
                                                    <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Sound Mixer</strong>
                                                    </option>
                                                    <option value="Production Designer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Designer</strong>
                                                    </option>
                                                    <option value="Green man" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Green man</strong>
                                                    </option>
                                                    <option value="Property master" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Property master</strong>
                                                    </option>
                                                    <option value="Weapons master" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Weapons master</strong>
                                                    </option>
                                                    <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Set Designer / Decorator</strong>
                                                    </option>
                                                    <option value="Location Manager" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Location Manager</strong>
                                                    </option>
                                                    <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>StoryBoard Artist</strong>
                                                    </option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ?
                                                        (($getIfAlreadyGallery[11]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Others</strong>
                                                    </option>
                                                              
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="showPhotoUploader('12')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                    <input type="file" class="d-none" id="photosectionphoto12" name="photosectionphoto11">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img src="<?php echo ((count($getIfAlreadyGallery) >= 12 ? (($getIfAlreadyGallery[11]->photo != NULL || $getIfAlreadyGallery[11]->photo != '') ? $getIfAlreadyGallery[11]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 12)
                                                        {
                                                            if($getIfAlreadyGallery[11]->photo != NULL || $getIfAlreadyGallery[11]->photo != '')
                                                            {
                                                        ?>
                                                    <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[11]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="btn-save">
                        <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyGallery) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="video" class="tabcontent" >
            <div id="portfolio-block" style="background:#f4f3f3; margin-top:10px">
                <div class="container">
                    <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <h2>Video</h2>
                        <p>You can add 6 Videos (Set of 3)</p>
                        Image width:370px height:310px 
                        <div class="col-lg-12">
                            <form action="{{URL::to('/save-video')}}" method="post" enctype="multipart/form-data" id="videoform">
                                <?php 
                                    $userId = session()->get('user_id');
                                    $getIfAlreadyVideo = DB::select('select * from artist_video where user_id='.$userId);
                                    ?>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->id != NULL) ? $getIfAlreadyVideo[0]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->video != NULL) ? $getIfAlreadyVideo[0]->video : '') : '')); ?>" />
                                <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->thumb != NULL) ? $getIfAlreadyVideo[0]->thumb : '') : '')); ?>" />
                                <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->id != NULL) ? $getIfAlreadyVideo[1]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->video != NULL) ? $getIfAlreadyVideo[1]->video : '') : '')); ?>" />
                                <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->thumb != NULL) ? $getIfAlreadyVideo[1]->thumb : '') : '')); ?>" />
                                <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->id != NULL) ? $getIfAlreadyVideo[2]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->video != NULL) ? $getIfAlreadyVideo[2]->video : '') : '')); ?>" />
                                <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->thumb != NULL) ? $getIfAlreadyVideo[2]->thumb : '') : '')); ?>" />
                                <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->id != NULL) ? $getIfAlreadyVideo[3]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->video != NULL) ? $getIfAlreadyVideo[3]->video : '') : '')); ?>" />
                                <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->thumb != NULL) ? $getIfAlreadyVideo[3]->thumb : '') : '')); ?>" />
                                <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->id != NULL) ? $getIfAlreadyVideo[4]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->video != NULL) ? $getIfAlreadyVideo[4]->video : '') : '')); ?>" />
                                <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->thumb != NULL) ? $getIfAlreadyVideo[4]->thumb : '') : '')); ?>" />
                                <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->id != NULL) ? $getIfAlreadyVideo[5]->id : '') : '')); ?>" />
                                <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->video != NULL) ? $getIfAlreadyVideo[5]->video : '') : '')); ?>" />
                                <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->thumb != NULL) ? $getIfAlreadyVideo[5]->thumb : '') : '')); ?>" />
                                <div class="u-form">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                                <label for="e1">Video Title - Position 1</label>
                                                <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->video_title != NULL) ? $getIfAlreadyVideo[0]->video_title : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6" style="padding:0px;">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Voiceover" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voiceover</strong>
                                                    </option>
                                                    <option value="Comedian" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedian</strong>
                                                    </option>
                                                    <option value=" Photography" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Photography</strong>
                                                    </option>
                                                    <option value="Video Grapher" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Grapher</strong>
                                                    </option>
                                                    <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artrist</strong>
                                                    </option>
                                                    <option value="Actor" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Actor</strong>
                                                    </option>
                                                    <option value="Child Actor" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Child Actor</strong>
                                                    </option>
                                                    <option value="Comedy Actor" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedy Actor</strong>
                                                    </option>
                                                    <option value=" Junior Actor" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Junior Actor</strong>
                                                    </option>
                                                    <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Stunt Artist</strong>
                                                    </option>
                                                    <option value=" Drama Artist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Drama Artist</strong>
                                                    </option>
                                                    <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Mimicry Artist</strong>
                                                    </option>
                                                    <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Background / Atmosphere</strong>
                                                    </option>
                                                    <option value="Model" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Model</strong>
                                                    </option>
                                                    <option value=" Child Model" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Child Model</strong>
                                                    </option>
                                                    <option value="Pro Model" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Pro Model</strong>
                                                    </option>
                                                    <option value="Dancer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dancer</strong>
                                                    </option>
                                                    <option value="Lead Dancer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lead Dancer</strong>
                                                    </option>
                                                    <option value="Background Dancer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Background Dancer</strong>
                                                    </option>
                                                    <option value="Video Editor" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Editor</strong>
                                                    </option>
                                                    <option value="Music Editor" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Editor</strong>
                                                    </option>
                                                    <option value="Audio Editor" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Editor</strong>
                                                    </option>
                                                    <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dialogue Writer</strong>
                                                    </option>
                                                    <option value="Script Writer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Script Writer</strong>
                                                    </option>
                                                    <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Screeplay Writer</strong>
                                                    </option>
                                                    <option value="Story Writer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Story Writer</strong>
                                                    </option>
                                                    <option value="Lyricists" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lyricists</strong>
                                                    </option>
                                                    //video vj
                                                    <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Video Jockey (VJ)</strong>
                                                    </option>
                                                    <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Radio Jockey (RJ)</strong>
                                                    </option>
                                                    <option value=" News Reader" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> News Reader</strong>
                                                    </option>
                                                    <option value="Reporter" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Reporter</strong>
                                                    </option>
                                                    <option value="Journalist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Journalist</strong>
                                                    </option>
                                                    <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dubbing Artist</strong>
                                                    </option>
                                                    <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voice Over Artist</strong>
                                                    </option>
                                                    <option value="New Influncer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>New Influncer</strong>
                                                    </option>
                                                    <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Experienced Influncer</strong>
                                                    </option>
                                                    <option value="Video Advertising" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Advertising</strong>
                                                    </option>
                                                    <option value="Audio Advertising" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Advertising</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Banner Advertising" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Banner Advertising</strong>
                                                    </option>
                                                    <option value="Singer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Singer</strong>
                                                    </option>
                                                    <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Instrumentalist</strong>
                                                    </option>
                                                    <option value="Music Composer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Composer</strong>
                                                    </option>
                                                    <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Director Of Photography / Cinematographer</strong>
                                                    </option>
                                                    <option value="Cameraman" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Cameraman</strong>
                                                    </option>
                                                    <option value="Camera Operator" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Camera Operator</strong>
                                                    </option>
                                                    <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Light man / Gaffer</strong>
                                                    </option>
                                                    <option value="Makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artist</strong>
                                                    </option>
                                                    <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Visual Effects Editor</strong>
                                                    </option>
                                                    <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Digital imaging technician</strong>
                                                    </option>
                                                    <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Special Effects makeup Artist</strong>
                                                    </option>
                                                    <option value="Motion control technician" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Motion control technician</strong>
                                                    </option>
                                                    <option value="Fashion Designer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Fashion Designer</strong>
                                                    </option>
                                                    <option value="Hair Stylist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Hair Stylist</strong>
                                                    </option>
                                                    <option value="Costume designer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Costume designer</strong>
                                                    </option>
                                                    <option value="Grip" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Grip</strong>
                                                    </option>
                                                    <option value="Sound Designer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Designer</strong>
                                                    </option>
                                                    <option value="Sound Grip" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Grip</strong>
                                                    </option>
                                                    <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Sound Mixer</strong>
                                                    </option>
                                                    <option value="Production Designer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Designer</strong>
                                                    </option>
                                                    <option value="Green man" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Green man</strong>
                                                    </option>
                                                    <option value="Property master" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Property master</strong>
                                                    </option>
                                                    <option value="Weapons master" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Weapons master</strong>
                                                    </option>
                                                    <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Set Designer / Decorator</strong>
                                                    </option>
                                                    <option value="Location Manager" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Location Manager</strong>
                                                    </option>
                                                    <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>StoryBoard Artist</strong>
                                                    </option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ?
                                                        (($getIfAlreadyVideo[0]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Others</strong>
                                                    </option>
                                                              
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="e1">Video Description </label>
                                                <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->description != NULL) ? $getIfAlreadyVideo[0]->description : '') : '')); ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="uploadVideo('1')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                    <input type="file" class="d-none" id="videoFile1">
                                                </div>
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="uploadVideoThumbnail('1')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                    <input type="file" class="d-none" id="videoThumb1" name="videoThumbFile[]">
                                                </div>
                                                <input type="hidden" name="uploadedvideosec1" id="uploadedvideosec1">
                                                <input type="hidden" name="generatedvideosecthumb1" id="generatedvideosecthumb1">
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img id="previewvideosec1" src="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->thumb != NULL || $getIfAlreadyVideo[0]->thumb != '') ? $getIfAlreadyVideo[0]->thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyVideo) >= 1)
                                                        {
                                                            if($getIfAlreadyVideo[0]->thumb != NULL || $getIfAlreadyVideo[0]->thumb != '')
                                                            {
                                                        ?>
                                                    <a href="<?php echo URL::to('/delete-video-image/'.$getIfAlreadyVideo[0]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                                <div class="u-progress1" style="display:none;">
                                                    <div class="row">
                                                        <div class="col-lg-12 ud-caption">Upload Details</div>
                                                        <div class="col-lg-10">
                                                            <div class="u-title"></div>
                                                            <div id="upload-progress1">
                                                                <div class="u-size1"></div>
                                                                <div class="u-progress1">
                                                                    <div class="progress1">
                                                                        <div class="progress-bar1" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                            <span class="sr-only1">0% Complete</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="u-close">
                                                                        <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="u-desc1"><span class="percent-process1"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--<div class="u-progress1" style="display:none;">-->
                                                <!--   <div class="progress">-->
                                                <!--      <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width: 35%;">-->
                                                <!--         <span class="sr-only">35% Complete</span>-->
                                                <!--      </div>-->
                                                <!--   </div>-->
                                                <!--   <div class="u-close cgt">-->
                                                <!--      <a href="#"><i class="cvicon-cv-cancel"></i></a>-->
                                                <!--   </div>-->
                                                <!--</div>-->
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                                <label for="e1">Video Title - Position 2</label>
                                                <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->video_title != NULL) ? $getIfAlreadyVideo[1]->video_title : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6" style="padding:0px;">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Voiceover" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voiceover</strong>
                                                    </option>
                                                    <option value="Comedian" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedian</strong>
                                                    </option>
                                                    <option value=" Photography" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Photography</strong>
                                                    </option>
                                                    <option value="Video Grapher" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Grapher</strong>
                                                    </option>
                                                    <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artrist</strong>
                                                    </option>
                                                    <option value="Actor" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Actor</strong>
                                                    </option>
                                                    <option value="Child Actor" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Child Actor</strong>
                                                    </option>
                                                    <option value="Comedy Actor" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Comedy Actor</strong>
                                                    </option>
                                                    <option value=" Junior Actor" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Junior Actor</strong>
                                                    </option>
                                                    <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Stunt Artist</strong>
                                                    </option>
                                                    <option value=" Drama Artist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Drama Artist</strong>
                                                    </option>
                                                    <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Mimicry Artist</strong>
                                                    </option>
                                                    <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Background / Atmosphere</strong>
                                                    </option>
                                                    <option value="Model" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Model</strong>
                                                    </option>
                                                    <option value=" Child Model" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Child Model</strong>
                                                    </option>
                                                    <option value="Pro Model" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Pro Model</strong>
                                                    </option>
                                                    <option value="Dancer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dancer</strong>
                                                    </option>
                                                    <option value="Lead Dancer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lead Dancer</strong>
                                                    </option>
                                                    <option value="Background Dancer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Background Dancer</strong>
                                                    </option>
                                                    <option value="Video Editor" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Editor</strong>
                                                    </option>
                                                    <option value="Music Editor" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Editor</strong>
                                                    </option>
                                                    <option value="Audio Editor" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Editor</strong>
                                                    </option>
                                                    <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dialogue Writer</strong>
                                                    </option>
                                                    <option value="Script Writer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Script Writer</strong>
                                                    </option>
                                                    <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Screeplay Writer</strong>
                                                    </option>
                                                    <option value="Story Writer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Story Writer</strong>
                                                    </option>
                                                    <option value="Lyricists" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Lyricists</strong>
                                                    </option>
                                                    //video vj
                                                    <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Video Jockey (VJ)</strong>
                                                    </option>
                                                    <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> Radio Jockey (RJ)</strong>
                                                    </option>
                                                    <option value=" News Reader" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong> News Reader</strong>
                                                    </option>
                                                    <option value="Reporter" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Reporter</strong>
                                                    </option>
                                                    <option value="Journalist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Journalist</strong>
                                                    </option>
                                                    <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Dubbing Artist</strong>
                                                    </option>
                                                    <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Voice Over Artist</strong>
                                                    </option>
                                                    <option value="New Influncer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>New Influncer</strong>
                                                    </option>
                                                    <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Experienced Influncer</strong>
                                                    </option>
                                                    <option value="Video Advertising" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Video Advertising</strong>
                                                    </option>
                                                    <option value="Audio Advertising" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Audio Advertising</strong>
                                                    </option>
                                                    <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Threater</strong>
                                                    </option>
                                                    <option value="Banner Advertising" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Banner Advertising</strong>
                                                    </option>
                                                    <option value="Singer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Singer</strong>
                                                    </option>
                                                    <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Instrumentalist</strong>
                                                    </option>
                                                    <option value="Music Composer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Music Composer</strong>
                                                    </option>
                                                    <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Director Of Photography / Cinematographer</strong>
                                                    </option>
                                                    <option value="Cameraman" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Cameraman</strong>
                                                    </option>
                                                    <option value="Camera Operator" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Camera Operator</strong>
                                                    </option>
                                                    <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Light man / Gaffer</strong>
                                                    </option>
                                                    <option value="Makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Makeup Artist</strong>
                                                    </option>
                                                    <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Visual Effects Editor</strong>
                                                    </option>
                                                    <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Digital imaging technician</strong>
                                                    </option>
                                                    <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Special Effects makeup Artist</strong>
                                                    </option>
                                                    <option value="Motion control technician" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Motion control technician</strong>
                                                    </option>
                                                    <option value="Fashion Designer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Fashion Designer</strong>
                                                    </option>
                                                    <option value="Hair Stylist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Hair Stylist</strong>
                                                    </option>
                                                    <option value="Costume designer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Costume designer</strong>
                                                    </option>
                                                    <option value="Grip" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Grip</strong>
                                                    </option>
                                                    <option value="Sound Designer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Designer</strong>
                                                    </option>
                                                    <option value="Sound Grip" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Sound Grip</strong>
                                                    </option>
                                                    <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Sound Mixer</strong>
                                                    </option>
                                                    <option value="Production Designer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Production Designer</strong>
                                                    </option>
                                                    <option value="Green man" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Green man</strong>
                                                    </option>
                                                    <option value="Property master" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Property master</strong>
                                                    </option>
                                                    <option value="Weapons master" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Weapons master</strong>
                                                    </option>
                                                    <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Set Designer / Decorator</strong>
                                                    </option>
                                                    <option value="Location Manager" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Location Manager</strong>
                                                    </option>
                                                    <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>StoryBoard Artist</strong>
                                                    </option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ?
                                                        (($getIfAlreadyVideo[1]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                        <strong>Others</strong>
                                                    </option>
                                                              
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="e1">Video Description </label>
                                                <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->description != NULL) ? $getIfAlreadyVideo[1]->description : '') : '')); ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="uploadVideo('2')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                    <input type="file" class="d-none" id="videoFile2">
                                                </div>
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="uploadVideoThumbnail('2')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                    <input type="file" class="d-none" id="videoThumb2" name="videoThumbFile[]">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img src="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->thumb != NULL || $getIfAlreadyVideo[1]->thumb != '') ? $getIfAlreadyVideo[1]->thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyVideo) >= 2)
                                                        {
                                                            if($getIfAlreadyVideo[1]->thumb != NULL || $getIfAlreadyVideo[1]->thumb != '')
                                                            {
                                                        ?>
                                                    <a href="<?php echo URL::to('/delete-video-image/'.$getIfAlreadyVideo[1]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                                <input type="hidden" name="uploadedvideosec2" id="uploadedvideosec2">
                                                <input type="hidden" name="generatedvideosecthumb2" id="generatedvideosecthumb2">
                                                <div class="u-progress2" style="display:none;">
                                                    <div class="row">
                                                        <div class="col-lg-12 ud-caption">Upload Details</div>
                                                        <div class="col-lg-10">
                                                            <div class="u-title"></div>
                                                            <div id="upload-progress2">
                                                                <div class="u-size2"></div>
                                                                <div class="u-progress2">
                                                                    <div class="progress2">
                                                                        <div class="progress-bar2" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                            <span class="sr-only2">0% Complete</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="u-close">
                                                                        <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="u-desc2"><span class="percent-process2"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                            <label for="e1">Video Title - Position 3</label>
                                            <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->video_title != NULL) ? $getIfAlreadyVideo[2]->video_title : '') : '')); ?>">
                                        </div>
                                        <div class="form-group col-lg-6" style="padding:0px;">
                                            <label for="e1">Select category</label>
                                            <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                                <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Threater</strong>
                                                </option>
                                                <option value="Voiceover" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Voiceover</strong>
                                                </option>
                                                <option value="Comedian" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Comedian</strong>
                                                </option>
                                                <option value=" Photography" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Photography</strong>
                                                </option>
                                                <option value="Video Grapher" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Grapher</strong>
                                                </option>
                                                <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Makeup Artrist</strong>
                                                </option>
                                                <option value="Actor" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Actor</strong>
                                                </option>
                                                <option value="Child Actor" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Child Actor</strong>
                                                </option>
                                                <option value="Comedy Actor" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Comedy Actor</strong>
                                                </option>
                                                <option value=" Junior Actor" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Junior Actor</strong>
                                                </option>
                                                <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Stunt Artist</strong>
                                                </option>
                                                <option value=" Drama Artist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Drama Artist</strong>
                                                </option>
                                                <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Mimicry Artist</strong>
                                                </option>
                                                <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Background / Atmosphere</strong>
                                                </option>
                                                <option value="Model" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Model</strong>
                                                </option>
                                                <option value=" Child Model" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Child Model</strong>
                                                </option>
                                                <option value="Pro Model" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Pro Model</strong>
                                                </option>
                                                <option value="Dancer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dancer</strong>
                                                </option>
                                                <option value="Lead Dancer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Lead Dancer</strong>
                                                </option>
                                                <option value="Background Dancer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Background Dancer</strong>
                                                </option>
                                                <option value="Video Editor" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Editor</strong>
                                                </option>
                                                <option value="Music Editor" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Editor</strong>
                                                </option>
                                                <option value="Audio Editor" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Audio Editor</strong>
                                                </option>
                                                <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dialogue Writer</strong>
                                                </option>
                                                <option value="Script Writer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Script Writer</strong>
                                                </option>
                                                <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Screeplay Writer</strong>
                                                </option>
                                                <option value="Story Writer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Story Writer</strong>
                                                </option>
                                                <option value="Lyricists" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Lyricists</strong>
                                                </option>
                                                //video vj
                                                <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Video Jockey (VJ)</strong>
                                                </option>
                                                <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> Radio Jockey (RJ)</strong>
                                                </option>
                                                <option value=" News Reader" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong> News Reader</strong>
                                                </option>
                                                <option value="Reporter" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Reporter</strong>
                                                </option>
                                                <option value="Journalist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Journalist</strong>
                                                </option>
                                                <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Dubbing Artist</strong>
                                                </option>
                                                <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Voice Over Artist</strong>
                                                </option>
                                                <option value="New Influncer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>New Influncer</strong>
                                                </option>
                                                <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Experienced Influncer</strong>
                                                </option>
                                                <option value="Video Advertising" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Video Advertising</strong>
                                                </option>
                                                <option value="Audio Advertising" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Audio Advertising</strong>
                                                </option>
                                                <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Threater</strong>
                                                </option>
                                                <option value="Banner Advertising" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Banner Advertising</strong>
                                                </option>
                                                <option value="Singer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Singer</strong>
                                                </option>
                                                <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Instrumentalist</strong>
                                                </option>
                                                <option value="Music Composer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Music Composer</strong>
                                                </option>
                                                <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Director Of Photography / Cinematographer</strong>
                                                </option>
                                                <option value="Cameraman" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Cameraman</strong>
                                                </option>
                                                <option value="Camera Operator" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Camera Operator</strong>
                                                </option>
                                                <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Light man / Gaffer</strong>
                                                </option>
                                                <option value="Makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Makeup Artist</strong>
                                                </option>
                                                <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Visual Effects Editor</strong>
                                                </option>
                                                <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Digital imaging technician</strong>
                                                </option>
                                                <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Special Effects makeup Artist</strong>
                                                </option>
                                                <option value="Motion control technician" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Motion control technician</strong>
                                                </option>
                                                <option value="Fashion Designer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Fashion Designer</strong>
                                                </option>
                                                <option value="Hair Stylist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Hair Stylist</strong>
                                                </option>
                                                <option value="Costume designer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Costume designer</strong>
                                                </option>
                                                <option value="Grip" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Grip</strong>
                                                </option>
                                                <option value="Sound Designer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Sound Designer</strong>
                                                </option>
                                                <option value="Sound Grip" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Sound Grip</strong>
                                                </option>
                                                <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Production Sound Mixer</strong>
                                                </option>
                                                <option value="Production Designer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Production Designer</strong>
                                                </option>
                                                <option value="Green man" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Green man</strong>
                                                </option>
                                                <option value="Property master" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Property master</strong>
                                                </option>
                                                <option value="Weapons master" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Weapons master</strong>
                                                </option>
                                                <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Set Designer / Decorator</strong>
                                                </option>
                                                <option value="Location Manager" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Location Manager</strong>
                                                </option>
                                                <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>StoryBoard Artist</strong>
                                                </option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ?
                                                    (($getIfAlreadyVideo[2]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                    <strong>Others</strong>
                                                </option>
                                                          
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="e1">Video Description </label>
                                            <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->description != NULL) ? $getIfAlreadyVideo[2]->description : '') : '')); ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                <div class="browse-photo" onclick="uploadVideo('3')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                                </div>
                                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                <input type="file" class="d-none" id="videoFile3">
                                            </div>
                                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                <div class="browse-photo" onclick="uploadVideoThumbnail('3')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                </div>
                                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                <input type="file" class="d-none" id="videoThumb3" name="videoThumbFile[]">
                                            </div>
                                            <div class="u-close" style="float:left; margin-right:20px;">
                                                <img src="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->thumb != NULL || $getIfAlreadyVideo[2]->thumb != '') ? $getIfAlreadyVideo[2]->thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                <?php 
                                                    if(count($getIfAlreadyVideo) >= 3)
                                                    {
                                                        if($getIfAlreadyVideo[2]->thumb != NULL || $getIfAlreadyVideo[2]->thumb != '')
                                                        {
                                                    ?>
                                                <a href="<?php echo URL::to('/delete-video-image/'.$getIfAlreadyVideo[2]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                                </a>
                                                <?php } } ?>
                                            </div>
                                            <div style="clear:both; overflow:hidden"></div>
                                            <input type="hidden" name="uploadedvideosec3" id="uploadedvideosec3">
                                            <input type="hidden" name="generatedvideosecthumb3" id="generatedvideosecthumb3">
                                            <div class="u-progress3" style="display:none;">
                                                <div class="row">
                                                    <div class="col-lg-12 ud-caption">Upload Details</div>
                                                    <div class="col-lg-10">
                                                        <div class="u-title"></div>
                                                        <div id="upload-progress3">
                                                            <div class="u-size3"></div>
                                                            <div class="u-progress3">
                                                                <div class="progress3">
                                                                    <div class="progress-bar3" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                        <span class="sr-only3">0% Complete</span>
                                                                    </div>
                                                                </div>
                                                                <div class="u-close">
                                                                    <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="u-desc3"><span class="percent-process3"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0)" id="videoAdd" onclick="addMoreVideo()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Add More 3 Video grid</a>
                                    <a href="javascript:void(0)" id="videoRemove" class="d-none" onclick="removeMoreVideo()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Remove Video grid</a>
                                    <div id="clonevideodiv" class="d-none">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                                    <label for="e1">Video Title - Position 4</label>
                                                    <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->video_title != NULL) ? $getIfAlreadyVideo[3]->video_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6" style="padding:0px;">
                                                    <label for="e1">Select category</label>
                                                    <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                                        <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Threater</strong>
                                                        </option>
                                                        <option value="Voiceover" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Voiceover</strong>
                                                        </option>
                                                        <option value="Comedian" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Comedian</strong>
                                                        </option>
                                                        <option value=" Photography" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Photography</strong>
                                                        </option>
                                                        <option value="Video Grapher" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Video Grapher</strong>
                                                        </option>
                                                        <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Makeup Artrist</strong>
                                                        </option>
                                                        <option value="Actor" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Actor</strong>
                                                        </option>
                                                        <option value="Child Actor" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Child Actor</strong>
                                                        </option>
                                                        <option value="Comedy Actor" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Comedy Actor</strong>
                                                        </option>
                                                        <option value=" Junior Actor" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Junior Actor</strong>
                                                        </option>
                                                        <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Stunt Artist</strong>
                                                        </option>
                                                        <option value=" Drama Artist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Drama Artist</strong>
                                                        </option>
                                                        <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Mimicry Artist</strong>
                                                        </option>
                                                        <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Background / Atmosphere</strong>
                                                        </option>
                                                        <option value="Model" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Model</strong>
                                                        </option>
                                                        <option value=" Child Model" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Child Model</strong>
                                                        </option>
                                                        <option value="Pro Model" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Pro Model</strong>
                                                        </option>
                                                        <option value="Dancer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Dancer</strong>
                                                        </option>
                                                        <option value="Lead Dancer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Lead Dancer</strong>
                                                        </option>
                                                        <option value="Background Dancer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Background Dancer</strong>
                                                        </option>
                                                        <option value="Video Editor" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Video Editor</strong>
                                                        </option>
                                                        <option value="Music Editor" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Music Editor</strong>
                                                        </option>
                                                        <option value="Audio Editor" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Audio Editor</strong>
                                                        </option>
                                                        <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Dialogue Writer</strong>
                                                        </option>
                                                        <option value="Script Writer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Script Writer</strong>
                                                        </option>
                                                        <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Screeplay Writer</strong>
                                                        </option>
                                                        <option value="Story Writer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Story Writer</strong>
                                                        </option>
                                                        <option value="Lyricists" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Lyricists</strong>
                                                        </option>
                                                        //video vj
                                                        <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Video Jockey (VJ)</strong>
                                                        </option>
                                                        <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Radio Jockey (RJ)</strong>
                                                        </option>
                                                        <option value=" News Reader" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> News Reader</strong>
                                                        </option>
                                                        <option value="Reporter" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Reporter</strong>
                                                        </option>
                                                        <option value="Journalist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Journalist</strong>
                                                        </option>
                                                        <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Dubbing Artist</strong>
                                                        </option>
                                                        <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Voice Over Artist</strong>
                                                        </option>
                                                        <option value="New Influncer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>New Influncer</strong>
                                                        </option>
                                                        <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Experienced Influncer</strong>
                                                        </option>
                                                        <option value="Video Advertising" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Video Advertising</strong>
                                                        </option>
                                                        <option value="Audio Advertising" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Audio Advertising</strong>
                                                        </option>
                                                        <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Threater</strong>
                                                        </option>
                                                        <option value="Banner Advertising" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Banner Advertising</strong>
                                                        </option>
                                                        <option value="Singer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Singer</strong>
                                                        </option>
                                                        <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Music Instrumentalist</strong>
                                                        </option>
                                                        <option value="Music Composer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Music Composer</strong>
                                                        </option>
                                                        <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Director Of Photography / Cinematographer</strong>
                                                        </option>
                                                        <option value="Cameraman" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Cameraman</strong>
                                                        </option>
                                                        <option value="Camera Operator" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Camera Operator</strong>
                                                        </option>
                                                        <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Light man / Gaffer</strong>
                                                        </option>
                                                        <option value="Makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Makeup Artist</strong>
                                                        </option>
                                                        <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Visual Effects Editor</strong>
                                                        </option>
                                                        <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Digital imaging technician</strong>
                                                        </option>
                                                        <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Special Effects makeup Artist</strong>
                                                        </option>
                                                        <option value="Motion control technician" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Motion control technician</strong>
                                                        </option>
                                                        <option value="Fashion Designer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Fashion Designer</strong>
                                                        </option>
                                                        <option value="Hair Stylist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Hair Stylist</strong>
                                                        </option>
                                                        <option value="Costume designer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Costume designer</strong>
                                                        </option>
                                                        <option value="Grip" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Grip</strong>
                                                        </option>
                                                        <option value="Sound Designer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Sound Designer</strong>
                                                        </option>
                                                        <option value="Sound Grip" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Sound Grip</strong>
                                                        </option>
                                                        <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Production Sound Mixer</strong>
                                                        </option>
                                                        <option value="Production Designer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Production Designer</strong>
                                                        </option>
                                                        <option value="Green man" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Green man</strong>
                                                        </option>
                                                        <option value="Property master" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Property master</strong>
                                                        </option>
                                                        <option value="Weapons master" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Weapons master</strong>
                                                        </option>
                                                        <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Set Designer / Decorator</strong>
                                                        </option>
                                                        <option value="Location Manager" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Location Manager</strong>
                                                        </option>
                                                        <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>StoryBoard Artist</strong>
                                                        </option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ?
                                                            (($getIfAlreadyVideo[3]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Others</strong>
                                                        </option>
                                                                  
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="e1">Video Description </label>
                                                    <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->description != NULL) ? $getIfAlreadyVideo[3]->description : '') : '')); ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                        <div class="browse-photo" onclick="uploadVideo('4')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                        <input type="file" class="d-none" id="videoFile4">
                                                    </div>
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                        <div class="browse-photo" onclick="uploadVideoThumbnail('4')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                        <input type="file" class="d-none" id="videoThumb4" name="videoThumbFile[]">
                                                    </div>
                                                    <div class="u-close" style="float:left; margin-right:20px;">
                                                        <img src="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->thumb != NULL || $getIfAlreadyVideo[3]->thumb != '') ? $getIfAlreadyVideo[3]->thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                        <?php 
                                                            if(count($getIfAlreadyVideo) >= 4)
                                                            {
                                                                if($getIfAlreadyVideo[3]->thumb != NULL || $getIfAlreadyVideo[3]->thumb != '')
                                                                {
                                                            ?>
                                                        <a href="<?php echo URL::to('/delete-video-image/'.$getIfAlreadyVideo[3]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                        </a>
                                                        <?php } } ?>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>
                                                    <input type="hidden" name="uploadedvideosec4" id="uploadedvideosec2">
                                                    <input type="hidden" name="generatedvideosecthumb4" id="generatedvideosecthumb2">
                                                    <div class="u-progress4" style="display:none;">
                                                        <div class="row">
                                                            <div class="col-lg-12 ud-caption">Upload Details</div>
                                                            <div class="col-lg-10">
                                                                <div class="u-title"></div>
                                                                <div id="upload-progress4">
                                                                    <div class="u-size4"></div>
                                                                    <div class="u-progress4">
                                                                        <div class="progress4">
                                                                            <div class="progress-bar4" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                                <span class="sr-only4">0% Complete</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="u-close">
                                                                            <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="u-desc4"><span class="percent-process4"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                                    <label for="e1">Video Title - Position 5</label>
                                                    <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->video_title != NULL) ? $getIfAlreadyVideo[4]->video_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6" style="padding:0px;">
                                                    <label for="e1">Select category</label>
                                                    <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                                        <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Threater</strong>
                                                        </option>
                                                        <option value="Voiceover" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Voiceover</strong>
                                                        </option>
                                                        <option value="Comedian" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Comedian</strong>
                                                        </option>
                                                        <option value=" Photography" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Photography</strong>
                                                        </option>
                                                        <option value="Video Grapher" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Video Grapher</strong>
                                                        </option>
                                                        <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Makeup Artrist</strong>
                                                        </option>
                                                        <option value="Actor" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Actor</strong>
                                                        </option>
                                                        <option value="Child Actor" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Child Actor</strong>
                                                        </option>
                                                        <option value="Comedy Actor" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Comedy Actor</strong>
                                                        </option>
                                                        <option value=" Junior Actor" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Junior Actor</strong>
                                                        </option>
                                                        <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Stunt Artist</strong>
                                                        </option>
                                                        <option value=" Drama Artist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Drama Artist</strong>
                                                        </option>
                                                        <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Mimicry Artist</strong>
                                                        </option>
                                                        <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Background / Atmosphere</strong>
                                                        </option>
                                                        <option value="Model" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Model</strong>
                                                        </option>
                                                        <option value=" Child Model" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Child Model</strong>
                                                        </option>
                                                        <option value="Pro Model" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Pro Model</strong>
                                                        </option>
                                                        <option value="Dancer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Dancer</strong>
                                                        </option>
                                                        <option value="Lead Dancer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Lead Dancer</strong>
                                                        </option>
                                                        <option value="Background Dancer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Background Dancer</strong>
                                                        </option>
                                                        <option value="Video Editor" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Video Editor</strong>
                                                        </option>
                                                        <option value="Music Editor" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Music Editor</strong>
                                                        </option>
                                                        <option value="Audio Editor" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Audio Editor</strong>
                                                        </option>
                                                        <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Dialogue Writer</strong>
                                                        </option>
                                                        <option value="Script Writer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Script Writer</strong>
                                                        </option>
                                                        <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Screeplay Writer</strong>
                                                        </option>
                                                        <option value="Story Writer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Story Writer</strong>
                                                        </option>
                                                        <option value="Lyricists" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Lyricists</strong>
                                                        </option>
                                                        //video vj
                                                        <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Video Jockey (VJ)</strong>
                                                        </option>
                                                        <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Radio Jockey (RJ)</strong>
                                                        </option>
                                                        <option value=" News Reader" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> News Reader</strong>
                                                        </option>
                                                        <option value="Reporter" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Reporter</strong>
                                                        </option>
                                                        <option value="Journalist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Journalist</strong>
                                                        </option>
                                                        <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Dubbing Artist</strong>
                                                        </option>
                                                        <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Voice Over Artist</strong>
                                                        </option>
                                                        <option value="New Influncer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>New Influncer</strong>
                                                        </option>
                                                        <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Experienced Influncer</strong>
                                                        </option>
                                                        <option value="Video Advertising" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Video Advertising</strong>
                                                        </option>
                                                        <option value="Audio Advertising" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Audio Advertising</strong>
                                                        </option>
                                                        <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Threater</strong>
                                                        </option>
                                                        <option value="Banner Advertising" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Banner Advertising</strong>
                                                        </option>
                                                        <option value="Singer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Singer</strong>
                                                        </option>
                                                        <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Music Instrumentalist</strong>
                                                        </option>
                                                        <option value="Music Composer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Music Composer</strong>
                                                        </option>
                                                        <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Director Of Photography / Cinematographer</strong>
                                                        </option>
                                                        <option value="Cameraman" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Cameraman</strong>
                                                        </option>
                                                        <option value="Camera Operator" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Camera Operator</strong>
                                                        </option>
                                                        <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Light man / Gaffer</strong>
                                                        </option>
                                                        <option value="Makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Makeup Artist</strong>
                                                        </option>
                                                        <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Visual Effects Editor</strong>
                                                        </option>
                                                        <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Digital imaging technician</strong>
                                                        </option>
                                                        <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Special Effects makeup Artist</strong>
                                                        </option>
                                                        <option value="Motion control technician" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Motion control technician</strong>
                                                        </option>
                                                        <option value="Fashion Designer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Fashion Designer</strong>
                                                        </option>
                                                        <option value="Hair Stylist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Hair Stylist</strong>
                                                        </option>
                                                        <option value="Costume designer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Costume designer</strong>
                                                        </option>
                                                        <option value="Grip" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Grip</strong>
                                                        </option>
                                                        <option value="Sound Designer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Sound Designer</strong>
                                                        </option>
                                                        <option value="Sound Grip" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Sound Grip</strong>
                                                        </option>
                                                        <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Production Sound Mixer</strong>
                                                        </option>
                                                        <option value="Production Designer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Production Designer</strong>
                                                        </option>
                                                        <option value="Green man" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Green man</strong>
                                                        </option>
                                                        <option value="Property master" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Property master</strong>
                                                        </option>
                                                        <option value="Weapons master" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Weapons master</strong>
                                                        </option>
                                                        <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Set Designer / Decorator</strong>
                                                        </option>
                                                        <option value="Location Manager" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Location Manager</strong>
                                                        </option>
                                                        <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>StoryBoard Artist</strong>
                                                        </option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ?
                                                            (($getIfAlreadyVideo[4]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Others</strong>
                                                        </option>
                                                                  
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="e1">Video Description </label>
                                                    <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->description != NULL) ? $getIfAlreadyVideo[4]->description : '') : '')); ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                        <div class="browse-photo" onclick="uploadVideo('5')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                        <input type="file" class="d-none" id="videoFile5">
                                                    </div>
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                        <div class="browse-photo" onclick="uploadVideoThumbnail('5')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                        <input type="file" class="d-none" id="videoThumb5" name="videoThumbFile[]">
                                                    </div>
                                                    <div class="u-close" style="float:left; margin-right:20px;">
                                                        <img src="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->thumb != NULL || $getIfAlreadyVideo[4]->thumb != '') ? $getIfAlreadyVideo[4]->thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                        <?php 
                                                            if(count($getIfAlreadyVideo) >= 5)
                                                            {
                                                                if($getIfAlreadyVideo[4]->thumb != NULL || $getIfAlreadyVideo[4]->thumb != '')
                                                                {
                                                            ?>
                                                        <a href="<?php echo URL::to('/delete-video-image/'.$getIfAlreadyVideo[4]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                        </a>
                                                        <?php } } ?>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>
                                                    <input type="hidden" name="uploadedvideosec5" id="uploadedvideosec5">
                                                    <input type="hidden" name="generatedvideosecthumb5" id="generatedvideosecthumb5">
                                                    <div class="u-progress5" style="display:none;">
                                                        <div class="row">
                                                            <div class="col-lg-12 ud-caption">Upload Details</div>
                                                            <div class="col-lg-10">
                                                                <div class="u-title"></div>
                                                                <div id="upload-progress5">
                                                                    <div class="u-size5"></div>
                                                                    <div class="u-progress5">
                                                                        <div class="progress5">
                                                                            <div class="progress-bar5" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                                <span class="sr-only5">0% Complete</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="u-close">
                                                                            <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="u-desc5"><span class="percent-process5"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                                    <label for="e1">Video Title - Position 6</label>
                                                    <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->video_title != NULL) ? $getIfAlreadyVideo[5]->video_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6" style="padding:0px;">
                                                    <label for="e1">Select category</label>
                                                    <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                                        <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Threater</strong>
                                                        </option>
                                                        <option value="Voiceover" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Voiceover</strong>
                                                        </option>
                                                        <option value="Comedian" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Comedian</strong>
                                                        </option>
                                                        <option value=" Photography" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Photography</strong>
                                                        </option>
                                                        <option value="Video Grapher" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Video Grapher</strong>
                                                        </option>
                                                        <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Makeup Artrist</strong>
                                                        </option>
                                                        <option value="Actor" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Actor</strong>
                                                        </option>
                                                        <option value="Child Actor" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Child Actor</strong>
                                                        </option>
                                                        <option value="Comedy Actor" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Comedy Actor</strong>
                                                        </option>
                                                        <option value=" Junior Actor" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Junior Actor</strong>
                                                        </option>
                                                        <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Stunt Artist</strong>
                                                        </option>
                                                        <option value=" Drama Artist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Drama Artist</strong>
                                                        </option>
                                                        <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Mimicry Artist</strong>
                                                        </option>
                                                        <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Background / Atmosphere</strong>
                                                        </option>
                                                        <option value="Model" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Model</strong>
                                                        </option>
                                                        <option value=" Child Model" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Child Model</strong>
                                                        </option>
                                                        <option value="Pro Model" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Pro Model</strong>
                                                        </option>
                                                        <option value="Dancer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Dancer</strong>
                                                        </option>
                                                        <option value="Lead Dancer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Lead Dancer</strong>
                                                        </option>
                                                        <option value="Background Dancer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Background Dancer</strong>
                                                        </option>
                                                        <option value="Video Editor" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Video Editor</strong>
                                                        </option>
                                                        <option value="Music Editor" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Music Editor</strong>
                                                        </option>
                                                        <option value="Audio Editor" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Audio Editor</strong>
                                                        </option>
                                                        <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Dialogue Writer</strong>
                                                        </option>
                                                        <option value="Script Writer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Script Writer</strong>
                                                        </option>
                                                        <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Screeplay Writer</strong>
                                                        </option>
                                                        <option value="Story Writer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Story Writer</strong>
                                                        </option>
                                                        <option value="Lyricists" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Lyricists</strong>
                                                        </option>
                                                        //video vj
                                                        <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Video Jockey (VJ)</strong>
                                                        </option>
                                                        <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> Radio Jockey (RJ)</strong>
                                                        </option>
                                                        <option value=" News Reader" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong> News Reader</strong>
                                                        </option>
                                                        <option value="Reporter" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Reporter</strong>
                                                        </option>
                                                        <option value="Journalist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Journalist</strong>
                                                        </option>
                                                        <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Dubbing Artist</strong>
                                                        </option>
                                                        <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Voice Over Artist</strong>
                                                        </option>
                                                        <option value="New Influncer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>New Influncer</strong>
                                                        </option>
                                                        <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Experienced Influncer</strong>
                                                        </option>
                                                        <option value="Video Advertising" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Video Advertising</strong>
                                                        </option>
                                                        <option value="Audio Advertising" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Audio Advertising</strong>
                                                        </option>
                                                        <option value="Threater" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Threater</strong>
                                                        </option>
                                                        <option value="Banner Advertising" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Banner Advertising</strong>
                                                        </option>
                                                        <option value="Singer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Singer</strong>
                                                        </option>
                                                        <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Music Instrumentalist</strong>
                                                        </option>
                                                        <option value="Music Composer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Music Composer</strong>
                                                        </option>
                                                        <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Director Of Photography / Cinematographer</strong>
                                                        </option>
                                                        <option value="Cameraman" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Cameraman</strong>
                                                        </option>
                                                        <option value="Camera Operator" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Camera Operator</strong>
                                                        </option>
                                                        <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Light man / Gaffer</strong>
                                                        </option>
                                                        <option value="Makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Makeup Artist</strong>
                                                        </option>
                                                        <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Visual Effects Editor</strong>
                                                        </option>
                                                        <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Digital imaging technician</strong>
                                                        </option>
                                                        <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Special Effects makeup Artist</strong>
                                                        </option>
                                                        <option value="Motion control technician" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Motion control technician</strong>
                                                        </option>
                                                        <option value="Fashion Designer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Fashion Designer</strong>
                                                        </option>
                                                        <option value="Hair Stylist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Hair Stylist</strong>
                                                        </option>
                                                        <option value="Costume designer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Costume designer</strong>
                                                        </option>
                                                        <option value="Grip" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Grip</strong>
                                                        </option>
                                                        <option value="Sound Designer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Sound Designer</strong>
                                                        </option>
                                                        <option value="Sound Grip" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Sound Grip</strong>
                                                        </option>
                                                        <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Production Sound Mixer</strong>
                                                        </option>
                                                        <option value="Production Designer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Production Designer</strong>
                                                        </option>
                                                        <option value="Green man" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Green man</strong>
                                                        </option>
                                                        <option value="Property master" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Property master</strong>
                                                        </option>
                                                        <option value="Weapons master" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Weapons master</strong>
                                                        </option>
                                                        <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Set Designer / Decorator</strong>
                                                        </option>
                                                        <option value="Location Manager" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Location Manager</strong>
                                                        </option>
                                                        <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>StoryBoard Artist</strong>
                                                        </option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ?
                                                            (($getIfAlreadyVideo[5]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                            <strong>Others</strong>
                                                        </option>
                                                                  
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="e1">Video Description </label>
                                                    <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->description != NULL) ? $getIfAlreadyVideo[5]->description : '') : '')); ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                        <div class="browse-photo" onclick="uploadVideo('6')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                        <input type="file" class="d-none" id="videoFile6">
                                                    </div>
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                        <div class="browse-photo" onclick="uploadVideoThumbnail('6')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                        <input type="file" class="d-none" id="videoThumb6" name="videoThumbFile[]">
                                                    </div>
                                                    <div class="u-close" style="float:left; margin-right:20px;">
                                                        <img src="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->thumb != NULL || $getIfAlreadyVideo[5]->thumb != '') ? $getIfAlreadyVideo[5]->thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                        <?php 
                                                            if(count($getIfAlreadyVideo) >= 6)
                                                            {
                                                                if($getIfAlreadyVideo[5]->thumb != NULL || $getIfAlreadyVideo[5]->thumb != '')
                                                                {
                                                            ?>
                                                        <a href="<?php echo URL::to('/delete-video-image/'.$getIfAlreadyVideo[5]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                        </a>
                                                        <?php } } ?>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>
                                                    <input type="hidden" name="uploadedvideosec6" id="uploadedvideosec6">
                                                    <input type="hidden" name="generatedvideosecthumb6" id="generatedvideosecthumb6">
                                                    <div class="u-progress6" style="display:none;">
                                                        <div class="row">
                                                            <div class="col-lg-12 ud-caption">Upload Details</div>
                                                            <div class="col-lg-10">
                                                                <div class="u-title"></div>
                                                                <div id="upload-progress6">
                                                                    <div class="u-size6"></div>
                                                                    <div class="u-progress6">
                                                                        <div class="progress6">
                                                                            <div class="progress-bar6" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                                <span class="sr-only6">0% Complete</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="u-close">
                                                                            <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="u-desc6"><span class="percent-process6"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row col-md-12 btn-save">
                                    <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyVideo) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="latest-project" class="tabcontent" >
            <div id="showreel-block" style="background:#f4f3f3; margin-top:10px">
                <div class="container">
                    <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <h2>Latest Projects - add upto 3</h2>
                        <h6>First</h6>
                        <form action="{{URL::to('/save-project')}}" method="post" enctype="multipart/form-data" id="projectform">
                            <?php 
                                $userId = session()->get('user_id');
                                $getIfAlreadyProject = DB::select('select * from artist_project where user_id='.$userId);
                                ?>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="oldProjid[]" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->id != NULL) ? $getIfAlreadyProject[0]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldProjFile[]" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->photo != NULL) ? $getIfAlreadyProject[0]->photo : '') : '')); ?>" />
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Select category</label>  
                                <select class="input-select" style="width:100%" name="categoryproject[]" >
                                    <option value="Threater" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Threater</strong>
                                    </option>
                                    <option value="Voiceover" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Voiceover</strong>
                                    </option>
                                    <option value="Comedian" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Comedian</strong>
                                    </option>
                                    <option value=" Photography" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                        <strong> Photography</strong>
                                    </option>
                                    <option value="Video Grapher" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Video Grapher</strong>
                                    </option>
                                    <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Makeup Artrist</strong>
                                    </option>
                                    <option value="Actor" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Actor</strong>
                                    </option>
                                    <option value="Child Actor" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Child Actor</strong>
                                    </option>
                                    <option value="Comedy Actor" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Comedy Actor</strong>
                                    </option>
                                    <option value=" Junior Actor" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                        <strong> Junior Actor</strong>
                                    </option>
                                    <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                        <strong> Stunt Artist</strong>
                                    </option>
                                    <option value=" Drama Artist" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                        <strong> Drama Artist</strong>
                                    </option>
                                    <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Mimicry Artist</strong>
                                    </option>
                                    <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                        <strong> Background / Atmosphere</strong>
                                    </option>
                                    <option value="Model" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Model</strong>
                                    </option>
                                    <option value=" Child Model" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                        <strong> Child Model</strong>
                                    </option>
                                    <option value="Pro Model" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Pro Model</strong>
                                    </option>
                                    <option value="Dancer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Dancer</strong>
                                    </option>
                                    <option value="Lead Dancer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Lead Dancer</strong>
                                    </option>
                                    <option value="Background Dancer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Background Dancer</strong>
                                    </option>
                                    <option value="Video Editor" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Video Editor</strong>
                                    </option>
                                    <option value="Music Editor" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Music Editor</strong>
                                    </option>
                                    <option value="Audio Editor" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Audio Editor</strong>
                                    </option>
                                    <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Dialogue Writer</strong>
                                    </option>
                                    <option value="Script Writer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Script Writer</strong>
                                    </option>
                                    <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Screeplay Writer</strong>
                                    </option>
                                    <option value="Story Writer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Story Writer</strong>
                                    </option>
                                    <option value="Lyricists" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Lyricists</strong>
                                    </option>
                                    //video vj
                                    <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                        <strong> Video Jockey (VJ)</strong>
                                    </option>
                                    <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                        <strong> Radio Jockey (RJ)</strong>
                                    </option>
                                    <option value=" News Reader" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                        <strong> News Reader</strong>
                                    </option>
                                    <option value="Reporter" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Reporter</strong>
                                    </option>
                                    <option value="Journalist" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Journalist</strong>
                                    </option>
                                    <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Dubbing Artist</strong>
                                    </option>
                                    <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Voice Over Artist</strong>
                                    </option>
                                    <option value="New Influncer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>New Influncer</strong>
                                    </option>
                                    <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Experienced Influncer</strong>
                                    </option>
                                    <option value="Video Advertising" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Video Advertising</strong>
                                    </option>
                                    <option value="Audio Advertising" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Audio Advertising</strong>
                                    </option>
                                    <option value="Threater" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Threater</strong>
                                    </option>
                                    <option value="Banner Advertising" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Banner Advertising</strong>
                                    </option>
                                    <option value="Singer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Singer</strong>
                                    </option>
                                    <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Music Instrumentalist</strong>
                                    </option>
                                    <option value="Music Composer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Music Composer</strong>
                                    </option>
                                    <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Director Of Photography / Cinematographer</strong>
                                    </option>
                                    <option value="Cameraman" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Cameraman</strong>
                                    </option>
                                    <option value="Camera Operator" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Camera Operator</strong>
                                    </option>
                                    <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Light man / Gaffer</strong>
                                    </option>
                                    <option value="Makeup Artist" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Makeup Artist</strong>
                                    </option>
                                    <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Visual Effects Editor</strong>
                                    </option>
                                    <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Digital imaging technician</strong>
                                    </option>
                                    <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Special Effects makeup Artist</strong>
                                    </option>
                                    <option value="Motion control technician" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Motion control technician</strong>
                                    </option>
                                    <option value="Fashion Designer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Fashion Designer</strong>
                                    </option>
                                    <option value="Hair Stylist" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Hair Stylist</strong>
                                    </option>
                                    <option value="Costume designer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Costume designer</strong>
                                    </option>
                                    <option value="Grip" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Grip</strong>
                                    </option>
                                    <option value="Sound Designer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Sound Designer</strong>
                                    </option>
                                    <option value="Sound Grip" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Sound Grip</strong>
                                    </option>
                                    <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Production Sound Mixer</strong>
                                    </option>
                                    <option value="Production Designer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Production Designer</strong>
                                    </option>
                                    <option value="Green man" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Green man</strong>
                                    </option>
                                    <option value="Property master" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Property master</strong>
                                    </option>
                                    <option value="Weapons master" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Weapons master</strong>
                                    </option>
                                    <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Set Designer / Decorator</strong>
                                    </option>
                                    <option value="Location Manager" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Location Manager</strong>
                                    </option>
                                    <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>StoryBoard Artist</strong>
                                    </option>
                                    <option value="Others" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ?
                                        (($getIfAlreadyProject[0]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                        <strong>Others</strong>
                                    </option>
                                              
                                </select>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Title</label>  
                                <input type="text" name="projecttitle[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->title != NULL) ? $getIfAlreadyProject[0]->title : '') : '')); ?>">  
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Date</label>  
                                <input type="date" name="projectdate[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->date != NULL) ? $getIfAlreadyProject[0]->date : '') : '')); ?>">  
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Add Link - If any</label>  
                                <input type="text" name="projectlink[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->link != NULL) ? $getIfAlreadyProject[0]->link : '') : '')); ?>">  
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Button Name</label>  
                                <select class="form-control" name="projectbuttonname[]" style="padding: 10px;">
                                    <option value="" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                    <option value="View Video" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "View Video") ? 'selected' : '') : '') : ''); ?>>View Video</option>
                                    <option value="View Now" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "View Now") ? 'selected' : '') : '') : ''); ?>>View Now</option>
                                    <option value="Official Trailer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "Official Trailer") ? 'selected' : '') : '') : ''); ?>> Official Trailer</option>
                                    <option value="View Profile" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "View Profile") ? 'selected' : '') : '') : ''); ?>>View Profile</option>
                                    <option value="View More" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "View More") ? 'selected' : '') : '') : ''); ?>>View More</option>
                                    <option value="No Button" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "No Button") ? 'selected' : '') : '') : ''); ?>>No Button</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-12">  
                                <label for="exampleInputEmail1">Description 200 words</label>
                                <textarea id="w3review" name="projectdesc[]" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->description != NULL) ? substr($getIfAlreadyProject[0]->description,0,200) : '') : '')); ?></textarea>
                            </div>
                            <div class="clear"></div>
                            <a href="javascript:void(0)" onclick="openProjUploader('1')" style="background: #637076;color: #fff;width:145px;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                            Browse Banner Photo</a>
                            <input type="file" style="display:none;" name="fileproj[]" id="fileproj1" onchange="showProjFileLabel('1')">
                            <div class="row" style="margin:20px 0 30px 0;padding: 0;">
                                <div class="col-lg-3">
                                    <?php if(count($getIfAlreadyProject) >= 1){ 
                                        if($getIfAlreadyProject[0]->photo != NULL || $getIfAlready[0]->photo != '')
                                        {
                                        ?>
                                    <img src="<?php echo $getIfAlreadyProject[0]->photo ?>" style="width:200px;height:200px;margin-top:15px;">
                                    <?php } } ?>     
                                </div>
                                <div class="clear"></div>
                                <p id="fileprojlabel1" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                                <h6>Second</h6>
                                <div class="form-group col-lg-6">
                                    <input type="hidden" name="oldProjid[]" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->id != NULL) ? $getIfAlreadyProject[1]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldProjFile[]" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->photo != NULL) ? $getIfAlreadyProject[1]->photo : '') : '')); ?>" />
                                    <label for="exampleInputEmail1">Select category</label>  
                                    <select class="input-select" style="width:100%" name="categoryproject[]" >
                                        <option value="Threater" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Threater</strong>
                                        </option>
                                        <option value="Voiceover" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Voiceover</strong>
                                        </option>
                                        <option value="Comedian" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Comedian</strong>
                                        </option>
                                        <option value=" Photography" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                            <strong> Photography</strong>
                                        </option>
                                        <option value="Video Grapher" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Video Grapher</strong>
                                        </option>
                                        <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Makeup Artrist</strong>
                                        </option>
                                        <option value="Actor" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Actor</strong>
                                        </option>
                                        <option value="Child Actor" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Child Actor</strong>
                                        </option>
                                        <option value="Comedy Actor" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Comedy Actor</strong>
                                        </option>
                                        <option value=" Junior Actor" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                            <strong> Junior Actor</strong>
                                        </option>
                                        <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                            <strong> Stunt Artist</strong>
                                        </option>
                                        <option value=" Drama Artist" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                            <strong> Drama Artist</strong>
                                        </option>
                                        <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Mimicry Artist</strong>
                                        </option>
                                        <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                            <strong> Background / Atmosphere</strong>
                                        </option>
                                        <option value="Model" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Model</strong>
                                        </option>
                                        <option value=" Child Model" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                            <strong> Child Model</strong>
                                        </option>
                                        <option value="Pro Model" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Pro Model</strong>
                                        </option>
                                        <option value="Dancer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Dancer</strong>
                                        </option>
                                        <option value="Lead Dancer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Lead Dancer</strong>
                                        </option>
                                        <option value="Background Dancer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Background Dancer</strong>
                                        </option>
                                        <option value="Video Editor" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Video Editor</strong>
                                        </option>
                                        <option value="Music Editor" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Music Editor</strong>
                                        </option>
                                        <option value="Audio Editor" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Audio Editor</strong>
                                        </option>
                                        <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Dialogue Writer</strong>
                                        </option>
                                        <option value="Script Writer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Script Writer</strong>
                                        </option>
                                        <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Screeplay Writer</strong>
                                        </option>
                                        <option value="Story Writer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Story Writer</strong>
                                        </option>
                                        <option value="Lyricists" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Lyricists</strong>
                                        </option>
                                        //video vj
                                        <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                            <strong> Video Jockey (VJ)</strong>
                                        </option>
                                        <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                            <strong> Radio Jockey (RJ)</strong>
                                        </option>
                                        <option value=" News Reader" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                            <strong> News Reader</strong>
                                        </option>
                                        <option value="Reporter" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Reporter</strong>
                                        </option>
                                        <option value="Journalist" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Journalist</strong>
                                        </option>
                                        <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Dubbing Artist</strong>
                                        </option>
                                        <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Voice Over Artist</strong>
                                        </option>
                                        <option value="New Influncer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>New Influncer</strong>
                                        </option>
                                        <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Experienced Influncer</strong>
                                        </option>
                                        <option value="Video Advertising" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Video Advertising</strong>
                                        </option>
                                        <option value="Audio Advertising" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Audio Advertising</strong>
                                        </option>
                                        <option value="Threater" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Threater</strong>
                                        </option>
                                        <option value="Banner Advertising" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Banner Advertising</strong>
                                        </option>
                                        <option value="Singer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Singer</strong>
                                        </option>
                                        <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Music Instrumentalist</strong>
                                        </option>
                                        <option value="Music Composer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Music Composer</strong>
                                        </option>
                                        <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Director Of Photography / Cinematographer</strong>
                                        </option>
                                        <option value="Cameraman" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Cameraman</strong>
                                        </option>
                                        <option value="Camera Operator" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Camera Operator</strong>
                                        </option>
                                        <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Light man / Gaffer</strong>
                                        </option>
                                        <option value="Makeup Artist" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Makeup Artist</strong>
                                        </option>
                                        <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Visual Effects Editor</strong>
                                        </option>
                                        <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Digital imaging technician</strong>
                                        </option>
                                        <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Special Effects makeup Artist</strong>
                                        </option>
                                        <option value="Motion control technician" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Motion control technician</strong>
                                        </option>
                                        <option value="Fashion Designer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Fashion Designer</strong>
                                        </option>
                                        <option value="Hair Stylist" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Hair Stylist</strong>
                                        </option>
                                        <option value="Costume designer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Costume designer</strong>
                                        </option>
                                        <option value="Grip" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Grip</strong>
                                        </option>
                                        <option value="Sound Designer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Sound Designer</strong>
                                        </option>
                                        <option value="Sound Grip" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Sound Grip</strong>
                                        </option>
                                        <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Production Sound Mixer</strong>
                                        </option>
                                        <option value="Production Designer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Production Designer</strong>
                                        </option>
                                        <option value="Green man" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Green man</strong>
                                        </option>
                                        <option value="Property master" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Property master</strong>
                                        </option>
                                        <option value="Weapons master" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Weapons master</strong>
                                        </option>
                                        <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Set Designer / Decorator</strong>
                                        </option>
                                        <option value="Location Manager" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Location Manager</strong>
                                        </option>
                                        <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>StoryBoard Artist</strong>
                                        </option>
                                        <option value="Others" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ?
                                            (($getIfAlreadyProject[1]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                            <strong>Others</strong>
                                        </option>
                                                  
                                    </select>
                                </div>
                                <div class="form-group col-lg-6">
                                    <label for="exampleInputEmail1">Title</label>  
                                    <input type="text" name="projecttitle[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->title != NULL) ? $getIfAlreadyProject[1]->title : '') : '')); ?>">  
                                </div>
                                <div class="form-group col-lg-4">
                                    <label for="exampleInputEmail1">Date</label>  
                                    <input type="date" name="projectdate[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->date != NULL) ? $getIfAlreadyProject[1]->date : '') : '')); ?>">  
                                </div>
                                <div class="form-group col-lg-4">
                                    <label for="exampleInputEmail1">Add Link - If any</label>  
                                    <input type="text" name="projectlink[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->link != NULL) ? $getIfAlreadyProject[1]->link : '') : '')); ?>">  
                                </div>
                                <div class="form-group col-lg-4">
                                    <label for="exampleInputEmail1">Button Name</label>  
                                    <select class="form-control" name="projectbuttonname[]" style="padding: 10px;">
                                        <option value="" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                        <option value="View Video" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "View Video") ? 'selected' : '') : '') : ''); ?>>View Video</option>
                                        <option value="View Now" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "View Now") ? 'selected' : '') : '') : ''); ?>>View Now</option>
                                        <option value="Official Trailer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "Official Trailer") ? 'selected' : '') : '') : ''); ?>> Official Trailer</option>
                                        <option value="View Profile" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "View Profile") ? 'selected' : '') : '') : ''); ?>>View Profile</option>
                                        <option value="View More" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "View More") ? 'selected' : '') : '') : ''); ?>>View More</option>
                                        <option value="No Button" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "No Button") ? 'selected' : '') : '') : ''); ?>>No Button</option>
                                    </select>
                                </div>
                                <div class="form-group col-lg-12">  
                                    <label for="exampleInputEmail1">Description 200 words</label>
                                    <textarea id="w3review" name="projectdesc[]" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->description != NULL) ? substr($getIfAlreadyProject[2]->description,0,200) : '') : '')); ?></textarea>
                                </div>
                                <div class="clear"></div>
                                <a href="javascript:void(0)" onclick="openProjUploader('2')" style="background: #637076;color: #fff;width:145px;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                                Browse Banner Photo</a>
                                <input type="file" style="display:none;" name="fileproj[]" id="fileproj2" onchange="showProjFileLabel('2')">
                                <div class="row" style="margin:20px 0 30px 0;padding: 0;">
                                    <div class="col-lg-3">
                                        <?php if(count($getIfAlreadyProject) >= 2){ 
                                            if($getIfAlreadyProject[1]->photo != NULL || $getIfAlready[1]->photo != '')
                                            {
                                            ?>
                                        <img src="<?php echo $getIfAlreadyProject[1]->photo ?>" style="width:200px;height:200px;margin-top:15px;">
                                        <?php } } ?>     
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <p id="fileprojlabel2" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                                <div id="cloneprojdiv" class="d-none">
                                    <div class="form-group col-lg-6">
                                        <label for="exampleInputEmail1">Select category</label>  
                                        <input type="hidden" name="oldProjFile[]" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->photo != NULL) ? $getIfAlreadyProject[2]->photo : '') : '')); ?>" />
                                        <input type="hidden" name="oldProjid[]" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->id != NULL) ? $getIfAlreadyProject[2]->id : '') : '')); ?>" />
                                        <select class="input-select" style="width:100%" name="categoryproject[]" >
                                            <option value="Threater" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Threater</strong>
                                            </option>
                                            <option value="Voiceover" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Voiceover") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Voiceover</strong>
                                            </option>
                                            <option value="Comedian" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Comedian") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Comedian</strong>
                                            </option>
                                            <option value=" Photography" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == " Photography") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Photography</strong>
                                            </option>
                                            <option value="Video Grapher" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Video Grapher") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Video Grapher</strong>
                                            </option>
                                            <option value="Makeup Artrist" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Makeup Artrist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Makeup Artrist</strong>
                                            </option>
                                            <option value="Actor" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Actor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Actor</strong>
                                            </option>
                                            <option value="Child Actor" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Child Actor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Child Actor</strong>
                                            </option>
                                            <option value="Comedy Actor" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Comedy Actor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Comedy Actor</strong>
                                            </option>
                                            <option value=" Junior Actor" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == " Junior Actor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Junior Actor</strong>
                                            </option>
                                            <option value=" Stunt Artist" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == " Stunt Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Stunt Artist</strong>
                                            </option>
                                            <option value=" Drama Artist" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == " Drama Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Drama Artist</strong>
                                            </option>
                                            <option value="Mimicry Artist" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Mimicry Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Mimicry Artist</strong>
                                            </option>
                                            <option value=" Background / Atmosphere" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == " Background / Atmosphere") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Background / Atmosphere</strong>
                                            </option>
                                            <option value="Model" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Model") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Model</strong>
                                            </option>
                                            <option value=" Child Model" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == " Child Model") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Child Model</strong>
                                            </option>
                                            <option value="Pro Model" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Pro Model") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Pro Model</strong>
                                            </option>
                                            <option value="Dancer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Dancer</strong>
                                            </option>
                                            <option value="Lead Dancer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Lead Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Lead Dancer</strong>
                                            </option>
                                            <option value="Background Dancer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Background Dancer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Background Dancer</strong>
                                            </option>
                                            <option value="Video Editor" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Video Editor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Video Editor</strong>
                                            </option>
                                            <option value="Music Editor" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Music Editor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Music Editor</strong>
                                            </option>
                                            <option value="Audio Editor" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Audio Editor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Audio Editor</strong>
                                            </option>
                                            <option value="Dialogue Writer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Dialogue Writer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Dialogue Writer</strong>
                                            </option>
                                            <option value="Script Writer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Script Writer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Script Writer</strong>
                                            </option>
                                            <option value="Screeplay Writer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Screeplay Writer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Screeplay Writer</strong>
                                            </option>
                                            <option value="Story Writer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Story Writer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Story Writer</strong>
                                            </option>
                                            <option value="Lyricists" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Lyricists") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Lyricists</strong>
                                            </option>
                                            //video vj
                                            <option value=" Video Jockey (VJ)" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == " Video Jockey (VJ)") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Video Jockey (VJ)</strong>
                                            </option>
                                            <option value=" Radio Jockey (RJ)" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == " Radio Jockey (RJ)") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> Radio Jockey (RJ)</strong>
                                            </option>
                                            <option value=" News Reader" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == " News Reader") ? 'selected' : '') : '') : ''); ?>>
                                                <strong> News Reader</strong>
                                            </option>
                                            <option value="Reporter" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Reporter") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Reporter</strong>
                                            </option>
                                            <option value="Journalist" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Journalist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Journalist</strong>
                                            </option>
                                            <option value="Dubbing Artist" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Dubbing Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Dubbing Artist</strong>
                                            </option>
                                            <option value="Voice Over Artist" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Voice Over Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Voice Over Artist</strong>
                                            </option>
                                            <option value="New Influncer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "New Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>New Influncer</strong>
                                            </option>
                                            <option value="Experienced Influncer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Experienced Influncer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Experienced Influncer</strong>
                                            </option>
                                            <option value="Video Advertising" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Video Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Video Advertising</strong>
                                            </option>
                                            <option value="Audio Advertising" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Audio Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Audio Advertising</strong>
                                            </option>
                                            <option value="Threater" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Threater") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Threater</strong>
                                            </option>
                                            <option value="Banner Advertising" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Banner Advertising") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Banner Advertising</strong>
                                            </option>
                                            <option value="Singer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Singer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Singer</strong>
                                            </option>
                                            <option value="Music Instrumentalist" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Music Instrumentalist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Music Instrumentalist</strong>
                                            </option>
                                            <option value="Music Composer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Music Composer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Music Composer</strong>
                                            </option>
                                            <option value="Director Of Photography / Cinematographer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Director Of Photography / Cinematographer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Director Of Photography / Cinematographer</strong>
                                            </option>
                                            <option value="Cameraman" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Cameraman") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Cameraman</strong>
                                            </option>
                                            <option value="Camera Operator" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Camera Operator") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Camera Operator</strong>
                                            </option>
                                            <option value="Light man / Gaffer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Light man / Gaffer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Light man / Gaffer</strong>
                                            </option>
                                            <option value="Makeup Artist" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Makeup Artist</strong>
                                            </option>
                                            <option value="Visual Effects Editor" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Visual Effects Editor") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Visual Effects Editor</strong>
                                            </option>
                                            <option value="Digital imaging technician" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Digital imaging technician") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Digital imaging technician</strong>
                                            </option>
                                            <option value="Special Effects makeup Artist" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Special Effects makeup Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Special Effects makeup Artist</strong>
                                            </option>
                                            <option value="Motion control technician" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Motion control technician") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Motion control technician</strong>
                                            </option>
                                            <option value="Fashion Designer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Fashion Designer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Fashion Designer</strong>
                                            </option>
                                            <option value="Hair Stylist" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Hair Stylist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Hair Stylist</strong>
                                            </option>
                                            <option value="Costume designer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Costume designer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Costume designer</strong>
                                            </option>
                                            <option value="Grip" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Grip") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Grip</strong>
                                            </option>
                                            <option value="Sound Designer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Sound Designer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Sound Designer</strong>
                                            </option>
                                            <option value="Sound Grip" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Sound Grip") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Sound Grip</strong>
                                            </option>
                                            <option value="Production Sound Mixer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Production Sound Mixer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Production Sound Mixer</strong>
                                            </option>
                                            <option value="Production Designer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Production Designer") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Production Designer</strong>
                                            </option>
                                            <option value="Green man" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Green man") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Green man</strong>
                                            </option>
                                            <option value="Property master" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Property master") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Property master</strong>
                                            </option>
                                            <option value="Weapons master" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Weapons master") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Weapons master</strong>
                                            </option>
                                            <option value="Set Designer / Decorator" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Set Designer / Decorator") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Set Designer / Decorator</strong>
                                            </option>
                                            <option value="Location Manager" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Location Manager") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Location Manager</strong>
                                            </option>
                                            <option value="StoryBoard Artist" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "StoryBoard Artist") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>StoryBoard Artist</strong>
                                            </option>
                                            <option value="Others" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ?
                                                (($getIfAlreadyProject[2]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                <strong>Others</strong>
                                            </option>
                                                      
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="exampleInputEmail1">Title</label>  
                                        <input type="text" name="projecttitle[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->title != NULL) ? $getIfAlreadyProject[2]->title : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="exampleInputEmail1">Date</label>  
                                        <input type="date" name="projectdate[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->date != NULL) ? $getIfAlreadyProject[2]->date : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="exampleInputEmail1">Add Link - If any</label>  
                                        <input type="text" name="projectlink[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->link != NULL) ? $getIfAlreadyProject[2]->link : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="exampleInputEmail1">Button Name</label>  
                                        <select class="form-control" name="projectbuttonname[]" style="padding: 10px;">
                                            <option value="" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                            <option value="View Video" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "View Video") ? 'selected' : '') : '') : ''); ?>>View Video</option>
                                            <option value="View Now" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "View Now") ? 'selected' : '') : '') : ''); ?>>View Now</option>
                                            <option value="Official Trailer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "Official Trailer") ? 'selected' : '') : '') : ''); ?>> Official Trailer</option>
                                            <option value="View Profile" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "View Profile") ? 'selected' : '') : '') : ''); ?>>View Profile</option>
                                            <option value="View More" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "View More") ? 'selected' : '') : '') : ''); ?>>View More</option>
                                            <option value="No Button" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "No Button") ? 'selected' : '') : '') : ''); ?>>No Button</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-12">  
                                        <label for="exampleInputEmail1">Description 200 words</label>
                                        <textarea id="w3review" name="projectdesc[]" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->description != NULL) ? substr($getIfAlreadyProject[2]->description,0,200) : '') : '')); ?></textarea>
                                    </div>
                                    <div class="clear"></div>
                                    <a href="javascript:void(0)" onclick="openProjUploader('3')" style="background: #637076;color: #fff;width:145px;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                                    Browse Banner Photo</a>
                                    <input type="file" style="display:none;" name="fileproj[]" id="fileproj3" onchange="showProjFileLabel('3')">
                                    <p id="fileprojlabel3" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                                    <div class="row" style="margin:20px 0 30px 0;padding: 0;">
                                        <div class="col-lg-3">
                                            <?php if(count($getIfAlreadyProject) >= 3){ 
                                                if($getIfAlreadyProject[2]->photo != NULL || $getIfAlready[2]->photo != '')
                                                {
                                                ?>
                                            <img src="<?php echo $getIfAlreadyProject[2]->photo ?>" style="width:200px;height:200px;margin-top:15px;">
                                            <?php } } ?>     
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:20px">
                                    <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyProject) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                                </div>
                        </form>
                        <a href="javascript:void(0)" id="addProj" onclick="addProj()" style="background: #e5e5e5;color: #000;width:130px;margin:20px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                        Add More Projects</a>
                        <a href="javascript:void(0)" class="d-none" id="removeProj" onclick="removeProj()" style="background: #e5e5e5;color: #000;width:130px;margin:20px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                        Remove Projects</a>
                        </div>
                    </div>
                    <div id="contact" class="tabcontent" style="margin-top:10px">
                        <div class="container">
                            <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                                <div class="form-group achievemets">
                                    <h2>Contact Us</h2>
                                    <?php 
                                        $userId = session()->get('user_id');
                                        $getIfAlreadyContact = DB::select('select * from artist_contact where user_id='.$userId);
                                        ?>
                                    <form action="{{URL::to('save-contact')}}" id="contactform" style="margin-top:0px" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        <div class="form-group col-lg-12">
                                            <label for="exampleInputEmail1">Title eg: Contact me to know more!!!</label>
                                            <input type="text" name="titlecontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->title != NULL) ? $getIfAlreadyContact[0]->title : '') : '')); ?>">  
                                        </div>
                                        <div class="form-group col-lg-12">
                                            <label for="exampleInputEmail1"> Description (50 words)</label>
                                            <textarea id="w3review" name="descriptioncontact" onKeyPress="return check(event,value)" onInput="checkLength(50,this)" rows="4" cols="50"><?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->description != NULL) ? substr($getIfAlreadyContact[0]->description,0,50) : '') : '')); ?></textarea> 
                                        </div>
                                        <div style="clear:both; overflow:hidden"></div>
                                        <div class="col-lg-3">
                                            <div class="form-group"> <label for="exampleInputEmail1">Number  / Whatsapp No </label>  
                                                <input type="text" name="phonecontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->phone != NULL) ? $getIfAlreadyContact[0]->phone : '') : '')); ?>">  
                                            </div>
                                            <input type="checkbox" name="tick_whatsapp_feature" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->tick_whatsapp_feature != NULL) ? $getIfAlreadyContact[0]->tick_whatsapp_feature : '') : '')); ?>" style="height: 16px;margin: -1px 5px 0 0;float: left;" <?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->tick_whatsapp_feature != NULL && $getIfAlreadyContact[0]->tick_whatsapp_feature != 'no') ? 'checked' : '') : '')); ?>> Check tick for whatsaps features
                                            <br> <small style="font-size: 11px;font-weight: bold;">Note: Do not type number if you have privacy issue</small>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group"> 
                                                <label for="exampleInputEmail1">Email id</label>
                                                <input type="email" name="emailcontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->email != NULL) ? $getIfAlreadyContact[0]->email : '') : '')); ?>">    
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group"> 
                                                <label for="exampleInputEmail1">Facebook</label>
                                                <input type="text" name="facebookcontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->facebook != NULL) ? $getIfAlreadyContact[0]->facebook : '') : '')); ?>">    
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <label for="exampleInputEmail1">Instagram</label>
                                            <input type="text" name="instacontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->instagram != NULL) ? $getIfAlreadyContact[0]->instagram : '') : '')); ?>">    
                                        </div>
                                        <div class="col-lg-3">
                                            <label for="exampleInputEmail1">Youtube</label>
                                            <input type="text" name="youtubecontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->youtube != NULL) ? $getIfAlreadyContact[0]->youtube : '') : '')); ?>">    
                                        </div>
                                        <div style="clear:both; overflow:hidden"></div>
                                </div>
                                <div style="clear:both; overflow:hidden"></div>
                                <div class="row">
                                <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlready) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<style>
    .d-none{
    display:none;
    }
    .success{
    color:green!important;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
{{ HTML::style('public/css/bootstrap-tagsinput.css')}}
{{ HTML::script('public/js/jquery.validate.js')}}
{{ HTML::script('public/js/bootstrap-tagsinput.js')}}
{{ HTML::script('public/js/bootstrap3-typeahead.js')}}
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    function getStates(value)
    {
        $('#state').empty();
        $('#state').append('<option value="">Select State</option>');
        if(value != '')
        {
            $.ajax({
                url:'{{URL::to("/get-states")}}',
                data:{id:value,'_token':'<?php echo csrf_token(); ?>'},
                type:'post',
                success:function(res)
                {
                    let parse= JSON.parse(res);
                    if(parse.length > 0)
                    {
                        for(let i=0;i<parse.length;i++)
                            $('#state').append('<option value="'+parse[i].name+'">'+parse[i].name+'</option>');
                    }else{
                        $('#state').append('<option value="">No States Found</option>');    
                    }
                }
            })
        }else{
            $('#state').append('<option value="">Select State</option>');    
        }
    }
    
    function getCities(value)
    {
        $('#city').empty();
        $('#city').append('<option value="">Select City</option>');
        if(value != '')
        {
            $.ajax({
                url:'{{URL::to("/get-cities")}}',
                data:{id:value,'_token':'<?php echo csrf_token(); ?>'},
                type:'post',
                success:function(res)
                {
                    let parse= JSON.parse(res);
                    if(parse.length > 0)
                    {
                        for(let i=0;i<parse.length;i++)
                            $('#city').append('<option value="'+parse[i].name+'">'+parse[i].name+'</option>');
                    }else{
                       $('#city').append('<option value="">No Cities Found</option>');     
                    }
                }
            })
        }else{
            $('#city').append('<option value="">Select City</option>');     
        }
    }
    
    $('#artistForm').on('submit',function(e)
    {
        e.preventDefault();
        let arr = $(this).serializeArray();
        let count = 0;
        for(let i =0 ;i<arr.length;i++)
        {
            if(arr[i].name == 'artistcheck[]')
            {
                count++;
            }
        }
        if(count > 5)
        {
            alert('Please Select Upto 5 Multiple Options');
        }else if(count == 0)
        {
            alert('Please Select At Least One Option');
        }else{
            let data = new FormData(this);
            $.ajax({
                url:$(this).attr('action'),
                data:data,
                contentType: false,
                cache: false,
                processData:false,
                type:'post',
                success:function(res)
                {
                    alert('Data Saved Successfully');
                    location.reload();
                }
            });
        }
    })
    
    $('#achivementForm').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#reelform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#videoform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#whatwedoform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#projectform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    function showPhotoUploader(num)
    {
        $('#photosectionphoto'+num).click();
    }
    
    $('#contactform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                // location.reload();
            }
        });
    })
    
    function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";
    }
    
    function showAchivementUploader(num)
    {
        $('#fileachive'+num).click();
    }
    
    function showFileLabel(num)
    {
        $('#fileachivelabel'+num).html('1 File Selected <i class="fa fa-times" onclick="removeAchivementFile('+num+')"></i>');
    }
    
    function removeAchivementFile(num)
    {
        const file = document.querySelector('#fileachive'+num);
        file.value = '';
        $('#fileachivelabel'+num).html('');
    }
    
    function openReelVideo()
    {
        $('#reelvideo').click();
    }
    
    function openReelThumb(){
        let fileinput = $('input[data-target="video_thumb"]');
        fileinput.click();
    }
    
    function addfourmore(num)
    {
        if(num == 2)
        {
            $('#portfolio3').removeClass('d-none');
            $('#addPhotoThreeMore').removeClass('d-none');
            $('#removePhotoTwoMore').removeClass('d-none');
            $('#addPhotoTwoMore').addClass('d-none');
        }
        if(num == 3)
        {
            $('#portfolio4').removeClass('d-none');
            $('#addPhotoThreeMore').addClass('d-none');
            $('#removePhotoThreeMore').removeClass('d-none');
            $('#addPhotoTwoMore').addClass('d-none');
        }
    }
    
    function removefourmore(num)
    {
        if(num == 2)
        {
            $('#portfolio3').addClass('d-none');
            $('#removePhotoTwoMore').addClass('d-none');
            $('#addPhotoTwoMore').removeClass('d-none');
            $('#addPhotoThreeMore').addClass('d-none');
        }
        if(num == 3)
        {
            $('#portfolio4').addClass('d-none');
            if($('#portfolio3').hasClass('d-none'))
            {
                $('#addPhotoThreeMore').addClass('d-none');
            }else{
                $('#addPhotoThreeMore').removeClass('d-none');
            }
            $('#removePhotoThreeMore').addClass('d-none');
        }
    }
    
    async function getThumbnailForVideo(videoUrl) {
      const video = document.createElement("video");
      const canvas = document.createElement("canvas");
      video.style.display = "none";
      canvas.style.display = "none";
    
      // Trigger video load
      await new Promise((resolve, reject) => {
        video.addEventListener("loadedmetadata", () => {
          video.width = video.videoWidth;
          video.height = video.videoHeight;
          canvas.width = '665';
          canvas.height = '372';
          // Seek the video to 25%
          video.currentTime = video.duration * 0.25;
        });
        video.addEventListener("seeked", () => resolve());
        video.src = videoUrl;
      });
    
      // Draw the thumbnailz
      canvas
        .getContext("2d")
        .drawImage(video, 0, 0, '672', '372');
      const imageUrl = canvas.toDataURL("image/png");
      return imageUrl;
    }
    
    const videoFile1 = document.querySelector('#videoFile1');
    
    videoFile1.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb1').val(thumbUrl);
            $('#previewvideosec1').attr('src',thumbUrl);
            $('.u-progress1').show();
    	    handleFiles1(file,'#uploadedvideosec1','1');
    	}
    });
    
    const videoFile2 = document.querySelector('#videoFile2');
    
    videoFile2.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb2').val(thumbUrl);
            $('#previewvideosec2').attr('src',thumbUrl);
            $('.u-progress2').show();
    	    handleFiles1(file,'#uploadedvideosec2','2');
    	}
    });
    
    const videoFile3 = document.querySelector('#videoFile3');
    
    videoFile3.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb3').val(thumbUrl);
            $('#previewvideosec3').attr('src',thumbUrl);
            $('.u-progress3').show();
    	    handleFiles1(file,'#uploadedvideosec3','3');
    	}
    });
    
    const videoFile4 = document.querySelector('#videoFile4');
    
    videoFile4.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb4').val(thumbUrl);
            $('#previewvideosec4').attr('src',thumbUrl);
            $('.u-progress4').show();
    	    handleFiles1(file,'#uploadedvideosec4','4');
    	}
    });
    
    const videoFile5 = document.querySelector('#videoFile5');
    
    videoFile5.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb5').val(thumbUrl);
            $('#previewvideosec5').attr('src',thumbUrl);
            $('.u-progress5').show();
    	    handleFiles1(file,'#uploadedvideosec5','5');
    	}
    });
    
    const videoFile6 = document.querySelector('#videoFile6');
    
    videoFile6.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb6').val(thumbUrl);
            $('#previewvideosec6').attr('src',thumbUrl);
            $('.u-progress6').show();
    	    handleFiles1(file,'#uploadedvideosec6','6');
    	}
    });
    
    const fileInput = document.querySelector("#reelvideo");
    
    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), 
            n = bstr.length, 
            u8arr = new Uint8Array(n);
            
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        
        return new File([u8arr], filename, {type:mime});
    }
    
    fileInput.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generated_video_thumb').val(thumbUrl);
            $('#profile_photo_preview img').attr('src',thumbUrl);
            $('.u-details').show();
    	    handleFiles1(file,'#videouploaded','');
    	}
    });
    
    handleFiles1 = function (files,selector,num,e){
    		
    	var fileName = files.name;
    	fileName.split('.').slice(0, -1).join('.');
    	$('.u-title').html(fileName);
    	
    	var info = '<div class="u-size'+num+'">'+(parseInt(parseInt(files.size / 1024))/1024).toFixed(2)+' MB / <span class="up-done"></span> MB</div><div class="u-progress'+num+'"><div class="progress'+num+'"><div class="progress-bar'+num+'" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span class="sr-only'+num+'">0% Complete</span></div></div><div class="u-close"><a href="#"><i class="cvicon-cv-cancel"></i></a></div></div>';
    			
    	$("#upload-progress"+num).show("fast",function(){
    		$("#upload-progress"+num).html(info); 
    		uploadVideoFile1(files,selector,num);
    	});
    		
      }
      /*Upload Video File*/
      uploadVideoFile1 = function(file,target,num){
    	 console.log(file);
    	 
    	// check if browser supports file reader object 
        	if (typeof FileReader !== "undefined"){
        	//alert("uploading "+file.name);  
        	reader = new FileReader();
        	reader.onload = function(e){
        	    console.log(e);
        		//alert(e.target.result);
        		//$('.preview img').attr('src',e.target.result).css("width","70px").css("height","70px");
        	}
        	reader.readAsDataURL(file);
        	
        	var formdata = new FormData();
        	formdata.append("video", file);
        	
        	xhr = new XMLHttpRequest();
        	xhr.open("POST", "upload/video");
        	
        	xhr.upload.addEventListener("progress", function (event) {
        		//console.log(event);
        		if (event.lengthComputable) {
        			$(".progress-bar"+num).css("width",(event.loaded / event.total) * 100 + "%");
        			$(".progress-bar"+num).css("aria-valuenow",(event.loaded / event.total) * 100);
        			$(".sr-only"+num).html(" "+((event.loaded / event.total) * 100).toFixed() + "%");
        			$(".up-done").html((parseInt(parseInt(event.loaded / 1024))/1024).toFixed(2));
        			$('.percent-process'+num).html('<strong>'+parseInt((event.loaded / event.total) * 100).toFixed(0)+'%</strong>');
        		}
        		else {
        			alert("Failed to compute file upload length");
        		}
        	}, false);
        
        	xhr.onreadystatechange = function (oEvent) {  
        	  if (xhr.readyState === 4) {  
        		if (xhr.status === 200) {  
        		  $(".progress-bar"+num).css("width","100%");
        		  $(".progress-bar"+num).attr("aria-valuenow","100");
        		  $(".sr-only"+num).html("100%");
        		  $(".up-done").html((parseInt(parseInt(file.size / 1024))/1024).toFixed(2));
        		  $('.u-desc'+num).html("<strong>100% Completed.</strong> Your video has been successfully upload.");
        		  $('.u-desc'+num).addClass('text-success');
        		} else {  
        		  alert("Error"+ xhr.statusText);  
        		}  
        	  }  
        	}; 
        	
        	xhr.onload  = function() {
        	   var jsonResponse = xhr.response;
        	   result = JSON.parse(jsonResponse);
        	   if(result.status == true){
                    $(target).val(result.file);
        	   }else{
        		   alert('File is not uploading. Please try again.')
        	   }	  
        	};
        
        	
        	var csrfToken = $('#reelform').find('input[name=_token]').val();
        	xhr.setRequestHeader('X-csrf-token', csrfToken); 	
        	
        	xhr.send(formdata);
        
        	}else{
        		alert("Your browser doesnt support FileReader object");
        	} 		
          }
    
    function showReelThumbLabel()
    {
        $('#reelvideothumblabel').html('1 File Selected <i class="fa fa-times" onclick="removeVideoThumbFile()"></i>');
    }
    
    function removeVideoFile()
    {
        const file = document.querySelector('#reelvideolabel');
        file.value = '';
        $('#reelvideolabel').html('');
    }
    
    function removeVideoThumbFile()
    {
        const file = document.querySelector('#reelvideothumblabel');
        file.value = '';
        $('#reelvideothumblabel').html('');
    }
    
    function removeAchivementFile(num)
    {
        const file = document.querySelector('#fileachive'+num);
        file.value = '';
        $('#fileachivelabel'+num).html('');
    }
    
    function addMoreAchivement()
    {
        $('#addMoreAchivement').addClass('d-none');
        $('#removeMoreAchivement').removeClass('d-none');
        $('#cloneachivement').removeClass('d-none');
    }
    
    function removeMoreAchivement()
    {
        $('#addMoreAchivement').removeClass('d-none');
        $('#removeMoreAchivement').addClass('d-none');
        $('#cloneachivement').addClass('d-none');
    }
    
    function removeWhatWeDo()
    {
        for(let i=5;i<=8;i++)
        {
            $('#title'+i).val('');
            $('#desc'+i).val('');
        }
        $('#addMore').removeClass('d-none');
        $('#removeMore').addClass('d-none');
        $('#clonediv').addClass('d-none');
    }
    
    function addProj()
    {
        $('#addProj').addClass('d-none');
        $('#removeProj').removeClass('d-none');
        $('#cloneprojdiv').removeClass('d-none');    
    }
    
    function removeProj()
    {
        $('#addProj').removeClass('d-none');
        $('#removeProj').addClass('d-none');
        $('#cloneprojdiv').addClass('d-none');
    }
    
    function removeMoreVideo()
    {
        $('#videoAdd').removeClass('d-none');
        $('#videoRemove').addClass('d-none');
        $('#clonevideodiv').addClass('d-none');
    }
    
    function addMoreVideo()
    {
        $('#videoAdd').addClass('d-none');
        $('#videoRemove').removeClass('d-none');
        $('#clonevideodiv').removeClass('d-none');    
    }
    
    function addMoreWhatWeDo()
    {
        $('#addMore').addClass('d-none');
        $('#removeMore').removeClass('d-none');
        $('#clonediv').removeClass('d-none');
    }
    
    function openProjUploader(num)
    {
        $('#fileproj'+num).click();
    }
    
    function uploadVideo(num)
    {
        $('#videoFile'+num).click();
    }
    
    function uploadVideoThumbnail(num)
    {
        $('#videoThumb'+num).click();
    }
    
    function showProjFileLabel(num)
    {
        $('#fileprojlabel'+num).html('1 File Selected <i class="fa fa-times" onclick="removeProjFile('+num+')"></i>');
    }
    
    function removeProjFile(num)
    {
        const file = document.querySelector('#fileproj'+num);
        file.value = '';
        $('#fileprojlabel'+num).html('');
    }
    
    $('#video-language').tagsinput({
    	  typeahead: {
    		source: ['Afar', 'Abkhaz', 'Avestan', 'Afrikaans', 'Akan', 'Amharic','Aragonese', 'Arabic', 'Assamese', 'Avaric', 'Aymara', 'Azerbaijani','South Azerbaijani', 'Bashkir', 'Belarusian', 'Bulgarian', 'Bihari', 'Bislama',
    		'Bambara', 'Bengali', 'Tibetan Standard', 'Breton', 'Bosnian', 'Catalan; Valencian','Chechen', 'Chamorro', 'Corsican', 'Cree', 'Czech', 'Old Church Slavonic','Chuvash', 'Welsh', 'Danish', 'German', 'Maldivian', 'Dzongkha',
    		'Ewe', 'Greek', 'English', 'Esperanto', 'Spanish', 'Estonian','Basque', 'Persian ', 'Fula', 'Finnish', 'Fijian', 'Faroese',' French', 'Western Frisian', 'Irish', 'Scottish Gaelic', 'Galician', 'Guaraní',
    		'Gujarati', 'Manx', 'Hausa', 'Hebrew ', 'Hindi', 'Hiri Motu','Croatian', 'Haitian', 'Hungarian', 'Armenian', 'Herero', 'Interlingua',' Indonesian', 'Interlingue', 'Igbo', 'Nuosu', 'Inupiaq', 'Ido',
    		'Icelandic', 'Italian', 'Inuktitut', 'Japanese', 'Javanese', 'Georgian','Kongo', 'Gikuyu', 'Kwanyama', 'Kazakh', 'Kalaallisut', 'Khmer','Kannada', 'Korean', 'Kanuri', 'Kashmiri', 'Kurdish', 'Komi',
    		'Cornish', 'Kyrgyz', 'Latin', 'Luxembourgish', 'Ganda', 'Limburgish','Lingala', 'Lao', 'Lithuanian', 'Luba-Katanga', 'Latvian', 'Malagasy','Marshallese', 'Māori', 'Macedonian', 'Malayalam', 'Mongolian', 'Marathi',
    		'Malay', 'Maltese', 'Burmese', 'Nauru', 'Norwegian Bokmål', 'North Ndebele','Nepali', 'Ndonga', 'Dutch', 'Norwegian Nynorsk', 'Norwegian', 'South Ndebele','Navajo', 'Chichewa', 'Occitan', 'Ojibwe', 'Oromo', 'Oriya',
    		'Ossetian', 'Panjabi', 'Pāli', 'Polish', 'Pashto', 'Portuguese','Quechua', 'Romansh', 'Kirundi', 'Romanian', 'Russian', 'Kinyarwanda','Sanskrit', 'Sardinian', 'Sindhi', 'Northern Sami', 'Sango', 'Sinhala',
    		'Slovak', 'Slovene', 'Samoan', 'Shona', 'Somali', 'Albanian','Serbian', 'Swati', 'Southern Sotho', 'Sundanese', 'Swedish', 'Swahili','Tamil', 'Telugu', 'Tajik', 'Thai', 'Tigrinya', 'Turkmen',
    		'Tagalog', 'Tswana', 'Tonga', 'Turkish', 'Tsonga', 'Tatar','Twi', 'Tahitian', 'Uyghur', 'Ukrainian', 'Urdu	', 'Uzbek','Venda', 'Vietnamese', 'Volapük', 'Walloon', 'Wolof', 'Xhosa',
    		'Yiddish', 'Yoruba', 'Zhuang', 'Chinese', 'Zulu']
    	  },
    	  freeInput: true
    });	
    
    validateThumbFile = function(files,e){
        var flag = true;
        var imageType = /image.*/;  
        var file = files[0];
        // check file type
        if (!file.type.match(imageType)) {  
        alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
        flag = false;
        return false;	
        } 
        // check file size
        if (parseInt(file.size / 1024) > (1024*2)) {  
        alert("File \""+file.name+"\" is too big.");
        flag = false;
        return false;	
        } 
        
        if(flag == true){
            uploadImageFile(file);
        }else{
            return flag;
        }	
        
    };
    
    uploadImageFile = function(file){
        var formdata = new FormData();
        formdata.append("image", file);	
        //console.log(file);
        xhr = new XMLHttpRequest();
        //console.log(xhr);
        xhr.open("POST", "upload/temp_image");
        xhr.onload  = function() {
        var jsonResponse = xhr.response;
        console.log(jsonResponse);
        result = JSON.parse(jsonResponse);
        console.log(result);
        if(result.status == true){
            
        }else{
            alert('File is not uploading. Please try again.')
        }	  
        };	
        var csrfToken = $('#artistForm').find('input[name=_token]').val();
        xhr.setRequestHeader('X-csrf-token', csrfToken); 	
        xhr.send(formdata);		
    }
    
    function check(e,value)
    {
        //Check Charater
        var unicode=e.charCode? e.charCode : e.keyCode;
        if (value.indexOf(".") != -1)if( unicode == 46 )return false;
    }
    
    function checkLength(len,ele){
      var fieldLength = ele.value.length;
      if(fieldLength <= len){
        return true;
      }
      else
      {
        var str = ele.value;
        str = str.substring(0, str.length - 1);
        ele.value = str;
      }
    }
    
</script>
@endsection