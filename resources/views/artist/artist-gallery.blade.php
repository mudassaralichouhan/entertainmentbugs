@extends('layouts.home')

@section('meta_tags')
  
    <meta name="description" content="The gallery keeps getting updated by the artists.">
    <meta name="keywords" content="Artist Gallery, Search Artist Gallery, Find Artist Gallery, Influencer, Actor / Model, Artist portfolio, Artist website">
    <meta name="author" content="<?php echo ((isset($author) ? $author : 'Artist Gallery - Entertainment Bugs')); ?>">
    <meta property="og:site_name" content="{{URL::to('/')}}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="<?php echo ((isset($ogtitle) ? $ogtitle : 'Artist Gallery - Entertainment Bugs')); ?>"/>
    <meta property="og:description" content="<?php echo ((isset($ogdescription) ? $ogdescription : 'The gallery keeps getting updated by the artists.')); ?>"/>
    <meta name="theme-color" content="#f82e5e">
    <meta property="og:keywords" content="Artist Gallery, Search Artist Gallery, Find Artist Gallery, Influencer, Actor / Model, Artist portfolio, Artist website">
    <meta property="og:url" content="<?php echo url()->current(); ?>"/>
    <meta property="og:image" content="<?php echo ((isset($ogimage) ? $ogimage : 'https://entertainmentbugs.com/balu_development/public/uploads/logo/logo-social-media.png')); ?>"/><!--profile image-->
    
   @endsection

@section('content')
    <style>
        #live-video-user-list-home {
            display: none
        }
        #home_page_video .v-img img {
    height: auto;
}
    </style>



    <!--<link href="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/css/example-styles.css" rel="stylesheet">-->
    <!--<script src="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/js/jquery.multi-select.js"></script>-->
    <!--<link href="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/css/example-styles.css" rel="stylesheet">-->
    <!--<script src="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/js/jquery-2.2.4.min.js"></script>-->
    <!--<script src="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/js/jquery.multi-select.js"></script>-->



<link href="https://www.entertainmentbugs.com/public/new-js/example-styles.css" rel="stylesheet">
<script src="https://www.entertainmentbugs.com/public/new-js/jquery-2.2.4.min.js"></script>
<script src="https://www.entertainmentbugs.com/public/new-js/jquery.multi-select.js"></script>



    {{ HTML::style('public/css/bootstrap-tagsinput.css') }}
    {{ HTML::script('public/js/bootstrap-tagsinput.js') }}
    {{ HTML::script('public/js/bootstrap3-typeahead.js') }}




    <style>
        .oad091 a {
            color: #f72e5e !important
        }

        .multi-select-button {
            border: solid 1px #e0e1e2 !important;
            max-width: 94% !important;
            border-radius: 1px !important;
            box-shadow: 0 0px 0px rgb(0 0 0 / 20%) !important;
        }

        #rewards_main_page {
            padding: 40px 0 10px 0
        }

        #rewards_main_page ul li {
            font-size: 24px;
            line-height: 64px;
        }

        #home_page_video_new .cb-content {
            margin-bottom: 0;
        }

        #exampleInputEmail1 {
            height: 32px;
            padding: 4px 0 0 6px;
            font-size: 13px;
            width: 95%;
        }

        #video-language {
            height: 32px;
            padding: 4px 0 0 6px;
            font-size: 13px;
            width: 95%;
        }

        .form-control {
            height: 32px;
            padding: 4px 0 0 6px;
            font-size: 13px;
            width: 95%;
        }

        .bootstrap-tagsinput {
            height: 32px;
            padding: 4px 0 0 6px;
            font-size: 13px;
            width: 95%;
            overflow: hidden;
        }

        #location-cities {
            height: 32px;
            padding: 4px 0 0 6px;
            font-size: 13px;
            width: 95%;
        }

        .user {
            position: absolute;
            right: 0px;
            width: 40px;
            bottom: -25px;
        }

        #audition_main {
            padding: 10px 0 40px 0;
        }

        .audition_main_list h3 {
            font-size: 24px;
            text-transform: uppercase;
            text-align: center;
            margin: 40px 0 20px 0;
        }

        .cat-1 a {
            width: 19%;
            margin: 0.5%;
            float: left;
            background: #f72e5e !important;
            color: #fff;
            line-height: 100px;
            display: block;
            text-align: center;
            font-size: 16px;
        }

        .cat-1 {
            margin-bottom: 20px;
        }

        .cat-1 {
            border-top: solid 1px #eceff0;
            padding: 15px 10px 0px 10px;
        }

        #audition_main .content-block.head-div {
            border-top: solid 1px #eceff0;
        }

        #audition_main .home_page_second_block {
            border: 0px solid #e3e3e3;
        }

        #audition_main {
            background: #eaeaea
        }

        #casting-head {
            background: #fff
        }

        #casting-head {
            margin-bottom: 20px;
        }

        .photo-block {
            width: 14%;
            margin: 0 0%;
            padding: 0 4px;
            float: left
        }

        .cb-content {
            clear: both;
            overflow: hidden
        }

        .photo-block img {
            width: 100%;
        }

        .home_page_second_block {
            margin-top: 20px;
        }

        .middle-head {
            font-size: 22px;
            margin: 30px 0 0 -13px
        }

        .casting-home-sep {
            padding-top: 20px;
        }

        .casting-home-sep .home_page_second_block {
            margin-top: 13px;
        }

        #audition_main .casting-home-sep .content-block.head-div {
            border-top: solid 0px #eceff0;
        }

        #blog {
            padding: 0 20px
        }

        .top-banner {
            position: absolute;
            left: 100px;
            top: 25%;
            width: 45%;
        }

        .top-banner h2 {
            color: #fff;
            font-size: 44px;
        }

        .top-banner a {
            background-color: #f72e5e;
            color: white;
            padding: 7px 0;
            border: solid 1px #f72e5e;
            margin-top: 10px;
            font-size: 18px;
            border-radius: 24px;
            font-weight: 500;
            display: block;
            width: 209px;
            text-align: center;
            height: 48px;
            line-height: 36px;
        }

        .casting-menu {
            width: 100%;
        }

        .casting-menu ul {
            list-style: none;
            padding: 0px;
            text-align: center;
            background: #f72e5e;
            margin: 0px;
        }

        .casting-menu ul li {
            display: inline-block;
            text-align: center;
        }

        .casting-menu ul li a {
            letter-spacing: 0.1px;
            color: #fff;
            font-size: 12px;
            padding: 16px 18px 13px 18px;
            display: block;
        }



        .casting-menu ul li a:hover {
            background: #c42047
        }

        #result-block-main .photo-block {
            width: 20%;
        }

        .casting-search {
            width: 19%;
            float: left;
            background: #fff;
            padding: 0px;
            20px;
        }

        .casting-search-result {
            width: 80%;
            float: right
        }

        .casting-cat-block a {
            display: block;
            margin-left: 15px;
            margin-bottom: 3px;
        }

        .casting-cat-block {
            margin-bottom: 10px
        }

        #video-search-cast .casting-cat-block h4 {
            background: #fff;
            color: #f72e5e !important;
            padding: 5px 10px 2px 0px;
            font-size: 13px;
            margin: 0 0 1px 0px;
        }

        #video-search-cast {
            width: 100%;
            margin-bottom: 10px;
            padding: 15px 15px 7px 15px;
        }

        #video-search-cast .casting-cat-block {
            float: left;
            width: 20%
        }

        #video-search-cast .casting-cat-block.extra-small-fil {
            float: left;
            width: 13%
        }

        #video-cast-all-vide {
            width: 100%;
        }

        #video-cast-all-vide .v-desc p {
            margin: 0px;
            padding: 0px;
            font-size: 12px;
        }

        #home_page_video .b-video .v-desc {
            height: 56px;
            margin-bottom: 15px;
            padding: 10px;
            background: #fff;
        }

        #home_page_video .v-desc a {
            font-size: 12.5px;
            line-height: 18px;
            margin: px 0 0 0;
            display: block;
        }

        #video-cast-all-vide #home_page_video .col-lg-2 {
            width: 16.6%;
        }

        #result-block-main .user {
            position: absolute;
            right: 0px;
            width: 40px;
            bottom: -25px;
        }

        .home_page_seventh_block {
            background: none;
            padding: 0;
            margin-top: 10px;
            border: 0px solid #e3e3e3;
        }

        select {
            border: 1px solid #e0e1e2 !important;
            color: #7e7e7e;
        }

        .multi-select-button {
            border: 1px solid #e0e1e2;
            border-radius: 0px;
            box-shadow: 0 0px 0px rgb(0 0 0 / 20%);
            height: 31px;
            width: 100%;
            max-width: 95%;
        }

        .multi-select-container {
            width: 99% !important;
            border-radius: 0px !important;
            line-height: 26px;
        }

        .multi-select-menuitem {
            display: block;
            font-size: 0.875em;
            padding: 0.6em 1em 0.6em 30px;
            white-space: nowrap;
            font-weight: normal;
            padding: 1px 31px;
            margin: 1px 0 -6px 0;
        }

        .advanced-result {
            display: none;
        }
    </Style>

    <style>
        .pagination {
            display: inline-block;
            padding-left: 0;
            margin: 22px 0;
            border-radius: 4px;
            width: 100%;
            text-align: center;
        }


        .pagination li a {
            color: black;
            float: none;
            padding: 5px 16px 1px 15px;
            text-decoration: none;
            transition: background-color .3s;
            text-align: center;
            font-size: 15px;
        }

        .pagination li span {
            color: black;
            float: none;
            padding: 5px 16px 1px 15px;
            text-decoration: none;
            transition: background-color .3s;
            text-align: center;
            font-size: 15px;
        }

        .pagination .active {
            background-color: #f72e5e;
            color: white;
        }

        .pagination>.active>a,
        .pagination>.active>span,
        .pagination>.active>a:hover,
        .pagination>.active>span:hover,
        .pagination>.active>a:focus,
        .pagination>.active>span:focus {
            background-color: #f72e5e;
            color: white;
            border-color: #f72e5e;
        }

        .pagination>.disable {
            pointer-events: none;
        }

        .content-block .cb-content {
            margin-bottom: 0;
        }

        .pagination a:hover:not(.active) {
            background-color: #ddd;
        }
    </style>


    <div class="content-wrapper menu-top1">
        <?php
                         $userid=session()->get('user_id');
     if($userid != ''){
            $artist_about=\App\Models\ArtistAbout::where('user_id',$userid)->get();
                    $progress = 0;
                 if($artist_about->count() > 0 ){
                     $progress = 1;
                 }
                 if($progress){
                     ?>
        <style>
            #user-menu {
                display: none
            }
        </style>

        <div class="containers" id="">
            <div class="casting-menu">
                <ul>
                    <li><a href="{{ URL::to('artist-profile') }}" class="mobile-artist-profile"> Artist Profile</a></li>
                    <li><a href="{{ URL::to('artist-videos') }}" class="mobile-artist-videos"> Artist Videos</a></li>
                    <li><a href="{{ URL::to('artist-gallery') }}" class="mobile-artist-gallery"> Artist Gallery</a></li>
                    <li><a href="">Influencer</a></li>
                    <li><a href="">Actor / Model</a></li>
                    <!--<li><a href="{{ URL::to('production-houses') }}">Production House</a></li>-->
                    <li><a href="{{ URL::to('find-auditions') }}">Autions</a></li>
                </ul>
            </div>
        </div>
        <?php 
                 } 
                 
                     $product_about=\App\Models\ProductionAbout::where('user_id',$userid)->get();
                $progress = 0;
                if( $product_about->count() > 0){
                     $progress = 1;
                 }
                if($progress){
                ?>
        <style>
            #user-menu {
                display: none
            }
        </style>

        <?php 
                }
            }//check if user login ?>

        <div class="containers" id="user-menu">
            <div class="casting-menu">
                <ul>
                    <li><a href="{{ URL::to('artist-profile') }}"> Artist Profile</a></li>
                    <li><a href="{{ URL::to('artist-videos') }}"> Artist Videos</a></li>
                    <li><a href="{{ URL::to('artist-gallery') }}"> Artist Gallery</a></li>
                    <li><a href="">Influencer</a></li>
                    <li><a href="">Actor / Model</a></li>
                    <li><a href="{{ URL::to('production-houses') }}">Production House</a></li>
                    <li><a href="{{ URL::to('find-auditions') }}">Find Autions</a></li>
                    <li><a href="{{ URL::to('post-audition') }}">Post Aution</a></li>
                </ul>
            </div>
        </div>
    </div>


    <div class="content-wrapper">
        <div class="audition_main_list" id="audition_main">
            <div class="audition_main_list" id="audition_main">
                <div class="container">
                   @include('artist.inc.artist-gallery-filters')
               </div>
               <div data-gallery="content">
                   @include('artist.inc.artist-gallery-data')
               </div>
            </div>
        </div>
    </div>
    </div>

    <script type="text/javascript">
        function getMoreGalleryImages(page) {
            var selectedCategory = $("#category :selected").map(function(i, el) {
                return $(el).val();
            }).get();
            var selectedGender = $("#gender option:selected").val();
            var selectedLanguage = $("#video-language :selected").map(function(i, el) {
                return $(el).val();
            }).get();
            var selectedLocations = $("#location-cities").val();
            var selectedfollowers = $("#followers-filter option:selected").val();
            var selectedMinExperience = $("#min-experience option:selected").val();
            var selectedMaxExperience = $("#max-experience option:selected").val();
            var selectedMinAge = $("#min-age").val();
            var selectedMaxAge = $("#max-age").val();
            var selectedMinHeight = $("#min-height").val();
            var selectedMaxHeight = $("#max-height").val();
            var selectedMinWeight = $("#min-weight").val();
            var selectedMaxWeight = $("#max-weight").val();
            var artistName = $("#artist-name-search").val();



            $.ajax({
                type: "GET",
                url: "{{ route('api.search.artist-gallery.re-render') }}" + "?page=" + page,
                data: {
                    'category': selectedCategory,
                    'gender': selectedGender,
                    'lang': selectedLanguage,
                    'city': selectedLocations,
                    'followers': selectedfollowers,
                    'min_experience': selectedMinExperience,
                    'max_experience': selectedMaxExperience,
                    'min_age': selectedMinAge,
                    'max_age': selectedMaxAge,
                    'min_height': selectedMinHeight,
                    'max_height': selectedMaxHeight,
                    'min_weight': selectedMinWeight,
                    'max_weight': selectedMaxWeight,
                    'artist_name': artistName,
                },
                success: function(data) {
                    var cat = "";
                    selectedCategory.forEach(function(item) {
                        cat += "&category[]=" + item;
                    });
                    var lang = "";
                    selectedLanguage.forEach(function(item) {
                        lang += "&lang[]=" + item;
                    });
                    $('#audition_main [data-gallery="content"]').html(data);
                    // initializefields();
                    var full = window.location.href.split('?')[0];
                    var finalurl = full + "?page=" + page + cat + "&gender=" + selectedGender + lang +
                        "&city=" + selectedLocations + "&followers=" + selectedfollowers + "&min_experience=" +
                        selectedMinExperience + "&max_experience=" + selectedMaxExperience + '&min_age=' +
                        selectedMinAge + '&max_age=' + selectedMaxAge + '&min_height=' + selectedMinHeight +
                        '&max_height=' + selectedMaxHeight + '&min_weight=' + selectedMinWeight +
                        '&max_weight=' + selectedMaxWeight + '&artist_name=' + artistName;

                    window.history.pushState(full, '{{ $title }}', finalurl);

                }
            });
        }

        function initializefields() {
            $(document).on('click', '.pagination a', function(event) {
                event.preventDefault();
                var page = $(this).attr('href').split('page=')[1];
                getMoreGalleryImages(page);
            });


            $("#gender").on('change', function() {
                getMoreGalleryImages(1);
            });


            $("#video-language").on('change', function() {
                getMoreGalleryImages(1);
            });

            $("#location-cities").on('change', function() {
                getMoreGalleryImages(1);
            });


            $("#followers-filter").on('change', function() {
                getMoreGalleryImages(1);
            });

            $("#min-experience").on('change', function() {
                getMoreGalleryImages(1);
            });

            $("#max-experience").on('change', function() {
                getMoreGalleryImages(1);
            });

            $("#min-age").on('change', function() {
                getMoreGalleryImages(1);
            });

            $("#max-age").on('change', function() {
                getMoreGalleryImages(1);
            });

            $("#min-height").on('change', function() {
                getMoreGalleryImages(1);
            });

            $("#max-height").on('change', function() {
                getMoreGalleryImages(1);
            });

            $("#min-weight").on('change', function() {
                getMoreGalleryImages(1);
            });

            $("#max-weight").on('change', function() {
                getMoreGalleryImages(1);
            });

            $("#artist-name-search").on('change', function() {
                getMoreGalleryImages(1);
            });

            $("#advanced").on('click', function() {
                $(".advanced-result").toggle(200);
            });
            $('#category').on('change', function() {
                getMoreGalleryImages(1);
            });
            $('#video-language').multiSelect();

            $('#category').multiSelect();

            $('#location-cities').keyup(function() {
                var elem = $(this);
                if (elem.val().length >= 2) {
                    elem.clearQueue().stop().delay(1000).queue(function() {
                        $.ajax('{{ route('api.search.cities.list') }}', {
                            type: 'GET', // http method
                            data: {
                                query: elem.val()
                            }, // data to submit
                            dataType: 'json',
                            success: function(data, status, xhr) {
                                var out = '';
                                data.forEach(function(item, index, arr) {
                                    out += '<option value="' + item.name + '">';
                                })
                                $('#list-location-cities').html(out);

                            },
                            error: function(jqXhr, textStatus, errorMessage) {
                                alert('Error' + errorMessage);
                            }
                        });

                    });
                }
            });

            $('body#artist-name-search').keyup(function() {
                var elem = $(this);
                if (elem.val().length >= 2) {
                    elem.clearQueue().stop().delay(1000).queue(function() {
                        $.ajax('{{ route('api.search.artist.list') }}', {
                            type: 'GET', // http method
                            data: {
                                query: elem.val()
                            }, // data to submit
                            dataType: 'json',
                            success: function(data, status, xhr) {
                                var out = '';
                                data.forEach(function(item, index, arr) {
                                    out += '<option value="' + item.profile_name + '">';
                                })
                                $('#list-artist-name-search').html(out);

                            },
                            error: function(jqXhr, textStatus, errorMessage) {
                                alert('Error' + errorMessage);
                            }
                        });

                    });
                }
            });

        }


        initializefields();
    </script>
@endsection
