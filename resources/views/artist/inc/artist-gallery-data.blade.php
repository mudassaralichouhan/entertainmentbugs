<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <!-- Featured Videos -->
            <div class="content-block">
                <div class="casting-search-result" id="video-cast-all-vide">
                    <div class="cb-content" style="padding-top:5px;">
                        <div class="row">
                            @include('artist.inc.artist-gallery-filters-info')

                            <div class="col-lg-12 no-psd-desk0">
                                <!-- Featured Videos -->
                                <div class="content-block">
                                    <div class="cb-content videolist" id="home_page_video">
                                        @foreach($Images as $Image)
                                            <div class="col-lg-2 col-sm-6 videoitem">
                                                <div class="b-video">
                                                    <div class="v-img">
                                                        <a href="{{ !empty($Image->username) ? url($Image->username."/gallery/".$Image->_slug) : '#' }}"><img src="{{ $Image->photo }}" alt="" height="150"></a>
                                                        <div class="user">
                                                            <a href="{{ url("artist/".$Image->username) }}" class="user_img" target="">
                                                                <img src="{{$Image->profile_photo}}" alt="{{$Image->profile_name}}"></a>
                                                        </div>
                                                    </div>
                                                    <div class="v-desc">
                                                        <p>{{$Image->category}}</p>
                                                        <a href="#">{{$Image->photo_title}}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <div style="clear:both; overflow:hidden; height:20px"></div>
                                        {{$Images->links()}}
                                    </div>
                                </div>
                                <!-- /Featured Videos -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--casting video END-->
        </div>
    </div>
    <!-- /Featured Videos -->
</div>