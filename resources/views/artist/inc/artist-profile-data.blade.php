   
            <div class="row" style="padding: 15px 20px;">
                   <div  class="col-lg-12">
                      @include('artist.inc.artist-profile-filters-info')
                   </div> 
               </div>  
               
               
               <div id="only-mbl" class="result-show "> 
             <div class="casting-cat-filters">
                 <input class="form-control" list="list-artist-name-search" id="artist-name-search"
                     placeholder="Search Artist by name" autocomplete="off">
                 <datalist id="list-artist-name-search">
                 </datalist>
             </div>
             @if (isset($request['artist_name']))
                 <?php echo '<script>$("#artist-name-search").val("' . $request['artist_name'] . '")</script>'; ?>
             @endif 
  </div>
 
               <div class=" grid">
                 
                 

<style>
#ads-banner-img{width:100% !Important;}
#ads-banner-img-content{width:100% !Important;    height: 103px;}
#ads_block_profile .tags_white{display:none;}
#ads_block_profile .tags_language{display:none;}
#ads_block_profile .ads-trigger-btn{    float: none; bottom:2px !Important; width: 64%  !Important; padding: 5px 0 0 0 !Important;height: 21px !Important;}
#ads_name{bottom:2px !Important}
#ads-banner-img-content h6{    font-size: 12px !important;padding: 0px 5px 2px 0 !important; margin-top: -3px !important;}
    </style>             
<div class="col-sm-3 photo-block" id="ads_block_profile">
<div data-ad-block data-ad-type='ad' data-ad-mode='image' data-ad-size='1080x1080'></div>
</div>


                    @foreach($artists as $artist)
                  
                        
              
  <div class="col-sm-3 photo-block">
                            <a href="{{ !empty($artist->username) ? artist_url($artist->username) : "#" }}" class="profile-p">
                            <img src="{{$artist->profile_photo}}" height="150" alt="{{$artist->profile_name}}">
                            </a>
                            <div class="info-p p-inner">
                                <h5 class="artist-name-overflow">{{$artist->profile_name}}</h5>
                                <div class="cat"><i class="fa fa-map-marker"></i> {{$artist->city}} | {{$artist->state}} <br/>
                                <!--{{$artist->country}}-->
                               <p class="lang"> {{$artist->language}},&nbsp <br/></p> Age: {{$artist->age}}
                            @if( $artist->user->isPremium() || $artist->user->hasThemePlan() )
                                <!--Only display premium users  (Premium Profile - Theme Plan)--> 
                                    <div id="show-star">
                                    <img src="{{ url('/public/web_img/raising_star_pink.gif') }}" id="raising-star_profile_brand_color">
                                    <img src="{{ url('/public/web_img/raising_star_white.gif') }}" id="raising-star_profile_white_color">
                                </div>
                                <!--Only display premium users  (Premium Profile - Theme Plan)-->
                            @endif
                       
                                </div>
                            </div>
                    </div>
                   @endforeach
               </div>