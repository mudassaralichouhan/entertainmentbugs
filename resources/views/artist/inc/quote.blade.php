<div id="quote" class="tabcontent" style="padding-top: 15px; background: rgb(244, 243, 243);margin-top: 0px;">
    <form action="{{ url('artist/save/quote') }}" method="post">
        {{ csrf_field() }}
        <div class="container">
            <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                <h2>Quote</h2>
                <div class="form-group col-lg-12">
                    <label>Author Name</label>
                    <input type="text" name="author_name" class="form-control" value="{{ !$artist_quote ? '' : $artist_quote->name }}">  
                </div>
                <div class="form-group col-lg-12">
                    <label> Description (50 words)</label>
                    <textarea id="author_quote" name="author_quote" onKeyPress="return check(event,value)" onInput="checkLength(50,this)" rows="4" cols="50">{{ !$artist_quote ? '' : $artist_quote->quote }}</textarea> 
                </div>
                <div class="col-lg-2"><button type="submit" class="btn btn-cv1">Save</button></div>
            </div>
        </div>`
    </form>
</div>
<script>
    $(function(){
        const container = $("#quote");

        container.find("form").on("submit", function(e){
            e.preventDefault();

            const formData = new FormData();
            formData.append("_token", container.find("[name='_token']").val());
            formData.append("author_name", container.find("[name='author_name']").val());
            formData.append("author_quote", container.find("[name='author_quote']").val());

            $.ajax({
                url: $(this).attr('action'),
                type: 'post',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: function(res) {
                    if(res.status == 1) {
                        alert('Data Saved Successfully');
                        location.reload();
                    } else {
                        alert('Something went wrong');
                    }
                },
                error: function() {
                    alert('Something went wrong');
                }
            });
        });
    });
</script>