<style>
    .brand-01:nth-child(even){background:rgb(244, 243, 243);border: solid 1px #e0e1e2; padding-top: 13px;}
    .brand-01:nth-child(odd) .form-group {margin-bottom: 14px;}
.logo-01:nth-child(odd){width:49%;float:left;padding:10px 0 0 0;background:rgb(244, 243, 243);border: solid 1px #e0e1e2;}
.logo-01:nth-child(even){width:49%;float:right;padding:10px 0 0 0;background:rgb(244, 243, 243);border: solid 1px #e0e1e2;}
.logo-01 .form-group {margin-bottom: 17px;}



  @media  only screen and (max-width: 767px) {
 
#croppie-modal-31 .cr-viewport.cr-vp-square{width:274.5px !Important;height:292.5px !Important;}
#croppie-modal-31 .cr-boundary{width:274.5px !Important;height:292.5px !Important;} 
#croppie-modal-32 .cr-viewport.cr-vp-square{width:274.5px !Important;height:292.5px !Important;}
#croppie-modal-32 .cr-boundary{width:274.5px !Important;height:292.5px !Important;} 

#croppie-modal-33 .cr-viewport.cr-vp-square{width:261px !Important;height:135px !Important;}
#croppie-modal-33 .cr-boundary{width:261px !Important;height:135px !Important;} 

#croppie-modal-34 .cr-viewport.cr-vp-square{width:274.5px !Important;height:292.5px !Important;}
#croppie-modal-34 .cr-boundary{width:274.5px !Important;height:292.5px !Important;} 
#croppie-modal-35 .cr-viewport.cr-vp-square{width:274.5px !Important;height:292.5px !Important;}
#croppie-modal-35 .cr-boundary{width:274.5px !Important;height:292.5px !Important;} 


#croppie-modal-36 .cr-viewport.cr-vp-square{width:261px !Important;height:135px !Important;}
#croppie-modal-6 .cr-boundary{width:261px !Important;height:135px !Important;} 

.mbl-ad{width:40% !Important;    margin-bottom: 12px;}
.mbl-ad1{width:60% !Important;}
      
.mbl-ad1 label, .mbl-ad label  {
    margin-bottom: 0;
}

#mbl-gesa{    font-size: 21px !Important;
    text-align: left !Important;
    margin: 42px 0 14px 0 !Important;}

.form-group {
    margin-bottom: 17px;
}
  }


</style>
	@php
			
					$products = $user->hasThemePlan() ? 6 : 3;
				  
				@endphp
				
<div id="brand_collaboration" class="tabcontent" style="padding-top: 15px; background: rgb(244, 243, 243);margin-top: 0px;">
	<form action="{{ url('artist/save/brand-collaboration') }}" method="post">
		{{ csrf_field() }}
		<div class="container">
			<div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
				<h2> Brand Collaborations</h2>
				<!--<h2 style="font-size: 14px;">Brand Name</h2>-->
			    @if(count($artist_brand)>0)
			    
				@for($i = 0; $i < 2; $i++)
			
					<div class="logo-01" data-brand>
						<div class="form-group col-lg-4" style="position:relative">
							<label for="">Brand Logo {{ $i + 1 }}</label>
							<input type="text" id="brand_{{ $i }}_name" name="brand[{{ $i }}][name]" class="form-control" placeholder="" value="{{ !$artist_brand || !isset($artist_brand[$i]) ? '' : $artist_brand[$i]->brand_name }}" />
						</div>
						<div class="u-close col-lg-4 mbl-ad" id="select-p" style="float:left;"> 
							<label for="">Logo Upload <span style="font-size:10px">Size: 345 x 120</span></label><br>
							<button type="button" data-croppie data-croppie-file="#brand_{{ $i }}_file"
							data-croppie-progress="#artist-brands-progress-{{ $i }}"
							data-croppie-input="#brand_{{ $i }}_logo" data-croppie-output="#brand_{{ $i }}_preview" data-croppie-bind="file"
							data-croppie-viewport='{"width": 345, "height": 120}' data-croppie-boundary='{"width": 345, "height": 120}'>Upload Logo</button>
							<input type="file" class="d-none" target="" id="brand_{{ $i }}_file" accept="image/x-png,image/gif,image/jpeg" style="padding-top: 8px;" />
							<input type="hidden" id="brand_{{ $i }}_logo" name="brand[{{ $i }}][logo]" value="{{ !$artist_brand || !isset($artist_brand[$i]) ? '' : $artist_brand[$i]->brand_logo }}" />
						</div> 
						<div class="u-close col-lg-4 mbl-ad1" style="float:left;padding: 22px 0 0 30px;">
							<img id="brand_{{ $i }}_preview" src="{{ !$artist_brand || !isset($artist_brand[$i]) ? '' : $artist_brand[$i]->brand_logo }}" style="width:115px;height:40px;object-fit:cover;margin-right:2px;">
                            <div class="u-progress" id="artist-brands-progress-{{ $i }}" style="display: none; width: 115px">
								<div class="progress">
									<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
										<span class="sr-only" data-progress="text">0% Complete</span>
									</div>
								</div>
							</div>
							<a id="" href="https://entertainmentbugs.com/delete-team-image/22" style="margin-top: 8px;display: block;float: right;margin-right: 12px;"><i class="cvicon-cv-cancel"></i></a> 
						</div>
					</div>
				@endfor
			
				<div class="clear"></div>
				<h2 id=mbl-gesa"" style="font-size: 14px;margin-top:20px;">Brand Products</h2>
			
				@if($products)
				@for($i = 0; $i < $products; $i++)
				
				    @php $product = null; $brand = 0; @endphp
					@if( is_var_exist($artist_brand, 'brand_products') )
				
						@php
						    if(isset($artist_brand[0]->brand_products[$i]) && $artist_brand[0]->brand_products[$i]->active == 1){
    							$product = $artist_brand[0]->brand_products[$i];
						    }
						@endphp
					@endif
					@if( !$product && is_var_exist($artist_brand, 'brand_products', 1) )
					    @php
						    if(isset($artist_brand[1]->brand_products[$i]) && $artist_brand[1]->brand_products[$i]->active == 1){
    							$product = $artist_brand[1]->brand_products[$i];
    							$brand = 1;
						    }
					    @endphp
					@endif
					@php
					    if(!(isset($product->title))){
    						$product = new stdClass();
    						$product->title = '';
    						$product->button_label = '';
    						$product->url = '';
    						$product->brand = '';
    						$product->banner = '';
    						$product->logo = '';
    						
    						$artist_brand[0]->brand_products[$i]->active = 1;
	    				}
	    			@endphp
					<div class="brand-01" style="margin-top:10px" data-product="{{ $i }}">
						<div class="form-group col-lg-2" style="position:relative">
							<label for="brand_title">Title</label>
							<input type="text" class="form-control" name="title" placeholder="4 words limit" value="{{ $product->title }}" />
						</div>
						<div class="form-group col-lg-2">
							<label for="button_name">Button Name</label><br>
							<select style="width:100%" name="button_text">
								<option>Select</option>
								<option {{ $product->button_label == 'Shop Now' ? 'selected' : '' }}>Shop Now</option>
								<option {{ $product->button_label == 'Book Now' ? 'selected' : '' }}>Book Now</option>
								<option {{ $product->button_label == 'View Now' ? 'selected' : '' }}>View More</option>
								<option {{ $product->button_label == 'Apply Now' ? 'selected' : '' }}>Apply Now</option>
								<option {{ $product->button_label == 'Others' ? 'selected' : '' }}>Others</option>
								<option {{ $product->button_label == 'Visit Now' ? 'selected' : '' }}>Visit Now</option>
							</select>
						</div>
						<div class="form-group col-lg-3" style="position:relative">
							<label for="button_link">Button link</label>  
							<input type="text" class="form-control" name='button_link' placeholder="Link paste" value="{{ $product->url }}" >
						</div>
						<div class="form-group col-lg-2">
							<label for="tagged_to">Tagged to</label><br>
							<select style="width:100%" name='brand'>
								<option {{ $brand == 0 ? "selected" : "" }}>Brand Logo 1</option>
								<option {{ $brand == 1 ? "selected" : "" }}>Brand Logo 2</option>
							</select>
						</div>
						<div class="form-group col-lg-2">
							<div class="u-close" id="" style="float:left; margin-right:20px;"> 
								<label for="">Banner Upload <span style="font-size:10px"></span></label><br>
								@php
									$dimension = [
										"width" => $i == 2 || $i == 3 ? 870 : 422,
										"height" => 450
									];
									$jsonDimesion = json_encode($dimension);
								@endphp
								<button type="button" data-croppie data-croppie-file="#product_file_{{ $i }}"
								data-croppie-input="#product_input_{{ $i }}" data-croppie-output="#product_preview_{{ $i }}" data-croppie-bind="file"
								data-croppie-viewport='{{ $jsonDimesion }}' data-croppie-boundary='{{ $jsonDimesion }}'
								data-croppie-modal-key="bcp-{{ $i }}" data-croppie-progress='#artist-product-progress-{{ $i }}'>Upload Logo</button>
								<input type="file" class="d-none" target="" id="product_file_{{ $i }}" accept="image/x-png,image/gif,image/jpeg" style="padding-top: 8px;">
								<input type="hidden" id="product_input_{{ $i }}" name="product_logo" value="{{ $product->logo }}" >
							</div>
						</div>
						<div class="u-close" style="float:left;">
							<img id="product_preview_{{ $i }}" src="{{ $product->banner }}" style="width:50px;height:50px;object-fit:cover;margin-right:2px;">
                            <div class="u-progress" id="artist-product-progress-{{ $i }}" style="display: none; width: 50px">
								<div class="progress">
									<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
										<span class="sr-only" data-progress="text">0% Complete</span>
									</div>
								</div>
							</div>
							<a id="" href="#" style="margin-top: 2px;display: block;float: right;margin-left: 8px;"><i class="cvicon-cv-cancel"></i></a>
						</div>
						
							<div class="clear"></div>
					</div>
				
				@endfor
				@endif
					@endif
				<div class="btn-save col-lg-2" >
					<button type="submit" class="btn-cv1">Save</button>
				</div>
			</div>
		</div>
	</form>
</div>
<script>
	$(function(){
		const brands = Array.apply(null, Array(2)).map(() => new Brand());
		const container = $("#brand_collaboration");
		const productContainer = container.find(".brand-01");

		@if( $artist_brand )
			const collection = {!! !$artist_brand ? '{}' : json_encode($artist_brand) !!};

			try {
				if( /^\[(.+)\]$/.test(collection) ){
					// collection = JSON.parse(collection);
					if( collection ){
						collection.forEach((brand, key) => {
							brands[key].name = brand.brand_name;
							brands[key].logo = brand.brand_logo;

							brand.brand_products.forEach((product, index) => {
								brands[key].products[index].title = product.title;
								brands[key].products[index].buttonText = product.button_label;
								brands[key].products[index].buttonLink = product.url;
								brands[key].products[index].logo = product.banner;
								brands[key].products[index].logoSrc = product.banner;
								brands[key].products[index].active = product.active;
							});
						})
					}
				}
			} catch(error){}
		@endif

		function Brand(){
			this.name = "";
			this.logo = "";
			this.products = Array.apply(null, Array(6)).map(() => new Product());
		}

		function Product(){
			this.title = "";
			this.buttonText = "";
			this.buttonLink = "";
			this.logo = "";
			this.logoSrc = "";
			this.active = 0;
		}

		function getElemValue(elem){
			return $(elem).val().trim();
		}

		productContainer.find("[name='title']").on('change', function(){
			const product = $(this).closest("[data-product]");
			const index = product.data('product');
			const activeBrand = product.find("[name='brand']")[0].selectedIndex;
			brands[activeBrand].products[index].title = getElemValue(this);
		});

		productContainer.find("[name='button_text']").on('change', function(){
			const product = $(this).closest("[data-product]");
			const index = product.data('product');
			const activeBrand = product.find("[name='brand']")[0].selectedIndex;
			brands[activeBrand].products[index].buttonText = getElemValue(this);
		});

		productContainer.find("[name='button_link']").on('change', function(){
			const product = $(this).closest("[data-product]");
			const index = product.data('product');
			const activeBrand = product.find("[name='brand']")[0].selectedIndex;
			brands[activeBrand].products[index].buttonLink = getElemValue(this);
		});

		$("[data-product] [name='brand']").on("change", function(){
			const product = $(this).closest('[data-product]');
			const index = product.data('product');
			const activeBrand = $(this).get(0).selectedIndex;
			
			brands[activeBrand].products[index].title = product.find("[name='title']").val();
			brands[activeBrand].products[index].buttonText = product.find("[name='button_text']").val();
			brands[activeBrand].products[index].buttonLink = product.find("[name='button_link']").val();
			brands[activeBrand].products[index].logo = getElemValue(product.find("[name='product_logo']").get(0));
			brands[activeBrand].products[index].logoSrc = $("#product_preview_"+index).attr('src');
			brands[activeBrand].products[index].active = 1;
			
			brands.forEach(function(brand, i){
			    if(i != index){
			        brands[i].products[index].active = 0;
			    }
			});

// 			product.find("[name='title']").val(activeProduct.title);
// 			product.find("[name='button_link']").val(activeProduct.buttonLink);
// 			product.find("[name='product_logo']").val(activeProduct.logo);
// 			$("#product_preview_"+index).attr('src', activeProduct.logoSrc);
			
// 			const optionToSelect = product.find("[name='button_text'] option").filter(function(){
// 				return $(this).val().trim() == brands[activeBrand].products[index].buttonText;
// 			});
			
// 			if( optionToSelect.length ){
// 				optionToSelect.prop('selected', true);
// 			} else{
// 				product.find("[name='button_text'] option").eq(0).prop('selected', true);
// 			}
		});

		$(document).on("click", "[data-croppie-cbkey^='bcp-']", function(){
			const key = $(this).data('croppieCbkey');
			const elem = productContainer.find(`[data-croppie-modal-key='${key}']`);

			function setLogo(count = 0){
				setTimeout(() => {
					const product = elem.closest("[data-product]");
					const index = product.data('product');
					const activeBrand = product.find("[name='brand']")[0].selectedIndex;
					brands[activeBrand].products[index].logo = getElemValue(product.find("[name='product_logo']").get(0));
					brands[activeBrand].products[index].logoSrc = $("#product_preview_"+index).attr('src');
					
					if( $("#product_preview_"+index).attr('src') == '' && count < 10 ){
						setLogo(++count);
					}
				}, 2000);
			}
			
			setLogo();
		});

		container.find("form").on("submit", function(e){
			e.preventDefault();

			const formData = new FormData();
			formData.append("_token", container.find("[name='_token']").val());
			brands.forEach((v, i) => {
				formData.append(`brand[${i}][name]`, $(`#brand_${i}_name`).val());
				formData.append(`brand[${i}][logo]`, $(`#brand_${i}_logo`).val());

				v.products.forEach((_v, _i) => {
					formData.append(`brand[${i}][product][${_i}][title]`, _v.title);
					formData.append(`brand[${i}][product][${_i}][button_text]`, _v.buttonText);
					formData.append(`brand[${i}][product][${_i}][button_link]`, _v.buttonLink);
					formData.append(`brand[${i}][product][${_i}][logo]`, _v.logo);
					formData.append(`brand[${i}][product][${_i}][active]`, _v.active);
				});
			});

			$.ajax({
				url: $(this).attr('action'),
				type: 'post',
				data: formData,
				contentType: false,
				cache: false,
				processData: false,
				success: function(res){
					if(res.status == 1) {
						alert('Data Saved Successfully');
						location.reload();
					} else {
						alert('Something went wrong');
					}
				},
				error: function(){
					alert('Something went wrong');
				}

			});
		});
	});
</script>