 <h1 class="left-align-heading" style="font-size: 20px;text-align: center;margin: 1px 0 16px 0;color: #f72e5e;"> Search Artist Video </h1> 

 <div id="only-mbl" class="filter-right">
    <a href="javascript:void(0)" class="filter"> <i class="fa fa-filter"></i> Filter Option</a>
 </div>
 
 
  
         <div id="only-mbl" class="result-show "> 
           <div class="casting-cat-filters"> 
                    <div class="casting-cat-filter" style="padding:0px">
                        <input class="form-control" list="list-artist-name-search" id="artist-name-search"
                               placeholder="Search Artist Gallery by name" autocomplete="off">
                        <datalist id="list-artist-name-search">
                        </datalist>
                    </div>
                    @if(isset($request['artist_name']))
                        <?php echo '<script>$("#artist-name-search").val("'.$request['artist_name'].'")</script>'; ?>
                    @endif
                </div>
          </div>
             
                      
 <style>
 #only-mbl{display:none;}
 
 
@media  only screen and (max-width: 767px) {
.casting-search{display:none;}
#only-mbl{display:block;}
#only-mbl .casting-cat-filters{margin-bottom:10px;}
.left-align-heading{width:62%;float:left;margin: 4px 0 15px 0 !Important; font-size: 18px !Important;}
.left-align-heading{text-align:left !important;}
.filter-right{width:38%;float:left}
#only-mbl a.filter{ margin-top: -4px;display: block!important;background: #f72e5e;width: 120px;float: right;padding: 5px 11px 1px 10px;font-size: 13px;color: #fff;text-align: center;}
#advanced{display:none;}
.advanced-result{display:block !Important;} 
.filters-content .row{    padding: 4px 20px !important;}
#only-mbl .form-control {padding: 4px 16px 0 15px;font-size: 13px;width: 100%;   height: 40px;}
#raising-star_profile_brand_color {position: absolute;top: 210px;}
#mbl-hide{display:none;}
.mobile-artist-videos{background: #c42047;} 
.portfolio.section .container{padding-left: 10px; padding-right: 10px}
#audition_main {padding: 20px 0 40px 0;}
    
}
 </style>
           
             
             
             
             
             
             
             
             
               <div class="casting-search" id="video-search-cast">
                <div class="casting-cat-block">
                    <h4>Category</h4>
                    <select id="category" class="extra-ma" name="category" multiple>
                        <option value="Theater">Theater</option>
                        <option value="Voiceover">Voiceover</option>
                        <option value="Comedian">Comedian</option>
                        <option value="Photography">Photography</option>
                        <option value="Video Grapher">Video Grapher</option>
                        <option value="Makeup Artrist"> Makeup Artrist</option>

                        <optgroup label="Actor / Model">
                            <option value="Actor">Actor</option>
                            <option value="Child Actor">Child Actor</option>
                            <option value="Comedy Actor">Comedy Actor</option>
                            <option value="Junior Actor">Junior Actor</option>
                            <option value="Stunt Artist">Stunt Artist</option>
                            <option value="Drama Artist">Drama Artist</option>
                            <option value="Mimicry Artist">Mimicry Artist</option>
                            <option value="Background / Atmosphere">Background / Atmosphere</option>
                        </optgroup>
                        <optgroup label="Modeling">
                            <option value="Model">Model</option>
                            <option value="Child Model">Child Model</option>
                            <option value="Pro Model">Pro Model</option>
                        </optgroup>
                        <optgroup label="Dancer">
                            <option value="Dancer">Dancer</option>
                            <option value="Lead Dancer">Lead Dancer</option>
                            <option value="Background Dancer">Background Dancer</option>
                        </optgroup>
                        <optgroup label="Editor">
                            <option value="Video Editor">Video Editor</option>
                            <option value="Music Editor">Music Editor</option>
                            <option value="Audio Editor">Audio Editor</option>
                        </optgroup>
                        <optgroup label="Writer">
                            <option value="Dialogue Writer">Dialogue Writer</option>
                            <option value="Script Writer">Script Writer</option>
                            <option value="Screeplay Writer">Screeplay Writer</option>
                            <option value="Story Writer">Story Writer</option>
                            <option value="Lyricists">Lyricists</option>
                        </optgroup>
                        <optgroup label="Anchor / Presenter">
                            <option value="Video Jockey (VJ)">Video Jockey (VJ)</option>
                            <option value="Radio Jockey (RJ)">Radio Jockey (RJ)</option>
                            <option value="News Reader">News Reader</option>
                            <option value="Reporter">Reporter</option>
                            <option value="Journalist">Journalist</option>
                        </optgroup>
                        <optgroup label="Off screen">
                            <option value="Dubbing Artist">Dubbing Artist</option>
                            <option value="Voice Over Artist">Voice Over Artist</option>
                        </optgroup>
                        <optgroup label="Influncer">
                            <option value="New Influncer">New Influncer</option>
                            <option value="Experienced Influncer">Experienced Influncer</option>
                        </optgroup>
                        <optgroup label="Advertising Professional">
                            <option value="Video Advertising">Video Advertising</option>
                            <option value="Audio Advertising">Audio Advertising</option>
                            <option value="Banner Advertising">Banner Advertising</option>
                        </optgroup>
                        <optgroup label="Singer / Musician">
                            <option value="Singer">Singer</option>
                            <option value="Music Instrumentalist">Music Instrumentalist</option>
                            <option value="Music Composer">Music Composer</option>
                        </optgroup>
                        <optgroup label="Proffessional Artist">
                            <option value="Director Of Photography / Cinematographer">Director Of Photography /
                                Cinematographer
                            </option>
                            <option value="Cameraman">Cameraman</option>
                            <option value="Camera Operator">Camera Operator</option>
                            <option value="Light man / Gaffer">Light man / Gaffer</option>
                            <option value="Makeup Artist">Makeup Artist</option>
                            <option value="Visual Effects Editor">Visual Effects Editor</option>
                            <option value="Digital imaging technician">Digital imaging technician</option>
                            <option value="Special Effects makeup Artist">Special Effects makeup Artist</option>
                            <option value="Motion control technician">Motion control technician</option>
                            <option value="Fashion Designer">Fashion Designer</option>
                            <option value="Hair Stylist">Hair Stylist</option>
                            <option value="Costume designer">Costume designer</option>
                            <option value="Grip">Grip</option>
                            <option value="Sound Designer">Sound Designer</option>
                            <option value="Sound Grip">Sound Grip</option>
                            <option value="Production Sound Mixer">Production Sound Mixer</option>
                            <option value="Production Designer">Production Designer</option>
                            <option value="Green man">Green man</option>
                            <option value="Property master">Property master</option>
                            <option value="Weapons master">Weapons master</option>
                            <option value="Set Designer / Decorator">Set Designer / Decorator</option>
                            <option value="Location Manager">Location Manager</option>
                            <option value="StoryBoard Artist">StoryBoard Artist</option>
                        </optgroup>
                        <optgroup label="Others">
                            <option value="Others">Others</option>
                        </optgroup>
                    </select>
                    
                    @if(isset($request['category']))
                    <script>
                    @if(is_array($request['category']))
                        @foreach($request['category'] as $cat)
                            $("#category option").each(function(){
                              if ($(this).text() == "{{$cat}}")
                                $(this).attr("selected","selected");
                            });
                        @endforeach
                    @else
                        $("#category option").each(function(){
                              if ($(this).text() == "{{$request['category']}}")
                                $(this).attr("selected","selected");
                            });
                    @endif
                    </script>
                    @endif
                    
                </div>
                <div class="casting-cat-block">
                    <h4>Gender</h4>
                   <select required="" style="width:100%;padding:0 10px" id="gender">
                       <option>Select</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Kids-Male" >Kids - Male</option>
                                    <option value="Kids-Female">Kids - Female</option>
                                    <option value="Trans-Female">Trans Female</option>
                                    <option value="Trans-Male">Trans Male</option>
                                </select>
                </div>
                @if(isset($request['gender']))
                            <?php echo '<script>$("#gender").val("'.$request['gender'].'")</script>'; ?>
                            @endif
                
                <div class="casting-cat-block">
                    <h4> Language</h4>
                     <select id="video-language" class="extra-ma" style="width:100%;" name="language[]" multiple>
                                    @foreach(\App\Models\Language::all() as $language)
                                        @if(isset($request['lang']))
                                            @foreach($request['lang'] as $lang)
                                                @if($lang==$language->name)
                                                    <option value="{{$language->name}}" selected>{{$language->name}}</option>
                                                @else 
                                                    <option value="{{$language->name}}">{{$language->name}}</option>
                                                @endif
                                            @endforeach
                                        @else
                                                <option value="{{$language->name}}">{{$language->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                </div>

                            
                <div class="casting-cat-block">
                    <h4>Location</h4>
                    <input list="list-location-cities" class="form-control" id="location-cities" name="location-cities"
                           placeholder="city: multiple" autocomplete="off">
                    <datalist id="list-location-cities">
                    </datalist>
                </div>
                @if(isset($request['city']))
                    <?php echo '<script>$("#location-cities").val("'.$request['city'].'")</script>'; ?>
                @endif


                <div class="casting-cat-block">
                    <h4>Followers</h4>
                    <select style="width: 92%;" id="followers-filter">
                        <option>Select</option>
                        <option value="99">0 to 100k</option>
                        <option value="100">100k</option>
                        <option value="200">200k</option>
                        <option value="400">400k</option>
                        <option value="500">500k</option>
                        <option value="1000">1000k</option>
                        <option value="2000">2000k</option>
                        <option value="5000">5000k</option>
                        <option value="10000">10000k</option>
                        <option value="All">All</option>
                    </select>
                </div>
                @if(isset($request['followers']))
                <?php echo '<script>$("#followers-filter").val("'.$request['followers'].'")</script>'; ?>
                @endif
                <div class="clear"></div>
                <a href="javascript:void(0)" id="advanced" style="float:right;margin: -3px 15px 3px 0;">+ Advanced
                    Search</a>
                <div class="clear"></div>
                <Style>
                    .advanced-result {
                        display: none;
                    }
                </Style>
                <div class="advanced-result">
                    <div class="casting-cat-block">
                        <h4>Experience</h4>
                        <select style="width:46%; float:left; margin-right:3%"  id="min-experience">
                            <option>Min:</option>
                            
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                            <option value="32">32</option>
                            <option value="33">33</option>
                            <option value="34">34</option>
                            <option value="35">35</option>
                            <option value="36">36</option>
                            <option value="37">37</option>
                            <option value="38">38</option>
                            <option value="39">39</option>
                            <option value="40">40</option>
                        </select>
                         @if(isset($request['min_experience']))
                        <?php echo '<script>$("#min-experience").val("'.$request['min_experience'].'")</script>'; ?>
                        @endif
                        <select style="width:46%; float:left; margin-right:1%"  id="max-experience">
                            <option>Max:</option>
                              <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>
                            <option value="32">32</option>
                            <option value="33">33</option>
                            <option value="34">34</option>
                            <option value="35">35</option>
                            <option value="36">36</option>
                            <option value="37">37</option>
                            <option value="38">38</option>
                            <option value="39">39</option>
                            <option value="40">40</option>
                        </select>
                         @if(isset($request['max_experience']))
                        <?php echo '<script>$("#max-experience").val("'.$request['max_experience'].'")</script>'; ?>
                        @endif
                    </div>
                    <div class="casting-cat-block">
                        <h4>Age</h4>
                        <div class="casting-cat-filter" style="padding:0px">
                            <input  style="width:46%; float:left; margin-left:0%" id="min-age" type="number" min="10">
                            @if(isset($request['min_age']))
                            <?php echo '<script>$("#min-age").val("'.$request['min_age'].'")</script>'; ?>
                            @endif
                            <input  style="width:46%; float:left; margin-left:0%" id="max-age" type="number" min="10" max="100">
                            @if(isset($request['max_age']))
                            <?php echo '<script>$("#max-age").val("'.$request['max_age'].'")</script>'; ?>
                            @endif
                        </div>
                    </div>
                    <div class="casting-cat-block">
                        <h4>Height</h4>
                        <div class="casting-cat-filter" style="padding:0px">
                             <input  style="width:46%; float:left; margin-left:0%" id="min-height" type="number" min="10">
                            @if(isset($request['min_height']))
                            <?php echo '<script>$("#min-height").val("'.$request['min_height'].'")</script>'; ?>
                            @endif
                            <input  style="width:46%; float:left; margin-left:0%" id="max-height" type="number" min="0">
                           
                            @if(isset($request['max_height']))
                            <?php echo '<script>$("#max-height").val("'.$request['max_height'].'")</script>'; ?>
                            @endif
                        </div>
                    </div>
                    <div class="casting-cat-block">
                        <h4>Weight</h4>
                        <div class="casting-cat-filter" style="padding:0px">
                             <input  style="width:46%; float:left; margin-left:0%" id="min-weight" type="number" min="0">
                            @if(isset($request['min_weight']))
                            <?php echo '<script>$("#min-weight").val("'.$request['min_weight'].'")</script>'; ?>
                            @endif
                             <input  style="width:46%; float:left; margin-left:0%" id="max-weight" type="number" min="0">
                            @if(isset($request['max_weight']))
                            <?php echo '<script>$("#max-weight").val("'.$request['max_weight'].'")</script>'; ?>
                            @endif
                        </div>
                    </div>
                    <div class="casting-cat-block" id="mbl-hide">
                        <h4>Artist name</h4>
                        <div class="casting-cat-filter" style="padding:0px">
                            <input class="form-control" list="list-artist-name-search" id="artist-name-search"
                                   placeholder="Search by name"  autocomplete="off">
                            <datalist id="list-artist-name-search">
                            </datalist>
                        </div>
                        @if(isset($request['artist_name']))
                            <?php echo '<script>$("#artist-name-search").val("'.$request['artist_name'].'")</script>'; ?>
                        @endif
                    </div>
                </div>
            </div>
            
            
             
            
             <script>
    $(document).ready(function() {
 
  $(".filter").on('click', function() {
                $(".casting-search").toggle(200);
            });
            });
            
</script>