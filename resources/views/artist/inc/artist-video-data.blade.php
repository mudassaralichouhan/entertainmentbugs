<div class="row">
    @include('artist.inc.artist-video-filters-info')

    <div class="col-lg-12 no-psd-desk0">
        <!-- Featured Videos -->
        <div class="content-block">
         
        <style>
#ads-banner-img{width:50% !Important;}
#ads-banner-img-content{width:49% !Important;}
 .ads-trigger-btn{right:22% !Important;}
    </style>    
            
            <div class="cb-content videolist" id="home_page_video">
  
            <div class="col-sm-4 col-lg-4 " style="padding:0 5px 0px 5px;">
                
                <div data-ad-block="" data-ad-type="ad" data-ad-mode="image" data-ad-size="1080x1080" data-ad-loaded="1" value="19"><div class="ads-cont" style="background:#fff;position:relative;"><div id="ads-banner-img" style="width: 40%;float:left;">
    <a href="" data-ad-button="https://www.mycareerbugs.com/" target="_blank">
        <img src="https://www.entertainmentbugs.com/public/ads/second.png" style="width:100%;">
    </a>
</div>
<div id="ads-banner-img-content" style="width: 58%;float:right;padding:8px 4px 0 4px;">
    <h6 style="font-size: 12px ;margin:0px  !important;padding: 2px 5px 7px 0;font-family: arial;font-weight: bold;line-height: 13px;">
        Register as MCB candidate for quality work profile!
    </h6>
    <p style="font-size: 10.5px;line-height: 12px;padding-right: 10px;margin: 0 0 8px 0;font-family: arial;">
        How's your approach towards job hunting?
    </p>
    <div class="ads-banner-tag">
        <!--<span style="border:1px solid  #edecec;padding: 5px 6px 1px 6px;font-size: 8px;margin-bottom: 7px;display: inline-block;">Film &amp; Animation, Entertainment, Comedy</span>-->
        <div class="clearfix"></div>
        <span class="tags_language" style="background: #edecec;padding: 2px 6px 0px 6px;font-size: 10px;margin-right:2px;"></span>
        <span class="tags_white" style="background: #edecec;padding: 2px 6px 0px 6px;font-size: 9.5px;margin-right:2px;">mycareerbugs,mcb,jobs</span>
  
 <small id="ads_name" style="border:1px solid #edecec;border-radius:5px;position: absolute;bottom: 11px;right: 3%;font-size: 10px;padding: 5px 13px 0px 13px;background: #edecec;">Ad</small>
    </div>
</div>
  <a href="" class="ads-trigger-btn" data-ad-button="https://www.mycareerbugs.com/" style="float: none;font-size: 10px;color: #fff;margin: 5px 5px 0 0;background: #f72e5e;position: absolute;bottom: 10px;width: 25%;right: 31%;text-align: center;line-height: 17px;padding: 5px 0 0 0;">Visit Now <i class="fa fa-external-link" aria-hidden="true"></i></a>
<div class="clearfix"></div>
</div></div>
            </div>
            
                @foreach ($videos as $video)
                    <div class="col-sm-2 col-lg-2 ">
                        <div class="b-video">
                            <div class="v-img">
                                <a href="{{ !(empty($video->username)) ? url($video->username . '/video/' . $video->_slug) : '#' }}">
                                    <img src="{{ $video->thumb }}" alt="{{ $video->video_title }}" height="150"></a>
                                <div class="user">
                                    <a href="{{ url('artist/' . $video->username) }}" class="user_img">
                                        <img src="{{ $video->profile_photo }}" alt="{{ $video->profile_name }}"></a>
                                </div>
                            </div>
                            <div class="v-desc">
                                <p>{{ $video->category }}</p>
                                <a href="{{ $video->video }}">{{ $video->video_title }}</a>
                            </div>
                        </div>
                    </div>
                @endforeach


                <div style="clear:both; overflow:hidden; height:20px"></div>


                {{ $videos->links() }}

                <div style="clear:both; overflow:hidden"></div>
            </div>
        </div>
        <!-- /Featured Videos -->
    </div>
</div>
