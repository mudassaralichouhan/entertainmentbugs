@extends('layouts.home')

@section('meta_tags')
    <meta name="description" content="{{ $user->about ?? '' }}" >
    <meta name="keywords" content="{{ $user->name ?? '' }}" >
    <meta property="og:title" content="{{ $user->name ?? '' }} Vidoes" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:description" content="{{ $user->about }}" />
    <meta property="og:image" content="{{ asset('public/uploads/users/small/'.$user->photo) }}" />
@endsection
@section('content')
    <section class="channel light">
        <style>
         
   
  
         .b-video .v-desc {
      height: 39px;
    overflow: hidden;
    margin-bottom: 4px;
}
            .b-video .v-desc {
                color: #343434;
                font-weight: 500;
                padding-top: 7px;
            }

            .v-desc a {
                font-size: 13px;
                display: block;
                line-height: 18px;
                margin-bottom: 4px;
            }

            .b-video {
                position: relative;
                margin-bottom: 2px;
                min-height: 198px;
            }

            .profile {
              
                min-height: 355px;
                display: inline-block;
                width: 100%;
            }

            figcaption.ratings {
                margin-top: 20px;
            }

            figcaption.ratings a {
                color: #f1c40f;
                font-size: 11px;
            }

            figcaption.ratings a:hover {
                color: #f39c12;
                text-decoration: none;
            }

            .divider {
                border-top: 1px solid rgba(0, 0, 0, 0.1);
            }

            .emphasis {
                border-top: 4px solid transparent;
            }

            .emphasis:hover {
                border-top: 4px solid #1abc9c;
            }

            .emphasis h2 {
                margin-bottom: 0;
            }

            .c-details ul.list-inline>li:first-child a {
                border-bottom: 2px solid #f72e5e;
                color: #f72e5e;
            }


            @media only screen and (max-width: 767px) {

                .b-video .v-desc {
                    height: 42px;
                    padding: 6px 0 0 0;
                    overflow: hidden;
                }

                .v-desc a {
                    font-size: 12px;
                    line-height: 15px;
                    display: block;
                }

                .b-video {
                    position: relative;
                    margin-bottom: 14px;
                    min-height: auto;
                }
            }
    	    .c-details ul.list-inline > li:first-child a {
                border-bottom: 2px solid #f72e5e;
                color: #f72e5e;
            }
        </style>
        @if(!$user->isDeleted)
        @include('elements.header_profile')
      
        <div class="content-wrapper">
            <div class="container">
                <div class="row">
                    <div class="profile" style="padding:20px 0">
                        @if (!empty($videoData))
                            @php $isLoggedInUser = $user->id == Session::get('user_id'); @endphp
                            @foreach ($videoData as $key => $video)
                                @include('elements.post.video', ['key' => $key, 'isLoggedInUser' => $isLoggedInUser])
                            @endforeach
                            {{ $videoData->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>
          @else
          

        <div class="under-review" style="width:100%;  background: #eaeaea !important">
            
            <div class="container" id="no0dek">
    <img src="https://entertainmentbugs.com/public/img/under_review.png" alt="This profile is under review" style="width: 100%;margin:10px auto 0;display:block">
         </div>
         
          
         
         
         </div>
        @endif
    </section>
@endsection
