@extends('layouts.inner')
@section('content')

   <section class="my-profiles-section">
            <div class="container">
                <h2>Madan Jangid</h2>
                <p>0 Points</p>

            </div>
        </section>
        <section class="my-profiles-detail-section">
            <div class="container">
                <div class="mypor-bx">
                    <div class="row">
                        @include('elements.leftmenu') 
                        <div class="col-sm-8 col-md-8">
                            <div class="reservation-points" id="title1">
                                <div class="content-block-header" id="title2">
                                    <h3>Points</h3>
                                    <div class="content-block-header-icons">
                                        <span class="color-dark">Your Points:</span>
                                        <span class="color-light">0</span> 
                                        <span class="points-res-pts">PTS</span>
                                    </div>
                                </div>
                                <div class="content-block-body">
                                    <div class="columns">
                                        <div class="column-left">
                                            <span class="points-res-label">Earned</span>
                                            <span class="points-res-num ng-binding">0</span> <span class="points-res-pts margin-left-tiny">PTS</span>
                                        </div>
                                        <div class="column-right">
                                            <span class="points-res-label">Next Reward</span>
                                            <span class="points-res-num ng-binding">2,000</span> <span class="points-res-pts margin-left-tiny">PTS</span>
                                        </div>
                                    </div>
                                    <div class="padding-top-bar">
                                        <div class="points-bar"><span></span></div>
                                    </div>
                                   
                                </div>
                                <div class="content-block-footer">
                                    <a href="#">Learn more about Ls Table Booking Rewards</a>
                                </div>
                            </div>
                             <div class="reservation-points" id="title3">
                                <div class="content-block-header">
                                    <h3>Upcoming Reservations</h3>
                                </div>
                                <div class="content-block-footer">
                                    No Upcoming Reservations <a href="#">Book a Table.</a>
                                </div>
                            </div>
                             <div class="reservation-points" id="title4">
                                <div class="content-block-header">
                                    <h3>Past Reservations</h3>
                                </div>
                                <div class="content-block-footer">
                                   No Past Reservations
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
  
@endsection

 