@extends('layouts.home')
@section('content')

    <style>#live-video-user-list-home{display:none}
        #social_log{margin: -28px 0 29px 0}
        #spacer-extra{display:none;}
        .login-wraper::before{background:#f72e5e; content:""; position:absolute; left:0px; width:50%;}

    </style>
    <div class="container-fluid bg-image" style="background: url(http://entertainmentbugs.com/dev2020/public/img/login_bg.jpg) no-repeat;background-size: cover;margin: 0;">
        <div class="row">
            <div class="login-wraper">
                <!--      <div class="hidden-xs">-->
            <!--{{HTML::image('public/img/login.jpg')}}-->
                <!--      </div>-->
                <!--<div class="banner-text hidden-xs">-->
                <!--    <div class="b-text">-->
                <!--        Watch millions of Video for free-->
                <!--    </div>-->
                <!--    <div class="overtext">-->
                <!--        Over 6000 videos uploaded Daily.-->
                <!--    </div>-->
                <!--</div>-->
                <div class="login-window">
                    <div class="l-head">
                        Reset Password
                    </div>
                    <div class="l-form">
                        <div class="error-message">@include('elements.errorSuccessMessage')</div>
                        {{ Form::open(array('method' => 'post', 'id' => 'loginform', 'class' => 'form form-signin')) }}
                        <form action="login.html">
                            <div class="form-group">
                                <label for="exampleInputEmail1">New Password</label>
                                {{Form::password('password', ['class'=>'form-control required', 'placeholder' => 'New Password', 'minlength' => 8, 'id'=>'password'])}}
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Confirm Password</label>
                                {{Form::password('confirm_password', ['class'=>'form-control required', 'placeholder' => 'Confirm Password', 'equalTo' => '#password'])}}
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    {{Form::button('Login', ['type' => 'submit', 'class' => 'btn btn-cv1'])}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 forgottext" style="margin:10px 0 5px 0">
                                    <a href="{{URL::to('forgot-password')}}" style="display:block; text-align:center; margin:14px 0  6px 0">Forgot Username or Password?</a>
                                </div>
                            </div>
                            <div class="text-center mt-10">
                                <span class="forgottext"><a href="{{URL::to('register')}}">Don't have an account?</a></span>
                                <span class="signuptext"><a href="{{URL::to('register')}}">Signup here</a></span>
                            </div>


                            <!--                <div class="row visible-xs">-->
                            <!--                    <div class="col-xs-6">-->
                        <!--                        <div class="forgottext"><a href="{{URL::to('forgot-password')}}">Forgot Password?</a></div>-->
                            <!--                    </div>-->
                            <!--                    <div class="col-xs-6">-->
                        <!--<div class="signuptext text-right"><a href="{{URL::to('register')}}">Sign Up</a>-->
                            <!--</div>                          							-->
                            <!--</div>-->
                            <!--                </div>-->
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{ HTML::script('public/js/jquery.validate.js')}}
    <script type="text/javascript">
        $(document).ready(function () {
            $("#loginform").validate();
        });
    </script>

@endsection