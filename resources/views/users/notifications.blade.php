@extends('layouts.home')
@section('content')
<div id="bg-00"> 
    <div class="container">
	<style>
.noti{position:relative}
.thumbnail{width:75px;position:absolute; right:0px;top:10px;}
.thumbnail.photo-th{width:50px}
.thumbnail.stories-th{width:78px}
#bg-00 {background: #eaeaea;}
.user-search-individual{border-bottom:1px solid #ecebe6}
.notification_all h5 strong{ font-weight:600;  color: #f72e5e; }
.notification_all #new-profile{width:6%;float:left}
.notification_all #new-profile-details{width:92%;float:left; margin-left:1%}
.notification_all h5{ color: #000;   margin-bottom: 2px; font-family: 'Hind Guntur', sans-serif; font-size:14px; margin-top: 18px; font-weight:normal;letter-spacing: 0.2px;}

.notification_all p{font-size:12px;}
.notification_all a.flw{float:right; margin: 29px 0 0 0; background:#28b47e !important; color:#fff;  width: 90px;text-align: center; z-index: 99; position: relative; line-height: 27px; height: 25px;}
.checked, .fa-star-half-o {
    color: orange;
}

.fa.fa-check-square{color:#28b47e !important}
.notification_all{padding:10px 20px 20px 20px; background:#fff; margin:20px 0;}
.notification_all .channel-details{
	display: block;
}
</style>
		<div class="notification_all" id="user-search">
			@if( $notices && $notices->count() > 0 )
				@include('elements.notifications', ['notifications' => $notices])
				{{ $notices->links() }}
			@endif
		</div>
	</div>
</div>
@endsection