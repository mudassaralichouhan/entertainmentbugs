@extends('layouts.home')

@section('meta_tags')
   
    <meta name="description" content="Register to be found by millions around the globe. ">
    <meta name="keywords" content="Sign up, Register, Entertainment User, Artist, Production house, Advertise with us">
    <meta name="author" content="<?php echo ((isset($author) ? $author : 'Sign Up - Entertainment Bugs')); ?>">
    <meta property="og:site_name" content="{{URL::to('/')}}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="<?php echo ((isset($ogtitle) ? $ogtitle : 'Sign Up - Entertainment Bugs')); ?>"/>
    <meta property="og:description" content="<?php echo ((isset($ogdescription) ? $ogdescription : 'Register to be found by millions around the globe. ')); ?>"/>
    <meta name="theme-color" content="#f82e5e">
    <meta property="og:keywords" content="Sign up, Register, Entertainment User, Artist, Production house, Advertise with us">
    <meta property="og:url" content="<?php echo url()->current(); ?>"/>
    <meta property="og:image" content="<?php echo ((isset($ogimage) ? $ogimage : 'https://www.entertainmentbugs.com/public/uploads/logo/logo-social-media.png')); ?>"/><!--profile image-->
    
   @endsection
   
   
@section('content')

<style>#live-video-user-list-home{display:none}
#social_log{margin: -28px 0 29px 0}
#spacer-extra{display:none;}
.login-wraper::before{background:#f72e5e; content:""; position:Absolute; left:0px; width:50%;}

</style>

<div class="container-fluid bg-image" style="background: url(https://www.entertainmentbugs.com/public/img/login_bg.jpg) no-repeat;background-size: cover;margin: 0;">
   <div class="row">
       <div class="login-wraper">
           <!--<div class="hidden-xs">-->
           <!--    {{HTML::image('public/img/login.jpg')}}-->
           <!--</div>-->
           <!--<div class="banner-text hidden-xs">-->
           <!--    <div class="line"></div>-->
           <!--    <div class="b-text">-->
           <!--        Watch millions of Video for free-->
           <!--    </div>-->
           <!--    <div class="overtext">-->
           <!--        Over 6000 videos uploaded Daily.-->
           <!--    </div>-->
           <!--</div>-->
           <div class="login-window">
               <div class="l-head">
                   Sign Up for Free
               </div>
               <div class="l-form">
				<div class="error-message">@include('elements.errorSuccessMessage')</div> 
				  @include('elements/socialLogin')  
				  
                   {{ Form::open(array('method' => 'post', 'id' => 'registerform', 'class' => 'form form-signin')) }}
                       <div class="form-group">
                           <label for="exampleInputName">Name</label>
						   {{Form::text('name', null, ['class'=>'form-control required alphanumeric', 'placeholder'=>'Enter Full Name', 'autocomplete'=>'OFF', 'id' => 'exampleInputName'])}}
                       </div>
                       <div class="form-group">
                           <label for="exampleInputEmail1">Email</label>
						   {{Form::text('email', null, ['class'=>'form-control required email', 'placeholder'=>'sample@gmail.com', 'autocomplete'=>'OFF'])}}
                       </div>
                       <div class="form-group">
                           <label for="password">Password</label>
						    {{Form::password('password', ['class'=>'form-control required', 'placeholder' => '**********', 'minlength' => 8, 'id'=>'password'])}}
                       </div>
                       <div class="form-group">
                           <label for="confirmPassword">Re-type Password</label>
						   {{Form::password('confirm_password', ['class'=>'form-control required', 'placeholder' => '**********', 'equalTo' => '#password'])}}
                       </div>
                       <div class="row">
                           <div class="col-lg-12">    
							{{Form::button('Sign Up', ['type' => 'submit', 'class' => 'btn btn-cv1'])}}
						</div>
                       </div>
                       <div class="row hidden-xs">
                           <div class="col-lg-12 forgottext">
                               <a href="{{URL::to('login')}}">By clicking "Sign Up" I agree to our Terms of Service.</a>
                           </div>
                       </div>
                       <div class="text-center mt-10">
                           <br>
                           <span class="forgottext"><a href="{{URL::to('login')}}">Already have an account?</a></span>
                           <span class="signuptext"><a href="{{URL::to('login')}}">Login here</a></span>
                       </div>
                     
                   {{ Form::close()}}
               </div>
           </div>
       </div>
   </div>

</div>
{{ HTML::script('public/js/jquery.validate.js')}}
<script type="text/javascript">
    $(document).ready(function () {
        $("#registerform").validate();
    });
</script>
@endsection



