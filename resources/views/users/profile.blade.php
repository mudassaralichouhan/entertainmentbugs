@extends('layouts.home')
@section('content')

 
    @php
   
        $url_segment = \Request::segment(1);
    @endphp
    @if ($url_segment == 'videos')
        <style>
            .c-details ul.list-inline>li:first-child a {
                border-bottom: 2px solid #f72e5e;
                color: #f72e5e;
            }

            #single-my .videoitem {
                width: 20%;
                padding: 0 5px;
            }
        #language {
			height: 32px;
			padding: 4px 0 0 6px;
			font-size: 13px;
			width: 95%;
		}
        </style>
    @endif

    <style>
    
    #single-my{    padding: 15px 5px 6px 5px;
    background: #fff;
    margin-top: 20px;}
    #profile_photo_preview{    position: absolute;
    right: 68px;
    top: -15px;}
    
    #pm-post a{    width: 130px !Important;
    padding: 6px 3px 2px 3px !Important;
    position: absolute !Important;
    top: 59px !Important;
    font-size: 12px !Important;
    right: 0 !Important;}
    
    
.b-video .v-desc {
    height: 39px;
    overflow: hidden;
    margin-bottom: 3px;
}

        #single-my .videoitem {
            width: 20%;
            padding: 0 5px;
        }

        #single-my .col-lg-3.col-sm-6.videoitem .v-img {
            height: auto;
            overflow: hidden;
        }

        #single-my .profile .col-lg-3.col-sm-6.videoitem .v-img img {
            height: 100%;
            object-fit: cover;
        }


        .b-video .v-desc {
            color: #343434;
            font-weight: 500;
            padding-top: 7px;
        }

        .v-desc a {
            font-size: 13px;
            display: block;
            line-height: 16px;
            margin-bottom: 4px;
        }

        .b-video {
            position: relative;
            margin-bottom: 2px;
            min-height: 198px;
        }


        @media only screen and (max-width: 767px) {
            
                #single-my{   
    background: #fff;
    margin-top: 20px;}
    
    
    
              #profile_photo_preview{    position: relative;
    right: 0;
    top: 0;}
    
            #single-my .col-lg-3.col-sm-6.videoitem .v-img {
                height: 100px;
                overflow: hidden;
            }

            .b-video {
                position: relative;
                margin-bottom: 14px;
                min-height: auto;
            }

            #single-my .videolist .videoitem .b-video .v-desc {
                height: 41px;
                overflow: hidden;
            }
        }
    </style>
    <section class="channel light">
        @if (Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif
        @include('elements.header_profile')
        
        
 <!--       	<link href="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/css/example-styles.css" rel="stylesheet">-->
	<!--        <script src="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/js/jquery.multi-select.js"></script>-->
	<!--<link href="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/css/example-styles.css" rel="stylesheet">-->
	<!--<script src="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/js/jquery-2.2.4.min.js"></script>-->
	<!--<script src="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/js/jquery.multi-select.js"></script>-->
	
	
  <!--<script type='text/javascript' src='http://code.jquery.com/jquery-1.4.4.min.js'></script>-->

	
	<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>


        @if(!$user->isDeleted)
        <div class="content-wrapper">
            <div class="container">
                <div class="row">
                    <!--<div class='col-3'>-->
                    <!--    <div data-ad-block data-ad-type='ad' data-ad-mode='image' data-ad-size='1080x1080'></div>-->
                    <!--</div>-->
                    <div class="col-lg-12" id="no-ps091">
                        <!-- Featured Videos -->
                        <div class="content-block" style="" id="single-my">
                            <div class="cb-content videolist">

                                @if ($url_segment == session()->get('slug'))
                                    <div class="row">
                                        @if (!empty($videoData))
                                            @php $isLoggedInUser = $user->id == Session::get('user_id'); @endphp
                                            @foreach ($videoData as $key => $video)
                                                @include('elements.post.video', ['key' => $key, 'isLoggedInUser' => $isLoggedInUser])
                                            @endforeach
                                        @endif
                                    </div>
                                @endif
                                @if (Session::has('success'))
                                    <div class="alert alert-success">
                                        {{ Session::get('success') }}
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                
                                
                          
                                @if ($url_segment == 'edit-profile')
                                    <div class="row">
                                        <div class="login-window">
                                            <div class="l-form">
                                                {!! Form::open(['route' => 'profileAdd', 'id' => 'profile-upload', 'enctype' => 'multipart/form-data']) !!}
                                                <div>
                                                    <div
                                                        class="form-group  col-lg-4 col-sm-6 {{ $errors->has('name') ? 'has-error' : '' }}">
                                                        {!! Form::label('Name:*') !!}
                                                        {!! Form::text('name', $user->name, ['class' => 'form-control', 'required' => true, 'placeholder' => 'Name']) !!}
                                                        <span class="text-danger">{{ $errors->first('name') }}</span>
                                                    </div>
                                                    <div
                                                        class="form-group  col-lg-4 col-sm-6 {{ $errors->has('username') ? 'has-error' : '' }}">
                                                        {!! Form::label('Username:*') !!}
                                                        {!! Form::text('slug', $user->slug, [
                                                            'class' => 'form-control',
                                                            'required' => true,
                                                            'placeholder' => 'Username',
                                                        ]) !!}
                                                        <span class="text-danger">{{ $errors->first('username') }}</span>
                                                    </div>

                                                    <div
                                                        class="form-group col-lg-4 col-sm-6 {{ $errors->has('website') ? 'has-error' : '' }}">
                                                        {!! Form::label('Website:') !!}
                                                        {!! Form::text('website', $user->website, ['class' => 'form-control', 'placeholder' => 'Website']) !!}
                                                        <span class="text-danger">{{ $errors->first('website') }}</span>
                                                    </div>



                                                    <div
                                                        class="form-group col-lg-4 col-sm-6 {{ $errors->has('facebook') ? 'has-error' : '' }}">
                                                        {!! Form::label('Facebook:') !!}
                                                        {!! Form::text('facebook', $user->facebook, ['class' => 'form-control', 'placeholder' => 'Facebook']) !!}
                                                        <span class="text-danger">{{ $errors->first('facebook') }}</span>
                                                    </div>

                                                    <div
                                                        class="form-group col-lg-4 col-sm-6 {{ $errors->has('twitter') ? 'has-error' : '' }}">
                                                        {!! Form::label('Twitter:') !!}
                                                        {!! Form::text('twitter', $user->twitter, ['class' => 'form-control', 'placeholder' => 'Twitter']) !!}
                                                        <span class="text-danger">{{ $errors->first('twitter') }}</span>
                                                    </div>

                                                    <div
                                                        class="form-group col-lg-4 col-sm-6 {{ $errors->has('instagram') ? 'has-error' : '' }}">
                                                        {!! Form::label('Instagram:') !!}
                                                        {!! Form::text('instagram', $user->instagram, ['class' => 'form-control', 'placeholder' => 'Instagram']) !!}
                                                        <span class="text-danger">{{ $errors->first('instagram') }}</span>
                                                    </div>
                                                    
                                                     <div
                                                        class="form-group col-lg-4 col-sm-6 {{ $errors->has('country') ? 'has-error' : '' }}">
                                                        {!! Form::label('Country:*') !!}
                                                        
                                                        
                                                         <select id="country" name="country" required  class="select2 input-select" style="width:100%" >
                                                            <!--<option value="">Select Country</option>-->
                                                            <?php
                                                                if(count($countries) > 0){
                                                                foreach($countries as $k=>$v)
                                                                {
                                                                    $select = "";
                                                                    if($user->country_id == $v->id)
                                                                        $select = "selected";
                                                                ?>
                                                            <option value="<?php echo $v->id; ?>" {{$select}}><?php echo $v->name; ?></option>
                                                            <?php 
                                                            }
                                                                }
                                                            ?>
                                                        </select>
                                                        
                                                        @if($errors->has('country'))
                                                            <p class="alert alert-danger">{{ $errors->first('country') }}</p>
                                                        @endif                                         
                                                        </div>

                                                    <div
                                                        class="form-group col-lg-4 col-sm-6 {{ $errors->has('state') ? 'has-error' : '' }}">
                                                        {!! Form::label('State:*') !!}
                                                        
                                                        <select class="select2 input-select" style="width:100%" required id="state" name="state">
                                                            <option value="">Select State</option>
                                                           <?php
                                                                if(count($states) > 0){
                                                                foreach($states as $k=>$v)
                                                                {
                                                                    $select = "";
                                                                    if($user->country_id != $v['country_id'])
                                                                        continue;
                                                                    if($user->state_id == $v->id)
                                                                        $select = "selected";
                                                                ?>
                                                            <option value="<?php echo $v->id; ?>" {{$select}}><?php echo $v->name; ?></option>
                                                            <?php 
                                                            }
                                                                }
                                                            ?>
                                                        </select>
                                                         @if($errors->has('state'))
                                                            <p class="alert alert-danger">{{ $errors->first('state') }}</p>
                                                        @endif
                                                         
                                                    </div>

                                                    <div class="form-group col-lg-4 col-sm-6">
                                                        {!! Form::label('Language:*') !!}
          
            <input type="text" class="form-control" id ='video-language' name="language" placeholder="" value="{{$user->language}}">
                                     
                                      

                                                         @if($errors->has('language'))
                                                            <p class="alert alert-danger">{{ $errors->first('language') }}</p>
                                                        @endif
                                                        
                                                    </div>
                                                    
                                                     
                                                    
                                                    <div
                                                        class="form-group col-lg-6 col-sm-6 {{ $errors->has('email') ? 'has-error' : '' }}">
                                                        {!! Form::label('Email:') !!}
                                                        @if ($user->email_status == 1)
                                                            {!! Form::checkbox('email_status', '', true) !!}
                                                        @else
                                                            {!! Form::checkbox('email_status') !!}
                                                        @endif
                                                      <small style="font-size: 10px;"> select checkbox if you display your email id on profile </small>
                                                        {!! Form::email('email', $user->email, [
                                                            'class' => 'form-control',
                                                            'required' => true,
                                                            'placeholder' => 'Email',
                                                        ]) !!}

                                                        <span class="text-danger">{{ $errors->first('email') }}</span>
                                                    </div>

                                                    <div id="cover_image"
                                                        class="form-group col-lg-6 col-sm-6 {{ $errors->has('title') ? 'has-error' : '' }}">
                                                      
                                                        <label style="margin-bottom: 1px;" for="exampleInputEmail1"> Upload
                                                            profile photo</label><br>
                                                        <input type="file" target="upload_image" id="upload_image"
                                                            class="image custom-file-input image upload-cover-image"
                                                             />
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="upload_image">
                                                        
                                                          <div id="profile_photo_preview">
                                                            <img src="">
                                                        </div>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>

                                                </div>
                                                <div
                                                    class="form-group col-lg-12 {{ $errors->has('about') ? 'has-error' : '' }}">
                                                    {!! Form::label('About Me:*') !!}
                                                    {!! Form::textarea('about', $user->about, [
                                                        'class' => 'form-control',
                                                        'id' => 'summernote',
                                                        'placeholder' => 'About',
                                                        'required' => true,
                                                        'rows' => 6,
                                                        'cols' => 40,
                                                    ]) !!}
                                                    <span class="text-danger">{{ $errors->first('about') }}</span>
                                                    <div class="col-xs-12 text-right">
                                                        <span id="maxContentPost"></span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-2"><button type="submit"
                                                            class="btn btn-cv1">Save</button></div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>
            </div>
        </div>
        @else
        <div class="under-review"><h5>This profile is under review</h5></div>

        @endif
    
    
     
{{ HTML::style('public/css/bootstrap-tagsinput.css')}} 
{{ HTML::script('public/js/bootstrap-tagsinput.js')}}
{{ HTML::script('public/js/bootstrap3-typeahead.js')}}


<style>
    .typeahead strong{color:#000;}
.form-group select {

    border: solid 1px #e0e1e2;
    padding-left: 8px;
}
    #profile_photo {
                display: none;
            }

            .upload-cover-image {
                padding: 5px 46px;
                border: 2px dotted #ccc;
                font-size: 16px;
                color: #999;
                margin: 5px;
            }

            .upload-thumb-image {
                padding: 22px 46px;
                border: 2px dotted #ccc;
                font-size: 38px;
                color: #999;
                margin: 149px;
            }

            #cover_image img {
                max-width: 150px;
                max-height: 120px;
                margin: 0px;
            }

            .video-thumb-img {
                position: relative;
                display: inline-block;
                margin: 0px 5px;
            }

            .delete-video-thumb {
                position: absolute;
                right: 0px;
                color: #f72e5e;
                background-color: #fff;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                
              //  $('#language').multiSelect();
                
                $('#myModal').modal('show');
                var uri = window.location.toString();
                if (uri.indexOf("?") > 0) {
                    var clean_uri = uri.substring(0, uri.indexOf("?"));
                    window.history.replaceState({}, document.title, clean_uri);
                }

                $(".upload-cover-image").click(function() {
                    $("#profile_photo").trigger('click');
                });


       // $('#language').multiSelect();
        //$('.select2').select2();
        
        
        
        
         $('#country').change(function(){
            let country_id = $(this).val();
            

            $.ajax({
                type: "GET",
                url:"{{ route('country.state') }}",
                 data:{id:country_id,'_token':'<?php echo csrf_token(); ?>'},
                dataType: "JSON",
                success: function(data) {
                    var drop_down_date = null;
                    drop_down_date += '<option value="0" selected disabled>Select State</option>';
                    for (var i = 0; i < data.length; i++) {
                        drop_down_date += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                    }
                    $('#state').html(" ");
                    $('#state').html(drop_down_date);                
                    
                },
                error: function() {
                     var drop_down_date = null;
                    drop_down_date += '<option value="0" selected disabled>Select State</option>';
                    $('#state').html(" ");
                    $('#state').html(drop_down_date);  
                }
            });
            

        });
        
        

    $('form#profile-upload').validate({
        rules: {
            country: {
                required: true,
            },
             state: {
                required: true,
            },
             language: {
                required: true,
            },
          
        },
        messages : {
            country_id: {
                required: "Country is required",
            },
            state: {
                required: "State is required",
            },
            language: {
                required: "Language is required",
            },
        },

        errorPlacement: function(error, element) {

            if(element.hasClass('form-select') && element.next('.select2-container').length) {
                error.insertAfter(element.next('.select2-container'));
            } else if (element.parent('.input-group').length) {
                error.insertAfter(element.parent());
            }
            else if (element.prop('type') === 'radio' && element.parent('.radio-inline').length) {
                error.insertAfter(element.parent().parent());
            }
            else if (element.prop('type') === 'checkbox' || element.prop('type') === 'radio') {
                error.appendTo(element.parent().parent());
            }
            else {
                error.insertAfter(element);
            }
        }
    });

   
    
 
            /*Validate Thumb Image File*/
            validateThumbFile = function(files, e) {
                var flag = true;
                var imageType = /image.*/;
                var file = files[0];
				console.log(file.type);
				debugger;
                // check file type
                if (!file.type.match(imageType)) {
                    alert("File \"" + file.name + "\" is not a valid image, Please upload only jpg,png and gif image");
                    flag = false;
                    return false;
                }
                // check file size
                if (parseInt(file.size / 1024) > (1024 * 2)) {
                    alert("File \"" + file.name + "\" is too big.");
                    flag = false;
                    return false;
                }

                if (flag == true) {
                    uploadImageFile(file);
                } else {
                    return flag;
                }

            };

            /*Upload Image in Temp Folder*/
            uploadImageFile = function(file) {
                var formdata = new FormData();
                formdata.append("image", file);
                console.log(file);
				debugger;
                xhr = new XMLHttpRequest();
                //console.log(xhr);
                xhr.open("POST", "upload/temp_image");
                xhr.onload = function() {
                    var jsonResponse = xhr.response;
                    console.log(jsonResponse);
                    result = JSON.parse(jsonResponse);
                    console.log(result);
                    if (result.status == true) {
                        var fl = $('#cover_image').find('input[type=hidden]').length;
                        fl++;
                        var fNameHtml = '<input name="covers[' + fl + ']" type="hidden" id="video-thumb-' + fl +
                            '" value="' + result.file + '" />';
                        var imageHtml = '<a href="javascript:void(0)" key="' + fl +
                            '" class="video-thumb-img"><img src="' + result.image +
                            '" /><span class="delete-video-thumb"><i class="cvicon-cv-cancel"></i><span></a>';
                        $('#cover_image').append(fNameHtml);
                        $('.upload-cover-image').after(imageHtml);
                    } else {
                        alert('File is not uploading. Please try again.')
                    }
                };
                var csrfToken = $('#profile-upload').find('input[name=_token]').val();
                xhr.setRequestHeader('X-csrf-token', csrfToken);
                xhr.send(formdata);
            }
         
         
            });
 
            
        </script>
        
              <script>
        
        
        
          $(document).ready(function() {
        
        
        
            $.ajax({
                type: "GET",
                url:"{{ route('get.language') }}",
                 data:{'_token':'<?php echo csrf_token(); ?>'},
                dataType: "JSON",
                success: function(data) {
                               /// for(var )
                    
                },
                error: function() {
                    console.log('Data Not Found');
                }
            });
        
        
           $('#video-language').tagsinput({
    	  typeahead: {
    		source: ['Afar', 'Abkhaz', 'Avestan', 'Afrikaans', 'Akan', 'Amharic','Aragonese', 'Arabic', 'Assamese', 'Avaric', 'Aymara', 'Azerbaijani','South Azerbaijani', 'Bashkir', 'Belarusian', 'Bulgarian', 'Bihari', 'Bislama',
    		'Bambara', 'Bengali', 'Tibetan Standard', 'Breton', 'Bosnian', 'Catalan; Valencian','Chechen', 'Chamorro', 'Corsican', 'Cree', 'Czech', 'Old Church Slavonic','Chuvash', 'Welsh', 'Danish', 'German', 'Maldivian', 'Dzongkha',
    		'Ewe', 'Greek', 'English', 'Esperanto', 'Spanish', 'Estonian','Basque', 'Persian ', 'Fula', 'Finnish', 'Fijian', 'Faroese',' French', 'Western Frisian', 'Irish', 'Scottish Gaelic', 'Galician', 'Guaraní',
    		'Gujarati', 'Manx', 'Hausa', 'Hebrew ', 'Hindi', 'Hiri Motu','Croatian', 'Haitian', 'Hungarian', 'Armenian', 'Herero', 'Interlingua',' Indonesian', 'Interlingue', 'Igbo', 'Nuosu', 'Inupiaq', 'Ido',
    		'Icelandic', 'Italian', 'Inuktitut', 'Japanese', 'Javanese', 'Georgian','Kongo', 'Gikuyu', 'Kwanyama', 'Kazakh', 'Kalaallisut', 'Khmer','Kannada', 'Korean', 'Kanuri', 'Kashmiri', 'Kurdish', 'Komi',
    		'Cornish', 'Kyrgyz', 'Latin', 'Luxembourgish', 'Ganda', 'Limburgish','Lingala', 'Lao', 'Lithuanian', 'Luba-Katanga', 'Latvian', 'Malagasy','Marshallese', 'Māori', 'Macedonian', 'Malayalam', 'Mongolian', 'Marathi',
    		'Malay', 'Maltese', 'Burmese', 'Nauru', 'Norwegian Bokmål', 'North Ndebele','Nepali', 'Ndonga', 'Dutch', 'Norwegian Nynorsk', 'Norwegian', 'South Ndebele','Navajo', 'Chichewa', 'Occitan', 'Ojibwe', 'Oromo', 'Oriya',
    		'Ossetian', 'Panjabi', 'Pāli', 'Polish', 'Pashto', 'Portuguese','Quechua', 'Romansh', 'Kirundi', 'Romanian', 'Russian', 'Kinyarwanda','Sanskrit', 'Sardinian', 'Sindhi', 'Northern Sami', 'Sango', 'Sinhala',
    		'Slovak', 'Slovene', 'Samoan', 'Shona', 'Somali', 'Albanian','Serbian', 'Swati', 'Southern Sotho', 'Sundanese', 'Swedish', 'Swahili','Tamil', 'Telugu', 'Tajik', 'Thai', 'Tigrinya', 'Turkmen',
    		'Tagalog', 'Tswana', 'Tonga', 'Turkish', 'Tsonga', 'Tatar','Twi', 'Tahitian', 'Uyghur', 'Ukrainian', 'Urdu	', 'Uzbek','Venda', 'Vietnamese', 'Volapük', 'Walloon', 'Wolof', 'Xhosa',
    		'Yiddish', 'Yoruba', 'Zhuang', 'Chinese', 'Zulu']
    	  },
    	  freeInput: true,
		  
    });	
          });	
    
       </script>
        
        
        
        
        
        
        
        
        
        </div>
    </section>

    @php
        $status = app('request')->input('status');
    @endphp

    @if ($status == 'success')
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Success!</h4>
                    </div>
                    <div class="modal-body">
                        <h5>Your video has been submitted successfully.</h5>
                        <p>After further review of the video. we have determined that your video does violate our community
                            guidelines and have upheld our original decision. We appreciate your understanding.</p>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
