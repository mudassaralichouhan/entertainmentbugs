@extends('layouts.home')
@section('content')
<script type="text/javascript">
    $(document).ready(function () {
        $("#loginform").validate();
    });
</script>


<style>#live-video-user-list-home{display:none}
#social_log{margin: -28px 0 29px 0}
#spacer-extra{display:none;}
.login-wraper::before{background:#f72e5e; content:""; position:absolute; left:0px; width:50%;}
.btn.btn-primary{border-radius: 3px;border: solid 1px #f72e5e;background-color: #f72e5e;color: #ffffff;font-size: 18px;font-weight: 500;margin: 20px 0;padding: 13px; width: 100%;}
.login-wraper .login-window .l-head {padding-top: 25px;}
</style>


<div class="container-fluid bg-image" style="background: url(https://entertainmentbugs.com/public/img/login_bg.jpg) no-repeat;background-size: cover;margin: 0;">
 
   <div class="row">
<div class="detai_page detai_page_forgot">
    <!--<div class="top_pop"><div class="signn-logo"><a href="{!! HTTP_PATH !!}"> {{HTML::image(LOGO_PATH, SITE_TITLE)}}  </a></div></div>-->
    <div class="login-wraper">        
        <div class="login-window">
            <div class="l-head">
                   Forgot Password  
               </div>
               
               
            <div class="l-form">
            
   
            <div class="ee er_msg">@include('elements.errorSuccessMessage')</div>
            <div class="socila_login">
                <?php //echo $this->element('social_register', array('type' => 'register')); ?>
            </div>
            <div normal_login>
                {{ Form::open(array('method' => 'post', 'id' => 'loginform', 'class' => 'form form-signin')) }}
                <div class="login_fieldarea">
                    <div class="inputt">
                        <span class="fieldd">
                            <!--<i class="fa fa-envelope"></i>-->
                            {{Form::text('email_address', Cookie::get('user_email_address'), ['class'=>'form-control required', 'placeholder'=>'Email Address', 'autocomplete'=>'OFF'])}}
                        </span>
                    </div>
                    <div class="clear"></div>            
                    <div class="sign_in" id="sub_btn_dive">
                        {{Form::submit('Submit', ['class' => 'btn btn-primary btn-block btn-flat'])}}
                    </div>
                </div>
                <div class="sign_center ">
                    <div class="always_btn">Already Have an Account? <a href="{{ URL::to( 'login')}}"></i>Login</a></div> 
                </div>
                {{ Form::close()}}
            </div>
        </div>
   
   
   
   
   
   
   </div>
   
   
   
   
   
   
   
    </div>
            </div>
    </div>
</div>
@endsection