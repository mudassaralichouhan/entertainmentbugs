@extends('layouts.home')

@section('meta_tags')
	<meta name="description" content="Login to restart from where you left last time.">
	<meta name="keywords" content="Login, Sign in, Entertainment User, Artist, Production house, Advertise with us">
	<meta name="author" content="<?php echo ((isset($author) ? $author : 'Login - Entertainment Bugs')); ?>">
	<meta property="og:site_name" content="{{URL::to('/')}}"/>
	<meta property="og:locale" content="en_US"/>
	<meta property="og:title" content="<?php echo ((isset($ogtitle) ? $ogtitle : 'Login - Entertainment Bugs')); ?>"/>
	<meta property="og:description" content="<?php echo ((isset($ogdescription) ? $ogdescription : 'Login to restart from where you left last time.')); ?>"/>
	<meta name="theme-color" content="#f82e5e">
	<meta property="og:keywords" content="Login, Entertainment User, Artist, Production house, Advertise with us">
	<meta property="og:url" content="<?php echo url()->current(); ?>"/>
	<meta property="og:image" content="<?php echo ((isset($ogimage) ? $ogimage : 'https://www.entertainmentbugs.com/public/uploads/logo/logo-social-media.png')); ?>" />
@endsection
@section('content')

<style>
	#live-video-user-list-home{
		display:none
	}
	#social_log{
		margin: -28px 0 29px 0
	}
	#spacer-extra{
		display:none
	}
	.login-wraper::before{
		background:#f72e5e;
		content:"";
		position: absolute;
		left: 0px;
		width:50%
	}
</style>
<div class="container-fluid bg-image" style="background: url(https://entertainmentbugs.com/public/img/login_bg.jpg) no-repeat;background-size: cover;margin: 0;">
	<div class="row">
		<div class="login-wraper">
			<div class="login-window">
				<div class="l-head">
					Login  
				</div>
				<div class="l-form">
					<div class="error-message">@include('elements.errorSuccessMessage')</div> 
					@include('elements/socialLogin')          
				
					{{ Form::open(array('method' => 'post', 'id' => 'loginform', 'class' => 'form form-signin')) }}
						<div class="form-group">
							<label for="exampleInputEmail1">Email</label>
							{{Form::email('email_address', Cookie::get('user_email_address'), ['class'=>'form-control required email', 'placeholder'=>'sample@gmail.com', 'autocomplete'=>'OFF', 'id' => 'exampleInputEmail1'])}}
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">Password</label>
							{{Form::input('password', 'password', Cookie::get('user_password'), array('class' => "required form-control", 'placeholder' => '**********', 'id' => 'exampleInputPassword1'))}}
						</div>
						<div class="checkbox">
							<label>
								<label class="checkbox">
								{{Form::checkbox('user_remember', '1', Cookie::get('user_remember'))}}
								<span class="arrow"></span>
								</label> <span>Remember me on this computer</span>
								<span class="text2">(not recomended on public or shared computers)</span>
							</label>
						</div>
						<div class="row">
							<div class="col-lg-12">                
								{{Form::button('Login', ['type' => 'submit', 'class' => 'btn btn-cv1'])}}
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 forgottext" style="margin:10px 0 5px 0">
								<a href="{{URL::to('forgot-password')}}" style="display:block; text-align:center; margin:14px 0  6px 0">Forgot Username or Password?</a>
							</div>
						</div>
						<div class="text-center mt-10">
							<span class="forgottext"><a href="{{URL::to('register')}}">Don't have an account?</a></span>
							<span class="signuptext"><a href="{{URL::to('register')}}">Signup here</a></span>
						</div>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>
{{ HTML::script('public/js/jquery.validate.js')}}
<script type="text/javascript">
	$(document).ready(function () {
		$("#loginform").validate();
	});
</script>

@endsection