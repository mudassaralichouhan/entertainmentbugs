@extends('layouts.inner')
@section('content')

<section class="my-profiles-section">
    <div class="container">
        <h2>Madan Jangid</h2>
        <p>0 Points</p>
    </div>
</section>
<section class="my-profiles-detail-section">
    <div class="container">
        <div class="mypor-bx">
            <div class="row">
                @include('elements.leftmenu') 
                <div class="col-sm-8 col-md-8">
                    <form>
                        <div class="reservation-points">
                            <div class="profile-headers" id="profile-section">
                                <h3>Profile</h3><a href="#">Privacy Policy</a>
                            </div>
                            <div class="edit-started-bx">
                                <div class="my-profile-sections">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label><span class="mktoAsterix">*</span>First Name</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="First Name" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label><span class="mktoAsterix">*</span>Last Name</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="Last Name" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <label><span class="mktoAsterix">*</span>Email Address</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="Email Address" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label><span class="mktoAsterix">*</span>Phone Number</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="Phone Number" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label><span class="mktoAsterix">*</span>Mobile Number</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="Mobile Number" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row-acx">
                                        <h4 >Professional Profile</h4>
                                        <a class="" href="#">Explain this</a>
                                    </div>
                                    <div class="form-row-acx">
                                        <input type="checkbox" id="ConvertToAdmin">
                                        <label for="ConvertToAdmin">I am an administrative professional who books reservations for others.</label>
                                    </div>
                                </div>
                                <div class="password-section" id="password-section">
                                    <h3>Password</h3>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <label><span class="mktoAsterix">*</span>Password</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="Password" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <label><span class="mktoAsterix">*</span>Re-enter Password</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="Re-enter Password" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="password-section" id="address-section">
                                    <h3>Address</h3>
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label>Line 1 address</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="Line 1 address" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label>Line 2 address</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="Line 2 address" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label>City</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="City" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label>State / Province / Region</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="State / Province / Region" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <label>Zip / Postal Code</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="Zip / Postal Code" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                  <div class="password-section" id="public-diner-profile-section">
                                    <h3>Public profile details</h3>
                                    <div class="row">
                                        <div class="col-sm-6 col-md-6">
                                            <div class="form-group">
                                                <label>Review Display Name</label>  
                                                <div class="acc-inputs">
                                                    <input placeholder="Line 1 address" class="form-control" type="text">
                                                </div>
                                            </div>
                                        </div>
                                       
                                    </div>
                                 
                                </div>
                            </div>
                        </div>
                        <div class="reservation-points">
                            <div class="profile-headers" id="dining-preferences-section">
                                <h3>Dining Preferences</h3>
                            </div>
                            <div class="edit-started-bx">
                                <div class="my-profile-sections">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <label>Primary Dining Location</label>  
                                                <div class="acc-inputs">
                                                    <select id="selectbasic" name="selectbasic" class="form-control">
                                                        <option value="1">Option one</option>
                                                        <option value="2">Option two</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <label>Special Requests</label>  
                                                <div class="acc-inputs">
                                                    <textarea class="form-control"></textarea>
                                                    <span class="help-text">Restaurants will do their best to accommodate any special requests that you have.</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <label>Customized Dining Experience</label>  
                                                <div class="acc-inputs">
                                                    <div class="form-row-acx">
                                                        <input type="checkbox" id="UserOptIns_GuestShare">
                                                        <label for="UserOptIns_GuestShare">Improve your dining experience by allowing restaurants to share 
                                                            your dining information with other restaurants within their restaurant group and as 
                                                            detailed in our privacy policy</label>
                                                    </div>
                                                    <div class="form-row-acx">
                                                        <input type="checkbox" id="UserOptIns_Options">
                                                        <label for="UserOptIns_Options">Allow restaurants to share your dining information with partners of restaurants to 
                                                            provide you with a better dining experience and support the restaurants’ operations. See privacy policy for 
                                                            details.</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="password-section" id="email-preferences-section">
                                    <h3>Communication Preferences</h3>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <label>Romotional Emails</label>  
                                                <div class="acc-inputs">
                                                    <div class="form-row-acx">
                                                        <input type="checkbox" id="BestOfLists">
                                                        <label for="BestOfLists"><strong>Best-Of Lists.</strong> Stay current on trending restaurants, top picks & must-try spots.</label>
                                                    </div>
                                                    <div class="form-row-acx">
                                                        <input type="checkbox" id="TableBooking">
                                                        <label for="TableBooking"><strong>Ls Table Booking.</strong> From chef favorites to our favorite chefs, the Insider is your newsletter for all things dining.</label>
                                                    </div>
                                                    <div class="form-row-acx">
                                                        <input type="checkbox" id="UserOptIns_Options">
                                                        <label for="UserOptIns_Options"><strong>New to Table Booking.</strong> Be the first to try OpenTable's newest restaurants.</label>
                                                    </div>
                                                    <div class="form-row-acx">
                                                        <input type="checkbox" id="ProductNews">
                                                        <label for="ProductNews"><strong>Product News.</strong> Be the first to know about new product features, access beta tests and provide product feedback.</label>
                                                    </div>
                                                    <div class="form-row-acx">
                                                        <input type="checkbox" id="UserOptIns_Options">
                                                        <label for="UserOptIns_Options"><strong>Table Booking Updates.</strong> Learn how to get the most out of your points, get holiday reminders, see recommendations tailored just for you and more.</label>
                                                    </div>
                                                    <div class="form-row-acx">
                                                        <input type="checkbox" id="RestaurantWeeks">
                                                        <label for="RestaurantWeeks"><strong>Restaurant Weeks.</strong> Find out about Restaurant Weeks and other local dining events.</label>
                                                    </div>
                                                    <div class="form-row-acx">
                                                        <input type="checkbox" id="SpecialsOffers">
                                                        <label for="SpecialsOffers"><strong>Specials and Offers.</strong> Access specials and offers from top restaurants. Including exclusive deals for OpenTable diners.</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <label>Reservation Emails</label>  
                                                <div class="acc-inputs">
                                                    <div class="form-row-acx">
                                                        <input type="checkbox" id="RestaurantReviews">
                                                        <label for="RestaurantReviews"><strong>Restaurant Reviews.</strong> Easily review your dining experience. Plus, stay in the loop on all things restaurant reviews.</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12">
                                            <div class="form-group">
                                                <label>Text Messages</label>  
                                                <div class="acc-inputs">
                                                    <div class="form-row-acx">
                                                        <input type="checkbox" id="getreservvation">
                                                        <label for="getreservvation">Get reservations updates and reminders (standard text message rates may apply)</label>
                                                    </div>
                                                    <div class="form-row-acx">
                                                        <input type="checkbox" id="gettable">
                                                        <label for="gettable">Get table updates when your name is on the waitlist. This is required to use waitlist (standard text message rates may apply)</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="get-start-btn text_left">
                            <a href="#" class="btn btn-primary">Submit</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

