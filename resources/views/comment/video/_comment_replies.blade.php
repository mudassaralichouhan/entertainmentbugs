@foreach($comments as $comment)
    <!-- reply comment -->
    @php
        $created_at = strtotime($comment->created_at);
        $curDatetime = time();
        $difference = $curDatetime - $created_at;

        $userBastPath = PROFILE_SMALL_UPLOAD_PATH.$comment->user->photo;
        $userUrlPath = PROFILE_SMALL_DISPLAY_PATH.$comment->user->photo;
    @endphp

    <div class="cl-comment parent1">
     
        <div class="cl-comment-text">
            <div class="cl-avatar">
                @if ($comment->user->photo != null)
                    <a href="{{ user_url($comment->user->slug) }}">{{ HTML::image($userUrlPath) }}</a>
                @else
                    <a class="user_img" href="{{ user_url($comment->user->slug) }}">
                        <div class="shortname">{{ name_to_pic($comment->user_name) }}</div>
                    </a>
                @endif
            </div>
        
        
        <div class="right1">
            <div class="cl-name-date"><a href="{{ user_url($comment->user->slug) }}">{{ $comment->user->name }}</a> . {{ round($difference / 86400) }} days ago</div>
            <div class="cl-text"> {{ $comment->body }}</div>
            <div class="cl-meta"><span class="green"><span class="circle"></span> 70</span> <span class="grey"><span class="circle"></span> 9</span>   <a href="#comment_{{ $comment->id }}" onclick="showMe('#comment_{{ $comment->id }}');">Reply</a>
                <form method="post" action="{{ route('reply.add') }}" id="comment_{{ $comment->id }}" style="display:none" class="replyFrm">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                   <textarea rows="3" required name="comment_body" placeholder="Share what you think?"></textarea>
                   <input type="hidden" name="video_id" value="{{ $videoData->id }}" />
                   <input type="hidden" name="comment_id" value="{{ $comment->id }}" />
                   <input type="hidden" name="model" value="Video" />
                   <button type="submit">
                       <i class="cv cvicon-cv-add-comment">Reply</i>
                   </button>
                </form>
                @include('comment.video._comment_replies', ['comments' => $comment->replies])
            </div>
            
            </div>
           
        </div>
        <div class="clearfix"></div>
        <script>
            $('.replyFrm').submit(function(e) {
                e.preventDefault();
                var form = $(this);
                var url = form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), // serializes the form's elements.
                    success: function(data)
                    {
                        console.log(data);
                        $('#comments').html(''); // show response from the php script.
                        $('#comments').append(data.html);
                    }
                });
                $(this)[0].reset();
            });
        </script>
    </div>
    <!-- END reply comment -->
        <style>
       #comments .user_img .shortname {
    background: #28b47e !important;
    border-radius: 0;
    width: 100%;
    color: #fff;
    height: 100%;
    margin-bottom: 7px;
    line-height: 42px;
    font-weight: bold;
    font-size: 14px;
    padding: 0;
    text-align: center;
}
    </style>
      <style>
.right1 .right1 .cl-comment.parent1 .right1{    width: 93%;}
.right1 .right1 .right1 .cl-comment.parent1 .right1{    width: 92.5%;}
.right1 .right1 .right1 .right1 .cl-comment.parent1 .right1{    width: 92%;}

.right1{width:93%; float:right;}
.right1 .cl-comment.parent1  {    margin-left: -40px;}
.single-video .cl-comment .cl-comment-text {float: left;width: 100%;}


.cl-comment.parent1 .cl-comment.parent1{margin-bottom:0px}
.cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1{margin-bottom:0px}
.cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1{margin-bottom:0px}
.cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1{margin-bottom:0px}
.cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1{margin-bottom:0px}
.cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1{margin-bottom:0px}
.cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1{margin-bottom:0px}
.cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1{margin-bottom:0px}
.cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1 .cl-comment.parent1{margin-bottom:0px}
   
   
   
   
   
   
   
   
   
            </style>
@endforeach