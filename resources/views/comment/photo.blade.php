
@foreach ($comments as $comment)
    @php
        $words = explode(' ', $comment->name);
        $userImage = strtoupper(substr($words[0], 0, 1).substr(end($words), 0, 1));
    @endphp

    <div class='user mt-2' data-comment-id='{{ $comment->id }}'>
        <div class='comment'>
            <div style='display: flex'>
                <a href='{{ user_url($comment->slug) }}' class='user_img'>
                    @if( !(empty($comment->photo)) )
                        <img src="{{ asset("public/uploads/users/small/$comment->photo") }}" alt='{{ $comment->name }}' style='width:35px;height: 35px;object-fit: cover;'>
                    @else
                        <div class='shortnamevd' style='width:35px;height: 35px;object-fit: cover;'>{{ $userImage }}</div>
                    @endif
                </a>
                <div style='float:left'>
                    <a href='{{ user_url($comment->slug) }}' class='nm1' style='width:100%'>{{ $comment->name }}</a>
                    <span>{{ $comment->body }}
                        <div style='clear:both; overflow:hidden; margin:3px 0 5px 0'>
                            <small style='display:block;float:left;width:auto;font-size: 12px;padding: 2px 11px 0 0;'>{{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</small>
                            <button type='button' class='child_cmnt'>Reply</button>
                        </div>
                    </span>
                </div>
            </div>
        </div>
        <div class="comment-box"></div>
        <div class="nested-comments" style="padding-left: 10px">
            <div class="comments"></div>
            @if( $comment->totalChildren($comment->parent_id, $comment->id) )
                <button type='button' class="view-more-replies"><span style="padding-right: 5px">{{ $comment->totalChildren($comment->parent_id, $comment->id) }}</span> more comments</button>
            @endif
        </div>
    </div>
@endforeach
@if( isset($type) && $type == 'comment' )
    @if( $comments->toArray()['next_page_url'] )
        <div class="load-more">
            <button type="button" class="load-more-btn">Load More</button>
        </div>
    @endif
@endif