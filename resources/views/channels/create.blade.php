@extends('layouts.home')
@section('content')
<section class="channel light">
@include('elements.header_profile')
<!-- Create Channel Form Start -->
<div class="content-wrapper upload-page edit-page">
    <div class="container-fluid">

        <br>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="u-form">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="e1">Channel Title</label>
                                    <input type="text" class="form-control" id="e1" placeholder="Rocket League Pro Championship Gameplay (36 characters remaining)">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="e1"> Channel Tags</label>
                                    <input type="text" class="form-control" id="e1" placeholder="">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="e3">Prefered Language</label>
                                    <select class="form-control" id="e3">
                                        <option>Straight</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="e2">Description</label>
                                    <textarea class="form-control" name="e2" id="e2" rows="3">Description</textarea>
                                </div>
                            </div>

                        </div>

                        <div class="row ">
                            <div class="col-lg-12 u-category">Category ( you can select upto 3 categories )</div>
                        </div>

                        <div class="row">
                            <!-- checkbox 1col -->
                            <div class="col-lg-2 col-xs-6">

                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Film &amp; Animation
                                    </label>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Serials
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Cars &amp; Vehicles
                                    </label>
                                </div>

                            </div>

                            <!-- checkbox 2col -->
                            <div class="col-lg-2 col-xs-6">
                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Music
                                    </label>
                                </div>

                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Sports
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Science &amp; Technology
                                    </label>
                                </div>

                            </div>

                            <!-- checkbox 3col -->
                            <div class="col-lg-2 col-xs-6">
                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Entertainment
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Pets &amp; Animals
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Non-profits &amp; Activism
                                    </label>
                                </div>

                            </div>

                            <!-- checkbox 4col -->
                            <div class="col-lg-2 col-xs-6">
                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Travel &amp; Events
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> People &amp; Blogs
                                    </label>
                                </div>

                            </div>

                            <!-- checkbox 5col -->
                            <div class="col-lg-2 col-xs-6">
                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Comedy
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Gaming
                                    </label>
                                </div>

                            </div>

                            <!-- checkbox 6col -->
                            <div class="col-lg-2 col-xs-6">
                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> News &amp; Politics
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <label class="checkbox">
                                            <input type="checkbox" name="#">
                                            <span class="arrow"></span>
                                        </label> Education
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="u-area mt-small">
                        <form action="#" method="post">
                            <button class="btn btn-primary u-btn">Save</button>
                        </form>
                    </div>
                    <div class="u-terms">
                        <p>By submitting your videos to circle, you acknowledge that you agree to circle's <a href="#">Terms of Service</a> and <a href="#">Community Guidelines</a>.</p>
                        <p class="hidden-xs">Please be sure not to violate others' copyright or privacy rights. Learn more</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

</section>
@endsection