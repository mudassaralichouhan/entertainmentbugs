@extends('layouts.home') 
@section('content')
<div class="content-wrapper">
   <div class="container">
       <div class="row">
           <div class="col-lg-12 channels">
               <!-- Popular Channels -->
               <div class="content-block">
                   <div class="cb-header mb-0">
                       <div class="row">
                           <div class="col-lg-8 col-sm-8 col-xs-10">
                               <ul class="list-inline">
                                   <li><a href="#" class="color-active">Browse All Channels</a></li>
                                   <li class="hidden-xs"><a href="#">Most Popular</a></li>
                                   <li><a href="#">Trending</a></li>
                                   <li class="hidden-xs"><a href="#">Most Recent</a></li>
                                   <li class="hidden-xs"><a href="#">A - Z</a></li>
                               </ul>
                           </div>
                           <div class="col-lg-4 col-sm-4 col-xs-2">
                               <div class="cb-search channels-search">
                                   <form action="#">
                                       <label>
                                           <input type="search" placeholder="Search Channels ...">
                                           <i class="fa fa-search"></i>
                                       </label>
                                   </form>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="channels-content">
                       <div class="clearfix"></div>
                       <div class="theme-section" id="channel_list">
                           <div class="row">
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp_new">                                               {{HTML::image('public/img/ava5.png')}}                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Grainz<i class="arrow"></i></h5>
                                           <span>27,548 Followers</span>
                                           <span>615 Videos</span>
                                           <span>10 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 78%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp_new">
                                               {{HTML::image('public/img/ava2.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Adaptable</h5>
                                           <span>52,217 Followers</span>
                                           <span>275 Videos</span>
                                           <span>21 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 98%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp_new">
                                               {{HTML::image('public/img/ava3.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Immense</h5>
                                           <span>66,007 Followers</span>
                                           <span>34 Videos</span>
                                           <span>20 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 73%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp_new">
                                               {{HTML::image('public/img/ava4.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Kittens<i class="arrow"></i></h5>
                                           <span>136,601 Followers</span>
                                           <span>880 Videos</span>
                                           <span>64 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 94%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp_new">
                                               {{HTML::image('public/img/ava6.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Shoe</h5>
                                           <span>72,870 Followers</span>
                                           <span>147 Videos</span>
                                           <span>9 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 92%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <div class="row">
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp_new">
                                               {{HTML::image('public/img/ava5.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Grainz<i class="arrow"></i></h5>
                                           <span>27,548 Followers</span>
                                           <span>615 Videos</span>
                                           <span>10 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 78%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp_new">
                                               {{HTML::image('public/img/ava2.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Adaptable</h5>
                                           <span>52,217 Followers</span>
                                           <span>275 Videos</span>
                                           <span>21 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 98%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp_new">
                                               {{HTML::image('public/img/ava3.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Immense</h5>
                                           <span>66,007 Followers</span>
                                           <span>34 Videos</span>
                                           <span>20 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 73%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp_new">
                                               {{HTML::image('public/img/ava4.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Kittens<i class="arrow"></i></h5>
                                           <span>136,601 Followers</span>
                                           <span>880 Videos</span>
                                           <span>64 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 94%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp_new">
                                               {{HTML::image('public/img/ava6.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Shoe</h5>
                                           <span>72,870 Followers</span>
                                           <span>147 Videos</span>
                                           <span>9 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 92%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <h4>New Channels</h4>
                       <a href="#" class="btn-view-more">View more</a>
                       <div class="clearfix"></div>
                       <div class="theme-section">
                           <div class="row">
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <a href="#" class="cns-image">
											{{HTML::image('public/img/video1-1.png')}}
                                       </a>
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp">
                                               {{HTML::image('public/img/ava11.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Develop<i class="arrow"></i></h5>
                                           <span>66,007 Followers</span>
                                           <span>34 Videos</span>
                                           <span>22 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 85%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <a href="#" class="cns-image">
                                           {{HTML::image('public/img/video1-6.png')}}
                                       </a>
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp">
                                               {{HTML::image('public/img/ava12.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Picture</h5>
                                           <span>80,495 Followers</span>
                                           <span>71 Videos</span>
                                           <span>36 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 74%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <a href="#" class="cns-image">
                                           {{HTML::image('public/img/sv-16.png')}}
                                       </a>
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp">
                                               {{HTML::image('public/img/ava6.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Weather</h5>
                                           <span>47,367 Followers</span>
                                           <span>381 Videos</span>
                                           <span>42 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 89%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3 col-sm-4">
                                   <div class="cns-block">
                                       <a href="#" class="cns-image">
                                           {{HTML::image('public/img/video1-4.png')}}
                                       </a>
                                       <div class="cns-img-small">
                                           <div class="cns-small-wrapp">
                                               {{HTML::image('public/img/ava8.png')}}
                                           </div>
                                       </div>
                                       <div class="cns-info">
                                           <h5>Word</h5>
                                           <span>100,212 Followers</span>
                                           <span>496 Videos</span>
                                           <span>71 Million Views</span>
                                           <span class="cv-percent">
                                               <span class="cv-circle"></span> 93%
                                           </span>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="content-block head-div head-arrow">
                       <div class="head-arrow-icon">
                           <i class="cv cvicon-cv-next"></i>
                       </div>
                   </div>
               </div>
               <!-- /Popular Channels -->
           </div>
       </div>
   </div>
</div>
@endsection