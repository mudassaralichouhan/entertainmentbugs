<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <link rel="shortcut icon" type="image/x-icon" href="public/img/favicon.ico"> 
        <link rel="shortcut icon" href="{!! FAVICON_PATH !!}" type="image/x-icon"/>
        <link rel="icon" href="{!! FAVICON_PATH !!}" type="image/x-icon"/>
        <title>{{$title.TITLE_FOR_LAYOUT}}</title>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

        <link media="all" type="text/css" rel="stylesheet" href="public/css/bootstrap.min.css">
        <link media="all" type="text/css" rel="stylesheet" href="/public/css/style.css">
        <link media="all" type="text/css" rel="stylesheet" href="/public/css/font-awesome.css">
        <link media="all" type="text/css" rel="stylesheet" href="/public/css/aos.css">
        <script src="/public/js/jquery.min.js"></script>
  </head>
  <body>
        @include('elements.header_inner')
        @yield('content') 
        @include('elements.footer')
    </body>
</html>
