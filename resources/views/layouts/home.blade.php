<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>{{$title}} - Entertainment Bugs</title>
    <meta name="secure_token" content="{{ csrf_token() }}">
    <script>
        const CSRF = '<?= csrf_token() ?>';
    </script>
    @yield('meta_tags')
    <link rel="icon" href="{!! FAVICON_PATH !!}">

    <link media="all" type="text/css" rel="stylesheet" href="/public/css/bootstrap.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="/public/css/style.css">
    <link media="all" type="text/css" rel="stylesheet" href="/public/css/font-awesome.min.css">
    <link media="all" type="text/css" rel="stylesheet" href="/public/css/font-circle-video.css">
    <link media="all" type="text/css" rel="stylesheet" href="/public/css/font-awesome.css">
    <link media="all" type="text/css" rel="stylesheet" href="/public/css/croppie.css">
    <link media="all" type="text/css" rel="stylesheet" href="/public/css/story.css">
    <link media="all" type="text/css" rel="stylesheet" href="/public/css/owl.carousel.css">
    <link media="all" type="text/css" rel="stylesheet" href="/public/css/owl.theme.css">
    <link media="all" type="text/css" rel="stylesheet" href="/public/css/owl.transitions.css">
    <link media="all" type="text/css" rel="stylesheet" href="/public/assets/summernote/summernote.min.css">

    <script src="/public/js/jquery.min.js"></script>

    <link href='https://fonts.googleapis.com/css?family=Hind:400,300,500,600,700|Hind+Guntur:300,400,500,700'
          rel='stylesheet' type='text/css'>
    @yield('style')
    <script>
        const BASE = {
            url: function (path = "") {
                return "{{ env('APP_URL') }}" + path;
            }
        };
        // Object.freeze(obj);
    </script>
</head>

@php $bodyClass = ($action === "watchVideo") ? "single-video" : ""; @endphp
<body class="{{ $bodyClass }} light" data-gr-c-s-loaded="true">

@include('elements.header')
@yield('content')
@include('elements.footer')

<script src="/public/js/bootstrap.min.js"></script>
<script src="/public/js/custom.js"></script>

<script src="/public/js/jquery.easytabs.min.js"></script>
<script src="/public/js/owl.carousel.js"></script>
<script src="/public/js/only_slider.js"></script>
<script src="/public/assets/summernote/summernote.min.js"></script>
<script src="/public/js/croppie.js"></script>

<script>
    function openNav() {
        document.getElementById("myNav").style.height = "100%";
    }

    function closeNav() {
        document.getElementById("myNav").style.height = "0%";
    }

    function check(plan) {
        price = plan.price
    }
</script>
@yield('js')
</body>
</html>