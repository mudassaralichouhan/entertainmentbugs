<div class="post">
    <div class="fw-header">
        <div class="photo-head">
            <div class="users">
                <a href="http://127.0.0.1:8000/userprofileTester" class="user_img">
                    <div class="shortnamevd">TT</div>
                </a>
                <a href="" style="width:auto">Tester</a>
            </div>
            <div>
                <a onclick="suggestedFollowUser(99)" class="btn-0flw"> <i class="fa fa-heart-o"></i>
                    Follow
                </a>
            </div>
            <div>
                <div class="social">
                    <a style="color: #000;font-size: 12px;margin: 3px 5px 0 0;"> Share:</a>
                    <a id="facebook" class="facebook"><i class="fa fa-facebook"
                            aria-hidden="true"></i></a>
                    <a id="twitter" class="twitter"><i class="fa fa fa-twitter"
                            aria-hidden="true"></i></a>
                    <a href="#" class="tumblr"><i class="fa fa-linkedin"
                            aria-hidden="true"></i></a>
                    <a href="#" class="vk"><i class="fa fa-whatsapp" aria-hidden="true"
                            style="line-height: 17px;"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="fw-body">
        <div class="items">
            <div class="item">
            </div>
        </div>
        <div class="photo-middle_botttom" style="position:relative">
            <h2 id="photo_title"
                style="font-size: 13.5px;margin:3px 0px 3px 0;display:block;font-weight: normal;color: #000;line-height: 17px;">
                as</h2>
            <span id="photo_tag_list"
                style="font-size: 12.5px;display:block;color:#8e8e8e;line-height: 7px;margin: 9px 0 6px 0;"></span>
            <span style="font-size:10px;color:#8e8e8e">Jan 29 2023</span>
            <div class="tag-followers">
                <span style="font-size:12px;color:#8e8e8e; margin-right:5px"> Tagged to</span>
            </div>
        </div>
        <div class="comments">
            <div class="read-comment">
                <div class="mobile-comments" style="padding-left: 10px">
                    <div class="user mt-2" data-comment-id="232">
                        <div class="comment">
                            <div style="display: flex">
                                <a href="#" class="user_img">
                                    <div class="shortnamevd"
                                        style="width:30px;height: 30px;object-fit: cover;">TT</div>
                                </a>
                                <div style="float:left">
                                    <a href="" class="nm1" style="width:100%">Tester</a>
                                    <span>test
                                        <div style="clear:both; overflow:hidden; margin:3px 0 5px 0">
                                            <small
                                                style="display:block;float:left;width:auto;font-size: 12px;padding: 2px 11px 0 0;">1
                                                week ago</small>
                                            <button type="button" class="child_cmnt">Reply</button>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="comment-box"></div>
                        <div class="nested-comments" style="padding-left: 10px">
                            <div class="comments"></div>
                        </div>
                    </div>

                    <div class="user mt-2" data-comment-id="231">
                        <div class="comment">
                            <div style="display: flex">
                                <a href="#" class="user_img">
                                    <div class="shortnamevd"
                                        style="width:30px;height: 30px;object-fit: cover;">TT</div>
                                </a>
                                <div style="float:left">
                                    <a href="" class="nm1" style="width:100%">Tester</a>
                                    <span>test
                                        <div style="clear:both; overflow:hidden; margin:3px 0 5px 0">
                                            <small
                                                style="display:block;float:left;width:auto;font-size: 12px;padding: 2px 11px 0 0;">1
                                                week ago</small>
                                            <button type="button" class="child_cmnt">Reply</button>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="comment-box"></div>
                        <div class="nested-comments" style="padding-left: 10px">
                            <div class="comments"></div>
                        </div>
                    </div>
                    <div class="load-more">
                        <button type="button" class="load-more-btn">Load More</button>
                    </div>
                </div>
            </div>
            <div class="write-comment">
                <div class="mob09-comment" style="margin-top: 8px">
                    <form method="post" action="{{ route('commentPhoto.add') }}" id="commentfrmphotomobile" style="display: flex">
                        <input type="text" id="comment-block" name="comment_body" placeholder="Add a comment…" class="PUqUI Ypffh" autocomplete="off" autocorrect="off" style="border-radius: 0; border-top: 0; border-left: 0; border-right: 0" />
                        <input type="hidden" id="PhotoID" name="photo_id" value="" />
                        <input type="hidden" name="model" value="photo" />
                        <button type="submit" style="background-color:white; border:none;padding: 5px 12px;color:black; border-bottom: 1px solid #e0e1e2">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>