@extends('layouts.home')
@section('content')
<section class="channel light">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style>	

      #mobile-video-and-stories .photo_ac a{ border-bottom: 2px solid #f72e5e;
    color: #f72e5e;}
         

h4.media-heading {
    color: #fff;
    margin-top: 1px;
    font-weight: normal;
    font-size: 14px;
    font-weight: normal;
}
	.add_video {
		width: 100%;
		height: 150px;
		background-color: #eceff0;
		margin: 20px 0 0 0;
		text-align: center
	}
 

	.list-inline li:nth-child(2) a i{
    border-bottom: 0px solid #f72e5e;
    /*color: #f72e5e;*/
}


	.add_video a {
		padding: 52px 0 0 0;
		display: block;
		height: 100%;
		width: 100%
	}
</style>
@if (Session::has('success'))
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
@endif
@include('elements.header_profile')

  @php
                    $loggedName = session()->get('user_name'); 
                    $profileName = $user->name;	
                @endphp

<style>
 

.plus-details{display: block !important;position: absolute;left: 0px;top: -56px;}
.plus-details ul{list-style: none;margin: 0;padding: 2px 0 0 10px;width:170px}
.photo-block a.user_info_mini {
    top: 123px;
    right: 145px;
}
.on-hvf{position:absolute; right:10px;top:0px;}
.on-hvf a{margin-top:5px; display:block;}
 .on-hvf i{        color: #fff;
    background: #f72e5e;
    text-align: Center;
    display: block;
    padding: 5px;
    border-radius: 100px;
    width: 25px;
    height: 25px;
    font-size: 10px;
    line-height: 15px;
}
.photo-block {
    position: relative;
    width: 16.2%;
    margin: 0 0%;
    padding: 0 4px 8px 4px;
    float: left;
    height: 174px;
}
.plus-details ul li{float:left;margin:0 2%;width:46%; }            
.plus-details ul li a.edt-016{text-align:center !important; border-radius: 5px;  background:#28b47e !important; line-height: 27px;display:block;}

.media p {height: 13px;overflow: hidden;}
.media p:nth-child(1){display:none;}
.media p:nth-child(3){display:none;}
.media ul.list-inline {padding: 0 4px;}
</style>
                @if($profileName == $loggedName)



     @else
<style>.plus-details{display: none !important;position: absolute;left: 0px;top: -65px;}

 </style>

                       @endif
                       
                       
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-122">
                <!-- Featured Videos -->
                <div class="content-block" style="padding:20px 0">
                    <div class="cb-content videolist">
                        <div class="rows">
                            @if( $user_photos )
                                @php $isLoggedInUser = $user->id == Session::get('user_id') @endphp
                                @foreach($user_photos as $key => $photo)
                                    <?php $images = unserialize($photo->images); ?>
                                    <div class="photo-block" style="position:relative">
                                        <a href="#" class="user_info_mini"><i class="fa fa-thumbs-up"></i><span>{{ $photo->likes()->get()->count() ?? ''}}</span></a>
                                        <a href="{{ url('single-photo/'.$photo->id) }}"><img src="{{ photo_url($images[0]) }}"></a>
                                        @if( is_countable($images) && count($images) > 1 )
                                            <span style="position: absolute;width: 25px;bottom: 13px;right: 6px;z-index: 99999;">
                                                <i class="far fa-clone" style="font-size: 17px;color: #fff;text-shadow: 2px 1px 1px #000;"></i>
                                            </span>
                                        @endif

                                        @if( Session::has('user_id') && (Session::get('user_id') == $photo->user_id ) )
                                            <div class="on-hvf">
                                                <a href="{{route('photos.edit',$photo->id)}}" class=""> <i class="fas fa-edit"></i></a>
                                                <a href="javascript:;" onclick="swal({ title: 'Are you sure?', text: 'Once deleted, you will not be able to recover this Photo!', icon: 'warning', buttons: true, dangerMode: true, }).then((willDelete) => {
                                                    if (willDelete) {
                                                        document.getElementById('photo_delete_{{$key}}').submit();
                                                    }
                                                });"> <i class="fa fa-trash" aria-hidden="true"></i></a>
                                                <form action="{{route('photos.destroy',$photo->id)}}" method="post" id="photo_delete_{{$key}}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }} 
                                                </form>
                                            </div>
                                        @endif
                                        <a href="{{ url('promote-post/photo/'.$photo->id) }}"></a>
                                        @if( isset($isLoggedInUser) && $isLoggedInUser && !$photo->is_promoted && $key == 0 || $key == 2 )
                                            <a href="{{ url("promote-post/photo/".$photo->id) }}" style="width: 61%;padding: 5px 10px 1px 10px;text-align:center;background-color: #f72e5e;color:white;margin-top: 0;display: flex;position:absolute;justify-content:center;align-items:center;bottom: 11px;height: 23px;left: 34%;font-size: 11px;">Promote your post</a>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                            <div style="clear:both; overflow:hidden"></div>
                        </div>
                    </div>
                </div>
                <!-- /Featured Videos -->
            </div>
        </div>
    </div>
</div>
</section>


@endsection