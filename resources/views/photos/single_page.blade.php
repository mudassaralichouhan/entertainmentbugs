@extends('layouts.home')
@php
    $images = unserialize(@$single_page_all_details->images);
@endphp

@section('meta_tags')
    <meta name="keywords" content="{{ $single_page_all_details->title ?? '' }}">
    <meta property="og:title" content="{{ $single_page_all_details->title ?? '' }} Vidoes"/>
    <meta property="og:url" content="{{ url()->current() }}"/>
    <meta property="og:image" content="{{ photo_url(@$single_page_all_details->images[0]) }}"/>
@endsection

@section('style')
    <style>
        .photo-middle_botttom {
            padding: 0px;
            background: #f6f6f6;
        }


        .social a.whatsapp-i {
            background-color: #4FCE5D;
        }

        .social a.instagram {
            background-color: #d62976;
            padding-top: 7px;
        }

        .social a {
            border-radius: 0
        }

        .main-tag-display {
            width: 100%;
            background: #fff;
            padding: 10px 15px;
        }

        #photo-container-all .shortname {
            background: #28b47e !important;
            border-radius: 100px;
            width: 40px;
            color: #fff;
            height: 40px;
            text-align: center;
            margin: 0px 10px 10px 0;
            line-height: 44px;
            font-weight: bold;
            font-size: 16px;
            float: left;
        }


        #owl-demo .item {
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            text-align: center;
        }

        .customNavigation {
            text-align: center;
        }

        .customNavigation a {
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }


        .photo-middle .shortname {
            line-height: 41px;
            font-weight: bold;
            font-size: 16px;
        }


        .photo-container-all {
            background: #fff;
        }

        .bg-main06 {
            background: #f4f3f3;
        }

        .photo-head a.flw {
            cursor: pointer;
            color: #fff;
            display: block;
            height: 30px;
            line-height: 33px;
            position: absolute;
            right: 10px;
            top: 15px;
            background: #28b47e !Important;
            z-index: 9;
            padding: 0 10px;
            font-size: 11px;
        }

        .photo-head a.btn-0flw {
            cursor: pointer;
            color: #fff;
            display: block;
            height: 30px;
            line-height: 33px;
            position: absolute;
            right: 10px;
            top: 15px;
            background: #f72e5e !Important;
            z-index: 9;
            padding: 0 10px;
            font-size: 11px;
        }


        .photo-container {
            overflow: inherit;
            max-width: 1160px;
            position: relative;
            left: 0;
            top: 0;
            height: 100%;
            margin: 0 auto;
            background: none;
            padding: 0;
            display: flex;
            flex-direction: row-reverse;
        }

        .next-load {
            border-radius: 300px;
            position: absolute;
            right: -102px;
            top: 214px;
            width: 90px;
            height: 90px;
            overflow: hidden;
        }


        .next-load .photo-block {
            height: 90px;
            padding: 0;
        }

        .prev-load {
            position: relative;
            margin: 0 auto 6px;
            width: 85px;
            height: 85px;
            overflow: hidden;
        }

        .prev-load .photo-block {
            height: 100%;
            width: 100%;
            padding: 0;
        }


        .photo-right-small-block {
            width: 116px;
            float: right;
        }

        .photo-container-inside1 {
            width: calc(100% - 116px);
            display: flex;
        }

        .over-all .photo-container-left {
            flex: 0 0 50%;
            width: 50%;
        }

        @media only screen and (max-width: 767px) {

            #photo_title {
                font-size: 12px !Important;
            }

            .photo-container-inside1 {
                height: auto !important;
            }

            .bg-main06 #small_ads {
                width: 98%;
                margin: 0 1% !Important;
                float: left;
            }

            #category_photo_data .photo-right-small-block {
                background: #cfcfcd;
            }

            #ads-banner-img-content {
                width: 69% !important;
                padding-top: 0px !important;
            }

            #ads-banner-img-content h6 {
                font-size: 10px !Important;
                margin: 0px !important;
                padding: 7px 5px 0px 0 !Important;
                line-height: 11px !Important;
            }


            a.ads-trigger-btn {
                font-size: 9px !Important;
                margin: 7px 8px 0 0 !Important;
                bottom: 7px !Important;
                width: 101px !Important;
                right: 12% !Important;
                line-height: 14px !Important;
                padding: 4px 0 0 0 !Important;
            }

            #ads_name {
                border: 1px solid #edecec !Important;
                border-radius: 5px !Important;
                position: absolute !Important;
                bottom: 6px !Important;
                right: 1% !Important;
                font-size: 9px !Important;
                padding: 4px 13px 2px 13px !Important;
                background: #edecec !Important;
            }


            .ads-banner-tag .tags_language {
                display: none;
            }

            .ads-banner-tag .tags_white {
                display: none;
            }

            #home_page_channel .col-lg-2 {
                margin: 1px 1% 0px 1% !important;
            }

            input[type="text"],
            input[type="email"],
            input[type="number"],
            input[type="search"] {
                font-size: 13px;
            }

            .read-comment {
                height: 245px;
                overflow: scroll;
                background: #f4f3f3;
            }
        }

        .user.mt-2:nth-child(1) {
            border: 0px;
        }

        .user_info_mini {
            display: none !important;
        }

        .post {
            margin-bottom: 15px;
        }

        .mobile-view-photos-scroll {
            background: #f4f3f3 !important
        }

        .mobile-view-photos-scroll .fw-body {
            background: #fff !important
        }

        .mobile-view-photos-scroll .photo-head .social {
            display: none;
        }

        .mobile-view-photos-scroll .view-more-replies {
            padding: 0px 12px;
            background: #f4f3f3 !important;
            border-radius: 5px;
            height: 17px;
            text-align: right;
            display: block;
            font-size: 10px;
        }

        .mobile-view-photos-scroll .write-comment {
            border-top: 1px solid #e0e1e2;
        }

        .mobile-view-photos-scroll .mob09-comment {
            margin-top: 0px !important
        }

        .mobile-view-photos-scroll .shortnamevd {
            margin-top: 0px;
            font-size: 14px;
        }

        .mobile-view-photos-scroll .user a {
            font-size: 11px;
            padding: 8px 0 0 0;
            color: #f72e5e !Important;
        }

        .mobile-view-photos-scroll .user a.user_img {
            width: 35px;
            margin: 6px 7px 2px 0;
            padding: 0px;
        }

        .mobile-view-photos-scroll .comment span {
            font-size: 13px !important
        }

        .mobile-view-photos-scroll .child_cmnt {
            font-size: 10px !important;
        }

        .mobile-view-photos-scroll .load-more-btn {
            padding: 0px 12px;
            background: #f4f3f3 !important;
            border-radius: 5px;
            height: 17px;
            text-align: right;
            display: block;
            font-size: 10px;
        }

        .photo-middle_botttom {
            height: 100%;
        }

        .mobile-view-photos-scroll .tag-followers span {
            font-size: 10px !important
        }

        .mobile-view-photos-scroll .photo-middle_botttom span {
            font-size: 9px !important;
            margin: 2px 0 !Important;
        }

        .mobile-view-photos-scroll .read-comment {
            display: none;
        }

        .mobile-view-photos-scroll .photo-middle_botttom h2 {
            font-size: 12px !Important;
        }

        @media only screen and (max-width: 767px) {
            .tag-followers {
                bottom: 1px !important;
            }
        }

        .view-more-replies, .load-more-btn {
            background-color: #fff;
            color: #626262;
            padding: 4px 12px;
            border-radius: 0;
            border-color: transparent;
            display: flex;
            height: 25px;
            width: calc(100% - 8px);
            font-size: 12px;
        }

        .photo-head a.flw {
            cursor: pointer;
            color: #fff;
            display: block;
            height: 30px;
            line-height: 33px;
            position: absolute;
            right: 10px;
            top: 15px;
            background: #28b47e !Important;
            z-index: 9;
            padding: 0 10px;
            font-size: 11px;
        }

        .photo-head a.btn-0flw {
            cursor: pointer;
            color: #fff;
            display: block;
            height: 30px;
            line-height: 33px;
            position: absolute;
            right: 10px;
            top: 15px;
            background: #f72e5e !Important;
            z-index: 9;
            padding: 0 10px;
            font-size: 11px;
        }

        .user a.user_img {
            width: 35px;
            margin: 9px 10px 6px 0;
            padding: 0px;
        }

        #photo-container-all .users a img,
        #mobile-photos .users a img {
            width: 35px;
            height: 35px;
            border-radius: 100px;
            margin-right: 5px;
            object-fit: cover
        }

        .over-all .photo-container-all i.fa:hover,
        .over-all .photo-container-all i.cv:hover {
            color: #fff !important;
        }

        .fa.fa-thumbs-up {
            color: #fff !important;
        }

        .photo-head .shortnamevd {
            margin-top: 0px;
        }

        .photo-head .users {
            line-height: 39px;
        }

        .child_cmnt {
            background: none;
            border: 0px;
            text-decoration: underline;
            font-size: 12px;
        }

        .sidenav {
            z-index: 9999999;
        }

        .user:nth-child(even) {
            background: #fff;
            padding: 2px 7px 0 18px;
            border-radius: 69px 0 0 69px;
            margin: 4px 0 4px 0;
        }

        .user.mt-2 {
            background: none !important;
            border-radius: 0 !important;
            background: none !important;
            padding: 0 !important;
            border-top: 1px solid #e2e2e2;
            margin-left: 0% !important;
            width: 100% !important;
        }

        .user {
            padding: 2px 7px 0 8px !important;
            border-radius: 10px 0 0 10px !important;
            margin: 4px 0 4px 0;
            border-top: 0px solid #e2e2e2;
            margin-left: 5% !important;
            width: 95% !important;
            background: #fff !important;
        }

        .desktop-scroll {
            height: auto;
            max-height: 38vh;
        }

        .photo-bottom {
            margin-left: 0px;
        }

        #photo-container-all {
            width: 50%;
            flex: 0 0 50%;
        }

        @media only screen and (max-width: 767px) {
            .photo-container-all {
                min-height: auto !important;
            }

            #photo-container-all .photo-head {
                position: absolute;
                width: 100%;
                top: -410px;
            }

            .photo-container-left {
                margin-top: 60px;
            }

            #no-mb00 {
                display: block;
            }

            #no-mb00 {
                display: block;
            }

            .deskp09 {
                display: block;
            }

            .tag-followers {
                right: 9px !important;
            }

            .photo-container-left {
                height: 350px !important;
            }

            .desktop-scroll {
                padding-bottom: 50px;
            }

            .desktop-scroll {
                max-height: 60vh;
            }

            .user.mt-2 {
                background: none !important;
                border-radius: 0 !important;
                background: none !important;
                padding: 0 !important;
                border-top: 1px solid #e2e2e2;
                margin-left: 0% !important;
                width: 100% !important;
            }

            .user {
                padding: 2px 7px 0 8px !important;
                border-radius: 10px 0 0 10px !important;
                margin: 4px 0 4px 0;
                border-top: 0px solid #e2e2e2;
                margin-left: 5% !important;
                width: 95% !important;
                background: #fff !important;
            }

        }


        .user_main_profile_image img {
            width: 60px;
            height: 60px;
            display: block;
            margin: 0 auto 6px;
            border-radius: 100px;
        }

        .user_main_profile_image .name-b {
            text-align: center;
            font-size: 12px;
            margin-bottom: 5px;
            display: block;
        }


        .over-all #photo-raising-star {
            position: absolute;
            top: 10px;
            -webkit-transform: rotate(272deg);
            -moz-transform: rotate(272deg);
            -ms-transform: rotate(272deg);
            -o-transform: rotate(272deg);
            transform: rotate(272deg);
            width: 36px;
            height: 36px;
            left: 12px;
            top: 29px;
        }


        .user_main_profile_image .shortnames {
            width: 56px;
            background: #28b47e !important;
            border-radius: 100px;
            height: 56px;
            text-align: center;
            margin: 0 auto;
            display: block;
            color: #fff;
            font-weight: bold;
            font-size: 18px;
            line-height: 65px;
        }

    </style>
    <style>
        #mobile_social {
            width: 135px;
            float: right;
            margin-top: 5px;
        }

        #mobile_social a {
            margin-right: 5px;
            width: 20px;
            height: 20px;
            padding-top: 2px;
            margin-bottom: 2px;
            font-size: 9px;
        }


        .desc {
            position: absolute;
            color: #fff;
            bottom: 52px;
            left: 18px;
            font-size: 20px;
        }

        .carousel {
            position: inherit;
        }

        .carousel-indicators li {
            width: 40px !important;
            height: 40px !important;
            border-radius: 100px;
            overflow: hidden;
        }

        .carousel-indicators {
            bottom: 0;
        }

        .carousel-indicators .active {
            background: none
        }

        .carousel-control i {
            position: relative;
            top: 30%;
        }
    </style>
    <style>
        .tag-followers {
            position: absolute;
            right: 12px;
            bottom: 9px;
        }

        .photo-container-inside {
            position: relative;
        }
    </style>
    <Style>

        #small_ads {
            width: 49%;
            margin: 0 5px !Important;
            float: left
        }

        #ads-banner-img {
            width: 29% !Important;
        }

        #ads-banner-img-content {
            width: 69% !important;
        }

    </Style>
@endsection

@section('content')
    <div id="popup-wrapper--desk" data-strict-desk>
        <div class="over-all desk-post" id="category_photo_data" data-remove="false"
             style="display:block;position: relative;background: #f4f3f3;padding: 20px 0 20px 0;height:570px"
             value="{{ @$single_page_all_details->id }}" data-photo-id="{{ @$single_page_all_details->id }}">
            <div class="photo-container">
                <div class="photo-right-small-block">

                    @if(@$single_page_all_details)
                        <div class="user_main_profile_image">
                            <a href="{{ user_url(@$single_page_all_details->user->slug) }}"
                               style="width: 100%;height: auto;display: block;margin: 0 auto 8px;position:relative">
                                <img id="photo-raising-star"
                                     src="https://www.entertainmentbugs.com/public/web_img/raising_star_pink.gif"
                                     id="raising-star-desk" style="">

                                @if(@$single_page_all_details->user->photo)
                                    <img class="user_main_profile_image"
                                         src="{{ asset('/public/uploads/users/small/' . @$single_page_all_details->user->photo) }}"
                                         alt="User Icon"/>
                                @else
                                    <span class="shortnames">{{ name_to_pic(@$single_page_all_details->name) }}</span>
                                @endif
                                <a href="{{ user_url(@$single_page_all_details->user->slug) }}" class="name-b"
                                   style="width:auto">{{ @$single_page_all_details->name }}</a>
                            </a>
                            </a>

                        </div>

                    @endif
                    @foreach ($random_image as $random_images)
                        @if (!empty($random_images->images))
                            @php $random2 = unserialize($random_images->images); @endphp
                        @endif
                        <div class="prev-load">
                            <div class="photo-block">
                                @if (!empty($random_images->uploadID))
                                    <a href="{{ url('/') }}/single-photo/{{ $random_images->uploadID }}">
                                        @endif
                                        @if (!empty($random2[0]))
                                            <img src="{{ url('/') }}/public/uploads/photo/{{ $random2[0] }}">
                                        @endif
                                    </a>

                            </div>

                        </div>
                    @endforeach
                </div>

                <div class="photo-container-inside1">
                    <div class="photo-container-left">
                        <div class="photo-container-inside">

                            @if($single_page_all_details)
                                <div id="slider">
                                    <div id="myCarousel" data-photo-carousel="{{ @$single_page_all_details->id }}"
                                         class="carousel slide" data-ride="carousel">

                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            @if( $images && count($images) > 0 )
                                                @foreach($images as $key => $image)
                                                    <li data-target="#myCarousel" data-slide-to="0"
                                                        class="{{ $key == 0 ? 'active' : '' }}">
                                                        <img class="my" id="my"
                                                             src="{{ photo_url($image) }}"
                                                             alt=""
                                                             style="width:100%;">
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ol>

                                        <div class="carousel-inner">
                                            @if( $images && count($images) > 0 )
                                                @foreach($images as $key => $image)
                                                    <div class="item{{ $key == 0 ? ' active' : '' }}">
                                                        <img class="my" id="my" src="{{ photo_url($image) }}" alt=""
                                                             style="width:100%;">
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        @if (is_array($images) && count($images) > 1)
                                            <a class="left carousel-control"
                                               href="[data-photo-carousel={{ @$single_page_all_details->id }}]"
                                               data-slide="prev">
                                                <span class="glyphicon glyphicon-chevron-left"></span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                            <a class="right carousel-control"
                                               href="[data-photo-carousel={{ @$single_page_all_details->id }}]"
                                               data-slide="next">
                                                <span class="glyphicon glyphicon-chevron-right"></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        @endif
                                    </div>
                                </div>

                            @endif
                        </div>
                    </div>

                    <div id="photo-container-all">
                        @if($single_page_all_details)
                            <div class="photo-middle_botttom" style="position:relative" id="no-mb00">
                                <div class="photo-head">
                                    @if(@$single_page_all_details)
                                        <div class="users">
                                            <a href="{{ user_url(@$single_page_all_details->user->slug) }}"
                                               class="user_img">
                                                @if (@$single_page_all_details->user->photo)
                                                    <img src="{{ asset('/public/uploads/users/small/' . @$single_page_all_details->user->photo) }}"
                                                         alt="User Icon"/>
                                                @else
                                                    <span class="shortname">{{ name_to_pic(@$single_page_all_details->name) }}</span>
                                                @endif
                                                <a href="{{ user_url(@$single_page_all_details->user->slug) }}"
                                                   style="width:auto">{{ @$single_page_all_details->name }}</a>
                                            </a>
                                        </div>
                                    @endif

                                    @if( Session::get("user_id") && @$single_page_all_details->user->isFollowedBy() )
                                        <div id="suggested_unfollow_user{{ @$single_page_all_details->user->id }}">
                                            <a onclick="unFollowUser({{ @$single_page_all_details->user->id }})"
                                               class="btn-0flw" style="background:#f72e5e !Important">
                                                <i class="fa fa-solid fa-heart"></i> Following
                                            </a>
                                            @else
                                                <div id="suggested_follow_user{{ @$single_page_all_details->user->id }}">
                                                    <a onclick="followUser({{ @$single_page_all_details->user->id }})"
                                                       class="btn-0flw">
                                                        <i class="fa fa-heart-o"></i> Follow
                                                    </a>
                                                    @endif
                                                </div>
                                                <div id="no-mobile15" style="display:block !Important;">
                                                    <div class="social" id="mobile_social">
                                                        <a style="color: #000;font-size: 9px;margin: 3px 12px 0 0;">
                                                            Share:</a>
                                                        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                                            <a class="a2a_button_facebook"></a>
                                                            <a class="a2a_button_twitter"></a>
                                                            <a class="a2a_button_whatsapp"></a>
                                                        </div>
                                                        <script async
                                                                src="https://static.addtoany.com/menu/page.js"></script>
                                                    </div>

                                                </div>
                                        </div>

                                        <div class="main-tag-display" style="position:relative">

                                            <a href="{{ user_url(@$single_page_all_details->user->slug) }}">
                                                <h2 id="photo_title"
                                                    style="font-size: 13.5px;margin:3px 0px 3px 0;display:block;font-weight: normal;color: #000;line-height: 17px;">{{ @$single_page_all_details->photo_title }}</h2>
                                            </a>
                                            <span id="photo_tag_list"
                                                  style="font-size: 12.5px;display:block;color:#8e8e8e;line-height: 7px;margin: 9px 0 6px 0;">{{ @$single_page_all_details->tag_list }}</span>
                                            <span style="font-size:10px;color:#8e8e8e">{{ \Carbon\Carbon::parse(@$single_page_all_details->created_at)->format('M d Y') }}</span>


                                            <div class="tag-followers">
                                                <span style="font-size:12px;color:#8e8e8e; margin-right:5px"> Tagged to</span>
                                                @php
                                                    $tags = json_decode(@$single_page_all_details->followers_list);
                                                @endphp
                                                @if( $tags && is_array($tags) && count($tags) > 0 )
                                                    @foreach ($tags as $tag)
                                                        @php $taggedUser = \App\Models\User::select("name", "slug", "photo")->where('id', $tag)->first(); @endphp
                                                        @if( $taggedUser )
                                                            <a href="{{ url($taggedUser->slug) }}" class="user_img">
                                                                @if (!empty($taggedUser->photo))
                                                                    <img src="{{ url('/public/uploads/users/small/' . $taggedUser->photo) }}"
                                                                         style="width:30px;border-radius:100px;height:30px;object-fit:cover;margin-bottom:5px;">
                                                                @else
                                                                    <div class="shortnamevd"
                                                                         style="width:35px;height: 35px;object-fit: cover;">
                                                                        {{ name_to_pic($taggedUser->name) }}</div>
                                                                @endif
                                                            </a>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                        <!-- comment box start-->
                                        <div class="photo-middle deskp09">
                                            <div class="comments">
                                                <div class="read-comment">
                                                    <div class="mobile-comments desktop-scroll" style="padding-left: 10px" id="comment-box099">
                                                        @php
                                                            $comments = ($single_page_all_details)?$single_page_all_details->comments():"";
                                                        @endphp
                                                        @if ($comments && $comments->count() > 0)
                                                            @foreach ($comments as $comment)
                                                                <div class="user mt-2"
                                                                     data-comment-id="{{ $comment->id }}">
                                                                    <div class="comment">
                                                                        <div style="display: flex">
                                                                            <a href="#" class="user_img">
                                                                                @if ($comment->user->photo)
                                                                                    <img src="{{ asset('/public/uploads/users/small/' . $comment->user->photo) }}"
                                                                                         alt=""/>
                                                                                @else
                                                                                    <div class="shortnamevd"
                                                                                         style="width:35px;height: 35px;object-fit: cover;line-height: 40px; ">
                                                                                        {{ name_to_pic($comment->user->name) }}</div>
                                                                                @endif
                                                                            </a>
                                                                            <div style="float:left">
                                                                                <a href="{{ user_url($comment->user->slug) }}"
                                                                                   class="nm1"
                                                                                   style="width:100%">{{ $comment->user->name }}</a>
                                                                                <span>{{ $comment->body }}
                                                                    <div style="clear:both; overflow:hidden; margin:3px 0 5px 0">
                                                                        <small style="display:block;float:left;width:auto;font-size: 10px;padding: 2px 11px 0 0;">{{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</small>
                                                                        <button type="button"
                                                                                class="child_cmnt">Reply</button>
                                                                    </div>
                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="comment-box"></div>
                                                                    <div class="nested-comments"
                                                                         style="padding-left: 10px">
                                                                        <div class="comments"></div>
                                                                        @if ($comment->totalChildren(@$single_page_all_details->id, $comment->id))
                                                                            <button type='button'
                                                                                    class="view-more-replies"><span
                                                                                        style="padding-right: 5px">{{ $comment->totalChildren(@$single_page_all_details->id, $comment->id) }}</span>
                                                                                more comments
                                                                            </button>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                            @if ($comments->toArray()['next_page_url'])
                                                                <div class="load-more">
                                                                    <button type="button" class="load-more-btn">Load
                                                                        More
                                                                    </button>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- comment box ends-->
                                        <div class="heart-commentnew">
                                            <a href="{{ @$single_page_all_details->id }}" class="photoIDLikes"
                                               style="font-size:15px;line-height:25px;" id="likePhoto">
                                                <i class="fa fa-thumbs-up" data-toggle="tooltip" data-placement="top"
                                                   title="" style="margin-right: 5px;" data-original-title="Liked"></i>
                                                <span id="likedCount">{{ (@$single_page_all_details)?@$single_page_all_details->total:"" }}</span>
                                            </a>
                                        </div>
                                        <div class="photo-bottom deskp09">
                                            <form method="post" action="{{ url('comment/storephoto') }}"
                                                  id="commentfrmphoto">
                                                <textarea id="comment-block" name="comment_body"
                                                          placeholder="Add a comment…" class="PUqUI Ypffh"
                                                          autocomplete="off"></textarea>
                                                <input type="hidden" id="PhotoID" name="photo_id"
                                                       value="{{ @$single_page_all_details->id }}">
                                                <input type="hidden" name="model" value="photo">
                                                <button type="submit" style="position: absolute;right: 11px;top: 13px;">
                                                    Submit
                                                </button>
                                            </form>
                                        </div>
                                </div>
                            </div>
                        @else

                            <div class="under-review"><h5>This content is under review</h5></div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="bg-main06">
                <div class="container">
                    <div id="small_ads">
                        <div data-ad-block="" data-ad-type="ad" data-ad-mode="image" data-ad-size="728x90"
                             data-ad-loaded="1" value="21">
                            <div class="ads-cont" style="background:#fff;position:relative; margin-bottom:10px;">
                                <div id="ads-banner-img" style="width: 23.5% !important;float:left;">
                                    <a href="" data-ad-button="https://www.mycareerbugs.com" target="_blank">
                                        <img src="https://www.entertainmentbugs.com/public/ads/images/b0b2e160a2bbd7fcaad4cdc65cad9897.png"
                                             style="width:100%">
                                    </a>
                                </div>
                                <div id="ads-banner-img-content"
                                     style="width: 75% !important;float:right;padding:8px 4px 0 4px;">
                                    <h6 style="font-size: 12px ;margin:0px  !important;padding: 2px 5px 7px 0;font-family: arial;font-weight: bold;line-height: 13px;">
                                        Choose the job that accurately defines you and your passion
                                    </h6>
                                    <p style="font-size: 10.5px;line-height: 12px;padding-right: 10px;margin: 0 0 8px 0;font-family: arial;">
                                        How's your approach towards job hunting?
                                    </p>
                                    <div class="ads-banner-tag">
                                        <!--<span style="border:1px solid  #edecec;padding: 5px 6px 1px 6px;font-size: 8px;margin-bottom: 7px;display: inline-block;">Film &amp; Animation, Entertainment, Comedy</span>-->
                                        <div class="clearfix"></div>
                                        <span class="tags_language"
                                              style="background: #edecec;padding: 2px 6px 0px 6px;font-size: 10px;margin-right:2px;"></span>
                                        <span class="tags_white"
                                              style="background: #edecec;padding: 2px 6px 0px 6px;font-size: 9.5px;margin-right:2px;">mcb,mycareerbugs,jobs</span>

                                        <small id="ads_name"
                                               style="border:1px solid #edecec;border-radius:5px;position: absolute;bottom: 11px;right: 1%;font-size: 10px;padding: 5px 13px 0px 13px;background: #edecec;">Ad</small>
                                    </div>
                                </div>
                                <a href="" class="ads-trigger-btn" data-ad-button="https://www.mycareerbugs.com"
                                   style="float: none;font-size: 10px;color: #fff;margin: 5px 5px 0 0;background: #f72e5e;position: absolute;bottom: 10px;width: 101px;right: 11%;text-align: center;line-height: 17px;padding: 5px 0 0 0;">Visit
                                    Now <i class="fa fa-external-link" aria-hidden="true"></i></a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>

                    <div id="small_ads">
                        <div data-ad-block="" data-ad-type="ad" data-ad-mode="image" data-ad-size="728x90"
                             data-ad-loaded="1" value="21">
                            <div class="ads-cont" style="background:#fff;position:relative; margin-bottom:10px;">
                                <div id="ads-banner-img" style="width: 40%;float:left;">
                                    <a href="" data-ad-button="https://www.mycareerbugs.com" target="_blank">
                                        <img src="https://www.entertainmentbugs.com/public/ads/second.png"
                                             style="width:100%">
                                    </a>
                                </div>
                                <div id="ads-banner-img-content" style="width: 58%;float:right;padding:8px 4px 0 4px;">
                                    <h6 style="font-size: 12px ;margin:0px  !important;padding: 2px 5px 7px 0;font-family: arial;font-weight: bold;line-height: 13px;">
                                        Choose the job that accurately defines you and your passion
                                    </h6>
                                    <p style="font-size: 10.5px;line-height: 12px;padding-right: 10px;margin: 0 0 8px 0;font-family: arial;">
                                        How's your approach towards job hunting?
                                    </p>
                                    <div class="ads-banner-tag">
                                        <!--<span style="border:1px solid  #edecec;padding: 5px 6px 1px 6px;font-size: 8px;margin-bottom: 7px;display: inline-block;">Film &amp; Animation, Entertainment, Comedy</span>-->
                                        <div class="clearfix"></div>
                                        <span class="tags_language"
                                              style="background: #edecec;padding: 2px 6px 0px 6px;font-size: 10px;margin-right:2px;"></span>
                                        <span class="tags_white"
                                              style="background: #edecec;padding: 2px 6px 0px 6px;font-size: 9.5px;margin-right:2px;">mcb,mycareerbugs,jobs</span>

                                        <small id="ads_name"
                                               style="border:1px solid #edecec;border-radius:5px;position: absolute;bottom: 11px;right: 1%;font-size: 10px;padding: 5px 13px 0px 13px;background: #edecec;">Ad</small>
                                    </div>
                                </div>
                                <a href="" class="ads-trigger-btn" data-ad-button="https://www.mycareerbugs.com"
                                   style="float: none;font-size: 10px;color: #fff;margin: 5px 5px 0 0;background: #f72e5e;position: absolute;bottom: 10px;width: 101px;right: 10%;text-align: center;line-height: 17px;padding: 5px 0 0 0;">Visit
                                    Now <i class="fa fa-external-link" aria-hidden="true"></i></a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <Div class="clear"></Div>
                    <div class="col-lg-12">
                        <div class="content-block head-div">
                            <div class="cb-contents chanels-row" id="home_page_channel">
                                <div class="cb-header">
                                    <div class="row">
                                        <div class="col-lg-10 col-sm-10 col-xs-8">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="rewards.html" class="color-active">
                                                        <span class="visible-xs"> Latest Photos </span>
                                                        <span class="hidden-xs"> All Photos </span>
                                                    </a>
                                                </li>
                                                <li><a href="#">Popular Photos </a></li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-2 col-sm-2 col-xs-4">
                                            <div class="btn-group pull-right bg-clean">
                                                <button type="button" class="btn btn-default dropdown-toggle"
                                                        data-toggle="dropdown" aria-haspopup="true"
                                                        aria-expanded="false">
                                                    Sort by <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu">

                                                    <li><a href="#"><i class="cv cvicon-cv-relevant"></i> Relevant</a>
                                                    </li>

                                                    <li><a href="#"><i class="cv cvicon-cv-calender"></i> Recent</a>
                                                    </li>

                                                    <li><a href="#"><i class="cv cvicon-cv-view-stats"></i> Viewed</a>
                                                    </li>

                                                    <li><a href="#"><i class="cv cvicon-cv-star"></i> Top Rated</a></li>

                                                    <li><a href="#"><i class="cv cvicon-cv-watch-later"></i> Longest</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="photo-block-all-in-one">
                        @include('photos.inc.details-listing', ['photos' => $all_image])
                    </div>
                    <input type="hidden" id="start" value="0">
                    <input type="hidden" id="rowperpage" value="{{ $rowperpage }}">
                    <input type="hidden" id="totalrecords" value="{{ $totalrecords }}">
                </div>
            </div>
        </div>
    </div>

    <script>
        function someFunction() {
            var photo_id = $('.single_page_id').val();
            $.ajax({
                type: 'POST',

                data: {
                    "_token": '{{ csrf_token() }}',
                    value: photo_id
                },
                url: "{{ route('photos.details') }}",
                cache: false,
                // contentType: false,
                // processData: false,
                dataType: "json",
                success: (response) => {

                    $('#comment-box099').html(''); // show response from the php script.
                    $('#comment-box099').append(response.html);
                    $('#slider').html(''); // show response from the php script.
                    $('#slider').append(response.html2);
                    $('#photo-container-all').html(''); // show response from the php script.
                    $('#photo-container-all').append(response.html3);
                    var details = response.photo_details;
                    var image = response.image[0];

                    // $.each(response.photo, function (key,data) {
                    //     $('#carrusel').append($('<div class="carousel-item item"><img class="d-block img-fluid" src='  + 'http://entertainmentbugs.com/public/uploads/photo/' + data + '></div>'));
                    // });

                    // $('.carousel').carousel();

                    $.each(response.photo, function (key, data) {

                        // $('#carrusel').append($(
                        //     ' <div class="active item carousel-item" data-slide-number="0"><img  src=' +
                        //     'http://entertainmentbugs.com/public/uploads/photo/' + data +
                        //     '  class="img-fluid"></div><a class="carousel-control left pt-3" href=' +
                        //     'http://entertainmentbugs.com/public/uploads/photo/' + data +
                        //     ' data-slide="prev"><i class="fa fa-chevron-left"></i></a><a class="carousel-control right pt-3" href=' +
                        //     'http://entertainmentbugs.com/public/uploads/photo/' + data +
                        //     ' data-slide="next"><i class="fa fa-chevron-right"></i></a>'));
                    });

                    $('.carousel').carousel();
                    $('#photo_title').text(details['photo_title']);
                    $('#photo_tag_list').text(details['tag_list']);
                    //$('#photo_pic').src('http://entertainmentbugs.com/public/uploads/photo/'.photo_details);
                    // $("#photo_pic").attr("src", '{{ url('/') }}/public/uploads/photo/' + photo_details);
                    $('#photoID').val(photo_id);
                    $('#image').attr("src", '{{ url('/') }}/public/uploads/users/small/' + image);
                    $('#facebook').attr("href", details['facebook']);
                    $('#twitter').attr("href", details['twitter']);
                    $('#instagram').attr("src", details['instagram']);

                    if (response) {
                        $(".child_cmnt").click(function () {
                            var id = $(this).val();

                            $('.childphotoID').val(id);
                            var child_photo_id = $('.photo-block').attr('value');

                            $('.replyDiv').css("display", "block");
                            $('.childsubmit').click(function () {
                                var replyCmmt = $('textarea.message').val();

                                $.ajax({
                                    type: 'POST',
                                    data: {
                                        "_token": '{{ csrf_token() }}',
                                        value: id,
                                        value1: replyCmmt,
                                        value2: photo_id
                                    },
                                    url: "{{ route('photos.childdetails') }}",
                                    cache: false,
                                    // contentType: false,
                                    // processData: false,
                                    dataType: "json",
                                    success: (response) => {
                                        $('#comment-box099').html(''); // show response from the php script.
                                        $('#comment-box099').append(response.html);
                                    }
                                })
                            })
                        });
                    }
                },
            });
            $(".over-all").show();
        }

        $(document).on('touchmove', onScroll); // for mobile

        function onScroll() {
            if ($(window).scrollTop() > $(document).height() - $(window).height() - 100) {
                fetchData();
            }
        }

        checkWindowSize();

        // Check if the page has enough content or not. If not then fetch records
        function checkWindowSize() {
            if ($(window).height() >= $(document).height()) {
                fetchData();
            }
        }
        $(window).scroll(function () {
            var position = $(window).scrollTop();
            var bottom = $(document).height() - $(window).height() - 100;

            if (position >= bottom) {
                fetchData();
            }
        });

        function fetchData() {
            var start = Number($('#start').val());
            var allcount = Number($('#totalrecords').val());
            var rowperpage = Number($('#rowperpage').val());
            start = start + rowperpage;

            if (start <= allcount) {
                $('#start').val(start);

                $.ajax({
                    url: "{{ route('photo.load-ajax') }}",
                    data: {
                        start: start,
                        model: 'details-listing'
                    },
                    dataType: 'json',
                    success: function (response) {
                        // Add
                        $(".photo-block-all-in-one").find('.photo-block:last')
                            .after(response.html).show().fadeIn("slow");

                        // Check if the page has enough content or not. If not then fetch records
                        checkWindowSize();
                        EBUGS.ads.load();
                    }
                });
            }
        }

        $("#commentfrmphoto").submit(function (e) {
            e.preventDefault();
            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                },
                url: url,

                data: form.serialize(), // serializes the form's elements.
                success: function (response) {
                    $('#comment-box099').prepend(response.html);
                }
            });
            $(this)[0].reset();
        });

        $('#likePhoto').click(function (e) {
            e.preventDefault();

            var video_key = $(this).attr('href');
            $.ajax({
                type: 'get',
                url: "{{ url('/liked-photo') }}/" + video_key,
                success: function (data) {
                    if (data.disLikeCount > 0) {
                        $(".fa-thumbs-down").css('color', '#fff');
                    }
                    $("#likedCount").text(data.likeCount);
                    $("#unLikedCount").text(data.disLikeCount);
                    $(".fa-thumbs-up").css('color', '#fff');
                    $(".fa-thumbs-down").css('color', '#637076');
                }
            });
        });
        //following
        function followUser(user_id) {
            $.ajax({
                type: "GET",
                url: "<?php echo e(URL::route('follow_user')); ?>",
                data: {
                    'user_id': user_id
                },
                cache: false,
                success: function (response) {
                    $('#suggested_follow_user' + user_id).empty().html(response);
                    $('#suggested_follow_user' + user_id).attr("id", "suggested_unfollow_user" + user_id);

                }
            });
        }
        function unFollowUser(user_id) {
            $.ajax({
                type: "GET",
                url: "<?php echo e(URL::route('unfollow_user')); ?>",
                data: {
                    'user_id': user_id
                },
                cache: false,
                success: function (response) {
                    $('#suggested_unfollow_user' + user_id).empty().html(response);
                    $('#suggested_unfollow_user' + user_id).attr("id", "suggested_follow_user" + user_id);
                }
            });
        }

        $("#mobile-photos").on("click", ".click-comment-open-trigger", function () {
            event.preventDefault();
            $(this).closest(".fw-body").find(".read-comment").slideToggle("slow");
        });

        $("#mobile-photos").on("click", "[data-action='do-like']", function (e) {
            e.preventDefault();

            let my = $(this);
            let photoId = my.data('id');

            $.ajax({
                type: 'get',
                url: 'liked-photo/' + photoId,
                success: function (data) {
                    my.find("[data-likes-count]").text(data.likeCount);
                }
            });
        });

        $(function () {
            Comment.init($(".over-all").eq(0));
        })
    </script>
@endsection
