@if ( $photos && count($photos) > 0 )
    @foreach ($photos as $key => $photo)
        @php
            $images = unserialize($photo->images);
        @endphp
   
        @if( ($key + 1)%8 == 0 )
            <div class="photo-block" data-ad-type="photo" data-ad-style='details-listing' data-ad-block>
                
            </div>
        @endif
        @if( !$photo->isAd )
            <div class="photo-block" @if($photo->isAd) data-ad-type='photo' data-ad-style='details-listing' data-ad-block @endif>
        @endif
            <a href="#" class="user_info_mini">
                <i class="fa fa-heart-o"></i><span>{{ $photo->total }}</span>
            </a>
            <a href="{{ url('/single-photo/' . $photo->id) }}">
                <img src="{{ photo_url($images[0]) }}">
            </a>
            <span>
                @if( $images && is_array($images) && count($images) > 1 )
                    <span style="position: absolute;width: 25px;bottom: 13px;right: 6px;z-index: 99999;"><i class="far fa-clone" style="font-size: 17px;color: #fff;text-shadow: 2px 1px 1px #000;"></i></span>
                @endif
            </span>
        @if( $photo->isAd )
            <button type='button' data-ad-button='{{ $photo->button_url }}'>{{ $photo->button_text }}</button>
        @else
            </div>
        @endif
    @endforeach
@endif