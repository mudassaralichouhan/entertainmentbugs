@if ($photos && count($photos) > 0)
    @foreach ($photos as $key => $photo)
        @php
            $promoted = $photo->is_promoted;
            $isPromoted = false;
        @endphp

        @if( !$photo->isAd && $promoted )
            @php
                $isPromoted = true;
                $photo->button_url = $promoted->url;
                $photo->button_text = $promoted->button_label;
            @endphp
        @endif

        @if( ($key + 1)%8 == 0 )
            <div class="w-100 flex-100 desk-post" data-ad-type="photo" data-ad-style='popup-listing' data-ad-block></div>
        @endif

        @if( !$photo->isAd || $isPromoted )
            <div class="w-100 flex-100 desk-post" @if ($promoted) data-ad-type="photo" data-ad-style='popup-listing'
                 data-ad-loaded='1' data-ad-block='{{ $promoted->id }}' @endif data-photo-id='{{ $photo->id }}'>
                @endif
                <!--left block photo-->
                <div class="photo-container-left">
                    <div class="photo-container-inside">
                        <div data-photo-slider>
                            <div data-photo-carousel="{{ $photo->id }}" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                @php

                                    $photo->images = $photo->images ? unserialize($photo->images) : [];

                                @endphp

                                @if (count($photo->images) > 0)
                                    <ol class="carousel-indicators">
                                        @foreach ($photo->images as $key => $image)

                                            @if (!(empty($image) && file_exists(photo_path($image))))
                                                <li data-target="[data-photo-carousel={{ $photo->id }}]"
                                                    data-slide-to="{{ $key }}"
                                                    class="{{ $key == 0 ? 'active' : '' }}">
                                                    <img class="my" src="{{ photo_url($image) }}"
                                                         alt="{{ $photo->photo_title }}" style="width: 100%"/>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ol>
                                    <div class="carousel-inner">
                                        @foreach ($photo->images as $key => $image)
                                            @if (!(empty($image) && file_exists(photo_path($image))))
                                                <div class="item{{ $key == 0 ? ' active' : '' }}">
                                                    <img class="my" src="{{ photo_url($image) }}"
                                                         alt="{{ $photo->photo_title }}" style="width:100%;">
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    @if (count($photo->images) > 1)
                                        <a class="left carousel-control" href="[data-photo-carousel={{ $photo->id }}]"
                                           data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="[data-photo-carousel={{ $photo->id }}]"
                                           data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <!--left block photo-->

                <!--right block photo description start-->
                <div class="photo-container-all">
                    <div data-photo-details>
                        <div class="fw-header">
                            <div class="photo-head"
                                 style="display: flex; justify-content: space-between;position: relative;">
                                <div class="users" style="display: flex; align-items: center">
                                    <a href="{{ user_url($photo->user->slug) }}" data-photo="{{ $photo->user->photo }}"
                                       class="user_img">
                                        @if ($photo->user->photo)
                                            <img src="{{ asset('/public/uploads/users/small/' . $photo->user->photo) }}"
                                                 alt="User Icon"/>
                                        @else

                                            <div class="shortnamevd">{{ name_to_pic($photo->user->name) }}</div>
                                        @endif
                                    </a>
                                    <a href="{{ user_url($photo->user->slug) }}"
                                       style="width:auto; margin-left: 5px">{{ $photo->user->name }}</a>
                                </div>
                                <div class="social-tags">
                                    <div data-photo-userid='{{ $photo->user->id }}'>
                                        @if (Session::get('user_id') && $photo->user->isFollowedBy())
                                            <a onclick="unFollowUser({{ $photo->user->id }})" class="btn-0flw"
                                               style="background:#f72e5e !Important"> <i
                                                        class="fa fa-solid fa-heart"></i>
                                                Following
                                            </a>
                                        @else
                                            <a onclick="followUser({{ $photo->user->id }})" class="btn-0flw"> <i
                                                        class="fa fa-heart-o"></i>
                                                Follow
                                            </a>
                                        @endif
                                    </div>

                                    <div>
                                        <div class="social">
                                            <a style="color: #000;font-size: 12px;margin: 3px 5px 0 0;"> Share:</a>
                                            <!--									<a href="" id="facebook" class="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>-->
                                            <!--<a href="" id="twitter" class="twitter"><i class="fa fa fa-twitter" aria-hidden="true"></i></a>-->
                                            <!--<a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>-->
                                            <!--<a href="#" class="whatsapp-i"><i class="fa fa-whatsapp" aria-hidden="true" style="line-height: 17px;"></i></a>  -->
                                            <!-- AddToAny BEGIN -->
                                            <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                                <a class="a2a_button_facebook"></a>
                                                <a class="a2a_button_twitter"></a>
                                                <a class="a2a_button_whatsapp"></a>
                                            </div>
                                            <script async src="https://static.addtoany.com/menu/page.js"></script>
                                            <!-- AddToAny END -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="photo-middle_botttom" style="position:relative">
                            @if( $photo->isAd || $isPromoted )
                                <button type='button'
                                        data-ad-button='{{ $photo->button_url }}'>{{ $photo->button_text }}</button>
                            @endif
                            <h2 data-photo-title="photo_title"
                                style="font-size: 13.5px;margin:3px 0px 3px 0;display:block;font-weight: normal;color: #000;line-height: 17px;">{{ $photo->photo_title }}</h2>
                            <span data-photo-tags="photo_tag_list"
                                  style="font-size: 12.5px;display:block;color:#8e8e8e;line-height: 7px;margin: 9px 0 6px 0;">{{ $photo->tag_list }}</span>
                            <span style="font-size:10px;color:#8e8e8e">{{ \Carbon\Carbon::parse($photo->created_at)->format('M d Y') }}</span>
                            <div class="tag-followers">
                                <span style="font-size:12px;color:#8e8e8e; margin-right:5px"> Tagged to</span>
                                @php
                                    $tags = json_decode($photo->followers_list);
                                @endphp
                                @if( $tags && is_array($tags) && count($tags) > 0 )
                                    @foreach ($tags as $tag)
                                        @php $taggedUser = \App\Models\User::select("name", "slug", "photo")->where('id', $tag)->first(); @endphp
                                        @if( $taggedUser )
                                            <a href="{{ url($taggedUser->slug) }}" class="user_img">
                                                @if (!empty($taggedUser->photo))
                                                    <img src="{{ url('/public/uploads/users/small/' . $taggedUser->photo) }}"
                                                         style="width:30px;border-radius:100px;height:30px;object-fit:cover;margin-bottom:5px;">
                                                @else
                                                    <div class="shortnamevd"
                                                         style="width:35px;height: 35px;object-fit: cover;">
                                                        {{ name_to_pic($taggedUser->name) }}</div>
                                                @endif
                                            </a>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="comments">
                        <div class="read-comment">
                            <div class="mobile-comments desktop-scroll" style="padding-left: 10px">
                                @php
                                    $comments = $photo->comments();
                                @endphp
                                @if ($comments && $comments->count() > 0)
                                    @foreach ($comments as $comment)
                                        <div class="user mt-2" data-comment-id="{{ $comment->id }}">
                                            <div class="comment">
                                                <div style="display: flex">
                                                    <a href="#" class="user_img">
                                                        @if ($comment->user->photo)
                                                            <img src="{{ asset('/public/uploads/users/small/' . $comment->user->photo) }}"
                                                                 alt=""/>
                                                        @else
                                                            <div class="shortnamevd"
                                                                 style="width:35px;height: 35px;object-fit: cover;line-height: 40px; ">
                                                                {{ name_to_pic($comment->user->name) }}</div>
                                                        @endif
                                                    </a>
                                                    <div style="float:left">
                                                        <a href="{{ user_url($comment->user->slug) }}" class="nm1"
                                                           style="width:100%">{{ $comment->user->name }}</a>
                                                        <span>{{ $comment->body }}
														<div style="clear:both; overflow:hidden; margin:3px 0 5px 0">
															<small style="display:block;float:left;width:auto;font-size: 10px;padding: 2px 11px 0 0;">{{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</small>
															<button type="button" class="child_cmnt">Reply</button>
														</div>
													</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="comment-box"></div>
                                            <div class="nested-comments" style="padding-left: 10px">
                                                <div class="comments"></div>
                                                @if ($comment->totalChildren($photo->id, $comment->id))
                                                    <button type='button' class="view-more-replies"><span
                                                                style="padding-right: 5px">{{ $comment->totalChildren($photo->id, $comment->id) }}</span>
                                                        more comments
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                    @if ($comments->toArray()['next_page_url'])
                                        <div class="load-more">
                                            <button type="button" class="load-more-btn">Load More</button>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="heart-commentnew">
                        <a href="{{ $photo->id }}" class="photoIDLikes" style="font-size:15px;line-height:25px;"
                           id="likePhoto">
                            <i class="fa fa-thumbs-up" data-toggle="tooltip" data-placement="top" title=""
                               style="margin-right: 5px;" data-original-title="Liked"></i>
                            <span data-likes-count>{{ $photo->likes()->count() }}</span>
                        </a>
                    </div>
                    <div class="write-comment">
                        <div class="mob09-comment" style="margin-top: 8px">
                            <form method="post" action="{{ route('commentPhoto.add') }}"
                                  data-comment-form="commentfrmphotomobile" style="display: flex">
                                <input type="text" data-comment-block="comment-block" name="comment_body"
                                       placeholder="Add a comment…" class="PUqUI Ypffh" autocomplete="off"
                                       autocorrect="off"
                                       style="border-radius: 0; border-top: 0; border-left: 0; border-right: 0"/>
                                <input type="hidden" data-comment-name="PhotoID" name="photo_id"
                                       value="{{ $photo->id }}"/>
                                <input type="hidden" name="model" value="photo"/>
                                <button type="submit"
                                        style="font-size: 12px;border:none;padding: 8px 16px 5px 16px;color: #000;border-bottom: 1px solid #e0e1e2;background: #f4f3f3;opacity: 0.8;">
                                    Submit
                                </button>
                            </form>
                        </div>
                    </div>
                </div>

                @if( !$photo->isAd )
            </div>
        @endif
    @endforeach
    @if( is_object($photos) )
        @php
            $loadMore = $photos->toArray();
        @endphp
        @if ( $loadMore['next_page_url'])
            <div class="load-more-photos" data-page='{{ $loadMore['current_page'] + 1 }}'></div>
        @endif
    @endif
@endif