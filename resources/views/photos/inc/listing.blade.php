@foreach ($photos as $key => $photo)
    @php
        $images = unserialize($photo->images);
        $tagsList = explode(',', $photo->tag_list);
    @endphp

    @if (($key + 1) % 8 == 0)
        <div class="photo-block" value="" data-ad-type="photo" data-ad-style="listing" data-ad-block></div>
    @endif

    @if (!$photo->isAd)
        <div class="photo-block" value="{{ $photo->id }}">
            <a href="#" class="user_info_mini">
                <i class="fa fa-thumbs-up"></i><span>{{ $photo->total }}</span>
            </a>
            <a href="javascript:void(0)">
                <img src="{{ photo_url($images[0]) }}" alt="">
            </a>
            @if ($images && is_array($images) && count($images) > 1)
                <span style="position: absolute;width: 25px;bottom: 13px;right: 6px;z-index: 99999;">
                    <i class="far fa-clone" style="font-size: 17px;color: #fff;text-shadow: 2px 1px 1px #000;"></i>
                </span>
            @endif
        </div>
    @endif

    @if ($photo->isAd)
        <button type="button" data-ad-button="{{ $photo->button_url }}">{{ $photo->button_text }}</button>
    @endif
@endforeach
