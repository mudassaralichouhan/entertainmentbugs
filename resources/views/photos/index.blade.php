@extends('layouts.home')

@section('meta_tags')
    <meta name="_token" content="{{ csrf_token() }}">
    <meta name="description" content="These are the visual treats from different parts of the world.">
    <meta name="keywords" content="Entertainment photos, Comedy photos">
    <meta name="author" content="<?php echo isset($author) ? $author : 'Photos - Entertainment Bugs'; ?>">
    <meta property="og:site_name" content="{{ URL::to('/') }}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="<?php echo isset($ogtitle) ? $ogtitle : 'Photos - Entertainment Bugs'; ?>"/>
    <meta property="og:description"
          content="<?php echo isset($ogdescription) ? $ogdescription : 'These are the visual treats from different parts of the world.'; ?>"/>
    <meta property="og:url" content="<?php echo url()->current(); ?>"/>
    <meta property="og:image"
          content="<?php echo isset($ogimage) ? $ogimage : 'https://www.entertainmentbugs.com/public/uploads/logo/logo-social-media.png'; ?>"/>
    <meta name="keywords" content="<?php echo isset($keywords) ? $keywords : 'Entertainment photos, Comedy photos'; ?>">
@endsection

@section('style')
    <link href="/public/css/photo.css" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <p style="display:none">These are the visual treats from different parts of the world.</p>
    <div class="bg" style="background:#f4f3f3;">
        <div class="container">
            <div class="col-lg-12" id="extra-top-design">
                <div class="trending-rewards-tags">
                    <span class="hidden-xs"> All Tags </span>
                    <div class="span12" id="" style="width:100%">
                        <ul id="reward_tags1" class="owl-carousel" style="display:block">
                            @if (isset($tags) && count($tags) > 0)
                                @foreach ($tags as $tag)
                                    <li class="item"><a href="{{ url('photo/' . $tag) }}">{{ $tag }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                @if (Route::current()->getName() === 'trending_tags')
                    <div>
                        {{ request()->route()->parameters['id'] }}
                    </div>
                @endif
                <div class="photo_wrapper">
                    <div class="containes">
                        <div class="span12" id="category_photo_data" style="width:100%">
                            <div id="photo_tag_slide" class="owl-carousel" style="display:block">
                                @if ($latestPhotos && count($latestPhotos) > 0)
                                    @foreach ($latestPhotos as $key => $photo)
                                        @php
                                            $images = unserialize($photo->images);
                                            $tagsList = explode(',', $photo->tag_list);
                                        @endphp
                                        <div class="inner-photo" value="{{ $photo->id }}">
                                            <a class="pht0923" href="{{ url('/single-photo/' . $photo->id) }})">
                                                <img src="{{ photo_url($images[0]) }}">
                                            </a>
                                            <div class="photo_users">
                                                <a href="/userprofile/{{ $photo->user->slug }}"
                                                   data-photo="1687634679.png" class="user_img">
                                                    @if($photo->user->photo)
                                                        <img
                                                                src="{{ $photo->user->photo }}"
                                                                alt=""
                                                        />
                                                    @else
                                                        {{ name_to_pic("U") }}
                                                    @endif
                                                </a>
                                                <a href="https://www.entertainmentbugs.com/userprofile/{{ $photo->user->slug }}"
                                                   class="name_bl0982">{{ $photo->user->name }}</a>
                                            </div>
                                            <div class="photo-content-b">
                                                <div class="photo-content-bottom">
                                                    <h2>{{ $photo->photo_title }}</h2>
                                                    <div class="tag_click_main">
                                                        <div class="tag_click">
                                                            @foreach ($tagsList as $tag)
                                                                <a href="{{ url('photo/' . $tag) }}"
                                                                   class="tag_nme12">{{ $tag }}</a>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-block head-div">
                    <div class="cb-contents chanels-row" id="home_page_channel">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="rewards.html" class="color-active">
                                                <span class="visible-xs"> Latest Photos </span>
                                                <span class="hidden-xs"> All Photos </span>
                                            </a>
                                        </li>
                                        <li><a href="#">Popular Photos </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="small_ads">
                    <div data-ad-block data-ad-type='ad' data-ad-mode='video'></div>
                </div>
            </div>
            <div id="category_photo_data">
                <div class="photo-block-all-in-one" id="photo_lists">

                </div>
            </div>
            <div class="over-all">
                <a href="javascript:void(0)" id="closemodel" style="position:absolute; right:20px;top:20px;color:#fff;" class="cross">
                    <i class="cvicon-cv-cancel" style="font-size:24px"></i>
                </a>
                <div class="photo-container">
                    <div style="display:none" class="cross_block">
                        <a href="javaScript:void(0)" class="cross">
                            <i class="fas fa-angle-left" aria-hidden="true"></i>Back
                        </a>
                    </div>
                    <div class="photo-container-inside" id="popup-wrapper--desk"></div>
                    <button type="button" class="arrow arrow-prev">
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                    </button>
                    <button type="button" class="arrow arrow-next">
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
            <div class="mobile-view-photos-scroll" id="mobile-photos">
                <div class="cross_block" style="position:sticky">
                    <a href="javaScript:void(0)" class="cross" data-mobile-close>
                        <i class="fas fa-angle-left" aria-hidden="true"></i> Back
                    </a>
                </div>
                <div class="fw-container"></div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $.ajax({
                type: "GET",
                url: '/photos?action=photo_lists',
                success: function (response) {
                    $('#photo_lists').html(response);
                }
            });
        });

        $(document).on("submit", "[data-comment-form='commentfrmphotomobile']", function (e) {
            e.preventDefault();
            var form = $(this);
            var url = form.attr('action');
            const post = $(this).closest("[data-photo-id]");
            const isMobile = window.innerWidth <= 576 ? true : false;

            $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}",
                },
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function (response) {
                    const commentContainer = isMobile ? "fw-body" : "photo-container-all";
                    const comments = post.find(`.${commentContainer}>.comments>.read-comment`);
                    post.find(`.${commentContainer}>.comments>.read-comment`);

                    if (comments.css('display') != "block") {
                        comments.slideDown();
                    }

                    post
                        .find(`.${commentContainer}>.comments>.read-comment>.mobile-comments`)
                        .prepend(response.html);

                    post
                        .find("[data-comments-count]")
                        .text(parseInt(post.find("[data-comments-count]").text()) + 1);
                }
            });
            $(this)[0].reset();
        });

        $(".cross").on('click', function () {
            $('.my').attr('src', '');
        });

        $(document).on("click", ".photoIDLikes", function (e) {
            e.preventDefault();
            var video_key = $(this).attr('href');
            let that = $(this);

            $.ajax({
                type: 'get',
                url: 'liked-photo/' + video_key,
                success: function (data) {
                    if (data.disLikeCount > 0) {
                        that.find(".fa-thumbs-down").css('color', '#fff');
                    }
                    that.find("[data-likes-count]").text(data.likeCount);
                    that.find(".fa-thumbs-up").css('color', '#fff');
                }
            });

        });

        function categoryPhotosSortByRecent() {
            $.ajax({
                type: "GET",
                url: "{{ URL::to('category_photo_sort_by_recent') }}",
                data: {
                    'keyword': url
                },
                cache: false,
                success: function (response) {
                    $('#category_photo_data').empty().html(response.data);
                    fetchData();
                }

            });
        }

        function categoryPhotosSortByLikes() {
            $.ajax({
                type: "GET",
                url: "{{ URL::to('category_photo_sort_by_likes') }}",
                data: {
                    'keyword': url
                },
                cache: false,
                success: function (response) {
                    $('#category_photo_data').empty().html(response.data);
                }
            });
        }

        function followUser(user_id) {
            $.ajax({
                type: "GET",
                url: "<?php echo e(URL::route('follow_user')); ?>",
                data: {
                    'user_id': user_id
                },
                cache: false,
                success: function (response) {
                    $(`[data-photo-userid='${user_id}']`).html(response);
                }
            });
        }

        function unFollowUser(user_id) {
            $.ajax({
                type: "GET",
                url: "<?php echo e(URL::route('unfollow_user')); ?>",
                data: {
                    'user_id': user_id
                },
                cache: false,
                success: function (response) {
                    $(`[data-photo-userid='${user_id}']`).html(response);
                }
            });
        }
    </script>

    <script>
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////Working with page on scroll load data //////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////
        checkWindowSize();

        // Check if the page has enough content or not. If not then fetch records
        function checkWindowSize() {
            if ($(window).height() >= $(document).height()) {
                // Fetch records
                fetchData();
            }
        }

        // Fetch records
        function fetchData() {
            var start = Number($('#start').val());
            var allcount = Number($('#totalrecords').val());
            var rowperpage = Number($('#rowperpage').val());
            start = start + rowperpage;

            if (start <= allcount) {
                $('#start').val(start);

                $.ajax({
                    url: "{{ route('photo.load-ajax') }}",
                    data: {
                        start: start
                    },
                    dataType: 'json',
                    success: function (response) {
                        // Add
                        $("#category_photo_data").find(".photo-block-all-in-one").find('.photo-block:last')
                            .after(response.html).show().fadeIn("slow");

                        // Check if the page has enough content or not. If not then fetch records
                        checkWindowSize();
                        EBUGS.ads.load();
                    }
                });
            }
        }

        $(document).on('touchmove', onScroll); // for mobile

        function onScroll() {
            if ($(window).scrollTop() > $(document).height() - $(window).height() - 100) {
                fetchData();
            }
        }

        $(window).scroll(function () {
            var position = $(window).scrollTop();
            var bottom = $(document).height() - $(window).height() - 100;

            if (position >= bottom) {
                fetchData();
            }
        });

        $("#mobile-photos").on("click", ".click-comment-open-trigger", function () {
            event.preventDefault();
            $(this).closest(".fw-body").find(".read-comment").slideToggle("slow");
        });

        $("#mobile-photos").on("click", "[data-action='do-like']", function (e) {
            e.preventDefault();

            let my = $(this);
            let photoId = my.data('id');

            $.ajax({
                type: 'get',
                url: 'liked-photo/' + photoId,
                success: function (data) {
                    my.find("[data-likes-count]").text(data.likeCount);
                }
            });
        });
    </script>
@endsection

@section('js')

@endsection
