@extends('layouts.home')

@section('content')
{{ HTML::style('public/css/bootstrap-tagsinput.css') }}
{{ HTML::script('public/js/jquery.validate.js') }}
{{ HTML::script('public/js/bootstrap-tagsinput.js') }}
{{ HTML::script('public/js/bootstrap3-typeahead.js') }}
{{ HTML::script('public/js/script.js') }}

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<style type="text/css">
	img {
		display: block;
		max-width: 100%;
	}

	.preview {
		overflow: hidden;
		width: 160px;
		height: 160px;
		margin: 10px;
		border: 1px solid red;
	}

	.modal-lg {
		max-width: 1000px !important;
	}

	.select2-container--default.select2-container--focus .select2-selection--multiple {
		border: solid 1px #e0e1e2;
		border-radius: 2px;
	}

	.select2-container--default .select2-selection--multiple {
		border: solid 1px #e0e1e2;
		border-radius: 2px;
		padding: 9px 0;
	}

	.browse-photo {
		position: relative;
	}

	.browse-photo i {
		font-size: 42px;
		position: absolute;
		left: 0px;
		top: 7px;
		width: 150px;
		height: 150px;
	}

	.browse-photo {
		margin-bottom: 7px;
	}

	#inputPhoto-0 {
		font-size: 10px;
		width: 160px;
		margin: 4px 0 0 0;
	}

	#inputPhoto-1 {
		font-size: 10px;
		width: 160px;
		margin: 4px 0 0 0;
	}

	#inputPhoto-2 {
		font-size: 10px;
		width: 160px;
		margin: 4px 0 0 0;
	}

	#inputPhoto-3 {
		font-size: 10px;
		width: 160px;
		margin: 4px 0 0 0;
	}

	#inputPhoto-4 {
		font-size: 10px;
		width: 160px;
		margin: 4px 0 0 0;
	}

	.select2-container--default .select2-selection--multiple .select2-selection__choice {
		margin-top: 0px;
	}

	.btn-primary {
		background-color: #f72e5e;
		border-color: #f72e5e;
	}

	.btn-primary:hover {
		background-color: #f72e5e;
		border-color: #f72e5e;
	}

	.modal-header {
		padding: 15px 15px 0 15px;
		border-bottom: 1px solid #e5e5e5;
	}

	.modal-header .close {
		margin-top: -20px;
	}

	#ttl {
		font-size: 18px;
		font-family: 'Hind Guntur', sans-serif;
		font-weight: normal;
		margin: 0px 0 -12px 0px;
		background: #eceff0;
		color: #000;
		line-height: 38px;
		padding: 7px 0 1px 0;
		text-align: center;
	}
</style>
<form id="photoCreateForm1" name="photoCreateForm" method="POST" enctype="multipart/form-data" action="{{ url("photos/$photo->id") }}">

	@if (Session::has('success'))
		<div class="alert alert-success">
			{{ Session::get('success') }}
		</div>
	@endif
	
	@if ($errors->any())
		<div class="alert alert-danger">
			{!! implode('', $errors->all('<div>:message</div>')) !!}
		</div>
	@endif
	{{ method_field('PUT') }}
	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	<div class="content-wrapper upload-page edit-page">
		<div class="container" style="padding:0">
			<h2 id="ttl">Photo Upload </h2>
			<div class="row">
				<div class="col-lg-12">
					<div class="u-form">
						<div class="row">
							<br><br>
							<div class="col-lg-6">
								<div class="form-group">
									<label for="e1">Photo Title</label>
									<input type="text" class="form-control" id="photoTitle" placeholder="Rocket League Pro Championship Gameplay (100 characters remaining)" name="photoTitle" value="{{ $photo->photo_title }}" required>
									<input type="hidden" class="form-control" value="{{ session()->get('user_id') }}" id="photo_user_id" placeholder="Rocket League Pro Championship Gameplay (100 characters remaining)" name="photo_user_id">
									<span id="phototitleError"></span>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for="e1">Tag Followers (upto 5)</label>
									<select class="form-control js-example-tokenizer" multiple="multiple" name="followersList[]">
										@php
											$_followers = json_decode($photo->followers_list, true);
											
											if( !is_array($_followers) ){
												$_followers = [];
											}
										@endphp
										@foreach ($followers as $follower)
											@if (isset($follower->followers->name))
												<option {{ in_array($follower->followers->id, $_followers) ? "selected" : "" }} value="{{ $follower->followers->id }}">{{ $follower->followers->name }}</option>
											@endif
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label for="e1">Hash Tags</label>
										{{ Form::text('tags', $photo->tag_list, ['id' => 'photo-tags', 'close' => 'form-control', 'required' => true, 'placeholder' => 'Select mulitple tags']) }}

										<span class="error" id="videoTagsError"></span>
									</div>
								</div>
							</div>
							@php
								$images = unserialize($photo->images);
							@endphp
							<div class="col-lg-12">
								<div class="form-group">
									@foreach( $images as $key => $image )
										<div class="u-close" id="select-p" style="float:left; margin-right:20px;">
											<div class="browse-photo">
												<i class="fa fa-picture-o" aria-hidden="true"></i>
												<img style="position: relative;z-index: 1;" id="fileuploadedImage-{{ $key }}" src="{{ asset("public/uploads/photo/".$image) }}" />
												<input type="hidden" name="Photos[{{ $key }}][baseSixFourImageDataSource]" value="{{ $image }}" id="imagesource-{{ $key }}">
											</div>
											Browse photo<input type="file" class="image" id="inputPhoto-{{ $key }}" data-key-index="{{ $key }}" />
										</div>
									@endforeach
									<div style="clear:both; overflow:hidden"></div>
									<br>
									<a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Add More photos grid</a>
								</div>
							</div>
						</div>
						<style>
							.edit-page .u-area {
								text-align: center;
								padding-top: 12px;
								padding-bottom: 50px;
							}

							#select-p i {
								color: #637076;
								font-size: 66px;
								text-align: center;
								display: block;
								padding: 38px 0 0 0;
							}

							.upload-page .u-area i {
								font-size: 90px;
								color: #637076;
							}

							.upload-page .u-area .u-text1 {
								margin-top: 10px;
								margin-bottom: 9px;
								font-size: 14px;
							}

							.browse-photo {
								width: 150px;
								height: 150px;
								background: #eceff0
							}
						</style>
					</div>
					<div class="u-area mt-small">
						<button type="submit" class="btn btn-primary u-btn uploadPhotos"
							id="photoCreateFormBtn">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
	aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="modalLabel">Crop Photo</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="img-container">
					<div class="row">
						<input type="text" value="" id="dataKeyIndexValue">
						<div class="col-md-8">
							<img id="image" src="https://avatars0.githubusercontent.com/u/3456749">
						</div>
						<div class="col-md-4">
							<div class="preview"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-primary" id="crop">Crop</button>
			</div>
		</div>
	</div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.6/cropper.js"></script>
<script>
	/*-------Csrf Token Loaded---------*/
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

		}
	});
	var $modal = $('#modal');
	var image = document.getElementById('image');
	var cropper;
	$("body").on("change", ".image", function(e) {
		var currentindex = $(this).data('key-index');
		$('input#dataKeyIndexValue').val(currentindex);
		var files = e.target.files;
		var done = function(url) {
			image.src = url;
			$modal.modal('show');
		};
		var reader;
		var file;
		var url;
		if (files && files.length > 0) {
			file = files[0];
			if (URL) {
				done(URL.createObjectURL(file));
			} else if (FileReader) {
				reader = new FileReader();
				reader.onload = function(e) {
					done(reader.result);
				};
				reader.readAsDataURL(file);
			}

		}

	});
	$modal.on('shown.bs.modal', function() {
		cropper = new Cropper(image, {
			aspectRatio: 1,
			viewMode: 2,
			preview: '.preview'

		});
	}).on('hidden.bs.modal', function() {
		cropper.destroy();
		cropper = null;
	});
	$("#crop").click(function() {
		canvas = cropper.getCroppedCanvas({
			width: 540,
			height: 540,
		});
		canvas.toBlob(function(blob) {
			url = URL.createObjectURL(blob);
			var reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = function() {
				var base64data = reader.result;
				var indexloaded = $('input#dataKeyIndexValue').val();
				$('img#fileuploadedImage-' + indexloaded + '').attr("src", base64data);
				$('input#imagesource-' + indexloaded + '').val(base64data);
				$modal.modal('hide');
			}

		});
	})

	function validatedField() {
		var photoName = $('input#photoTitle').val();
		var error = '';
		if (photoName === '' || photoName === 'null' || photoName === 'undefined') {
			$('span#phototitleError').html('The Photo title field is required.')
			error = 1;
		} else {
			$('span#phototitleError').html('')

		}

		if (error === 1) {
			return false;
		} else {
			return true;
		}

	}

	$('#photoCreateForm').submit(function(e) {
		e.preventDefault();
		var validateForm = validatedField();
		if (validateForm === false) {
			return false
		}

		var formData = new FormData(this);
		formData.append("_method", "PUT");
		$.ajax({
			type: 'POST',
			url: "{{ url('photos/'.$photo->id) }}",
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: (response) => {
				if (response) {
					console.log(response);
					alert("Successfully save");
					setTimeout(function() {
						window.location.reload();
					}, 1000);
				}

			},
			error: function(xhr) {
				alert("Somehting Wrong");
				$('#photoCreateFormBtn').html("Save");
			},
			beforeSend: function(xhr) {
				$('#photoCreateFormBtn').html(
					"<span class=\"fa fa-spin fa-spinner\"></span> Processing...");
			},
		});
	});
	$('#photo-tags').tagsinput({
		confirmKeys: [13, 188],
		maxTags: 5
	});
	$('#photo-tags').prev().find("input").on('keypress input', function(e){
		if (e.keyCode == 13){
			e.keyCode = 188;
			e.preventDefault();
		};
	});

	$(".js-example-tokenizer").select2({
		multiple: true,
		tokenSeparators: [',', ' ']
	});
</script>
@endsection
