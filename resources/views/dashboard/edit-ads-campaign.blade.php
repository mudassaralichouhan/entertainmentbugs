@extends('layouts.home')
@section('content')
    @include('dashboard/nav')
    <style>
        #promote_page_image .img:hover {
            background: #f72e5e !important;
            color: #fff !important
        }

        #promote_page_image .img:hover i.fa-check {
            color: #f72e5e !Important;
        }

        #promote_page_image .img:hover i {
            color: #fff !important
        }

        #promote_page_image .img {
            position: relative
        }

        #promote_page_image .img:hover .circle-tick {
            display: block;
        }

        #promote_page_image .circle-tick {
            display: none;
            background: #fff !Important;
            color: #f72e5e;
            border-radius: 100px;
            width: 40px;
            height: 40px;
            position: absolute;
            right: 16px;
            top: 18px;
        }

        #promote_page_image .circle-tick i {
            color: #f72e5e !Important;
            margin: 8px 0 0 0 !important;
            font-size: 22px;
        }

        #promote_page_image .circle-tick i {
            color: #f72e5e !Important;
            margin: 8px 0 0 0 !important;
            font-size: 22px;
        }

        .create-new-ads-campaign a {
            color: #f72e5e;
        }

        .upload-page .u-area .u-btn,
        .u-area .u-btn.submit-btn,
        .u-area .u-btn.preview-btn {
            margin-top: 10px;
            background-color: #f72e5e;
            color: white;
            padding: 15px 65px;
            border: solid 1px #f72e5e;
            height: 48px;
            border-radius: 24px;
            font-weight: 500;
        }

        #promote_page_image {
            padding: 0px;
        }

        #promote_page_image .img {
            float: left;
            margin: 0 0.5% 10px 0.5%;
            width: 24%;
        }

        #promote_page_image .img i {
            display: block;
            text-align: center;
        }

        .clear {
            clear: both;
            overflow: hidden
        }

        .img#margin-left_img {
            margin-left: 25%
        }

        #promote_page_image .img cv {
            display: block;
            text-align: center;
            color: #f72e5e;
            font-size: 56px;
            margin: -35px 0 22px 0
        }

        .spacer2 {
            height: 100px;
            clear: both;
            overflow: hidden
        }

        .adblock .img {
            background-color: #fff;
            border-radius: 3px;
            width: 336px;
            height: 280px;
            color: #a1a1a1;
            text-align: center;
            padding-top: 120px;
        }

        #promote_page_image .img:hover {
            background: #f72e5e !important;
            color: #fff !important;
        }

        #promote_page_image .img {
            position: relative;
        }

        #promote_page_image .img {
            cursor: pointer;
        }

        #promote_vieo_title h3 {
            float: none;
        }

        #promote_vieo_title h3 {
            width: 100%;
            font-size: 18px;
            font-size: 17px;
            padding: 7px 0 3px 0;
        }

        .form-group {
            margin-bottom: 12px;
        }

        .edit-page .u-area {
            text-align: center;
            padding-top: 16px;
        }


        .price_block {
            margin: -3px 0 0 11px;
        }

        label.checkbox {
            float: left;
            margin: 0px 15px 0 0;
        }
        #multi-form-steps{
            position: relative;
            display: flex;
            padding: 10px 0;
            margin-bottom: 10px;
            justify-content: space-between
        }
        #multi-form-steps::after{
            content: "";
            position: absolute;
            width: calc(100% - 40px);
            height: 2px;
            background: #a1a1a1;
            top: 50%;
            left: 20px;
            z-index: 0;
            transform: translateY(-50%);
        }
        #multi-form-steps .step{
            background-color: #a1a1a1;
            color: white;
            flex: 0 0 40px;
            width: 40px;
            height: 40px;
            font-size: 20px;
            font-weight: 600;
            border-radius: 50%;
            display: flex;
            justify-content: center;
            align-items: center;
            position: relative;
            z-index: 1;
        }
        #multi-form-steps .step.active{
            background-color: #f72e5e
        }
        #ads-form .imgplace{
            width: 200px;
            height: 200px;
            background: #e0e1e2;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        #ads-form .imgplace img{
            width: 100%;
            height: 100%;
            border: 1px solid #a1a1a1;
        }
    </style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
{{ HTML::style('public/css/bootstrap-tagsinput.css')}}
{{ HTML::script('public/js/jquery.validate.js')}}
{{ HTML::script('public/js/bootstrap-tagsinput.js')}}
{{ HTML::script('public/js/bootstrap3-typeahead.js')}}
<div class="right-all">
    <form method="POST" autocomplete="off" id="ads-form">
        <input type="hidden" name="_method" value="PUT" />
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <div id="head-title" class="col-lg-12">
            <div class="dashboard_head">
                <h2>Edit Ads Campaign</h2>
            </div>
        </div>
        <div class="ads-section-3 ads-msf" data-step="3">
            @if( $ad->type == "video" )
                <div class="upload-page edit-page video-block">
                    <div class="container-fluid u-details-wrap">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="u-details">
                                    <div class="row">
                                        <div class="ud-caption" id="promote_vieo_title">
                                            <h3>Entertainment AdSense Video </h3>
                                        </div>
                                        <!--video-->
                                        <div class="col-lg-2">
                                            <div class="imgplace" data-video-thumb-place>
                                                <img src="{{ $ad->thumb }}" alt="" />
                                                <i class="fa fa-picture-o" style="font-size: 80px; display: none"></i>
                                            </div>
                                        </div>
                                        <div class="col-lg-10">
                                            {{-- <div class="u-title">Rocket League Gameplay <span class="hidden-xs">03.08.2016 - 15.13.31.01</span></div> --}}
                                            {{-- <div class="u-size">12.6 MB . 15 SEC Remaining</div> --}}
                                            <div class="u-progress" style="display: none" id="video-progressbar">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                        <span class="sr-only">0% Complete</span>
                                                    </div>
                                                </div>
                                                {{-- <div class="u-close">
                                                    <a href="#"><i class="cvicon-cv-cancel"></i></a>
                                                </div> --}}
                                            </div>
                                            {{-- <div class="u-desc">Your Video is still uploading, please keep this page open until it's done.</div> --}}
                                            <input type="file" id="video-file" style="display: none" accept="video/*" />
                                            <input type="file" id="browse-thumb-file" style="display: none" accept="image/*" />
                                            <div id="browse-thumb-content">
                                                <button type="button" style="background:#f72e5e;color:#fff;padding:0 10px" id="browse-video">Browse Video</button>
                                                <button type="button" data-croppie data-croppie-file="#browse-thumb-file" data-croppie-bind="file" data-croppie-input="#msf-thumb" data-croppie-output="[data-video-thumb-place]>img" style="background:#f72e5e;color:#fff;padding:0 10px" id="browse-thumbnail">Browse Thumbnail</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="u-form">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="e1">Video Title</label>
                                                <input type="text" name="video[title]" class="form-control" id="e1" value="{{ $ad->title }}" placeholder="Rocket League Pro Championship Gameplay (36 characters remaining)">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="e1">Video Tags</label>
                                                <input type="text" name="video[tags]" value="{{ $ad->tags }}" class="form-control" id="video-tags" placeholder="Type any 3">
                                            </div>
                                        </div>
    
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="e1">Video Language</label>
                                                <input type="text" name="video[language]" value="{{ $ad->language }}" class="form-control" id="video-language-select"
                                                    placeholder="Type any 3">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="e4"> Interested Category Select 5 </label>
                                                <select style="width:100%;padding: 8px 0 7px 13px;border: solid 1px #e0e1e2;" name="video[category][]" id="video-category" >
                                                    @php $categories = explode(", ", $ad->categories); @endphp
                                                    <option {{ in_array("Film &amp; Animation", $categories) ? "selected":"" }}> Film &amp; Animation </option>
                                                    <option {{ in_array("Serials", $categories) ? "selected":"" }}>Serials</option>
                                                    <option {{ in_array("Cars & Vehicles", $categories) ? "selected":"" }}>Cars &amp; Vehicles</option>
                                                    <option {{ in_array("Music", $categories) ? "selected":"" }}>Music</option>
                                                    <option {{ in_array("Sports", $categories) ? "selected":"" }}>Sports</option>
                                                    <option {{ in_array("Science", $categories) ? "selected":"" }}>Science &amp; Technology</option>
                                                    <option {{ in_array("Entertainment", $categories) ? "selected":"" }}>Entertainment</option>
                                                    <option {{ in_array("Pets & Animals", $categories) ? "selected":"" }}>Pets &amp; Animals</option>
                                                    <option {{ in_array("Non-profits & Activism", $categories) ? "selected":"" }}>Non-profits &amp; Activism</option>
                                                    <option {{ in_array("Travel", $categories) ? "selected":"" }}>Travel &amp; Events</option>
                                                    <option {{ in_array("People Blogs", $categories) ? "selected":"" }}>People &amp; Blogs </option>
                                                    <option {{ in_array("Comedy", $categories) ? "selected":"" }}>Comedy</option>
                                                    <option {{ in_array("Gaming", $categories) ? "selected":"" }}>Gaming</option>
                                                    <option {{ in_array("News & Politics", $categories) ? "selected":"" }}>News &amp; Politics</option>
                                                    <option {{ in_array("Education", $categories) ? "selected":"" }}>Education</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="e3">Button name</label>
                                                <select
                                                    style="width:100%;padding: 8px 0 7px 13px;border: solid 1px #e0e1e2;margin-bottom:10px" name="video[button]">
                                                    <option> Select </option>
                                                    <option {{ $ad->button_name == "Shop Now" ? "selected" : "" }}>Shop Now</option>
                                                    <option {{ $ad->button_name == "Visit Now" ? "selected" : "" }}>Visit Now</option>
                                                    <option {{ $ad->button_name == "Book Now" ? "selected" : "" }}>Book Now</option>
                                                    <option {{ $ad->button_name == "View Now" ? "selected" : "" }}>View More</option>
                                                    <option {{ $ad->button_name == "Apply Now" ? "selected" : "" }}>Apply Now</option>
                                                    <option {{ $ad->button_name == "Whats App" ? "selected" : "" }}>Whats App</option>
                                                    <!-- if yes then open a input box for number file-->
                                                    <option {{ $ad->button_name == "Others" ? "selected" : "" }}>Others</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="e3">Website link</label>
                                                <input type="text" name="video[website]" class="form-control" id="e1" value="{{ $ad->website_link }}" placeholder="your website link">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="e2">Description (25 Words)</label>
                                                <textarea class="form-control" id="e2" rows="3" name="video[description]" placeholder="Description">{{ $ad->description }}</textarea>
                                            </div>
                                        </div>
                                    </div>
    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="upload-page edit-page image-block">
                    <div class="container-fluid u-details-wrap">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-danger" id="img-errors"></div>
                                <div class="u-details">
                                    <div class="row">
                                        <div class="ud-caption" id="promote_vieo_title">
                                            <h3>Entertainment AdSense Image </h3>
                                        </div>
                                        <!--video-->
                                        <div class="col-lg-2">
                                            <div class="imgplace" data-image-place>
                                                <img src="{{ asset('public/ads/images/'.$ad->file) }}" alt="" />
                                                <i class="fa fa-picture-o" style="font-size: 80px;display: none"></i>
                                            </div>
                                        </div>
                                        <div class="col-lg-10">
                                            <br>
                                            <div class="u-progress" id="img-progress-container" style="display: none">
                                                <div class="progress">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                                                        <span class="sr-only" data-progress="text">0% Complete</span>
                                                    </div>
                                                </div>
                                                {{-- <div class="u-close">
                                                    <a href="#"><i class="cvicon-cv-cancel"></i></a>
                                                </div> --}}
                                            </div>
                                            <div id="browse-thumb-content" data-image-browse-btn>
                                                <input type="file" accept="image/*" style="display: none;" id="msf-image-input" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="u-form">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="e1">Banner Title</label>
                                                <input type="text" name="image[title]" class="form-control" value="{{ $ad->title }}" id="e1" placeholder="Rocket League Pro Championship Gameplay (36 characters remaining)">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="e1">Banner Tags</label>
                                                <input type="text" class="form-control" value="{{ $ad->tags }}" placeholder="Type any 3" name="image[tags]" id="banner-tags" >
                                            </div>
                                        </div>
    
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="e1">Language (Optional)</label>
                                                <input type="text" class="form-control" value="{{ $ad->language }}" placeholder="Type any 2" name="image[language]" id="banner-language-select">
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="e4"> Interested Category Select 5 </label>
                                                <select id="banner-category" name="image[category][]" style="width:100%;padding: 8px 0 7px 13px;border: solid 1px #e0e1e2;" multiple>
                                                    @php $categories = explode(", ", $ad->categories); @endphp
                                                    <option {{ in_array("Film &amp; Animation", $categories) ? "selected":"" }}> Film &amp; Animation </option>
                                                    <option {{ in_array("Serials", $categories) ? "selected":"" }}>Serials</option>
                                                    <option {{ in_array("Cars & Vehicles", $categories) ? "selected":"" }}>Cars &amp; Vehicles</option>
                                                    <option {{ in_array("Music", $categories) ? "selected":"" }}>Music</option>
                                                    <option {{ in_array("Sports", $categories) ? "selected":"" }}>Sports</option>
                                                    <option {{ in_array("Science", $categories) ? "selected":"" }}>Science &amp; Technology</option>
                                                    <option {{ in_array("Entertainment", $categories) ? "selected":"" }}>Entertainment</option>
                                                    <option {{ in_array("Pets & Animals", $categories) ? "selected":"" }}>Pets &amp; Animals</option>
                                                    <option {{ in_array("Non-profits & Activism", $categories) ? "selected":"" }}>Non-profits &amp; Activism</option>
                                                    <option {{ in_array("Travel", $categories) ? "selected":"" }}>Travel &amp; Events</option>
                                                    <option {{ in_array("People Blogs", $categories) ? "selected":"" }}>People &amp; Blogs </option>
                                                    <option {{ in_array("Comedy", $categories) ? "selected":"" }}>Comedy</option>
                                                    <option {{ in_array("Gaming", $categories) ? "selected":"" }}>Gaming</option>
                                                    <option {{ in_array("News & Politics", $categories) ? "selected":"" }}>News &amp; Politics</option>
                                                    <option {{ in_array("Education", $categories) ? "selected":"" }}>Education</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="e3">Button name</label>
                                                <select name="image[button]" style="width:100%;padding: 8px 0 7px 13px;border: solid 1px #e0e1e2;margin-bottom:10px">
                                                    <option {{ $ad->button_name == "Shop Now" ? "selected" : "" }}>Shop Now</option>
                                                    <option {{ $ad->button_name == "Visit Now" ? "selected" : "" }}>Visit Now</option>
                                                    <option {{ $ad->button_name == "Book Now" ? "selected" : "" }}>Book Now</option>
                                                    <option {{ $ad->button_name == "View Now" ? "selected" : "" }}>View More</option>
                                                    <option {{ $ad->button_name == "Apply Now" ? "selected" : "" }}>Apply Now</option>
                                                    <option {{ $ad->button_name == "Whats App" ? "selected" : "" }}>Whats App</option>
                                                    <!-- if yes then open a input box for number file-->
                                                    <option {{ $ad->button_name == "Others" ? "selected" : "" }}>Others</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label for="e3">Website link</label>
                                                <input type="text" name="image[website]" value="{{ $ad->website_link }}" class="form-control" id="e1"
                                                    placeholder="your website link">
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="e2">Description (25 Words)</label>
                                                <textarea class="form-control" name="image[description]" id="e2" rows="3" placeholder="Description">{{ $ad->description }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both; overflow:hidden"></div>
                </div>
            @endif
            <input type="hidden" name="file" id="msf-file" value="{{ $ad->file }}" />
            <input type="hidden" name="thumb" id="msf-thumb" value="{{ $ad->thumb }}" />
            <div class="u-area mt-small" style="padding-left: 20px">
                <input type="submit" value="Save & Proceed" class="btn btn-primary u-btn submit-btn" id="submit-btn" />
            </div>
        </div>
    </form>

        <div class="containera ads-section-4 ads-msf" data-step="4" style="display: none">
            <div class="col-lg-12">
                <div class="u-details">
                    <div class="row">
                        <div class="col-lg-12s ud-caption" id="promote_vieo_title">
                            <h3 data-preview="title"></h3>
                        </div>
                        <div class="col-lg-5" style="padding-right:0px">
                            <video data-preview="video" id="" width="100%" controls style="display: none">
                                <source src="" type="video/mp4">
                            </video>
                            <div class="imgplace" data-preview="image" style="display: none">
                                <img src="" />
                            </div>

                            <br>

                            <div id="video_up_preview">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="e2">About:</label>
                                        <span data-preview="description">
                                            
                                        </span>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="e1">Tags:</label>
                                        <span data-preview="tags"></span>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="e3">Language:</label>
                                        <span data-preview="language"></span>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="e3">Category:</label>
                                        <span data-preview="category"></span>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="e3">State:</label>
                                        <span>kolkata, delhi</span>
                                    </div>
                                </div>
                            </div>

                            {{-- <a class="btn btn-primary u-btn" href="company_promotion.html"
                                style="margin-top: 10px;background-color: #f72e5e;color: white;padding: 7px 41px;border: solid 1px #f72e5e;height: 34px;border-radius: 24px;font-weight: 500;">
                                Edit </a> --}}
                        </div>
                        <div class="col-lg-7">
                            <div class="small_box_plan" style="width:100%">
                                <div class="price_block">
                                    Choose plan on Cost per views - CPV and Cost per click - CPV
                                    <div class="checkbox">
                                        <label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="#">
                                                <span class="arrow"></span>
                                            </label> Rs: 100/- (800-1200) 1 Day
                                        </label>
                                    </div>

                                    <div class="checkbox">
                                        <label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="#">
                                                <span class="arrow"></span>
                                            </label> Rs: 200/- (1200-2000) 3 Days
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="#">
                                                <span class="arrow"></span>
                                            </label> Rs: 500/- (2000-5000) 7 Days
                                        </label>
                                        <br>
                                    </div>


                                    <div class="checkbox">
                                        <label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="#">
                                                <span class="arrow"></span>
                                            </label> Rs: 1000/- (5000-7500) 10 Days
                                        </label>
                                        <br>
                                    </div>


                                    <div class="checkbox" style="margin-bottom:20px">
                                        <label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="#">
                                                <span class="arrow"></span>
                                            </label> Rs: 2000/- (7500-10000) 30 Days
                                        </label>
                                        <br>
                                    </div>
                                    <div class="checkbox" style="margin-bottom:5px">
                                        <label class="">Choose Country </label>
                                        <select
                                            style="display: block;width: 86%;margin-left: 19px;padding: 6px 15px 3px 15px;border: solid 1px #e0e1e2;">
                                            <option>India</option>
                                            <option>USA</option>
                                        </select>
                                        <br>
                                    </div>


                                    <div class="checkbox" style="margin-bottom:10px">
                                        <label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="#">
                                                <span class="arrow"></span>
                                            </label> All States
                                        </label>
                                        <br>
                                    </div>
                                    <div class="checkbox" style="margin-bottom:10px">
                                        <label>
                                            <label class="checkbox">
                                                <input type="checkbox" name="#">
                                                <span class="arrow"></span>
                                            </label> Selected States
                                        </label>
                                        <br>
                                    </div>

                                    <div class="col-lg-11" style="margin-top:10px">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="e1"
                                                placeholder="Choose any 5 States"
                                                style="padding: 5px 20px 0 20px;line-height: 41px;">
                                        </div>
                                    </div>
                                    <button type="button" id="save-campaign" class="btn btn-primary u-btn" href="company_promotion.html" style="margin-top: 60px;background-color: #f72e5e;color: white;padding: 15px 65px;border: solid 1px #f72e5e;height: 48px;border-radius: 24px;font-weight: 500;"> Save Campaign </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <br>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <script>
        $(function(){
            let section = 4;
            let block = "{{ $ad->type }}";
            let notBlock = "";
            let template = "";
            const sectionOne = $(".ads-section-1");
            const sectionTwo = $(".ads-section-2");
            const sectionThree = $(".ads-section-3");
            const sectionFour = $(".ads-section-4");
            const steps = $("#multi-form-steps");
            const submitBtn = $("#submit-btn");
            const previewBtn = $("#preview-btn");
            const saveCampaign = $("#save-campaign");
            const adsForm = $("#ads-form");
            let splitted = [];

            saveCampaign.on("click", function(){
                submitBtn.click();
            });

            $("#ads-form").on("submit", function(e){
                if( section != 4 ){
                    e.preventDefault();
                }
            })

            $("#browse-video").on("click", function(){
                if( section == 3 && block == "video" ){
                    $("#video-file").click();
                }
            })

            $("#video-file").on("change", function(){
                if( section == 3 && block == "video" ){
                    var files = this.files;
                    if( files && files[0] ){
                        var imageType = /video.*/;
                        var file = files[0];
                        // check file type
                        if (!file.type.match(imageType)) {  
                            alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
                            return false;	
                        } 
                        // check file size
                        if (parseInt(file.size / 1024) > (1024*50)) {  
                            alert("File \""+file.name+"\" is too big");
                            return false;
                        }
                        processVideo(file);
                    } else {
                        console.log("file not selected");
                    }
                } else {
                    console.log("invalid block selected");
                }
            });

            async function processVideo(file){
                const fileContent = URL.createObjectURL(file);
                const video = await EBUGS.utils.getThumbnailForVideo(fileContent);
                
                if( video.duration <= parseInt(template) ){
                    var formdata = new FormData();
                    formdata.append("video", file);
                    formdata.append("thumb", video.thumb);
                    formdata.append("store", JSON.stringify({path: "public/ads/videos"}));
                    $("#video-progressbar").show();
                    
                    const xhr = new XMLHttpRequest();
                    xhr.open("POST", "upload/video");
                    
                    xhr.upload.addEventListener("progress", function (event) {
                        //console.log(event);
                        if (event.lengthComputable) {
                            $("#video-progressbar .progress-bar").css("width",(event.loaded / event.total) * 100 + "%");
                            $("#video-progressbar .progress-bar").css("aria-valuenow",(event.loaded / event.total) * 100);
                            $("#video-progressbar .sr-only").html(" "+((event.loaded / event.total) * 100).toFixed() + "%");
                            // $("#video-progressbar .up-done").html((parseInt(parseInt(event.loaded / 1024))/1024).toFixed(2));
                            // $('#video-progressbar .percent-process').html('<strong>'+parseInt((event.loaded / event.total) * 100).toFixed(0)+'%</strong>');
                        }
                        else {
                            alert("Failed to compute file upload length");
                        }
                    }, false);
                    xhr.onreadystatechange = function (oEvent) {  
                        if (xhr.readyState === 4) {  
                            if (xhr.status === 200) {
                                $("#video-progressbar .progress-bar").css("width", "100%");
                                $("#video-progressbar .progress-bar").css("aria-valuenow", "100");
                                $("#video-progressbar .sr-only").html("100%");
                            } else {
                                alert("Error"+ xhr.statusText);  
                            }  
                        }
                    }; 

                    xhr.onload  = function() {
                        var jsonResponse = xhr.response;
                        result = JSON.parse(jsonResponse);
                        if(result.status == true){
                            $("#msf-file").val(result.file);
                            $("#msf-thumb").val(result.thumbnail);
                            $("[data-video-thumb-place] img").attr("src", result.thumbnail).show();
                            $("[data-video-thumb-place] i").hide();
                        }else{
                            alert('File is not uploading. Please try again.')
                        }	  
                    };

                    var csrfToken = "{{ csrf_token() }}";
                    xhr.setRequestHeader('X-csrf-token', csrfToken); 	
                    
                    xhr.send(formdata);
                } else {
                    alert("Your video duration is longer than selected template");
                }
            }
            
            $(".ads-section-1 .adblock>.img").on("click", toStepTwo);
            $(".ads-section-2 .adblock .img").on("click", toStepThree);
            previewBtn.on("click", toStepFour);

            function toStepTwo(){
                block = $(this).data('block');
                section = 2;
                notBlock = block == "image" ? "video" : "image";

                sectionOne.hide();
                sectionTwo.show();
                sectionTwo.find(`.${block}-block`).show();
                sectionTwo.find(`.${notBlock}-block`).hide();

                $("#msf-type").val(block);
                changeStep(section);
                previewBtn.hide();
            }

            function toStepThree(){
                section = 3;
                template = $(this).data("template");

                sectionTwo.hide();
                sectionThree.find(`.${block}-block`).show();
                sectionThree.find(`.${notBlock}-block`).hide();
                sectionThree.show();
                
                $("#msf-template").val(template);
                changeStep(section);
                previewBtn.show();

                if( block == "image" ){
                    browseImageBtn(template);
                }
            }

            function toStepFour(){
                section = 4;
                sectionThree.hide();
                // sectionFour.find(`.${block}-block`).show();
                // sectionFour.find(`.${notBlock}-block`).hide();
                sectionFour.show();
                
                let selectedBlock = sectionThree.find(`.${block}-block`);

                $("[data-preview='title']").text(selectedBlock.find(`[name='${block}[title]']`).val());
                $("[data-preview='description']").text(selectedBlock.find(`[name='${block}[description']`).val());
                $("[data-preview='tags']").text(selectedBlock.find(`[name='${block}[tags]']`).val());
                $("[data-preview='language']").text(selectedBlock.find(`[name='${block}[language]']`).val());
                $("[data-preview='category']").text(selectedBlock.find(`[name='${block}[category][]']`).val().join(", "));
                $(`[data-preview='${block}']`).show();
                $(`[data-preview='${block}']>*`).attr("src", BASE.url(`public/ads/${block}s/${$("#msf-file").val()}`));

                if( block == "video" ){
                    $(`[data-preview='${block}']`).attr('poster', $("[name='thumb']").val());
                    $(`[data-preview='${block}']`).get(0).load();
                }

                changeStep(section);
                previewBtn.hide();
            }

            function changeStep(activeStep){
                steps.find(".step").removeClass("active");
                steps.find(".step[data-step='"+activeStep+"']").addClass("active");
            }

            function browseImageBtn(template){
                splitted = template.split("x");
                // if( splitted[0] == splitted[1] ){
                //     splitted[0] = splitted[1] = 200;
                // }
                const btn = `<button type="button" style="background:#f72e5e;color:#fff;padding:0 10px" data-croppie data-croppie-file="#msf-image-input" data-croppie-bind="file" data-croppie-store="public/ads/images" data-croppie-viewport='{"width": ${splitted[0]}, "height": ${splitted[1]}}' data-croppie-boundary='{"width": ${splitted[0]}, "height": ${splitted[1]}}' >Browse Image</button>`;
                
                if( $("[data-image-browse-btn] button").length ){
                    $("[data-image-browse-btn] button").remove();
                }

                $("[data-image-browse-btn]").append(btn);
                EBUGS.croppie.load($("[data-image-browse-btn] button:last-child"), croppieCallback);
            }

            function croppieCallback(response, result){
                $("#msf-file").val(result.file);
                $("[data-image-place] img").attr("src", response).show();
                $("[data-image-place]").css({width: splitted[0], height: splitted[1]});
                $("[data-image-place] i").hide();
            }

            $("#banner-tags").tagsinput();
            $("#video-tags").tagsinput();
            
            $("#banner-language-select").tagsinput({
                    typeahead: {
                    source: ['Afar', 'Abkhaz', 'Avestan', 'Afrikaans', 'Akan', 'Amharic','Aragonese', 'Arabic', 'Assamese', 'Avaric', 'Aymara', 'Azerbaijani','South Azerbaijani', 'Bashkir', 'Belarusian', 'Bulgarian', 'Bihari', 'Bislama',
                    'Bambara', 'Bengali', 'Tibetan Standard', 'Breton', 'Bosnian', 'Catalan; Valencian','Chechen', 'Chamorro', 'Corsican', 'Cree', 'Czech', 'Old Church Slavonic','Chuvash', 'Welsh', 'Danish', 'German', 'Maldivian', 'Dzongkha',
                    'Ewe', 'Greek', 'English', 'Esperanto', 'Spanish', 'Estonian','Basque', 'Persian ', 'Fula', 'Finnish', 'Fijian', 'Faroese',' French', 'Western Frisian', 'Irish', 'Scottish Gaelic', 'Galician', 'Guaraní',
                    'Gujarati', 'Manx', 'Hausa', 'Hebrew ', 'Hindi', 'Hiri Motu','Croatian', 'Haitian', 'Hungarian', 'Armenian', 'Herero', 'Interlingua',' Indonesian', 'Interlingue', 'Igbo', 'Nuosu', 'Inupiaq', 'Ido',
                    'Icelandic', 'Italian', 'Inuktitut', 'Japanese', 'Javanese', 'Georgian','Kongo', 'Gikuyu', 'Kwanyama', 'Kazakh', 'Kalaallisut', 'Khmer','Kannada', 'Korean', 'Kanuri', 'Kashmiri', 'Kurdish', 'Komi',
                    'Cornish', 'Kyrgyz', 'Latin', 'Luxembourgish', 'Ganda', 'Limburgish','Lingala', 'Lao', 'Lithuanian', 'Luba-Katanga', 'Latvian', 'Malagasy','Marshallese', 'Māori', 'Macedonian', 'Malayalam', 'Mongolian', 'Marathi',
                    'Malay', 'Maltese', 'Burmese', 'Nauru', 'Norwegian Bokmål', 'North Ndebele','Nepali', 'Ndonga', 'Dutch', 'Norwegian Nynorsk', 'Norwegian', 'South Ndebele','Navajo', 'Chichewa', 'Occitan', 'Ojibwe', 'Oromo', 'Oriya',
                    'Ossetian', 'Panjabi', 'Pāli', 'Polish', 'Pashto', 'Portuguese','Quechua', 'Romansh', 'Kirundi', 'Romanian', 'Russian', 'Kinyarwanda','Sanskrit', 'Sardinian', 'Sindhi', 'Northern Sami', 'Sango', 'Sinhala',
                    'Slovak', 'Slovene', 'Samoan', 'Shona', 'Somali', 'Albanian','Serbian', 'Swati', 'Southern Sotho', 'Sundanese', 'Swedish', 'Swahili','Tamil', 'Telugu', 'Tajik', 'Thai', 'Tigrinya', 'Turkmen',
                    'Tagalog', 'Tswana', 'Tonga', 'Turkish', 'Tsonga', 'Tatar','Twi', 'Tahitian', 'Uyghur', 'Ukrainian', 'Urdu	', 'Uzbek','Venda', 'Vietnamese', 'Volapük', 'Walloon', 'Wolof', 'Xhosa',
                    'Yiddish', 'Yoruba', 'Zhuang', 'Chinese', 'Zulu']
                },
                freeInput: true
            });
            $("#video-language-select").tagsinput({
                    typeahead: {
                    source: ['Afar', 'Abkhaz', 'Avestan', 'Afrikaans', 'Akan', 'Amharic','Aragonese', 'Arabic', 'Assamese', 'Avaric', 'Aymara', 'Azerbaijani','South Azerbaijani', 'Bashkir', 'Belarusian', 'Bulgarian', 'Bihari', 'Bislama',
                    'Bambara', 'Bengali', 'Tibetan Standard', 'Breton', 'Bosnian', 'Catalan; Valencian','Chechen', 'Chamorro', 'Corsican', 'Cree', 'Czech', 'Old Church Slavonic','Chuvash', 'Welsh', 'Danish', 'German', 'Maldivian', 'Dzongkha',
                    'Ewe', 'Greek', 'English', 'Esperanto', 'Spanish', 'Estonian','Basque', 'Persian ', 'Fula', 'Finnish', 'Fijian', 'Faroese',' French', 'Western Frisian', 'Irish', 'Scottish Gaelic', 'Galician', 'Guaraní',
                    'Gujarati', 'Manx', 'Hausa', 'Hebrew ', 'Hindi', 'Hiri Motu','Croatian', 'Haitian', 'Hungarian', 'Armenian', 'Herero', 'Interlingua',' Indonesian', 'Interlingue', 'Igbo', 'Nuosu', 'Inupiaq', 'Ido',
                    'Icelandic', 'Italian', 'Inuktitut', 'Japanese', 'Javanese', 'Georgian','Kongo', 'Gikuyu', 'Kwanyama', 'Kazakh', 'Kalaallisut', 'Khmer','Kannada', 'Korean', 'Kanuri', 'Kashmiri', 'Kurdish', 'Komi',
                    'Cornish', 'Kyrgyz', 'Latin', 'Luxembourgish', 'Ganda', 'Limburgish','Lingala', 'Lao', 'Lithuanian', 'Luba-Katanga', 'Latvian', 'Malagasy','Marshallese', 'Māori', 'Macedonian', 'Malayalam', 'Mongolian', 'Marathi',
                    'Malay', 'Maltese', 'Burmese', 'Nauru', 'Norwegian Bokmål', 'North Ndebele','Nepali', 'Ndonga', 'Dutch', 'Norwegian Nynorsk', 'Norwegian', 'South Ndebele','Navajo', 'Chichewa', 'Occitan', 'Ojibwe', 'Oromo', 'Oriya',
                    'Ossetian', 'Panjabi', 'Pāli', 'Polish', 'Pashto', 'Portuguese','Quechua', 'Romansh', 'Kirundi', 'Romanian', 'Russian', 'Kinyarwanda','Sanskrit', 'Sardinian', 'Sindhi', 'Northern Sami', 'Sango', 'Sinhala',
                    'Slovak', 'Slovene', 'Samoan', 'Shona', 'Somali', 'Albanian','Serbian', 'Swati', 'Southern Sotho', 'Sundanese', 'Swedish', 'Swahili','Tamil', 'Telugu', 'Tajik', 'Thai', 'Tigrinya', 'Turkmen',
                    'Tagalog', 'Tswana', 'Tonga', 'Turkish', 'Tsonga', 'Tatar','Twi', 'Tahitian', 'Uyghur', 'Ukrainian', 'Urdu	', 'Uzbek','Venda', 'Vietnamese', 'Volapük', 'Walloon', 'Wolof', 'Xhosa',
                    'Yiddish', 'Yoruba', 'Zhuang', 'Chinese', 'Zulu']
                },
                freeInput: true
            });

            $("#banner-category").select2({
                multiple: true
            });
            
            $("#video-category").select2({
                multiple: true
            });
        });
    </script>
@endsection
