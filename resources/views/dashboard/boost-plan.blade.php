@extends('layouts.home')
@section('content')
    @include('dashboard/nav')
    <style>
        .boost-plan a {
            color: #f72e5e;
        }
        .razorpay-payment-button{
          background: #f72e5e;
          border: 0px;
          text-align: center;
          margin: 10px 0 10px 0;
          width: 185px;
          display: block;
          color: #fff;
          line-height: 40px;
          font-size: 16px;
          padding: 6px 0 0 0;
        }
    </style>
    <div class="right-all">
        @if (Session::has('success'))
          <div class="alert alert-info alert-dismissible fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </a>
            <strong>Success!</strong> {{ Session::get('success') }}
          </div>
        @endif
        @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
            </a>
            <div>
              <strong>Error!</strong> {!! implode('', $errors->all('<span>:message</span>')) !!}
            </div>
          </div>
        @endif
        <div id="head-title" class="col-lg-12">
            <div class="dashboard_head">
                <h2>Boost Plan</h2>
            </div>
        </div>
        <div class="col-lg-12" id="extra-pad-n1">
            <div id="boost-plan" class="tab-content">
                <div class="setting-page" style="background:#fff;padding:10px 20px 20px 20px;margin-top:-5px">
               
            
              
                    <div class="plans-main">
                        <div class="u-clearfix u-expanded-width u-gutter-30 u-layout-wrap u-layout-wrap-2">
                            <div class="u-gutter-0 u-layout">
                                <div class="u-layout-row">
                                  @if( $my->is_premium > 0 )
                                    <div class="after-purchase-message" style="background:#28b47e; margin-bottom:20px;padding-bottom:20px;">
                                      <h1 class="" style="color:#fff;text-align:center; margin: 0; font-size: 26px;padding: 25px 0 0px 0;"> Your Premium Profile - {{ $my->premium_plan_type == 2 ? 'Gold' : 'Silver' }} Plan is activated. </h1>
                                      <p class="u-text" style="color:#fff;text-align:center;font-size: 16px;line-height: 21px;padding-bottom: 10px;"> Payment: {{ $my->activePremiumPlan ? to_amount($my->activePremiumPlan->amount) : "" }}/- <br> Payment made date: {{ \Carbon\Carbon::createFromTimestamp($my->premium_started_at)->format("d-m-Y") }} <br> Expire Date: {{ \Carbon\Carbon::createFromTimestamp($my->premium_ended_at)->format("d-m-Y") }} <br>
                                      </p>
                                      <a class="u-btn" style="background: #fff; border: 0px; text-align: center; margin: 10px auto 10px; width: 245px; display: block; color: #000; line-height: 40px; font-size: 16px; padding: 6px 0 0 0;"> Renew again after  {{ \Carbon\Carbon::createFromTimestamp($my->premium_ended_at)->format("d-m-Y") }}</a>
                                    </div>
                                    <Style>
                                        #after-purchase-no-display{display:none;}
                                        .plans-main {    margin: 20px auto 0 !important;}
                                    </style>
                                  @endif
                                  <div id="after-purchase-no-display">
                                <h4 style="text-align:center; margin-top:10px;margin-bottom:10px;  font-size:22px;">
                        To get premium profile select any any on this plan
                    </h4>       
                                  
                                  
                                    <div class="u-align-left u-container-style u-layout-cell u-left-cell u-palette-2-base u-radius-15 u-shape-round u-size-20 u-layout-cell-4">
                                        <div class="u-container-layout u-valign-top u-container-layout-4">
                                            <h4 class="u-text u-text-body-alt-color u-text-default u-text-5"> Basic Plan
                                            </h4>
                                            <h1 class="u-custom-font u-font-pt-sans u-text u-text-default u-text-6"> Free
                                            </h1>
                                            <a class="u-btn">
                                                Already Activated
                                            </a>
                                            <p class="u-text u-text-7" style="color:#fff"> Free for 3 years. <br>No Payment Required
                                            </p>
                                        </div>
                                    </div>
                                    <div class="u-align-left" style="background:#eaeaea">
                                        <div class="u-container-layout u-valign-top u-container-layout-5">
                                            <h4 class="u-text u-text-default u-text-palette-2-base u-text-8"> Silver Plan
                                            </h4>
                                            <h1
                                                class="u-custom-font u-font-pt-sans u-text u-text-default u-text-palette-2-base u-text-9">
                                                Rs:349/- <span style="display:block;font-size: 1.5rem;"> for 1 month</span>
                                            </h1>
                                            <form action="{{ route('payment') }}" method="POST">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                              <script src="https://checkout.razorpay.com/v1/checkout.js" data-key="{{ env('RAZOR_KEY') }}" data-amount="34900" data-buttontext="Proceed Monthly" data-name="Premium profile silver plan" data-description="Purchase theme plan" data-prefill.name="name" data-prefill.email="email" data-theme.color="#ff7529" data-notes.type='premium-profile' data-notes.setting='silver'></script>
                                            </form>
                                            <p class="u-text u-text-10"> Portfolio Profile highlited. <br>Number of views Increase
                                            </p>
                                        </div>
                                    </div>

                                    <div class="u-align-left" style="background:#eaeaea">
                                        <div class="u-container-layout u-valign-top u-container-layout-5">
                                            <h4 class="u-text u-text-default u-text-palette-2-base u-text-8"> Gold Plan</h4>
                                            <h1 class="u-custom-font u-font-pt-sans u-text u-text-default u-text-palette-2-base u-text-9">
                                                Rs:1499/- <span style="display:block;font-size: 1.5rem;">for 6 months
                                                </span>
                                            </h1>
                                            <form action="{{ route('payment') }}" method="POST">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                              <script src="https://checkout.razorpay.com/v1/checkout.js" data-key="{{ env('RAZOR_KEY') }}" data-amount="149900" data-buttontext="Proceed Monthly" data-name="Premium profile gold plan" data-description="Purchase theme plan" data-prefill.name="name" data-prefill.email="email" data-theme.color="#ff7529" data-notes.type='premium-profile' data-notes.setting='gold'></script>
                                            </form>
                                            <p class="u-text u-text-10"> Portfolio Profile highlited. <br>Number of views Increase
                                            </p>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                <p style="font-size:14px;line-height: 25px;margin-top: 25px;text-align:center">Boost your portfoilio to
                        get better result. You profile get highlighted to our main page and production house for better
                        result. </p>      
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
@endsection
