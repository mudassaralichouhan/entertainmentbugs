@extends('layouts.home')
@section('content')
    @include('dashboard/nav')
    
    <style>
        .promote-option .radio label,
        .promote-option .checkbox label {
            color: #fff;
        }



        .promoted-posts a {
            color: #f72e5e;
        }

        .promote-option .radio,
        .promote-option .checkbox {
            margin-bottom: 4px;
        }

        .promote-option .radio label,
        .promote-option .checkbox label {
            line-height: 30px;
        }

        .promote-option .checkbox,
        .promote-option .radio {
            display: block;
            min-height: 20px;
            padding-left: 1px;
        }

        .promote-option label.checkbox {
            float: left;
            margin: 0px 15px 0 0;
        }

        .promote-option {
            border-top: 1px solid rgb(236, 235, 230);
            padding-top: 18px;
            display: none;
            margin-top: 10px;
        }

        #individual-post .pagination a {
            padding: 6px 12px;
        }
    </style>



    <div class="right-all">
        <div id="head-title" class="col-lg-12">
            <div class="dashboard_head">
                <h2>Promoted Posts</h2>
                <p>Promoted posts List</p>
            </div>
        </div>
        <div class="col-lg-12" id="extra-pad-n1">
            <div id="promote-post" class="tab-content">
                <div id="individual-post" class="setting-page"
                    style="background:#fff;padding:10px 20px 20px 20px;margin-top:-5px">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 style="margin-top: 23px;font-size:16px;">All Post</h4>
                        </div>

                        <div class="col-lg-6">
                            <input type="text" class="form-control" placeholder="Search by: Post title" name="searchdata"
                                aria-describedby="sizing-addon2"
                                style="margin: 12px 0 8px  0;line-height: 33px;padding: 3px 14px 0 14px;">
                        </div>
                    </div>

                    <div class="row">
                        <div class="covers" style="position:relative;width:100%">
                            <div class="post-final" id="user-search" style="padding:0px">
                                @if ($recentPosts && $recentPosts->count() > 0)
                                    @php
                                        $currentTimeStamp = \Carbon\Carbon::now()->timestamp;
                                    @endphp
                                    @foreach ($recentPosts as $post)
                                        @php
                                            $url = '';
                                            $image = $post->image;
                                            $username = '';
                                            $type = 'video';
                                            $_idOrSlug = $post->slug;
                                        @endphp
                                        @if ($post->tag == 'photo')
                                            @php
                                                $images = unserialize($post->image);
                                                $image = "uploads/photo/$images[0]";
                                                $url = url("single-photo/$post->slug");
                                                $type = "photo";
                                                $_idOrSlug = $post->id;
                                            @endphp
                                        @elseif ($post->tag == 'story')
                                            @php
                                                $type = "story";
                                                $_idOrSlug = $post->slug;
                                                $image = preg_match('/^(http|https):\/\//', $post->image) ? $post->image : STORY_THUMB_DISPLAY_PATH . $post->image;
                                                $splitted = explode('/', $image);
                                                $len = count($splitted);
                                                if ($splitted[$len - 1] == $splitted[$len - 2]) {
                                                    array_pop($splitted);
                                                    $image = implode('/', $splitted);
                                                }
                                                
                                                if (!empty($post->image) && file_exists(preg_replace('/^(http|https):\/\/(.+)\/public\//', public_path('/'), $image))) {
                                                    $image = preg_replace('/^(http|https):\/\/(.+)\/public\//', '', $image);
                                                } else {
                                                    $image = 'img/story_thumb.jpg';
                                                }
                                                $url = to_story($post->slug);
                                            @endphp
                                        @else
                                            @php
                                                if (!empty($post->image)) {
                                                    $video = \App\Models\VideoThumb::select('thumb')
                                                        ->where('id', $post->image)
                                                        ->first();
                                                
                                                    if ($video) {
                                                        $image = str_replace(HTTP_PATH . '/public/', '', VIDEO_THUMB_DISPLAY_PATH) . $video->thumb;
                                                    } else {
                                                        $image = 'img/video1-1.png';
                                                    }
                                                } else {
                                                    $image = 'img/video1-1.png';
                                                }
                                                
                                                $url = url("watch/$post->slug");
                                            @endphp
                                        @endif
                                        <div class="user-search-individual">
                                            <div class="col-lg-10  col-xs-10" id="new-profile-details">
                                                <div class="channel-details noti">
                                                    <div class="row">
                                                        <a href="{{ $url }}" style="display:block">
                                                            <h5><strong>{{ $post->title }}</strong> </h5>
                                                        </a>
                                                        <p> your {{ $post->tag }} is uploaded successfully! <i class="fa fa fa-thumbs-up" aria-hidden="true"></i> -
                                                            {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}
                                                        </p>
                                                    </div>
                                                    <div class="thumbnail">
                                                        <a href="{{ $url }}">
                                                            <img src="{{ asset('public/' . $image) }}">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="promote-bt">
                                               
                                                @php $isPromoted = false; @endphp
                                                @if( $promoted = get_promoted_post($post->tag, $post->id) )
                                                
                                                    @php
                                                        $createdAt = \Carbon\Carbon::parse($promoted->created_at)->timestamp;
                                                    @endphp
                                                    @if( $promoted->status == 1 && ($createdAt + ($promoted->days * 86400)) > $currentTimeStamp )
                                                        @php $isPromoted = true; @endphp
                                                        <a href="{{ url("promoted-post/".$promoted->id) }}" class="promote-click">Promted</a>
                                                    @endif
                                                @endif
                                                @if( !$isPromoted )
                                                    <a href="{{ url("promote-post/$type/$_idOrSlug") }}" class="promote-click">Promote Now</a>
                                                @endif
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    @endforeach
                                    {{ $recentPosts->links() }}
                                @endif
                            </div>
                            <div style="clear:both; overflow:"></div>
                        </div>
                    </div>

                    <div class="setting-page" style="background:#fff;padding:20px;margin-top:10px">
                        <h4 style="text-align:center; margin-top:10px;  font-size:22px;">Promote Multiple post</h4>

                        <div class="plans-main">
                            <div class="u-clearfix u-expanded-width u-gutter-30 u-layout-wrap u-layout-wrap-2">
                                <div class="u-gutter-0 u-layout">
                                    <div class="u-layout-row">

                                        <div class="u-align-left" style="background:#eaeaea">
                                            <div class="u-container-layout u-valign-top u-container-layout-5">
                                                <h4 class="u-text u-text-default u-text-palette-2-base u-text-8"> Video
                                                    Plan</h4>
                                                <h1
                                                    class="u-custom-font u-font-pt-sans u-text u-text-default u-text-palette-2-base u-text-9">
                                                    Rs:139<span style="font-size: 1.5rem;">/30days</span>
                                                </h1>
                                                <p class="u-text u-text-10">
                                                    - First 3 videos will be boost
                                                    <br>- Number of views Increase
                                                    <br>- Always Recommended your video
                                                </p> <a href="#" class="second-plan">Proceed </a>
                                            </div>
                                        </div>

                                        <div class="u-align-left" style="background:#eaeaea">
                                            <div class="u-container-layout u-valign-top u-container-layout-5">
                                                <h4 class="u-text u-text-default u-text-palette-2-base u-text-8"> Photos
                                                    Plan</h4>
                                                <h1
                                                    class="u-custom-font u-font-pt-sans u-text u-text-default u-text-palette-2-base u-text-9">
                                                    Rs:499/-<span style="font-size: 1.5rem;">/30days</span>
                                                </h1>
                                                <p class="u-text u-text-10">
                                                    - First 2 Post will be boost
                                                    <br>- Number of views Increase
                                                    <br>- Always display first
                                                </p> <a href="#" class="second-plan">Proceed </a>
                                            </div>
                                        </div>

                                        <div class="u-align-left" style="background:#eaeaea">
                                            <div class="u-container-layout u-valign-top u-container-layout-5">
                                                <h4 class="u-text u-text-default u-text-palette-2-base u-text-8"> Stories
                                                    Plan</h4>
                                                <h1
                                                    class="u-custom-font u-font-pt-sans u-text u-text-default u-text-palette-2-base u-text-9">
                                                    Rs:299<span style="font-size: 1.5rem;">/90days</span>
                                                </h1>

                                                <p class="u-text u-text-10">
                                                    - First 2 videos will be boost
                                                    <br>- Number of views Increase
                                                    <br>- Recommended your stories
                                                </p> <a href="#" class="second-plan">Proceed </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p style="font-size:14px;line-height: 25px;margin-top: 0px;text-align:center">Boost your portfoilio
                            to get better result. You profile get highlighted to our main page and production house for
                            better result. </p>
                    </div>







                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>

    <script src="https://preview.keenthemes.com/good/assets/plugins/global/plugins.bundle.js"></script>

    {{-- <script>
        $(".result-show").click(function() {
            $(".my-chart").toggle(300);
        });
    </script>

    <script>
        $(".promote-click").click(function() {
            $(".promote-option").toggle(300);
        });
    </script> --}}




    <script>
        $("#kt_daterangepicker_2").daterangepicker({
            timePicker: true,
            startDate: moment().startOf("hour"),
            endDate: moment().startOf("hour").add(32, "hour"),
            locale: {
                format: "M/DD hh:mm A"
            }
        });
    </script>




    <script>
        //Transaction Graph 
        var ctx = document.getElementById("visitor-num");
        ctx.height = 200;
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    "1 Jan",
                    "2 Jan",
                    "3 Jan",
                    "4 Jan",
                    "5 Jan",
                    "6 Jan",
                    "7 Jan",
                    "8 Jan",
                    "9 Jan",
                    "10 Jan",
                    "11 Jan",
                    "12 Jan",
                    "13 Jan",
                    "14 Jan",
                    "15 Jan",
                    "16 Jan",
                    "17 Jan",
                    "18 Jan",
                    "19 Jan",
                    "20 Jan",
                    "21 Jan",
                    "22 Jan",
                    "23 Jan",
                    "24 Jan",
                    "25 Jan",
                    "26 Jan",
                    "27 Jan",
                    "28 Jan",
                    "29 Jan",
                    "30 Jan",

                ],
                datasets: [{
                    label: "Video",
                    backgroundColor: "rgba(32, 212, 137, 1)",
                    borderColor: "transparent",
                    data: [40, 105, 92, 155, 138, 205, 120, 92, 155, 138, 205, 120, 138, 205, 120, 138, 205,
                        120, 92, 155, 138, 138, 205, 120, 138, 205, 120, 92, 155, 138,
                    ],
                    borderWidth: 2,
                    borderRadius: Number.MAX_VALUE,
                    borderSkipped: false,
                }, ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                scales: {
                    xAxes: [{
                        stacked: false,
                        barPercentage: 0.8,
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            // fontColor: "#8a909d",
                        },
                    }, ],
                    yAxes: [{
                        stacked: false,
                        gridLines: {
                            display: false,
                            color: "#eee",
                        },
                        ticks: {
                            stepSize: 50,
                            // fontColor: "#8a909d",
                        },
                    }, ],
                },
                tooltips: {
                    mode: "index",
                    intersect: false,
                    titleFontColor: "#888",
                    bodyFontColor: "#555",
                    titleFontSize: 12,
                    bodyFontSize: 15,
                    backgroundColor: "rgba(256,256,256,0.95)",
                    displayColors: true,
                    xPadding: 10,
                    yPadding: 7,
                    borderColor: "rgba(220, 220, 220, 0.9)",
                    borderWidth: 2,
                    caretSize: 6,
                    caretPadding: 5,
                }
            }
        });
    </script>
    <script>
        $('#demo').daterangepicker({
            "startDate": "10/07/2022",
            "endDate": "10/13/2022"
        }, function(start, end, label) {
            console.log(
                "New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')"
            );
        });
    </script>
@endsection
