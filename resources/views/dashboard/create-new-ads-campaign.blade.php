@extends('layouts.home')
@section('content')
                    @php
					$photoplan = \DB::table('photo_price')->get();
                    $videoplan = \DB::table('video_price')->get();
                    $photopriceclick = \DB::table('photo_views_click')->first();
                    $videopriceclick = \DB::table('video_views_click')->first();
					@endphp
	@include('dashboard/nav')
	<style>
	

.imgplace img{width:100%  !important;}
.imgplace{width:100% !important;    height: 100% !important;}
#ads-form .imgplace {margin-bottom: 17px;}
#promote_page_image .adblock.img_ban31 .img{width:32.3%;}
.edit-page .u-details-wrap{margin-bottom:0px;    padding-bottom: 0;}

	
		#promote_page_image .img:hover {background: #f72e5e !important;color: #fff !important}

		#promote_page_image .img:hover i.fa-check {
			color: #f72e5e !Important;
		}

		#promote_page_image .img:hover i {
			color: #fff !important
		}

		#promote_page_image .img {
			position: relative
		}

		#promote_page_image .img:hover .circle-tick {
			display: block;
		}

		#promote_page_image .circle-tick {
			display: none;
			background: #fff !Important;
			color: #f72e5e;
			border-radius: 100px;
			width: 40px;
			height: 40px;
			position: absolute;
			right: 16px;
			top: 18px;
		}

		#promote_page_image .circle-tick i {
			color: #f72e5e !Important;
			margin: 8px 0 0 0 !important;
			font-size: 22px;
		}

		#promote_page_image .circle-tick i {
			color: #f72e5e !Important;
			margin: 8px 0 0 0 !important;
			font-size: 22px;
		}

		.create-new-ads-campaign a {
			color: #f72e5e;
		}

		.upload-page .u-area .u-btn,
		.u-area .u-btn.submit-btn,
		.u-area .u-btn.preview-btn {
			margin-top: 10px;
			background-color: #f72e5e;
			color: white;
			padding: 15px 65px;
			border: solid 1px #f72e5e;
			height: 48px;
			border-radius: 24px;
			font-weight: 500;
		}

		#promote_page_image {
			padding: 0px;
		}

		#promote_page_image .img {
			float: left;
			margin: 0 0.5% 10px 0.5%;
			width: 24%;
		}

		#promote_page_image .img i {
			display: block;
			text-align: center;
		}

		.clear {
			clear: both;
			overflow: hidden
		}

		.img#margin-left_img {
			margin-left: 25%
		}

		#promote_page_image .img cv {
			display: block;
			text-align: center;
			color: #f72e5e;
			font-size: 56px;
			margin: -35px 0 22px 0
		}

		.spacer2 {
			height: 100px;
			clear: both;
			overflow: hidden
		}

		.adblock .img {
			background-color: #fff;
			border-radius: 3px;
			width: 336px;
			height: 280px;
			color: #a1a1a1;
			text-align: center;
			padding-top: 120px;
		}

		#promote_page_image .img:hover {
			background: #f72e5e !important;
			color: #fff !important;
		}

		#promote_page_image .img {
			position: relative;
		}

		#promote_page_image .img {
			cursor: pointer;
		}

		#promote_vieo_title h3 {
			float: none;
		}

		#promote_vieo_title h3 {
			width: 100%;
			font-size: 18px;
			font-size: 17px;
			padding: 7px 0 3px 0;
		}

		.form-group {
			margin-bottom: 12px;
		}

		.edit-page .u-area {
			text-align: center;
			padding-top: 16px;
		}


		.price_block {
			margin: -3px 0 0 11px;
		}

		label.checkbox {
			float: left;
			margin: 0px 15px 0 0;
		}
		#multi-form-steps{
			position: relative;
			display: flex;
			padding: 10px 0;
			margin-bottom: 10px;
			justify-content: space-between
		}
		#multi-form-steps::after{
			content: "";
			position: absolute;
			width: calc(100% - 40px);
			height: 2px;
			background: #a1a1a1;
			top: 50%;
			left: 20px;
			z-index: 0;
			transform: translateY(-50%);
		}
		#multi-form-steps .step{
			background-color: #a1a1a1;
			color: white;
			flex: 0 0 40px;
			width: 40px;
			height: 40px;
			font-size: 20px;
			font-weight: 600;
			border-radius: 50%;
			display: flex;
			justify-content: center;
			align-items: center;
			position: relative;
			z-index: 1;
		}
		#multi-form-steps .step.active{
			background-color: #f72e5e
		}
		#ads-form .imgplace{
			width: 200px;
			height: 200px;
			background: #e0e1e2;
			display: flex;
			justify-content: center;
			align-items: center;
		}
		#ads-form .imgplace img{
			width: 100%;
			height: 100%;
			border: 1px solid #a1a1a1;
		}
	</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
{{ HTML::style('public/css/bootstrap-tagsinput.css')}}
{{ HTML::script('public/js/jquery.validate.js')}}
{{ HTML::script('public/js/bootstrap-tagsinput.js')}}
{{ HTML::script('public/js/bootstrap3-typeahead.js')}}
<div class="right-all">
	@if ($errors->any())
		<div class="alert alert-danger">
			{!! implode('', $errors->all('<div>:message</div>')) !!}
		</div>
	@endif
	<form method="POST" autocomplete="off" id="ads-form">
		<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		<div id="head-title" class="col-lg-12">
			<div class="dashboard_head">
				<h2>Create New Ads Campaign</h2>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="steps" id="multi-form-steps">
				<div class="step active" data-step="1">1</div>
				<div class="step" data-step="2">2</div>
				<div class="step" data-step="3">3</div>
				<div class="step" data-step="4">4</div>
			</div>
		</div>
		<div class="col-lg-12" id="extra-pad-n1">
			<div class="u-area ads-section-1 ads-msf" data-step="1" id="promote_page_image">
				<div class="adblock">
					<div class="img" style="width:49%" data-block="image">
						<div class="circle-tick"><i class="fa fa-check"></i></div>
						<i class="fa fa-image"></i>
						Banner Select
					</div>
					<div class="img" style="width:49%" data-block="video">
						<div class="circle-tick"><i class="fa fa-check"></i></div>
						<i class="cv cvicon-cv-upload-video"></i>
						Video Select
					</div>
				</div>
				<div class="clear"></div>
				<p style="color: #000; margin: 20px 0 10px  0;font-size: 16px; font-weight: normal;">Select anyone to proceed</p>
			</div>
			<div class="col-lg-12a upload-page">
				<div class="u-area ads-section-2 ads-msf" data-step="2" style="display:none" id="promote_page_image">
					<div class="adblock">
						<div class="video-block" style="display: none">
							<div class="img" data-template="5">
								<div class="circle-tick"><i class="fa fa-check"></i></div>
								<i class="cv cvicon-cv-upload-video"></i>
								Entertainment AdSense Video<br> Your video time frame is<br>
								<strong style="font-weight: normal;font-size: 15px;margin: 6px 0 0 0;display: block;">0.01sec - 0.5sec</strong>
									<br>
								<p style="padding: 6px 10px;font-size:12px;margin-top: 30px;background: #f72e5e;color: #fff;">This ads video will run on above the main video and have skip option </p>
						
							</div>
							<div class="img" data-template="10">
								<div class="circle-tick"><i class="fa fa-check"></i></div>
								<i class="cv cvicon-cv-upload-video"></i>
								Entertainment AdSense Video<br> Your video time frame is<br>
								<strong style="font-weight: normal;font-size: 15px;margin: 6px 0 0 0;display: block;">0.01sec - 0.10sec</strong>
							</div>
							<div class="img" data-template="30">
								<div class="circle-tick"><i class="fa fa-check"></i></div>
								<i class="cv cvicon-cv-upload-video"></i>
								Entertainment AdSense Video<br> Your video time frame is<br>
								<strong style="font-weight: normal;font-size: 15px;margin: 6px 0 0 0;display: block;">0.01sec - 30sec</strong>
								
							</div>
							<div class="img" data-template="60">
								<div class="circle-tick"><i class="fa fa-check"></i></div>
								<i class="cv cvicon-cv-upload-video"></i>
								Entertainment AdSense Video<br> Your video time frame is<br>
								<strong style="font-weight: normal;font-size: 15px;margin: 6px 0 0 0;display: block;">0.01sec - 1min</strong>
								
								<br>
								<p style="padding: 6px 10px;font-size:12px;margin-top: 30px;background: #f72e5e;color: #fff;">This ads video will run on above the main video and have skip option </p>
							</div>
							<div class="clear"></div>
						</div>
						<div class="image-block" style="display: none">
							<div class="adblock img_ban31">
								<div class="img" data-template="1080x1080">
									<div class="circle-tick"><i class="fa fa-check"></i></div>
									<i class="fa fa-image"></i>
									Entertainment AdSense Banner<br>Size: 1080 x 1080  
								</div>
								<div class="img" data-template="336x280">
									<div class="circle-tick"><i class="fa fa-check"></i></div>
									<i class="fa fa-image"></i> Entertainment AdSense Banner<br>Size: 336 x 280
								</div>
								<!--<div class="img" data-template="336x150">-->
								<!--	<div class="circle-tick"><i class="fa fa-check"></i></div>-->
								<!--	<i class="fa fa-image"></i>-->
								<!--	Entertainment AdSense Image<br>Size: 336 × 150-->
								<!--</div>-->
								<div class="img" data-template="728x90">
									<div class="circle-tick"><i class="fa fa-check"></i></div>
									<i class="fa fa-image"></i>
									Entertainment AdSense Banner<br>Size: 728 x 90
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="ads-section-3 ads-msf" data-step="3" style="d-none">
			<div class="upload-page edit-page video-block" style="display: none">
				<div class="container-fluid u-details-wrap">
					<div class="row">
						<div class="col-lg-12">
							<div class="u-details">
								<div class="row">
									<div class="ud-caption" id="promote_vieo_title">
										<h3>Entertainment AdSense Video </h3>
									</div>
									<!--video-->
									<div class="col-lg-2">
										<div class="imgplace" data-video-thumb-place>
											<img src="" alt="" style="display: none" />
											<i class="fa fa-picture-o" style="font-size: 32px;line-height:80px;"></i>
										</div>
									</div>
									<div class="col-lg-9">
										<div class="u-progress" style="display: none" id="video-progressbar">
											<div class="progress">
												<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
													<span class="sr-only">0% Complete</span>
												</div>
											</div>
										</div>
										<input type="file" id="video-file" style="display: none" accept="video/*" />
										<input type="file" id="browse-thumb-file" style="display: none" accept="image/*" />
										<div id="browse-thumb-content">
											<button type="button" style="background:#f72e5e;color:#fff;padding:0 10px" id="browse-video">Browse Video</button>
											<button type="button" data-croppie data-croppie-file="#browse-thumb-file" data-croppie-bind="file" data-croppie-input="#msf-thumb" data-croppie-output="[data-video-thumb-place]>img" style="background:#f72e5e;color:#fff;padding:0 10px" id="browse-thumbnail">Browse Thumbnail</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="u-form">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label for="e1">Video Title</label>
											<input type="text" name="video[title]" class="form-control" id="e1" placeholder="Rocket League Pro Championship Gameplay (36 characters remaining)">
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group">
											<label for="e1">Video Tags</label>
											<input type="text" name="video[tags]" class="form-control" id="video-tags" placeholder="Type any 3">
										</div>
									</div>
									<div class="col-lg-3">
										<div class="form-group">
											<label for="e1">Video Language</label>
											<input type="text" name="video[language]" class="form-control" id="video-language-select" placeholder="Type any 3">
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<label for="e4">Interested Category Select 5 </label>
											<select style="width:100%;padding: 8px 0 7px 13px;border: solid 1px #e0e1e2;" name="video[category][]" id="video-category" >
												<option> Film &amp; Animation </option>
												<option>Serials</option>
												<option>Cars &amp; Vehicles</option>
												<option>Music</option>
												<option>Sports</option>
												<option>Science &amp; Technology</option>
												<option>Entertainment</option>
												<option>Pets &amp; Animals</option>
												<option>Non-profits &amp; Activism</option>
												<option>Travel &amp; Events</option>
												<option>People &amp; Blogs </option>
												<option>Comedy</option>
												<option>Gaming</option>
												<option>News &amp; Politics</option>
												<option>Education</option>
											</select>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<label for="e3">Button name</label>
											<select style="width:100%;padding: 8px 0 7px 13px;border: solid 1px #e0e1e2;margin-bottom:10px" name="video[button]">
												<option> Select </option>
												<option>Shop Now</option>
												<option>Visit Now</option>
												<option>Book Now</option>
												<option>View More</option>
												<option>Apply Now</option>
												<option>Whats App</option>
												<!-- if yes then open a input box for number file-->
												<option>Others</option>
											</select>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<label for="e3">Website link</label>
											<input type="text" name="video[website]" class="form-control" id="e1" placeholder="your website link">
										</div>
									</div>
									<div class="col-lg-12">
										<div class="form-group">
											<label for="e2">Description (25 Words)</label>
											<textarea class="form-control" id="e2" rows="3" name="video[description]" placeholder="Description"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="upload-page edit-page image-block" style="display: none">
				<div class="container-fluid u-details-wrap">
					<div class="row">
						<div class="col-lg-12">
							<div class="alert alert-danger" id="img-errors" style="margin-bottom:1px"></div>
							<div class="u-details">
								<div class="row">
									<div class="ud-caption" id="promote_vieo_title">
										<h3>Entertainment AdSense Banner </h3>
									</div>
									<!--video-->
									<div class="col-lg-3">
										<div class="imgplace" data-image-place>
											<img src="" alt="" style="display: none" />
											<i class="fa fa-picture-o" style="font-size: 32px;line-height:80px;"></i>
										</div>
									</div>
									<div class="col-lg-9">
										<br>
										<div class="u-progress" id="image-progressbar" style="display: none">
											<div class="progress">
												<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
													<span class="sr-only" data-progress="text">0% Complete</span>
												</div>
											</div>
										</div>
										<div id="browse-thumb-content" data-image-browse-btn>
											<input type="file" accept="image/*" style="display: none;" id="msf-image-input" />
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="u-form">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label for="e1">Banner Title</label>
											<input type="text" name="image[title]" class="form-control" id="e1" placeholder="Type your ads title here">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<label for="e1">Banner Tags</label>
											<input type="text" class="form-control" placeholder="Type any 3" name="image[tags]" id="banner-tags" >
										</div>
									</div>
									<!--<div class="col-lg-3">-->
									<!--    <div class="form-group">-->
									<!--        <label for="e1">Language (Optional)</label>-->
									<!--        <input type="text" class="form-control"  placeholder="Type any 2" name="image[language]" id="banner-language-select">-->
									<!--    </div>-->
									<!--</div>-->
									<div class="col-lg-4">
										<div class="form-group">
											<label for="e4"> Interested Category Select 5 </label>
											<select id="banner-category" name="image[category][]" style="width:100%;padding: 8px 0 7px 13px;border: solid 1px #e0e1e2;" multiple>
												<option> Film &amp; Animation </option>
												<option>Serials</option>
												<option>Cars &amp; Vehicles</option>
												<option>Music</option>
												<option>Sports</option>
												<option>Science &amp; Technology</option>
												<option>Entertainment</option>
												<option>Pets &amp; Animals</option>
												<option>Non-profits &amp; Activism</option>
												<option>Travel &amp; Events</option>
												<option>People &amp; Blogs </option>
												<option>Comedy</option>
												<option>Gaming</option>
												<option>News &amp; Politics</option>
												<option>Education</option>
											</select>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<label for="e3">Button name</label>
											<select name="image[button]"
												style="width:100%;padding: 8px 0 7px 13px;border: solid 1px #e0e1e2;margin-bottom:10px">
												<option> Select </option>
												<option>Shop Now</option>
												<option>Visit Now</option>
												<option>Book Now</option>
												<option>View More</option>
												<option>Apply Now</option>
												<option>Whats App</option>
												<!-- if yes then open a input box for number file-->
												<option>Others</option>
											</select>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<label for="e3">Website link</label>
											<input type="text" name="image[website]" class="form-control" id="e1" placeholder="your website link">
										</div>
									</div>
									<div class="col-lg-12">
										<div class="form-group">
											<label for="e2">Description (25 Words)</label>
											<textarea class="form-control" name="image[description]" id="e2" rows="3" placeholder="Description"></textarea>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="clear:both; overflow:hidden"></div>
			</div>
			<input type="hidden" name="type" id="msf-type" />
			<input type="hidden" name="template" id="msf-template" />
			<input type="hidden" name="file" id="msf-file" />
			<input type="hidden" name="thumb" id="msf-thumb" />
			<input type="hidden" name="plan" id="plan" />
			<input type="hidden" name="custom_days" id="plan_days" />
			<input type="hidden" name="custom_amount" id="plan_amount" />
			<input type="hidden" name="country" id="country" />
			<input type="hidden" name="states" id="states" />
			<input type="hidden" name="selected_states" id="selected_states" />
			<div class="u-area mt-small" style="padding-left: 20px">
				<input type="button" value="Preview" class="btn btn-primary u-btn preview-btn" id="preview-btn" style="display: none;" />
				<input type="submit" value="Save & Proceed" class="btn btn-primary u-btn submit-btn" id="submit-btn" style="display: none;" />
			</div>
		</div>
	</form>
	<div class="containera ads-section-4 ads-msf" data-step="4" style="display: none;">
		<div class="col-lg-12">
			<div class="u-details">
				<div class="row">
					<div class="col-lg-12s ud-caption" id="promote_vieo_title">
						<h3 data-preview="title"></h3>
					</div>
					<div class="col-lg-5" style="padding-right:0px">
						<video data-preview="video" id="" width="100%" controls style="display: none">
							<source src="" type="video/mp4">
						</video>
						<div class="imgplace" data-preview="image" style="display: none">
							<img src="" />
						</div>
						<br>
						<div id="video_up_preview">
							<div class="col-lg-12">
								<div class="form-group">
									<label for="e2">About:</label>
									<span data-preview="description">
										
									</span>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="form-group">
									<label for="e1">Tags:</label>
									<span data-preview="tags"></span>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="form-group">
									<label for="e3">Language:</label>
									<span data-preview="language"></span>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="e3">Category:</label>
									<span data-preview="category"></span>
								</div>
							</div>
							<div class="col-lg-12">
								<div class="form-group">
									<label for="e3">State:</label>
									<span>kolkata, delhi</span>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-lg-7">
						<div class="small_box_plan" id="plans" style="width:100%">
							<div class="price_block">
								Choose plan on Cost per views - CPV and Cost per click - CPV
								<div id="imageplan" style="display:none;">
								@foreach($photoplan as $plan)
								<div class="radio text-black">
									<label style="color:black;">
										<label class="radio text-black" style="color:black;">
											<input class="imageplan" disabled onclick="check({{json_encode($plan)}})" type="radio" name="_plan" value="<?php echo $plan->id ?>">
											<span class="arrow"></span>
										</label> Rs: <?php echo $plan->price ?>/- ({{($plan->price/$photopriceclick->clicks_price)." - ".($plan->price/$photopriceclick->views_price)}}) {{$plan->days}} Day
									</label>
								</div>
							@endforeach
							<input type="hidden" id="photoss" disabled name="type" value="imageplan"/>
								</div>
								<div id="videoplan" style="display:none;">
								    @foreach($videoplan as $plan)
								<div class="radio text-black">
									<label style="color:black;">
										<label class="radio text-black" style="color:black;">
											<input class="videoplan" disabled onclick="check({{json_encode($plan)}})" type="radio" name="_plan" value="<?php echo $plan->id ?>">
											<span class="arrow"></span>
										</label> Rs: <?php echo $plan->price ?>/- ({{($plan->price/$videopriceclick->clicks_price)." - ".($plan->price/$videopriceclick->views_price)}}) {{$plan->days}} Day
									</label>
								</div>
							@endforeach 
								<input type="hidden" disabled id="videoss" name="type" value="videoplan"/>
								</div>
								
						
								<div style="display: flex">
									<div style="flex:0">
										<input type="number" id="amount" min="0" step="0.01" placeholder="Type Amount" style="height:46px;padding: 5px 20px 0 20px;line-height: 41px;width: 200px;" />
										<div id="amount-error"></div>
									</div>
									<div style="flex:1">
										<select id="custom_days" style="width: 80%;padding: 8px 0 7px 13px;border: solid 1px #e0e1e2;margin-bottom:10px;margin-left: 10px;">
											<option>No of days</option>
											<option value="3">3 days</option>
											<option value="5">5 days</option>
											<option value="7">7 days</option>
											<option value="15">15 days</option>
											<option value="24">24 days</option>
											<option value="30">30 days</option>
										</select>
										<div id="custom-days-error"></div>
									</div>
								</div>
								<h5 style="color:black;margin-bottom: 10px;margin-top: 2px;"> No of Views and clicks: <span id="custom-view-click"></span></h5>
								<div class="checkbox" style="margin-bottom:5px">
									<label class="">Choose Country </label>
									<select id='countries' style="display: block;width: 86%;margin-left: 19px;padding: 6px 15px 3px 15px;border: solid 1px #e0e1e2;">
										<option value="" selected disabled>Choose Country</option>
										@if( $countries && count($countries) > 0 )
											@foreach($countries as $country)
												<option value="{{ $country->id }}">{{ $country->name }}</option>
											@endforeach
										@endif
									</select>
									<div id="country-error"></div>
									<br>
								</div>


								<div class="radio" style="margin-bottom:10px">
									<label style="color:black;">
										<label class="radio">
											<input type="radio" name="_states" value="all">
											<span class="arrow"></span>
										</label> All States
									</label>
									<br>
								</div>
								<div class="radio" style="margin-bottom:10px">
									<label style="color:black;">
										<label class="radio">
											<input type="radio" name="_states" value="selected">
											<span class="arrow"></span>
										</label> Selected States
									</label>
									<select class="form-control js-example-tokenizer" name="_states[]" id="_states" placeholder="Choose any 5 States" style="display: block;width: 86%; margin-top:10px; padding: 8px 15px 5px 15px;border: solid 1px #e0e1e2;" multiple="multiple"></select>
									<div id="selected-states-error"></div>
								</div>
								<button type="button" id="save-campaign" class="btn btn-primary u-btn" href="company_promotion.html" style="margin-top: 60px;background-color: #f72e5e;color: white;padding: 15px 65px;border: solid 1px #f72e5e;height: 48px;border-radius: 24px;font-weight: 500;"> Save Campaign </a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	
	
	
	
	
	
	
	<!--multiple image design start-->
		<div class="container-fluid u-details-wrap">
					<div class="row">
						<div class="col-lg-12">
						 
							<div class="u-details">
								<div class="row">
									<div class="ud-caption" id="promote_vieo_title">
										<h3> Multiple Ads banner </h3>
									</div>
									<!--video--> 
								</div>
							</div>
						</div>
				
				
					<style>
.edit-page .u-area {text-align: center;padding-top: 12px;padding-bottom: 50px;}
#inputPhoto-1{font-size:9px;}
#select-p1 i {color: #637076;font-size: 66px;text-align: center;display: block;padding: 38px 0 0 0;}
.upload-page .u-area i {font-size: 90px;color: #637076;}
.upload-page .u-area .u-text1 {margin-top: 10px;margin-bottom: 9px;font-size: 14px;}
.browse-photo {width: 80px;height: 80px;float:left;margin-right:20px;background: #eceff0;margin-bottom: 5px;}
#select-p1 i {color: #637076; font-size: 25px;text-align: center;display: block;  padding: 28px 0 0 0;}
.mulptiple-option .nt-190 {width: 100%;padding: 3px 16px 0 16px;line-height: 39px;margin-bottom: 2px;display: block;height: 36px;}
.mulptiple-option select {height: 22px;font-size: 10px;}
.browse-btn {float: left;width: 72%;}
#mul-0943{background:#fff; padding:20px;margin-top: -22px;}
.mulptiple-option{width:48%;float:left; min-height: 99px;  margin:0 1% 0 0 ;    font-size: 13px;}
.photo_op321{padding:20px 20px 0 20px;background:#eaeaea; }
							</style>	
							
							
							<div class="col-lg-12">
							<div class="u-form" id="mul-0943">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label for="e1">Banner Title (20 Words)</label>
											<textarea class="form-control" name="image[description]" id="e2" rows="3" placeholder=""></textarea>
										</div>
									</div>
								    
								   
								   <div class="col-lg-12">
									
									
									<div class="form-group photo_op321"> 
										<div class="u-close mulptiple-option" id="select-p1" > 
												<div class="browse-photo">
												<i class="fa fa-picture-o" aria-hidden="true"></i> 
												<input type="hidden" name="Photos[1]" value="" id="imagesource-1">
										<span style="display:block;text-align:Center;font-size: 10px;margin: 6px 0 0 0;"> 800x800</span>
											</div>
											<div class="browse-btn">
											   <input type="text" name="image[title]" class="form-control nt-190"   placeholder="Banner  title here"> 
<input type="text" name="image[title]" class="form-control nt-190"   placeholder="Description / Price">
											Browse photo<input type="file" class="image" id="inputPhoto-1" data-key-index="1" accept="image/x-png,image/gif,image/jpeg" data-croppie-size='{"width": 540, "height": 540}' data-croppie-viewport='{"width": 540, "height": 540}' data-croppie-boundary='{"width": 540, "height": 540}' data-croppie data-croppie-file="#inputPhoto-1" data-croppie-input="#imagesource-1" data-croppie-output="#fileuploadedImage-1" data-croppie-progress="#upload-photo-progress-1" />
										</div>
										</div>
									 
										 
										<div class="u-close mulptiple-option" id="select-p1" > 
												<div class="browse-photo">
												<i class="fa fa-picture-o" aria-hidden="true"></i> 
												<input type="hidden" name="Photos[1]" value="" id="imagesource-1"><span style="display:block;text-align:Center;font-size: 10px;margin: 6px 0 0 0;"> 800x800</span>
											</div> 
											<div class="browse-btn">
<input type="text" name="image[title]" class="form-control nt-190"   placeholder="Banner  title here"> 
<input type="text" name="image[title]" class="form-control nt-190"   placeholder="Description / Price">
											Browse photo<input type="file" class="image" id="inputPhoto-1" data-key-index="1" accept="image/x-png,image/gif,image/jpeg" data-croppie-size='{"width": 540, "height": 540}' data-croppie-viewport='{"width": 540, "height": 540}' data-croppie-boundary='{"width": 540, "height": 540}' data-croppie data-croppie-file="#inputPhoto-1" data-croppie-input="#imagesource-1" data-croppie-output="#fileuploadedImage-1" data-croppie-progress="#upload-photo-progress-1" />
										</div>
										</div>
										
										
										
										
										
										
										
										
										<div class="u-close mulptiple-option" id="select-p1" > 
												<div class="browse-photo">
												<i class="fa fa-picture-o" aria-hidden="true"></i> 
												<input type="hidden" name="Photos[1]" value="" id="imagesource-1">
											</div>
											<div class="browse-btn">
											   <input type="text" name="image[title]" class="form-control nt-190"   placeholder="Banner Type your ads title here"> 
<input type="text" name="image[title]" class="form-control nt-190"   placeholder="Description / Price"> 
											Browse photo<input type="file" class="image" id="inputPhoto-1" data-key-index="1" accept="image/x-png,image/gif,image/jpeg" data-croppie-size='{"width": 540, "height": 540}' data-croppie-viewport='{"width": 540, "height": 540}' data-croppie-boundary='{"width": 540, "height": 540}' data-croppie data-croppie-file="#inputPhoto-1" data-croppie-input="#imagesource-1" data-croppie-output="#fileuploadedImage-1" data-croppie-progress="#upload-photo-progress-1" />
										</div>
										</div>
									 
										 
										<div class="u-close mulptiple-option" id="select-p1" > 
												<div class="browse-photo">
												<i class="fa fa-picture-o" aria-hidden="true"></i> 
												<input type="hidden" name="Photos[1]" value="" id="imagesource-1">
											</div> 
											<div class="browse-btn">
											   <input type="text" name="image[title]" class="form-control nt-190"   placeholder="Banner Type your ads title here"> 
<input type="text" name="image[title]" class="form-control nt-190"   placeholder="Description / Price">
											Browse photo<input type="file" class="image" id="inputPhoto-1" data-key-index="1" accept="image/x-png,image/gif,image/jpeg" data-croppie-size='{"width": 540, "height": 540}' data-croppie-viewport='{"width": 540, "height": 540}' data-croppie-boundary='{"width": 540, "height": 540}' data-croppie data-croppie-file="#inputPhoto-1" data-croppie-input="#imagesource-1" data-croppie-output="#fileuploadedImage-1" data-croppie-progress="#upload-photo-progress-1" />
										</div>
										</div>
										
										
										
										
										
										<div class="u-close mulptiple-option" id="select-p1" > 
												<div class="browse-photo">
												<i class="fa fa-picture-o" aria-hidden="true"></i> 
												<input type="hidden" name="Photos[1]" value="" id="imagesource-1">
											</div>
											<div class="browse-btn">
										<input type="text" name="image[title]" class="form-control nt-190"   placeholder="Banner title here"> 
<input type="text" name="image[title]" class="form-control nt-190"   placeholder="Description / Price">
											Browse photo<input type="file" class="image" id="inputPhoto-1" data-key-index="1" accept="image/x-png,image/gif,image/jpeg" data-croppie-size='{"width": 540, "height": 540}' data-croppie-viewport='{"width": 540, "height": 540}' data-croppie-boundary='{"width": 540, "height": 540}' data-croppie data-croppie-file="#inputPhoto-1" data-croppie-input="#imagesource-1" data-croppie-output="#fileuploadedImage-1" data-croppie-progress="#upload-photo-progress-1" />
										</div>
										</div>
									  
										
										
										
										
											<div style="clear:both; overflow:hidden"></div>
										
										
										
										
											</div>
									   
										
									
										<br>
									</div>
							 
								
								
									<div class="col-lg-4">
										<div class="form-group">
											<label for="e3">Button name</label>
											<select name="image[button]"
												style="width:100%;padding: 8px 0 7px 13px;border: solid 1px #e0e1e2;margin-bottom:10px">
												<option> Select </option>
												<option>Shop Now</option>
												<option>Visit Now</option>
												<option>Book Now</option>
												<option>View More</option>
												<option>Apply Now</option>
												<option>Whats App</option>
												<!-- if yes then open a input box for number file-->
												<option>Others</option>
											</select>
										</div>
									</div>
									<div class="col-lg-4">
										<div class="form-group">
											<label for="e3">Website link</label>
											<input type="text" name="image[website]" class="form-control" id="e1" placeholder="your website link">
										</div>
									</div>
								
								</div>
							</div>
						</div>
					</div>
				</div>
	<!--multiple image design end-->
	
	
	
	
	
	
	
	
	
	
	
</div>
	</div>
	</div>
	<br>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
	<script>
	  var price =null;
		$(function(){
			let section = 1;
			let block = "";
			let notBlock = "";
			let template = "";
			const sectionOne = $(".ads-section-1");
			const sectionTwo = $(".ads-section-2");
			const sectionThree = $(".ads-section-3");
			const sectionFour = $(".ads-section-4");
			const steps = $("#multi-form-steps");
			const submitBtn = $("#submit-btn");
			const previewBtn = $("#preview-btn");
			const saveCampaign = $("#save-campaign");
			const adsForm = $("#ads-form");
			let splitted = [];
			let is_paid = false, rzp, order;
			const key_id = "{{ env('RAZOR_KEY') }}";

			const options = {
				key: key_id,
				amount: parseInt(price),
				currency: "INR",
				name: 'Ads Campaign',
				description: "Ads Campaign in Entertainment Bugs",
				handler: function(response){
					adsForm.append(`<input type='hidden' name='razorpay_payment_id' value='${response.razorpay_payment_id}' />`);
					adsForm.append(`<input type='hidden' name='razorpay_order_id' value='${response.razorpay_order_id}' />`);
					adsForm.append(`<input type='hidden' name='razorpay_signature' value='${response.razorpay_signature}' />`);

					is_paid = true;
					submitBtn.click();
				},
				prefill: {
					name: "{{ Session::get('user_name') }}",
				},
				theme: {
					color: "#ff7529"
				},
				notes: {
					type: 'ads-campaign'
				}
			};

			async function createOrder(amount){
				order = await rzp.orders.create({
					amount: amount,
					currency: 'INR'
				});
			}


			async function updateOrder(amount){
				return await rzp.orders.edit(order.id, {
					amount: amount
				});
			}

			saveCampaign.on("click", function(){
				const country = $("#countries");
				const states = $("[name='_states']:checked");
				const selectedStates = $("#_states");
				const plan = $("[name='_plan']:checked").val();
                var plans =[];
				if( !country.val() ){
					$("#country-error").text("Country is required");
					$('html, body').animate({
						scrollTop: $("#country-error").offset().top - 80
					}, 300);
					return false;
				} else {
					$("#country").val(country.val());
					$("#country-error").text("");
				}

				if( states.val() == 'selected' )
				{
					if( !selectedStates.val() || (selectedStates.val() && selectedStates.val().length == 0) ){
						$("#selected-states-error").text("Selected states must be required, if all states is not selected.");
						$('html, body').animate({
							scrollTop: $("#selected-states-error").offset().top - 80
						}, 300);
						return false;
					} else {
						$("#selected_states").val(selectedStates.val().join());
						$("#selected-states-error").text("");
					}
				} else if(states.val() === ''){
					$("#selected-states-error").text("Either select all states or select top 5 states");
					$('html, body').animate({
						scrollTop: $("#selected-states-error").offset().top - 80
					}, 300);
					return false;
				} else {
					$("#selected-states-error").text("");
					$("#selected_states").val("");
				}

				$("#states").val(states.val());
				if( plan == 'custom' ){
					$("#plan").val(plan);
					if( !$("#amount").val() || parseInt($("#amount").val()) == 0 ){
						$("#amount-error").text("Amount must be greater than 0, if custom plan is selected");
						$('html, body').animate({
							scrollTop: $("#amount-error").offset().top - 80
						}, 300);
						return false;
					} else {
						$("#plan_amount").val($("#amount").val());
						$("#amount-error").text("");
					}

					const customDays = $("#custom_days").val();
					if( customDays && ![3,5,7,15,24,30].includes(parseInt(customDays)) ){
						$("#custom-days-error").text("Valid custom days must be required, if all states is selected");
						$('html, body').animate({
							scrollTop: $("#custom-days-error").offset().top - 80
						}, 300);
						return false;
					} else {
						$("#plan").val(plan);
						$("#plan_amount").val($("#amount").val());
						$("#plan_days").val(customDays);
						$("#amount-error").text("");
						$("#custom-days-error").text("");
					}
				} else {
					$("#plan").val(plan);
					$("#amount-error").text("");
					$("#custom-days-error").text("");
				}

				let amount = 0;
				if( plan == "custom" ){
					amount = $("#amount").val();
				} else if( typeof plans[plan - 1] !== 'undefined' ){
					amount = plans[plan - 1];
				}

				if( !amount ){
					amount = parseInt(price);
				}

				options.amount = amount * 100;
				rzp = new Razorpay(options);

				rzp.on('payment.failed', function (response){
					is_paid = false;
				});

				rzp.open();
				return false;
			});

			$("#_states").select2({
				multiple: true,
				tokenSeparators: [',', ' ']
			});

			$("#countries").on("change", function(){
				const id = $(this).find("option:selected").text();
				$("#_states").html('');
				$("#_states").val('');
				
				if( id ){
					$.ajax({
						url: BASE.url("get-states"),
						method: "POST",
						headers: {
							"X-CSRF-TOKEN": $("meta[name='secure_token']").attr("content")
						},
						data: { id: id },
						success: function(response){
							let states = $("#_states");
							if( response ){
								response = JSON.parse(response);
								
								if( Array.isArray(response) && response.length ){
									response.sort(function(a, b){
										if( a.name < b.name ) return -1;
										if( a.name > b.name ) return 1;
										return 0;
									}).forEach(state => {
										states.append(`<option value="${state.id}">${state.name}</option>`);
									});
								}
							}
						},
						error: function(){
							alert("Something went wrong");
						}
					})
				}
			});

			$("#_states").on("change", function(e){
				const select = $(this).get(0);
				const selectedOptions = select.selectedOptions;

				if (selectedOptions.length >= 3) {
					if( $(this).val() ){
						$(this).val($(this).val().slice(0, 3));
					}
					for (var i = 0; i < select.options.length; i++) {
						if (!select.options[i].selected) {
							select.options[i].disabled = true;
						}
					}
				} else {
					for (var i = 0; i < select.options.length; i++) {
						select.options[i].disabled = false;
					}
				}
			});

			$("#_states").trigger('change');
			$("#amount").on("keypress input", function(){
				const amount = $(this).val();

				if( amount ){
					let viewsAndClick = parseInt(1/0.25 * amount);
					$("#custom-view-click").text(viewsAndClick);
				}
				else {
					$("#custom-view-click").text('');
				}
			});

			$("#ads-form").on("submit", function(e){
				if( section != 4 ){
					e.preventDefault();
				}
			})

			$("#browse-video").on("click", function(){
				if( section == 3 && block == "video" ){
					$("#video-file").click();
				}
			})

			$("#video-file").on("change", function(){
				if( section == 3 && block == "video" ){
					var files = this.files;
					if( files && files[0] ){
						var imageType = /video.*/;
						var file = files[0];
						// check file type
						if (!file.type.match(imageType)) {  
							alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
							return false;	
						} 
						// check file size
						if (parseInt(file.size / 1024) > (1024*50)) {  
							alert("File \""+file.name+"\" is too big");
							return false;
						}
						processVideo(file);
					} else {
						console.log("file not selected");
					}
				} else {
					console.log("invalid block selected");
				}
			});

			async function processVideo(file){
				const fileContent = URL.createObjectURL(file);
				const video = await EBUGS.utils.getThumbnailForVideo(fileContent);
				
				if( video.duration <= parseInt(template) ){
					var formdata = new FormData();
					formdata.append("video", file);
					formdata.append("thumb", video.thumb);
					formdata.append("store", JSON.stringify({path: "public/ads/videos"}));
					$("#video-progressbar").show();
					
					const xhr = new XMLHttpRequest();
					xhr.open("POST", "upload/video");
					
					xhr.upload.addEventListener("progress", function (event) {
						if (event.lengthComputable) {
							$("#video-progressbar .progress-bar").css("width",(event.loaded / event.total) * 100 + "%");
							$("#video-progressbar .progress-bar").css("aria-valuenow",(event.loaded / event.total) * 100);
							$("#video-progressbar .sr-only").html(" "+((event.loaded / event.total) * 100).toFixed() + "%");
						}
						else {
							alert("Failed to compute file upload length");
						}
					}, false);
					xhr.onreadystatechange = function (oEvent) {  
						if (xhr.readyState === 4) {  
							if (xhr.status === 200) {
								$("#video-progressbar .progress-bar").css("width", "100%");
								$("#video-progressbar .progress-bar").css("aria-valuenow", "100");
								$("#video-progressbar .sr-only").html("100%");
							} else {
								alert("Error"+ xhr.statusText);  
							}  
						}
					}; 

					xhr.onload  = function() {
						var jsonResponse = xhr.response;
						result = JSON.parse(jsonResponse);
						if(result.status == true){
							$("#msf-file").val(result.file);
							$("#msf-thumb").val(result.thumbnail);
							$("[data-video-thumb-place] img").attr("src", result.thumbnail).show();
							$("[data-video-thumb-place] i").hide();
						}else{
							alert('File is not uploading. Please try again.')
						}	  
					};

					var csrfToken = "{{ csrf_token() }}";
					xhr.setRequestHeader('X-csrf-token', csrfToken); 	
					
					xhr.send(formdata);
				} else {
					alert("Your video duration is longer than selected template");
				}
			}
			
			$(".ads-section-1 .adblock>.img").on("click", toStepTwo);
			$(".ads-section-2 .adblock .img").on("click", toStepThree);
			previewBtn.on("click", toStepFour);

			function toStepTwo(){
				block = $(this).data('block');
				section = 2;
				notBlock = block == "image" ? "video" : "image";

				sectionOne.hide();
				sectionTwo.show();
				sectionTwo.find(`.${block}-block`).show();
				sectionTwo.find(`.${notBlock}-block`).hide();

				$("#msf-type").val(block);
				changeStep(section);
				previewBtn.hide();
			}

			function toStepThree(){
				section = 3;
				template = $(this).data("template");

				sectionTwo.hide();
				sectionThree.find(`.${block}-block`).show();
				sectionThree.find(`.${notBlock}-block`).hide();
				sectionThree.show();
				
				$("#msf-template").val(template);
				changeStep(section);
				previewBtn.show();

				if( block == "image" ){
				    $('#imageplan').show();
					$('#videoplan').hide();
					$(".imageplan").prop('disabled',false);
					$(".videoplan").prop('disabled',true);
					$("#videoss").attr('disabled',true);
				    $("#photoss").attr('disabled',false);
					browseImageBtn(template);
				}else{
				     $('#imageplan').hide();
				    $('#videoplan').show();
				    $(".imageplan").prop('disabled',true);
				    $(".videoplan").prop('disabled',false);
				}
			}

			function toStepFour(){
				section = 4;
				sectionThree.hide();
				sectionFour.show();
				
				let selectedBlock = sectionThree.find(`.${block}-block`);
			
                
				$("[data-preview='title']").text(selectedBlock.find(`[name='${block}[title]']`).val());
				$("[data-preview='description']").text(selectedBlock.find(`[name='${block}[description']`).val());
				$("[data-preview='tags']").text(selectedBlock.find(`[name='${block}[tags]']`).val());
				$("[data-preview='language']").text(selectedBlock.find(`[name='${block}[language]']`).val());
				$("[data-preview='category']").text(selectedBlock.find(`[name='${block}[category][]']`).val().join(", "));
				$(`[data-preview='${block}']`).show();
				$(`[data-preview='${block}']>*`).attr("src", BASE.url(`public/ads/${block}s/${$("#msf-file").val()}`));

				if( block == "video" ){
				   
				    $(".imageplan input:radio").attr('disabled',true);
				     $(".videoplan input:radio").attr('disabled',false);
				       $("#videoss").attr('disabled',false);
				        $("#photoss").attr('disabled',true);
					$(`[data-preview='${block}']`).attr('poster', $("[name='thumb']").val());
					$(`[data-preview='${block}']`).get(0).load();
				}

				changeStep(section);
				previewBtn.hide();
			}

			function changeStep(activeStep){
				steps.find(".step").removeClass("active");
				steps.find(".step[data-step='"+activeStep+"']").addClass("active");
			}

			function browseImageBtn(template){
				splitted = template.split("x");
				const btn = `<button type="button" style="background:#f72e5e;color:#fff;padding:0 10px" data-croppie data-croppie-progress="#image-progressbar" data-croppie-file="#msf-image-input" data-croppie-bind="file" data-croppie-store="public/ads/images" data-croppie-viewport='{"width": ${splitted[0]}, "height": ${splitted[1]}}' data-croppie-boundary='{"width": 1080, "height": 1080}' >Browse Image</button>`;
				
				if( $("[data-image-browse-btn] button").length ){
					$("[data-image-browse-btn] button").remove();
				}

				$("[data-image-browse-btn]").append(btn);
				EBUGS.croppie.load($("[data-image-browse-btn] button:last-child"), croppieCallback);
			}

			function croppieCallback(response, result){
				$("#msf-file").val(result.file);
				$("[data-image-place] img").attr("src", response).show();
				$("[data-image-place]").css({
					width: splitted[0]+"px",
					height: splitted[1]+"px"
				});
				$("[data-image-place] i").hide();
			}

			$("#banner-tags").tagsinput();
			$("#video-tags").tagsinput();
			
			$("#banner-language-select").tagsinput({
					typeahead: {
					source: ['Afar', 'Abkhaz', 'Avestan', 'Afrikaans', 'Akan', 'Amharic','Aragonese', 'Arabic', 'Assamese', 'Avaric', 'Aymara', 'Azerbaijani','South Azerbaijani', 'Bashkir', 'Belarusian', 'Bulgarian', 'Bihari', 'Bislama',
					'Bambara', 'Bengali', 'Tibetan Standard', 'Breton', 'Bosnian', 'Catalan; Valencian','Chechen', 'Chamorro', 'Corsican', 'Cree', 'Czech', 'Old Church Slavonic','Chuvash', 'Welsh', 'Danish', 'German', 'Maldivian', 'Dzongkha',
					'Ewe', 'Greek', 'English', 'Esperanto', 'Spanish', 'Estonian','Basque', 'Persian ', 'Fula', 'Finnish', 'Fijian', 'Faroese',' French', 'Western Frisian', 'Irish', 'Scottish Gaelic', 'Galician', 'Guaraní',
					'Gujarati', 'Manx', 'Hausa', 'Hebrew ', 'Hindi', 'Hiri Motu','Croatian', 'Haitian', 'Hungarian', 'Armenian', 'Herero', 'Interlingua',' Indonesian', 'Interlingue', 'Igbo', 'Nuosu', 'Inupiaq', 'Ido',
					'Icelandic', 'Italian', 'Inuktitut', 'Japanese', 'Javanese', 'Georgian','Kongo', 'Gikuyu', 'Kwanyama', 'Kazakh', 'Kalaallisut', 'Khmer','Kannada', 'Korean', 'Kanuri', 'Kashmiri', 'Kurdish', 'Komi',
					'Cornish', 'Kyrgyz', 'Latin', 'Luxembourgish', 'Ganda', 'Limburgish','Lingala', 'Lao', 'Lithuanian', 'Luba-Katanga', 'Latvian', 'Malagasy','Marshallese', 'Māori', 'Macedonian', 'Malayalam', 'Mongolian', 'Marathi',
					'Malay', 'Maltese', 'Burmese', 'Nauru', 'Norwegian Bokmål', 'North Ndebele','Nepali', 'Ndonga', 'Dutch', 'Norwegian Nynorsk', 'Norwegian', 'South Ndebele','Navajo', 'Chichewa', 'Occitan', 'Ojibwe', 'Oromo', 'Oriya',
					'Ossetian', 'Panjabi', 'Pāli', 'Polish', 'Pashto', 'Portuguese','Quechua', 'Romansh', 'Kirundi', 'Romanian', 'Russian', 'Kinyarwanda','Sanskrit', 'Sardinian', 'Sindhi', 'Northern Sami', 'Sango', 'Sinhala',
					'Slovak', 'Slovene', 'Samoan', 'Shona', 'Somali', 'Albanian','Serbian', 'Swati', 'Southern Sotho', 'Sundanese', 'Swedish', 'Swahili','Tamil', 'Telugu', 'Tajik', 'Thai', 'Tigrinya', 'Turkmen',
					'Tagalog', 'Tswana', 'Tonga', 'Turkish', 'Tsonga', 'Tatar','Twi', 'Tahitian', 'Uyghur', 'Ukrainian', 'Urdu	', 'Uzbek','Venda', 'Vietnamese', 'Volapük', 'Walloon', 'Wolof', 'Xhosa',
					'Yiddish', 'Yoruba', 'Zhuang', 'Chinese', 'Zulu']
				},
				freeInput: true
			});
			$("#video-language-select").tagsinput({
				typeahead: {
					source: ['Afar', 'Abkhaz', 'Avestan', 'Afrikaans', 'Akan', 'Amharic','Aragonese', 'Arabic', 'Assamese', 'Avaric', 'Aymara', 'Azerbaijani','South Azerbaijani', 'Bashkir', 'Belarusian', 'Bulgarian', 'Bihari', 'Bislama',
					'Bambara', 'Bengali', 'Tibetan Standard', 'Breton', 'Bosnian', 'Catalan; Valencian','Chechen', 'Chamorro', 'Corsican', 'Cree', 'Czech', 'Old Church Slavonic','Chuvash', 'Welsh', 'Danish', 'German', 'Maldivian', 'Dzongkha',
					'Ewe', 'Greek', 'English', 'Esperanto', 'Spanish', 'Estonian','Basque', 'Persian ', 'Fula', 'Finnish', 'Fijian', 'Faroese',' French', 'Western Frisian', 'Irish', 'Scottish Gaelic', 'Galician', 'Guaraní',
					'Gujarati', 'Manx', 'Hausa', 'Hebrew ', 'Hindi', 'Hiri Motu','Croatian', 'Haitian', 'Hungarian', 'Armenian', 'Herero', 'Interlingua',' Indonesian', 'Interlingue', 'Igbo', 'Nuosu', 'Inupiaq', 'Ido',
					'Icelandic', 'Italian', 'Inuktitut', 'Japanese', 'Javanese', 'Georgian','Kongo', 'Gikuyu', 'Kwanyama', 'Kazakh', 'Kalaallisut', 'Khmer','Kannada', 'Korean', 'Kanuri', 'Kashmiri', 'Kurdish', 'Komi',
					'Cornish', 'Kyrgyz', 'Latin', 'Luxembourgish', 'Ganda', 'Limburgish','Lingala', 'Lao', 'Lithuanian', 'Luba-Katanga', 'Latvian', 'Malagasy','Marshallese', 'Māori', 'Macedonian', 'Malayalam', 'Mongolian', 'Marathi',
					'Malay', 'Maltese', 'Burmese', 'Nauru', 'Norwegian Bokmål', 'North Ndebele','Nepali', 'Ndonga', 'Dutch', 'Norwegian Nynorsk', 'Norwegian', 'South Ndebele','Navajo', 'Chichewa', 'Occitan', 'Ojibwe', 'Oromo', 'Oriya',
					'Ossetian', 'Panjabi', 'Pāli', 'Polish', 'Pashto', 'Portuguese','Quechua', 'Romansh', 'Kirundi', 'Romanian', 'Russian', 'Kinyarwanda','Sanskrit', 'Sardinian', 'Sindhi', 'Northern Sami', 'Sango', 'Sinhala',
					'Slovak', 'Slovene', 'Samoan', 'Shona', 'Somali', 'Albanian','Serbian', 'Swati', 'Southern Sotho', 'Sundanese', 'Swedish', 'Swahili','Tamil', 'Telugu', 'Tajik', 'Thai', 'Tigrinya', 'Turkmen',
					'Tagalog', 'Tswana', 'Tonga', 'Turkish', 'Tsonga', 'Tatar','Twi', 'Tahitian', 'Uyghur', 'Ukrainian', 'Urdu	', 'Uzbek','Venda', 'Vietnamese', 'Volapük', 'Walloon', 'Wolof', 'Xhosa',
					'Yiddish', 'Yoruba', 'Zhuang', 'Chinese', 'Zulu'
					]
				},
				freeInput: true
			});

			$("#banner-category").select2({
				multiple: true
			});
			
			$("#video-category").select2({
				multiple: true
			});
		});
	</script>
	

<Style>
 
#croppie-modal-1 .cr-viewport.cr-vp-square{width:800px !Important;height:800px !Important;}
#croppie-modal-1 .cr-boundary{width:800px !Important;height:800px !Important;}
@media  only screen and (max-width: 767px){
#croppie-modal-1 .cr-viewport.cr-vp-square{width:300px !Important;height:300px !Important;}
#croppie-modal-1 .cr-boundary{width:300px !Important;height:300px !Important;}
 } 
		
		
</Style>	
	
	
	
	
	
	
	
@endsection
