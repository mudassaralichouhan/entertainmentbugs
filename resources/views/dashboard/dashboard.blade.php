@extends('layouts.home')

@section('style')
    <style>
        .thumbnail > img, .thumbnail a > img {
            margin-left: auto;
            margin-right: auto;
            height: 100%;
            width: 100%;
            object-fit: cover;
        }

        .thumbnail {
            top: 8px;
            height: 43px;
            object-fit: cover;
        }

        .dashboard-menu a {
            color: #f72e5e;
        }

        .jqvmap-label,
        .jqvmap-pin {
            pointer-events: none;
            z-index: 9999;
        }

        .jqvmap-label {
            position: absolute;
            display: none;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            background: #292929;
            color: #fff;
            font-family: sans-serif, Verdana;
            font-size: smaller;
            padding: 3px;
            z-index: 999;
        }

        .jqvmap-zoomin,
        .jqvmap-zoomout {
            position: absolute;
            left: 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            background: #000;
            padding: 3px;
            color: #fff;
            width: 10px;
            height: 10px;
            cursor: pointer;
            line-height: 10px;
            text-align: center;
        }

        .jqvmap-zoomin {
            top: 10px;
        }

        .jqvmap-zoomout {
            top: 30px;
        }

        .jqvmap-region {
            cursor: pointer;
        }

        .jqvmap-ajax_response {
            width: 100%;
            height: 500px;
        }

        .dashboard_head_top .col-sm-3 {
            padding: 0 10px;
        }

        .dashboard_head_top {
            margin: 0 10px;
        }

        .card-header:first-child {
            padding: 12px 20px 1px 20px;
        }
    </style>
@endsection

@section('content')
    @include('dashboard/nav')
    <div class="right-all">
        <div class="col-lg-12s">
            <div class="dashboard_all">
                <div id="head-title" class="col-lg-12">
                    <div class="dashboard_head">
                        <h2>Dashboard</h2>
                        <p> Welcome to your dashboard</p>
                    </div>
                </div>

                <div class="dashboard_head_top padding-right-my-0">
                    <div class="col-lg-3 col-sm-3">
                        <div class="card widget-stat ">
                            <div class="card-body p-4">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <p class="fs-18 mb-2 wspace-no">Total Earnings</p>
                                        <h1 class="fs-36 font-w600 text-black mb-0">{{ $totalEarning }}</h1>
                                    </div>
                                    <span class="ml-3 bg-primary text-white">
                                        <i class="fa fa-buysellads" aria-hidden="true"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-3">
                        <div class="card widget-stat">
                            <div class="card-body p-4">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <p class="fs-18 mb-2 wspace-no"> 30 days Earnings</p>
                                        <h1 class="fs-36 font-w600 d-flexs align-items-center text-black mb-0">
                                            {{ $lastThirtyDaysEarning }}</h1>
                                    </div>
                                    <span class="ml-3 bg-warning text-white" style="background:#ff6d4d !important">
                                        <i class="fa fa-user"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-3">
                        <div class="card widget-stat">
                            <div class="card-body p-4" style="background:#28b47e">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <p class="fs-18 mb-2 wspace-no">3 weeks Visitors</p>
                                        <h1 class="fs-36 font-w600 text-black mb-0">{{ $lastThreeWeekVisitors }}</h1>
                                    </div>
                                    <span class="ml-3 bg-primary text-white">
                                        <i class="fa fa-usd"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-3">
                        <div class="card widget-stat">
                            <div class="card-body p-4" style="background:#28b47e">
                                <div class="media align-items-center">
                                    <div class="media-body">
                                        <p class="fs-18 mb-2 wspace-no">6 months Visitors</p>
                                        <h1 class="fs-36 font-w600 text-black mb-0">{{ $lastSixMonthVisitors }}</h1>
                                    </div>
                                    <span class="ml-3 bg-primary text-white">
                                        <i class="fa fa-usd"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="canvas-main-dashboard" class="my-chart padding-right-my-0">
                    <h2 style="margin-top:-4px;"><span> Total Views : {{ $lastThirtyDaysGraph['total'] }}</span> Last 30
                        days total result <br>
                        <small style="font-size:12px;margin-top:-1px;display:block">(Video and photo)</small>
                    </h2>
                    <canvas id="visitor-num"></canvas>
                </div>

                <div id="recent-block" class="recent-post-block padding-right-my-0">
                    <div class="cover" style="position:relative">
                        <a href="{{ url("promote-recent-post") }}" style="position: absolute;right: 19px;top: 15px;">View
                            all</a>
                        <div class="post-final" id="user-search">
                            <h3 style="font-size:14px; margin:8px 0 0 0">Recent Posts</h3>
                            @if ( isset($recentPosts) && count($recentPosts) > 0 )
                                @foreach( $recentPosts as $key => $post )
                                    @php
                                        $url = "";
                                        $image = $post->image;
                                        $username = "";
                                    @endphp
                                    @if ($post->tag == 'photo')
                                        @php
                                            $images = unserialize($post->image);
                                            $image = "uploads/photo/$images[0]";
                                            $url = url("single-photo/$post->slug");
                                        @endphp
                                    @elseif ( $post->tag == "story" )
                                        @php
                                            $image = preg_match("/^(http|https):\/\//", $post->image)
                                                        ? $post->image
                                                        : STORY_THUMB_DISPLAY_PATH.$post->image;
                                            $splitted = explode("/", $image);
                                            $len = count($splitted);
                                            if( $splitted[$len - 1] == $splitted[$len - 2] ){
                                            array_pop($splitted);
                                            $image = implode("/", $splitted);
                                            }

                                            if(!empty($post->image) && file_exists(preg_replace("/^(http|https):\/\/(.+)\/public\//", public_path("/"), $image))) {
                                            $image = preg_replace("/^(http|https):\/\/(.+)\/public\//", "", $image);
                                            } else {
                                            $image = "img/story_thumb.jpg";
                                            }
                                            $url = to_story($post->slug);
                                        @endphp
                                    @else
                                        @php
                                            if( !empty($post->image) ){
                                                $video = \App\Models\VideoThumb::select("thumb")->where("id", $post->image)->first();

                                                if( $video ){
                                                $image = str_replace(HTTP_PATH."/public/", "", VIDEO_THUMB_DISPLAY_PATH).$video->thumb;
                                                } else {
                                                $image = "img/video1-1.png";
                                                }
                                            } else {
                                                $image = "img/video1-1.png";
                                            }

                                            $url = url("watch/$post->slug");
                                        @endphp
                                    @endif

                                    <div class="user-search-individual">
                                        <div class="col-lg-10  col-xs-10" id="new-profile-details">
                                            <div class="channel-details noti">
                                                <div class="row">
                                                    <a href="{{ $url }}" style="display:block">
                                                        <h5><strong> {{ $post->title }} </strong></h5>
                                                    </a>
                                                    <p> your {{$post->tag}} is uploaded successfully! <i
                                                                class="fa fa fa-thumbs-up" aria-hidden="true"></i>
                                                        - {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}
                                                    </p>
                                                </div>
                                                <div class="thumbnail">
                                                    <a href="{{ $url }}">
                                                        <img src="{{ asset("public/".$image) }}"></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="cover-right">
                        <div class="right-b">
                            <div class="card widget-stat ">
                                <div class="card-body p-4">
                                    <div class="media align-items-center">
                                        <div class="media-body">
                                            <p class="fs-18 mb-2 wspace-no">Total Videos Post</p>
                                            <h1 class="fs-36 font-w600 text-black mb-0">{{ $totalVideos }}</h1>
                                        </div>
                                        <span class="ml-3 bg-primary text-white">
                                            <i class="fa fa-buysellads" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-b">
                            <div class="card widget-stat">
                                <div class="card-body p-4">
                                    <div class="media align-items-center">
                                        <div class="media-body">
                                            <p class="fs-18 mb-2 wspace-no">Total Photos Post</p>
                                            <h1 class="fs-36 font-w600 d-flexs align-items-center text-black mb-0">
                                                {{ $totalPhotos }}</h1>
                                        </div>
                                        <span class="ml-3 bg-warning text-white" style="background:#ff6d4d !important">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-b">
                            <div class="card widget-stat">
                                <div class="card-body p-4">
                                    <div class="media align-items-center">
                                        <div class="media-body">
                                            <p class="fs-18 mb-2 wspace-no">Total Stories</p>
                                            <h1 class="fs-36 font-w600 d-flexs align-items-center text-black mb-0">
                                                {{ $totalStories }}</h1>
                                        </div>
                                        <span class="ml-3 bg-warning text-white" style="background:#ff6d4d !important">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="rewards-block" class="padding-right-my-0">
                    <h3>Rewards Video Participate</h3>
                    <div class="rewards-block-text">
                        <div class="right-b">
                            <div class="card widget-stat">
                                <div class="card-body p-4">
                                    <div class="media align-items-center">
                                        <div class="media-body">
                                            <p class="fs-18 mb-2 wspace-no">Reward Participate Video</p>
                                            <h1 class="fs-36 font-w600 d-flexs align-items-center text-black mb-0">
                                                {{ $totalRewardVideos }}</h1>
                                        </div>
                                        <span class="ml-3 bg-warning text-white" style="background:#ff6d4d !important">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="right-b">
                            <div class="card widget-stat">
                                <div class="card-body p-4">
                                    <div class="media align-items-center">
                                        <div class="media-body">
                                            <p class="fs-18 mb-2 wspace-no">Last video total hours</p>
                                            <h1 class="fs-36 font-w600 d-flexs align-items-center text-black mb-0">
                                                {{ $lastRewardVideoTotalHours }} <span
                                                        style="font-size: 12px;color: #f72e5e;position: relative;top: -5px;">Minutes</span>
                                            </h1>
                                        </div>
                                        <span class="ml-3 bg-warning text-white" style="background:#ff6d4d !important">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-b">
                            <div class="card widget-stat">
                                <div class="card-body p-4">
                                    <div class="media align-items-center">
                                        <div class="media-body">
                                            <p class="fs-18 mb-2 wspace-no">Total Reward Earning</p>
                                            <h1 class="fs-36 font-w600 d-flexs align-items-center text-black mb-0"> <span
                                                        style="font-size:13px;color: #f72e5e;position: relative;top: -5px;font-weight: 400;">Rs:</span>
                                                {{ $totalRewardVideosEarning }}/- </h1>
                                        </div>
                                        <span class="ml-3 bg-warning text-white" style="background:#ff6d4d !important">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body-graph">
                        <h2> Last 7 days total plays - 23451 times</h2>
                        <canvas id="transaction-graph"></canvas>
                    </div>
                </div>
                <div id="stories-block" class="padding-right-my-0">
                    <h3> Video Post</h3>
                    <div class="card-body-graph">
                        <h2> Last 30 days stories views - 0 Views</h2>
                        <canvas id="transaction-stories"></canvas>
                    </div>
                    <div class="rewards-block-text" style="margin:0 0 0 1%">
                        <div class="right-b">
                            <div class="card widget-stat">
                                <div class="card-body p-4">
                                    <div class="media align-items-center">
                                        <div class="media-body">
                                            <p class="fs-18 mb-2 wspace-no">Total Video</p>
                                            <h1 class="fs-36 font-w600 d-flexs align-items-center text-black mb-0">
                                                {{ $totalVideos }} <span
                                                        style="font-size: 12px;color: #f72e5e;position: relative;top: -5px;font-weight: 400;">Posts</span>
                                            </h1>
                                        </div>
                                        <span class="ml-3 bg-warning text-white" style="background:#ff6d4d !important">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="right-b">
                            <div class="card widget-stat">
                                <div class="card-body p-4">
                                    <div class="media align-items-center">
                                        <div class="media-body">
                                            <p class="fs-18 mb-2 wspace-no">Total Audience</p>
                                            <h1 class="fs-36 font-w600 d-flexs align-items-center text-black mb-0"> {{ array_sum($lastThirtyDaysGraph['stories']['data']) }}
                                                <span
                                                        style="font-size:13px;color: #f72e5e;position: relative;top: -5px;font-weight: 400;">Views</span>
                                            </h1>
                                        </div>
                                        <span class="ml-3 bg-warning text-white" style="background:#ff6d4d !important">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="right-b">
                            <div class="card widget-stat" style="background:#f72e5e">
                                <div class="card-body p-4">
                                    <div class="media align-items-center">
                                        <div class="media-body">
                                            <p class="fs-18 mb-2 wspace-no" style="color:#fff">Total Earning</p>
                                            <h1 style="color:#fff"><span
                                                        style="font-size: 12px;color: #fff;position: relative;top: -5px;">Rs:</span>
                                                {{ $videosTotalEarning }}/- </h1>
                                        </div>
                                        <span class="ml-3 bg-warning text-white" style="background:#ff6d4d !important">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear padding-right-my-0">
                    <div id="ads-main" class="padding-right-my-0">
                        <h2>Ads Title: Colgate Advertise</h2>
                    </div>
                    <div id="canvas-main-dashboard" class="my-chart padding-right-my-0">
                        <h2><span> Total Views : 1500 <br>Total Click: 1000</span>Your Plan is Rs: 4500/- 30 day</h2>
                        <canvas id="advertise-count"></canvas>
                    </div>
                </div>
                <div id="canvas-two-block">
                    <div class="left-b09">
                        <div class="card" style="padding-bottom:10px">
                            <div class="card-header">
                                <canvas id="pie-chart" width="460" height="340"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="right-b09">
                        <div class="card" style="padding-bottom:10px">
                            <div class="card-header">
                                <canvas id="bar-chart" width="460" height="340"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script>
        //Transaction Graph
        var ctx = document.getElementById("advertise-count");
        ctx.height = 200;
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    "1 Jan",
                    "2 Jan",
                    "3 Jan",
                    "4 Jan",
                    "5 Jan",
                    "6 Jan",
                    "7 Jan",
                    "8 Jan",
                    "9 Jan",
                    "10 Jan",
                    "11 Jan",
                    "12 Jan",
                    "13 Jan",
                    "14 Jan",
                    "15 Jan",
                    "16 Jan",
                    "17 Jan",
                    "18 Jan",
                    "19 Jan",
                    "20 Jan",
                    "21 Jan",
                    "22 Jan",
                    "23 Jan",
                    "24 Jan",
                    "25 Jan",
                    "26 Jan",
                    "27 Jan",
                    "28 Jan",
                    "29 Jan",
                    "30 Jan",

                ],
                datasets: [{
                    label: "Views",
                    backgroundColor: "rgba(32, 212, 137, 1)",
                    borderColor: "transparent",
                    data: [40, 105, 92, 155, 138, 205, 120, 92, 155, 138, 205, 120, 138, 205, 120, 138, 205,
                        120, 92, 155, 138, 138, 205, 120, 138, 205, 120, 92, 155, 138,
                    ],
                    borderWidth: 2,
                    borderRadius: Number.MAX_VALUE,
                    borderSkipped: false,
                },

                    {
                        label: "Click",
                        backgroundColor: "rgba(32, 212, 137, 0.5)",
                        borderColor: "transparent",
                        data: [0, 12, 9, 4, 2, 3, 7, 92, 5, 18, 25, 10, 13, 20, 10, 18, 5, 12, 2, 15, 18, 18,
                            25, 12, 18, 20, 12, 9, 15, 18
                        ],
                        borderWidth: 2,
                        borderRadius: Number.MAX_VALUE,
                        borderSkipped: false,
                    },

                ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                scales: {
                    xAxes: [{
                        stacked: false,
                        barPercentage: 0.8,
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            // fontColor: "#8a909d",
                        },
                    },],
                    yAxes: [{
                        stacked: false,
                        gridLines: {
                            display: false,
                            color: "#eee",
                        },
                        ticks: {
                            stepSize: 50,
                            // fontColor: "#8a909d",
                        },
                    },],
                },
                tooltips: {
                    mode: "index",
                    intersect: false,
                    titleFontColor: "#888",
                    bodyFontColor: "#555",
                    titleFontSize: 12,
                    bodyFontSize: 15,
                    backgroundColor: "rgba(256,256,256,0.95)",
                    displayColors: true,
                    xPadding: 10,
                    yPadding: 7,
                    borderColor: "rgba(220, 220, 220, 0.9)",
                    borderWidth: 2,
                    caretSize: 6,
                    caretPadding: 5,
                }
            }
        });
    </script>
    <script>
        //Transaction Graph
        var ctx = document.getElementById("transaction-graph");
        ctx.height = 300;
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: {!! json_encode($lastSevenDaysRewardVideoGraph['date']) !!},
                datasets: [
                    {!! json_encode($lastSevenDaysRewardVideoGraph['rewardVideos']) !!}
                ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                elements: {
                    point: {
                        radius: 0
                    }
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        barPercentage: 0.45,
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            // fontColor: "#8a909d",
                        },
                    },],
                    yAxes: [{
                        stacked: true,
                        gridLines: {
                            display: false,
                            color: "#eee",
                        },
                        ticks: {
                            stepSize: 50,
                            // fontColor: "#8a909d",
                        },
                    },],
                },
                tooltips: {
                    mode: "index",
                    intersect: false,
                    titleFontColor: "#888",
                    bodyFontColor: "#555",
                    titleFontSize: 12,
                    bodyFontSize: 15,
                    backgroundColor: "rgba(256,256,256,0.95)",
                    displayColors: true,
                    xPadding: 10,
                    yPadding: 7,
                    borderColor: "rgba(220, 220, 220, 0.9)",
                    borderWidth: 2,
                    caretSize: 6,
                    caretPadding: 5,
                }
            }
        });
    </script>
    <script>
        //Transaction Graph
        var ctx = document.getElementById("transaction-stories");
        ctx.height = 300;
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: {!! json_encode($lastThirtyDaysGraph['date']) !!},
                datasets: [
                    {!! json_encode($lastThirtyDaysGraph['stories']) !!}
                ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                elements: {
                    point: {
                        radius: 0
                    }
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                        barPercentage: 0.45,
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            // fontColor: "#8a909d",
                        },
                    },],
                    yAxes: [{
                        stacked: true,
                        gridLines: {
                            display: false,
                            color: "#eee",
                        },
                        ticks: {
                            stepSize: 50,
                            // fontColor: "#8a909d",
                        },
                    },],
                },
                tooltips: {
                    mode: "index",
                    intersect: false,
                    titleFontColor: "#888",
                    bodyFontColor: "#555",
                    titleFontSize: 12,
                    bodyFontSize: 15,
                    backgroundColor: "rgba(256,256,256,0.95)",
                    displayColors: true,
                    xPadding: 10,
                    yPadding: 7,
                    borderColor: "rgba(220, 220, 220, 0.9)",
                    borderWidth: 2,
                    caretSize: 6,
                    caretPadding: 5,
                }
            }
        });
    </script>
    <script>
        new Chart(document.getElementById("bar-chart"), {
            type: 'bar',
            data: {
                labels: ["Kerala", "Assam", "Goa", "Gujrat", "Delhi", "Tripura", "Assam", "Goa", "Gujrat", "Total"],
                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: ["#3e95cd", "#8e5ea2", "#000", "red", "blue", "#3e95cd", "#8e5ea2",
                        "#3cba9f", "#e8c3b9", "#c45850"
                    ],
                    data: [50, 30, 10, 20, 10, 30, 10, 20, 50, 160]
                }]
            },
            options: {
                legend: {
                    display: false
                },
                title: {
                    display: true,
                    text: 'Current ads views as per locations'
                }
            }
        });
    </script>
    <script>
        new Chart(document.getElementById("pie-chart"), {
            type: 'pie',
            data: {
                labels: ["Spend: 150", "Total: 400"],
                datasets: [{
                    label: "Population (millions)",
                    backgroundColor: ["#3e95cd", "#8e5ea2"],
                    data: [150, 400]
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'Total Spent moneny on your active ads'
                }
            }
        });
    </script>
    <script>
        //Transaction Graph
        var ctx = document.getElementById("visitor-num");
        ctx.height = 200;
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: {!! json_encode($lastThirtyDaysGraph['date']) !!},
                datasets: [
                    {!! json_encode($lastThirtyDaysGraph['videos']) !!},
                    {!! json_encode($lastThirtyDaysGraph['photos']) !!},
                    {!! json_encode($lastThirtyDaysGraph['stories']) !!}
                ],
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                },
                scales: {
                    xAxes: [{
                        stacked: false,
                        barPercentage: 0.8,
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            // fontColor: "#8a909d",
                        },
                    },],
                    yAxes: [{
                        stacked: false,
                        gridLines: {
                            display: false,
                            color: "#eee",
                        },
                        ticks: {
                            stepSize: 50,
                            // fontColor: "#8a909d",
                        },
                    },],
                },
                tooltips: {
                    mode: "index",
                    intersect: false,
                    titleFontColor: "#888",
                    bodyFontColor: "#555",
                    titleFontSize: 12,
                    bodyFontSize: 15,
                    backgroundColor: "rgba(256,256,256,0.95)",
                    displayColors: true,
                    xPadding: 10,
                    yPadding: 7,
                    borderColor: "rgba(220, 220, 220, 0.9)",
                    borderWidth: 2,
                    caretSize: 6,
                    caretPadding: 5,
                }
            }
        });
    </script>
@endsection
