@extends('layouts.home') @section('content') @include('dashboard/nav')
<style>
#all-b{padding:20px 0;}
#all-b h3{font-size: 16px;}
#all-b h2{font-size: 26px;margin:20px 0 0 0;padding:0px;}
.inside-dyn{padding:10px 20px;}
 
.complain-request a {color: #f72e5e !important;}
.login-wraper .login-window .l-form {padding: 24px 20px 27px 20px;}
.login-wraper .login-window {width: 96.8%;margin: 12px 20px 20px 22px;}
  
#status_display  .dashboard_head {margin: 0 4px 0px 5px;    padding: 9px 19px 9px 20px;}
#status_display {margin: 5px 6px 0 17px;clear: both;position:relative;overflow: hidden;}
#status_display h3 {font-size: 13px;margin: 0;padding: 0;}
.content-tg{ background:#fff;padding: 10px 20px 20px 20px;   margin: 0 4px; }
.content-tg p {margin: 0 0 6px;}    
.content-tg a{background: #eaeaea;
    padding: 13px 20px 10px 20px;
    margin-top: 1px;
    display: block;
    width: 173px;
    text-align: center;}
.form.form-signin label {margin-bottom: 0px;}
</style>   

<div class="right-all">
    
    @php
    $compaints = DB::table('complains')->where('user_id',session()->get('user_id'))->get();
    @endphp
                 <div id="head-title" class="col-lg-12">
                         <div class="dashboard_head">
                            <h2>Complain Request </h2>  
                         </div>
                     </div> 
                    @forelse($compaints as $compaint)
                    <div id="status_display"> 
                        <div class="dashboard_head" > 
                            <a href="javascript:void(0)" class="click-ac">        
                            <h3><strong style="color:#f72e5e; width:60px;display:inline-block"> Subject:</strong>  {{$compaint->subject}}  ({{date('d-m-Y',strtotime($compaint->created_at))}})
                            <span style="background: #f72e5e;color: #fff;padding: 6px 11px 1px 11px;font-size: 10px;display: inline-block;margin-left: 20px;">Status: {{$compaint->status === 1?'Open':'Close' }}</span></h3>  </a>      

                       <img src="https://th.bing.com/th/id/OIP.xqevqnzQ1Mx8KQnhweOYAQHaHa?pid=ImgDet&amp;rs=1" style="position: absolute;width: 16px;right: 20px;top: 9px;"> 
                         </div> 
                         <div class="content-tg" > 
                            <p><strong style="color:#f72e5e; width:100px;display:inline-block">Name:</strong>{{$compaint->name}}</p>
                            <p><strong style="color:#f72e5e;width:100px;display:inline-block">Phone No:</strong> {{$compaint->phone}}</p>
                            <p><strong style="color:#f72e5e;width:100px;display:inline-block">Email Id:</strong> {{$compaint->email}}</p> 
                            <p><strong style="color:#f72e5e;width:100px;display:inline-block">Website link:</strong> {{$compaint->link}}</p>
                            <p><strong style="color:#f72e5e;width:100px;display:inline-block">Message:</strong> {{$compaint->message}}</p>
                         <a href="{{asset('/public/img/compain/'.$compaint->file)}}">View Uploaded File</a>
                         </div> 
                     </div>
                     @empty
                     No Request Found!
                     @endforelse
                    
                     
         
         
                     
<div class="container-fluid bg-image" style=" ">
   <div class="row">
       <div class="login-wraper">
           <div class="login-window">
           
               <div class="l-form">
                   @if(session()->has('success'))
                   	<div class="alert alert-success">
                   	  <strong>{{session()->get('success')}}</strong> 
                   	</div>
                   @endif
			   @if(count($errors))

	<div class="alert alert-danger">

		<strong>Whoops!</strong> There were some problems with your input.

		<br/>

		<ul>

			@foreach($errors->all() as $error)

			<li>{{ $error }}</li>

			@endforeach

		</ul>

	</div>

@endif
				
		 
 	
				<form method="POST" action="{{route('complainsave')}}"  id="loginform" class="form form-signin"  enctype="multipart/form-data">
				   {{ csrf_field() }}

                   
                       <div class="form-group">
                           <label for="exampleInputEmail1">Name <strong style="color:#f72e5e;">*</strong></label>
						   <input value="{{ old('name') }}" class="form-control required email valid" placeholder="" autocomplete="OFF" id="exampleInputEmail1" name="name" type="text">
                       </div>
                       
                           <div class="form-group">
                           <label for="exampleInputEmail1">Phone Number <strong style="color:#f72e5e;">*</strong> </label>
						   <input value="{{ old('phone') }}" class="form-control required email valid" placeholder="" autocomplete="OFF" id="exampleInputEmail1" name="phone" type="number">
                       </div>
                       
                           <div class="form-group">
                           <label for="exampleInputEmail1">Email Id <strong style="color:#f72e5e;">*</strong> </label>
						   <input value="{{ old('email') }}" class="form-control required email valid" placeholder="" autocomplete="OFF" id="exampleInputEmail1" name="email" type="email">
                       </div>
                       
                           <div class="form-group">
                           <label for="exampleInputEmail1">Subject <strong style="color:#f72e5e;">*</strong> </label>
						   <input value="{{ old('subject') }}" class="form-control required email valid" placeholder="" autocomplete="OFF" id="exampleInputEmail1" name="subject" type="text">
                       </div>
                       
                       
                            <div class="form-group">
                           <label for="exampleInputEmail1">Link (Video, Photo and Stories) <strong style="color:#f72e5e;">*</strong> </label>
						   <input value="{{ old('link') }}" class="form-control required email valid" placeholder="" autocomplete="OFF" id="exampleInputEmail1" name="link" type="text">
                       </div>
                       
                       
                        <div class="form-group">
                           <label for="exampleInputEmail1">Message <strong style="color:#f72e5e;">*</strong> </label>
						     <textarea rows="6" name="message" id="complaint" placeholder="" required="">{{old('message')}}</textarea>
                       </div>
                       
               <label for="myfile">Select a file:</label>
             <input type="file" id="myfile" class="form-control" name="file"><br><br>
                      
                       <div class="row">
                           <div class="col-lg-12">                
							<button type="submit" class="btn btn-cv1">Submit</button>
							</div>
                       </div>
                      
                    
					         
                   </form>
               </div>
           </div>
       </div>
   </div>

</div>
    
    
    
    
    
    </div>
</div>
</div>
</div>
</div>


<script>
$(".click-ac").click(function(){
$(".content-tg").toggle('500');
});
</script>


@endsection 

