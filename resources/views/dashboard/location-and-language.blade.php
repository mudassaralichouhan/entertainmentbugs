@extends('layouts.home')
@section('content')
@include('dashboard/nav') 

<style>.location-and-language a{color:#f72e5e;} </style>

    <div class="right-all"> 
                     <div id="head-title" class="col-lg-12">
                         <div class="dashboard_head">
                            <h2>Location & Language</h2>   
                            <p>Your Address and phone number will not be shown to any users.</p> 
                         </div>
                     </div>
                      <div class="col-lg-12" id="extra-pad-n1">
                         <div id="location_and_language" class="tab-content">	 
        		                         	<div class="setting-page" style="background:#fff;padding:15px 20px 20px 20px;margin-top:-5px;">
        									<h4 style="margin-bottom: 10px;">Set your home based location : </h4>
        					               
        
                                        <input placeholder="Address:" type="text" style="border:1px solid #eaeaea;width:400px;padding:8px 20px 5px 20px;margin-right:10px;">
                                        <input placeholder="Pincode:" type="text" style="border:1px solid #eaeaea;width:400px;padding:8px 20px 5px 20px">
                                        <div style="clear:both; overflow:hidden;width: 100%;height: 10px;"></div>
        			                   
        					                 <select style="border:1px solid #eaeaea;width:400px;margin-right:10px;padding:8px 20px 5px 20px;float:left">
        					                     <option>Country</option> 
        					                      <option>Country 1</option> 
        					                 </select>
        					                   
        					                   <select style="border:1px solid #eaeaea;width:400px;padding:8px 20px 5px 20px;float:left">
        					                       <option>State</option> 
        					                      <option>State 1</option> 
        					                 </select>
        					                 
                                <div style="clear:both; overflow:hidden;width: 100%;height: 10px;"></div>
                                     
        <input placeholder="Cities" type="text" style="border:1px solid #eaeaea;width:400px;padding:8px 20px 5px 20px;margin-right:6px;">
        <input placeholder="Phone Number" type="text" style="border:1px solid #eaeaea;width:400px;padding:8px 20px 5px 20px">
        
                                <div style="clear:both; overflow:hidden;width: 100%;height: 10px;"></div>
        
                               
                               
                               
                               
                                <p>Language</p>
                                
                                <input placeholder="Multiple Language: English, Hindi..." type="text" style="border:1px solid #eaeaea;width:400px;padding:8px 20px 5px 20px">
                                 <div style="clear:both; overflow:"></div>
                                <button type="button">Save</button>
                        </div>
                                          <div style="clear:both; overflow:"></div>
                                        </div>
                    </div>  
                 </div>  
			   </div>
            </div>
        </div>
    </div>
	  
@endsection
