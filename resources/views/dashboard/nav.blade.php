  <link href="{{ asset('public/css/dashboard.css') }}" rel="stylesheet">
 
 <style>
 #extra-pad-n1 {padding: 0 11px 0 20px;}
</style>

 <div style="background:#eaeaea">	
		 <div class="container" style="    padding-right: 5px;">		
            <div class="container-fluid u-details-wrap" style="margin:15px 0;padding:0px;">
              <div class="rows"> 
                   <div   id="ad_camp_menu"> 
                    <ul class="">

                        <li class="dashboard-menu"><a href="{{URL::to('dashboard')}}"><i class="fa fa-dashboard icon" aria-hidden="true"></i> Dashboard</a></li> 
                        <li class="total-earning"><a href="{{URL::to('total-earning')}}"><i class="fa fa-money" aria-hidden="true"></i> Total Earning</a></li> 
                        <li class="payment-withdraw"><a href="{{URL::to('payment-withdraw')}}"><i class="fa fa-university" aria-hidden="true"></i> Payment Withdraw</a></li>
                        <li class="promote-recent-post"><a href="{{URL::to('promote-recent-post')}}"><i class="fa fa-rocket" aria-hidden="true"></i> Promote Recent Posts</a></li>
                        <li class="promoted-posts"><a href="{{URL::to('promoted-posts')}}"><i class="fa fa-list" aria-hidden="true"></i> Promoted Posts Lists</a></li>




@php
    $artist = \App\Models\ArtistAbout::where('user_id', session()->get('user_id'))->first();
@endphp

@if($artist)
    <!--Only for artist-->
    <li class="hidden-xs" style="padding-top: 16px;border-top: 1px solid #e5e5e5;padding-bottom: 0;"><a href="#"><i class="fas fa-theater-masks"></i> Promote Artist Portfolio<i class="fa fa-angle-down" style="float:right;margin: 2px 30px 0 0;"></i></a></li>  
    <li style="padding-left:15px;" class="boost-plan"><a href="{{URL::to('boost-plan')}}">Premium Profile</a></li> 
    <li  style="padding-left:15px;" class="theme-plan"><a href="{{URL::to('theme-plan')}}">Theme Plan</a></li> 
    <!--Only for artist-->
@endif
    <li class="" style="padding-top: 16px;border-top: 1px solid #e5e5e5;padding-bottom: 0;"><a href="#"><i class="fa-solid fa-bullhorn"></i> Promote ADS <i class="fa fa-angle-down" style="float:right;margin: 2px 30px 0 0;"></i></a></li>
    <li class="ads-overview" style="padding-left:19px;"><a href="{{URL::to('ads-overview')}}">Ads Overview</a> </li>
    <li class="active-ads-campaign" style="padding-left:19px;"><a href="{{URL::to('active-ads-campaign')}}">Active Ads Campaign</a></li>
    <li class="closed-ads-campaign" style="padding-left:19px;"><a href="{{URL::to('closed-ads-campaign')}}">Closed Ads Campaign</a></li>
    <li class="create-new-ads-campaign" style="padding-left: 19px;border-bottom: 1px solid #e5e5e5;padding-bottom: 17px;"><a href="{{URL::to('create-new-ads-campaign')}}"> Create New Ads Campaign</a></li>
    <li class="hidden-xs information"><a href="{{URL::to('information')}}"><i class="fa fa-info-circle" aria-hidden="true"></i> Information </a></li> 
    <li class="hidden-xs location-and-language"><a href="{{URL::to('location-and-language')}}"><i class="fa fa-globe" aria-hidden="true"></i> Location & Language </a></li>  
    <li class="hidden-xs privacy-setting"><a href="{{URL::to('privacy-setting')}}"><i class="fa fa-sliders" aria-hidden="true"></i> Privacy Setting</a></li>   
     <li class="hidden-xs complain-request"><a href="{{URL::to('complain-request')}}"><i class="fa fa-sliders" aria-hidden="true"></i> Complain Request</a></li>   
    
    
    <li class="hidden-xs contact-us"><a href="{{URL::to('contact-us')}}"><i class="fa fa-envelope" aria-hidden="true"></i> Contact Us</a></li>
</ul>
<div class="clearfix"></div>
                
            
@php
    $artist = \App\Models\ArtistAbout::where('user_id', session()->get('user_id'))->first();
@endphp

@if($artist)
            <style>#only-user-and-pro{display:none}</style>
                <div id="user-payment_block" style="width: 100%;margin: 30px 0 0 0;padding: 0px 0 0 0; background:#eaeaea">
                    
                   <h3 style="font-size:15px;">Artist Profile Premium  Plans</h3>
                    <div class="plan-option-left-panel">
                        <div>  
                            <div class="left-side-pad"> 
                              <h4 class="u-text u-text-default u-text-palette-2-base u-text-8"> Silver Plan</h4>
                               <h1 class="u-custom-font u-font-pt-sans u-text u-text-default u-text-palette-2-base u-text-9"> Rs:349/- <span style="display:block;font-size: 1.5rem;"> for 1 month</span>
                                                                              </h1>
                              <a href="#" class="second-plan" style="margin:-10px 0 13px 0">Proceed Monthly </a>
                              <p class="u-text u-text-10"> - Artist  Profile highlited. <br>- Number of viewers Increase <br>- Suggested to production house</p>
                              <a href="" style="text-align:right;color:#fff;float: right;margin: -2px 5px -2px  0;">View all plans</a>
                            </div>
                          </div> 
                    </div>
                    
                        <div style="width: 100%;margin: 30px 0 0 0;padding: 0px 0 0 0; background:#eaeaea">
                    
                   <h3 style="font-size:15px;">Artist Profile Theme Plans</h3>
                    <div class="plan-option-left-panel">
                        <div>  
                            <div class="left-side-pad" style="background:#28b47e"> 
                              <h4 class="u-text u-text-default u-text-palette-2-base u-text-8"> Premium Theme</h4>
                               <h1 class="u-custom-font u-font-pt-sans u-text u-text-default u-text-palette-2-base u-text-9"> Rs:499/- <span style="display:block;font-size: 1.5rem;">for 6 months </span>
                                                                              </h1>
                              <!--<a href="#" class="second-plan" style="margin:-10px 0 13px 0">Proceed Yearly </a>-->
                              <p class="u-text u-text-10"> 
                                    - Portfoilio theme Change option<br>
                                    - Background Image change option<br>
                                    - 12 Gallery Image<br>
                                    - 12 Video<br>
                                    - For 1 years<br>
                                    - Payment Required
                               </p> 
                               <Style>.razorpay-payment-button {
    background: #f72e5e;
    border: 0px;
    text-align: center;
    margin: 10px 0 10px 0;
    width: 185px;
    display: block;
    color: #fff;
    line-height: 40px;
    font-size: 16px;
    padding: 6px 0 0 0;
}</Style>
                               
                               <form action="{{ route('payment') }}" method="POST">
                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <script src="https://checkout.razorpay.com/v1/checkout.js" data-key="{{ env('RAZOR_KEY') }}" data-amount="49900" data-buttontext="Proceed Yearly" data-name="Theme plan" data-description="Purchase theme plan" data-prefill.name="name" data-prefill.email="email" data-theme.color="#ff7529" data-notes.type='theme-plan'></script>
                              </form>
                          
                              <a href="" style="text-align:right;color:#fff;float: right;margin: -2px 5px -2px  0;">View all plans</a>
                            </div>
                          </div> 
                    </div> 
                      </div> 
                    
                </div>
               @endif  
                
                
                
                 <div id="only-user-and-pro" style="width: 100%;margin: 30px 0 0 0;padding: 0px 0 0 0; background:#eaeaea">
                    
                   <h3 style="font-size:15px;">Promote Multiple post</h3>
                    <div class="plan-option-left-panel">
                        <div>  
                            <div class="left-side-pad"> 
                              <h4 class="u-text u-text-default u-text-palette-2-base u-text-8"> Video Plan</h4>
                               <h1 class="u-custom-font u-font-pt-sans u-text u-text-default u-text-palette-2-base u-text-9"> Rs:139/- <span style="display:block;font-size: 1.5rem;"> 30days</span>
                                                                              </h1>
                              <a href="#" class="second-plan" style="margin:-10px 0 13px 0">Proceed to buy </a>
                              <p class="u-text u-text-10"> - First 3 videos will be boost. <br>- Number of views Increase <br>- Always Recommended your video</p>
                            
                            </div>
                          </div> 
                    </div>
  
  
  
      <div style="width: 100%;margin: 30px 0 0 0;padding: 0px 0 0 0; background:#eaeaea">
                     
                    <div class="plan-option-left-panel">
                        <div>  
                            <div class="left-side-pad" style="background:#28b47e"> 
                              <h4 class="u-text u-text-default u-text-palette-2-base u-text-8"> Photos Plan</h4>
                               <h1 class="u-custom-font u-font-pt-sans u-text u-text-default u-text-palette-2-base u-text-9"> Rs:499/- <span style="display:block;font-size: 1.5rem;">30days </span>
                                                                              </h1>
                              <a href="#" class="second-plan" style="margin:-10px 0 13px 0">Proceed Yearly </a>
                              <p class="u-text u-text-10"> 
                                    -  First 2 Post will be boost<br>
                                    - Number of views Increase<br>
                                    - Always display first<br> 
                               </p> 
                             
                            </div>
                          </div> 
                    </div> 
                      </div> 
                      
                      
                </div>
                
                
                
                
            </div>

