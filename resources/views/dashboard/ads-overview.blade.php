@extends('layouts.home')
@section('content')
@include('dashboard/nav') 
<style>.ads-overview a{color:#f72e5e;} #ads-banner1{width:100% !important}</style>
    <div class="right-all"> 
        <div id="head-title" class="col-lg-12">
            <div class="dashboard_head">
            <h2>Ads Overview</h2></h2> 
            <p>All you Ads Campaign</p>
            </div>
        </div>
        <div class="col-lg-12" id="extra-pad-n1" >
            <div id="campaign-three" class="padding-right-my-0"  style="margin:0 -10px !important">
        
            <div class="col-lg-4 col-sm-4">
                <div class="card widget-stat ">
                    <div class="card-body p-4">
                        <div class="media align-items-center">
                            <div class="media-body">
                                <p class="fs-18 mb-2 wspace-no">Total Campaign </p>
                                <h1 class="fs-36 font-w600 text-black mb-0">{{ $totalAdCampaigns }}</h1>
                            </div>
                            <span class="ml-3 bg-primary text-white">
                            <i class="fa fa-buysellads" aria-hidden="true"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="card widget-stat">
                    <div class="card-body p-4">
                        <div class="media align-items-center">
                            <div class="media-body">
                                <p class="fs-18 mb-2 wspace-no">Total Campaign Audience</p>
                                <h1 class="fs-36 font-w600 d-flexs align-items-center text-black mb-0">{{ 0 }}</h1>
                            </div>
                            <span class="ml-3 bg-warning text-white" style="background:#ff6d4d !important">
                                <i class="fa fa-user"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col-lg-4 col-sm-4">
                <div class="card widget-stat">
                    <div class="card-body p-4">
                        <div class="media align-items-center">
                            <div class="media-body">
                                <p class="fs-18 mb-2 wspace-no">Total Spends</p>
                                <h1 class="fs-36 font-w600 text-black mb-0">Rs.{{ $totalSpent }}</h1>
                            </div>
                            <span class="ml-3 bg-primary text-white">
                                <i class="fa fa-usd"></i>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>		       
        <div class="clear padding-right-my-0"  style="margin:0 -10px !important">
            <h3 class="fs-20" style="padding-left: 11px">Ads Title: {{ is_var_exist($latestPlan, 'ad') ? $latestPlan[0]->ad->title : '' }}</h3>
            <div id="canvas-main-dashboard" class="my-chart padding-right-my-0">
                <iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; inset: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                <h2></h2><h2><span> Total Views : {{ $latestPlanGraphData ? $latestPlanGraphData['totalViews'] : 0 }} <br>Total Click: {{ $latestPlanGraphData ? $latestPlanGraphData['totalClicks'] : 0 }}</span>Your Plan is Rs: {{ is_var_exist($latestPlan, 'amount') ? $latestPlan[0]->amount : 0 }}/-  30 day</h2>
                <canvas id="advertise-count" height="300" style="display: block; width: 882px; height: 300px;" width="882"></canvas>
            </div>
            <div id="canvas-two-block">
                <div class="left-b09">
                    <div class="card" style="padding-bottom:10px">
                        <div class="card-header"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; inset: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe> 
                        <canvas id="pie-chart" width="405" height="299" style="display: block; width: 405px; height: 299px;"></canvas>
                        </div>
                    </div>
                </div>
                <div class="right-b09">
                    <div class="card" style="padding-bottom:10px">
                        <div class="card-header"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; inset: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe> 
                            <canvas id="bar-chart" width="405" height="299" style="display: block; width: 405px; height: 299px;"></canvas>     
                        </div>
                    </div>
                </div>
            </div>
        </div>				       
        <div class="col-lg-12" style="padding:0px">
            <h3 class="fs-20">Trending Ads</h3> 
            <div class="card widget-stat">
                <div class="card-header border-0">
                
                </div>
                <div class="card-body pb-0">
                    <div class="row pb-4 mb-4 border-bottom align-items-center">
                        <div class="col-6">
                            <h4 class="fs-18 text-ov"><a class="text-black" href="https://makaanlelo.com/tf_products_007/eclan/laravel/demo/social-network-campaign">Game Online Vouchers 20% OFF</a></h4>
                            <span class="fs-14 text-ov">Published on 5 June 2020</span>
                        </div>
                        <div class="col-6 d-flex align-items-center">
                            <svg class="mr-3 ml-auto" width="89" height="46" viewBox="0 0 89 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M26.0681 24.5C15.8708 19.8496 4.32677 34.0099 2 40.8994L2.60932 46H87V2C70.2437 4.5 70.2437 13.5 56.534 29.5C43.9117 44.231 38.8148 30.313 26.0681 24.5Z" fill="#52B141" fill-opacity="0.33"></path>
                                <path d="M2 41C4.32677 34.0927 15.8708 19.8958 26.0681 24.5582C38.8148 30.3863 43.9117 44.3403 56.534 29.5711C70.2437 13.5297 70.2437 4.50647 87 2" stroke="#52B141" stroke-width="4" stroke-linecap="round"></path>
                            </svg>
                            <div>
                                <h4 class="fs-20 text-black">672k <span class="fs-14">Visitors</span></h4>
                                
                            </div>
                        </div>
                    </div>
                    <div class="row pb-4 mb-4 border-bottom align-items-center">
                        <div class="col-6">
                            <h4 class="fs-18 text-ov"><a class="text-black" href="https://makaanlelo.com/tf_products_007/eclan/laravel/demo/social-network-campaign">15% OFF Granite Stone</a></h4>
                            <span class="fs-14 text-ov">Published on 5 June 2020</span>
                        </div>
                        <div class="col-6 d-flex align-items-center">
                            <svg class="mr-3 ml-auto" width="89" height="46" viewBox="0 0 89 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M62.9319 24.5C73.1292 19.8496 84.6732 34.0099 87 40.8994L86.3907 46H2V2C18.7563 4.5 18.7563 13.5 32.466 29.5C45.0883 44.231 50.1852 30.313 62.9319 24.5Z" fill="#F83737" fill-opacity="0.11"></path>
                                <path d="M87 41C84.6732 34.0927 73.1292 19.8958 62.9319 24.5582C50.1852 30.3863 45.0883 44.3403 32.466 29.5711C18.7563 13.5297 18.7563 4.50647 2 2" stroke="#F83737" stroke-width="4" stroke-linecap="round"></path>
                            </svg>
                            <div>
                                <h4 class="fs-20 text-black">672k 	<span class="fs-14">Visitors</span></h4>
                            
                            </div>
                        </div>
                    </div>
                    <div class="row pb-4 border-bottom align-items-center">
                        <div class="col-6">
                            <h4 class="fs-18 text-ov"><a class="text-black" href="https://makaanlelo.com/tf_products_007/eclan/laravel/demo/social-network-campaign">50% OFF Floor Lamp Get it Now!</a></h4>
                            <span class="fs-14 text-ov">Published on 5 June 2020</span>
                        </div>
                        <div class="col-6 d-flex align-items-center">
                            <svg class="mr-3 ml-auto" width="89" height="46" viewBox="0 0 89 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M26.0681 24.5C15.8708 19.8496 4.32677 34.0099 2 40.8994L2.60932 46H87V2C70.2437 4.5 70.2437 13.5 56.534 29.5C43.9117 44.231 38.8148 30.313 26.0681 24.5Z" fill="#52B141" fill-opacity="0.33"></path>
                                <path d="M2 41C4.32677 34.0927 15.8708 19.8958 26.0681 24.5582C38.8148 30.3863 43.9117 44.3403 56.534 29.5711C70.2437 13.5297 70.2437 4.50647 87 2" stroke="#52B141" stroke-width="4" stroke-linecap="round"></path>
                            </svg>
                            <div>
                                <h4 class="fs-20 text-black">672k 	<span class="fs-14">Visitors</span></h4>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if( $latestPlan && count($latestPlan) > 0 )
            @foreach($latestPlan as $plan)
                <div class="col-lg-12" style="padding:0px">
                    <div class="u-details"> 
                        <div class="row"> 
                            <div class="col-lg-12">
                                <span style="background:#f72e5e;color:#fff;padding: 11px 10px 5px 33px;margin: 10px 0 0px 0;display:block;text-align: left;font-size: 17px;">{{ $plan->ad->title }} -- ACTIVE</span>
                            </div>
                            <div class="all-dis" style="clear: both;overflow: hidden;padding: 20px 10px; background: #fff !important;margin: 14px;">
                                <div class="col-lg-3">
                                    @if( $plan->ad->type == "video" )
                                        <video id="" width="100%" controls="">
                                            <source src="{{ asset('public/ads/videos/'.$plan->ad->file) }}" poster="{{ asset('public/ads/videos/thumb'.$plan->ad->file) }}" type="video/mp4">
                                        </video>
                                    @else
                                        <img id="ads-banner1" src="{{ asset('public/ads/images/'.$plan->ad->file) }}" style="width: {{ $plan->ad->width }}; height: {{ $plan->ad->height }}" />
                                    @endif
                                    <div class="u-title" style="font-size:18px;">                
                                </div>
                                <br>
                                <p>Views: {{ $plan->views }}</p>
                                <p>Validity: {{ $plan->days }} Days</p>
                                <p>Amount: Rs {{ $plan->amount }}/-</p>
                            </div>
                            <div class="col-lg-9">
                                <div id="video_up_preview"> 
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="e2">About:</label>
                                            <span>{{ $plan->ad->description }}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="e1">Video Tags:</label>
                                            <span>{{ $plan->ad->tags }}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="e3">Language:</label> 
                                            <span>{{ $plan->ad->language }}</span>
                                        </div>
                                    </div>  
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="e3">Category:</label> 
                                            <span>{{ $plan->ad->categories }}</span>
                                        </div>
                                    </div>  
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="e3">State:</label> 
                                            <span>kolkata, delhi</span>
                                        </div>
                                    </div>  
                                    <a href="{{ url('edit-ads-campaign/'.$plan->ad->slug) }}" style="background:#f72e5e;color:#fff;padding: 5px 10px 0 10px;float: right;font-size: 14px;margin: -3px 0 0 0;">Edit</a>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
            </div>
            </div>
            </div>
            </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script> 
<script>
    //Transaction Graph 
    var ctx = document.getElementById("advertise-count");
    ctx.height = 200;
    var myChart = new Chart(ctx, {
        type: 'bar',
        labels: {!! $latestPlanGraphData ? json_encode($latestPlanGraphData['date']) : "[]" !!},
        datasets: [
            {!! $latestPlanGraphData ? json_encode($latestPlanGraphData['views']) : "[]" !!},
            {!! $latestPlanGraphData ? json_encode($latestPlanGraphData['clicks']) : "[]" !!}
        ],
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false,
            },
            scales: {
                xAxes: [
                    {
                        stacked: false,
                        barPercentage: 0.8,
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            // fontColor: "#8a909d",
                        },
                    },
                ],
                yAxes: [
                    {
                        stacked: false,
                        gridLines: {
                            display: false,
                            color: "#eee",
                        },
                        ticks: {
                            stepSize: 50,
                            // fontColor: "#8a909d",
                        },
                    },
                ],
            },
            tooltips: {
                mode: "index",
                intersect: false,
                titleFontColor: "#888",
                bodyFontColor: "#555",
                titleFontSize: 12,
                bodyFontSize: 15,
                backgroundColor: "rgba(256,256,256,0.95)",
                displayColors: true,
                xPadding: 10,
                yPadding: 7,
                borderColor: "rgba(220, 220, 220, 0.9)",
                borderWidth: 2,
                caretSize: 6,
                caretPadding: 5,
            }
        }
    });
</script>
 
<script>
    //Transaction Graph 
    var ctx = document.getElementById("transaction-graph");
    ctx.height = 300;
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [
                "1 Jan",
                "2 Jan",
                "3 Jan",
                "4 Jan",
                "5 Jan",
                "6 Jan",
                "7 Jan", 
            ],
            datasets: [
                {
                    label: "Visit",
                    backgroundColor: "rgba(32, 212, 137, 1)",
                    borderColor: "transparent",
                    data:[40, 105, 250, 55, 138, 5, 205],
                    borderWidth: 2,
                }, 
            ],
        },
        options: {responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false,
            },
            elements: {
                point:{
                    radius: 0
                }
            },
            scales: {
                xAxes: [
                    {
                        stacked: true,
                        barPercentage: 0.45,
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            // fontColor: "#8a909d",
                        },
                    },
                ],
                yAxes: [
                    {
                        stacked: true,
                        gridLines: {
                            display: false,
                            color: "#eee",
                        },
                        ticks: {
                            stepSize: 50,
                            // fontColor: "#8a909d",
                        },
                    },
                ],
            },
            tooltips: {
                mode: "index",
                intersect: false,
                titleFontColor: "#888",
                bodyFontColor: "#555",
                titleFontSize: 12,
                bodyFontSize: 15,
                backgroundColor: "rgba(256,256,256,0.95)",
                displayColors: true,
                xPadding: 10,
                yPadding: 7,
                borderColor: "rgba(220, 220, 220, 0.9)",
                borderWidth: 2,
                caretSize: 6,
                caretPadding: 5,
            }
        }
    });
</script>
<script>
    //Transaction Graph 
    var ctx = document.getElementById("transaction-stories");
    ctx.height = 300;
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [
                "1 Jan",
                "2 Jan",
                "3 Jan",
                "4 Jan",
                "5 Jan",
                "6 Jan",
                "7 Jan",
                "8 Jan",
                "9 Jan",
                "10 Jan",
                "11 Jan",
                "12 Jan",
                "13 Jan",
                "14 Jan",
                "15 Jan",
                "16 Jan",
                "17 Jan",
                "18 Jan", 
                "19 Jan",
                "20 Jan",
                "21 Jan",
                "22 Jan",
                "23 Jan",
                "24 Jan",
                "25 Jan",
                "26 Jan",
                "27 Jan",
                "28 Jan",
                "29 Jan",
                "30 Jan",
            ],
            datasets: [
                {
                    label: "Visit",
                    backgroundColor: "rgba(32, 212, 137, 0.6)",
                    borderColor: "transparent",
                    data:[40, 105, 250, 55, 138, 5, 5,40, 105, 250, 55, 158, 5, 235,40, 125, 250, 55, 138, 5, 205,40, 105, 250, 55, 138, 5, 205, 23, 64],
                    borderWidth: 2,
                }, 
            ],
        },
        options: {responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false,
            },
            elements: {
                point:{
                    radius: 0
                }
            },
            scales: {
                xAxes: [
                    {
                        stacked: true,
                        barPercentage: 0.45,
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            // fontColor: "#8a909d",
                        },
                    },
                ],
                yAxes: [
                    {
                        stacked: true,
                        gridLines: {
                            display: false,
                            color: "#eee",
                        },
                        ticks: {
                            stepSize: 50,
                            // fontColor: "#8a909d",
                        },
                    },
                ],
            },
            tooltips: {
                mode: "index",
                intersect: false,
                titleFontColor: "#888",
                bodyFontColor: "#555",
                titleFontSize: 12,
                bodyFontSize: 15,
                backgroundColor: "rgba(256,256,256,0.95)",
                displayColors: true,
                xPadding: 10,
                yPadding: 7,
                borderColor: "rgba(220, 220, 220, 0.9)",
                borderWidth: 2,
                caretSize: 6,
                caretPadding: 5,
            }
        }
    });
</script>
 <script>
    new Chart(document.getElementById("bar-chart"), {
        type: 'bar',
        data: {
        labels: ["Kerala", "Assam", "Goa", "Gujrat", "Delhi", "Tripura", "Assam", "Goa", "Gujrat", "Total"],
        datasets: [
            {
                label: "Population (millions)",
                backgroundColor: ["#3e95cd", "#8e5ea2", "#000", "red", "blue", "#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#c45850"],
                data: [50,30,10,20,10,30,10,20,50,160]
            }
        ]
        },
        options: {
            legend: { display: false },
            title: {
                display: true,
                text: 'Current ads views as per locations'
            }
        }
    });
</script>

<script>
    new Chart(document.getElementById("pie-chart"), {
        type: 'pie',
        data: {
            labels: ["Spend: 150", "Total: 400"],
            datasets: [{
                label: "Population (millions)",
                backgroundColor: ["#3e95cd", "#8e5ea2"],
                data: [150,400]
            }]
        },
        options: {
            title: {
                display: true,
                text: 'Total Spent moneny on your active ads'
            }
        }
    });
</script> 
<script>
    //Transaction Graph
    var ctx = document.getElementById("visitor-num");
    ctx.height = 200;
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                "1 Jan",
                "2 Jan",
                "3 Jan",
                "4 Jan",
                "5 Jan",
                "6 Jan",
                "7 Jan",
                "8 Jan",
                "9 Jan",
                "10 Jan",
                "11 Jan",
                "12 Jan",
                "13 Jan",
                "14 Jan",
                "15 Jan",
                "16 Jan",
                "17 Jan",
                "18 Jan", 
                "19 Jan",
                "20 Jan",
                "21 Jan",
                "22 Jan",
                "23 Jan",
                "24 Jan",
                "25 Jan",
                "26 Jan",
                "27 Jan",
                "28 Jan",
                "29 Jan",
                "30 Jan",
                
            ],
            datasets: [
                {
                    label: "Video",
                    backgroundColor: "rgba(32, 212, 137, 1)",
                    borderColor: "transparent",
                    data: [40, 105, 92, 155, 138, 205, 120, 92, 155, 138, 215, 120,138, 12, 120, 138, 205, 120, 92, 155, 22, 155, 138, 185, 120, 92, 155, 138, 265, 120,],
                    borderWidth: 4,
                    borderRadius: Number.MAX_VALUE,
                    borderSkipped: false,
                },
                
                {
                    label: "Photos",
                    backgroundColor: "rgba(32, 212, 137, 0.25)",
                    borderColor: "transparent",
                    data: [65, 59, 80, 81, 56, 55, 40, 80, 81, 56, 55, 40, 56, 33, 40, 55, 40, 80, 81, 56, 55, 80, 61, 56, 55, 40, 56, 55, 40, 55],
                    borderWidth: 2,
                    borderRadius:4,
                    borderSkipped: false,
                },
            ],
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false,
            },
            scales: {
                xAxes: [
                    {
                        stacked: false,
                        barPercentage: 0.8,
                        gridLines: {
                            display: false,
                            drawBorder: false,
                        },
                        ticks: {
                            // fontColor: "#8a909d",
                        },
                    },
                ],
                yAxes: [
                    {
                        stacked: false,
                        gridLines: {
                            display: false,
                            color: "#eee",
                        },
                        ticks: {
                            stepSize: 50,
                            // fontColor: "#8a909d",
                        },
                    },
                ],
            },
            tooltips: {
                mode: "index",
                intersect: false,
                titleFontColor: "#888",
                bodyFontColor: "#555",
                titleFontSize: 12,
                bodyFontSize: 15,
                backgroundColor: "rgba(256,256,256,0.95)",
                displayColors: true,
                xPadding: 10,
                yPadding: 7,
                borderColor: "rgba(220, 220, 220, 0.9)",
                borderWidth: 2,
                caretSize: 6,
                caretPadding: 5,
            }
        }
    });
</script>
      
@endsection
