@extends('layouts.home') @section('content') @include('dashboard/nav') <style>
  .theme-plan a {
    color: #f72e5e;
  }
  .razorpay-payment-button{    background: red;
    color: #fff;
    border: 0px;
    line-height: 14px;
    padding: 10px 35px 7px 35px;}
  
</style>
<div class="right-all">
  @if (Session::has('success'))
    <div class="alert alert-info alert-dismissible fade in" role="alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
      </a>
      <strong>Success!</strong> {{ Session::get('success') }}
    </div>
  @endif
  @if ($errors->any())
    <div class="alert alert-danger alert-dismissible fade in" role="alert">
      <a href="#" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
      </a>
      <div>
        <strong>Error!</strong> {!! implode('', $errors->all('<span>:message</span>')) !!}
      </div>
    </div>
  @endif
  <div id="head-title" class="col-lg-12">
    <div class="dashboard_head">
      <h2>Theme Plan</h2>
    </div>
  </div>
  <div class="col-lg-12" id="extra-pad-n1">
    <div id="boost-plan" class="tab-content">
      <div class="setting-page" style="background:#fff;padding:10px 20px 20px 20px;margin-top:-5px">
        <h4 style="text-align:center; margin-top:10px;  font-size:22px;">Artist Profile Theme Plans</h4>
        <div class="plans-main" style="width:75%">
          <div class="u-clearfix u-expanded-width u-gutter-30 u-layout-wrap u-layout-wrap-2">
            <div class="u-gutter-0 u-layout">
              <div class="u-layout-row">
              <?php
                if($my->hasThemePlan()) {
              ?>  
              <style>
                  .off-adter-purchase{display:none;}
              </style>
              
              
                <div class="after-purchase-message" style="background:#28b47e; margin-bottom:20px;padding-bottom:20px;">
                  <h1 class="" style="color:#fff;text-align:center; margin: 0; font-size: 26px;padding: 25px 0 0px 0;"> Your Theme Plan is activated </h1>
               <div style="clear:both; overflow:hidden;padding:20px">
               <div style="width:50%;float:left">
                  <p class="u-text" style="color:#fff;text-align:left;font-size: 16px;line-height: 21px;padding-bottom: 10px;"> 
                  Payment: {{ $my->activeThemePlan ? to_amount($my->activeThemePlan->amount) : "499.00" }}/- <br> 
                  Payment made date: {{ $my && $my->plan_started_at ? \Carbon\Carbon::createFromTimestamp($my->plan_started_at)->format("Y-m-d") : "" }} <br> 
                  Expire Date: {{ $my && $my->plan_expired_at ? \Carbon\Carbon::createFromTimestamp($my->plan_expired_at)->format("Y-m-d") : "" }} <br>
                       </p>
                       </div>  
                       
                       
                  <div style="width:50%;float:left">
<p style="color:#fff; padding:20px;">Background Image change option <br>
8 Services (What we do)<br>
3 Achievement<br>
12 Gallery Image<br>
6 Video<br>
Latest Project <br>
Brand Collaboration<br>
Quote<br>
For 1 years
</p>
                  </div>
                </div>  
                  
             
                  <a class="u-btn" style="background: #fff; border: 0px; text-align: center; margin: 10px auto 10px; width: 185px; display: block; color: #000; line-height: 40px; font-size: 16px; padding: 6px 0 0 0;"> Renew Again </a>
                </div>
                <?php } ?>
                <div class="off-adter-purchase">
                <div class="half-6-6 u-align-left u-layout-cell-4">
                  <div class="u-container-layout u-valign-top u-container-layout-4">
                    <h4 class="u-text u-text-body-alt-color u-text-default u-text-5"> Basic Theme</h4>
                    <h1 class="u-custom-font u-font-pt-sans u-text u-text-default u-text-6"> Free</h1>
                    <a class="u-btn"> Already Activated </a>
                    <p class="u-text u-text-7" style="color:#fff"> Static Background Image <br> 4 Services <br> 4 Gallery Image <br> 3 Video <br> New Design <br> Free for 3 years. <br> No Payment Required </p>
                  </div>
                </div>
                <div class="u-align-left half-6-6" style="background:#eaeaea; position:relative">
                  <div class="u-container-layout u-valign-top u-container-layout-5">
                    <h4 class="u-text u-text-default u-text-palette-2-base u-text-8"> Premium Theme</h4>
                    <h1 style="margin: 8px 0;" class="u-custom-font u-font-pt-sans u-text u-text-default u-text-palette-2-base u-text-9"> Rs:499 <span style="font-size: 1.5rem;">/year</span>
                    </h1>
                    <div class="col-md-12" style="padding:0px">
                      <div class="panel panel-default" style="margin-top: 0px;margin-bottom: 14px;">
                        <?php
                          $userId = session()->get('user_id');
                          $user = DB::select('select * from users where id='.$userId);
                          if($my->plan != 2) {
                        ?>  
                      <div class="panel-heading">
                        <h2 style="font-size: 16px;margin: 5px 0 7px 0;">Puchase Yearly Plan</h2>
                          <form action="{{ route('payment') }}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <script src="https://checkout.razorpay.com/v1/checkout.js" data-key="{{ env('RAZOR_KEY') }}" data-amount="49900" data-buttontext="Buy Now" data-name="Theme plan" data-description="Purchase theme plan" data-prefill.name="name" data-prefill.email="email" data-theme.color="#ff7529" data-notes.type='theme-plan'></script>
                          </form>
                        </div>
                      <?php } else { ?>
                        <a class="u-btn"> Already Activated </a>
                      <?php } ?>
                      </div>
                    </div>
                    <!--<a href="#" class="second-plan">Puchase Yearly Plan </a>-->
                    <p class="u-text u-text-10"> Background Image change option <br> 8 Services <br> 12 Gallery Image <br> 6 Video <br> Static Design <br> For 1 years <br> Payment Required </p>
                    <a href="" target="_blank" style="background: #28b47e; color:#fff;width: 117px;display:block;text-align: Center;padding: 10px 0 7px 0;position: absolute;right: 1px;top: 78%;font-size: 12px;">View Demo</a>
                  </div>
                </div>
                
                </div>
              </div>
            </div>
          </div>
        </div>
        <p class="off-adter-purchase" style="font-size:14px;line-height: 25px;margin-top: 0px;text-align:center"> You profile get more powerfull to our platform and your profile will be more highlighted on premium theme. </p>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div> @endsection