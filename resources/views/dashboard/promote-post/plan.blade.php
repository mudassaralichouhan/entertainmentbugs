@extends('layouts.home')
@section('content')
                    @php
					$photoplan = \DB::table('photo_price')->get();
                    $videoplan = \DB::table('video_price')->get();
					@endphp
    @include('dashboard/nav')

    @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif

    @if( $errors->any() )
        <div class="alert alert-danger">
            {!! implode('', $errors->all('<div>:message</div>')) !!}
        </div>
    @endif


 <style>.promoted-posts a{color: #f72e5e;}</style>

    <div class="right-all">
        <div class="rows">
          

           <div id="head-title" class="col-lg-12" style="margin-bottom:10px">
                 <div class="dashboard_head">
                @if($type == 'video')
                    @include('dashboard.promote-post.inc.video')
                @elseif($type == 'photo')
                    @include('dashboard.promote-post.inc.photo')
                @elseif( $type == 'story' )
                    @include('dashboard.promote-post.inc.story')
                @endif
                 </div>
            </div>
            <div class="col-12" id="no-show-on-form" style="padding:20px 20px 0 20px;margin:15px;background:#fff;clear:both;overflow:hidden;position:relative;">
                @if( $promoted )
                    <h4 style="margin-top:-4px;text-align:left"><span> Plan: {{ to_amount($promoted->amount) }}/-</span><br>
                    <span> Balance: {{ to_amount($promoted->remain_bal??$promoted->amount) }}/-</span><br>
                    
                        <small style="font-size:12px;margin-top:-1px;display:block">({{ $promoted->days." days ".($promoted->amount/$promoted->click_range)." - ".($promoted->amount/$promoted->view_range) }} estimated range)</small>
                    </h4>
                  
                    <span style="margin: 0px 20px 0 6px;color: #000;background: #eaeaea;width: 170px;float: right;text-align: left;font-size: 12px;padding: 7px 6px 3px 13px;line-height: 17px;position: absolute;right: -14px;top: 8px;text-align: center;"> Total Views : <span id='totalViews'>{{ $promoted->graph['totalViews'] .' * '.$promoted->view_range.' = '.($promoted->view_range*$promoted->graph['totalViews'])}}</span> <br>Total Click : <span id='totalClicks'>{{ $promoted->graph['totalClicks'] .' * '.$promoted->click_range.' = '.$promoted->click_range*$promoted->graph['totalClicks']}}</span></span>
                @endif
                 </div>
                
            <div id="show-canva" style="height:340px; display: block;height: 340px;margin: -5px 15px 15px 15px !important;background: #fff;padding: 20px;">
              
                <canvas id="plan-performance" />
            </div>
            <div style="clear:both;"></div>
            
            
              @if( $promoted )
                @if( (\Carbon\Carbon::parse($promoted->created_at)->timestamp + (86400 * intval($promoted->days))) > \Carbon\Carbon::now()->timestamp && $promoted->status == 1)
                    <div class="col-12" style="margin-bottom: 10px;padding:0 15px">
                        <div style="display:flex; justify-content: flex-end;">
                            <button type="button" id="close-promoted-post" style="background: #f72e5e; border: none;color:white;padding: 5px 15px">Close promoted post</button>
                        </div>
                    </div>
                @endif
                @if(((\Carbon\Carbon::parse($promoted->created_at)->timestamp + (86400 * $promoted->days)) < \Carbon\Carbon::now()->timestamp || $promoted->status == 0))
                    <div class="col-12">
                        <div class="alert alert-info">
                            This plan is already closed. You can create new plan or update the active plan.
                        </div>
                    </div>
                @endif
                {{-- <div class='col-12'>
                    <div class='alert alert-info'>If you degrade your plan amount, then your balance amount will transferred to your wallet.</div>
                </div> --}}
            @endif
            <div style="clear:both;"></div>
            
            
            <div class="col-12">
                <div class="promote-option" style="padding: 10px 15px; display: block">
                    <form id="promote-post-form" action="{{ url( 'promote-post/'.($promoted ? 'edit/'.$promoted->id : 'create')) }}" method="POST">
                        {{ csrf_field() }}
                        @if( !$promoted )
                            <style>
                                #no-show-on-form{display:none;}
                                #show-canva{display:none !Important}
                            </style>
            
                            <div class="option-b" style="width:100%;background: #28b47e;padding: 20px;">
                                <p style="text-align: center;font-size: 17px;margin: 0 0 -13px 0;padding: 5px 0 0 0;color: #fff;">Choose plan as based on yout budget</p>
                                <div class="clear" style="height:15px"></div>
                                <div style="width:50%; float:left">
                                    @if(Route::currentRouteName() == "promotephoto" or Route::currentRouteName() == "promotestory")
                                    <input type="hidden" value="promotephoto" name="promotephoto" />         
                                    @foreach($photoplan as $plan)
                                    <div class="radio">
                                        <label>
                                            <label class="radio">
                                                <input type="radio" name="plan" onclick="check({{json_encode($plan)}})" value="{{$plan->id}}" {{ !$promoted || $promoted->plan_id == 1 ? "checked" : "" }} />
                                                <span class="arrow"></span>
                                            </label> Rs: <?php echo $plan->price ?>/- {{$plan->days}} Days - (800-1200) views and clicks
                                        </label>
                                    </div>
                                    @endforeach
                                    @endif
                                    @if(Route::currentRouteName() == "promotevideo")
                                    <input type="hidden" value="promotevideo" name="promotevideo" />         
                                    @foreach($videoplan as $plan)
                                    <div class="radio">
                                        <label>
                                            <label class="radio">
                                                <input type="radio" name="plan" onclick="check({{json_encode($plan)}})" value="{{$plan->price}}" {{ !$promoted || $promoted->plan_id == 1 ? "checked" : "" }} />
                                                <span class="arrow"></span>
                                            </label> Rs: <?php echo $plan->price ?>/- {{$plan->days}} Days - (800-1200) views and clicks
                                        </label>
                                    </div>
                                    @endforeach
                                     <div class="radio">
                                        <label>
                                            <label class="radio">
                                                <input type="radio" name="plan"  value="custom" />
                                                <span class="arrow"></span>
                                            </label>custom
                                        </label>
                                    </div>
                                    @endif
                                    <div style="display: flex">
                                        <div style="flex:0">
                                            <input type="number" name="custom_amount" id="amount" min="0" step="0.01" placeholder="Type Amount" style="height:46px;padding: 5px 20px 0 20px;line-height: 41px;width: 200px;" value="{{ $promoted && $promoted->plan_id == "custom" ? $promoted->amount : "" }}" />
                                            <div id="amount-error"></div>
                                        </div>
                                        <div style="flex:1">
                                            <select name="custom_days" style="width: 80%;padding: 8px 0 7px 13px;border: solid 1px #e0e1e2;margin-bottom:10px;margin-left: 10px;">
                                                <option>No of days</option>
                                                <option value="3" {{ $promoted && $promoted->plan_id == "custom" && $promoted->days == 3 ? "selected" : "" }}>3 days</option>
                                                <option value="5" {{ $promoted && $promoted->plan_id == "custom" && $promoted->days == 5 ? "selected" : "" }}>5 days</option>
                                                <option value="7" {{ $promoted && $promoted->plan_id == "custom" && $promoted->days == 7 ? "selected" : "" }}>7 days</option>
                                                <option value="15" {{ $promoted && $promoted->plan_id == "custom" && $promoted->days == 15 ? "selected" : "" }}>15 days</option>
                                                <option value="24" {{ $promoted && $promoted->plan_id == "custom" && $promoted->days == 24 ? "selected" : "" }}>24 days</option>
                                                <option value="30" {{ $promoted && $promoted->plan_id == "custom" && $promoted->days == 30 ? "selected" : "" }}>30 days</option>
                                            </select>
                                            <div id="custom-days-error"></div>
                                        </div>
                                    </div>
                                    <h5 style="color: #fff;margin-bottom: 10px;margin-top: 2px;"> No of Views and clicks: <span id="custom-view-click">{{ $promoted && $promoted->plan_id == "custom" ? intval(1 / 0.25 * $promoted->amount) : "" }}</span></h5>
                                </div>
                                <div style="width:50%; float:left">
                                    <div class="radio" style="margin-bottom: 16px;margin-top: 25px;">
                                        <label class="">Country</label>
                                        <select name="country" id="country" style="display: block;width: 86%;padding: 6px 15px 3px 15px;border: solid 1px #e0e1e2;">
                                            <option value="" selected disabled>Choose Country</option>
                                            @if( $countries && count($countries) > 0 )
                                                @foreach($countries as $country)
                                                    <option value="{{ $country->id }}" {{ $promoted && $promoted->country_id == $country->id ? "selected" : "" }} >{{ $country->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div id="country-error"></div>
                                    </div>
                                    <div class="radio" style="margin-bottom:14px; margin-top:12px">
                                        <label>
                                            <label class="radio">
                                                <input type="radio" name="states" value="all" {{ !$promoted || $promoted->state_ids == 'all' ? "checked" : "" }} >
                                                <span class="arrow"></span>
                                            </label> All States
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label style="margin-bottom: 10px;"> 
                                            <label class="radio">
                                                <input type="radio" name="states" value="selected" {{ $promoted && $promoted->state_ids != 'all' ? "checked" : "" }} />
                                                <span class="arrow"></span>
                                            </label> Selected States
                                        </label>   
                                       
    
                                        <select class="form-control js-example-tokenizer" name="selected_states[]" id="selected_states" style="display: block;width: 86%; margin-top:10px; padding: 8px 15px 5px 15px;border: solid 1px #e0e1e2;"  multiple="multiple">
                                            @if( $promoted && $promoted->country_id && $promoted->state_ids != 'all' && $promoted->states )
                                                @php $states = explode(",", $promoted->state_ids); @endphp
                                                @foreach( $promoted->states as $key => $state )
                                                    <option value="{{ $state->id }}" {{ in_array($state->id, $states) ? "selected" : "" }}>{{ $state->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div id="selected-states-error"></div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div class='row'>
                                    <div class="col-sm-4" style="padding:0px;">
                                        <div class="form-block">
                                            <label style="font-weight: normal;margin: 16px 0 6px 0;color: #fff;">Button name (Optional)</label>
                                            <select name="button" style="width:97%;padding: 8px 0 7px 13px;border: solid 1px #e0e1e2;margin-bottom:10px">
                                                <option>Select</option>
                                                <option {{ $promoted && $promoted->button_label == 'Shop Now' ? "selected" : "" }} >Shop Now</option>
                                                <option {{ $promoted && $promoted->button_label == 'Visit Now' ? "selected" : "" }}>Visit Now</option>
                                                <option {{ $promoted && $promoted->button_label == 'Book Now' ? "selected" : "" }}>Book Now</option>
                                                <option {{ $promoted && $promoted->button_label == 'View More' ? "selected" : "" }}>View More</option>
                                                <option {{ $promoted && $promoted->button_label == 'Apply Now' ? "selected" : "" }}>Apply Now</option>
                                                <option {{ $promoted && $promoted->button_label == 'Others' ? "selected" : "" }}>Others</option>
                                            </select>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <label>Button URL</label>
                                        <input type="text" name="button_url" placeholder="Enter Button URL" />
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <input type="hidden" name="type" value="{{ $type }}" />
                                <input type="hidden" name="id" value="{{ $post->id }}" />

                                {{ HTML::style('public/css/bootstrap-tagsinput.css')}}
                                {{ HTML::script('public/js/bootstrap-tagsinput.js')}}
                                {{ HTML::script('public/js/bootstrap3-typeahead.js')}}
                                {{ HTML::script('public/js/bloodhound.js')}}
                                <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
                                <script>
                                
                                 var price =null;
                                       
                                    (async function(){
                                        price = $('input[name="plan"]:checked').val();
                                        let is_paid = false, rzp, order;
                                        const key_id = "{{ env('RAZOR_KEY') }}" ;
                                        
                                        const options = {
                                                key: key_id,
                                                amount: parseInt(price),
                                                currency: "INR",
                                                name: 'Promote Post',
                                                description: "Promote post in Entertainment Bugs",
                                                handler: function(response){
                                                    const form = $("#promote-post-form");
                                                    form.append(`<input type='hidden' name='razorpay_payment_id' value='${response.razorpay_payment_id}' />`);
                                                    form.append(`<input type='hidden' name='razorpay_order_id' value='${response.razorpay_order_id}' />`);
                                                    form.append(`<input type='hidden' name='razorpay_signature' value='${response.razorpay_signature}' />`);

                                                    is_paid = true;
                                                    form.submit();
                                                },
                                                prefill: {
                                                    name: "{{ Session::get('user_name') }}",
                                                },
                                                theme: {
                                                    color: "#ff7529"
                                                },
                                                notes: {
                                                    type: 'promote-post'
                                                }
                                            };

                                        // const razorpay = new Razorpay({
                                        //     key: key_id
                                        // });

                                        async function createOrder(amount){
                                            order = await rzp.orders.create({
                                                amount: amount,
                                                currency: 'INR'
                                            });
                                        }
    
    
                                        async function updateOrder(amount){
                                            return await rzp.orders.edit(order.id, {
                                                amount: amount
                                            });
                                        }

                                        $("#promote-post-form").on("submit", function(e){
                                            const country = $(this).find("[name='country']");
                                            const states = $(this).find("[name='states']:checked");
                                            const selectedStates = $("#selected_states");
                                            const plan = $(this).find("[name='plan']:checked").val();
                                            var plans =[];
                                            if( !country.val() ){
                                                $("#country-error").text("Country is required");
                                                $('html, body').animate({
                                                    scrollTop: $("#country-error").offset().top - 80
                                                }, 300);
                                                return false;
                                            } else {
                                                $("#country-error").text("");
                                            }

                                            if( states.val() == 'selected' )
                                            {
                                                if( !selectedStates.val() || (selectedStates.val() && selectedStates.val().length == 0) ){
                                                    $("#selected-states-error").text("Selected states must be required, if all states is not selected.");
                                                    $('html, body').animate({
                                                        scrollTop: $("#selected-states-error").offset().top - 80
                                                    }, 300);
                                                    return false;
                                                } else {
                                                    $("#selected-states-error").text("");
                                                }
                                            } else {
                                                $("#selected-states-error").text("");
                                            }

                                            if( plan == 'custom' ){
                                                console.log(plan);
                                                if( !$("#amount").val() || parseInt($("#amount").val()) == 0 ){
                                                    $("#amount-error").text("Amount must be greater than 0, if custom plan is selected");
                                                    $('html, body').animate({
                                                        scrollTop: $("#amount-error").offset().top - 80
                                                    }, 300);
                                                    return false;
                                                } else {
                                                    $("#amount-error").text("");
                                                }

                                                const customDays = $(this).find("[name='custom_days']").val();
                                                if( customDays && ![3,5,7,15,24,30].includes(parseInt(customDays)) ){
                                                    $("#custom-days-error").text("Valid custom days must be required, if all states is selected");
                                                    $('html, body').animate({
                                                        scrollTop: $("#custom-days-error").offset().top - 80
                                                    }, 300);
                                                    return false;
                                                } else {
                                                    $("#custom-days-error").text("")
                                                }
                                            } else {
                                                $("#amount-error").text("");
                                                $("#custom-days-error").text("");
                                            }

                                            if( typeof is_paid !== 'undefined' && !is_paid ){
                                                e.preventDefault();
                                            } else {
                                                return true;
                                            }

                                            let amount = 0;
                                            if( plan == "custom" ){
                                                amount = $("#amount").val();
                                            } else if( typeof plans[plan - 1] !== 'undefined' ){
                                                amount = plans[plan - 1];
                                            }

                                            if( !amount ){
                                                amount = parseInt(price);
                                            }

                                            options.amount = amount * 100;
                                            rzp = new Razorpay(options);

                                            rzp.on('payment.failed', function (response){
                                                is_paid = false;
                                            });

                                            rzp.open();
                                            return false;
                                        });
                                    })()
                                </script>

                                <button type="submit" class="btn btn-primary u-btn" style="margin:10px 0 5px 0;background: #f72e5e;color: #fff;padding: 12px 30px;height: 40px;font-weight: 500;">Sumbit</button>
                            </div>
                        @else
                            {{ method_field("PUT") }}
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <style>
#plan-performance{display:none;}
#plan-performance {height: 300px !important;  width: 892px !important;;}
 
.dashboard_head h2 {font-size: 22px;margin: 5px 0 12px 0;}
.dashboard_head {margin: 0 10px 0px 0px;padding: 17px 20px 12px 20px;}
label {color: #fff;}
.radio label {color: #fff;margin-left: -9px;}
</style>
       
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
    <script>
        $(function(){
            @if( $promoted && $promoted->graph )
                // Plan Perfomance Graph
                var planPerformance = document.getElementById("plan-performance");
                planPerformance.height = 200;
                var myChart = new Chart(planPerformance, {
                    type: 'bar',
                    data: {
                        labels: {!! json_encode($promoted->graph['date']) !!},
                        datasets: [
                            {!! json_encode($promoted->graph['views']) !!},
                            {!! json_encode($promoted->graph['clicks']) !!}
                        ],
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            display: false,
                        },
                        scales: {
                            xAxes: [{
                                stacked: false,
                                barPercentage: 0.8,
                                gridLines: {
                                    display: false,
                                    drawBorder: false,
                                },
                                ticks: {
                                    // fontColor: "#8a909d",
                                },
                            }, ],
                            yAxes: [{
                                stacked: false,
                                gridLines: {
                                    display: false,
                                    color: "#eee",
                                },
                                ticks: {
                                    stepSize: 50,
                                    // fontColor: "#8a909d",
                                },
                            }, ],
                        },
                        tooltips: {
                            mode: "index",
                            intersect: false,
                            titleFontColor: "#888",
                            bodyFontColor: "#555",
                            titleFontSize: 12,
                            bodyFontSize: 15,
                            backgroundColor: "rgba(256,256,256,0.95)",
                            displayColors: true,
                            xPadding: 10,
                            yPadding: 7,
                            borderColor: "rgba(220, 220, 220, 0.9)",
                            borderWidth: 2,
                            caretSize: 6,
                            caretPadding: 5,
                        }
                    }
                });
            @else
                $("#amount").on("keypress input", function(){
                    const amount = $(this).val();

                    if( amount ){
                        let viewsAndClick = parseInt(1/0.25 * amount);
                        $("#custom-view-click").text(viewsAndClick);
                    }
                    else {
                        $("#custom-view-click").text('');
                    }
                });

                // const selectedState = $("#selected_states");
                // const bh_states = new Bloodhound({
                //     datumTokenizer: function(d){ return Bloodhound.tokenizers.obj.whitespace(d.name) },
                //     queryTokenizer: Bloodhound.tokenizers.whitespace,
                //     local: [{id: 1, text: "a"}, {id: 3, text: "b"}]
                // });
                // bh_states.initialize();
                // selectedState.tagsinput({
                //     confirmKeys: [13, 44],
                //     maxTags: 3,
                //     itemValue: 'id',
                //     itemText: 'text',
                //     allowDuplicates: false,
                //     typeaheadjs: {
                //         name: 'tags',
                //         valueKey: 'id',
                //         displayKey: 'text',
                //         source: [{id: 1, text: "a"}, {id: 3, text: "b"}]
                //     },
                //     freeInput: false
                // });

                // console.log(bh_states);

                $("#country").on("change", function(){
                    const id = $(this).find("option:selected").text();
                    $("#selected_states").html('');
                    $("#selected_states").val('');
                    // selectedState.tagsinput('removeAll');
                    // selectedState.tagsinput('refresh');
                    // bh_states.clear()

                    if( id ){
                        $.ajax({
                            url: BASE.url("get-states"),
                            method: "POST",
                            headers: {
                                "X-CSRF-TOKEN": $("meta[name='secure_token']").attr("content")
                            },
                            data: { id: id },
                            success: function(response){
                                let states = $("#selected_states");
                                if( response ){
                                    response = JSON.parse(response);
                                    response.forEach(state => {
                                        // selectedState.tagsinput('add', {"value": state.id, "text": state.name})
                                        states.append(`<option value="${state.id}">${state.name}</option>`);
                                        // return {id: state.id, text: state.name}
                                    });
                                    // $("#selected_states").tagsinput('input').typeahead('destroy');
                                    // bh_states.initialize(true);
                                }
                            },
                            error: function(){
                                alert("Something went wrong");
                            }
                        })
                    }
                });

                $("#selected_states").on("change", function(e){
                    const select = $(this).get(0);
                    const selectedOptions = select.selectedOptions;

                    if (selectedOptions.length >= 3) {
                        if( $(this).val() ){
                            $(this).val($(this).val().slice(0, 3));
                        }
                        for (var i = 0; i < select.options.length; i++) {
                            if (!select.options[i].selected) {
                                select.options[i].disabled = true;
                            }
                        }
                    } else {
                        for (var i = 0; i < select.options.length; i++) {
                            select.options[i].disabled = false;
                        }
                    }
                });

                $("#selected_states").trigger('change');
                $("#close-promoted-post").on("click", function(){
                    if( confirm("Do you really want to close the promotion") ){
                        $("#promoted-post-form").append("<input type='hidden' name='action' value='CLOSE' />");
                        $("#promoted-post-form").submit();
                    }
                });
            @endif
        });
    </script>
    
    
 <script>
        $(".js-example-tokenizer").select2({
            multiple: true,
            tokenSeparators: [',', ' ']
        });
    </script>
    
    
         </div>
         </div>   </div>   
         <script>

       
    </script>
@endsection