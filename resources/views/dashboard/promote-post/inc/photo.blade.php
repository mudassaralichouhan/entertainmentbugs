

<div class="image-container">
    @php
        $images = unserialize($post->images);
        if( !empty($images[0]) ){
            $image = $images[0];
        }elseif( !empty($images[1]) ){
            $image = $images[1];
        }elseif( !empty($images[2]) ){
            $image = $images[2];
        }
    @endphp
    <img src="{{ photo_url($image) }}" alt="{{ $post->photo_title }}" style="width: 100%; max-width: 90px;float:left;margin-right:15px; aspect-ratio:3/2; object-fit:cover" >

<h2 class="title">{{ $post->photo_title }}</h2>
<div style="clear:both; overflow:hidden"></div>
</div>