<h2 class="title">{{ $post->title }}</h2>
<div class="video-container">
    <video poster="{{ video_thumb_url($post->thumb->thumb) }}" style="width: 100%; max-width: 400px; aspect-ratio:3/2; object-fit:cover" controls >
        <source src="{{ video_url($post->video) }}" type="video/mp4" />
    </video>
</div>