@php
    $image = unserialize($post->images);
@endphp
<h2 class="title">{{ $post->title }}</h2>
<div class="video-container">
    <img src="{{ photo_url($image[0]) }}" alt="{{ $post->photo_title }}" style="width: 100%; max-width: 400px; aspect-ratio:3/2; object-fit:cover" >
    </video>
</div>