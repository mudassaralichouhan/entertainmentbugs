@extends('layouts.home')

@section('style')
    <style>
        .total-earning a {
            color: #f72e5e;
        }

        .u-layout-cell-4 a.u-btn {
            width: 150px;
        }

        a.second-plan {
            width: 150px;
        }

        .u-align-left {
            width: 24%;
            float: left;
            margin: 0 0.5%;
        }
    </style>
@endsection

@section('content')
    @include('dashboard/nav')
    <div class="right-all">
        <div id="head-title" class="col-lg-12">
            <div class="dashboard_head">
                <h2>Total Earning</h2>
            </div>
        </div>
        <div class="col-lg-12" id="extra-pad-n1">
            <div id="boost-plan" class="tab-content">
                <div class="setting-page" style="background:#fff;padding:10px 10px 20px 10px;margin-top:-5px">

                    <div class="plans-main">
                        <div class="u-clearfix u-expanded-width u-gutter-30 u-layout-wrap u-layout-wrap-2">
                            <div class="u-gutter-0 u-layout">
                                <div class="u-layout-row">
                                    <div
                                            class="u-align-left u-container-style u-layout-cell u-left-cell u-palette-2-base u-radius-15 u-shape-round u-size-20 u-layout-cell-4">
                                        <div class="u-container-layout u-valign-top u-container-layout-4">
                                            <h1 class="u-custom-font u-font-pt-sans u-text u-text-default u-text-6"
                                                style="text-align:center;margin: 0;font-size: 24px;margin-bottom: 12px;">
                                                Video</h1>
                                            <a class="u-btn" style="margin:0 auto;">
                                                Rs: {{ $videoTotalEarning }}/-
                                            </a>
                                        </div>
                                    </div>
                                    <div class="u-align-left" style="background:#eaeaea">
                                        <div class="u-container-layout u-valign-top u-container-layout-5">
                                            <h1 class="u-custom-font u-font-pt-sans u-text u-text-default u-text-palette-2-base u-text-9"
                                                style="text-align:center;margin: 0;font-size: 24px;margin-bottom: 12px;">
                                                Stories</span>
                                            </h1>
                                            <a href="#" class="second-plan"
                                               style="margin:0 auto;   margin: 0;">Rs: {{ $storiesTotalEarning }}/- </a>
                                        </div>
                                    </div>
                                    <div
                                            class="u-align-left u-container-style u-layout-cell u-left-cell u-palette-2-base u-radius-15 u-shape-round u-size-20 u-layout-cell-4">
                                        <div class="u-container-layout u-valign-top u-container-layout-4">
                                            <h1 class="u-custom-font u-font-pt-sans u-text u-text-default u-text-6"
                                                style="text-align:center;margin: 0;font-size: 24px;margin-bottom: 12px;">
                                                Photos</h1>
                                            <a class="u-btn" style="margin:0 auto;">
                                                Rs: {{ $photoTotalEarning }}/-
                                            </a>
                                        </div>
                                    </div>
                                    <div class="u-align-left" style="background:#eaeaea">
                                        <div class="u-container-layout u-valign-top u-container-layout-5">
                                            <h1 class="u-custom-font u-font-pt-sans u-text u-text-default u-text-palette-2-base u-text-9"
                                                style="text-align:center;margin: 0;font-size: 24px;margin-bottom: 12px;">
                                                Rewards</span>
                                            </h1>
                                            <a href="#" class="second-plan"
                                               style="margin:0 auto;   margin: 0;">Rs: {{ $rewardVideosTotalEarning }}
                                                /- </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p style="font-size:14px;line-height: 25px;margin-top: 0px;text-align:center">To increase you
                        revenue. You can promote your post to get better result.
                        <a style="color:#f72e5e" href="{{URL::to('promote-recent-post')}}">
                            Promote your post now
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
