@extends('layouts.home')
@section('content')
    @include('dashboard/nav')
    <style>
    
    .all-dis img{width:100%;}
        .active-ads-campaign a {
            color: #f72e5e;
        }

        #canvas-main-dashboard {
            margin: 0 14px 15px 14px !important;
        }

        #canvas-main-dashboard h2 {
            font-size: 18px;
        }

        #video_up_preview .form-group {
            margin-bottom: 2px;
        }
    </style>
    <div class="right-all">
        @if (Session::has('success'))
            <div class="alert alert-success">
                <span>{{ Session::get('success') }}</span>
            </div>
        @endif
        <div id="head-title" class="col-lg-12">
            <div class="dashboard_head">
                <h2>Active Ads Campaign</h2>
                <p>Your all Active Ads Campaign</p>
            </div>
        </div>
        <div class="col-lg-12" id="extra-pad-n1">

            @if ($ads && $ads->count() > 0)
                @php $postController = new \App\Http\Controllers\PromotePostController; @endphp
                @foreach ($ads as $key => $ad)
                    <div class="col-lg-12" style="padding:0px">
                        <div class="u-details">
                            <div class="row">
                                <div class="col-lg-12">
                                    <span style="background:#f72e5e;color:#fff;padding: 11px 10px 5px 25px;margin: 0px 0 0px 0;display:block;text-align: left;font-size: 15px;">{{ $ad->title }} -- ACTIVE</span>
                                </div>
                                <div class="all-dis"
                                    style="clear: both;overflow: hidden;padding: 20px 10px 0 10px; background: #fff !important;margin: 14px 14px 0px 14px;">
                                    <div class="col-lg-2" style=>
                                        @if( $ad->type == "video" )
                                            <video id="" width="100%" controls="">
                                                <source src="{{ asset('public/ads/videos/'.$ad->file) }}" poster="{{ asset('public/ads/videos/thumb'.$ad->file) }}" type="video/mp4">
                                            </video>
                                        @else
                                            <img src="{{ asset('public/ads/images/'.$ad->file) }}" style="width: {{ $ad->width }}; height: {{ $ad->height }}" />
                                        @endif
                                        <br>
                                    </div>
                                    <div class="col-lg-10">
                                        <div id="video_up_preview">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="e2">About:</label>
                                                    <span>{{ $ad->description }}</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="e1">Video Tags:</label>
                                                    <span>{{ $ad->tags }}</span>
                                                </div>
                                            </div>
                                            
                                                @if( $ad->type == "video" )
                                             <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="e3">Language:</label>
                                                    <span>{{ $ad->language }}</span>
                                                </div>
                                            </div>
                                        @else
                                              
                                        @endif
                                        
                                        
                                          
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="e3">Category:</label>
                                                    <span>{{ $ad->categories }}</span>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label for="e3">State:</label>
                                                    <span>kolkata, delhi</span>
                                                </div>
                                            </div>
                                            <a href="{{ url('edit-ads-campaign/'.$ad->slug) }}" style="background:#f72e5e;color:#fff;padding: 5px 10px 0 10px;float: right;font-size: 14px;margin: -3px 0 0 0;">Edit</a>
                                        </div>
                                    </div>
                                </div>
                                <div id="canvas-main-dashboard" class="my-chart padding-right-my-0" style="height:350px">
                                    <iframe class="chartjs-hidden-iframe" tabindex="-1"
                                        style="display: block; overflow: hidden; border: 0px; margin: 0px; inset: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                    <h2><span> Total Views : 1500 <br>Total Click: 1000</span>Views: 500 Times - Valid: 5
                                        Days - Rs:100/-</h2>
                                    <canvas id="advertise-count-{{ $key }}" height="300" style="display: block; width: 882px; height: 300px;" width="882"></canvas>
                                    @php
                                        $adInfo = $ad->promote_post;
                                        if( $adInfo ){
                                            $graph = $postController->getGraph($adInfo->id);
                                        } else {
                                            $graph = [
                                                'views' => [0],
                                                'clicks' => [0],
                                                'total' => 0,
                                                'date'  => []
                                            ];
                                        }
                                    @endphp
                                    <script>
                                        $(function(){
                                            let planPerformance = document.getElementById("advertise-count-{{ $key }}");
                                            planPerformance.height = 200;
                                            let myChart = new Chart(planPerformance, {
                                                type: 'bar',
                                                data: {
                                                    labels: {!! json_encode($graph['date']) !!},
                                                    datasets: [
                                                        {!! json_encode($graph['views']) !!},
                                                        {!! json_encode($graph['clicks']) !!}
                                                    ],
                                                },
                                                options: {
                                                    responsive: true,
                                                    maintainAspectRatio: false,
                                                    legend: {
                                                        display: false,
                                                    },
                                                    scales: {
                                                        xAxes: [{
                                                            stacked: false,
                                                            barPercentage: 0.8,
                                                            gridLines: {
                                                                display: false,
                                                                drawBorder: false,
                                                            },
                                                            ticks: {
                                                                // fontColor: "#8a909d",
                                                            },
                                                        }, ],
                                                        yAxes: [{
                                                            stacked: false,
                                                            gridLines: {
                                                                display: false,
                                                                color: "#eee",
                                                            },
                                                            ticks: {
                                                                stepSize: 50,
                                                                // fontColor: "#8a909d",
                                                            },
                                                        }, ],
                                                    },
                                                    tooltips: {
                                                        mode: "index",
                                                        intersect: false,
                                                        titleFontColor: "#888",
                                                        bodyFontColor: "#555",
                                                        titleFontSize: 12,
                                                        bodyFontSize: 15,
                                                        backgroundColor: "rgba(256,256,256,0.95)",
                                                        displayColors: true,
                                                        xPadding: 10,
                                                        yPadding: 7,
                                                        borderColor: "rgba(220, 220, 220, 0.9)",
                                                        borderWidth: 2,
                                                        caretSize: 6,
                                                        caretPadding: 5,
                                                    }
                                                }
                                            });
                                        })
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
@endsection
