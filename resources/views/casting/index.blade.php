@extends('layouts.home')
@section('meta_tags')
 
    <meta name="description" content="We make casting for your next project easy. Find the best talents from the list of available artists for the next character you are casting for. Register as a casting agency now.">
    <meta name="keywords" content="Casting, Artist Gallery, Artist Video, Artist Profile, Production House, Production, Agency, Auditions, Production firm, Find Artist, Search Artist">
    <meta name="author" content="<?php echo ((isset($author) ? $author : 'Production House - Entertainment Bugs')); ?>">
    <meta property="og:site_name" content="{{URL::to('/')}}"/>
    <meta property="og:locale" content="en_US"/>



<meta property="og:image" content="<?php echo ((isset($ogimage) ? $ogimage : 'https://www.entertainmentbugs.com/public/uploads/logo/logo-social-media.png')); ?>"/><!--profile image-->
<meta property="og:title" content="<?php echo ((isset($ogtitle) ? $ogtitle : 'Casting - Entertainment Bugs')); ?>/webpage"/>
<meta property="og:description" content="<?php echo ((isset($ogdescription) ? $ogdescription : 'We make casting for your next project easy. Find the best talents from the list of available artists for the next character you are casting for. Register as a casting agency now.')); ?>/webpage"/>
<meta property="og:url" content="<?php echo url()->current(); ?>/webpage"/>



<meta name="theme-color" content="#f82e5e">
<meta property="og:keywords" content="Casting, Artist Gallery, Artist Video, Artist Profile, Production House, Production, Agency, Auditions, Production firm, Find Artist, Search Artist/webpage">


   @endsection
   
   
   
@section('content')
    <link href="{{ asset('public/css/casting.css') }}" rel="stylesheet">

    <style>
        #live-video-user-list-home {
            display: none
        }

.casting_active i{    color: #f72e5e !important;}
.casting_active span{    color: #f72e5e !important;}

        #casting-home-page-audition-block p.prd-info {
            height: 36px;
            overflow: hidden;
        }

        #casting-home-page-audition-block .production-all-display {
            min-height: 175px;
        }

        #right-side-bar h5 {
        margin: 14px 0 7px 0;
    float: left;
    width: 70%;
    line-height: 15px;
    height: 43px;
    overflow: hidden;
        }

        p.cat-over {
            text-overflow: ellipsis;
            white-space: nowrap;
            display: block;
            width: 100%;
            overflow: hidden;
        }

        .photo-block {
            position: relative;
        }

        .photo-block a:hover .info-p {
            opacity: 1;
        }

        .info-p {
            opacity: 0;
            position: absolute;
            top: 0px;
            width: 95%;
            height: 100%;
            background: rgba(0, 0, 0, 0.5);
        }

        .info-p h5 {
            color: #fff;
        }

        .info-p p {
            color: #fff;
            text-overflow: ellipsis;
            white-space: nowrap;
            display: block;
            width: 100%;
            overflow: hidden;
            font-weight: normal;
        }

        #result-block-main .photo-block h5 {
            margin: 30px 0 10px 0;
            font-size: 12px;
            font-weight: normal;
        }


        .row.home_page_second_block .col-lg-2 {
            margin-bottom: 0;
        }
    </style>


    <div class="content-wrapper menu-top1">
        <?php
                         $userid=session()->get('user_id');
     if($userid != ''){
            $artist_about=\App\Models\ArtistAbout::where(['user_id'=>$userid,'isDeleted'=>0])->get();
                    $progress = 0;
                 if($artist_about->count() > 0 ){
                     $progress = 1;
                 }
                 if($progress){
                     ?>
        <style>
            #user-menu {
                display: none
            }
        </style>

        <div class="containers" id="">
            <div class="casting-menu">
                <ul>
                    <li><a href="{{ URL::to('artist-profile') }}"> Artist Profile</a></li>
                    <li><a href="{{ URL::to('artist-videos') }}"> Artist Videos</a></li>
                    <li><a href="{{ URL::to('artist-gallery') }}"> Artist Gallery</a></li>
                    <li><a href="https://www.entertainmentbugs.com/artist-profile?page=1&category[]=New%20Influncer&category[]=Experienced%20Influncer&gender=Select&city=&followers=Select&min_experience=Min:&max_experience=Max:&min_age=&max_age=&min_height=&max_height=&min_weight=&max_weight=&artist_name=">Influencer</a></li>
                    <li><a href="https://www.entertainmentbugs.com/artist-profile?page=1&category[]=Actor&category[]=Comedy%20Actor&category[]=Model&category[]=Pro%20Model&gender=Select&city=&followers=Select&min_experience=Min:&max_experience=Max:&min_age=&max_age=&min_height=&max_height=&min_weight=&max_weight=&artist_name=">Actor / Model</a></li>
                    <!--<li><a href="{{ URL::to('production-houses') }}">Production House</a></li>-->
                    <li><a href="{{ URL::to('find-auditions') }}">Auditions</a></li>
                </ul>
            </div>
        </div>
        <?php 
                 } 
                 
                     $product_about=\App\Models\ProductionAbout::where(['user_id'=>$userid,'isDeleted'=>0])->get();
                     
                $progress = 0;
                if( $product_about->count() > 0){
                     $progress = 1;
                 }
                if($progress){
                ?>
        <style>
            #user-menu {
                display: none
            }
        </style>


        <?php 
                }
            }//check if user login ?>

        <div class="containers" id="user-menu">
            <div class="casting-menu">
                <ul>
                    <li><a href="{{ URL::to('artist-profile') }}"> Artist Profile</a></li>
                    <li><a href="{{ URL::to('artist-videos') }}"> Artist Videos</a></li>
                    <li><a href="{{ URL::to('artist-gallery') }}"> Artist Gallery</a></li>
                    <li><a href="https://www.entertainmentbugs.com/artist-profile?page=1&category[]=New%20Influncer&category[]=Experienced%20Influncer&gender=Select&city=&followers=Select&min_experience=Min:&max_experience=Max:&min_age=&max_age=&min_height=&max_height=&min_weight=&max_weight=&artist_name=">Influencer</a></li>
                    <li><a href="https://www.entertainmentbugs.com/artist-profile?page=1&category[]=Actor&category[]=Comedy%20Actor&category[]=Model&category[]=Pro%20Model&gender=Select&city=&followers=Select&min_experience=Min:&max_experience=Max:&min_age=&max_age=&min_height=&max_height=&min_weight=&max_weight=&artist_name=">Actor / Model</a></li>
                    <li><a href="{{ URL::to('production-houses') }}">Production House</a></li>
                    <li><a href="{{ URL::to('find-auditions') }}">Find Auditions</a></li>
                    <li><a href="{{ URL::to('post-audition') }}">Post Auditions</a></li>
                </ul>
            </div>
        </div>
    </div>
   
 
    <?php
                         $userid=session()->get('user_id');
     if($userid != ''){
            $artist_about=\App\Models\ArtistAbout::where(['user_id'=>$userid,'isDeleted'=>0])->get();
            
                    $progress = 0;
                 if($artist_about->count() > 0 ){
                     $progress = 1;
                 }
                 if($progress){
                     ?>

    <style>
        #casting-top-banner {
            display: none
        }
    </style>



    <?php 
                 } 
                 
                     $product_about=\App\Models\ProductionAbout::where(['user_id'=>$userid,'isDeleted'=>0])->get();
                $progress = 0;
                if( $product_about->count() > 0){
                     $progress = 1;
                 }
                if($progress){
                ?>
    <style>
        #casting-top-banner {
            display: none
        }
    </style>





    <?php 
                }
            }//check if user login ?>


    <div id="casting-top-banner" class="banner">
        <div class="intro-banner main_banner">
            <div class="containers">
                <!-- Intro Headline -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="banner-headline-alt">
                            <h4>Create your profile as an artist </h4>
                            <h3>Promote your talent and regiter with us</h3>
                            <p>Join and be visible to the production houses, ad makers and casting agencies. </p>
                            <a href="./create-an-artist-profile" class="button"
                                style="border-color:#6c146b; color:#6c146b;width: 240px;">Create Profile</a>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="banner-headline-alt">
                            <h4>Production House</h4>
                            <h3> Create Your profile as a production house</h3>
                            <p>Join us and find the best talent for your upcoming projects. </p>
                            <a href="./artist" class="button" style="border-color:#6c146b; color:#6c146b;width: 240px;">
                                Find Artist</a>
                        </div>
                    </div>
                </div>
                <!-- Search Bar -->
            </div>
        </div>
    </div>



    <div class="audition_main_list" id="audition_main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Featured Videos -->
                    <div class="content-block">
                    </div>
                    <!--casting video END-->

                    @include('casting.inc.influencer')


                    <div style="position: relative;">
                        <h2 class="middle-head">Modelling</h2>
                        <a href="./artist-profile"
                            style="position:absolute;right:-12px;top:0px; color:#fff; padding:4px 15px 0 15px; background: #f72e5e !important;">View
                            all modelling</a>
                    </div>

                    @include('casting.inc.modeling')
                    @include('casting.inc.latest-artist-videos')


                    @include('casting.inc.dancer')

                    @include('casting.inc.latest-production-house')
                    @include('casting.inc.auditions')
                </div>
            </div>
            <!-- /Featured Videos -->
        </div>
    </div>
    </div>
    </div>



    <!-- Owl Carousel -->
    <style>
        #owl-demo .item {
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            text-align: center;
        }

        .customNavigation {
            text-align: center;
        }

        .customNavigation a {
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }
    </style>
    <script>
        $(document).ready(function() {

            var owl = $("#owl-demo");

            owl.owlCarousel({

                items: 15, //10 items above 1000px browser width
                itemsDesktop: [1000, 5], //5 items between 1000px and 901px
                itemsDesktopSmall: [900, 3], // 3 items betweem 900px and 601px
                itemsTablet: [600, 2], //2 items between 600 and 0;
                itemsMobile: false // itemsMobile disabled - inherit from itemsTablet option

            });

            // Custom Navigation Events
            $(".next").click(function() {
                owl.trigger('owl.next');
            })
            $(".prev").click(function() {
                owl.trigger('owl.prev');
            })
            $(".play").click(function() {
                owl.trigger('owl.play', 1000);
            })
            $(".stop").click(function() {
                owl.trigger('owl.stop');
            })


        });
    </script>
@endsection
