 <div style="position:relative">
     <h2 class="middle-head">Auditions</h2>
     <a href="auditions.html" style="position:absolute;right:-12px;top:0px; color:#fff; padding:4px 15px 0 15px; background:#f72e5e !important">View All</a>

     <div class="row">
         <div style="background:#fff;clear: both;overflow: hidden;margin:15px 0 0 0">
             <div class="col-lg-12" style="padding:0px 5px">
                 <!-- Popular Channels -->
                 <div class="content-block" id="casting-home-page-audition-block">
                     @foreach ($post_auditions as $audition)
                        @php
                             $production=\App\Models\ProductionAbout::where(['user_id'=>$audition->user_id,'isDeleted'=>0])->get();
                        @endphp
                        @if(count($production)>0)
                         <div class="production-all-display" id="right-side-bar">
                             <div class="production-hs1">
                                 <a href="{{ url("audition/".$audition->slug) }}" class="ic">
                                     <img src="{{ $production[0]->profile_photo }}">
                                 </a>
                                 <h5>{{ $audition->title }}</h5>
                                 <div class="clear"></div>
                             </div>
                             <div class="production-hs2">
                                 <p class="prd-info"> {{ $audition->description }}</p>
                                 <div class="sp1" id="auditions">
                                     <p class="cat-over"><span><i class="fa fa-list-alt"></i> Category: </span>
                                         {{ $audition->category }}</p>
                                     <div class="clear"></div>
                                     <p class="cat-over"><span><i class="fa fa-map-marker"></i> Location: </span>
                                         {{ $audition->city }}-{{ $audition->state }}</p>
                                     <div class="clear"></div>

                                     <p><span><i class="fa fa-calendar-check-o"></i> Valid Till:
                                             {{ \Carbon\Carbon::parse($audition->posted_date)->isPast() ? 'Expired' : \Carbon\Carbon::parse($audition->posted_date)->diffForHumans() }}</span>
                                     </p>
                                 </div>
                                 @if (!\Carbon\Carbon::parse($audition->posted_date)->isPast())
                                     <a href="{{ url("audition/$audition->slug") }}" class="view-m">View Now</a>
                                 @endif
                                 <div class="clear"></div>
                             </div>
                             <div class="clear"></div>
                         </div>
                        @endif
                     @endforeach
                 </div>
             </div>
             <!-- /Popular Channels -->
         </div>
     </div>

 </div>
