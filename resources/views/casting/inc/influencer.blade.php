          <div style="position: relative;">
              <h2 class="middle-head">Influencer</h2>
              <a href="./artist-profile"
                  style="position:absolute;right:-12px;top:0px; color:#fff; padding:4px 15px 0 15px; background: #f72e5e !important;">View all Influencer</a>
          </div>
          <div class="row home_page_seventh_block  casting-home-sep">
              <div class="col-lg-12">
                  <!-- Popular Channels -->
                  <div class="content-block">
                      <div class="cb-content chanels-row" id="result-block-main">

                          @foreach ($artist_cat_influencer as $video)
                              <div class="photo-block">
                                  <a href="{{ $video->username ? url("artist/$video->username") : "#" }}" class="profile-p">
                                      <img src="{{ $video->profile_photo }}">

                                      <div class="info-p">
                                          <h5>{{ $video->profile_title }}</h5>
                                          <p>Height: {{ $video->height }}</p>
                                          <p><i class="fa fa-language" aria-hidden="true"></i> {{ $video->language }}
                                          </p>
                                          <p><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $video->city }} - {{ $video->state }}</p>
                                      </div>
                                  </a>
                              </div>
                          @endforeach

                      </div>
                  </div>
                  <!-- /Popular Channels -->
              </div>
          </div>
