 <div style="position:relative">
     <h2 class="middle-head">Find Latest Artist videos</h2>
     <a href="./artist-videos" style="position:absolute;right:-12px;top:0px; color:#fff; padding:4px 15px 0 15px; background: #f72e5e !important;">View all</a>

     <!-- casting video start-->
     <div class="row home_page_second_block" style="padding-top:30px;">
         <div class="col-lg-12">
             <div class="cb-content videolist" id="home_page_video">
                 @foreach ($latest_videos as $video)
                     <div class="col-lg-2 col-sm-6 col-sm-3 videoitem">
                         <div class="user">
                             <a href="{{ isset($video->aboutAuthor) && $video->aboutAuthor->username ? url('artist/' . $video->aboutAuthor->username) : '#' }}" class="user_img">
                                @if (isset($video->aboutAuthor) && $video->aboutAuthor->profile_photo)
                                    <img src="{{ $video->aboutAuthor->profile_photo }}" />
                                @else
                                    <span class="shortname" style="display: inline-block">{{ name_to_pic( isset($video->aboutAuthor) ? $video->aboutAuthor->profile_name : '') }}</span>
                                @endif
                             </a>
                         </div>
                         <div class="b-video">
                             <div class="v-img">
                                 <a href="{{ isset($video->aboutAuthor) && $video->aboutAuthor->username ? url($video->aboutAuthor->username.'/video/' . $video->slug) : "#" }}"><img src="{{ $video->thumb }}" alt=""></a>
                                 <div class="time">3:50</div>
                             </div>
                             <div class="v-desc">
                                 <p>{{ $video->category }}</p>
                                 <a href="{{ $video->video }}">{{ $video->video_title }}</a>
                             </div>
                         </div>
                     </div>
                 @endforeach
             </div>
         </div>
     </div>
 </div>
