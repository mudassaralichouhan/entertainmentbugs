 <div class="row" style="position:relative; margin-top:20px;">
     <img src="https://onthemarcmedia.com/wp-content/uploads/2014/08/video-production-banner.png" style="width:100%">
     <div class="top-banner" style="left: 45px;">
         <h2 style="font-size:32px">Get cast and find artist for your new movies and web series. </h2>
         <a href="">Register as a production house</a>
     </div>
 </div>
 <div style="position:relative">
     <h2 class="middle-head">Trending Production House</h2>
     <a href=""
         style="position:absolute;right:-12px;top:0px; color:#fff; padding:4px 15px 0 15px; background: #f72e5e  !important;">View
         All</a>
     <div class="row home_page_seventh_block  casting-home-sep">
         <div class="col-lg-12">
             <!-- Popular Channels -->
             <div class="content-block">
                 <div class="cb-content chanels-row" id="result-block-main">
                     @foreach ($trending_production_houses as $prodhouse)
                         <div class="casting-agency-main">
                             <div class="casting-agency-photo-block">
                                 <a href="{{ url('production-house/' . $prodhouse->username) }}">
                                     <img src="{{ $prodhouse->profile_photo }}">
                                 </a>
                             </div>
                             <div class="casting-agency-info">
                                 <a href="{{ url('production-house/' . $prodhouse->username) }}">
                                     <h5>{{ $prodhouse->name }}</h5>
                                 </a>
                                 <p>{{ $prodhouse->city }} - {{ $prodhouse->state }}</p>
                             </div>
                             <div class="clear"></div>
                         </div>
                     @endforeach
                     <div class="clear"></div>
                 </div>

             </div>
         </div>
         <!-- /Popular Channels -->
     </div>
 </div>
