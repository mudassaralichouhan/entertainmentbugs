  <h2 class="middle-head">Dancer Audition Video</h2>
                     <!-- casting video start-->
                     <div class="row home_page_second_block casting-home-sep">
                        <!-- Featured Videos -->
                        <div class="content-block">
                           <style>
                              .row.home_page_second_block.casting-home-sep .videolist{margin-bottom:5px}
                              #blog .col-lg-2{width:100% !important}
                           </style>
                           <div class="cb-content videolist" id="home_page_video">
                              <div id="blog" class="owl-carousel company-post">
                              @foreach($dancer_videos as $video)
                                 <div class="company">
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                       <div class="b-video">
                                          <div class="v-img">
                                             <a href="{{ url(isset($video->user) && $video->user->slug."/video/".$video->slug) }}"><img src="{{$video->thumb}}" alt=""></a>
                                             <div class="time">3:50</div>
                                          </div>
                                          <div class="sv-views-progress"> </div>
                                          <div class="v-desc">
                                             <a href="./artist-videos">{{$video->video_title}}</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              @endforeach
                              </div>
                           </div>
                        </div>
                        <!-- /Featured Videos -->
                     </div>
                     <!--casting video END-->