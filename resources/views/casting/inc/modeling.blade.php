<div class="row home_page_seventh_block  casting-home-sep">
    <div class="col-lg-12">
        <!-- Popular Channels -->
        <div class="content-block">
            <div class="cb-content chanels-row" id="result-block-main">
                @foreach ($artist_cat_modeling as $video)
                    <div class="photo-block">
                        <a href="{{ $video->username ? url('artist/' . $video->username) : '' }}" class="profile-p">
                            <img src="{{ $video->profile_photo }}" />
                            <div class="info-p">
                                <h5>{{ $video->profile_title }}</h5>
                                <p>Height: {{ $video->height }}</p>
                                <p><i class="fa fa-language" aria-hidden="true"></i> {{ $video->language }}</p>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $video->state }} - {{ $video->city }}</p>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <!-- /Popular Channels -->
    </div>
</div>
