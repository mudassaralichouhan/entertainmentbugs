<div class="casting-search-result">
    @include('production-house.inc.production-houses-filter-info')
    <div class="row home_page_seventh_block" style="margin:0px;">
        <div class="col-lg-12">
            <!-- Popular Channels -->
            <div class="content-block">
                <div id="production-house-main">
                    @foreach ($prductionHouses as $houses)
                        <div class="production-all-display">
                            <div class="production-hs1">
                                <a href="{{ url("production-house/".$houses->username) }}">
                                    <img src="{{ $houses->profile_photo }}">
                                </a>
                            </div>
                            <div class="production-hs2">
                                <h5>{{ $houses->name }}</h5>
                                <p class="prd-info">{{ $houses->about }}</p>
                                <div class="sp1" id="auditions">
                                    <p class="cat-over"><span><i class="fa fa-list-alt"></i> Category: </span>
                                        {{ $houses->multiple_category }}</p>
                                    <p class="cat-over"><span><i class="fa fa-map-marker"></i> Location: </span>
                                        {{ $houses->city }}- {{ $houses->state }} - {{ $houses->country }} </p>
                                    <p class="cat-over"><span><i class="fa fa-language"></i> Language: </span>
                                        {{ $houses->language }}</p>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    @endforeach
                </div>
                {{ $prductionHouses->links() }}
            </div>
        </div>
        <!-- /Popular Channels -->
    </div>
</div>
