@extends('layouts.home')
@section('content')
<style>#live-video-user-list-home{display:none}</style>
<style>
    .progress-bar1, .progress-bar2,.progress-bar3,.progress-bar4,.progress-bar5,.progress-bar6{
        float: left;
        width: 0%;
        height: 100%;
        font-size: 14px;
        line-height: 22px;
        color: #fff;
        text-align: center;
        background-color: #337ab7;
        -webkit-box-shadow: inset 0 -1px 0 rgb(0 0 0 / 15%);
        box-shadow: inset 0 -1px 0 rgb(0 0 0 / 15%);
        -webkit-transition: width .6s ease;
        -o-transition: width .6s ease;
        transition: width .6s ease;
    }
    #create-profile{margin:40px 0}
    #top-check-field0 h2{font-size:22px; margin:0 0 0 0}
    .option-block{float:left; width:33.3%}
    .field strong{margin-top:10px;display: block;font-weight:600;font-family: 'Hind Guntur', sans-serif;}
    #input-field0 label{font-weight:500;font-family: 'Hind Guntur', sans-serif;margin:0px 0 3px 0;}
    #input-field0 .form-group {margin-bottom: 18px;}
    #input-field0 textarea {padding-top: 10px;font-size: 13px;}
    #what-we-de-bl label{font-weight:600;font-family: 'Hind Guntur', sans-serif;margin:0px 0 3px 0;}
    #what-we-de-bl .form-group {margin-bottom: 18px;}
    #what-we-de-bl  textarea {padding-top: 10px;font-size: 13px;}
    #what-we-de-bl h2{font-size:22px; margin:0 0 20px 0}
    #input-field0 h2{font-size:22px; margin:0 0 20px 0}
    #portfolio-block h2{font-size:22px; margin:0 0 20px 0}
    #portfolio-block label {font-weight: 600;font-family: 'Hind Guntur', sans-serif;margin: 0px 0 3px 0;}
    textarea {padding-top: 0;margin: 0 0 -10px 0;}
    #showreel-block .form-group {margin-bottom: 10px;}
    .two.fields{margin-bottom:20px;clear:both; overflow:hidden;}
    .btn-cv1 {padding: 11px;}
    #select-btn5 h2{    margin-bottom: 0;font-weight: 500;}
    .tab4{background: #eceff0; margin-bottom:0px;clear:both; overflow:hidden;} 
    .tab4 a {padding: 10px 20px;text-align: Center;color: #000;line-height: 45px;}
    .tab4 a.active{    background: #28b47e !important;color:#fff;}
    .w3-green, .w3-hover-green:hover {color: #fff!important;background-color: #f72e5e !important;text-align: Center;line-height: 17px;padding: 3px 0 0 0;}
    .w3-light-grey {color: #000!important;margin-top:8px;background-color: #eceff0 !important;}
    .form-group .form-control{padding:10px 20px;}
    select {border:solid 1px #e0e1e2}   
    .edit-page .u-area {text-align: center;padding-top: 12px;padding-bottom: 20px;}
    #select-p i{color: #637076;font-size: 36px;text-align: center;display: block;padding: 32px 0 0 0;}
    .upload-page .u-area i {font-size: 90px;color: #637076;}
    .upload-page .u-area .u-text1 {margin-top: 10px;margin-bottom: 9px;font-size: 14px;}
    #portfolio-block .browse-photo{width:100px; height:100px; background:#eceff0}
    #portfolio-block .form-group {margin-bottom: 10px;}
    #portfolio-block .col-lg-6{margin:5px 0 20px 0;}
    #portfolio-block .btn-save{margin-top:10px;}
    #showreel-block h2{font-size:22px; margin:0 0 20px 0}
    #showreel-block label {font-weight: 600;font-family: 'Hind Guntur', sans-serif;margin: 0px 0 3px 0;}
    #portfolio-block .progress {width: 90%;margin-bottom: 0;height: 8px;background-color: #eceff0;-webkit-box-shadow: none;box-shadow: none;}
    #portfolio-block .cgt{position: absolute;top: -6px;right: 21px;}
    .u-progress{position:relative; margin-top:10px;}
    .tabcontent h2 {  font-size: 22px;margin: 0 0 20px 0;}
    .tab {overflow: hidden;border: 1px solid #ccc;background-color: #f1f1f1;}
    .tab button {background-color: inherit;float: left;border: none;outline: none;cursor: pointer;padding: 16px 20px 10px 20px;transition: 0.3s;font-size: 14px;}
    .tab button:hover {background-color: #ddd;}
    .tab button.active {background-color: #ccc;}
    .tabcontent {padding: 6px 12px;border: 1px solid #ccc;border-top: none;display:none;}
    .tabcontent{margin-top:10px;}
    label {margin-bottom: 10px;font-weight: 500;}
</style>
<!--Production house Start-->
<div class="content-wrapper">
    <div class="audition_main_list" id="auditions" style="background:#f4f3f3">
        <div id="create-profile-tab">
            <div class="container">
                <div class="rows" style="background:#fff;padding:0px; margin-top:10px">
                    <div class="tab tab4">
                        <button class="tablinks active" onclick="openCity(event, 'about')">About</button>
                        <button class="tablinks" onclick="openCity(event, 'what-we-de-bl')">What we do</button>
                        <button class="tablinks" onclick="openCity(event, 'latest-project')">Latest project</button>
                        <button class="tablinks" onclick="openCity(event, 'awards')">Awards</button>
                        <button class="tablinks" onclick="openCity(event, 'portfolio')">My Gallery</button>
                        <button class="tablinks" onclick="openCity(event, 'video')">Video</button>
                        <button class="tablinks" onclick="openCity(event, 'team')">My Team</button> 
                        <button class="tablinks" onclick="openCity(event, 'contact')">Contact</button> 
                        <?php 
                            $username = session()->get('user_name');
                        ?>
                        <?php 
                            $userId = session()->get('user_id');
                            $getIfAlready = DB::select('select * from production_about where user_id='.$userId);
                        ?>
                        <a target="_blank" href="{{URL::to('production-house/')}}/{{ $getIfAlready[0]->username ?? '' }}" style="float: right;width: 150px;margin: 0;padding: 0 10px;background: #28b47e !important;color: #fff;" target="_blank">Preview Profile</a>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="w3-light-grey">
                    <?php
                        $userid=session()->get('user_id');
                        
                        $artist_about=DB::select('select * from production_about where user_id='.$userid);
                        $artist_what_we_do=DB::select('select * from production_what_we_do where user_id='.$userid);
                        $artist_achievment=DB::select('select * from production_awards where user_id='.$userid);
                        $production_my_team=DB::select('select * from production_my_team where user_id='.$userid);
                        $artist_gallery=DB::select('select * from production_gallery where user_id='.$userid);
                        $artist_video=DB::select('select * from production_video where user_id='.$userid);
                        $artist_project=DB::select('select * from production_latest_project where user_id='.$userid);
                        $artist_contact=DB::select('select * from production_contact_us where user_id='.$userid);
                        $progress = 0;
                        if( count($artist_about) > 0){
                        $progress += 12.5;
                        }
                        if(count($artist_what_we_do) > 0){
                        $progress += 12.5;
                        }
                        if(count($artist_achievment) >0){
                        $progress += 12.5;
                        }
                        if(count($production_my_team ) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_gallery ) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_video ) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_project ) >0){
                        $progress += 12.5;
                        }
                        if(count($artist_contact ) >0){
                        $progress += 12.5;
                        }
                        ?>
                    <div class="w3-container w3-green w3-center" style="width:<?php echo $progress; ?>%"><?php echo $progress; ?>%</div>
                </div>
            </div>
            <div id="about" style="display:block" class="tabcontent">
                <div class="container">
                    <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <h2 style="margin-bottom: 3px;">About -  Production House</h2>
                        <?php 
                            function limit_text($text, $a, $limit) {
                                if (str_word_count($text, 0) > $limit) {
                                    $words = str_word_count($text, 2);
                                    $pos   = array_keys($words);
                                    $text  = substr($text, 0, $pos[$limit]);
                                }
                                return $text;
                            }
                            $userId = session()->get('user_id');
                            $getIfAlready = DB::select('select * from production_about where user_id='.$userId);
                            ?>
                        <p>Create your profile as a Production House</p>
                        @if($message = Session::get('success')) <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </a>
                        <strong>Success!</strong> {{ $message }}
                      </div> @endif {!! Session::forget('success') !!}
                        <form action="{{URL::to('save-production-profile')}}" id="productionForm" style="margin-top:0px" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="cropImage" id="cropImage" value="">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="form-group  col-lg-4 col-sm-6 ">
                                        <label> Name </label>   
                                        <input type="text" required name="name" class="form-control" id="exampleInputEmail1" 
                                            placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->name != NULL) ? $getIfAlready[0]->name : '') : '')); ?>">
                                        <input type="hidden" name="current_user" value="{{ $getIfAlready[0]->username ?? '' }}" /> 

                                    </div>
                                    <div class="form-group col-lg-4 col-sm-6">
                                        <label> Language* (Multiple) all global language </label>   
                                        <input type="text" class="form-control" id ='video-language' name="language" required placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->language != NULL) ? $getIfAlready[0]->language : '') : '')); ?>">
                                    </div>
                                 
                                    <div class="form-group col-lg-4 col-sm-6">
                                        <label> User Name* </label>   
                                        <input type="text" required name="username" class="form-control" id="" placeholder="This username will be your profile url." value="{{ $getIfAlready[0]->username ?? '' }}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-4 col-sm-6">
                                        <label for="exampleInputEmail1">Country* </label>
                                        <div class="clear"></div>
                                        <select id="country" name="country" required onchange="getStates(this.value)" class="select2 input-select" style="width:100%">
                                            <option value="">Select Country</option>
                                            <?php
                                                $getCountries = DB::table('tbl_countries')->get();
                                                foreach($getCountries as $k=>$v)
                                                {
                                                ?>
                                            <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->country != NULL) ? (($getIfAlready[0]->country == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4 col-sm-6">
                                        <label>Location* (State)  </label>    
                                        <select class="select2 input-select" style="width:100%" onchange="getCities(this.value)" required id="state" name="state">
                                            <option value="">Select State</option>
                                            <?php
                                                if(count($getIfAlready) > 0)
                                                {
                                                    if($getIfAlready[0]->country != NULL || $getIfAlready[0]->country != '')
                                                    {
                                                        $getCountryId = DB::table('tbl_countries')->where('name',$getIfAlready[0]->country)->get();
                                                        $getStates = DB::table('tbl_states')->where('country_id',$getCountryId[0]->id)->get();
                                                        foreach($getStates as $k=>$v)
                                                        {
                                                ?>
                                            <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->state != NULL) ? (($getIfAlready[0]->state == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                                            <?php } ?>
                                            <?php } } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4 col-sm-6">
                                        <label> City* </label>  
                                        <select class="select2 input-select" style="width:100%" required id="city" name="city">
                                            <option value="">Select City</option>
                                            <?php
                                                if(count($getIfAlready) > 0)
                                                {
                                                    if($getIfAlready[0]->state != NULL || $getIfAlready[0]->state != '')
                                                    {
                                                        $getStateId = DB::table('tbl_states')->where('name',$getIfAlready[0]->state)->get();
                                                        $getCities = DB::table('tbl_cities')->where('state_id',$getStateId[0]->id)->get();
                                                        foreach($getCities as $k=>$v)
                                                        {
                                                ?>
                                            <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->city != NULL) ? (($getIfAlready[0]->city == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                                            <?php } ?>
                                            <?php } } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="form-group"> <label for="exampleInputEmail1">Facebook*</label>  
                                            <input type="text" class="form-control" name="facebook" required 
                                                placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->facebook != NULL) ? $getIfAlready[0]->facebook : '') : '')); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"> <label for="exampleInputEmail1">Instagram*</label> 
                                            <input type="text" class="form-control" name="instagram" required 
                                                placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->instagram != NULL) ? $getIfAlready[0]->instagram : '') : '')); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"> <label for="exampleInputEmail1">Youtube*</label> 
                                            <input type="text" class="form-control" name="youtube" required 
                                                placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->youtube != NULL) ? $getIfAlready[0]->youtube : '') : '')); ?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"> <label for="exampleInputEmail1">Linkedin*</label>  
                                            <input type="text" class="form-control" name="linkedin" required 
                                                placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->linkedin != NULL) ? $getIfAlready[0]->linkedin : '') : '')); ?>">
                                        </div>
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-3 col-sm-6">
                                        <label>  Production House Registered date* </label>  
                                        <input type="date" class="form-control" name="production_house_date" required 
                                            placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->production_house_date != NULL) ? $getIfAlready[0]->production_house_date : '') : '')); ?>">
                                    </div>  
                                    
                                        <div class="form-group col-lg-4 col-sm-6">
                                        <label> Working Hours:* </label>  
                                        <div class="clear"></div>
                                        <select style="width:49%;padding:0 10px;border:solid 1px #e0e1e2" name="working_hours_one">
                                            <option value="6am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '6am') ? 'selected' : '') : '') : ''); ?>>6am</option>
                                            <option value="7am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '7am') ? 'selected' : '') : '') : ''); ?>>7am</option>
                                            <option value="8am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '8am') ? 'selected' : '') : '') : ''); ?>>8am</option>
                                            <option value="9am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '9am') ? 'selected' : '') : '') : ''); ?>>9am</option>
                                            <option value="10am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '10am') ? 'selected' : '') : '') : ''); ?>>10am</option>
                                            <option value="11am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '11am') ? 'selected' : '') : '') : ''); ?>>11am</option>
                                            <option value="12am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '12am') ? 'selected' : '') : '') : ''); ?>>12am</option>
                                        </select>
                                        <select style="width:49%;padding:0 10px; float:right;border:solid 1px #e0e1e2" name="working_hours_two">
                                            <option value="4pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '4pm') ? 'selected' : '') : '') : ''); ?>>4pm</option>
                                            <option value="5pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '5pm') ? 'selected' : '') : '') : ''); ?>>5pm</option>
                                            <option value="6pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '6pm') ? 'selected' : '') : '') : ''); ?>>6pm</option>
                                            <option value="7pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '7pm') ? 'selected' : '') : '') : ''); ?>>7pm</option>
                                            <option value="8pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '8pm') ? 'selected' : '') : '') : ''); ?>>8pm</option>
                                            <option value="9pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '9pm') ? 'selected' : '') : '') : ''); ?>>9pm</option>
                                            <option value="10pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '10pm') ? 'selected' : '') : '') : ''); ?>>10pm</option>
                                        </select>
                                    </div> 
                                    
                                    <div class="form-group col-lg-3 col-sm-6">
                                        <label> Upload profile photo </label>  
										 <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                             <!--onclick="openProjUploader('1')"-->
                                            <a href="javascript:void(0)" data-croppie data-croppie-file="#myfile" data-croppie-progress="#upload-photo-progress-0"  data-croppie-input="#myfile1" data-croppie-output="#profile_photo_preview" data-croppie-bind="file" data-croppie-viewport='{"width": 500, "height": 500}' data-croppie-boundary='{"width": 500, "height": 500}' style="background: #637076;color: #fff;width:145px;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">Browse Banner Photo</a>
											<input type="file" target="myfile" class="d-none" id="myfile"  name="myfile" class="image custom-file-input image upload-cover-image" onchange="validateThumbFile(this.files);" accept="image/x-png,image/gif,image/jpeg" />
                                        <!--input type="hidden" id="myfile" name="myfile"-->
											
                                            <!--input type="file" class="d-none" target="fileproj1" id="fileproj1"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                            <input type="hidden" id="uploaded_proj_image1" name="fileproj1"-->
                                        </div>
										
										
                                        
                                    </div>
                                    <?php if(count($getIfAlready) > 0){ 
                                        if($getIfAlready[0]->profile_photo != NULL || $getIfAlready[0]->profile_photo != '')
                                        {
                                        ?>
                                    <div class="form-group col-lg-3 col-sm-6">
                                        <img src="<?php echo $getIfAlready[0]->profile_photo ?>" style="width:100%;height:200px;margin-top:15px;">
                                    </div>
                                    <?php } } ?>   
                                    <div class="form-group col-lg-3 col-sm-6">
									 <img id="profile_photo_preview" src="https://entertainmentbugs.com/public/img/sv-5.jpg" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                        <!--div id="profile_photo_preview">
                                            <img style="width:100%;height:200px;margin-top:15px;" alt="previewImage">
                                        </div-->
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-lg-6">
                                <label> Best Tagline  (25 words) - this line will be display in profile*</label>   
                                <textarea id="w3review" required name="best_tagline" rows="4" cols="60" onKeyPress="return check(event,value)" onInput="checkLength(25,this)"><?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->best_tagline != NULL) ? limit_text($getIfAlready[0]->best_tagline,0,25) : '') : '')); ?></textarea> 
                            </div>
                            <div class="form-group col-lg-6">
                                <label> ABOUT  (200 words)*</label>   
                                <textarea id="w3review" required name="about" rows="4" cols="60" onKeyPress="return check(event,value)" onInput="checkLength(200,this)"><?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->about != NULL) ? limit_text($getIfAlready[0]->about,0,200) : '') : '')); ?></textarea> 
                            </div>
                            <div class="clear"></div>
                            <div class="rows">
                                <h2 style="font-size: 16px;margin:19px 0  13px 10px">Select upto 4 <strong>multiple options,</strong> which  represents to your profile.</h2>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="two fields" id="select-btn5">
                                    <div class="ui padded two columns stackable grid">
                                        <?php 
                                            $categoryarr = [];
                                            if(count($getIfAlready) > 0)
                                            {
                                                if($getIfAlready[0]->multiple_category != NULL)
                                                {
                                                    $explode = explode(',',$getIfAlready[0]->multiple_category);
                                                    if(count($explode) > 0)
                                                    {
                                                        foreach($explode as $k=>$v)
                                                        array_push($categoryarr,$v);
                                                    }
                                            ?>
                                        <?php    
                                            }
                                            }
                                            ?>
                                        <div class="col-lg-4">	
                                            <input  name="phousecheck[]" type="checkbox" value="Co-Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Co-Director',$categoryarr)) ? 'checked' : '') : ''); ?> > Co-Director <br>
                                            <input  name="phousecheck[]" type="checkbox" value="Film Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Film Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Film Director<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Short Film Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Short Film Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Short Film Director<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Tv Serial Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Tv Serial Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Tv Serial Director<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Tv Serial Assistant Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Tv Serial Assistant Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Tv Serial Assistant Director<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Art Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Art Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Art Director<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Music Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Music Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Music Director<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Unit Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Unit Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Unit Director<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Assistant Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Assistant Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Assistant Director<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Corporate Film Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Corporate Film Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Corporate Film Director<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Stunt director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Stunt director',$categoryarr)) ? 'checked' : '') : ''); ?>> Stunt director<br>
                                        </div>
                                        <div class="col-lg-4">
                                            <h2 style="font-size:16px;">File and Tv Producer</h2>
                                            <input  name="phousecheck[]" type="checkbox" value="Film Production house" <?php echo ((count($categoryarr) > 0) ? ((in_array('Film Production house',$categoryarr)) ? 'checked' : '') : ''); ?>> Film Production house<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Film Producer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Film Producer',$categoryarr)) ? 'checked' : '') : ''); ?>> Film Producer<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Tv Serial Production house" <?php echo ((count($categoryarr) > 0) ? ((in_array('Tv Serial Production house',$categoryarr)) ? 'checked' : '') : ''); ?>> Tv Serial Production house<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Tv Serial poroducer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Tv Serial poroducer',$categoryarr)) ? 'checked' : '') : ''); ?>> Tv Serial poroducer<br>
                                            <br>
                                            <h2 style="font-size:16px;">Casting</h2>
                                            <input  name="phousecheck[]" type="checkbox" value="Casting Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Casting Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Casting Director<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Casting Co-ordinator" <?php echo ((count($categoryarr) > 0) ? ((in_array('Casting Co-ordinator',$categoryarr)) ? 'checked' : '') : ''); ?>> Casting Co-ordinator<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Casting Agent" <?php echo ((count($categoryarr) > 0) ? ((in_array('Casting Agent',$categoryarr)) ? 'checked' : '') : ''); ?>> Casting Agent<br>
                                        </div>
                                        <div class="col-lg-4">
                                            <h2 style="font-size:16px;">Producer and Manager</h2>
                                            <input  name="phousecheck[]" type="checkbox" value="Executive Producer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Executive Producer',$categoryarr)) ? 'checked' : '') : ''); ?>> Executive Producer<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Production Manager" <?php echo ((count($categoryarr) > 0) ? ((in_array('Production Manager',$categoryarr)) ? 'checked' : '') : ''); ?>> Production Manager<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Associate producer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Associate producer',$categoryarr)) ? 'checked' : '') : ''); ?>> Associate producer<br>
                                            <br>
                                            <h2 style="font-size:16px;">Others</h2>
                                            <input  name="phousecheck[]" type="checkbox" value="Model Co-ordinator" <?php echo ((count($categoryarr) > 0) ? ((in_array('Model Co-ordinator',$categoryarr)) ? 'checked' : '') : ''); ?>> Model Co-ordinator<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Adv Film Maker" <?php echo ((count($categoryarr) > 0) ? ((in_array('Adv Film Maker',$categoryarr)) ? 'checked' : '') : ''); ?>> Adv Film Maker<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Visual effects creative director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Visual effects creative director',$categoryarr)) ? 'checked' : '') : ''); ?>> Visual effects creative director<br>
                                            <input  name="phousecheck[]" type="checkbox" value="Others" <?php echo ((count($categoryarr) > 0) ? ((in_array('Others',$categoryarr)) ? 'checked' : '') : ''); ?>> Others<br>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <p style="color:red">Select any one the category</p>
                                <div class="form-group">
                                    <div class="u-progress" id="upload-photo-progress-15" style="display: none; width: 150px">
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                <span class="sr-only" data-progress="text">0% Complete</span>
            </div>
        </div>
    </div>
                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                        <div class="browse-photo" onclick="uploadVideo('_3')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                        </div>
                                        
                                        <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                        <input type="file" accept="video/*" class="d-none" id="videoFile_3">
                                        <input type="hidden" name="uploadedvideosec_3" id="uploadedvideosec_3">
                                        <input type="hidden" id="generatedvideosecthumb_3" name="generatedvideosecthumb_3">
                                    </div>
                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                        <div class="browse-photo" data-croppie-progress="#upload-photo-progress-15"  data-croppie="" data-croppie-file="#about-thumb-file" data-croppie-input="#about-thumb-input" data-croppie-output="#previewvideosec_3" data-croppie-bind="file" data-croppie-viewport='{"width": 365, "height": 205}' data-croppie-boundary='{"width": 365, "height": 205}'><i class="fa fa-picture-o" aria-hidden="true"></i>
                                        </div>
                                        <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Thumbnail</a>
                                        <input type="file" class="d-none" id="about-thumb-file" accept="image/x-png,image/gif,image/jpeg">
                                        <input type="hidden" id="about-thumb-input" name="about_thumb">
                                    </div>
                                    <div class="u-close" style="float:left; margin-right:20px;">
                                        <img id="previewvideosec_3" src="https://entertainmentbugs.com/public/img/sv-5.jpg" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                    </div>
                                    <div style="clear:both; overflow:hidden"></div>
                                    <div class="u-progress_3" style="display:none;">
                                        <div class="row">
                                            <div class="col-lg-12 ud-caption">Upload Details</div>
                                            <div class="col-lg-10">
                                                <div class="u-title"></div>
                                                <div id="upload-progress_3">
                                                    <div class="u-size_3"></div>
                                                    <div class="u-progress_3">
                                                        <div class="progress_3">
                                                            <div class="progress-bar_3" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                <span class="sr-only_3">0% Complete</span>
                                                            </div>
                                                        </div>
                                                        <div class="u-close">
                                                            <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="u-desc_3"><span class="percent-process_3"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlready) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                                    <div class="col-lg-2" style="float:right"><button type="button" class="btn btn-cv1">Next</button></div>
                                </div>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div id="what-we-de-bl" class="tabcontent" style="margin-top:10px">
                <div class="container">
                    <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <?php 
                            $userId = session()->get('user_id');
                            $getIfAlreadyWhatWedo = DB::select('select * from production_what_we_do where user_id='.$userId);
                            
                            ?>
                        <form action="{{URL::to('production-what-we-do-profile')}}" id="whatwedoform" style="margin-top:0px" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <div class="form-group achievemets">
                                <h2>What we do</h2>
                                <div class="form-group col-lg-12">
                                    <label for="exampleInputEmail1">What we do *</label>
                                    <input type="text" name="what_we_do" class="form-control" required id="exampleInputEmail1" placeholder="What we do" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->what_we_do != NULL) ? $getIfAlreadyWhatWedo[0]->what_we_do : '') : '')); ?>">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="exampleInputEmail1"> Description (50 words) *</label>
                                    <textarea id="w3review" name="description" onKeyPress="return check(event,value)" onInput="checkLength(50,this)" rows="4" cols="50" required><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->description != NULL) ? limit_text($getIfAlreadyWhatWedo[0]->description,0,50) : '') : '')); ?></textarea> 
                                </div>
                                <div style="clear:both; overflow:hidden"></div>
                                <div id="copydiv">
                                    <div class="col-lg-3">
                                        <div class="form-group"> <label for="exampleInputEmail1">Title *</label>  
                                            <input type="text" required class="form-control" name="title[]" id="exampleInputEmail1" placeholder="eg: CINEMAS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_1 != NULL) ? $getIfAlreadyWhatWedo[0]->title_1 : '') : '')); ?>">  
                                        </div>
                                        <div class="form-group">  
                                            <label for="exampleInputEmail1"> Description (25 words) *</label>
                                            <textarea name="desc[]" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" required rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_1 != NULL) ? limit_text($getIfAlreadyWhatWedo[0]->des_1,0,25) : '') : '')); ?></textarea> 
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"> 
                                            <label for="exampleInputEmail1">Title *</label>  
                                            <input type="text" required name="title[]" class="form-control" id="exampleInputEmail1" placeholder="eg: PRODUCTION" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_2 != NULL) ? $getIfAlreadyWhatWedo[0]->title_2 : '') : '')); ?>">  
                                        </div>
                                        <div class="form-group">  
                                            <label for="exampleInputEmail1"> Description (25 words) *</label>
                                            <textarea id="w3review" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" required name="desc[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_2 != NULL) ? limit_text($getIfAlreadyWhatWedo[0]->des_2,0,25) : '') : '')); ?></textarea> 
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"> 
                                            <label for="exampleInputEmail1">Title *</label>
                                            <input type="text" required name="title[]" class="form-control" id="exampleInputEmail1" placeholder="eg: THEATRE" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_3 != NULL) ? $getIfAlreadyWhatWedo[0]->title_3 : '') : '')); ?>">  
                                        </div>
                                        <div class="form-group">  
                                            <label for="exampleInputEmail1"> Description (25 words) *</label>
                                            <textarea id="w3review" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" required name="desc[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_3 != NULL) ? limit_text($getIfAlreadyWhatWedo[0]->des_3,0,25) : '') : '')); ?></textarea> 
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"> 
                                            <label for="exampleInputEmail1">Title *</label>  
                                            <input type="text" required name="title[]" class="form-control" id="exampleInputEmail1" placeholder="eg: BLOCK BUSTERS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_4 != NULL) ? $getIfAlreadyWhatWedo[0]->title_4 : '') : '')); ?>">  
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"> Description (25 words) *</label>
                                            <textarea id="w3review" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" required name="desc[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_4 != NULL) ? limit_text($getIfAlreadyWhatWedo[0]->des_4,0,25) : '') : '')); ?></textarea> 
                                        </div>
                                    </div>
                                </div>
                                <style>
                                    .d-none{
                                    display:none!important;
                                    }
                                </style>
                                <a href="javascript:void(0)" id="addMore" onclick="addMoreWhatWeDo(this)" style="background: #f72e5e;color: #fff;padding: 0 10px;" class="<?php if(count($getIfAlreadyWhatWedo) > 0){ if($getIfAlreadyWhatWedo[0]->title_5 != NULL || $getIfAlreadyWhatWedo[0]->title_6 != NULL || $getIfAlreadyWhatWedo[0]->title_7 != NULL || $getIfAlreadyWhatWedo[0]->title_8 != NULL){ echo "d-none"; } }else{ echo ""; } ?>">+ Add More</a>
                                <a href="javascript:void(0)" id="removeMore" onclick="removeWhatWeDo(this)" style="background: #f72e5e;color: #fff;padding: 0 10px;" class="<?php if(count($getIfAlreadyWhatWedo) > 0){ if($getIfAlreadyWhatWedo[0]->title_5 == NULL && $getIfAlreadyWhatWedo[0]->title_6 == NULL && $getIfAlreadyWhatWedo[0]->title_7 == NULL && $getIfAlreadyWhatWedo[0]->title_8 == NULL){ echo "d-none"; } }else{ echo "d-none"; } ?>">- Remove</a>
                                <br> <br>
                                <div id="clonediv" class="<?php if(count($getIfAlreadyWhatWedo) > 0){ if($getIfAlreadyWhatWedo[0]->title_5 == NULL && $getIfAlreadyWhatWedo[0]->title_6 == NULL && $getIfAlreadyWhatWedo[0]->title_7 == NULL && $getIfAlreadyWhatWedo[0]->title_8 == NULL){ echo "d-none"; } }else{ echo "d-none"; } ?>">
                                    <div class="col-lg-3">
                                        <div class="form-group"> 
                                            <label for="exampleInputEmail1">Title</label>  
                                            <input type="text" name="title[]" id="title5" class="form-control" placeholder="eg: BLOCK BUSTERS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_5 != NULL) ? $getIfAlreadyWhatWedo[0]->title_5 : '') : '')); ?>">  
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"> Description (25 words)</label>
                                            <textarea id="desc5" name="desc[]" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_5 != NULL) ? limit_text($getIfAlreadyWhatWedo[0]->des_5,0,25) : '') : '')); ?></textarea> 
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"> 
                                            <label for="exampleInputEmail1">Title</label>  
                                            <input type="text" name="title[]" class="form-control" id="title6" placeholder="eg: BLOCK BUSTERS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_6 != NULL) ? $getIfAlreadyWhatWedo[0]->title_6 : '') : '')); ?>">  
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"> Description (25 words)</label>
                                            <textarea id="desc6" name="desc[]" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_6 != NULL) ? limit_text($getIfAlreadyWhatWedo[0]->des_6,0,25) : '') : '')); ?></textarea> 
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"> 
                                            <label for="exampleInputEmail1">Title</label>  
                                            <input type="text" name="title[]" class="form-control" id="title7" placeholder="eg: BLOCK BUSTERS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_7 != NULL) ? $getIfAlreadyWhatWedo[0]->title_7 : '') : '')); ?>">  
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"> Description (25 words)</label>
                                            <textarea id="desc7" name="desc[]" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_7 != NULL) ? limit_text($getIfAlreadyWhatWedo[0]->des_7,0,25) : '') : '')); ?></textarea> 
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group"> 
                                            <label for="exampleInputEmail1">Title</label>  
                                            <input type="text" name="title[]" class="form-control" id="title8" placeholder="eg: BLOCK BUSTERS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_8 != NULL) ? $getIfAlreadyWhatWedo[0]->title_8 : '') : '')); ?>">  
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"> Description (25 words)</label>
                                            <textarea id="desc8" name="desc[]" onKeyPress="return check(event,value)" onInput="checkLength(25,this)" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_8 != NULL) ? limit_text($getIfAlreadyWhatWedo[0]->des_8,0,25) : '') : '')); ?></textarea> 
                                        </div>
                                    </div>
                                </div>
                                <br><br>
                                Note: Minimum 4 you have to insert and max 8
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="row">
                                <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyWhatWedo) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div id="latest-project" class="tabcontent" >
                <div class="container">
                    <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <h2>Latest Projects - add upto 3</h2>
                        <form action="{{URL::to('/production-save-project')}}" method="post" enctype="multipart/form-data" id="projectform">
                            <?php 
                                $userId = session()->get('user_id');
                                $getIfAlreadyProject = DB::select('select * from production_latest_project where user_id='.$userId);
                                ?>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="oldProjid[]" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->id != NULL) ? $getIfAlreadyProject[0]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldProjFile[]" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->photo != NULL) ? $getIfAlreadyProject[0]->photo : '') : '')); ?>" />
                            <div class="col-lg-12" style="background:#fff;">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>First</h6>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="exampleInputEmail1">Select category</label>  
                                        <select class="form-control input-select" style="padding: 10px;" name="categoryproject[]">
                                              <option value="" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                              <option value="Co-Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                              <option value="Film Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                              <option value="Short Film Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                              <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                              <option value="Art Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                              <option value="Music Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                              <option value="Unit Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                              <option value="Assistant Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                              <option value="Corporate Film Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Corporate Film Director") ? 'selected' : '') : '') : ''); ?>>Corporate Film Director</option>
                                              <option value="Stunt director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Stunt director") ? 'selected' : '') : '') : ''); ?>>Stunt director</option>
                                              <option value="Film Production house" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Film Production house") ? 'selected' : '') : '') : ''); ?>>Film Production house</option>
                                              <option value="Film Producer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                              <option value="Tv Serial Production house" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Tv Serial Production house") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production house</option>
                                              <option value="Tv Serial poroducer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Tv Serial poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial poroducer</option>
                                              <option value="Casting Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                              <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                              <option value="Casting Agent" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>>Casting Agent</option>
                                              <option value="Executive Producer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>>Executive Producer</option>
                                              <option value="Production Manager" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>>Production Manager</option>
                                              <option value="Associate producer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>>Associate producer</option>
                                              <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>>Adv Film Maker</option>
                                               <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Model Co-ordinator</option>
                                              <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                              <option value="Others" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                   Others
                                              </option>                                            
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="exampleInputEmail1">Title</label>  
                                        <input type="text" name="projecttitle[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->title != NULL) ? $getIfAlreadyProject[0]->title : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="exampleInputEmail1">Date</label>  
                                        <input type="date" name="projectdate[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->date != NULL) ? $getIfAlreadyProject[0]->date : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="exampleInputEmail1">Add Link - If any</label>  
                                        <input type="text" name="projectlink[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->link != NULL) ? $getIfAlreadyProject[0]->link : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="exampleInputEmail1">Button Name</label>  
                                        <select class="form-control" name="projectbuttonname[]" style="padding: 10px;">
                                            <option value="" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                            <option value="View Video" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "View Video") ? 'selected' : '') : '') : ''); ?>>View Video</option>
                                            <option value="View Now" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "View Now") ? 'selected' : '') : '') : ''); ?>>View Now</option>
                                            <option value="Official Trailer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "Official Trailer") ? 'selected' : '') : '') : ''); ?>> Official Trailer</option>
                                            <option value="View Profile" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "View Profile") ? 'selected' : '') : '') : ''); ?>>View Profile</option>
                                            <option value="View More" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "View More") ? 'selected' : '') : '') : ''); ?>>View More</option>
                                            <option value="No Button" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "No Button") ? 'selected' : '') : '') : ''); ?>>No Button</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-12">  
                                        <label for="exampleInputEmail1">Description 200 words</label>
                                        <textarea id="w3review" name="projectdesc[]" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->description != NULL) ? limit_text($getIfAlreadyProject[0]->description,0,200) : '') : '')); ?></textarea>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="form-group col-lg-12">  
                                     <div class="u-progress" id="upload-photo-progress-0" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                        <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                             <!--onclick="openProjUploader('1')"-->
                                            <a href="javascript:void(0)" data-croppie data-croppie-file="#fileproj1" data-croppie-progress="#upload-photo-progress-0"  data-croppie-input="#uploaded_proj_image1" data-croppie-output="#previewprojsec1" data-croppie-bind="file" data-croppie-viewport='{"width": 500, "height": 500}' data-croppie-boundary='{"width": 500, "height": 500}' style="background: #637076;color: #fff;width:145px;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">Browse Banner Photo</a>
                                            <input type="file" class="d-none" target="fileproj1" id="fileproj1"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                            <input type="hidden" id="uploaded_proj_image1" name="fileproj1">
                                        </div>
                                        <div class="u-close" style="float:left; margin-right:20px;">
                                            <img id="previewprojsec1" src="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->photo != NULL || $getIfAlreadyProject[0]->photo != '') ? $getIfAlreadyProject[0]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyProject) >= 1)
                                                {
                                                    if($getIfAlreadyProject[0]->photo != NULL || $getIfAlreadyProject[0]->photo != '')
                                                    {
                                                ?>
                                            <a id="crossproj1" href="<?php echo URL::to('/delete-prod-proj-image/'.$getIfAlreadyProject[0]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                            <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                            <div class="col-lg-12" style="background:#fff;">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Second</h6>
                                    </div>
                                    <input type="hidden" name="oldProjid[]" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->id != NULL) ? $getIfAlreadyProject[1]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldProjFile[]" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->photo != NULL) ? $getIfAlreadyProject[1]->photo : '') : '')); ?>" />
                                    <div class="form-group col-lg-6">
                                        <label for="exampleInputEmail1">Select category</label>  
                                        <select class="form-control input-select" style="padding: 10px;" name="categoryproject[]">
                                            <option value="" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                            <option value="Co-Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                            <option value="Film Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                            <option value="Short Film Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                            <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                            <option value="Art Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                            <option value="Music Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                            <option value="Unit Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                            <option value="Assistant Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                            <option value="Corporate Film Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Corporate Film Director") ? 'selected' : '') : '') : ''); ?>>Corporate Film Director</option>
                                            <option value="Stunt director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Stunt director") ? 'selected' : '') : '') : ''); ?>>Stunt director</option>
                                            <option value="Film Production house" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Film Production house") ? 'selected' : '') : '') : ''); ?>>Film Production house</option>
                                            <option value="Film Producer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                            <option value="Tv Serial Production house" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Tv Serial Production house") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production house</option>
                                            <option value="Tv Serial poroducer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Tv Serial poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial poroducer</option>
                                            <option value="Casting Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                            <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                            <option value="Casting Agent" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>>Casting Agent</option>
                                            <option value="Executive Producer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>>Executive Producer</option>
                                            <option value="Production Manager" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>>Production Manager</option>
                                            <option value="Associate producer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>>Associate producer</option>
                                            <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>>Adv Film Maker</option>
                                             <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Model Co-ordinator</option>
                                            <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                            <option value="Others" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                 Others
                                            </option>                                            
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="exampleInputEmail1">Title</label>  
                                        <input type="text" name="projecttitle[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->title != NULL) ? $getIfAlreadyProject[1]->title : '') : '')); ?>">  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-lg-4">
                                        <label for="exampleInputEmail1">Date</label>  
                                        <input type="date" name="projectdate[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->date != NULL) ? $getIfAlreadyProject[1]->date : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="exampleInputEmail1">Add Link - If any</label>  
                                        <input type="text" name="projectlink[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->link != NULL) ? $getIfAlreadyProject[1]->link : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label for="exampleInputEmail1">Button Name</label>  
                                        <select class="form-control" name="projectbuttonname[]" style="padding: 10px;">
                                            <option value="" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                            <option value="View Video" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "View Video") ? 'selected' : '') : '') : ''); ?>>View Video</option>
                                            <option value="View Now" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "View Now") ? 'selected' : '') : '') : ''); ?>>View Now</option>
                                            <option value="Official Trailer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "Official Trailer") ? 'selected' : '') : '') : ''); ?>> Official Trailer</option>
                                            <option value="View Profile" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "View Profile") ? 'selected' : '') : '') : ''); ?>>View Profile</option>
                                            <option value="View More" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "View More") ? 'selected' : '') : '') : ''); ?>>View More</option>
                                            <option value="No Button" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "No Button") ? 'selected' : '') : '') : ''); ?>>No Button</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12">  
                                    <label for="exampleInputEmail1">Description 200 words</label>
                                    <textarea id="w3review" name="projectdesc[]" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->description != NULL) ? limit_text($getIfAlreadyProject[2]->description,0,200) : '') : '')); ?></textarea>
                                </div>
                                <div class="clear"></div>
                                <div class="form-group col-lg-12">  
                                
                                <div class="u-progress" id="upload-photo-progress-1" style="display: none; width: 150px">
        									<div class="progress">
        										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
        											<span class="sr-only" data-progress="text">0% Complete</span>
        										</div>
        									</div>
        								</div>
                                        <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                             <!--onclick="openProjUploader('2')"-->
                                            <a href="javascript:void(0)" data-croppie data-croppie-file="#fileproj2" data-croppie-progress="#upload-photo-progress-1" 
data-croppie-input="#uploaded_proj_image2" data-croppie-output="#previewprojsec2" data-croppie-bind="file" data-croppie-viewport='{"width": 500, "height": 500}' data-croppie-boundary='{"width": 500, "height": 500}' style="background: #637076;color: #fff;width:145px;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">Browse Banner Photo</a>
                                            <input type="file" class="d-none" target="fileproj2" id="fileproj2"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                            <input type="hidden" id="uploaded_proj_image2" name="fileproj2">
                                        </div>
                                        <div class="u-close" style="float:left; margin-right:20px;">
                                            <img id="previewprojsec2" src="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->photo != NULL || $getIfAlreadyProject[1]->photo != '') ? $getIfAlreadyProject[1]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyProject) >= 2)
                                                {
                                                    if($getIfAlreadyProject[1]->photo != NULL || $getIfAlreadyProject[1]->photo != '')
                                                    {
                                                ?>
                                            <a id="crossproj2" href="<?php echo URL::to('/delete-prod-proj-image/'.$getIfAlreadyProject[1]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                            <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                        </div>
                                    </div>  
                            </div>
                            <div id="cloneprojdiv" class="d-none">
                                <div class="col-lg-12" style="background:#fff;">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h6>Third</h6>
                                        </div>
                                        <input type="hidden" name="oldProjFile[]" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->photo != NULL) ? $getIfAlreadyProject[2]->photo : '') : '')); ?>" />
                                        <input type="hidden" name="oldProjid[]" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->id != NULL) ? $getIfAlreadyProject[2]->id : '') : '')); ?>" />
                                        <div class="form-group col-lg-6">
                                            <label for="exampleInputEmail1">Select category</label>  
                                            <select class="form-control input-select" style="padding: 10px;" name="categoryproject[]">
                                                  <option value="" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                                  <option value="Co-Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                  <option value="Film Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                  <option value="Short Film Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                  <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                  <option value="Art Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                  <option value="Music Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                  <option value="Unit Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                  <option value="Assistant Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                  <option value="Corporate Film Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Corporate Film Director") ? 'selected' : '') : '') : ''); ?>>Corporate Film Director</option>
                                                  <option value="Stunt director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Stunt director") ? 'selected' : '') : '') : ''); ?>>Stunt director</option>
                                                  <option value="Film Production house" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Film Production house") ? 'selected' : '') : '') : ''); ?>>Film Production house</option>
                                                  <option value="Film Producer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                  <option value="Tv Serial Production house" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Tv Serial Production house") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production house</option>
                                                  <option value="Tv Serial poroducer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Tv Serial poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial poroducer</option>
                                                  <option value="Casting Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                  <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                  <option value="Casting Agent" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>>Casting Agent</option>
                                                  <option value="Executive Producer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>>Executive Producer</option>
                                                  <option value="Production Manager" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>>Production Manager</option>
                                                  <option value="Associate producer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>>Associate producer</option>
                                                  <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>>Adv Film Maker</option>
                                                   <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Model Co-ordinator</option>
                                                  <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                  <option value="Others" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                       Others
                                                  </option>                                            
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-6">
                                            <label for="exampleInputEmail1">Title</label>  
                                            <input type="text" name="projecttitle[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->title != NULL) ? $getIfAlreadyProject[2]->title : '') : '')); ?>">  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-4">
                                            <label for="exampleInputEmail1">Date</label>  
                                            <input type="date" name="projectdate[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->date != NULL) ? $getIfAlreadyProject[2]->date : '') : '')); ?>">  
                                        </div>
                                        <div class="form-group col-lg-4">
                                            <label for="exampleInputEmail1">Add Link - If any</label>  
                                            <input type="text" name="projectlink[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->link != NULL) ? $getIfAlreadyProject[2]->link : '') : '')); ?>">  
                                        </div>
                                        <div class="form-group col-lg-4">
                                            <label for="exampleInputEmail1">Button Name</label>  
                                            <select class="form-control" name="projectbuttonname[]" style="padding: 10px;">
                                                <option value="" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="View Video" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "View Video") ? 'selected' : '') : '') : ''); ?>>View Video</option>
                                                <option value="View Now" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "View Now") ? 'selected' : '') : '') : ''); ?>>View Now</option>
                                                <option value="Official Trailer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "Official Trailer") ? 'selected' : '') : '') : ''); ?>> Official Trailer</option>
                                                <option value="View Profile" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "View Profile") ? 'selected' : '') : '') : ''); ?>>View Profile</option>
                                                <option value="View More" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "View More") ? 'selected' : '') : '') : ''); ?>>View More</option>
                                                <option value="No Button" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "No Button") ? 'selected' : '') : '') : ''); ?>>No Button</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">  
                                        <label for="exampleInputEmail1">Description 200 words</label>
                                        <textarea id="w3review" name="projectdesc[]" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->description != NULL) ? limit_text($getIfAlreadyProject[2]->description,0,200) : '') : '')); ?></textarea>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="form-group col-lg-12">
                                         <div class="u-progress" id="upload-photo-progress-88" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>	
                                        <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                             <!--onclick="openProjUploader('3')"-->
                                            <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-88" data-croppie data-croppie-file="#fileproj3" data-croppie-input="#uploaded_proj_image3" data-croppie-output="#previewprojsec3" data-croppie-bind="file" data-croppie-viewport='{"width": 500, "height": 500}' data-croppie-boundary='{"width": 500, "height": 500}' style="background: #637076;color: #fff;width:145px;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">Browse Banner Photo</a>
                                            <input type="file" class="d-none" target="fileproj3" id="fileproj3"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                            <input type="hidden" id="uploaded_proj_image3" name="fileproj3">
                                        </div>
                                        <div class="u-close" style="float:left; margin-right:20px;">
                                            <img id="previewprojsec3" src="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->photo != NULL || $getIfAlreadyProject[2]->photo != '') ? $getIfAlreadyProject[2]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyProject) >= 3)
                                                {
                                                    if($getIfAlreadyProject[2]->photo != NULL || $getIfAlreadyProject[2]->photo != '')
                                                    {
                                                ?>
                                            <a id="crossproj3" href="<?php echo URL::to('/delete-prod-proj-image/'.$getIfAlreadyProject[2]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                            <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                            <div class="row" style="margin-top:20px;margin-bottom:20px">
                                <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyProject) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                            </div>
                        </form>
                        <a href="javascript:void(0)" id="addProj" class="btn btn-dark" onclick="addProj()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Add More Projects</a>
                        <a href="javascript:void(0)" class="d-none btn btn-dark" id="removeProj" onclick="removeProj()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">
                            Remove Projects
                        </a>
                    </div>
                </div>
            </div>
            <div id="awards" class="tabcontent">
                <div class="container">
                    <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <div class="form-group achievemets">
                            <?php 
                                $userId = session()->get('user_id');
                                $getIfAlreadyAchivement = DB::select('select * from production_awards where user_id='.$userId);
                                ?>
                            <form action="{{URL::to('save-production-achivement')}}" id="achivementForm" style="margin-top:0px" enctype="multipart/form-data" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <h2>My Awards upto 3</h2>
                                <b> Image size 350 x 350  <b>  <br><br>
                                <div class="form-group col-lg-6">
                                    <label for="exampleInputEmail1">Title</label>
                                    <input type="text" name="titleachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->title_1 != NULL) ? $getIfAlreadyAchivement[0]->title_1 : '') : '')); ?>">  
                                </div>
                                <input type="hidden" name="id[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->id != NULL) ? $getIfAlreadyAchivement[0]->id : '') : '')); ?>">  
                                <input type="hidden" name="oldFile[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->photo_1 != NULL) ? $getIfAlreadyAchivement[0]->photo_1 : '') : '')); ?>">  
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Link - If any</label>
                                    <input type="text" name="linkachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->link_1 != NULL) ? $getIfAlreadyAchivement[0]->link_1 : '') : '')); ?>">  
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Date</label>
                                    <input type="date" name="date[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->date_1 != NULL) ? date('Y-m-d',strtotime($getIfAlreadyAchivement[0]->date_1)) : '') : '')); ?>">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="exampleInputEmail1"> Description (200 words)</label>
                                    <textarea id="w3review" name="descachieve[]" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->des_1 != NULL) ? limit_text($getIfAlreadyAchivement[0]->des_1,0,200) : '') : '')); ?></textarea>
                                </div>
                                <div class="clear"></div>
                                <div class="form-group col-lg-12">  
                                <div class="u-progress" id="upload-photo-progress-2" style="display: none; width: 150px">
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
            <span class="sr-only" data-progress="text">0% Complete</span>
        </div>
    </div>
</div>
                                        <input type="file" class="d-none"  target="fileachive12" id="fileachive12"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />

                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                        <a href="javascript:void(0)"  data-croppie data-croppie-file="#fileachive12" data-croppie-output="#previewachievesec1"  data-croppie-bind="file" data-croppie-input="#uploaded_achieve_image1" data-croppie-progress="#upload-photo-progress-2" style="background: #637076;color: #fff;width:auto;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">Browse Achievement  Photo</a>
                                        <input type="hidden" id="uploaded_achieve_image1" name="fileachive1">
                                    </div>
                                    <div class="u-close" style="float:left; margin-right:20px;">
                                        <img id="previewachievesec1" src="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->photo_1 != NULL || $getIfAlreadyAchivement[0]->photo_1 != '') ? $getIfAlreadyAchivement[0]->photo_1 : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                        <?php 
                                            if(count($getIfAlreadyAchivement) >= 1)
                                            {
                                                if($getIfAlreadyAchivement[0]->photo_1 != NULL || $getIfAlreadyAchivement[0]->photo_1 != '')
                                                {
                                            ?>
                                        <a id="crossachive1" href="<?php echo URL::to('/delete-award-image/'.$getIfAlreadyAchivement[0]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                        <i class="cvicon-cv-cancel"></i>
                                        </a>
                                        <?php } } ?>
                                    </div>
                                </div>  
                                <div style="clear:both; overflow:hidden"></div>
                                <Br>   
                                <div class="form-group col-lg-6">
                                    <label for="exampleInputEmail1">Title</label>
                                    <input type="text" name="titleachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->title_1 != NULL) ? $getIfAlreadyAchivement[1]->title_1 : '') : '')); ?>">  
                                </div>
                                <input type="hidden" name="id[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->id != NULL) ? $getIfAlreadyAchivement[1]->id : '') : '')); ?>">  
                                <input type="hidden" name="oldFile[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->photo_1 != NULL) ? $getIfAlreadyAchivement[1]->photo_1 : '') : '')); ?>">  
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Link - If any</label>
                                    <input type="text" name="linkachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->link_1 != NULL) ? $getIfAlreadyAchivement[1]->link_1 : '') : '')); ?>">  
                                </div>
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Date</label>
                                    <input type="date" name="date[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->date_1 != NULL) ? date('Y-m-d',strtotime($getIfAlreadyAchivement[1]->date_1)) : '') : '')); ?>">
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="exampleInputEmail1"> Description (200 words)</label>
                                    <textarea id="w3review" name="descachieve[]" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->des_1 != NULL) ? limit_text($getIfAlreadyAchivement[1]->des_1,0,200) : '') : '')); ?></textarea>
                                </div>
                                <div class="clear"></div>
                                <div class="form-group col-lg-12">  
                                <div class="u-progress" id="upload-photo-progress-3" style="display: none; width: 150px">
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
            <span class="sr-only" data-progress="text">0% Complete</span>
        </div>
    </div>
</div>
                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                        <a href="javascript:void(0)" data-croppie data-croppie-file="#fileachive23" data-croppie-output="#previewachievesec2" data-croppie-input="#uploaded_achieve_image2" data-croppie-bind="file" data-croppie-progress="#upload-photo-progress-3" 
 style="background: #637076;color: #fff;width:auto;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">Browse Achievement  Photo</a>
                                        <input type="file" class="d-none" target="fileachive23" id="fileachive23"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                        <input type="hidden" id="uploaded_achieve_image2" name="fileachive2">
                                    </div>
                                    <div class="u-close" style="float:left; margin-right:20px;">
                                        <img id="previewachievesec2" src="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->photo_1 != NULL || $getIfAlreadyAchivement[1]->photo_1 != '') ? $getIfAlreadyAchivement[1]->photo_1 : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                        <?php 
                                            if(count($getIfAlreadyAchivement) >= 2)
                                            {
                                                if($getIfAlreadyAchivement[1]->photo_1 != NULL || $getIfAlreadyAchivement[1]->photo_1 != '')
                                                {
                                            ?>
                                        <a id="crossachive2" href="<?php echo URL::to('/delete-award-image/'.$getIfAlreadyAchivement[1]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                            <i class="cvicon-cv-cancel"></i>
                                        </a>
                                        <?php } } ?>
                                    </div>
                                </div>  
                                <div class="clear"></div>
                                <div style="clear:both; overflow:hidden"></div>
                                <Br>   
                                <div class="clear"></div>
                                <a href="javascript:void(0)" id="addMoreAchivement" onclick="addMoreAchivement(this)" style="background: #f72e5e;color: #fff;padding: 0 10px;" class="<?php if(count($getIfAlreadyAchivement) >= 3){ echo "d-none"; }else{ } ?>">+ Add More</a>
                                <a href="javascript:void(0)" id="removeMoreAchivement" onclick="removeMoreAchivement(this)" style="background: #f72e5e;color: #fff;padding: 0 10px;" class="<?php if(count($getIfAlreadyAchivement) <= 2){ echo "d-none"; } ?>">- Remove</a>
                                <div style="clear:both; overflow:hidden"></div>
                                <Br>   
                                <div class="clear"></div>
                                <div id="cloneachivement" class="<?php if(count($getIfAlreadyAchivement) <= 2){ echo "d-none"; } ?>">
                                    <div class="form-group col-lg-6">
                                        <label for="exampleInputEmail1">Title</label>
                                        <input type="text" name="titleachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->title_1 != NULL) ? $getIfAlreadyAchivement[2]->title_1 : '') : '')); ?>">  
                                    </div>
                                    <input type="hidden" name="id[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->id != NULL) ? $getIfAlreadyAchivement[2]->id : '') : '')); ?>">  
                                    <input type="hidden" name="oldFile[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->photo_1 != NULL) ? $getIfAlreadyAchivement[2]->photo_1 : '') : '')); ?>">  
                                    <div class="form-group col-lg-3">
                                        <label for="exampleInputEmail1">Link - If any</label>
                                        <input type="text" name="linkachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->link_1 != NULL) ? $getIfAlreadyAchivement[2]->link_1 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group col-lg-3">
                                        <label for="exampleInputEmail1">Date</label>
                                        <input type="date" name="date[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->date_1 != NULL) ? date('Y-m-d',strtotime($getIfAlreadyAchivement[2]->date_1)) : '') : '')); ?>">
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <label for="exampleInputEmail1"> Description (200 words)</label>
                                        <textarea id="w3review" name="descachieve[]" onKeyPress="return check(event,value)" onInput="checkLength(200,this)" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->des_1 != NULL) ? limit_text($getIfAlreadyAchivement[2]->des_1,0,200) : '') : '')); ?></textarea>
                                    </div>
                                    <div class="clear"></div>
                                    <br>
                                    <div class="form-group col-lg-12"> 
                                     <div class="u-progress" id="upload-photo-progress-1787" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                        <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <a href="javascript:void(0)" data-croppie data-croppie-file="#croppie-video-thumb-file" data-croppie-progress="#upload-photo-progress-1787"  data-croppie-input="#croppie-video-thumb-input" data-croppie-output="#croppie-video-thumb-preview" data-croppie-bind="file"  style="background: #637076;color: #fff;width:auto;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">Browse Achievement  Photo</a>
                                            <input type="file" class="d-none" target="croppie-video-thumb-file" id="croppie-video-thumb-file"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                            <input type="hidden" id="croppie-video-thumb-input" name="fileachive3">
                                        </div>
                                        <div class="u-close" style="float:left; margin-right:20px;">
                                            <img id="croppie-video-thumb-preview" src="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->photo_1 != NULL || $getIfAlreadyAchivement[2]->photo_1 != '') ? $getIfAlreadyAchivement[2]->photo_1 : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyAchivement) >= 3)
                                                {
                                                    if($getIfAlreadyAchivement[2]->photo_1 != NULL || $getIfAlreadyAchivement[2]->photo_1 != '')
                                                    {
                                                ?>
                                            <a id="crossachive3" href="<?php echo URL::to('/delete-award-image/'.$getIfAlreadyAchivement[2]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                            <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                        </div>
                                    </div>  
                                    <div style="clear:both; overflow:hidden"></div>
                                    <Br>   
                                    <Br>   
                                </div>
                                <div style="clear:both; overflow:hidden"></div>
                                <div class="row">
                                    <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyAchivement) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!--<div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">-->
                    <!--   <h2>My Awards upto 3</h2>-->
                    <!--   <div class="form-group col-lg-6">-->
                    <!--      <label for="exampleInputEmail1">Title</label>  -->
                    <!--      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">-->
                    <!--   </div>-->
                    <!--   <div class="form-group col-lg-3">-->
                    <!--      <label for="exampleInputEmail1">Add Link - If any</label>  -->
                    <!--      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="eg: ">-->
                    <!--   </div>-->
                    <!--      <div class="form-group col-lg-3">-->
                    <!--      <label for="exampleInputEmail1">Date</label>  -->
                    <!--      <input type="date" class="form-control" id="exampleInputEmail1" placeholder="">-->
                    <!--   </div>-->
                    <!--   <div class="form-group col-lg-12">  -->
                    <!--      <label for="exampleInputEmail1">Description 200 words</label>-->
                    <!--      <textarea id="w3review" name="w3review" rows="4" cols="50" spellcheck="false"> -->
                    <!--      </textarea>-->
                    <!--   </div>-->
                    <!--   <div class="clear"></div>-->
                    <!--   <a href="" style="background: #637076;color: #fff;width:155px;margin:10px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">-->
                    <!--   Browse Awards Photo</a>   Image size 350 x 350-->
                    <!--   <div class="clear"></div>-->
                    <!--   <div class="row" style="margin-top:20px">-->
                    <!--      <div class="col-lg-2"><button type="submit" class="btn btn-cv1">Save</button></div>-->
                    <!--   </div>-->
                    <!--   <a href="" style="background: #e5e5e5;color: #000;width:130px;margin:20px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">-->
                    <!--   Add More Projects</a>-->
                    <!--</div>-->
                </div>
            </div>
            <div id="portfolio" class="tabcontent" style="margin-top:10px">
                <div class="container">
                    <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <h2>Portfolio</h2>
                        <p>You can add 12 Photos</p>
                        <div class="col-lg-12">
                            <div class="u-form">
                                <?php 
                                    $userId = session()->get('user_id');
                                    $getIfAlreadyGallery = DB::select('select * from production_gallery where user_id='.$userId);
                                    ?>
                                <form action="{{URL::to('/save-production-gallery')}}" method="post" enctype="multipart/form-data" id="galleryForm">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 1 ? (($getIfAlreadyGallery[0]->id != NULL) ? $getIfAlreadyGallery[0]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 1 ? (($getIfAlreadyGallery[0]->photo != NULL) ? $getIfAlreadyGallery[0]->photo : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 2 ? (($getIfAlreadyGallery[1]->id != NULL) ? $getIfAlreadyGallery[1]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 2 ? (($getIfAlreadyGallery[1]->photo != NULL) ? $getIfAlreadyGallery[1]->photo : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[2]->id != NULL) ? $getIfAlreadyGallery[2]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[2]->photo != NULL) ? $getIfAlreadyGallery[2]->photo : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 4 ? (($getIfAlreadyGallery[3]->id != NULL) ? $getIfAlreadyGallery[3]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 4 ? (($getIfAlreadyGallery[3]->photo != NULL) ? $getIfAlreadyGallery[3]->photo : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 5 ? (($getIfAlreadyGallery[4]->id != NULL) ? $getIfAlreadyGallery[4]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 5 ? (($getIfAlreadyGallery[4]->photo != NULL) ? $getIfAlreadyGallery[4]->photo : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 6 ? (($getIfAlreadyGallery[5]->id != NULL) ? $getIfAlreadyGallery[5]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 6 ? (($getIfAlreadyGallery[5]->photo != NULL) ? $getIfAlreadyGallery[5]->photo : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 7 ? (($getIfAlreadyGallery[6]->id != NULL) ? $getIfAlreadyGallery[6]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 7 ? (($getIfAlreadyGallery[6]->photo != NULL) ? $getIfAlreadyGallery[6]->photo : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 8 ? (($getIfAlreadyGallery[7]->id != NULL) ? $getIfAlreadyGallery[7]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 8 ? (($getIfAlreadyGallery[7]->photo != NULL) ? $getIfAlreadyGallery[7]->photo : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 9 ? (($getIfAlreadyGallery[8]->id != NULL) ? $getIfAlreadyGallery[8]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 9 ? (($getIfAlreadyGallery[8]->photo != NULL) ? $getIfAlreadyGallery[8]->photo : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 10 ? (($getIfAlreadyGallery[9]->id != NULL) ? $getIfAlreadyGallery[9]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 10 ? (($getIfAlreadyGallery[9]->photo != NULL) ? $getIfAlreadyGallery[9]->photo : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 11 ? (($getIfAlreadyGallery[10]->id != NULL) ? $getIfAlreadyGallery[10]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 11 ? (($getIfAlreadyGallery[10]->photo != NULL) ? $getIfAlreadyGallery[10]->photo : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoid[]" value="<?php echo ((count($getIfAlreadyGallery) >= 12 ? (($getIfAlreadyGallery[11]->id != NULL) ? $getIfAlreadyGallery[11]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldPhotoFile[]" value="<?php echo ((count($getIfAlreadyGallery) >= 12 ? (($getIfAlreadyGallery[11]->photo != NULL) ? $getIfAlreadyGallery[11]->photo : '') : '')); ?>" />
                                    <div class="row" id="portfolio1">
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title 1 - (3 words)</label>
                                                <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 1 ? (($getIfAlreadyGallery[0]->photo_title != NULL) ? $getIfAlreadyGallery[0]->photo_title : '') : '')); ?>">  
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                    <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                    <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                    <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                    <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                    <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                    <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                    <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                    <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                    <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                    <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                    <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                    <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                    <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                    <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                    <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                    <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                    <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                    <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                    <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                    <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                    <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                    <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                    <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-progress" id="upload-photo-progress-4" style="display: none; width: 150px">
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
            <span class="sr-only" data-progress="text">0% Complete</span>
        </div>
    </div>
</div>
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-4" data-croppie data-croppie-file="#photosectionphoto1" data-croppie-input="#uploaded_gal_image1" data-croppie-output="#gal1" data-croppie-bind="file" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                    <input type="file" class="d-none" target="photosectionphoto0" id="photosectionphoto1"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                    <input type="hidden" id="uploaded_gal_image1" name="photosectionphoto0">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img id="gal1" src="<?php echo ((count($getIfAlreadyGallery) >= 1 ? (($getIfAlreadyGallery[0]->photo != NULL || $getIfAlreadyGallery[0]->photo != '') ? $getIfAlreadyGallery[0]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 1)
                                                        {
                                                            if($getIfAlreadyGallery[0]->photo != NULL || $getIfAlreadyGallery[0]->photo != '')
                                                            {
                                                        ?>
                                                    <a id="crossgal1" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[0]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title 2 - (3 words)</label>
                                                <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 2 ? (($getIfAlreadyGallery[1]->photo_title != NULL) ? $getIfAlreadyGallery[1]->photo_title : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                    <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                    <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                    <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                    <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                    <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                    <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                    <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                    <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                    <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                    <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                    <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                    <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                    <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                    <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                    <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                    <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                    <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                    <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                    <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                    <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                    <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                    <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                    <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-progress" id="upload-photo-progress-5" style="display: none; width: 150px">
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
            <span class="sr-only" data-progress="text">0% Complete</span>
        </div>
    </div>
</div>
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" data-croppie data-croppie-file="#photosectionphoto2" data-croppie-progress="#upload-photo-progress-5"  data-croppie-input="#uploaded_gal_image2" data-croppie-output="#gal2" data-croppie-bind="file"  style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                    <input type="file" class="d-none" target="photosectionphoto1" id="photosectionphoto2"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                    <input type="hidden" id="uploaded_gal_image2" name="photosectionphoto1">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img id="gal2" src="<?php echo ((count($getIfAlreadyGallery) >= 2 ? (($getIfAlreadyGallery[1]->photo != NULL || $getIfAlreadyGallery[1]->photo != '') ? $getIfAlreadyGallery[1]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 2)
                                                        {
                                                            if($getIfAlreadyGallery[1]->photo != NULL || $getIfAlreadyGallery[1]->photo != '')
                                                            {
                                                        ?>
                                                    <a id="crossgal2" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[1]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title 3 - (3 words)</label>
                                                <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[2]->photo_title != NULL) ? $getIfAlreadyGallery[2]->photo_title : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                    <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                    <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                    <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                    <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                    <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                    <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                    <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                    <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                    <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                    <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                    <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                    <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                    <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                    <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                    <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                    <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                    <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                    <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                    <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                    <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                    <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                    <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                    <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-progress" id="upload-photo-progress-6" style="display: none; width: 150px">
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
            <span class="sr-only" data-progress="text">0% Complete</span>
        </div>
    </div>
</div>
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-6"  data-croppie data-croppie-file="#photosectionphoto3" data-croppie-input="#uploaded_gal_image3" data-croppie-output="#gal3" data-croppie-bind="file"  style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                    <input type="file" class="d-none" target="photosectionphoto2" id="photosectionphoto3"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                    <input type="hidden" id="uploaded_gal_image3" name="photosectionphoto2">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img id="gal3" src="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[2]->photo != NULL || $getIfAlreadyGallery[2]->photo != '') ? $getIfAlreadyGallery[2]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 3)
                                                        {
                                                            if($getIfAlreadyGallery[2]->photo != NULL || $getIfAlreadyGallery[2]->photo != '')
                                                            {
                                                        ?>
                                                    <a id="crossgal3" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[2]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6" style="padding-left:0px">
                                                <label for="e1">Photo Title 4 - (3 words)</label>
                                                <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 4 ? (($getIfAlreadyGallery[3]->photo_title != NULL) ? $getIfAlreadyGallery[3]->photo_title : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                                <select class="input-select" style="width:100%" name="photo_category[]" >
                                                    <option value="" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                    <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                    <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                    <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                    <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                    <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                    <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                    <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                    <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                    <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                    <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                    <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                    <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                    <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                    <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                    <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                    <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                    <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                    <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                    <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                    <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                    <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                    <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                    <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                
<div class="u-progress" id="upload-photo-progress-7" style="display: none; width: 150px">
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
            <span class="sr-only" data-progress="text">0% Complete</span>
        </div>
    </div>
</div>
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" data-croppie data-croppie-file="#photosectionphoto4" data-croppie-input="#uploaded_gal_image4" data-croppie-output="#gal4" data-croppie-bind="file" data-croppie-progress="#upload-photo-progress-7"  style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                    <input type="file" class="d-none" target="photosectionphoto3" id="photosectionphoto4"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                    <input type="hidden" id="uploaded_gal_image4" name="photosectionphoto3">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img id="gal4" src="<?php echo ((count($getIfAlreadyGallery) >= 4 ? (($getIfAlreadyGallery[3]->photo != NULL || $getIfAlreadyGallery[3]->photo != '') ? $getIfAlreadyGallery[3]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyGallery) >= 4)
                                                        {
                                                            if($getIfAlreadyGallery[3]->photo != NULL || $getIfAlreadyGallery[3]->photo != '')
                                                            {
                                                        ?>
                                                    <a id="crossgal4" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[3]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;" id="addPhotoTwoMore" onclick="addfourmore('2')">Add More 4 photos grid</a>
                                    <a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;" class="d-none" id="removePhotoTwoMore" onclick="removefourmore('2')">Remove 4 photos grid</a>
                                    <br>
                                    <div id="portfolio2" style="margin-top:2rem;">
                                        <div id="portfolio3" class="d-none">
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6" style="padding-left:0px">
                                                    <label for="e1">Photo Title 5 - (3 words)</label>
                                                    <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 5 ? (($getIfAlreadyGallery[4]->photo_title != NULL) ? $getIfAlreadyGallery[4]->photo_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6"  style="padding:0px">
                                                    <label for="e1">Select category</label>
                                                    <select class="input-select" style="width:100%" name="photo_category[]" >
                                                        <option value="" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                        <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                        <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                        <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                        <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                        <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                        <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                        <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                        <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                        <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                        <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                        <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                        <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                        <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                        <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                        <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                        <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                        <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                        <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                        <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                        <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                        <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                        <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                        <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                        <div class="u-progress" id="upload-photo-progress-100" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                                        <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-100" data-croppie data-croppie-file="#photosectionphoto543" data-croppie-input="#uploaded_gal_image5" data-croppie-output="#gal5" data-croppie-bind="file"  style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                        <input type="file" class="d-none" target="photosectionphoto543" id="photosectionphoto543"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                        <input type="hidden" id="uploaded_gal_image5" name="photosectionphoto4">
                                                    </div>
                                                    <div class="u-close" style="float:left; margin-right:20px;">
                                                        <img id="gal5" src="<?php echo ((count($getIfAlreadyGallery) >= 5 ? (($getIfAlreadyGallery[4]->photo != NULL || $getIfAlreadyGallery[4]->photo != '') ? $getIfAlreadyGallery[4]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                        <?php 
                                                            if(count($getIfAlreadyGallery) >= 5)
                                                            {
                                                                if($getIfAlreadyGallery[4]->photo != NULL || $getIfAlreadyGallery[4]->photo != '')
                                                                {
                                                            ?>
                                                        <a id="crossgal5" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[4]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                        </a>
                                                        <?php } } ?>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6" style="padding-left:0px">
                                                    <label for="e1">Photo Title 6 - (3 words)</label>
                                                    <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 6 ? (($getIfAlreadyGallery[5]->photo_title != NULL) ? $getIfAlreadyGallery[5]->photo_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6"  style="padding:0px">
                                                    <label for="e1">Select category</label>
                                                    <select class="input-select" style="width:100%" name="photo_category[]" >
                                                        <option value="" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                        <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                        <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                        <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                        <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                        <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                        <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                        <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                        <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                        <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                        <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                        <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                        <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                        <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                        <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                        <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                        <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                        <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                        <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                        <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                        <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                        <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                        <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                        <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                         <div class="u-progress" id="upload-photo-progress-99" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                                        <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-99" data-croppie data-croppie-file="#photosectionphoto565" data-croppie-input="#uploaded_gal_image6" data-croppie-output="#gal6" data-croppie-bind="file"  style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                        <input type="file" class="d-none" target="photosectionphoto565" id="photosectionphoto565"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                        <input type="hidden" id="uploaded_gal_image6" name="photosectionphoto5">
                                                    </div>
                                                    <div class="u-close" style="float:left; margin-right:20px;">
                                                        <img id="gal6" src="<?php echo ((count($getIfAlreadyGallery) >= 6 ? (($getIfAlreadyGallery[5]->photo != NULL || $getIfAlreadyGallery[5]->photo != '') ? $getIfAlreadyGallery[5]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                        <?php 
                                                            if(count($getIfAlreadyGallery) >= 6)
                                                            {
                                                                if($getIfAlreadyGallery[5]->photo != NULL || $getIfAlreadyGallery[5]->photo != '')
                                                                {
                                                            ?>
                                                        <a id="crossgal6" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[5]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                        </a>
                                                        <?php } } ?>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6" style="padding-left:0px">
                                                    <label for="e1">Photo Title 7 - (3 words)</label>
                                                    <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 7 ? (($getIfAlreadyGallery[6]->photo_title != NULL) ? $getIfAlreadyGallery[6]->photo_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6"  style="padding:0px">
                                                    <label for="e1">Select category</label>
                                                    <select class="input-select" style="width:100%" name="photo_category[]" >
                                                        <option value="" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                        <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                        <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                        <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                        <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                        <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                        <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                        <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                        <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                        <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                        <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                        <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                        <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                        <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                        <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                        <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                        <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                        <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                        <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                        <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                        <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                        <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                        <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                        <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                         <div class="u-progress" id="upload-photo-progress-98" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                                        <div class="browse-photo" data-croppie-progress="#upload-photo-progress-98" data-croppie data-croppie-file="#photosectionphoto654" data-croppie-input="#uploaded_gal_image7" data-croppie-output="#gal7" data-croppie-bind="file"  ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                        <input type="file" class="d-none" target="photosectionphoto654" id="photosectionphoto654"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                        <input type="hidden" id="uploaded_gal_image7" name="photosectionphoto6">
                                                    </div>
                                                    <div class="u-close" style="float:left; margin-right:20px;">
                                                        <img id="gal7" src="<?php echo ((count($getIfAlreadyGallery) >= 7 ? (($getIfAlreadyGallery[6]->photo != NULL || $getIfAlreadyGallery[6]->photo != '') ? $getIfAlreadyGallery[6]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                        <?php 
                                                            if(count($getIfAlreadyGallery) >= 7)
                                                            {
                                                                if($getIfAlreadyGallery[6]->photo != NULL || $getIfAlreadyGallery[6]->photo != '')
                                                                {
                                                            ?>
                                                        <a id="crossgal7" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[6]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                        </a>
                                                        <?php } } ?>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6" style="padding-left:0px">
                                                    <label for="e1">Photo Title 8 - (3 words)</label>
                                                    <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 8 ? (($getIfAlreadyGallery[7]->photo_title != NULL) ? $getIfAlreadyGallery[7]->photo_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6"  style="padding:0px">
                                                    <label for="e1">Select category</label>
                                                    <select class="input-select" style="width:100%" name="photo_category[]" >
                                                        <option value="" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                        <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                        <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                        <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                        <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                        <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                        <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                        <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                        <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                        <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                        <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                        <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                        <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                        <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                        <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                        <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                        <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                        <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                        <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >=8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                        <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                        <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                        <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                        <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                        <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                         <div class="u-progress" id="upload-photo-progress-97" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                                        <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-97" data-croppie data-croppie-file="#uploaded_gal_image87666" data-croppie-input="#uploaded_gal_image8" data-croppie-output="#gal8" data-croppie-bind="file"  style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                        <input type="file" class="d-none" target="uploaded_gal_image87666" id="uploaded_gal_image87666"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                        <input type="hidden" id="uploaded_gal_image8" name="photosectionphoto7">
                                                    </div>
                                                    <div class="u-close" style="float:left; margin-right:20px;">
                                                        <img id="gal8" src="<?php echo ((count($getIfAlreadyGallery) >= 8 ? (($getIfAlreadyGallery[7]->photo != NULL || $getIfAlreadyGallery[7]->photo != '') ? $getIfAlreadyGallery[7]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                        <?php 
                                                            if(count($getIfAlreadyGallery) >= 8)
                                                            {
                                                                if($getIfAlreadyGallery[7]->photo != NULL || $getIfAlreadyGallery[7]->photo != '')
                                                                {
                                                            ?>
                                                        <a id="crossgal8" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[7]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                        </a>
                                                        <?php } } ?>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;" class="d-none" id="addPhotoThreeMore" onclick="addfourmore('3')">Add More 4 photos grid</a>
                                        <a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;" class="d-none" id="removePhotoThreeMore" onclick="removefourmore('3')">Remove 4 photos grid</a>
                                        <div id="portfolio4" class="d-none" style="margin-top:2rem;">
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6" style="padding-left:0px">
                                                    <label for="e1">Photo Title 9 - (3 words)</label>
                                                    <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 9 ? (($getIfAlreadyGallery[8]->photo_title != NULL) ? $getIfAlreadyGallery[8]->photo_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6"  style="padding:0px">
                                                    <label for="e1">Select category</label>
                                                    <select class="input-select" style="width:100%" name="photo_category[]" >
                                                        <option value="" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                        <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                        <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                        <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                        <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                        <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                        <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                        <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                        <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                        <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                        <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                        <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                        <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                        <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                        <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                        <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                        <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                        <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                        <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                        <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                        <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                        <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                        <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                        <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                          <div class="u-progress" id="upload-photo-progress-974" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                                        <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-974" data-croppie data-croppie-file="#photosectionphoto8433" data-croppie-input="#uploaded_gal_image9" data-croppie-output="#gal9" data-croppie-bind="file"  style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                        <input type="file" class="d-none" target="photosectionphoto8433" id="photosectionphoto8433"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                        <input type="hidden" id="uploaded_gal_image9" name="photosectionphoto8">
                                                    </div>
                                                    <div class="u-close" style="float:left; margin-right:20px;">
                                                        <img id="gal9" src="<?php echo ((count($getIfAlreadyGallery) >= 9 ? (($getIfAlreadyGallery[8]->photo != NULL || $getIfAlreadyGallery[8]->photo != '') ? $getIfAlreadyGallery[8]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                        <?php 
                                                            if(count($getIfAlreadyGallery) >= 9)
                                                            {
                                                                if($getIfAlreadyGallery[8]->photo != NULL || $getIfAlreadyGallery[8]->photo != '')
                                                                {
                                                            ?>
                                                        <a id="crossgal9" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[8]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                        </a>
                                                        <?php } } ?>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6" style="padding-left:0px">
                                                    <label for="e1">Photo Title 10 - (3 words)</label>
                                                    <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 10 ? (($getIfAlreadyGallery[9]->photo_title != NULL) ? $getIfAlreadyGallery[9]->photo_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6"  style="padding:0px">
                                                    <label for="e1">Select category</label>
                                                    <select class="input-select" style="width:100%" name="photo_category[]" >
                                                        <option value="" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                        <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                        <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                        <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                        <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                        <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                        <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                        <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                        <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                        <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                        <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                        <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                        <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                        <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                        <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                        <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                        <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                        <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                        <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                        <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                        <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                        <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                        <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                        <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                        <div class="u-progress" id="upload-photo-progress-975" style="display: none; width: 150px">
                                                        <div class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                                                        <span class="sr-only" data-progress="text">0% Complete</span>
                                                        </div>
                                                        </div>
</div>
                                                        <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-975" data-croppie data-croppie-file="#photosectionphoto975" data-croppie-input="#uploaded_gal_image10" data-croppie-output="#gal10" data-croppie-bind="file"  style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                        <input type="file" class="d-none" target="photosectionphoto975" id="photosectionphoto975"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                        <input type="hidden" id="uploaded_gal_image10" name="photosectionphoto9">
                                                    </div>
                                                    <div class="u-close" style="float:left; margin-right:20px;">
                                                        <img id="gal10" src="<?php echo ((count($getIfAlreadyGallery) >= 10 ? (($getIfAlreadyGallery[9]->photo != NULL || $getIfAlreadyGallery[9]->photo != '') ? $getIfAlreadyGallery[9]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                        <?php 
                                                            if(count($getIfAlreadyGallery) >= 10)
                                                            {
                                                                if($getIfAlreadyGallery[9]->photo != NULL || $getIfAlreadyGallery[9]->photo != '')
                                                                {
                                                            ?>
                                                        <a id="crossgal10" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[9]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                        </a>
                                                        <?php } } ?>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6" style="padding-left:0px">
                                                    <label for="e1">Photo Title 11 - (3 words)</label>
                                                    <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 11 ? (($getIfAlreadyGallery[10]->photo_title != NULL) ? $getIfAlreadyGallery[10]->photo_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6"  style="padding:0px">
                                                    <label for="e1">Select category</label>
                                                    <select class="input-select" style="width:100%" name="photo_category[]" >
                                                        <option value="" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                        <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                        <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                        <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                        <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                        <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                        <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                        <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                        <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                        <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                        <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                        <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                        <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                        <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                        <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                        <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                        <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                        <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                        <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                        <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                        <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                        <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                        <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                        <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                        <div class="u-progress" id="upload-photo-progress-975" style="display: none; width: 150px">
	<div class="progress">
	<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
	<span class="sr-only" data-progress="text">0% Complete</span>
	</div>
	</div>
	</div>
                                                        <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)"  data-croppie-progress="#upload-photo-progress-975" data-croppie data-croppie-file="#photosectionphoto975" data-croppie-input="#uploaded_gal_image11" data-croppie-output="#gal11" data-croppie-bind="file"   style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                        <input type="file" class="d-none" target="photosectionphoto975" id="photosectionphoto975"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                        <input type="hidden" id="uploaded_gal_image11" name="photosectionphoto10">
                                                    </div>
                                                    <div class="u-close" style="float:left; margin-right:20px;">
                                                        <img id="gal11" src="<?php echo ((count($getIfAlreadyGallery) >= 11 ? (($getIfAlreadyGallery[10]->photo != NULL || $getIfAlreadyGallery[10]->photo != '') ? $getIfAlreadyGallery[10]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                        <?php 
                                                            if(count($getIfAlreadyGallery) >= 11)
                                                            {
                                                                if($getIfAlreadyGallery[10]->photo != NULL || $getIfAlreadyGallery[10]->photo != '')
                                                                {
                                                            ?>
                                                        <a id="crossgal11" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[10]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                        </a>
                                                        <?php } } ?>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6" style="padding-left:0px">
                                                    <label for="e1">Photo Title 12 - (3 words)</label>
                                                    <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 12 ? (($getIfAlreadyGallery[11]->photo_title != NULL) ? $getIfAlreadyGallery[11]->photo_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6"  style="padding:0px">
                                                    <label for="e1">Select category</label>
                                                    <select class="input-select" style="width:100%" name="photo_category[]" >
                                                        <option value="" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                        <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                        <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                        <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                        <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                        <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                        <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                        <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                        <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                        <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                        <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                        <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                        <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                        <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                        <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                        <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                        <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                        <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                        <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                        <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                        <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                        <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                        <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                        <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                        <div class="u-progress" id="upload-photo-progress-976" style="display: none; width: 150px">
	<div class="progress">
	<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
	<span class="sr-only" data-progress="text">0% Complete</span>
	</div>
	</div>
	</div>
                                                        <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)"   data-croppie-progress="#upload-photo-progress-976" data-croppie data-croppie-file="#photosectionphoto976" data-croppie-input="#uploaded_gal_image12" data-croppie-output="#gal12" data-croppie-bind="file"  style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                        <input type="file" class="d-none" target="photosectionphoto976" id="photosectionphoto976"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                        <input type="hidden" id="uploaded_gal_image12" name="photosectionphoto11">
                                                    </div>
                                                    <div class="u-close" style="float:left; margin-right:20px;">
                                                        <img id="gal12" src="<?php echo ((count($getIfAlreadyGallery) >= 12 ? (($getIfAlreadyGallery[11]->photo != NULL || $getIfAlreadyGallery[11]->photo != '') ? $getIfAlreadyGallery[11]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                        <?php 
                                                            if(count($getIfAlreadyGallery) >= 12)
                                                            {
                                                                if($getIfAlreadyGallery[11]->photo != NULL || $getIfAlreadyGallery[11]->photo != '')
                                                                {
                                                            ?>
                                                        <a id="crossgal12" href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[11]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                        </a>
                                                        <?php } } ?>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div class="btn-save" style="margin-top:2rem;">
                            <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyGallery) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="video" class="tabcontent" >
                <div class="container">
                        <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                            <h2>Video</h2>
                            <p>You can add 6 Videos (Set of 3)</p>
                            Image width:370px height:310px 
                            <div class="col-lg-12">
                                <form action="{{URL::to('/save-production-video')}}" method="post" enctype="multipart/form-data" id="videoform">
                                    <?php 
                                        $userId = session()->get('user_id');
                                        $getIfAlreadyVideo = DB::select('select * from production_video where user_id='.$userId);
                                    ?>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->id != NULL) ? $getIfAlreadyVideo[0]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->video != NULL) ? $getIfAlreadyVideo[0]->video : '') : '')); ?>" />
                                    <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->thumb != NULL) ? $getIfAlreadyVideo[0]->thumb : '') : '')); ?>" />
                                    <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->id != NULL) ? $getIfAlreadyVideo[1]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->video != NULL) ? $getIfAlreadyVideo[1]->video : '') : '')); ?>" />
                                    <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->thumb != NULL) ? $getIfAlreadyVideo[1]->thumb : '') : '')); ?>" />
                                    <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->id != NULL) ? $getIfAlreadyVideo[2]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->video != NULL) ? $getIfAlreadyVideo[2]->video : '') : '')); ?>" />
                                    <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->thumb != NULL) ? $getIfAlreadyVideo[2]->thumb : '') : '')); ?>" />
                                    <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->id != NULL) ? $getIfAlreadyVideo[3]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->video != NULL) ? $getIfAlreadyVideo[3]->video : '') : '')); ?>" />
                                    <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->thumb != NULL) ? $getIfAlreadyVideo[3]->thumb : '') : '')); ?>" />
                                    <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->id != NULL) ? $getIfAlreadyVideo[4]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->video != NULL) ? $getIfAlreadyVideo[4]->video : '') : '')); ?>" />
                                    <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->thumb != NULL) ? $getIfAlreadyVideo[4]->thumb : '') : '')); ?>" />
                                    <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->id != NULL) ? $getIfAlreadyVideo[5]->id : '') : '')); ?>" />
                                    <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->video != NULL) ? $getIfAlreadyVideo[5]->video : '') : '')); ?>" />
                                    <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->thumb != NULL) ? $getIfAlreadyVideo[5]->thumb : '') : '')); ?>" />
                                    <div class="u-form">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                                    <label for="e1">Video Title - Position 1</label>
                                                    <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->video_title != NULL) ? $getIfAlreadyVideo[0]->video_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6" style="padding:0px;">
                                                    <label for="e1">Select category</label>
                                                    <select class="form-control input-select" style="padding: 10px;" name="categoryvideo[]">
                                                        <option value="" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                                        <option value="Co-Director" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                        <option value="Film Director" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                        <option value="Short Film Director" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                        <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                        <option value="Art Director" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                        <option value="Music Director" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                        <option value="Unit Director" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                        <option value="Assistant Director" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                        <option value="Corporate Film Director" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Corporate Film Director") ? 'selected' : '') : '') : ''); ?>>Corporate Film Director</option>
                                                        <option value="Stunt director" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Stunt director") ? 'selected' : '') : '') : ''); ?>>Stunt director</option>
                                                        <option value="Film Production house" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Film Production house") ? 'selected' : '') : '') : ''); ?>>Film Production house</option>
                                                        <option value="Film Producer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                        <option value="Tv Serial Production house" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Tv Serial Production house") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production house</option>
                                                        <option value="Tv Serial poroducer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Tv Serial poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial poroducer</option>
                                                        <option value="Casting Director" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                        <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                        <option value="Casting Agent" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>>Casting Agent</option>
                                                        <option value="Executive Producer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>>Executive Producer</option>
                                                        <option value="Production Manager" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>>Production Manager</option>
                                                        <option value="Associate producer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>>Associate producer</option>
                                                        <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>>Adv Film Maker</option>
                                                        <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Model Co-ordinator</option>
                                                        <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                            Others
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="e1">Video Description </label>
                                                    <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->description != NULL) ? $getIfAlreadyVideo[0]->description : '') : '')); ?></textarea>
                                                </div>
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="uploadVideo('1')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                                        </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                    <input type="file" class="d-none" id="videoFile1">
                                                    <input type="hidden" name="uploadedvideosec1" id="uploadedvideosec1">
                                                    <input type="hidden" id="generatedvideosecthumb1" name="generatedvideosecthumb1">
                                                </div>
                                                
                                                <div class="u-progress" id="upload-photo-progress-8" style="display: none; width: 150px">
                                                <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                                                    <span class="sr-only" data-progress="text">0% Complete</span>
                                                </div>
                                                </div>
                                                </div>
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                     <!--onclick="uploadVideoThumbnail('1')"-->
                                                    <div class="browse-photo" data-croppie-progress="#upload-photo-progress-8"  data-croppie data-croppie-file="#videoThumb1" data-croppie-input="#uploaded_vid_image1" data-croppie-output="#previewvideosec1" data-croppie-bind="file" data-croppie-viewport='{"width": 370, "height": 310}' data-croppie-boundary='{"width": 370, "height": 310}'><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                    <input type="file" class="d-none" target="videoThumbFile1" id="videoThumb1" class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                    <input type="hidden" id="uploaded_vid_image1" name="videoThumbFile1">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img id="previewvideosec1" src="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->thumb != NULL || $getIfAlreadyVideo[0]->thumb != '') ? $getIfAlreadyVideo[0]->thumb : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyVideo) >= 1)
                                                        {
                                                            if($getIfAlreadyVideo[0]->thumb != NULL || $getIfAlreadyVideo[0]->thumb != '')
                                                            {
                                                        ?>
                                                    <a id="crossvid1" href="<?php echo URL::to('/delete-production-video-image/'.$getIfAlreadyVideo[0]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                                <div class="u-progress1" style="display:none;">
                                                    <div class="row">
                                                        <div class="col-lg-12 ud-caption">Upload Details</div>
                                                        <div class="col-lg-10">
                                                            <div class="u-title"></div>
                                                            <div id="upload-progress1">
                                                                <div class="u-size1"></div>
                                                                <div class="u-progress1">
                                                                    <div class="progress1">
                                                                        <div class="progress-bar1" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                            <span class="sr-only1">0% Complete</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="u-close">
                                                                        <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="u-desc1"><span class="percent-process1"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                                    <label for="e1">Video Title - Position 2</label>
                                                    <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->video_title != NULL) ? $getIfAlreadyVideo[1]->video_title : '') : '')); ?>">
                                                </div>
                                                <div class="form-group col-lg-6" style="padding:0px;">
                                                    <label for="e1">Select category</label>
                                                    <select class="form-control input-select" style="padding: 10px;" name="categoryvideo[]">
                                                        <option value="" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                                        <option value="Co-Director" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                        <option value="Film Director" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                        <option value="Short Film Director" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                        <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                        <option value="Art Director" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                        <option value="Music Director" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                        <option value="Unit Director" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                        <option value="Assistant Director" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                        <option value="Corporate Film Director" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Corporate Film Director") ? 'selected' : '') : '') : ''); ?>>Corporate Film Director</option>
                                                        <option value="Stunt director" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Stunt director") ? 'selected' : '') : '') : ''); ?>>Stunt director</option>
                                                        <option value="Film Production house" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Film Production house") ? 'selected' : '') : '') : ''); ?>>Film Production house</option>
                                                        <option value="Film Producer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                        <option value="Tv Serial Production house" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Tv Serial Production house") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production house</option>
                                                        <option value="Tv Serial poroducer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Tv Serial poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial poroducer</option>
                                                        <option value="Casting Director" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                        <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                        <option value="Casting Agent" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>>Casting Agent</option>
                                                        <option value="Executive Producer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>>Executive Producer</option>
                                                        <option value="Production Manager" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>>Production Manager</option>
                                                        <option value="Associate producer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>>Associate producer</option>
                                                        <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>>Adv Film Maker</option>
                                                        <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Model Co-ordinator</option>
                                                        <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                        <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                            Others
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="e1">Video Description </label>
                                                    <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->description != NULL) ? $getIfAlreadyVideo[1]->description : '') : '')); ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                        <div class="browse-photo" onclick="uploadVideo('2')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                        <input type="file" class="d-none" id="videoFile2">
                                                        <input type="hidden" name="uploadedvideosec2" id="uploadedvideosec2">
                                                        <input type="hidden" name="generatedvideosecthumb2" id="generatedvideosecthumb2">
                                                    </div>
                                                    <div class="u-progress" id="upload-photo-progress-9" style="display: none; width: 150px">
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
            <span class="sr-only" data-progress="text">0% Complete</span>
        </div>
    </div>
</div>
                                                    <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                         <!--onclick="uploadVideoThumbnail('2')"-->
                                                        <div class="browse-photo" data-croppie-progress="#upload-photo-progress-9" data-croppie data-croppie-file="#videoThumb2" data-croppie-input="#uploaded_vid_image2" data-croppie-output="#previewvideosec2" data-croppie-bind="file" data-croppie-viewport='{"width": 370, "height": 310}' data-croppie-boundary='{"width": 370, "height": 310}'><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                        </div>
                                                        <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                        <input type="file" class="d-none" target="videoThumbFile2" id="videoThumb2"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                        <input type="hidden" id="uploaded_vid_image2" name="videoThumbFile2">
                                                    </div>
                                                    <div class="u-close" style="float:left; margin-right:20px;">
                                                        <img id="previewvideosec2" src="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->thumb != NULL || $getIfAlreadyVideo[1]->thumb != '') ? $getIfAlreadyVideo[1]->thumb : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                        <?php 
                                                            if(count($getIfAlreadyVideo) >= 2)
                                                            {
                                                                if($getIfAlreadyVideo[1]->thumb != NULL || $getIfAlreadyVideo[1]->thumb != '')
                                                                {
                                                            ?>
                                                        <a id="crossvid2" href="<?php echo URL::to('/delete-production-video-image/'.$getIfAlreadyVideo[1]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                        <i class="cvicon-cv-cancel"></i>
                                                        </a>
                                                        <?php } } ?>
                                                    </div>
                                                    <div style="clear:both; overflow:hidden"></div>
                                                    <div class="u-progress2" style="display:none;">
                                                        <div class="row">
                                                            <div class="col-lg-12 ud-caption">Upload Details</div>
                                                            <div class="col-lg-10">
                                                                <div class="u-title"></div>
                                                                <div id="upload-progress2">
                                                                    <div class="u-size2"></div>
                                                                    <div class="u-progress2">
                                                                        <div class="progress2">
                                                                            <div class="progress-bar2" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                                <span class="sr-only2">0% Complete</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="u-close">
                                                                            <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="u-desc2"><span class="percent-process2"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                                <label for="e1">Video Title - Position 3</label>
                                                <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->video_title != NULL) ? $getIfAlreadyVideo[2]->video_title : '') : '')); ?>">
                                            </div>
                                            <div class="form-group col-lg-6" style="padding:0px;">
                                                <label for="e1">Select category</label>
                                                <select class="form-control input-select" style="padding: 10px;" name="categoryvideo[]">
                                                    <option value="" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                                    <option value="Co-Director" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                    <option value="Film Director" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                    <option value="Short Film Director" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                    <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                    <option value="Art Director" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                    <option value="Music Director" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                    <option value="Unit Director" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                    <option value="Assistant Director" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                    <option value="Corporate Film Director" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Corporate Film Director") ? 'selected' : '') : '') : ''); ?>>Corporate Film Director</option>
                                                    <option value="Stunt director" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Stunt director") ? 'selected' : '') : '') : ''); ?>>Stunt director</option>
                                                    <option value="Film Production house" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Film Production house") ? 'selected' : '') : '') : ''); ?>>Film Production house</option>
                                                    <option value="Film Producer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                    <option value="Tv Serial Production house" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Tv Serial Production house") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production house</option>
                                                    <option value="Tv Serial poroducer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Tv Serial poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial poroducer</option>
                                                    <option value="Casting Director" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                    <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                    <option value="Casting Agent" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>>Casting Agent</option>
                                                    <option value="Executive Producer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>>Executive Producer</option>
                                                    <option value="Production Manager" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>>Production Manager</option>
                                                    <option value="Associate producer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>>Associate producer</option>
                                                    <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>>Adv Film Maker</option>
                                                    <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Model Co-ordinator</option>
                                                    <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                    <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                        Others
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="e1">Video Description </label>
                                                <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->description != NULL) ? $getIfAlreadyVideo[2]->description : '') : '')); ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                    <div class="browse-photo" onclick="uploadVideo('3')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                    <input type="file" class="d-none" id="videoFile3">
                                                    <input type="hidden" name="uploadedvideosec3" id="uploadedvideosec3">
                                                    <input type="hidden" name="generatedvideosecthumb3" id="generatedvideosecthumb3">
                                                </div>
                                                <div class="u-progress" id="upload-photo-progress-10" style="display: none; width: 150px">
    <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
            <span class="sr-only" data-progress="text">0% Complete</span>
        </div>
    </div>
</div>
                                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                     <!--onclick="uploadVideoThumbnail('3')"-->
                                                    <div class="browse-photo" data-croppie data-croppie-file="#videoThumb3" data-croppie-progress="#upload-photo-progress-10" data-croppie-input="#uploaded_vid_image3" data-croppie-output="#previewvideosec3" data-croppie-bind="file" data-croppie-viewport='{"width": 370, "height": 310}' data-croppie-boundary='{"width": 370, "height": 310}'><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                    </div>
                                                    <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                    <input type="file" class="d-none" target="videoThumbFile3" id="videoThumb3"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                    <input type="hidden" id="uploaded_vid_image3" name="videoThumbFile3">
                                                </div>
                                                <div class="u-close" style="float:left; margin-right:20px;">
                                                    <img id="previewvideosec3" src="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->thumb != NULL || $getIfAlreadyVideo[2]->thumb != '') ? $getIfAlreadyVideo[2]->thumb : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                    <?php 
                                                        if(count($getIfAlreadyVideo) >= 3)
                                                        {
                                                            if($getIfAlreadyVideo[2]->thumb != NULL || $getIfAlreadyVideo[2]->thumb != '')
                                                            {
                                                        ?>
                                                    <a id="crossvid3" href="<?php echo URL::to('/delete-production-video-image/'.$getIfAlreadyVideo[2]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                    <i class="cvicon-cv-cancel"></i>
                                                    </a>
                                                    <?php } } ?>
                                                </div>
                                                <div style="clear:both; overflow:hidden"></div>
                                                <div class="u-progress3" style="display:none;">
                                                    <div class="row">
                                                        <div class="col-lg-12 ud-caption">Upload Details</div>
                                                        <div class="col-lg-10">
                                                            <div class="u-title"></div>
                                                            <div id="upload-progress3">
                                                                <div class="u-size3"></div>
                                                                <div class="u-progress3">
                                                                    <div class="progress3">
                                                                        <div class="progress-bar3" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                            <span class="sr-only3">0% Complete</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="u-close">
                                                                        <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="u-desc3"><span class="percent-process3"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" id="videoAdd" onclick="addMoreVideo()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Add More 3 Video grid</a>
                                        <a href="javascript:void(0)" id="videoRemove" class="d-none" onclick="removeMoreVideo()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Remove Video grid</a>
                                        <div id="clonevideodiv" class="d-none" style="margin: 5px 0 20px 0;">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                                        <label for="e1">Video Title - Position 4</label>
                                                        <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->video_title != NULL) ? $getIfAlreadyVideo[3]->video_title : '') : '')); ?>">
                                                    </div>
                                                    <div class="form-group col-lg-6" style="padding:0px;">
                                                        <label for="e1">Select category </label>
                                                        <select class="form-control input-select" style="padding: 10px;" name="categoryvideo[]">
                                                            <option value="" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                                            <option value="Co-Director" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                            <option value="Film Director" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                            <option value="Short Film Director" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                            <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                            <option value="Art Director" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                            <option value="Music Director" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                            <option value="Unit Director" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                            <option value="Assistant Director" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                            <option value="Corporate Film Director" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Corporate Film Director") ? 'selected' : '') : '') : ''); ?>>Corporate Film Director</option>
                                                            <option value="Stunt director" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Stunt director") ? 'selected' : '') : '') : ''); ?>>Stunt director</option>
                                                            <option value="Film Production house" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Film Production house") ? 'selected' : '') : '') : ''); ?>>Film Production house</option>
                                                            <option value="Film Producer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                            <option value="Tv Serial Production house" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Tv Serial Production house") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production house</option>
                                                            <option value="Tv Serial poroducer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Tv Serial poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial poroducer</option>
                                                            <option value="Casting Director" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                            <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                            <option value="Casting Agent" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>>Casting Agent</option>
                                                            <option value="Executive Producer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>>Executive Producer</option>
                                                            <option value="Production Manager" <?php echo ((count($getIfAlreadyVideo) >= 4)? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>>Production Manager</option>
                                                            <option value="Associate producer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>>Associate producer</option>
                                                            <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>>Adv Film Maker</option>
                                                             <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Model Co-ordinator</option>
                                                            <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                            <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                                 Others
                                                            </option>                                            
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="e1">Video Description </label>
                                                        <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->description != NULL) ? $getIfAlreadyVideo[3]->description : '') : '')); ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                            <div class="browse-photo" onclick="uploadVideo('4')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                                            </div>
                                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                            <input type="file" class="d-none" id="videoFile4">
                                                            <input type="hidden" name="uploadedvideosec4" id="uploadedvideosec4">
                                                            <input type="hidden" name="generatedvideosecthumb4" id="generatedvideosecthumb4">
                                                        </div>
                                                        <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                            <div class="u-progress" id="upload-photo-progress-977" style="display: none; width: 150px">
	<div class="progress">
	<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
	<span class="sr-only" data-progress="text">0% Complete</span>
	</div>
	</div>
	</div>
                                                            <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                            </div>
                                                            <a href="javascript:void(0)"  data-croppie-progress="#upload-photo-progress-977" data-croppie data-croppie-file="#videoThumbFileth" data-croppie-input="#uploaded_vid_image4" data-croppie-output="#previewvideosec4" data-croppie-bind="file"  data-croppie-viewport='{"width": 370, "height": 310}' data-croppie-boundary='{"width": 370, "height": 310}' style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                            <input type="file" class="d-none" target="videoThumbFileth" id="videoThumbFileth"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                            <input type="hidden" id="uploaded_vid_image4" name="videoThumbFile4">
                                                        </div>
                                                        <div class="u-close" style="float:left; margin-right:20px;">
                                                            <img id="previewvideosec4" src="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->thumb != NULL || $getIfAlreadyVideo[3]->thumb != '') ? $getIfAlreadyVideo[3]->thumb : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                            <?php 
                                                                if(count($getIfAlreadyVideo) >= 4)
                                                                {
                                                                    if($getIfAlreadyVideo[3]->thumb != NULL || $getIfAlreadyVideo[3]->thumb != '')
                                                                    {
                                                                ?>
                                                            <a id="crossvid4" href="<?php echo URL::to('/delete-production-video-image/'.$getIfAlreadyVideo[3]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                            <i class="cvicon-cv-cancel"></i>
                                                            </a>
                                                            <?php } } ?>
                                                        </div>
                                                        <div style="clear:both; overflow:hidden"></div>
                                                        <div class="u-progress4" style="display:none;">
                                                            <div class="row">
                                                                <div class="col-lg-12 ud-caption">Upload Details</div>
                                                                <div class="col-lg-10">
                                                                    <div class="u-title"></div>
                                                                    <div id="upload-progress4">
                                                                        <div class="u-size4"></div>
                                                                        <div class="u-progress4">
                                                                            <div class="progress4">
                                                                                <div class="progress-bar4" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                                    <span class="sr-only4">0% Complete</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="u-close">
                                                                                <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="u-desc4"><span class="percent-process4"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                                        <label for="e1">Video Title - Position 5</label>
                                                        <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->video_title != NULL) ? $getIfAlreadyVideo[4]->video_title : '') : '')); ?>">
                                                    </div>
                                                    <div class="form-group col-lg-6" style="padding:0px;">
                                                        <label for="e1">Select category</label>
                                                        <select class="form-control input-select" style="padding: 10px;" name="categoryvideo[]">
                                                            <option value="" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                                            <option value="Co-Director" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                            <option value="Film Director" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                            <option value="Short Film Director" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                            <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                            <option value="Art Director" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                            <option value="Music Director" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                            <option value="Unit Director" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                            <option value="Assistant Director" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                            <option value="Corporate Film Director" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Corporate Film Director") ? 'selected' : '') : '') : ''); ?>>Corporate Film Director</option>
                                                            <option value="Stunt director" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Stunt director") ? 'selected' : '') : '') : ''); ?>>Stunt director</option>
                                                            <option value="Film Production house" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Film Production house") ? 'selected' : '') : '') : ''); ?>>Film Production house</option>
                                                            <option value="Film Producer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                            <option value="Tv Serial Production house" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Tv Serial Production house") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production house</option>
                                                            <option value="Tv Serial poroducer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Tv Serial poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial poroducer</option>
                                                            <option value="Casting Director" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                            <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                            <option value="Casting Agent" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>>Casting Agent</option>
                                                            <option value="Executive Producer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>>Executive Producer</option>
                                                            <option value="Production Manager" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>>Production Manager</option>
                                                            <option value="Associate producer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>>Associate producer</option>
                                                            <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>>Adv Film Maker</option>
                                                            <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Model Co-ordinator</option>
                                                            <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                            <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                                Others
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="e1">Video Description </label>
                                                        <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->description != NULL) ? $getIfAlreadyVideo[4]->description : '') : '')); ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                            <div class="browse-photo" onclick="uploadVideo('5')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                                            </div>
                                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                            <input type="file" class="d-none" id="videoFile5">
                                                            <input type="hidden" name="uploadedvideosec5" id="uploadedvideosec5">
                                                            <input type="hidden" name="generatedvideosecthumb5" id="generatedvideosecthumb5">
                                                        </div>
                                                        <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                            <div class="u-progress" id="upload-photo-progress-979" style="display: none; width: 150px">
	<div class="progress">
	<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
	<span class="sr-only" data-progress="text">0% Complete</span>
	</div>
	</div>
	</div>
                                                            <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                            </div>
                                                            <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-979" data-croppie data-croppie-file="#videoThumbFile979" data-croppie-input="#uploaded_vid_image5" data-croppie-output="#previewvideosec5" data-croppie-bind="file"  data-croppie-viewport='{"width": 370, "height": 310}' data-croppie-boundary='{"width": 370, "height": 310}' style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                            <input type="file" class="d-none" target="videoThumbFile979" id="videoThumbFile979"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                            <input type="hidden" id="uploaded_vid_image5" name="videoThumbFile5">
                                                        </div>
                                                        <div class="u-close" style="float:left; margin-right:20px;">
                                                            <img id="previewvideosec5" src="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->thumb != NULL || $getIfAlreadyVideo[4]->thumb != '') ? $getIfAlreadyVideo[4]->thumb : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                            <?php 
                                                                if(count($getIfAlreadyVideo) >= 5)
                                                                {
                                                                    if($getIfAlreadyVideo[4]->thumb != NULL || $getIfAlreadyVideo[4]->thumb != '')
                                                                    {
                                                                ?>
                                                            <a id="crossvid5" href="<?php echo URL::to('/delete-production-video-image/'.$getIfAlreadyVideo[4]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                            <i class="cvicon-cv-cancel"></i>
                                                            </a>
                                                            <?php } } ?>
                                                        </div>
                                                        <div style="clear:both; overflow:hidden"></div>
                                                        <div class="u-progress5" style="display:none;">
                                                            <div class="row">
                                                                <div class="col-lg-12 ud-caption">Upload Details</div>
                                                                <div class="col-lg-10">
                                                                    <div class="u-title"></div>
                                                                    <div id="upload-progress5">
                                                                        <div class="u-size5"></div>
                                                                        <div class="u-progress5">
                                                                            <div class="progress5">
                                                                                <div class="progress-bar5" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                                    <span class="sr-only5">0% Complete</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="u-close">
                                                                                <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="u-desc5"><span class="percent-process5"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                                        <label for="e1">Video Title - Position 6</label>
                                                        <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->video_title != NULL) ? $getIfAlreadyVideo[5]->video_title : '') : '')); ?>">
                                                    </div>
                                                    <div class="form-group col-lg-6" style="padding:0px;">
                                                        <label for="e1">Select category</label>
                                                        <select class="form-control input-select" style="padding: 10px;" name="categoryvideo[]">
                                                            <option value="" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                                            <option value="Co-Director" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                            <option value="Film Director" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                            <option value="Short Film Director" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                            <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                            <option value="Art Director" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                            <option value="Music Director" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                            <option value="Unit Director" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                            <option value="Assistant Director" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                            <option value="Corporate Film Director" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Corporate Film Director") ? 'selected' : '') : '') : ''); ?>>Corporate Film Director</option>
                                                            <option value="Stunt director" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Stunt director") ? 'selected' : '') : '') : ''); ?>>Stunt director</option>
                                                            <option value="Film Production house" <?php echo ((count($getIfAlreadyVideo) >=6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Film Production house") ? 'selected' : '') : '') : ''); ?>>Film Production house</option>
                                                            <option value="Film Producer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                            <option value="Tv Serial Production house" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Tv Serial Production house") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production house</option>
                                                            <option value="Tv Serial poroducer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Tv Serial poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial poroducer</option>
                                                            <option value="Casting Director" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                            <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                            <option value="Casting Agent" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>>Casting Agent</option>
                                                            <option value="Executive Producer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>>Executive Producer</option>
                                                            <option value="Production Manager" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>>Production Manager</option>
                                                            <option value="Associate producer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>>Associate producer</option>
                                                            <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>>Adv Film Maker</option>
                                                            <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Model Co-ordinator</option>
                                                            <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                            <option value="Others" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "Others") ? 'selected' : '') : '') : ''); ?>>
                                                                Others
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="e1">Video Description </label>
                                                        <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->description != NULL) ? $getIfAlreadyVideo[5]->description : '') : '')); ?></textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                            <div class="browse-photo" onclick="uploadVideo('6')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                                            </div>
                                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                                            <input type="file" class="d-none" id="videoFile6">
                                                            <input type="hidden" name="uploadedvideosec6" id="uploadedvideosec6">
                                                            <input type="hidden" name="generatedvideosecthumb6" id="generatedvideosecthumb6">
                                                        </div>
                                                        <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                            <div class="u-progress" id="upload-photo-progress-980" style="display: none; width: 150px">
	<div class="progress">
	<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
	<span class="sr-only" data-progress="text">0% Complete</span>
	</div>
	</div>
	</div>
                                                            <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                            </div>
                                                            <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-980" data-croppie data-croppie-file="#videoThumbFile980" data-croppie-input="#uploaded_vid_image6" data-croppie-output="#previewvideosec6" data-croppie-bind="file"  data-croppie-viewport='{"width": 370, "height": 310}' data-croppie-boundary='{"width": 370, "height": 310}'  style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                                            <input type="file" class="d-none" target="videoThumbFile980" id="videoThumbFile980"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                                            <input type="hidden" id="uploaded_vid_image6" name="videoThumbFile6">
                                                        </div>
                                                        <div class="u-close" style="float:left; margin-right:20px;">
                                                            <img id="previewvideosec6" src="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->thumb != NULL || $getIfAlreadyVideo[5]->thumb != '') ? $getIfAlreadyVideo[5]->thumb : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                                            <?php 
                                                                if(count($getIfAlreadyVideo) >= 6)
                                                                {
                                                                    if($getIfAlreadyVideo[5]->thumb != NULL || $getIfAlreadyVideo[5]->thumb != '')
                                                                    {
                                                                ?>
                                                            <a id="crossvid6" href="<?php echo URL::to('/delete-production-video-image/'.$getIfAlreadyVideo[5]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                            <i class="cvicon-cv-cancel"></i>
                                                            </a>
                                                            <?php } } ?>
                                                        </div>
                                                        <div style="clear:both; overflow:hidden"></div>
                                                        <div class="u-progress6" style="display:none;">
                                                            <div class="row">
                                                                <div class="col-lg-12 ud-caption">Upload Details</div>
                                                                <div class="col-lg-10">
                                                                    <div class="u-title"></div>
                                                                    <div id="upload-progress6">
                                                                        <div class="u-size6"></div>
                                                                        <div class="u-progress6">
                                                                            <div class="progress6">
                                                                                <div class="progress-bar6" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                                                    <span class="sr-only6">0% Complete</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="u-close">
                                                                                <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="u-desc6"><span class="percent-process6"></span>&nbsp;&nbsp;Your Video is still uploading, please keep this page open until it's done.</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row col-md-12 btn-save">
                                        <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyVideo) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
            </div>
            <div id="team" class="tabcontent">
                <div class="container">
                    <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <h2>Add Team member (Add 12 member)</h2>
                        <form action="{{URL::to('/save-production-team')}}" method="post" enctype="multipart/form-data" id="teamform">
                            <?php 
                                $userId = session()->get('user_id');
                                $getIfAlreadyTeam = DB::select('select * from production_my_team where user_id='.$userId);
                            ?>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            
                            <input type="hidden" name="oldTeamid[]" value="<?php echo ((count($getIfAlreadyTeam) >= 1 ? (($getIfAlreadyTeam[0]->id != NULL) ? $getIfAlreadyTeam[0]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldTeamPhoto[]" value="<?php echo ((count($getIfAlreadyTeam) >= 1 ? (($getIfAlreadyTeam[0]->photo != NULL) ? $getIfAlreadyTeam[0]->photo : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldTeamid[]" value="<?php echo ((count($getIfAlreadyTeam) >= 2 ? (($getIfAlreadyTeam[1]->id != NULL) ? $getIfAlreadyTeam[1]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldTeamPhoto[]" value="<?php echo ((count($getIfAlreadyTeam) >= 2 ? (($getIfAlreadyTeam[1]->photo != NULL) ? $getIfAlreadyTeam[1]->photo : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldTeamid[]" value="<?php echo ((count($getIfAlreadyTeam) >= 3 ? (($getIfAlreadyTeam[2]->id != NULL) ? $getIfAlreadyTeam[2]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldTeamPhoto[]" value="<?php echo ((count($getIfAlreadyTeam) >= 3 ? (($getIfAlreadyTeam[2]->photo != NULL) ? $getIfAlreadyTeam[2]->photo : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldTeamid[]" value="<?php echo ((count($getIfAlreadyTeam) >= 4 ? (($getIfAlreadyTeam[3]->id != NULL) ? $getIfAlreadyTeam[3]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldTeamPhoto[]" value="<?php echo ((count($getIfAlreadyTeam) >= 4 ? (($getIfAlreadyTeam[3]->photo != NULL) ? $getIfAlreadyTeam[3]->photo : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldTeamid[]" value="<?php echo ((count($getIfAlreadyTeam) >= 5 ? (($getIfAlreadyTeam[4]->id != NULL) ? $getIfAlreadyTeam[4]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldTeamPhoto[]" value="<?php echo ((count($getIfAlreadyTeam) >= 5 ? (($getIfAlreadyTeam[4]->photo != NULL) ? $getIfAlreadyTeam[4]->photo : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldTeamid[]" value="<?php echo ((count($getIfAlreadyTeam) >= 6 ? (($getIfAlreadyTeam[5]->id != NULL) ? $getIfAlreadyTeam[5]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldTeamPhoto[]" value="<?php echo ((count($getIfAlreadyTeam) >= 6 ? (($getIfAlreadyTeam[5]->photo != NULL) ? $getIfAlreadyTeam[5]->photo : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldTeamid[]" value="<?php echo ((count($getIfAlreadyTeam) >= 7 ? (($getIfAlreadyTeam[6]->id != NULL) ? $getIfAlreadyTeam[6]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldTeamPhoto[]" value="<?php echo ((count($getIfAlreadyTeam) >= 7 ? (($getIfAlreadyTeam[6]->photo != NULL) ? $getIfAlreadyTeam[6]->photo : '') : '')); ?>" />
                            
                            
                            <input type="hidden" name="oldTeamid[]" value="<?php echo ((count($getIfAlreadyTeam) >= 8 ? (($getIfAlreadyTeam[7]->id != NULL) ? $getIfAlreadyTeam[7]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldTeamPhoto[]" value="<?php echo ((count($getIfAlreadyTeam) >= 8 ? (($getIfAlreadyTeam[7]->photo != NULL) ? $getIfAlreadyTeam[7]->photo : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldTeamid[]" value="<?php echo ((count($getIfAlreadyTeam) >= 9 ? (($getIfAlreadyTeam[8]->id != NULL) ? $getIfAlreadyTeam[8]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldTeamPhoto[]" value="<?php echo ((count($getIfAlreadyTeam) >= 9 ? (($getIfAlreadyTeam[8]->photo != NULL) ? $getIfAlreadyTeam[8]->photo : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldTeamid[]" value="<?php echo ((count($getIfAlreadyTeam) >= 10 ? (($getIfAlreadyTeam[9]->id != NULL) ? $getIfAlreadyTeam[9]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldTeamPhoto[]" value="<?php echo ((count($getIfAlreadyTeam) >= 10 ? (($getIfAlreadyTeam[9]->photo != NULL) ? $getIfAlreadyTeam[9]->photo : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldTeamid[]" value="<?php echo ((count($getIfAlreadyTeam) >= 11 ? (($getIfAlreadyTeam[10]->id != NULL) ? $getIfAlreadyTeam[10]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldTeamPhoto[]" value="<?php echo ((count($getIfAlreadyTeam) >= 11 ? (($getIfAlreadyTeam[10]->photo != NULL) ? $getIfAlreadyTeam[10]->photo : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldTeamid[]" value="<?php echo ((count($getIfAlreadyTeam) >= 12 ? (($getIfAlreadyTeam[11]->id != NULL) ? $getIfAlreadyTeam[11]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldTeamPhoto[]" value="<?php echo ((count($getIfAlreadyTeam) >= 12 ? (($getIfAlreadyTeam[11]->photo != NULL) ? $getIfAlreadyTeam[11]->photo : '') : '')); ?>" />
                            
                            <div class="form-group col-lg-4" style="position:relative">
                                <label for="exampleInputEmail1">Position</label>  
                                <input type="text" name="position1" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlreadyTeam) >= 1 ? (($getIfAlreadyTeam[0]->position != NULL) ? $getIfAlreadyTeam[0]->position : '') : '')); ?>"> 
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 1)
                                    {
                                ?>
                                <div class="u-progress">
                                    <div class="u-close cgt">
                                        <a href="{{ URL::to('/delete-team') }}/<?php echo $getIfAlreadyTeam[0]->id; ?>"><i class="cvicon-cv-cancel"></i></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Search By Name</label>  
                                <?php 
                                    $userId = session()->get('user_id');
                                    $getFollowers = DB::table('followers')->where('user_id',$userId)->get();
                                ?>
                                <select class="select2 input-select" style="width:100%" name="name1" onchange="getTeamImage(this.value,1)">
                                    <?php 
                                        if(count($getFollowers) > 0)
                                        {
                                            foreach($getFollowers as $k=>$v)
                                            {
                                                $getUserDetail = DB::table('users')->where('id',$v->follows_id)->get();
                                    ?>
                                    <?php 
                                        if($k == 0)
                                        {
                                    ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 1 ? (($getIfAlreadyTeam[0]->search_by_name != NULL) ? (($getIfAlreadyTeam[0]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                    <option value="<?php echo $getUserDetail[0]->name; ?>"<?php echo ((count($getIfAlreadyTeam) >= 1 ? (($getIfAlreadyTeam[0]->search_by_name != NULL) ? (($getIfAlreadyTeam[0]->search_by_name == $getUserDetail[0]->name) ? 'selected' : '') : '') : '')); ?>><?php echo $getUserDetail[0]->name; ?></option>
                                    <?php } }else{ ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 1 ? (($getIfAlreadyTeam[0]->search_by_name != NULL) ? (($getIfAlreadyTeam[0]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                </select>
                                <p style="margin-top:4px"> Note: Your team member should be in your following list. <a href="javascript:void(0)" style="color:red">Follow them first</a></p>
                            </div>
                            <div class="u-progress" id="upload-photo-progress-11" style="display: none; width: 150px">
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                <span class="sr-only" data-progress="text">0% Complete</span>
            </div>
        </div>
    </div>
                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                <div class="browse-photo" data-croppie-output="#previewteamsec14" data-croppie data-croppie-file="#team13" data-croppie-input="#uploaded_team_image133" data-croppie-bind="file" data-croppie-progress="#upload-photo-progress-11" 
><i class="fa fa-picture-o" aria-hidden="true"></i>
                                </div>
                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Image</a>
                                <input type="file" class="d-none" target="team13" id="team13"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                <input type="hidden" id="uploaded_team_image133" name="team1">
                            </div>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img id="previewteamsec14" src="<?php echo ((count($getIfAlreadyTeam) >= 1 ? (($getIfAlreadyTeam[0]->photo != NULL || $getIfAlreadyTeam[0]->photo != '') ? $getIfAlreadyTeam[0]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 1)
                                    {
                                        if($getIfAlreadyTeam[0]->photo != NULL || $getIfAlreadyTeam[0]->photo != '')
                                        {
                                    ?>
                                <a id="crossteam1" href="<?php echo URL::to('/delete-team-image/'.$getIfAlreadyTeam[0]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="clear"></div>
                            
                            <div class="form-group col-lg-4" style="position:relative">
                                <label for="exampleInputEmail1">Position</label>  
                                <input type="text" name="position2" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlreadyTeam) >= 2 ? (($getIfAlreadyTeam[1]->position != NULL) ? $getIfAlreadyTeam[1]->position : '') : '')); ?>"> 
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 2)
                                    {
                                ?>
                                <div class="u-progress">
                                    <div class="u-close cgt">
                                        <a href="{{ URL::to('/delete-team') }}/<?php echo $getIfAlreadyTeam[1]->id; ?>"><i class="cvicon-cv-cancel"></i></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Search By Name</label>  
                                <select class="select2 input-select" style="width:100%" name="name2" onchange="getTeamImage(this.value,2)">
                                    <?php 
                                        if(count($getFollowers) > 0)
                                        {
                                            foreach($getFollowers as $k=>$v)
                                            {
                                                $getUserDetail = DB::table('users')->where('id',$v->follows_id)->get();
                                    ?>
                                    <?php 
                                        if($k == 0)
                                        {
                                    ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 2 ? (($getIfAlreadyTeam[1]->search_by_name != NULL) ? (($getIfAlreadyTeam[1]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                    <option value="<?php echo $getUserDetail[0]->name; ?>"<?php echo ((count($getIfAlreadyTeam) >= 2 ? (($getIfAlreadyTeam[1]->search_by_name != NULL) ? (($getIfAlreadyTeam[1]->search_by_name == $getUserDetail[0]->name) ? 'selected' : '') : '') : '')); ?>><?php echo $getUserDetail[0]->name; ?></option>
                                    <?php } }else{ ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 2 ? (($getIfAlreadyTeam[1]->search_by_name != NULL) ? (($getIfAlreadyTeam[1]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="u-progress" id="upload-photo-progress-12" style="display: none; width: 150px">
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                <span class="sr-only" data-progress="text">0% Complete</span>
            </div>
        </div>
    </div>
                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                <div class="browse-photo" data-croppie-output="#previewteamsec232" data-croppie data-croppie-file="#team1333" data-croppie-input="#uploaded_team_image755" data-croppie-bind="file" data-croppie-progress="#upload-photo-progress-12" 
><i class="fa fa-picture-o" aria-hidden="true"></i>
                                </div>
                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Image</a>
                                <input type="file" class="d-none" target="team1333" id="team1333"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                <input type="hidden" id="uploaded_team_image755" name="team2">
                            </div>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img id="previewteamsec232" src="<?php echo ((count($getIfAlreadyTeam) >= 2 ? (($getIfAlreadyTeam[1]->photo != NULL || $getIfAlreadyTeam[1]->photo != '') ? $getIfAlreadyTeam[1]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 2)
                                    {
                                        if($getIfAlreadyTeam[1]->photo != NULL || $getIfAlreadyTeam[1]->photo != '')
                                        {
                                    ?>
                                <a id="crossteam2" href="<?php echo URL::to('/delete-team-image/'.$getIfAlreadyTeam[1]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="clear"></div>
                            
                            <div class="form-group col-lg-4" style="position:relative">
                                <label for="exampleInputEmail1">Position</label>  
                                <input type="text" name="position3" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlreadyTeam) >= 3 ? (($getIfAlreadyTeam[2]->position != NULL) ? $getIfAlreadyTeam[2]->position : '') : '')); ?>"> 
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 3)
                                    {
                                ?>
                                <div class="u-progress">
                                    <div class="u-close cgt">
                                        <a href="{{ URL::to('/delete-team') }}/<?php echo $getIfAlreadyTeam[2]->id; ?>"><i class="cvicon-cv-cancel"></i></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Search By Name</label>  
                                <select class="select2 input-select" style="width:100%" name="name3" onchange="getTeamImage(this.value,3)">
                                    <?php 
                                        if(count($getFollowers) > 0)
                                        {
                                            foreach($getFollowers as $k=>$v)
                                            {
                                                $getUserDetail = DB::table('users')->where('id',$v->follows_id)->get();
                                    ?>
                                    <?php 
                                        if($k == 0)
                                        {
                                    ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 3 ? (($getIfAlreadyTeam[2]->search_by_name != NULL) ? (($getIfAlreadyTeam[2]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                    <option value="<?php echo $getUserDetail[0]->name; ?>"<?php echo ((count($getIfAlreadyTeam) >= 3 ? (($getIfAlreadyTeam[2]->search_by_name != NULL) ? (($getIfAlreadyTeam[2]->search_by_name == $getUserDetail[0]->name) ? 'selected' : '') : '') : '')); ?>><?php echo $getUserDetail[0]->name; ?></option>
                                    <?php } }else{ ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 3 ? (($getIfAlreadyTeam[2]->search_by_name != NULL) ? (($getIfAlreadyTeam[2]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                            <div class="u-progress" id="upload-photo-progress-13" style="display: none; width: 150px">
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                <span class="sr-only" data-progress="text">0% Complete</span>
            </div>
        </div>
    </div>
                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                <div class="browse-photo" data-croppie-output="#previewteamsec23234" data-croppie data-croppie-file="#team133323" data-croppie-input="#uploaded_team_image32322" data-croppie-bind="file" data-croppie-progress="#upload-photo-progress-13" 
><i class="fa fa-picture-o" aria-hidden="true"></i>
                                </div>
                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Image</a>
                                <input type="file" class="d-none" target="team133323" id="team133323"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                <input type="hidden" id="uploaded_team_image32322" name="team3">
                            </div>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img id="previewteamsec23234" src="<?php echo ((count($getIfAlreadyTeam) >= 3 ? (($getIfAlreadyTeam[2]->photo != NULL || $getIfAlreadyTeam[2]->photo != '') ? $getIfAlreadyTeam[2]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 3)
                                    {
                                        if($getIfAlreadyTeam[2]->photo != NULL || $getIfAlreadyTeam[2]->photo != '')
                                        {
                                    ?>
                                <a id="crossteam3" href="<?php echo URL::to('/delete-team-image/'.$getIfAlreadyTeam[2]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="clear"></div>
                            
                            <div id="cloneteamdiv" class="<?php if(count($getIfAlreadyTeam) <= 3){ echo "d-none"; } ?>">
                                <div class="form-group col-lg-4" style="position:relative">
                                    <label for="exampleInputEmail1">Position</label>  
                                    <input type="text" name="position4" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlreadyTeam) >= 4 ? (($getIfAlreadyTeam[3]->position != NULL) ? $getIfAlreadyTeam[3]->position : '') : '')); ?>"> 
                                    <?php 
                                        if(count($getIfAlreadyTeam) >= 4)
                                        {
                                    ?>
                                    <div class="u-progress">
                                        <div class="u-close cgt">
                                            <a href="{{ URL::to('/delete-team') }}/<?php echo $getIfAlreadyTeam[3]->id; ?>"><i class="cvicon-cv-cancel"></i></a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Search By Name</label>  
                                <select class="select2 input-select" style="width:100%" name="name4" onchange="getTeamImage(this.value,4)">
                                    <?php 
                                        if(count($getFollowers) > 0)
                                        {
                                            foreach($getFollowers as $k=>$v)
                                            {
                                                $getUserDetail = DB::table('users')->where('id',$v->follows_id)->get();
                                    ?>
                                    <?php 
                                        if($k == 0)
                                        {
                                    ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 4 ? (($getIfAlreadyTeam[3]->search_by_name != NULL) ? (($getIfAlreadyTeam[3]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                    <option value="<?php echo $getUserDetail[0]->name; ?>"<?php echo ((count($getIfAlreadyTeam) >= 4 ? (($getIfAlreadyTeam[3]->search_by_name != NULL) ? (($getIfAlreadyTeam[3]->search_by_name == $getUserDetail[0]->name) ? 'selected' : '') : '') : '')); ?>><?php echo $getUserDetail[0]->name; ?></option>
                                    <?php } }else{ ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 4 ? (($getIfAlreadyTeam[3]->search_by_name != NULL) ? (($getIfAlreadyTeam[3]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                         <div class="u-progress" id="upload-photo-progress-96" style="display: none; width: 150px">
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                <span class="sr-only" data-progress="text">0% Complete</span>
            </div>
        </div>
    </div>
                                <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                </div>
                                <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-96" data-croppie data-croppie-file="#team467" data-croppie-input="#uploaded_team_image4" data-croppie-output="#previewteamsec4" data-croppie-bind="file"  style="text-align:Center;display:block;margin-top:9px;">Upload Image</a>
                                <input type="file" class="d-none" target="team467" id="team467"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                <input type="hidden" id="uploaded_team_image4" name="team4">
                            </div>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img id="previewteamsec4" src="<?php echo ((count($getIfAlreadyTeam) >= 4 ? (($getIfAlreadyTeam[3]->photo != NULL || $getIfAlreadyTeam[3]->photo != '') ? $getIfAlreadyTeam[3]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 4)
                                    {
                                        if($getIfAlreadyTeam[3]->photo != NULL || $getIfAlreadyTeam[3]->photo != '')
                                        {
                                    ?>
                                <a id="crossteam4" href="<?php echo URL::to('/delete-team-image/'.$getIfAlreadyTeam[3]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="clear"></div>
                                
                                <div class="form-group col-lg-4" style="position:relative">
                                    <label for="exampleInputEmail1">Position</label>  
                                    <input type="text" name="position5" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlreadyTeam) >= 5 ? (($getIfAlreadyTeam[4]->position != NULL) ? $getIfAlreadyTeam[4]->position : '') : '')); ?>"> 
                                    <?php 
                                        if(count($getIfAlreadyTeam) >= 5)
                                        {
                                    ?>
                                    <div class="u-progress">
                                        <div class="u-close cgt">
                                            <a href="{{ URL::to('/delete-team') }}/<?php echo $getIfAlreadyTeam[4]->id; ?>"><i class="cvicon-cv-cancel"></i></a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Search By Name</label>  
                                <?php 
                                    $userId = session()->get('user_id');
                                    $getFollowers = DB::table('followers')->where('user_id',$userId)->get();
                                ?>
                                <select class="select2 input-select" style="width:100%" name="name5" onchange="getTeamImage(this.value,5)">
                                    <?php 
                                        if(count($getFollowers) > 0)
                                        {
                                            foreach($getFollowers as $k=>$v)
                                            {
                                                $getUserDetail = DB::table('users')->where('id',$v->follows_id)->get();
                                    ?>
                                    <?php 
                                        if($k == 0)
                                        {
                                    ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 5 ? (($getIfAlreadyTeam[4]->search_by_name != NULL) ? (($getIfAlreadyTeam[4]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                    <option value="<?php echo $getUserDetail[0]->name; ?>"<?php echo ((count($getIfAlreadyTeam) >= 5 ? (($getIfAlreadyTeam[4]->search_by_name != NULL) ? (($getIfAlreadyTeam[4]->search_by_name == $getUserDetail[0]->name) ? 'selected' : '') : '') : '')); ?>><?php echo $getUserDetail[0]->name; ?></option>
                                    <?php } }else{ ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 5 ? (($getIfAlreadyTeam[4]->search_by_name != NULL) ? (($getIfAlreadyTeam[4]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                     <div class="u-progress" id="upload-photo-progress-95" style="display: none; width: 150px">
        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                <span class="sr-only" data-progress="text">0% Complete</span>
            </div>
        </div>
    </div>
                                <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                </div>
                                <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-95" data-croppie data-croppie-file="#team5873" data-croppie-input="#uploaded_team_image5" data-croppie-output="#cpreviewteamsec5" data-croppie-bind="file"   style="text-align:Center;display:block;margin-top:9px;">Upload Image</a>
                                <input type="file" class="d-none" target="team5873" id="team5873"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                <input type="hidden" id="uploaded_team_image5" name="team5">
                            </div>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img id="previewteamsec5" src="<?php echo ((count($getIfAlreadyTeam) >= 5 ? (($getIfAlreadyTeam[4]->photo != NULL || $getIfAlreadyTeam[4]->photo != '') ? $getIfAlreadyTeam[4]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 5)
                                    {
                                        if($getIfAlreadyTeam[4]->photo != NULL || $getIfAlreadyTeam[4]->photo != '')
                                        {
                                    ?>
                                <a id="crossteam5" href="<?php echo URL::to('/delete-team-image/'.$getIfAlreadyTeam[4]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="clear"></div>
                                
                                <div class="form-group col-lg-4" style="position:relative">
                                    <label for="exampleInputEmail1">Position</label>  
                                    <input type="text" name="position6" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlreadyTeam) >= 6 ? (($getIfAlreadyTeam[5]->position != NULL) ? $getIfAlreadyTeam[5]->position : '') : '')); ?>"> 
                                    <?php 
                                        if(count($getIfAlreadyTeam) >= 6)
                                        {
                                    ?>
                                    <div class="u-progress">
                                        <div class="u-close cgt">
                                            <a href="{{ URL::to('/delete-team') }}/<?php echo $getIfAlreadyTeam[5]->id; ?>"><i class="cvicon-cv-cancel"></i></a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Search By Name</label>  
                                <?php 
                                    $userId = session()->get('user_id');
                                    $getFollowers = DB::table('followers')->where('user_id',$userId)->get();
                                ?>
                                <select class="select2 input-select" style="width:100%" name="name6" onchange="getTeamImage(this.value,6)">
                                    <?php 
                                        if(count($getFollowers) > 0)
                                        {
                                            foreach($getFollowers as $k=>$v)
                                            {
                                                $getUserDetail = DB::table('users')->where('id',$v->follows_id)->get();
                                    ?>
                                    <?php 
                                        if($k == 0)
                                        {
                                    ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 6 ? (($getIfAlreadyTeam[5]->search_by_name != NULL) ? (($getIfAlreadyTeam[5]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                    <option value="<?php echo $getUserDetail[0]->name; ?>"<?php echo ((count($getIfAlreadyTeam) >= 6 ? (($getIfAlreadyTeam[5]->search_by_name != NULL) ? (($getIfAlreadyTeam[5]->search_by_name == $getUserDetail[0]->name) ? 'selected' : '') : '') : '')); ?>><?php echo $getUserDetail[0]->name; ?></option>
                                    <?php } }else{ ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 6 ? (($getIfAlreadyTeam[5]->search_by_name != NULL) ? (($getIfAlreadyTeam[5]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                 <div class="u-progress" id="upload-photo-progress-94" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                </div>
                                <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-94" data-croppie data-croppie-file="#team94" data-croppie-input="#uploaded_team_image6" data-croppie-output="#previewteamsec6" data-croppie-bind="file"   style="text-align:Center;display:block;margin-top:9px;">Upload Image</a>
                                <input type="file" class="d-none" target="team94" id="team94"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                <input type="hidden" id="uploaded_team_image6" name="team6">
                            </div>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img id="previewteamsec6" src="<?php echo ((count($getIfAlreadyTeam) >= 6 ? (($getIfAlreadyTeam[5]->photo != NULL || $getIfAlreadyTeam[5]->photo != '') ? $getIfAlreadyTeam[5]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 6)
                                    {
                                        if($getIfAlreadyTeam[5]->photo != NULL || $getIfAlreadyTeam[5]->photo != '')
                                        {
                                    ?>
                                <a id="crossteam6" href="<?php echo URL::to('/delete-team-image/'.$getIfAlreadyTeam[5]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="clear"></div>
                                
                                <div class="form-group col-lg-4" style="position:relative">
                                    <label for="exampleInputEmail1">Position</label>  
                                    <input type="text" name="position7" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlreadyTeam) >= 7 ? (($getIfAlreadyTeam[6]->position != NULL) ? $getIfAlreadyTeam[6]->position : '') : '')); ?>"> 
                                    <?php 
                                        if(count($getIfAlreadyTeam) >= 7)
                                        {
                                    ?>
                                    <div class="u-progress">
                                        <div class="u-close cgt">
                                            <a href="{{ URL::to('/delete-team') }}/<?php echo $getIfAlreadyTeam[6]->id; ?>"><i class="cvicon-cv-cancel"></i></a>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                                <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Search By Name</label>  
                                <?php 
                                    $userId = session()->get('user_id');
                                    $getFollowers = DB::table('followers')->where('user_id',$userId)->get();
                                ?>
                                <select class="select2 input-select" style="width:100%" name="name7" onchange="getTeamImage(this.value,7)">
                                    <?php 
                                        if(count($getFollowers) > 0)
                                        {
                                            foreach($getFollowers as $k=>$v)
                                            {
                                                $getUserDetail = DB::table('users')->where('id',$v->follows_id)->get();
                                    ?>
                                    <?php 
                                        if($k == 0)
                                        {
                                    ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 7 ? (($getIfAlreadyTeam[6]->search_by_name != NULL) ? (($getIfAlreadyTeam[6]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                    <option value="<?php echo $getUserDetail[0]->name; ?>"<?php echo ((count($getIfAlreadyTeam) >= 7 ? (($getIfAlreadyTeam[6]->search_by_name != NULL) ? (($getIfAlreadyTeam[6]->search_by_name == $getUserDetail[0]->name) ? 'selected' : '') : '') : '')); ?>><?php echo $getUserDetail[0]->name; ?></option>
                                    <?php } }else{ ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 7 ? (($getIfAlreadyTeam[6]->search_by_name != NULL) ? (($getIfAlreadyTeam[6]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                                <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                     <div class="u-progress" id="upload-photo-progress-93" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                </div>
                                <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-93" data-croppie data-croppie-file="#team93" data-croppie-input="#uploaded_team_image7" data-croppie-output="#previewteamsec7" data-croppie-bind="file"   style="text-align:Center;display:block;margin-top:9px;">Upload Image</a>
                                <input type="file" class="d-none" target="team93" id="team93"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                <input type="hidden" id="uploaded_team_image7" name="team7">
                            </div>
                                <div class="u-close" style="float:left; margin-right:20px;">
                                <img id="previewteamsec7" src="<?php echo ((count($getIfAlreadyTeam) >= 7 ? (($getIfAlreadyTeam[6]->photo != NULL || $getIfAlreadyTeam[6]->photo != '') ? $getIfAlreadyTeam[6]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 7)
                                    {
                                        if($getIfAlreadyTeam[6]->photo != NULL || $getIfAlreadyTeam[6]->photo != '')
                                        {
                                    ?>
                                <a id="crossteam7" href="<?php echo URL::to('/delete-team-image/'.$getIfAlreadyTeam[6]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="clear"></div>
                            <div class="form-group col-lg-4" style="position:relative">
                                <label for="exampleInputEmail1">Position</label>  
                                <input type="text" name="position8" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlreadyTeam) >= 8 ? (($getIfAlreadyTeam[7]->position != NULL) ? $getIfAlreadyTeam[7]->position : '') : '')); ?>"> 
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 8)
                                    {
                                ?>
                                <div class="u-progress">
                                    <div class="u-close cgt">
                                        <a href="{{ URL::to('/delete-team') }}/<?php echo $getIfAlreadyTeam[7]->id; ?>"><i class="cvicon-cv-cancel"></i></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Search By Name</label>  
                                <?php 
                                    $userId = session()->get('user_id');
                                    $getFollowers = DB::table('followers')->where('user_id',$userId)->get();
                                ?>
                                <select class="select2 input-select" style="width:100%" name="name8" onchange="getTeamImage(this.value,8)">
                                    <?php 
                                        if(count($getFollowers) > 0)
                                        {
                                            foreach($getFollowers as $k=>$v)
                                            {
                                                $getUserDetail = DB::table('users')->where('id',$v->follows_id)->get();
                                    ?>
                                    <?php 
                                        if($k == 0)
                                        {
                                    ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 8 ? (($getIfAlreadyTeam[7]->search_by_name != NULL) ? (($getIfAlreadyTeam[7]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                    <option value="<?php echo $getUserDetail[0]->name; ?>"<?php echo ((count($getIfAlreadyTeam) >= 8 ? (($getIfAlreadyTeam[7]->search_by_name != NULL) ? (($getIfAlreadyTeam[7]->search_by_name == $getUserDetail[0]->name) ? 'selected' : '') : '') : '')); ?>><?php echo $getUserDetail[0]->name; ?></option>
                                    <?php } }else{ ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 8 ? (($getIfAlreadyTeam[7]->search_by_name != NULL) ? (($getIfAlreadyTeam[7]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                 <div class="u-progress" id="upload-photo-progress-92" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                </div>
                                <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-92" data-croppie data-croppie-file="#team92" data-croppie-input="#uploaded_team_image8" data-croppie-output="#previewteamsec8" data-croppie-bind="file"   style="text-align:Center;display:block;margin-top:9px;">Upload Image</a>
                                <input type="file" class="d-none" target="team92" id="team92"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                <input type="hidden" id="uploaded_team_image8" name="team8">
                            </div>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img id="previewteamsec8" src="<?php echo ((count($getIfAlreadyTeam) >= 8 ? (($getIfAlreadyTeam[7]->photo != NULL || $getIfAlreadyTeam[7]->photo != '') ? $getIfAlreadyTeam[7]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 8)
                                    {
                                        if($getIfAlreadyTeam[7]->photo != NULL || $getIfAlreadyTeam[7]->photo != '')
                                        {
                                    ?>
                                <a id="crossteam8" href="<?php echo URL::to('/delete-team-image/'.$getIfAlreadyTeam[7]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="clear"></div>
                            <div class="form-group col-lg-4" style="position:relative">
                                <label for="exampleInputEmail1">Position</label>  
                                <input type="text" name="position9" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlreadyTeam) >= 9 ? (($getIfAlreadyTeam[8]->position != NULL) ? $getIfAlreadyTeam[8]->position : '') : '')); ?>"> 
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 9)
                                    {
                                ?>
                                <div class="u-progress">
                                    <div class="u-close cgt">
                                        <a href="{{ URL::to('/delete-team') }}/<?php echo $getIfAlreadyTeam[8]->id; ?>"><i class="cvicon-cv-cancel"></i></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Search By Name</label>  
                                <?php 
                                    $userId = session()->get('user_id');
                                    $getFollowers = DB::table('followers')->where('user_id',$userId)->get();
                                ?>
                                <select class="select2 input-select" style="width:100%" name="name9" onchange="getTeamImage(this.value,9)">
                                    <?php 
                                        if(count($getFollowers) > 0)
                                        {
                                            foreach($getFollowers as $k=>$v)
                                            {
                                                $getUserDetail = DB::table('users')->where('id',$v->follows_id)->get();
                                    ?>
                                    <?php 
                                        if($k == 0)
                                        {
                                    ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 9 ? (($getIfAlreadyTeam[8]->search_by_name != NULL) ? (($getIfAlreadyTeam[8]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                    <option value="<?php echo $getUserDetail[0]->name; ?>"<?php echo ((count($getIfAlreadyTeam) >= 9 ? (($getIfAlreadyTeam[8]->search_by_name != NULL) ? (($getIfAlreadyTeam[8]->search_by_name == $getUserDetail[0]->name) ? 'selected' : '') : '') : '')); ?>><?php echo $getUserDetail[0]->name; ?></option>
                                    <?php } }else{ ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 9 ? (($getIfAlreadyTeam[8]->search_by_name != NULL) ? (($getIfAlreadyTeam[8]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                 <div class="u-progress" id="upload-photo-progress-91" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                </div>
                                <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-91" data-croppie data-croppie-file="#team91" data-croppie-input="#uploaded_team_image9" data-croppie-output="#uploaded_team_image9" data-croppie-bind="file"   style="text-align:Center;display:block;margin-top:9px;">Upload Image</a>
                                <input type="file" class="d-none" target="team91" id="team91"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                <input type="hidden" id="uploaded_team_image9" name="team9">
                            </div>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img id="previewteamsec9" src="<?php echo ((count($getIfAlreadyTeam) >= 9 ? (($getIfAlreadyTeam[8]->photo != NULL || $getIfAlreadyTeam[8]->photo != '') ? $getIfAlreadyTeam[8]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 9)
                                    {
                                        if($getIfAlreadyTeam[8]->photo != NULL || $getIfAlreadyTeam[8]->photo != '')
                                        {
                                    ?>
                                <a id="crossteam9" href="<?php echo URL::to('/delete-team-image/'.$getIfAlreadyTeam[8]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="clear"></div>
                            
                            <div class="form-group col-lg-4" style="position:relative">
                                <label for="exampleInputEmail1">Position</label>  
                                <input type="text" name="position10" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlreadyTeam) >= 10 ? (($getIfAlreadyTeam[9]->position != NULL) ? $getIfAlreadyTeam[9]->position : '') : '')); ?>"> 
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 10)
                                    {
                                ?>
                                <div class="u-progress">
                                    <div class="u-close cgt">
                                        <a href="{{ URL::to('/delete-team') }}/<?php echo $getIfAlreadyTeam[9]->id; ?>"><i class="cvicon-cv-cancel"></i></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Search By Name</label>  
                                <?php 
                                    $userId = session()->get('user_id');
                                    $getFollowers = DB::table('followers')->where('user_id',$userId)->get();
                                ?>
                                <select class="select2 input-select" style="width:100%" name="name10" onchange="getTeamImage(this.value,10)">
                                    <?php 
                                        if(count($getFollowers) > 0)
                                        {
                                            foreach($getFollowers as $k=>$v)
                                            {
                                                $getUserDetail = DB::table('users')->where('id',$v->follows_id)->get();
                                    ?>
                                    <?php 
                                        if($k == 0)
                                        {
                                    ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 10 ? (($getIfAlreadyTeam[9]->search_by_name != NULL) ? (($getIfAlreadyTeam[9]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                    <option value="<?php echo $getUserDetail[0]->name; ?>"<?php echo ((count($getIfAlreadyTeam) >= 10 ? (($getIfAlreadyTeam[9]->search_by_name != NULL) ? (($getIfAlreadyTeam[9]->search_by_name == $getUserDetail[0]->name) ? 'selected' : '') : '') : '')); ?>><?php echo $getUserDetail[0]->name; ?></option>
                                    <?php } }else{ ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 10 ? (($getIfAlreadyTeam[9]->search_by_name != NULL) ? (($getIfAlreadyTeam[9]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                 <div class="u-progress" id="upload-photo-progress-90" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                </div>
                                <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-90" data-croppie data-croppie-file="#team90" data-croppie-input="#uploaded_team_image10" data-croppie-output="#previewteamsec10" data-croppie-bind="file"   style="text-align:Center;display:block;margin-top:9px;">Upload Image</a>
                                <input type="file" class="d-none" target="team90" id="team90"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                <input type="hidden" id="uploaded_team_image10" name="team10">
                            </div>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img id="previewteamsec10" src="<?php echo ((count($getIfAlreadyTeam) >= 10 ? (($getIfAlreadyTeam[9]->photo != NULL || $getIfAlreadyTeam[9]->photo != '') ? $getIfAlreadyTeam[9]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 10)
                                    {
                                        if($getIfAlreadyTeam[9]->photo != NULL || $getIfAlreadyTeam[9]->photo != '')
                                        {
                                    ?>
                                <a id="crossteam10" href="<?php echo URL::to('/delete-team-image/'.$getIfAlreadyTeam[9]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="clear"></div>
                            
                            <div class="form-group col-lg-4" style="position:relative">
                                <label for="exampleInputEmail1">Position</label>  
                                <input type="text" name="position11" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlreadyTeam) >= 11 ? (($getIfAlreadyTeam[10]->position != NULL) ? $getIfAlreadyTeam[10]->position : '') : '')); ?>"> 
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 11)
                                    {
                                ?>
                                <div class="u-progress">
                                    <div class="u-close cgt">
                                        <a href="{{ URL::to('/delete-team') }}/<?php echo $getIfAlreadyTeam[10]->id; ?>"><i class="cvicon-cv-cancel"></i></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Search By Name</label>  
                                <?php 
                                    $userId = session()->get('user_id');
                                    $getFollowers = DB::table('followers')->where('user_id',$userId)->get();
                                ?>
                                <select class="select2 input-select" style="width:100%" name="name11" onchange="getTeamImage(this.value,11)">
                                    <?php 
                                        if(count($getFollowers) > 0)
                                        {
                                            foreach($getFollowers as $k=>$v)
                                            {
                                                $getUserDetail = DB::table('users')->where('id',$v->follows_id)->get();
                                    ?>
                                    <?php 
                                        if($k == 0)
                                        {
                                    ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 11 ? (($getIfAlreadyTeam[10]->search_by_name != NULL) ? (($getIfAlreadyTeam[10]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                    <option value="<?php echo $getUserDetail[0]->name; ?>"<?php echo ((count($getIfAlreadyTeam) >= 11 ? (($getIfAlreadyTeam[10]->search_by_name != NULL) ? (($getIfAlreadyTeam[10]->search_by_name == $getUserDetail[0]->name) ? 'selected' : '') : '') : '')); ?>><?php echo $getUserDetail[0]->name; ?></option>
                                    <?php } }else{ ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 11 ? (($getIfAlreadyTeam[10]->search_by_name != NULL) ? (($getIfAlreadyTeam[10]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                 <div class="u-progress" id="upload-photo-progress-89" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                </div>
                                <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-89" data-croppie data-croppie-file="#team89" data-croppie-input="#uploaded_team_image11" data-croppie-output="#previewteamsec11" data-croppie-bind="file"   style="text-align:Center;display:block;margin-top:9px;">Upload Image</a>
                                <input type="file" class="d-none" target="team89" id="team89"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                <input type="hidden" id="uploaded_team_image11" name="team11">
                            </div>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img id="previewteamsec11" src="<?php echo ((count($getIfAlreadyTeam) >= 11 ? (($getIfAlreadyTeam[10]->photo != NULL || $getIfAlreadyTeam[10]->photo != '') ? $getIfAlreadyTeam[10]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 11)
                                    {
                                        if($getIfAlreadyTeam[10]->photo != NULL || $getIfAlreadyTeam[10]->photo != '')
                                        {
                                    ?>
                                <a id="crossteam11" href="<?php echo URL::to('/delete-team-image/'.$getIfAlreadyTeam[10]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="clear"></div>
                                
                            <div class="form-group col-lg-4" style="position:relative">
                                <label for="exampleInputEmail1">Position</label>  
                                <input type="text" name="position12" class="form-control" id="exampleInputEmail1" placeholder="" value="<?php echo ((count($getIfAlreadyTeam) >= 12 ? (($getIfAlreadyTeam[11]->position != NULL) ? $getIfAlreadyTeam[11]->position : '') : '')); ?>"> 
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 12)
                                    {
                                ?>
                                <div class="u-progress">
                                    <div class="u-close cgt">
                                        <a href="{{ URL::to('/delete-team') }}/<?php echo $getIfAlreadyTeam[11]->id; ?>"><i class="cvicon-cv-cancel"></i></a>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Search By Name</label>  
                                <?php 
                                    $userId = session()->get('user_id');
                                    $getFollowers = DB::table('followers')->where('user_id',$userId)->get();
                                ?>
                                <select class="select2 input-select" style="width:100%" name="name12" onchange="getTeamImage(this.value,12)">
                                    <?php 
                                        if(count($getFollowers) > 0)
                                        {
                                            foreach($getFollowers as $k=>$v)
                                            {
                                                $getUserDetail = DB::table('users')->where('id',$v->follows_id)->get();
                                    ?>
                                    <?php 
                                        if($k == 0)
                                        {
                                    ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 12 ? (($getIfAlreadyTeam[11]->search_by_name != NULL) ? (($getIfAlreadyTeam[11]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                    <option value="<?php echo $getUserDetail[0]->name; ?>"<?php echo ((count($getIfAlreadyTeam) >= 12 ? (($getIfAlreadyTeam[11]->search_by_name != NULL) ? (($getIfAlreadyTeam[11]->search_by_name == $getUserDetail[0]->name) ? 'selected' : '') : '') : '')); ?>><?php echo $getUserDetail[0]->name; ?></option>
                                    <?php } }else{ ?>
                                    <option value="<?php echo ((count($getIfAlreadyTeam) >= 12 ? (($getIfAlreadyTeam[11]->search_by_name != NULL) ? (($getIfAlreadyTeam[11]->search_by_name == "") ? 'selected' : '') : '') : '')); ?>">Select Follower</option>
                                    <?php } ?>
                                </select>
                                
                            </div>
                            <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                 <div class="u-progress" id="upload-photo-progress-85" style="display: none; width: 150px">
                									<div class="progress">
                										<div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;" data-progress="bar">
                											<span class="sr-only" data-progress="text">0% Complete</span>
                										</div>
                									</div>
                								</div>
                                <div class="browse-photo" ><i class="fa fa-picture-o" aria-hidden="true"></i>
                                </div>
                                <a href="javascript:void(0)" data-croppie-progress="#upload-photo-progress-85" data-croppie data-croppie-file="#team88" data-croppie-input="#uploaded_team_image12" data-croppie-output="#previewteamsec12" data-croppie-bind="file"   style="text-align:Center;display:block;margin-top:9px;">Upload Image</a>
                                <input type="file" class="d-none" target="team88" id="team88"  class="image custom-file-input image upload-cover-image" accept="image/x-png,image/gif,image/jpeg" />
                                <input type="hidden" id="uploaded_team_image12" name="team12">
                            </div>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img id="previewteamsec12" src="<?php echo ((count($getIfAlreadyTeam) >= 12 ? (($getIfAlreadyTeam[11]->photo != NULL || $getIfAlreadyTeam[11]->photo != '') ? $getIfAlreadyTeam[11]->photo : 'https://entertainmentbugs.com/public/img/sv-5.jpg') : 'https://entertainmentbugs.com/public/img/sv-5.jpg')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyTeam) >= 12)
                                    {
                                        if($getIfAlreadyTeam[11]->photo != NULL || $getIfAlreadyTeam[11]->photo != '')
                                        {
                                    ?>
                                <a id="crossteam12" href="<?php echo URL::to('/delete-team-image/'.$getIfAlreadyTeam[11]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="clear"></div>
                        </div>
                        <div class="btn-save">
                            <button class="btn btn-primary u-btn"><?php if(count($getIfAlreadyTeam) == 0){ echo "Save";}else{ echo "Update"; } ?></button>
                        </div>
                    </form>
                    <a href="javascript:void(0)" id="addMoreTeam" onclick="addMoreTeam(this)" style="background: #000;color: #fff;margin:20px 0 0 0 ;display:block;width:140px; padding: 6px 15px 2px 17px;font-size: 12px;" class="<?php if(count($getIfAlreadyTeam) >= 4){ echo "d-none"; }else{ } ?>">Add team Member</a>
                    <a href="javascript:void(0)" id="removeMoreTeam" onclick="removeMoreTeam(this)" style="background: #000;color: #fff;margin:20px 0 0 0 ;display:block;width:160px; padding: 6px 15px 2px 17px;font-size: 12px;" class="<?php if(count($getIfAlreadyTeam) <= 3){ echo "d-none"; } ?>">Remove team Member</a>
                </div>
            </div>
            </div>
            <div id="contact" class="tabcontent">
                <div class="container">
                    <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <form action="{{URL::to('save-production-contact')}}" id="contactForm" style="margin-top:0px" enctype="multipart/form-data" method="post">
                            <?php 
                                $userId = session()->get('user_id');
                                $getIfAlreadyContact = DB::select('select * from production_contact_us where user_id='.$userId);
                                ?>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group achievemets">
                                <h2>Contact Us</h2>
                                <div class="form-group col-lg-12">
                                    <label for="exampleInputEmail1">Title eg: We'd love to hear from you</label>
                                    <input type="text" name="titlecontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->title != NULL) ? $getIfAlreadyContact[0]->title : '') : '')); ?>">  
                                </div>
                                <div class="form-group col-lg-12">
                                    <label for="exampleInputEmail1"> Description (50 words)</label>
                                    <textarea id="w3review" name="descriptioncontact" rows="4" cols="50"><?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->description != NULL) ? $getIfAlreadyContact[0]->description : '') : '')); ?></textarea> 
                                </div>
                                <div style="clear:both; overflow:hidden"></div>
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Address</label>
                                        <input type="text" name="addresscontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->address != NULL) ? $getIfAlreadyContact[0]->address : '') : '')); ?>">    
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Email id</label>  
                                        <input type="email" name="emailcontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->email_id != NULL) ? $getIfAlreadyContact[0]->email_id : '') : '')); ?>">    
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"> <label for="exampleInputEmail1">Landline number</label> 
                                        <input type="text" name="phonecontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->landline_number != NULL) ? $getIfAlreadyContact[0]->landline_number : '') : '')); ?>">  
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"> <label for="exampleInputEmail1">Number / Whatsapp No </label>  
                                        <input type="text" name="numbercontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->number != NULL) ? $getIfAlreadyContact[0]->number : '') : '')); ?>">  
                                    </div>
                                    <input type="checkbox" name="tick_whatsapp_feature" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->whatsapp_tick != NULL) ? $getIfAlreadyContact[0]->whatsapp_tick : '') : '')); ?>" style="height: 16px;margin: -1px 5px 0 0;float: left;" <?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->whatsapp_tick != NULL && $getIfAlreadyContact[0]->whatsapp_tick != 'no') ? 'checked' : '') : '')); ?>> Check tick for whatsaps features
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="col-lg-6"> 
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="row">
                                <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyContact) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div> 
</div>
</div>
</div>
</div> 
<style>
    .d-none{
    display:none!important;
    }
      @media(max-width:667px){
     .cr-boundary{
       width: auto !important;
    }
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
{{ HTML::style('public/css/bootstrap-tagsinput.css')}}
{{ HTML::script('public/js/jquery.validate.js')}}
{{ HTML::script('public/js/bootstrap-tagsinput.js')}}
{{ HTML::script('public/js/bootstrap3-typeahead.js')}}
<script>
    $(document).ready(function() {
        $('.select2').select2();
    });
    function getStates(value)
    {
        $('#state').empty();
        $('#state').append('<option value="">Select State</option>');
        if(value != '')
        {
            $.ajax({
                url:'{{URL::to("/get-states")}}',
                data:{id:value,'_token':'<?php echo csrf_token(); ?>'},
                type:'post',
                success:function(res)
                {
                    let parse= JSON.parse(res);
                    if(parse.length > 0)
                    {
                        for(let i=0;i<parse.length;i++)
                            $('#state').append('<option value="'+parse[i].name+'">'+parse[i].name+'</option>');
                    }else{
                        $('#state').append('<option value="">No States Found</option>');    
                    }
                }
            })
        }else{
            $('#state').append('<option value="">Select State</option>');    
        }
    }
    
    function getCities(value)
    {
        $('#city').empty();
        $('#city').append('<option value="">Select City</option>');
        if(value != '')
        {
            $.ajax({
                url:'{{URL::to("/get-cities")}}',
                data:{id:value,'_token':'<?php echo csrf_token(); ?>'},
                type:'post',
                success:function(res)
                {
                    let parse= JSON.parse(res);
                    if(parse.length > 0)
                    {
                        for(let i=0;i<parse.length;i++)
                            $('#city').append('<option value="'+parse[i].name+'">'+parse[i].name+'</option>');
                    }else{
                       $('#city').append('<option value="">No Cities Found</option>');     
                    }
                }
            })
        }else{
            $('#city').append('<option value="">Select City</option>');     
        }
    }
    
    $('#productionForm').on('submit',function(e)
    {
        e.preventDefault();
        let arr = $(this).serializeArray();
        let count = 0;
        for(let i =0 ;i<arr.length;i++)
        {
            if(arr[i].name == 'phousecheck[]')
            {
                count++;
            }
        }
        if(count > 4)
        {
            alert('Please Select Upto 4 Multiple Options');
        }else if(count == 0)
        {
            alert('Please Select At Least One Option');
        }else{
            let data = new FormData(this);
            $.ajax({
                url:$(this).attr('action'),
                data:data,
                contentType: false,
                cache: false,
                processData:false,
                type:'post',
                success:function(res)
                {
                    if(res == 'exists') {
                        alert('Username already exists.');
                    } 
                    else {                        
                        alert('Data Saved Successfully');
                        //console.log(res);
                       location.reload();
                    }
                }
            });
        }
    })
    
    $('#achivementForm').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#reelform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#videoform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#whatwedoform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#projectform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#teamform').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#galleryForm').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    $('#contactForm').on('submit',function(e)
    {
        e.preventDefault();
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    })
    
    function openCity(evt, cityName) {
      var i, tabcontent, tablinks;
      tabcontent = document.getElementsByClassName("tabcontent");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }
      tablinks = document.getElementsByClassName("tablinks");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }
      document.getElementById(cityName).style.display = "block";
      evt.currentTarget.className += " active";
    }
    
    function showAchivementUploader(num)
    {
        $('#fileachive'+num).click();
    }
    
    function showFileLabel(num)
    {
        $('#fileachivelabel'+num).html('1 File Selected <i class="fa fa-times" onclick="removeAchivementFile('+num+')"></i>');
    }
    
    function removeAchivementFile(num)
    {
        const file = document.querySelector('#fileachive'+num);
        file.value = '';
        $('#fileachivelabel'+num).html('');
    }
    
    function openReelVideo()
    {
        $('#reelvideo').click();
    }
    
    function openReelThumb(){
        $('#reelthumb').click();
    }
    
    function addfourmore(num)
    {
        if(num == 2)
        {
            $('#portfolio3').removeClass('d-none');
            $('#addPhotoThreeMore').removeClass('d-none');
            $('#removePhotoTwoMore').removeClass('d-none');
            $('#addPhotoTwoMore').addClass('d-none');
        }
        if(num == 3)
        {
            $('#portfolio4').removeClass('d-none');
            $('#addPhotoThreeMore').addClass('d-none');
            $('#removePhotoThreeMore').removeClass('d-none');
            $('#addPhotoTwoMore').addClass('d-none');
        }
    }
    
    function removefourmore(num)
    {
        if(num == 2)
        {
            $('#portfolio3').addClass('d-none');
            $('#removePhotoTwoMore').addClass('d-none');
            $('#addPhotoTwoMore').removeClass('d-none');
            $('#addPhotoThreeMore').addClass('d-none');
        }
        if(num == 3)
        {
            $('#portfolio4').addClass('d-none');
            if($('#portfolio3').hasClass('d-none'))
            {
                $('#addPhotoThreeMore').addClass('d-none');
            }else{
                $('#addPhotoThreeMore').removeClass('d-none');
            }
            $('#removePhotoThreeMore').addClass('d-none');
        }
    }
    
    function showReelVideoLabel()
    {
        $('#reelvideolabel').html('1 File Selected <i class="fa fa-times" onclick="removeVideoFile()"></i>');
    }
    
    function showReelThumbLabel()
    {
        $('#reelvideothumblabel').html('1 File Selected <i class="fa fa-times" onclick="removeVideoThumbFile()"></i>');
    }
    
    function removeVideoFile()
    {
        const file = document.querySelector('#reelvideolabel');
        file.value = '';
        $('#reelvideolabel').html('');
    }
    
    function removeVideoThumbFile()
    {
        const file = document.querySelector('#reelvideothumblabel');
        file.value = '';
        $('#reelvideothumblabel').html('');
    }
    
    function removeAchivementFile(num)
    {
        const file = document.querySelector('#fileachive'+num);
        file.value = '';
        $('#fileachivelabel'+num).html('');
    }
    
    function addMoreTeam()
    {
        $('#addMoreTeam').addClass('d-none');
        $('#removeMoreTeam').removeClass('d-none');
        $('#cloneteamdiv').removeClass('d-none');
    }
    
    function removeMoreTeam()
    {
        $('#addMoreTeam').removeClass('d-none');
        $('#removeMoreTeam').addClass('d-none');
        $('#cloneteamdiv').addClass('d-none');
    }
    
    function addMoreAchivement()
    {
        $('#addMoreAchivement').addClass('d-none');
        $('#removeMoreAchivement').removeClass('d-none');
        $('#cloneachivement').removeClass('d-none');
    }
    
    function removeMoreAchivement()
    {
        $('#addMoreAchivement').removeClass('d-none');
        $('#removeMoreAchivement').addClass('d-none');
        $('#cloneachivement').addClass('d-none');
    }
    
    function removeWhatWeDo()
    {
        for(let i=5;i<=8;i++)
        {
            $('#title'+i).val('');
            $('#desc'+i).val('');
        }
        $('#addMore').removeClass('d-none');
        $('#removeMore').addClass('d-none');
        $('#clonediv').addClass('d-none');
    }
    
    function addProj()
    {
        $('#addProj').addClass('d-none');
        $('#removeProj').removeClass('d-none');
        $('#cloneprojdiv').removeClass('d-none');    
    }
    
    function removeProj()
    {
        $('#addProj').removeClass('d-none');
        $('#removeProj').addClass('d-none');
        $('#cloneprojdiv').addClass('d-none');
    }
    
    function removeMoreVideo()
    {
        $('#videoAdd').removeClass('d-none');
        $('#videoRemove').addClass('d-none');
        $('#clonevideodiv').addClass('d-none');
    }
    
    function addMoreVideo()
    {
        $('#videoAdd').addClass('d-none');
        $('#videoRemove').removeClass('d-none');
        $('#clonevideodiv').removeClass('d-none');    
    }
    
    function addMoreWhatWeDo()
    {
        $('#addMore').addClass('d-none');
        $('#removeMore').removeClass('d-none');
        $('#clonediv').removeClass('d-none');
    }
    
    function openProjUploader(num)
    {
        $('#fileproj'+num).click();
    }
    
    function uploadVideo(num)
    {
        $('#videoFile'+num).click();
    }
    
    function showPhotoUploader(num)
    {
        $('#photosectionphoto'+num).click();
    }
    
    function uploadTeamImage(num)
    {
        $('#team'+num).click();
    }
    
    function uploadVideoThumbnail(num)
    {
        $('#videoThumb'+num).click();
    }
    
    function showProjFileLabel(num)
    {
        $('#fileprojlabel'+num).html('1 File Selected <i class="fa fa-times" onclick="removeProjFile('+num+')"></i>');
    }
    
    function removeProjFile(num)
    {
        const file = document.querySelector('#fileproj'+num);
        file.value = '';
        $('#fileprojlabel'+num).html('');
    }
    
</script>
<script>
    $('#video-language').tagsinput({
     	  typeahead: {
     		source: ['Afar', 'Abkhaz', 'Avestan', 'Afrikaans', 'Akan', 'Amharic','Aragonese', 'Arabic', 'Assamese', 'Avaric', 'Aymara', 'Azerbaijani','South Azerbaijani', 'Bashkir', 'Belarusian', 'Bulgarian', 'Bihari', 'Bislama',
     		'Bambara', 'Bengali', 'Tibetan Standard', 'Breton', 'Bosnian', 'Catalan; Valencian','Chechen', 'Chamorro', 'Corsican', 'Cree', 'Czech', 'Old Church Slavonic','Chuvash', 'Welsh', 'Danish', 'German', 'Maldivian', 'Dzongkha',
     		'Ewe', 'Greek', 'English', 'Esperanto', 'Spanish', 'Estonian','Basque', 'Persian ', 'Fula', 'Finnish', 'Fijian', 'Faroese',' French', 'Western Frisian', 'Irish', 'Scottish Gaelic', 'Galician', 'Guaraní',
     		'Gujarati', 'Manx', 'Hausa', 'Hebrew ', 'Hindi', 'Hiri Motu','Croatian', 'Haitian', 'Hungarian', 'Armenian', 'Herero', 'Interlingua',' Indonesian', 'Interlingue', 'Igbo', 'Nuosu', 'Inupiaq', 'Ido',
     		'Icelandic', 'Italian', 'Inuktitut', 'Japanese', 'Javanese', 'Georgian','Kongo', 'Gikuyu', 'Kwanyama', 'Kazakh', 'Kalaallisut', 'Khmer','Kannada', 'Korean', 'Kanuri', 'Kashmiri', 'Kurdish', 'Komi',
     		'Cornish', 'Kyrgyz', 'Latin', 'Luxembourgish', 'Ganda', 'Limburgish','Lingala', 'Lao', 'Lithuanian', 'Luba-Katanga', 'Latvian', 'Malagasy','Marshallese', 'Māori', 'Macedonian', 'Malayalam', 'Mongolian', 'Marathi',
     		'Malay', 'Maltese', 'Burmese', 'Nauru', 'Norwegian Bokmål', 'North Ndebele','Nepali', 'Ndonga', 'Dutch', 'Norwegian Nynorsk', 'Norwegian', 'South Ndebele','Navajo', 'Chichewa', 'Occitan', 'Ojibwe', 'Oromo', 'Oriya',
     		'Ossetian', 'Panjabi', 'Pāli', 'Polish', 'Pashto', 'Portuguese','Quechua', 'Romansh', 'Kirundi', 'Romanian', 'Russian', 'Kinyarwanda','Sanskrit', 'Sardinian', 'Sindhi', 'Northern Sami', 'Sango', 'Sinhala',
     		'Slovak', 'Slovene', 'Samoan', 'Shona', 'Somali', 'Albanian','Serbian', 'Swati', 'Southern Sotho', 'Sundanese', 'Swedish', 'Swahili','Tamil', 'Telugu', 'Tajik', 'Thai', 'Tigrinya', 'Turkmen',
     		'Tagalog', 'Tswana', 'Tonga', 'Turkish', 'Tsonga', 'Tatar','Twi', 'Tahitian', 'Uyghur', 'Ukrainian', 'Urdu	', 'Uzbek','Venda', 'Vietnamese', 'Volapük', 'Walloon', 'Wolof', 'Xhosa',
     		'Yiddish', 'Yoruba', 'Zhuang', 'Chinese', 'Zulu']
     	  },
     	  freeInput: true
     });	
    
    validateThumbFile = function(files,e){
        var flag = true;
        var imageType = /image.*/;  
        var file = files[0];
        // check file type
        if (!file.type.match(imageType)) {  
        alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
        flag = false;
        return false;	
        } 
        // check file size
        if (parseInt(file.size / 1024) > (1024*2)) {  
        alert("File \""+file.name+"\" is too big.");
        flag = false;
        return false;	
        } 
        
        if(flag == true){
            uploadImageFile(file);
        }else{
            return flag;
        }	
        
    };
    
    uploadImageFile = function(file){
        var formdata = new FormData();
        formdata.append("image", file);	
        //console.log(file);
        xhr = new XMLHttpRequest();
        //console.log(xhr);
        xhr.open("POST", "upload/temp_image");
        xhr.onload  = function() {
        var jsonResponse = xhr.response;
        console.log(jsonResponse);
        result = JSON.parse(jsonResponse);
        console.log(result);
        if(result.status == true){
            
        }else{
            alert('File is not uploading. Please try again.')
        }	  
        };	
        var csrfToken = $('#productionForm').find('input[name=_token]').val();
        xhr.setRequestHeader('X-csrf-token', csrfToken); 	
        xhr.send(formdata);		
    }
    
    function check(e,value)
    {
        //Check Charater
        var unicode=e.charCode? e.charCode : e.keyCode;
        if (value.indexOf(".") != -1)if( unicode == 46 )return false;
    }
    
    function checkLength(len,ele){
      var fieldLength = ele.value.length;
      let split = ele.value.split(' ');
      if(split.length <= len)
      {
          return true;
      }
      else
      {
        var str = ele.value;
        let finalstr = '';
        for(let i=0;i<len;i++)
        {
            finalstr += split[i] + ' ';
        }
        ele.value = finalstr;
      }
    }
    
    async function getThumbnailForVideo(videoUrl) {
      const video = document.createElement("video");
      const canvas = document.createElement("canvas");
      video.style.display = "none";
      canvas.style.display = "none";
    
      // Trigger video load
      await new Promise((resolve, reject) => {
        video.addEventListener("loadedmetadata", () => {
          video.width = video.videoWidth;
          video.height = video.videoHeight;
          canvas.width = '665';
          canvas.height = '372';
          // Seek the video to 25%
          video.currentTime = video.duration * 0.25;
        });
        video.addEventListener("seeked", () => resolve());
        video.src = videoUrl;
      });
    
      // Draw the thumbnailz
      canvas
        .getContext("2d")
        .drawImage(video, 0, 0, '672', '372');
      const imageUrl = canvas.toDataURL("image/png");
      return imageUrl;
    }
    
    const videoFile1 = document.querySelector('#videoFile1');
    
    videoFile1.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb1').val(thumbUrl);
            $('#previewvideosec1').attr('src',thumbUrl);
            $('.u-progress1').show();
    	    handleFiles1(file,'#uploadedvideosec1','1');
    	}
    });
    
    const videoFile2 = document.querySelector('#videoFile2');
    
    videoFile2.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb2').val(thumbUrl);
            $('#previewvideosec2').attr('src',thumbUrl);
            $('.u-progress2').show();
    	    handleFiles1(file,'#uploadedvideosec2','2');
    	}
    });
    
    const videoFile3 = document.querySelector('#videoFile3');
    
    videoFile3.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb3').val(thumbUrl);
            $('#previewvideosec3').attr('src',thumbUrl);
            $('.u-progress3').show();
    	    handleFiles1(file,'#uploadedvideosec3','3');
    	}
    });

    const videoFile_3 = document.querySelector('#videoFile_3');
    
    videoFile_3.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb_3').val(thumbUrl);
            $('#previewvideosec_3').attr('src',thumbUrl);
            $('.u-progress_3').show();
    	    handleFiles1(file,'#uploadedvideosec_3','_3');
    	}
    });
    
    const videoFile4 = document.querySelector('#videoFile4');
    
    videoFile4.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb4').val(thumbUrl);
            $('#previewvideosec4').attr('src',thumbUrl);
            $('.u-progress4').show();
    	    handleFiles1(file,'#uploadedvideosec4','4');
    	}
    });
    
    const videoFile5 = document.querySelector('#videoFile5');
    
    videoFile5.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb5').val(thumbUrl);
            $('#previewvideosec5').attr('src',thumbUrl);
            $('.u-progress5').show();
    	    handleFiles1(file,'#uploadedvideosec5','5');
    	}
    });
    
    const videoFile6 = document.querySelector('#videoFile6');
    
    videoFile6.addEventListener("change", async e => {
        var flag = true;
    	var imageType = /video.*/;  
    	var file = e.target.files[0];
    	// check file type
    	if (!file.type.match(imageType)) {  
    	  alert("File \""+file.name+"\" is not a valid video file, Please upload only video file");
    	  flag = false;
    	  return false;	
    	} 
    	// check file size
    	if (parseInt(file.size / 1024) > (1024*50)) {  
    	  alert("File \""+file.name+"\" is too big. I am using shared server :P");
    	  flag = false;
    	  return false;	
    	} 
    	if(flag)
    	{
    	    const fileUrl = URL.createObjectURL(file);
            const thumbUrl = await getThumbnailForVideo(fileUrl);
            $('#generatedvideosecthumb6').val(thumbUrl);
            $('#previewvideosec6').attr('src',thumbUrl);
            $('.u-progress6').show();
    	    handleFiles1(file,'#uploadedvideosec6','6');
    	}
    });
    
    handleFiles1 = function (files,selector,num,e){
    		
    	var fileName = files.name;
    	fileName.split('.').slice(0, -1).join('.');
    	$('.u-title').html(fileName);
    	
    	var info = '<div class="u-size'+num+'">'+(parseInt(parseInt(files.size / 1024))/1024).toFixed(2)+' MB / <span class="up-done"></span> MB</div><div class="u-progress'+num+'"><div class="progress'+num+'"><div class="progress-bar'+num+'" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"><span class="sr-only'+num+'">0% Complete</span></div></div><div class="u-close"><a href="#"><i class="cvicon-cv-cancel"></i></a></div></div>';
    			
    	$("#upload-progress"+num).show("fast",function(){
    		$("#upload-progress"+num).html(info); 
    		uploadVideoFile1(files,selector,num);
    	});
    		
      }
      /*Upload Video File*/
      uploadVideoFile1 = function(file,target,num){
    	 console.log(file);
    	 
    	// check if browser supports file reader object 
        	if (typeof FileReader !== "undefined"){
        	//alert("uploading "+file.name);  
        	reader = new FileReader();
        	reader.onload = function(e){
        	    console.log(e);
        		//alert(e.target.result);
        		//$('.preview img').attr('src',e.target.result).css("width","70px").css("height","70px");
        	}
        	reader.readAsDataURL(file);
        	
        	var formdata = new FormData();
        	formdata.append("video", file);
        	
        	xhr = new XMLHttpRequest();
        	xhr.open("POST", "upload/video");
        	
        	xhr.upload.addEventListener("progress", function (event) {
        		//console.log(event);
        		if (event.lengthComputable) {
        			$(".progress-bar"+num).css("width",(event.loaded / event.total) * 100 + "%");
        			$(".progress-bar"+num).css("aria-valuenow",(event.loaded / event.total) * 100);
        			$(".sr-only"+num).html(" "+((event.loaded / event.total) * 100).toFixed() + "%");
        			$(".up-done").html((parseInt(parseInt(event.loaded / 1024))/1024).toFixed(2));
        			$('.percent-process'+num).html('<strong>'+parseInt((event.loaded / event.total) * 100).toFixed(0)+'%</strong>');
        		}
        		else {
        			alert("Failed to compute file upload length");
        		}
        	}, false);
        
        	xhr.onreadystatechange = function (oEvent) {  
        	  if (xhr.readyState === 4) {  
        		if (xhr.status === 200) {  
        		  $(".progress-bar"+num).css("width","100%");
        		  $(".progress-bar"+num).attr("aria-valuenow","100");
        		  $(".sr-only"+num).html("100%");
        		  $(".up-done").html((parseInt(parseInt(file.size / 1024))/1024).toFixed(2));
        		  $('.u-desc'+num).html("<strong>100% Completed.</strong> Your video has been successfully upload.");
        		  $('.u-desc'+num).addClass('text-success');
        		} else {  
        		  alert("Error"+ xhr.statusText);  
        		}  
        	  }  
        	}; 
        	
        	xhr.onload  = function() {
        	   var jsonResponse = xhr.response;
        	   result = JSON.parse(jsonResponse);
        	   if(result.status == true){
                    $(target).val(result.file);
        	   }else{
        		   alert('File is not uploading. Please try again.')
        	   }	  
        	};
        
        	
        	var csrfToken = $('form').find('input[name=_token]').val();
        	xhr.setRequestHeader('X-csrf-token', csrfToken); 	
        	
        	xhr.send(formdata);
        
        	}else{
        		alert("Your browser doesnt support FileReader object");
        	} 		
          }
          
    function getTeamImage(teamname,num)
    {
        var csrfToken = $('form').find('input[name=_token]').val();
        $.ajax({
           url:'{{ URL::to("/getTeamImage") }}', 
           data:{teamname:teamname,'_token':csrfToken},
           type:'post',
           success:function(res)
           {
               $('#previewteamsec'+num).attr('src',res);
               $('#uploaded_team_image'+num).val(res);
           }
        });
    }
    
    function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
     tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
     tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
    }
</script>
@endsection	