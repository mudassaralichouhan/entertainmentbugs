@extends('layouts.home')
@section('content')

  
  <style>#live-video-user-list-home{display:none}</style>
  
  
  
           
	<style>
 #create-profile{margin:40px 0}
#top-check-field0 h2{font-size:22px; margin:0 0 0 0}
.option-block{float:left; width:33.3%}
.field strong{margin-top:10px;display: block;font-weight:600;font-family: 'Hind Guntur', sans-serif;}
#input-field0 label{font-weight:500;font-family: 'Hind Guntur', sans-serif;margin:0px 0 3px 0;}
#input-field0 .form-group {margin-bottom: 18px;}
#input-field0 textarea {padding-top: 10px;font-size: 13px;}

#what-we-de-bl label{font-weight:600;font-family: 'Hind Guntur', sans-serif;margin:0px 0 3px 0;}
#what-we-de-bl .form-group {margin-bottom: 18px;}
#what-we-de-bl  textarea {padding-top: 10px;font-size: 13px;}
#what-we-de-bl h2{font-size:22px; margin:0 0 20px 0}
#input-field0 h2{font-size:22px; margin:0 0 20px 0}
#portfolio-block h2{font-size:22px; margin:0 0 20px 0}

#portfolio-block label {font-weight: 600;font-family: 'Hind Guntur', sans-serif;margin: 0px 0 3px 0;}
textarea {padding-top: 0;margin: 0 0 -10px 0;}


#showreel-block .form-group {margin-bottom: 10px;}
.two.fields{margin-bottom:20px;clear:both; overflow:hidden;}
.btn-cv1 {padding: 11px;}
#select-btn5 h2{    margin-bottom: 0;font-weight: 500;}
.tab4{background: #eceff0; margin-bottom:0px;clear:both; overflow:hidden;} 
.tab4 a {padding: 10px 20px;text-align: Center;color: #000;line-height: 45px;}
.tab4 a.active{    background: #28b47e !important;color:#fff;}
.w3-green, .w3-hover-green:hover {color: #fff!important;background-color: #f72e5e !important;text-align: Center;line-height: 17px;padding: 3px 0 0 0;}
.w3-light-grey {color: #000!important;margin-top:8px;background-color: #eceff0 !important;}
.form-group .form-control{padding:10px 20px;}
select {border:solid 1px #e0e1e2}   
.edit-page .u-area {text-align: center;padding-top: 12px;padding-bottom: 20px;}
#select-p i{color: #637076;font-size: 36px;text-align: center;display: block;padding: 32px 0 0 0;}
.upload-page .u-area i {font-size: 90px;color: #637076;}
.upload-page .u-area .u-text1 {margin-top: 10px;margin-bottom: 9px;font-size: 14px;}
#portfolio-block .browse-photo{width:100px; height:100px; background:#eceff0}
#portfolio-block .form-group {margin-bottom: 10px;}
#portfolio-block .col-lg-6{margin:5px 0 20px 0;}
#portfolio-block .btn-save{margin-top:10px;}
#showreel-block h2{font-size:22px; margin:0 0 20px 0}
#showreel-block label {font-weight: 600;font-family: 'Hind Guntur', sans-serif;margin: 0px 0 3px 0;}
#portfolio-block .progress {width: 90%;margin-bottom: 0;height: 8px;background-color: #eceff0;-webkit-box-shadow: none;box-shadow: none;}
#portfolio-block .cgt{position: absolute;top: -6px;right: 21px;}
.u-progress{position:relative; margin-top:10px;}
.tabcontent h2 {  font-size: 22px;margin: 0 0 20px 0;}
.tab {overflow: hidden;border: 1px solid #ccc;background-color: #f1f1f1;}
.tab button {background-color: inherit;float: left;border: none;outline: none;cursor: pointer;padding: 16px 20px 10px 20px;transition: 0.3s;font-size: 14px;}
.tab button:hover {background-color: #ddd;}
.tab button.active {background-color: #ccc;}
.tabcontent {padding: 6px 12px;border: 1px solid #ccc;border-top: none;display:none;}
.tabcontent{margin-top:10px;}
label {margin-bottom: 10px;font-weight: 500;}
</style>



 <!--Production house Start-->
  <div class="content-wrapper">
   <div class="audition_main_list" id="auditions" style="background:#f4f3f3">
      <div id="create-profile-tab">
         <div class="container">
            <div class="rows" style="background:#fff;padding:0px; margin-top:10px">
                <div class="tab tab4">
                    <button class="tablinks active" onclick="openCity(event, 'about')">About</button>
                    <button class="tablinks" onclick="openCity(event, 'what-we-de-bl')">What we do</button>
                    <button class="tablinks" onclick="openCity(event, 'latest-project')">Latest project</button>
                     <button class="tablinks" onclick="openCity(event, 'awards')">Awards</button>
                    <button class="tablinks" onclick="openCity(event, 'portfolio')">My Gallery</button>
                    <button class="tablinks" onclick="openCity(event, 'video')">Video</button>
                    <button class="tablinks" onclick="openCity(event, 'team')">My Team</button> 
                    <button class="tablinks" onclick="openCity(event, 'contact')">Contact</button> 
                     <a target="_blank" href="{{URL::to('production-house-profile')}}" style="float: right;width: 150px;margin: 0;padding: 0 10px;background: #28b47e !important;color: #fff;" target="_blank">Preview Profile</a>
                    
                    
               </div>
            </div>
         </div> 
         <div class="container">
            <div class="w3-light-grey">
               <div class="w3-container w3-green w3-center" style="width:12.5%">12.5%</div>
            </div>
         </div> 
            <div id="about" style="display:block" class="tabcontent">
                <div class="container">
               <div class="rows" style="background:#fff;padding: 30px 15px;margin: 0;">
                  <h2 style="margin-bottom: 3px;">About -  Production House</h2>
                   <?php 
                    $userId = session()->get('user_id');
                    $getIfAlready = DB::select('select * from production_about where user_id='.$userId);
                  ?>
                  <p>Create your profile as a Production House</p>
                   <form action="{{URL::to('save-production-profile')}}" id="productionForm" style="margin-top:0px" enctype="multipart/form-data" method="post">
                     <div class="form-group  col-lg-4 col-sm-6 ">
                        <label> Name </label>   
                        <input type="text" required name="name" class="form-control" id="exampleInputEmail1" 
                        placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->name != NULL) ? $getIfAlready[0]->name : '') : '')); ?>">
                     </div>
                     <div class="form-group col-lg-4 col-sm-6">
                        <label> Language* (Multiple) all global language </label>   
                        <input type="text" class="form-control" name="language" required placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->language != NULL) ? $getIfAlready[0]->language : '') : '')); ?>">
                     </div>
                     <div class="form-group col-lg-4 col-sm-6">
                        <label> Working Hours:* </label>  
                        <div class="clear"></div>
                        <select style="width:49%;padding:0 10px;border:solid 1px #e0e1e2" name="working_hours_one">
                           <option value="6am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '6am') ? 'selected' : '') : '') : ''); ?>>6am</option>
                           <option value="7am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '7am') ? 'selected' : '') : '') : ''); ?>>7am</option>
                           <option value="8am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '8am') ? 'selected' : '') : '') : ''); ?>>8am</option>
                           <option value="9am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '9am') ? 'selected' : '') : '') : ''); ?>>9am</option>
                           <option value="10am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '10am') ? 'selected' : '') : '') : ''); ?>>10am</option>
                           <option value="11am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '11am') ? 'selected' : '') : '') : ''); ?>>11am</option>
                           <option value="12am" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_one != NULL) ? (($getIfAlready[0]->working_hours_one == '12am') ? 'selected' : '') : '') : ''); ?>>12am</option>
                           
                        </select>
                        <select style="width:49%;padding:0 10px; float:right;border:solid 1px #e0e1e2" name="working_hours_two">
                           <option value="4pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '4pm') ? 'selected' : '') : '') : ''); ?>>4pm</option>
                           <option value="5pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '5pm') ? 'selected' : '') : '') : ''); ?>>5pm</option>
                           <option value="6pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '6pm') ? 'selected' : '') : '') : ''); ?>>6pm</option>
                           <option value="7pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '7pm') ? 'selected' : '') : '') : ''); ?>>7pm</option>
                           <option value="8pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '8pm') ? 'selected' : '') : '') : ''); ?>>8pm</option>
                           <option value="9pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '9pm') ? 'selected' : '') : '') : ''); ?>>9pm</option>
                           <option value="10pm" <?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->working_hours_two != NULL) ? (($getIfAlready[0]->working_hours_two == '10pm') ? 'selected' : '') : '') : ''); ?>>10pm</option>
                           
                        </select>
                     </div>
                     <div class="form-group col-lg-4 col-sm-6">
                        <label for="exampleInputEmail1">Country* </label>
                        <div class="clear"></div>
                         <select id="country" name="country" required onchange="getStates(this.value)" class="select2 input-select" style="width:100%">
                            <option value="">Select Country</option>
                            <?php
                                $getCountries = DB::table('tbl_countries')->get();
                                foreach($getCountries as $k=>$v)
                                {
                            ?>
                            <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->country != NULL) ? (($getIfAlready[0]->country == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                            <?php } ?>
                        </select>
                     </div>
                     <div class="form-group col-lg-4 col-sm-6"> 
                        <label>Location* (State)  </label>    
                         <select class="select2 input-select" style="width:100%" onchange="getCities(this.value)" required id="state" name="state">
                            <option value="">Select State</option>
                            <?php
                                if(count($getIfAlready) > 0)
                                {
                                    if($getIfAlready[0]->country != NULL || $getIfAlready[0]->country != '')
                                    {
                                        $getCountryId = DB::table('tbl_countries')->where('name',$getIfAlready[0]->country)->get();
                                        $getStates = DB::table('tbl_states')->where('country_id',$getCountryId[0]->id)->get();
                                        foreach($getStates as $k=>$v)
                                        {
                            ?>
                            <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->state != NULL) ? (($getIfAlready[0]->state == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                            <?php } ?>
                            <?php } } ?>
                        </select>
                     </div>
                     <div class="form-group col-lg-4 col-sm-6"> 
                        <label> City* </label>  
                        <select class="select2 input-select" style="width:100%" required id="city" name="city">
                            <option value="">Select City</option>
                            <?php
                                if(count($getIfAlready) > 0)
                                {
                                    if($getIfAlready[0]->state != NULL || $getIfAlready[0]->state != '')
                                    {
                                        $getStateId = DB::table('tbl_states')->where('name',$getIfAlready[0]->state)->get();
                                        $getCities = DB::table('tbl_cities')->where('state_id',$getStateId[0]->id)->get();
                                        foreach($getCities as $k=>$v)
                                        {
                            ?>
                            <option value="<?php echo $v->name; ?>"<?php echo ((count($getIfAlready) > 0) ? (($getIfAlready[0]->city != NULL) ? (($getIfAlready[0]->city == $v->name) ? 'selected' : '') : '') : ''); ?>><?php echo $v->name; ?></option>
                            <?php } ?>
                            <?php } } ?>
                        </select>
                     </div>
                     <div class="col-lg-3">
                        <div class="form-group"> <label for="exampleInputEmail1">Facebook*</label>  
                         <input type="text" class="form-control" name="facebook" required 
                         placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->facebook != NULL) ? $getIfAlready[0]->facebook : '') : '')); ?>">
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="form-group"> <label for="exampleInputEmail1">Instagram*</label> 
                         <input type="text" class="form-control" name="instagram" required 
                         placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->instagram != NULL) ? $getIfAlready[0]->instagram : '') : '')); ?>">
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="form-group"> <label for="exampleInputEmail1">Youtube*</label> 
                         <input type="text" class="form-control" name="youtube" required 
                         placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->youtube != NULL) ? $getIfAlready[0]->youtube : '') : '')); ?>">
                        </div>
                     </div>
                     <div class="col-lg-3">
                        <div class="form-group"> <label for="exampleInputEmail1">Linkedin*</label>  
                         <input type="text" class="form-control" name="linkedin" required 
                         placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->linkedin != NULL) ? $getIfAlready[0]->linkedin : '') : '')); ?>">
                        </div>
                     </div>
                     <div class="form-group col-lg-3 col-sm-6">
                        <label>  Production House Registered date* </label>  
                        <input type="date" class="form-control" name="production_house_date" required 
                         placeholder="" value="<?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->production_house_date != NULL) ? $getIfAlready[0]->production_house_date : '') : '')); ?>">
                     </div>
                     <div class="form-group col-lg-4 col-sm-6">
                        <label> Upload profile photo </label>  
                        <input type="file" id="myfile" name="myfile" style="margin-top:10px">
                   
                     </div>
                     <div class="form-group col-lg-12">
                        <label> Best Tagline  (25 words) - this line will be display in profile*</label>   
                      
                        <textarea id="w3review" required name="best_tagline" rows="4" cols="60"><?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->best_tagline != NULL) ? $getIfAlready[0]->best_tagline : '') : '')); ?></textarea> 
                     </div>
                     <div class="form-group col-lg-12">
                        <label> ABOUT  (200 words)*</label>   
                      <textarea id="w3review" required name="about" rows="4" cols="60"><?php echo ((count($getIfAlready) > 0 ? (($getIfAlready[0]->about != NULL) ? $getIfAlready[0]->about : '') : '')); ?></textarea> 
                     </div>
                     <div class="clear"></div>
                     <div class="rows">
                        <h2 style="font-size: 16px;margin:19px 0  13px 10px">Select upto 4 <strong>multiple options,</strong> which  represents to your profile.</h2>
                  
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="two fields" id="select-btn5">
                  <div class="ui padded two columns stackable grid">
                       <?php 
                                $categoryarr = [];
                                if(count($getIfAlready) > 0)
                                {
                                    if($getIfAlready[0]->multiple_category != NULL)
                                    {
                                        $explode = explode(',',$getIfAlready[0]->multiple_category);
                                        if(count($explode) > 0)
                                        {
                                            foreach($explode as $k=>$v)
                                            array_push($categoryarr,$v);
                                        }
                            ?>
                            <?php    
                                    }
                                }
                            ?>
                      
                      
                  <div class="col-lg-4">	
                  <input  name="phousecheck[]" type="checkbox" value="Co-Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Co-Director',$categoryarr)) ? 'checked' : '') : ''); ?> > Co-Director <br>
                  <input  name="phousecheck[]" type="checkbox" value="Film Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Film Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Film Director<br>
                  <input  name="phousecheck[]" type="checkbox" value="Short Film Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Short Film Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Short Film Director<br>
                  <input  name="phousecheck[]" type="checkbox" value="Tv Serial Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Tv Serial Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Tv Serial Director<br>
                  <input  name="phousecheck[]" type="checkbox" value="Tv Serial Assistant Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Tv Serial Assistant Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Tv Serial Assistant Director<br>
                  <input  name="phousecheck[]" type="checkbox" value="Art Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Art Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Art Director<br>
                  <input  name="phousecheck[]" type="checkbox" value="Music Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Music Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Music Director<br>
                  <input  name="phousecheck[]" type="checkbox" value="Unit Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Unit Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Unit Director<br>
                  <input  name="phousecheck[]" type="checkbox" value="Assistant Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Assistant Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Assistant Director<br>
                  <input  name="phousecheck[]" type="checkbox" value="Corporate Film Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Corporate Film Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Corporate Film Director<br>
                  <input  name="phousecheck[]" type="checkbox" value="Stunt director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Stunt director',$categoryarr)) ? 'checked' : '') : ''); ?>> Stunt director<br>
                  </div>
                  <div class="col-lg-4">	
                  <h2 style="font-size:16px;">File and Tv Producer</h2>
                  <input  name="phousecheck[]" type="checkbox" value="Film Production house" <?php echo ((count($categoryarr) > 0) ? ((in_array('Film Production house',$categoryarr)) ? 'checked' : '') : ''); ?>> Film Production house<br>
                  <input  name="phousecheck[]" type="checkbox" value="Film Producer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Film Producer',$categoryarr)) ? 'checked' : '') : ''); ?>> Film Producer<br>
                  <input  name="phousecheck[]" type="checkbox" value="Tv Serial Production house" <?php echo ((count($categoryarr) > 0) ? ((in_array('Tv Serial Production house',$categoryarr)) ? 'checked' : '') : ''); ?>> Tv Serial Production house<br>
                  <input  name="phousecheck[]" type="checkbox" value="Tv Serial poroducer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Tv Serial poroducer',$categoryarr)) ? 'checked' : '') : ''); ?>> Tv Serial poroducer<br>
                  <br>
                  <h2 style="font-size:16px;">Casting</h2>
                  <input  name="phousecheck[]" type="checkbox" value="Casting Director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Casting Director',$categoryarr)) ? 'checked' : '') : ''); ?>> Casting Director<br>
                  <input  name="phousecheck[]" type="checkbox" value="Casting Co-ordinator" <?php echo ((count($categoryarr) > 0) ? ((in_array('Casting Co-ordinator',$categoryarr)) ? 'checked' : '') : ''); ?>> Casting Co-ordinator<br>
                  <input  name="phousecheck[]" type="checkbox" value="Casting Agent" <?php echo ((count($categoryarr) > 0) ? ((in_array('Casting Agent',$categoryarr)) ? 'checked' : '') : ''); ?>> Casting Agent<br>
                  </div>
                  <div class="col-lg-4">	
                  <h2 style="font-size:16px;">Producer and Manager</h2>
                  <input  name="phousecheck[]" type="checkbox" value="Executive Producer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Executive Producer',$categoryarr)) ? 'checked' : '') : ''); ?>> Executive Producer<br>
                  <input  name="phousecheck[]" type="checkbox" value="Production Manager" <?php echo ((count($categoryarr) > 0) ? ((in_array('Production Manager',$categoryarr)) ? 'checked' : '') : ''); ?>> Production Manager<br>
                  <input  name="phousecheck[]" type="checkbox" value="Associate producer" <?php echo ((count($categoryarr) > 0) ? ((in_array('Associate producer',$categoryarr)) ? 'checked' : '') : ''); ?>> Associate producer<br>
                  <br>
                  <h2 style="font-size:16px;">Others</h2>
                  <input  name="phousecheck[]" type="checkbox" value="Model Co-ordinator" <?php echo ((count($categoryarr) > 0) ? ((in_array('Model Co-ordinator',$categoryarr)) ? 'checked' : '') : ''); ?>> Model Co-ordinator<br>
                  <input  name="phousecheck[]" type="checkbox" value="Adv Film Maker" <?php echo ((count($categoryarr) > 0) ? ((in_array('Adv Film Maker',$categoryarr)) ? 'checked' : '') : ''); ?>> Adv Film Maker<br>
                  <input  name="phousecheck[]" type="checkbox" value="Visual effects creative director" <?php echo ((count($categoryarr) > 0) ? ((in_array('Visual effects creative director',$categoryarr)) ? 'checked' : '') : ''); ?>> Visual effects creative director<br>
                  <input  name="phousecheck[]" type="checkbox" value="Others" <?php echo ((count($categoryarr) > 0) ? ((in_array('Others',$categoryarr)) ? 'checked' : '') : ''); ?>> Others<br>
                  </div> 
                  </div>
                  </div> <div class="clear"></div>    
                  <p style="color:red">Select any one the category</p>
                  <div class="row">
                  <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlready) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div> 
                  <div class="col-lg-2" style="float:right"><button type="button" class="btn btn-cv1">Next</button></div> 
                  </div>
                  
                  </div>                         
                  <div style="clear:both; overflow:hidden"></div>
                  </form>
               </div>
            </div>
            </div>
            <div id="what-we-de-bl" class="tabcontent">
               <div class="container">
                  <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                       <?php 
                        $userId = session()->get('user_id');
                        $getIfAlreadyWhatWedo = DB::select('select * from production_what_we_do where user_id='.$userId);
                    ?>
                    <form action="{{URL::to('production-what-we-do-profile')}}" id="whatwedoform" style="margin-top:0px" enctype="multipart/form-data" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                      
                     <div class="form-group achievemets">
                        <h2>What we do</h2>
                        <div class="form-group col-lg-12">
                           <label for="exampleInputEmail1">What we do</label>
                           <input type="text" name="what_we_do" class="form-control" required id="exampleInputEmail1" placeholder="What we do" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->what_we_do != NULL) ? $getIfAlreadyWhatWedo[0]->what_we_do : '') : '')); ?>">
                        </div>
                        <div class="form-group col-lg-12">
                           <label for="exampleInputEmail1"> Description (50 words)</label>
                            <textarea id="w3review" name="description" rows="4" cols="50" required><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->description != NULL) ? $getIfAlreadyWhatWedo[0]->description : '') : '')); ?></textarea> 
                        </div>
                        <div style="clear:both; overflow:hidden"></div>
                        <div id="copydiv">
                        <div class="col-lg-3">
                           <div class="form-group"> <label for="exampleInputEmail1">Title</label>
                           
                           <input type="text" required class="form-control" name="title[]" id="exampleInputEmail1" placeholder="eg: DIRECTORS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_1 != NULL) ? $getIfAlreadyWhatWedo[0]->title_1 : '') : '')); ?>">  
                           </div>
                           <div class="form-group">  <label for="exampleInputEmail1"> Description (50 words)</label>
                              <textarea name="desc[]" required rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_1 != NULL) ? $getIfAlreadyWhatWedo[0]->des_1 : '') : '')); ?></textarea> 
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="form-group"> <label for="exampleInputEmail1">Title</label> 
                           
                           <input type="text" required name="title[]" class="form-control" id="exampleInputEmail1" placeholder="eg: TV SHOWS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_2 != NULL) ? $getIfAlreadyWhatWedo[0]->title_2 : '') : '')); ?>">  
                           </div>
                           <div class="form-group">  <label for="exampleInputEmail1"> Description (50 words)</label>
                              <textarea name="desc[]" required rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_2 != NULL) ? $getIfAlreadyWhatWedo[0]->des_2 : '') : '')); ?></textarea>  
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="form-group"> <label for="exampleInputEmail1">Title</label>  
                           
                           <input type="text" required name="title[]" class="form-control" id="exampleInputEmail1" placeholder="eg: FILM FESTIVALS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_3 != NULL) ? $getIfAlreadyWhatWedo[0]->title_3 : '') : '')); ?>">  
                           </div>
                           <div class="form-group">  <label for="exampleInputEmail1"> Description (50 words)</label>
                              <textarea name="desc[]" required rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_3 != NULL) ? $getIfAlreadyWhatWedo[0]->des_3 : '') : '')); ?></textarea> 
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="form-group"> <label for="exampleInputEmail1">Title</label> 
                           
                           <input type="text" required name="title[]" class="form-control" id="exampleInputEmail1" placeholder="eg: FILM AWARDS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_4 != NULL) ? $getIfAlreadyWhatWedo[0]->title_4 : '') : '')); ?>">  
                           </div>
                           <div class="form-group">  <label for="exampleInputEmail1"> Description (50 words)</label>
                              <textarea name="desc[]" required rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_4 != NULL) ? $getIfAlreadyWhatWedo[0]->des_4 : '') : '')); ?></textarea> 
                           </div>
                        </div>
                        </div>
                        
                        
                            <style>
                                .d-none{
                                    display:none!important;
                                }
                            </style>
                            <a href="javascript:void(0)" id="addMore" onclick="addMoreWhatWeDo(this)" style="background: #f72e5e;color: #fff;padding: 0 10px;" class="<?php if(count($getIfAlreadyWhatWedo) > 0){ if($getIfAlreadyWhatWedo[0]->title_5 != NULL || $getIfAlreadyWhatWedo[0]->title_6 != NULL || $getIfAlreadyWhatWedo[0]->title_7 != NULL || $getIfAlreadyWhatWedo[0]->title_8 != NULL){ echo "d-none"; } }else{ echo "d-none"; } ?>">+ Add More</a>
                            <a href="javascript:void(0)" id="removeMore" onclick="removeWhatWeDo(this)" style="background: #f72e5e;color: #fff;padding: 0 10px;" class="<?php if(count($getIfAlreadyWhatWedo) > 0){ if($getIfAlreadyWhatWedo[0]->title_5 == NULL && $getIfAlreadyWhatWedo[0]->title_6 == NULL && $getIfAlreadyWhatWedo[0]->title_7 == NULL && $getIfAlreadyWhatWedo[0]->title_8 == NULL){ echo "d-none"; } }else{ echo "d-none"; } ?>">- Remove</a>
                            <br> <br>
                            <div id="clonediv" class="<?php if(count($getIfAlreadyWhatWedo) > 0){ if($getIfAlreadyWhatWedo[0]->title_5 == NULL && $getIfAlreadyWhatWedo[0]->title_6 == NULL && $getIfAlreadyWhatWedo[0]->title_7 == NULL && $getIfAlreadyWhatWedo[0]->title_8 == NULL){ echo "d-none"; } } ?>">
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Title</label>  
                                        <input type="text" name="title[]" id="title5" class="form-control" placeholder="eg: FILM AWARDS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_5 != NULL) ? $getIfAlreadyWhatWedo[0]->title_5 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"> Description (25 words)</label>
                                        <textarea id="desc5" name="desc[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_5 != NULL) ? $getIfAlreadyWhatWedo[0]->des_5 : '') : '')); ?></textarea> 
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Title</label>  
                                        <input type="text" name="title[]" class="form-control" id="title6" placeholder="eg: FILM AWARDS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_6 != NULL) ? $getIfAlreadyWhatWedo[0]->title_6 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"> Description (25 words)</label>
                                        <textarea id="desc6" name="desc[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_6 != NULL) ? $getIfAlreadyWhatWedo[0]->des_6 : '') : '')); ?></textarea> 
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Title</label>  
                                        <input type="text" name="title[]" class="form-control" id="title7" placeholder="eg: FILM AWARDS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_7 != NULL) ? $getIfAlreadyWhatWedo[0]->title_7 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"> Description (25 words)</label>
                                        <textarea id="desc7" name="desc[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_7 != NULL) ? $getIfAlreadyWhatWedo[0]->des_7 : '') : '')); ?></textarea> 
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group"> 
                                        <label for="exampleInputEmail1">Title</label>  
                                        <input type="text" name="title[]" class="form-control" id="title8" placeholder="eg: FILM AWARDS" value="<?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->title_8 != NULL) ? $getIfAlreadyWhatWedo[0]->title_8 : '') : '')); ?>">  
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1"> Description (25 words)</label>
                                        <textarea id="desc8" name="desc[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyWhatWedo) > 0 ? (($getIfAlreadyWhatWedo[0]->des_8 != NULL) ? $getIfAlreadyWhatWedo[0]->des_8 : '') : '')); ?></textarea> 
                                    </div>
                                </div>
                            </div>
                        <br>
                        Note: Minimum 4 you have to insert and max 8
                     </div>
                     <div style="clear:both; overflow:hidden"></div>
                     <div class="row">
                        <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyWhatWedo) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                     </div>
                     </form>
                  </div>
               </div>
            </div>
            <div id="latest-project" class="tabcontent">
               <div class="container">
                  <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                     <h2>Latest Projects - upto 3</h2>
                     
                     <form action="{{URL::to('/production-save-project')}}" method="post" enctype="multipart/form-data" id="projectform">
                        <?php 
                            $userId = session()->get('user_id');
                            $getIfAlreadyProject = DB::select('select * from production_latest_project where user_id='.$userId);
                        ?>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" name="oldProjid[]" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->id != NULL) ? $getIfAlreadyProject[0]->id : '') : '')); ?>" />
                        <input type="hidden" name="oldProjFile[]" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->photo != NULL) ? $getIfAlreadyProject[0]->photo : '') : '')); ?>" />
                     <div class="form-group col-lg-6">
                        <label for="exampleInputEmail1">Select category</label>  
                        <select class="form-control input-select" style="padding: 10px;" name="categoryproject[]">
                         <option value="" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                           <option value="Co-Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                           <option value="Film Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                           <option value="Short Film Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                           <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->category != NULL) ? (($getIfAlreadyProject[0]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                           
                        </select>
                     </div>
                     <div class="form-group col-lg-6">
                        <label for="exampleInputEmail1">Title</label>  
                        <input type="text" name="projecttitle[]" class="form-control" id="exampleInputEmail1" 
                        value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->title != NULL) ? $getIfAlreadyProject[0]->title : '') : '')); ?>">  
                     </div>
                       <div class="form-group col-lg-4">
                        <label for="exampleInputEmail1">Date</label>  
                        <input type="date" name="projectdate[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->date != NULL) ? $getIfAlreadyProject[0]->date : '') : '')); ?>">  
                     </div>
                     <div class="form-group col-lg-4">
                        <label for="exampleInputEmail1">Add Link - If any</label>  
                       <input type="text" name="projectlink[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->link != NULL) ? $getIfAlreadyProject[0]->link : '') : '')); ?>">  
                     </div>
                     <div class="form-group col-lg-4">
                        <label for="exampleInputEmail1">Button Name</label>  
                        <select class="form-control" name="projectbuttonname[]" style="padding: 10px;">
                            <option value="" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                            <option value="View Video" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "View Video") ? 'selected' : '') : '') : ''); ?>>View Video</option>
                            <option value="View Now" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "View Now") ? 'selected' : '') : '') : ''); ?>>View Now</option>
                            <option value="Official Trailer" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "Official Trailer") ? 'selected' : '') : '') : ''); ?>> Official Trailer</option>
                            <option value="View Profile" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "View Profile") ? 'selected' : '') : '') : ''); ?>>View Profile</option>
                            <option value="View More" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "View More") ? 'selected' : '') : '') : ''); ?>>View More</option>
                            <option value="No Button" <?php echo ((count($getIfAlreadyProject) >= 1) ? (($getIfAlreadyProject[0]->button_name != NULL) ? (($getIfAlreadyProject[0]->button_name == "No Button") ? 'selected' : '') : '') : ''); ?>>No Button</option>
                         </select>
                     </div>
                     <div class="form-group col-lg-12">  
                        <label for="exampleInputEmail1">Description 200 words</label>
                       <textarea id="w3review" name="projectdesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyProject) >= 1 ? (($getIfAlreadyProject[0]->description != NULL) ? $getIfAlreadyProject[0]->description : '') : '')); ?></textarea>
                     </div>
                     <div class="clear"></div>
                       <a href="javascript:void(0)" onclick="openProjUploader('1')" style="background: #637076;color: #fff;width:145px;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                      Browse Banner Photo</a>
                       <input type="file" style="display:none;" name="fileproj[]" id="fileproj1" onchange="showProjFileLabel('1')">
                     <div class="clear"></div>
                     <p id="fileprojlabel1" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                       <h6>Second</h6>
                                             <div class="form-group col-lg-6">
                          <input type="hidden" name="oldProjid[]" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->id != NULL) ? $getIfAlreadyProject[1]->id : '') : '')); ?>" />
                          <input type="hidden" name="oldProjFile[]" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->photo != NULL) ? $getIfAlreadyProject[1]->photo : '') : '')); ?>" />
                            <label for="exampleInputEmail1">Select category</label>  
                           <select class="form-control input-select" style="padding: 10px;" name="categoryproject[]">
                         <option value="" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                           <option value="Co-Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                           <option value="Film Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                           <option value="Short Film Director" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                           <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyProject) >= 21) ? (($getIfAlreadyProject[1]->category != NULL) ? (($getIfAlreadyProject[1]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                           
                        </select>
                      </div>
                      <div class="form-group col-lg-6">
                         <label for="exampleInputEmail1">Title</label>  
                         <input type="text" name="projecttitle[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->title != NULL) ? $getIfAlreadyProject[1]->title : '') : '')); ?>">  
                      </div>
                       <div class="form-group col-lg-4">
                         <label for="exampleInputEmail1">Date</label>  
                         <input type="date" name="projectdate[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->date != NULL) ? $getIfAlreadyProject[1]->date : '') : '')); ?>">  
                      </div>
                      <div class="form-group col-lg-4">
                         <label for="exampleInputEmail1">Add Link - If any</label>  
                         <input type="text" name="projectlink[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 2 ? (($getIfAlreadyProject[1]->link != NULL) ? $getIfAlreadyProject[1]->link : '') : '')); ?>">  
                      </div>
                      <div class="form-group col-lg-4">
                         <label for="exampleInputEmail1">Button Name</label>  
                         <select class="form-control" name="projectbuttonname[]" style="padding: 10px;">
                            <option value="" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                            <option value="View Video" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "View Video") ? 'selected' : '') : '') : ''); ?>>View Video</option>
                            <option value="View Now" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "View Now") ? 'selected' : '') : '') : ''); ?>>View Now</option>
                            <option value="Official Trailer" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "Official Trailer") ? 'selected' : '') : '') : ''); ?>> Official Trailer</option>
                            <option value="View Profile" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "View Profile") ? 'selected' : '') : '') : ''); ?>>View Profile</option>
                            <option value="View More" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "View More") ? 'selected' : '') : '') : ''); ?>>View More</option>
                            <option value="No Button" <?php echo ((count($getIfAlreadyProject) >= 2) ? (($getIfAlreadyProject[1]->button_name != NULL) ? (($getIfAlreadyProject[1]->button_name == "No Button") ? 'selected' : '') : '') : ''); ?>>No Button</option>
                         </select>
                      </div>
                      <div class="form-group col-lg-12">  
                         <label for="exampleInputEmail1">Description 200 words</label>
                         <textarea id="w3review" name="projectdesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->description != NULL) ? $getIfAlreadyProject[2]->description : '') : '')); ?></textarea>
                      </div>
                      <div class="clear"></div>
                      <a href="javascript:void(0)" onclick="openProjUploader('2')" style="background: #637076;color: #fff;width:145px;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                      Browse Banner Photo</a>
                        <input type="file" style="display:none;" name="fileproj[]" id="fileproj2" onchange="showProjFileLabel('2')">
                        <div class="clear"></div>
                        <p id="fileprojlabel2" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                    <div id="cloneprojdiv" class="d-none">
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Select category</label>  
                                <input type="hidden" name="oldProjFile[]" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->photo != NULL) ? $getIfAlreadyProject[2]->photo : '') : '')); ?>" />
                                <input type="hidden" name="oldProjid[]" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->id != NULL) ? $getIfAlreadyProject[2]->id : '') : '')); ?>" />
                                <select class="form-control input-select" style="padding: 10px;" name="categoryproject[]">
                         <option value="" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                           <option value="Co-Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                           <option value="Film Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                           <option value="Short Film Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                           <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->category != NULL) ? (($getIfAlreadyProject[2]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                           
                        </select>
                          </div>
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Title</label>  
                                <input type="text" name="projecttitle[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->title != NULL) ? $getIfAlreadyProject[2]->title : '') : '')); ?>">  
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Date</label>  
                                <input type="date" name="projectdate[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->date != NULL) ? $getIfAlreadyProject[2]->date : '') : '')); ?>">  
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Add Link - If any</label>  
                                <input type="text" name="projectlink[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->link != NULL) ? $getIfAlreadyProject[2]->link : '') : '')); ?>">  
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="exampleInputEmail1">Button Name</label>  
                                <select class="form-control" name="projectbuttonname[]" style="padding: 10px;">
                                    <option value="" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                    <option value="View Video" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "View Video") ? 'selected' : '') : '') : ''); ?>>View Video</option>
                                    <option value="View Now" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "View Now") ? 'selected' : '') : '') : ''); ?>>View Now</option>
                                    <option value="Official Trailer" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "Official Trailer") ? 'selected' : '') : '') : ''); ?>> Official Trailer</option>
                                    <option value="View Profile" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "View Profile") ? 'selected' : '') : '') : ''); ?>>View Profile</option>
                                    <option value="View More" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "View More") ? 'selected' : '') : '') : ''); ?>>View More</option>
                                    <option value="No Button" <?php echo ((count($getIfAlreadyProject) >= 3) ? (($getIfAlreadyProject[2]->button_name != NULL) ? (($getIfAlreadyProject[2]->button_name == "No Button") ? 'selected' : '') : '') : ''); ?>>No Button</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-12">  
                                <label for="exampleInputEmail1">Description 200 words</label>
                                <textarea id="w3review" name="projectdesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyProject) >= 3 ? (($getIfAlreadyProject[2]->description != NULL) ? $getIfAlreadyProject[2]->description : '') : '')); ?></textarea>
                            </div>
                            <div class="clear"></div>
                            <a href="javascript:void(0)" onclick="openProjUploader('3')" style="background: #637076;color: #fff;width:145px;margin:0px 0 20px 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                            Browse Banner Photo</a>
                            <input type="file" style="display:none;" name="fileproj[]" id="fileproj3" onchange="showProjFileLabel('3')">
                            <p id="fileprojlabel3" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                      </div>
                      <div class="row" style="margin-top:20px">
                         <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyProject) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                      </div>
                      </form>
                      <a href="javascript:void(0)" id="addProj" onclick="addProj()" style="background: #e5e5e5;color: #000;width:130px;margin:20px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                  Add More Projects</a>
                  <a href="javascript:void(0)" class="d-none" id="removeProj" onclick="removeProj()" style="background: #e5e5e5;color: #000;width:130px;margin:20px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                  Remove Projects</a>
                  </div>
               </div>
            </div>
            <div id="awards" class="tabcontent">
               <div class="container">
                  
                  <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                    <div class="form-group achievemets">
                        <?php 
                            $userId = session()->get('user_id');
                            $getIfAlreadyAchivement = DB::select('select * from production_awards where user_id='.$userId);
                        ?>
                        <form action="{{URL::to('save-production-achivement')}}" id="achivementForm" style="margin-top:0px" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                           <h2>My Awards upto 3</h2>
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Title</label>
                                <input type="text" name="titleachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->title_1 != NULL) ? $getIfAlreadyAchivement[0]->title_1 : '') : '')); ?>">  
                            </div>
                            <input type="hidden" name="id[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->id != NULL) ? $getIfAlreadyAchivement[0]->id : '') : '')); ?>">  
                            <input type="hidden" name="oldFile[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->photo_1 != NULL) ? $getIfAlreadyAchivement[0]->photo_1 : '') : '')); ?>">  
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Link - If any</label>
                                <input type="text" name="linkachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->link_1 != NULL) ? $getIfAlreadyAchivement[0]->link_1 : '') : '')); ?>">  
                            </div> 
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Date</label>
                                <input type="date" name="date[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->date_1 != NULL) ? date('Y-m-d',strtotime($getIfAlreadyAchivement[0]->date_1)) : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="exampleInputEmail1"> Description (200 words)</label>
                                <textarea id="w3review" name="descachieve[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->des_1 != NULL) ? $getIfAlreadyAchivement[0]->des_1 : '') : '')); ?></textarea> 
                            </div>
                            <div class="clear"></div>
                            <a href="javascript:void(0)" onclick="showAchivementUploader('1')" style="background: #637076;color: #fff;width:195px;margin:10px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                                Browse Achievement  Photo
                            </a>  
                            <input type="file" style="display:none;" name="fileachive[]" id="fileachive1" onchange="showFileLabel('1')">
                            <br>
                            <p id="fileachivelabel1" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                            <div style="clear:both; overflow:hidden"></div>
                            <Br>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img src="<?php echo ((count($getIfAlreadyAchivement) >= 1 ? (($getIfAlreadyAchivement[0]->photo_1 != NULL || $getIfAlreadyAchivement[0]->photo_1 != '') ? $getIfAlreadyAchivement[0]->photo_1 : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyAchivement) >= 1)
                                    {
                                        if($getIfAlreadyAchivement[0]->photo_1 != NULL || $getIfAlreadyAchivement[0]->photo_1 != '')
                                        {
                                ?>
                                <a href="<?php echo URL::to('/delete-award-image/'.$getIfAlreadyAchivement[0]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                    <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <br> Image size 350 x 350  <br>  <br>
                            <div style="clear:both; overflow:hidden"></div>
                            <Br>   
                            <div class="form-group col-lg-6">
                                <label for="exampleInputEmail1">Title</label>
                                <input type="text" name="titleachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->title_1 != NULL) ? $getIfAlreadyAchivement[1]->title_1 : '') : '')); ?>">  
                            </div>
                            <input type="hidden" name="id[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->id != NULL) ? $getIfAlreadyAchivement[1]->id : '') : '')); ?>">  
                            <input type="hidden" name="oldFile[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->photo_1 != NULL) ? $getIfAlreadyAchivement[1]->photo_1 : '') : '')); ?>">  
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Link - If any</label>
                                <input type="text" name="linkachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->link_1 != NULL) ? $getIfAlreadyAchivement[1]->link_1 : '') : '')); ?>">  
                            </div> 
                            <div class="form-group col-lg-3">
                                <label for="exampleInputEmail1">Date</label>
                                <input type="date" name="date[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->date_1 != NULL) ? date('Y-m-d',strtotime($getIfAlreadyAchivement[1]->date_1)) : '') : '')); ?>">
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="exampleInputEmail1"> Description (200 words)</label>
                                <textarea id="w3review" name="descachieve[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->des_1 != NULL) ? $getIfAlreadyAchivement[1]->des_1 : '') : '')); ?></textarea> 
                            </div>
                            <div class="clear"></div>
                            <a href="javascript:void(0)"  onclick="showAchivementUploader('2')" style="background: #637076;color: #fff;width:195px;margin:10px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                                Browse Achievement  Photo
                            </a>
                            <input type="file" style="display:none;" name="fileachive[]" id="fileachive2" onchange="showFileLabel('2')">
                            <br>
                            <p id="fileachivelabel2" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                            <div style="clear:both; overflow:hidden"></div>
                            <Br>
                            <div class="u-close" style="float:left; margin-right:20px;">
                                <img src="<?php echo ((count($getIfAlreadyAchivement) >= 2 ? (($getIfAlreadyAchivement[1]->photo_1 != NULL || $getIfAlreadyAchivement[1]->photo_1 != '') ? $getIfAlreadyAchivement[1]->photo_1 : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                <?php 
                                    if(count($getIfAlreadyAchivement) >= 2)
                                    {
                                        if($getIfAlreadyAchivement[1]->photo_1 != NULL || $getIfAlreadyAchivement[1]->photo_1 != '')
                                        {
                                ?>
                                <a href="<?php echo URL::to('/delete-achivement-image/'.$getIfAlreadyAchivement[1]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                    <i class="cvicon-cv-cancel"></i>
                                </a>
                                <?php } } ?>
                            </div>
                            <div class="clear"></div>
                            <div style="clear:both; overflow:hidden"></div>
                            <Br>   
                            <div class="clear"></div>
                            <a href="javascript:void(0)" id="addMoreAchivement" onclick="addMoreAchivement(this)" style="background: #f72e5e;color: #fff;padding: 0 10px;" class="<?php if(count($getIfAlreadyAchivement) >= 3){ echo "d-none"; }else{ } ?>">+ Add More</a>
                            <a href="javascript:void(0)" id="removeMoreAchivement" onclick="removeMoreAchivement(this)" style="background: #f72e5e;color: #fff;padding: 0 10px;" class="<?php if(count($getIfAlreadyAchivement) <= 2){ echo "d-none"; } ?>">- Remove</a>
                            <div style="clear:both; overflow:hidden"></div>
                            <Br>   
                            <div class="clear"></div>
                            <div id="cloneachivement" class="<?php if(count($getIfAlreadyAchivement) <= 2){ echo "d-none"; } ?>">
                                <div class="form-group col-lg-6">
                                    <label for="exampleInputEmail1">Title</label>
                                    <input type="text" name="titleachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->title_1 != NULL) ? $getIfAlreadyAchivement[2]->title_1 : '') : '')); ?>">  
                                </div>
                                <input type="hidden" name="id[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->id != NULL) ? $getIfAlreadyAchivement[2]->id : '') : '')); ?>">  
                                <input type="hidden" name="oldFile[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->photo_1 != NULL) ? $getIfAlreadyAchivement[2]->photo_1 : '') : '')); ?>">  
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Link - If any</label>
                                    <input type="text" name="linkachieve[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->link_1 != NULL) ? $getIfAlreadyAchivement[2]->link_1 : '') : '')); ?>">  
                                </div> 
                                <div class="form-group col-lg-3">
                                    <label for="exampleInputEmail1">Date</label>
                                    <input type="date" name="date[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->date_1 != NULL) ? date('Y-m-d',strtotime($getIfAlreadyAchivement[2]->date_1)) : '') : '')); ?>">
                                </div> 
                                <div class="form-group col-lg-12">
                                    <label for="exampleInputEmail1"> Description (200 words)</label>
                                    <textarea id="w3review" name="descachieve[]" rows="4" cols="50"><?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->des_1 != NULL) ? $getIfAlreadyAchivement[2]->des_1 : '') : '')); ?></textarea> 
                                </div>
                                <div class="clear"></div>
                                <br>
                                <a href="javascript:void(0)"  onclick="showAchivementUploader('3')" style="background: #637076;color: #fff;width:195px;margin:10px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">
                                    Browse Achievement  Photo
                                </a>
                                <input type="file" style="display:none;" name="fileachive[]" id="fileachive3" onchange="showFileLabel('3')">
                                <br>
                                <p id="fileachivelabel3" class="text-success" style="color:green;font-weight:bold;margin-left:30px;"></p>
                                <div style="clear:both; overflow:hidden"></div>
                                <Br>
                                <div class="u-close" style="float:left; margin-right:20px;">
                                    <img src="<?php echo ((count($getIfAlreadyAchivement) >= 3 ? (($getIfAlreadyAchivement[2]->photo_1 != NULL || $getIfAlreadyAchivement[2]->photo_1 != '') ? $getIfAlreadyAchivement[2]->photo_1 : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                    <?php 
                                        if(count($getIfAlreadyAchivement) >= 3)
                                        {
                                            if($getIfAlreadyAchivement[2]->photo_1 != NULL || $getIfAlreadyAchivement[2]->photo_1 != '')
                                            {
                                    ?>
                                    <a href="<?php echo URL::to('/delete-achivement-image/'.$getIfAlreadyAchivement[2]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                        <i class="cvicon-cv-cancel"></i>
                                    </a>
                                    <?php } } ?>
                                </div>
                                <br> Image size 350 x 350  <br>  <br>
                                <div style="clear:both; overflow:hidden"></div>
                                <Br>   
                                
                                
                                <Br>   
                            </div>
                            <div style="clear:both; overflow:hidden"></div>
                            <div class="row">
                                <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyAchivement) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                             </div>
                        </form>
                    </div>
                </div>
                  
                  
                  
                  <!--<div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">-->
                  <!--   <h2>My Awards upto 3</h2>-->
                  <!--   <div class="form-group col-lg-6">-->
                  <!--      <label for="exampleInputEmail1">Title</label>  -->
                  <!--      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">-->
                  <!--   </div>-->
                  <!--   <div class="form-group col-lg-3">-->
                  <!--      <label for="exampleInputEmail1">Add Link - If any</label>  -->
                  <!--      <input type="email" class="form-control" id="exampleInputEmail1" placeholder="eg: ">-->
                  <!--   </div>-->
                  <!--      <div class="form-group col-lg-3">-->
                  <!--      <label for="exampleInputEmail1">Date</label>  -->
                  <!--      <input type="date" class="form-control" id="exampleInputEmail1" placeholder="">-->
                  <!--   </div>-->
                  <!--   <div class="form-group col-lg-12">  -->
                  <!--      <label for="exampleInputEmail1">Description 200 words</label>-->
                  <!--      <textarea id="w3review" name="w3review" rows="4" cols="50" spellcheck="false"> -->
                  <!--      </textarea>-->
                  <!--   </div>-->
                  <!--   <div class="clear"></div>-->
                  <!--   <a href="" style="background: #637076;color: #fff;width:155px;margin:10px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">-->
                  <!--   Browse Awards Photo</a>   Image size 350 x 350-->
                  <!--   <div class="clear"></div>-->
                  <!--   <div class="row" style="margin-top:20px">-->
                  <!--      <div class="col-lg-2"><button type="submit" class="btn btn-cv1">Save</button></div>-->
                  <!--   </div>-->
                  <!--   <a href="" style="background: #e5e5e5;color: #000;width:130px;margin:20px 0 0 10px;display:block;padding: 6px 15px 2px 17px;font-size: 12px;">-->
                  <!--   Add More Projects</a>-->
                  <!--</div>-->
               </div>
            </div>
            <div id="portfolio" class="tabcontent" style="margin-top:10px">
                <div class="container">
                   <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                        <h2>Portfolio</h2>
                      <p>You can add 12 Photos</p>
                      <div class="col-lg-12">
                         <div class="u-form">
                             <?php 
                                $userId = session()->get('user_id');
                                $getIfAlreadyGallery = DB::select('select * from artist_gallery where user_id='.$userId);
                            ?>
                            <form action="{{URL::to('/save-gallery')}}" method="post" enctype="multipart/form-data" id="galleryform">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                <div class="row" id="portfolio1">
                                   <div class="col-lg-6">
                                      <div class="form-group col-lg-6" style="padding-left:0px">
                                         <label for="e1">Photo Title 1 - (3 words)</label>
                                         <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 1 ? (($getIfAlreadyGallery[0]->photo_title != NULL) ? $getIfAlreadyGallery[0]->photo_title : '') : '')); ?>">  
                                      </div>
                                      <div class="form-group col-lg-6"  style="padding:0px">
                                            <label for="e1">Select category</label>
                                            <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                            </select>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="showPhotoUploader('1')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                            <input type="file" class="d-none" id="photosectionphoto1" name="photosectionphoto[]">
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                            <img src="<?php echo ((count($getIfAlreadyGallery) >= 1 ? (($getIfAlreadyGallery[0]->photo != NULL || $getIfAlreadyGallery[0]->photo != '') ? $getIfAlreadyGallery[0]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyGallery) >= 1)
                                                {
                                                    if($getIfAlreadyGallery[0]->photo != NULL || $getIfAlreadyGallery[0]->photo != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[0]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                      </div>
                                 </div>
                                   <div class="col-lg-6">
                                      <div class="form-group col-lg-6" style="padding-left:0px">
                                         <label for="e1">Photo Title 2 - (3 words)</label>
                                         <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 2 ? (($getIfAlreadyGallery[1]->photo_title != NULL) ? $getIfAlreadyGallery[1]->photo_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6"  style="padding:0px">
                                         <label for="e1">Select category</label>
                                         <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery10]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery10]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 2) ? (($getIfAlreadyGallery[1]->category != NULL) ? (($getIfAlreadyGallery[1]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                            </select>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                            
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                                 <img src="<?php echo ((count($getIfAlreadyGallery) >= 2 ? (($getIfAlreadyGallery[1]->photo != NULL || $getIfAlreadyGallery[1]->photo != '') ? $getIfAlreadyGallery[1]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyGallery) >= 2)
                                                {
                                                    if($getIfAlreadyGallery[1]->photo != NULL || $getIfAlreadyGallery[1]->photo != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[1]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                      </div>
                                   </div>
                                   <div class="col-lg-6">
                                      <div class="form-group col-lg-6" style="padding-left:0px">
                                         <label for="e1">Photo Title 3 - (3 words)</label>
                                          <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[2]->photo_title != NULL) ? $getIfAlreadyGallery[2]->photo_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6"  style="padding:0px">
                                         <label for="e1">Select category</label>
                                         <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 3) ? (($getIfAlreadyGallery[2]->category != NULL) ? (($getIfAlreadyGallery[2]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                            </select>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                            <img src="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[2]->photo != NULL || $getIfAlreadyGallery[2]->photo != '') ? $getIfAlreadyGallery[2]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyGallery) >= 3)
                                                {
                                                    if($getIfAlreadyGallery[2]->photo != NULL || $getIfAlreadyGallery[2]->photo != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[2]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                      </div>
                                   </div>
                                   <div class="col-lg-6">
                                      <div class="form-group col-lg-6" style="padding-left:0px">
                                         <label for="e1">Photo Title 4 - (3 words)</label>
                                      <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 4 ? (($getIfAlreadyGallery[3]->photo_title != NULL) ? $getIfAlreadyGallery[3]->photo_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6"  style="padding:0px">
                                         <label for="e1">Select category</label>
                                          <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 4) ? (($getIfAlreadyGallery[3]->category != NULL) ? (($getIfAlreadyGallery[3]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                            </select>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                           <img src="<?php echo ((count($getIfAlreadyGallery) >= 3 ? (($getIfAlreadyGallery[3]->photo != NULL || $getIfAlreadyGallery[3]->photo != '') ? $getIfAlreadyGallery[3]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyGallery) >= 4)
                                                {
                                                    if($getIfAlreadyGallery[3]->photo != NULL || $getIfAlreadyGallery[3]->photo != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[3]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
    
    
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                      </div>
                                   </div>
                                </div>
                                <a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;" id="addPhotoTwoMore" onclick="addfourmore('2')">Add More 4 photos grid</a>
                                <a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;" class="d-none" id="removePhotoTwoMore" onclick="removefourmore('2')">Remove 4 photos grid</a>
                                <br>
                                <div id="portfolio2" style="margin-top:2rem;">
                                    <div id="portfolio3" class="d-none">
                                        <div class="col-lg-6">
                                          <div class="form-group col-lg-6" style="padding-left:0px">
                                             <label for="e1">Photo Title 5 - (3 words)</label>
                                             <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 5 ? (($getIfAlreadyGallery[4]->photo_title != NULL) ? $getIfAlreadyGallery[4]->photo_title : '') : '')); ?>">
                                          </div>
                                          <div class="form-group col-lg-6"  style="padding:0px">
                                                <label for="e1">Select category</label>
                                             <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 1) ? (($getIfAlreadyGallery[0]->category != NULL) ? (($getIfAlreadyGallery[0]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 5) ? (($getIfAlreadyGallery[4]->category != NULL) ? (($getIfAlreadyGallery[4]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                             <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                <div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                </div>
                                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                             </div>
                                             <div class="u-close" style="float:left; margin-right:20px;">
                                               <img src="<?php echo ((count($getIfAlreadyGallery) >= 5 ? (($getIfAlreadyGallery[4]->photo != NULL || $getIfAlreadyGallery[4]->photo != '') ? $getIfAlreadyGallery[4]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyGallery) >= 5)
                                                {
                                                    if($getIfAlreadyGallery[4]->photo != NULL || $getIfAlreadyGallery[4]->photo != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[4]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
    
                                             </div>
                                             <div style="clear:both; overflow:hidden"></div>
                                          </div>
                                     </div>
                                       <div class="col-lg-6">
                                          <div class="form-group col-lg-6" style="padding-left:0px">
                                             <label for="e1">Photo Title 6 - (3 words)</label>
                                             <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 6 ? (($getIfAlreadyGallery[5]->photo_title != NULL) ? $getIfAlreadyGallery[5]->photo_title : '') : '')); ?>">
                                          </div>
                                          <div class="form-group col-lg-6"  style="padding:0px">
                                             <label for="e1">Select category</label>
                                             <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 6) ? (($getIfAlreadyGallery[5]->category != NULL) ? (($getIfAlreadyGallery[5]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                             <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                <div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                </div>
                                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                                
                                             </div>
                                             <div class="u-close" style="float:left; margin-right:20px;">
                                                 <img src="<?php echo ((count($getIfAlreadyGallery) >= 6 ? (($getIfAlreadyGallery[5]->photo != NULL || $getIfAlreadyGallery[5]->photo != '') ? $getIfAlreadyGallery[5]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyGallery) >= 6)
                                                {
                                                    if($getIfAlreadyGallery[5]->photo != NULL || $getIfAlreadyGallery[5]->photo != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[5]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                             </div>
                                             <div style="clear:both; overflow:hidden"></div>
                                          </div>
                                       </div>
                                       <div class="col-lg-6">
                                          <div class="form-group col-lg-6" style="padding-left:0px">
                                             <label for="e1">Photo Title 7 - (3 words)</label>
                                             <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 7 ? (($getIfAlreadyGallery[6]->photo_title != NULL) ? $getIfAlreadyGallery[6]->photo_title : '') : '')); ?>">
                                          </div>
                                          <div class="form-group col-lg-6"  style="padding:0px">
                                             <label for="e1">Select category</label>
                                            <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 7) ? (($getIfAlreadyGallery[6]->category != NULL) ? (($getIfAlreadyGallery[6]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                             <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                <div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                </div>
                                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                             </div>
                                             <div class="u-close" style="float:left; margin-right:20px;">
                                                <img src="<?php echo ((count($getIfAlreadyGallery) >= 7 ? (($getIfAlreadyGallery[6]->photo != NULL || $getIfAlreadyGallery[6]->photo != '') ? $getIfAlreadyGallery[6]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyGallery) >= 7)
                                                {
                                                    if($getIfAlreadyGallery[6]->photo != NULL || $getIfAlreadyGallery[6]->photo != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[6]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                             </div>
                                             <div style="clear:both; overflow:hidden"></div>
                                          </div>
                                       </div>
                                       <div class="col-lg-6">
                                          <div class="form-group col-lg-6" style="padding-left:0px">
                                             <label for="e1">Photo Title 8 - (3 words)</label>
                                             <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 8 ? (($getIfAlreadyGallery[7]->photo_title != NULL) ? $getIfAlreadyGallery[7]->photo_title : '') : '')); ?>">
                                          </div>
                                          <div class="form-group col-lg-6"  style="padding:0px">
                                             <label for="e1">Select category</label>
                                             <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >=8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 8) ? (($getIfAlreadyGallery[7]->category != NULL) ? (($getIfAlreadyGallery[7]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                             <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                                <div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                                </div>
                                                <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                             </div>
                                             <div class="u-close" style="float:left; margin-right:20px;">
                                                <img src="<?php echo ((count($getIfAlreadyGallery) >= 8 ? (($getIfAlreadyGallery[7]->photo != NULL || $getIfAlreadyGallery[7]->photo != '') ? $getIfAlreadyGallery[7]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyGallery) >= 8)
                                                {
                                                    if($getIfAlreadyGallery[7]->photo != NULL || $getIfAlreadyGallery[7]->photo != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[7]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                             </div>
                                             <div style="clear:both; overflow:hidden"></div>
                                          </div>
                                       </div>
                                    </div>
                                    <br>
                                    <a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;" class="d-none" id="addPhotoThreeMore" onclick="addfourmore('3')">Add More 4 photos grid</a>
                                    <a href="javascript:void(0)" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;" class="d-none" id="removePhotoThreeMore" onclick="removefourmore('3')">Remove 4 photos grid</a>
                                    <div id="portfolio4" class="d-none" style="margin-top:2rem;">
                                        <div class="col-lg-6">
                                      <div class="form-group col-lg-6" style="padding-left:0px">
                                         <label for="e1">Photo Title 9 - (3 words)</label>
                                         <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 9 ? (($getIfAlreadyGallery[8]->photo_title != NULL) ? $getIfAlreadyGallery[8]->photo_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6"  style="padding:0px">
                                            <label for="e1">Select category</label>
                                            <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 9) ? (($getIfAlreadyGallery[8]->category != NULL) ? (($getIfAlreadyGallery[8]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                            </select>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                             <img src="<?php echo ((count($getIfAlreadyGallery) >= 9 ? (($getIfAlreadyGallery[8]->photo != NULL || $getIfAlreadyGallery[8]->photo != '') ? $getIfAlreadyGallery[8]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyGallery) >= 9)
                                                {
                                                    if($getIfAlreadyGallery[8]->photo != NULL || $getIfAlreadyGallery[8]->photo != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[8]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                      </div>
                                 </div>
                                        <div class="col-lg-6">
                                      <div class="form-group col-lg-6" style="padding-left:0px">
                                         <label for="e1">Photo Title 10 - (3 words)</label>
                                         <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 10 ? (($getIfAlreadyGallery[9]->photo_title != NULL) ? $getIfAlreadyGallery[9]->photo_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6"  style="padding:0px">
                                         <label for="e1">Select category</label>
                                          <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 10) ? (($getIfAlreadyGallery[9]->category != NULL) ? (($getIfAlreadyGallery[9]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                            </select>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                            
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                            <img src="<?php echo ((count($getIfAlreadyGallery) >= 10 ? (($getIfAlreadyGallery[9]->photo != NULL || $getIfAlreadyGallery[9]->photo != '') ? $getIfAlreadyGallery[9]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyGallery) >= 10)
                                                {
                                                    if($getIfAlreadyGallery[9]->photo != NULL || $getIfAlreadyGallery[9]->photo != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[9]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                      </div>
                                   </div>
                                        <div class="col-lg-6">
                                      <div class="form-group col-lg-6" style="padding-left:0px">
                                         <label for="e1">Photo Title 11 - (3 words)</label>
                                         <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 11 ? (($getIfAlreadyGallery[10]->photo_title != NULL) ? $getIfAlreadyGallery[10]->photo_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6"  style="padding:0px">
                                         <label for="e1">Select category</label>
                                         <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery1[10]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 11) ? (($getIfAlreadyGallery[10]->category != NULL) ? (($getIfAlreadyGallery[10]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                            </select>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                            <img src="<?php echo ((count($getIfAlreadyGallery) >= 11 ? (($getIfAlreadyGallery[10]->photo != NULL || $getIfAlreadyGallery[10]->photo != '') ? $getIfAlreadyGallery[10]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyGallery) >= 11)
                                                {
                                                    if($getIfAlreadyGallery[10]->photo != NULL || $getIfAlreadyGallery[10]->photo != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[10]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                      </div>
                                   </div>
                                        <div class="col-lg-6">
                                      <div class="form-group col-lg-6" style="padding-left:0px">
                                         <label for="e1">Photo Title 12 - (3 words)</label>
                                         <input type="text" name="photo_title[]" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyGallery) >= 12 ? (($getIfAlreadyGallery[11]->photo_title != NULL) ? $getIfAlreadyGallery[111]->photo_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6"  style="padding:0px">
                                         <label for="e1">Select category</label>
                                        <select class="input-select" style="width:100%" name="photo_category[]" >
                                                <option value="" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "") ? 'selected' : '') : '') : ''); ?>>Select</option>
                                                <option value="Co-Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Co-Director") ? 'selected' : '') : '') : ''); ?>>Co-Director</option>
                                                <option value="Film Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Film Director") ? 'selected' : '') : '') : ''); ?>>Film Director</option>
                                                <option value="Short Film Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Short Film Director") ? 'selected' : '') : '') : ''); ?>>Short Film Director</option>
                                                <option value="Tv Serial Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Tv Serial Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Director</option>
                                                <option value="Tv Serial Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Tv Serial Assistant Director") ? 'selected' : '') : '') : ''); ?>>Tv Serial Assistant Director</option>
                                                <option value="Art Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Art Director") ? 'selected' : '') : '') : ''); ?>>Art Director</option>
                                                <option value="Music Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Music Director") ? 'selected' : '') : '') : ''); ?>>Music Director</option>
                                                <option value="Unit Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Unit Director") ? 'selected' : '') : '') : ''); ?>>Unit Director</option>
                                                <option value="Assistant Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Assistant Director") ? 'selected' : '') : '') : ''); ?>>Assistant Director</option>
                                                <option value="Stunt Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Stunt Director") ? 'selected' : '') : '') : ''); ?>>Stunt Director</option>
                                                <option value="Film production House" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Film production House") ? 'selected' : '') : '') : ''); ?>>Flim production House</option>
                                                <option value="Film Producer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Film Producer") ? 'selected' : '') : '') : ''); ?>>Film Producer</option>
                                                <option value="Tv Serial Production House" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Tv Serial Production House") ? 'selected' : '') : '') : ''); ?>>Tv Serial Production House</option>
                                                <option value="Tv Serial Poroducer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Tv Serial Poroducer") ? 'selected' : '') : '') : ''); ?>>Tv Serial Poroducer</option>
                                                <option value="Casting Director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Casting Director") ? 'selected' : '') : '') : ''); ?>>Casting Director</option>
                                                <option value="Casting Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Casting Co-ordinator") ? 'selected' : '') : '') : ''); ?>>Casting Co-ordinator</option>
                                                <option value="Casting Agent" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Casting Agent") ? 'selected' : '') : '') : ''); ?>> Casting Agent</option>
                                                <option value="Executive Producer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Executive Producer") ? 'selected' : '') : '') : ''); ?>> Executive Producer</option>
                                                <option value="Production Manager" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Production Manager") ? 'selected' : '') : '') : ''); ?>> Production Manager</option>
                                                <option value="Associate producer" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Associate producer") ? 'selected' : '') : '') : ''); ?>> Associate producer</option>
                                                <option value="Model Co-ordinator" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Model Co-ordinator") ? 'selected' : '') : '') : ''); ?>> Model Co-ordinator</option>
                                                <option value="Adv Film Maker" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Adv Film Maker") ? 'selected' : '') : '') : ''); ?>> Adv Film Maker</option>
                                                <option value="Visual effects creative director" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Visual effects creative director") ? 'selected' : '') : '') : ''); ?>>Visual effects creative director</option>
                                                <option value="Others" <?php echo ((count($getIfAlreadyGallery) >= 12) ? (($getIfAlreadyGallery[11]->category != NULL) ? (($getIfAlreadyGallery[11]->category == "Others") ? 'selected' : '') : '') : ''); ?>> Others</option>
                                            </select>1
                                      </div>
                                1      <div class="form-group">
                                         <div 1class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                            <img src="<?php echo ((count($getIfAlreadyGallery) >= 12 ? (($getIfAlreadyGallery[11]->photo != NULL || $getIfAlreadyGallery[11]->photo != '') ? $getIfAlreadyGallery[11]->photo : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyGallery) >= 12)
                                                {
                                                    if($getIfAlreadyGallery[11]->photo != NULL || $getIfAlreadyGallery[11]->photo != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-photo-image/'.$getIfAlreadyGallery[11]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                      </div>
                                   </div>
                                    </div>
                                </div>
                         </div>
                         <div class="btn-save" style="margin-top:2rem;">
                                <button class="btn btn-primary u-btn">Save</button>
                            </form>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
            <div id="video" class="tabcontent" >
                <div class="container">
                   <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                      <h2>Video</h2>
                      <div class="col-lg-12">
                         <form action="{{URL::to('/save-production-video')}}" method="post" enctype="multipart/form-data" id="videoform">
                             
                            <?php 
                                $userId = session()->get('user_id');
                                $getIfAlreadyVideo = DB::select('select * from production_video where user_id='.$userId);
                            ?>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->id != NULL) ? $getIfAlreadyVideo[0]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->video != NULL) ? $getIfAlreadyVideo[0]->video : '') : '')); ?>" />
                            <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->thumb != NULL) ? $getIfAlreadyVideo[0]->thumb : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->id != NULL) ? $getIfAlreadyVideo[1]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->video != NULL) ? $getIfAlreadyVideo[1]->video : '') : '')); ?>" />
                            <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->thumb != NULL) ? $getIfAlreadyVideo[1]->thumb : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->id != NULL) ? $getIfAlreadyVideo[2]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->video != NULL) ? $getIfAlreadyVideo[2]->video : '') : '')); ?>" />
                            <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->thumb != NULL) ? $getIfAlreadyVideo[2]->thumb : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->id != NULL) ? $getIfAlreadyVideo[3]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->video != NULL) ? $getIfAlreadyVideo[3]->video : '') : '')); ?>" />
                            <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->thumb != NULL) ? $getIfAlreadyVideo[3]->thumb : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->id != NULL) ? $getIfAlreadyVideo[4]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->video != NULL) ? $getIfAlreadyVideo[4]->video : '') : '')); ?>" />
                            <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->thumb != NULL) ? $getIfAlreadyVideo[4]->thumb : '') : '')); ?>" />
                            
                            <input type="hidden" name="oldVidid[]" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->id != NULL) ? $getIfAlreadyVideo[5]->id : '') : '')); ?>" />
                            <input type="hidden" name="oldVidFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->video != NULL) ? $getIfAlreadyVideo[5]->video : '') : '')); ?>" />
                            <input type="hidden" name="oldThumbFile[]" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->thumb != NULL) ? $getIfAlreadyVideo[5]->thumb : '') : '')); ?>" />
                            
                            <div class="u-form">
                                <div class="row">
                                   <div class="col-lg-6">
                                      <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                         <label for="e1">Video Title - Position 1</label>
                                         <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->video_title != NULL) ? $getIfAlreadyVideo[0]->video_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6" style="padding:0px;">
                                         <label for="e1">Select category</label>
                                         <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                             <option value="" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                             <option value="theater" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "theater") ? 'selected' : '') : '') : ''); ?>><strong>theater</strong></option>
                                             <option value="voiceover" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "voiceover") ? 'selected' : '') : '') : ''); ?>>voiceover</option>
                                             <option value="comedian" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "comedian") ? 'selected' : '') : '') : ''); ?>>comedian</option>
                                             <option value="photography" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "photography") ? 'selected' : '') : '') : ''); ?>>Photography</option>
                                             <option value="video grapher" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "video grapher") ? 'selected' : '') : '') : ''); ?>>Video Grapher</option>
                                             <option value="makeup artrist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "makeup artrist") ? 'selected' : '') : '') : ''); ?>>MakeUp Artrist</option>
                                             <option value="actor" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "actor") ? 'selected' : '') : '') : ''); ?>>Actor</option>
                                             <option value="writer" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "writer") ? 'selected' : '') : '') : ''); ?>>Writer</option>
                                             <option value="anchor" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "anchor") ? 'selected' : '') : '') : ''); ?>>Anchor / Presenter</option>
                                             <option value="off screen" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "off screen") ? 'selected' : '') : '') : ''); ?>>Off Screen</option>
                                             <option value="professional artist" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "professional artist") ? 'selected' : '') : '') : ''); ?>>Proffessional Artrist</option>
                                             <option value="modeling" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "modeling") ? 'selected' : '') : '') : ''); ?>>Modeling</option>
                                             <option value="singer /Musician" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "singer /Musician") ? 'selected' : '') : '') : ''); ?>>singer /Musician</option>
                                             <option value="editor" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "editor") ? 'selected' : '') : '') : ''); ?>>Editor</option>
                                             <option value="other" <?php echo ((count($getIfAlreadyVideo) >= 1) ? (($getIfAlreadyVideo[0]->category != NULL) ? (($getIfAlreadyVideo[0]->category == "other") ? 'selected' : '') : '') : ''); ?>>others</option>
                                        </select>
                                      </div>
                                      <div class="form-group">
                                         <label for="e1">Video Description </label>
                                         <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->description != NULL) ? $getIfAlreadyVideo[0]->description : '') : '')); ?></textarea>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="uploadVideo('1')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                            <input type="file" class="d-none" id="videoFile1" name="videoFile[]">
                                         </div>
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="uploadVideoThumbnail('1')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                            <input type="file" class="d-none" id="videoThumb1" name="videoThumbFile[]">
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                            <img src="<?php echo ((count($getIfAlreadyVideo) >= 1 ? (($getIfAlreadyVideo[0]->thumb != NULL || $getIfAlreadyVideo[0]->thumb != '') ? $getIfAlreadyVideo[0]->thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyVideo) >= 1)
                                                {
                                                    if($getIfAlreadyVideo[0]->thumb != NULL || $getIfAlreadyVideo[0]->thumb != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-production-video-image/'.$getIfAlreadyVideo[0]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                         <div class="u-progress">
                                            <div class="progress">
                                               <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width: 35%;">
                                                  <span class="sr-only">35% Complete</span>
                                               </div>
                                            </div>
                                            <div class="u-close cgt">
                                               <a href="#"><i class="cvicon-cv-cancel"></i></a>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="col-lg-6">
                                      <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                         <label for="e1">Video Title - Position 2</label>
                                         <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->video_title != NULL) ? $getIfAlreadyVideo[1]->video_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6" style="padding:0px;">
                                         <label for="e1">Select category</label>
                                         <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                             <option value="" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                             <option value="theater" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "theater") ? 'selected' : '') : '') : ''); ?>><strong>theater</strong></option>
                                             <option value="voiceover" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "voiceover") ? 'selected' : '') : '') : ''); ?>>voiceover</option>
                                             <option value="comedian" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "comedian") ? 'selected' : '') : '') : ''); ?>>comedian</option>
                                             <option value="photography" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "photography") ? 'selected' : '') : '') : ''); ?>>Photography</option>
                                             <option value="video grapher" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "video grapher") ? 'selected' : '') : '') : ''); ?>>Video Grapher</option>
                                             <option value="makeup artrist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "makeup artrist") ? 'selected' : '') : '') : ''); ?>>MakeUp Artrist</option>
                                             <option value="actor" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "actor") ? 'selected' : '') : '') : ''); ?>>Actor</option>
                                             <option value="writer" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "writer") ? 'selected' : '') : '') : ''); ?>>Writer</option>
                                             <option value="anchor" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "anchor") ? 'selected' : '') : '') : ''); ?>>Anchor / Presenter</option>
                                             <option value="off screen" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "off screen") ? 'selected' : '') : '') : ''); ?>>Off Screen</option>
                                             <option value="professional artist" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "professional artist") ? 'selected' : '') : '') : ''); ?>>Proffessional Artrist</option>
                                             <option value="modeling" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "modeling") ? 'selected' : '') : '') : ''); ?>>Modeling</option>
                                             <option value="singer /Musician" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "singer /Musician") ? 'selected' : '') : '') : ''); ?>>singer /Musician</option>
                                             <option value="editor" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "editor") ? 'selected' : '') : '') : ''); ?>>Editor</option>
                                             <option value="other" <?php echo ((count($getIfAlreadyVideo) >= 2) ? (($getIfAlreadyVideo[1]->category != NULL) ? (($getIfAlreadyVideo[1]->category == "other") ? 'selected' : '') : '') : ''); ?>>others</option>
                                        </select>
                                      </div>
                                      <div class="form-group">
                                         <label for="e1">Video Description </label>
                                         <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->description != NULL) ? $getIfAlreadyVideo[1]->description : '') : '')); ?></textarea>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="uploadVideo('2')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                            <input type="file" class="d-none" id="videoFile2" name="videoFile[]">
                                         </div>
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="uploadVideoThumbnail('2')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                            <input type="file" class="d-none" id="videoThumb2" name="videoThumbFile[]">
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                            <img src="<?php echo ((count($getIfAlreadyVideo) >= 2 ? (($getIfAlreadyVideo[1]->thumb != NULL || $getIfAlreadyVideo[1]->thumb != '') ? $getIfAlreadyVideo[1]->thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyVideo) >= 2)
                                                {
                                                    if($getIfAlreadyVideo[1]->thumb != NULL || $getIfAlreadyVideo[1]->thumb != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-production-video-image/'.$getIfAlreadyVideo[1]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                         <div class="u-progress">
                                            <div class="progress">
                                               <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width: 35%;">
                                                  <span class="sr-only">35% Complete</span>
                                               </div>
                                            </div>
                                            <div class="u-close cgt">
                                               <a href="#"><i class="cvicon-cv-cancel"></i></a>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="col-lg-6">
                                      <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                         <label for="e1">Video Title - Position 3</label>
                                         <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->video_title != NULL) ? $getIfAlreadyVideo[2]->video_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6" style="padding:0px;">
                                         <label for="e1">Select category</label>
                                         <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                             <option value="" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                             <option value="theater" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "theater") ? 'selected' : '') : '') : ''); ?>><strong>theater</strong></option>
                                             <option value="voiceover" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "voiceover") ? 'selected' : '') : '') : ''); ?>>voiceover</option>
                                             <option value="comedian" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "comedian") ? 'selected' : '') : '') : ''); ?>>comedian</option>
                                             <option value="photography" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "photography") ? 'selected' : '') : '') : ''); ?>>Photography</option>
                                             <option value="video grapher" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "video grapher") ? 'selected' : '') : '') : ''); ?>>Video Grapher</option>
                                             <option value="makeup artrist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "makeup artrist") ? 'selected' : '') : '') : ''); ?>>MakeUp Artrist</option>
                                             <option value="actor" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "actor") ? 'selected' : '') : '') : ''); ?>>Actor</option>
                                             <option value="writer" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "writer") ? 'selected' : '') : '') : ''); ?>>Writer</option>
                                             <option value="anchor" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "anchor") ? 'selected' : '') : '') : ''); ?>>Anchor / Presenter</option>
                                             <option value="off screen" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "off screen") ? 'selected' : '') : '') : ''); ?>>Off Screen</option>
                                             <option value="professional artist" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "professional artist") ? 'selected' : '') : '') : ''); ?>>Proffessional Artrist</option>
                                             <option value="modeling" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "modeling") ? 'selected' : '') : '') : ''); ?>>Modeling</option>
                                             <option value="singer /Musician" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "singer /Musician") ? 'selected' : '') : '') : ''); ?>>singer /Musician</option>
                                             <option value="editor" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "editor") ? 'selected' : '') : '') : ''); ?>>Editor</option>
                                             <option value="other" <?php echo ((count($getIfAlreadyVideo) >= 3) ? (($getIfAlreadyVideo[2]->category != NULL) ? (($getIfAlreadyVideo[2]->category == "other") ? 'selected' : '') : '') : ''); ?>>others</option>
                                        </select>
                                      </div>
                                      <div class="form-group">
                                         <label for="e1">Video Description </label>
                                         <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->description != NULL) ? $getIfAlreadyVideo[2]->description : '') : '')); ?></textarea>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="uploadVideo('3')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                            <input type="file" class="d-none" id="videoFile3" name="videoFile[]">
                                         </div>
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="uploadVideoThumbnail('3')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                            <input type="file" class="d-none" id="videoThumb3" name="videoThumbFile[]">
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                            <img src="<?php echo ((count($getIfAlreadyVideo) >= 3 ? (($getIfAlreadyVideo[2]->thumb != NULL || $getIfAlreadyVideo[2]->thumb != '') ? $getIfAlreadyVideo[2]->thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyVideo) >= 3)
                                                {
                                                    if($getIfAlreadyVideo[2]->thumb != NULL || $getIfAlreadyVideo[2]->thumb != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-production-video-image/'.$getIfAlreadyVideo[2]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                         <div class="u-progress">
                                            <div class="progress">
                                               <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width: 35%;">
                                                  <span class="sr-only">35% Complete</span>
                                               </div>
                                            </div>
                                            <div class="u-close cgt">
                                               <a href="#"><i class="cvicon-cv-cancel"></i></a>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <a href="javascript:void(0)" id="videoAdd" onclick="addMoreVideo()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Add More 3 Video grid</a>
                                <a href="javascript:void(0)" id="videoRemove" class="d-none" onclick="removeMoreVideo()" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Remove Video grid</a>
                                <div id="clonevideodiv" class="d-none" style="margin-top:2rem;">
                                   <div class="col-lg-6">
                                      <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                         <label for="e1">Video Title - Position 4</label>
                                         <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->video_title != NULL) ? $getIfAlreadyVideo[3]->video_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6" style="padding:0px;">
                                         <label for="e1">Select category</label>
                                         <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                             <option value="" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                             <option value="theater" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "theater") ? 'selected' : '') : '') : ''); ?>><strong>theater</strong></option>
                                             <option value="voiceover" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "voiceover") ? 'selected' : '') : '') : ''); ?>>voiceover</option>
                                             <option value="comedian" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "comedian") ? 'selected' : '') : '') : ''); ?>>comedian</option>
                                             <option value="photography" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "photography") ? 'selected' : '') : '') : ''); ?>>Photography</option>
                                             <option value="video grapher" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "video grapher") ? 'selected' : '') : '') : ''); ?>>Video Grapher</option>
                                             <option value="makeup artrist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "makeup artrist") ? 'selected' : '') : '') : ''); ?>>MakeUp Artrist</option>
                                             <option value="actor" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "actor") ? 'selected' : '') : '') : ''); ?>>Actor</option>
                                             <option value="writer" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "writer") ? 'selected' : '') : '') : ''); ?>>Writer</option>
                                             <option value="anchor" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "anchor") ? 'selected' : '') : '') : ''); ?>>Anchor / Presenter</option>
                                             <option value="off screen" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "off screen") ? 'selected' : '') : '') : ''); ?>>Off Screen</option>
                                             <option value="professional artist" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "professional artist") ? 'selected' : '') : '') : ''); ?>>Proffessional Artrist</option>
                                             <option value="modeling" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "modeling") ? 'selected' : '') : '') : ''); ?>>Modeling</option>
                                             <option value="singer /Musician" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "singer /Musician") ? 'selected' : '') : '') : ''); ?>>singer /Musician</option>
                                             <option value="editor" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "editor") ? 'selected' : '') : '') : ''); ?>>Editor</option>
                                             <option value="other" <?php echo ((count($getIfAlreadyVideo) >= 4) ? (($getIfAlreadyVideo[3]->category != NULL) ? (($getIfAlreadyVideo[3]->category == "other") ? 'selected' : '') : '') : ''); ?>>others</option>
                                        </select>
                                      </div>
                                      <div class="form-group">
                                         <label for="e1">Video Description </label>
                                         <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->description != NULL) ? $getIfAlreadyVideo[3]->description : '') : '')); ?></textarea>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="uploadVideo('4')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                            <input type="file" class="d-none" id="videoFile4" name="videoFile[]">
                                         </div>
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="uploadVideoThumbnail('4')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                            <input type="file" class="d-none" id="videoThumb4" name="videoThumbFile[]">
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                            <img src="<?php echo ((count($getIfAlreadyVideo) >= 4 ? (($getIfAlreadyVideo[3]->thumb != NULL || $getIfAlreadyVideo[3]->thumb != '') ? $getIfAlreadyVideo[3]->thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyVideo) >= 4)
                                                {
                                                    if($getIfAlreadyVideo[3]->thumb != NULL || $getIfAlreadyVideo[3]->thumb != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-production-video-image/'.$getIfAlreadyVideo[3]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                         <div class="u-progress">
                                            <div class="progress">
                                               <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width: 35%;">
                                                  <span class="sr-only">35% Complete</span>
                                               </div>
                                            </div>
                                            <div class="u-close cgt">
                                               <a href="#"><i class="cvicon-cv-cancel"></i></a>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="col-lg-6">
                                      <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                         <label for="e1">Video Title - Position 5</label>
                                         <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->video_title != NULL) ? $getIfAlreadyVideo[4]->video_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6" style="padding:0px;">
                                         <label for="e1">Select category</label>
                                         <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                             <option value="" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                             <option value="theater" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "theater") ? 'selected' : '') : '') : ''); ?>><strong>theater</strong></option>
                                             <option value="voiceover" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "voiceover") ? 'selected' : '') : '') : ''); ?>>voiceover</option>
                                             <option value="comedian" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "comedian") ? 'selected' : '') : '') : ''); ?>>comedian</option>
                                             <option value="photography" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "photography") ? 'selected' : '') : '') : ''); ?>>Photography</option>
                                             <option value="video grapher" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "video grapher") ? 'selected' : '') : '') : ''); ?>>Video Grapher</option>
                                             <option value="makeup artrist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "makeup artrist") ? 'selected' : '') : '') : ''); ?>>MakeUp Artrist</option>
                                             <option value="actor" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "actor") ? 'selected' : '') : '') : ''); ?>>Actor</option>
                                             <option value="writer" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "writer") ? 'selected' : '') : '') : ''); ?>>Writer</option>
                                             <option value="anchor" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "anchor") ? 'selected' : '') : '') : ''); ?>>Anchor / Presenter</option>
                                             <option value="off screen" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "off screen") ? 'selected' : '') : '') : ''); ?>>Off Screen</option>
                                             <option value="professional artist" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "professional artist") ? 'selected' : '') : '') : ''); ?>>Proffessional Artrist</option>
                                             <option value="modeling" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "modeling") ? 'selected' : '') : '') : ''); ?>>Modeling</option>
                                             <option value="singer /Musician" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "singer /Musician") ? 'selected' : '') : '') : ''); ?>>singer /Musician</option>
                                             <option value="editor" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "editor") ? 'selected' : '') : '') : ''); ?>>Editor</option>
                                             <option value="other" <?php echo ((count($getIfAlreadyVideo) >= 5) ? (($getIfAlreadyVideo[4]->category != NULL) ? (($getIfAlreadyVideo[4]->category == "other") ? 'selected' : '') : '') : ''); ?>>others</option>
                                        </select>
                                      </div>
                                      <div class="form-group">
                                         <label for="e1">Video Description </label>
                                         <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->description != NULL) ? $getIfAlreadyVideo[4]->description : '') : '')); ?></textarea>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="uploadVideo('5')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                            <input type="file" class="d-none" id="videoFile5" name="videoFile[]">
                                         </div>
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="uploadVideoThumbnail('5')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                            <input type="file" class="d-none" id="videoThumb5" name="videoThumbFile[]">
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                            <img src="<?php echo ((count($getIfAlreadyVideo) >= 5 ? (($getIfAlreadyVideo[4]->thumb != NULL || $getIfAlreadyVideo[4]->thumb != '') ? $getIfAlreadyVideo[4]->thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyVideo) >= 5)
                                                {
                                                    if($getIfAlreadyVideo[4]->thumb != NULL || $getIfAlreadyVideo[4]->thumb != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-production-video-image/'.$getIfAlreadyVideo[4]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                         <div class="u-progress">
                                            <div class="progress">
                                               <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width: 35%;">
                                                  <span class="sr-only">35% Complete</span>
                                               </div>
                                            </div>
                                            <div class="u-close cgt">
                                               <a href="#"><i class="cvicon-cv-cancel"></i></a>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                                   <div class="col-lg-6">
                                      <div class="form-group col-lg-6"  style="padding-left:0;"> 
                                         <label for="e1">Video Title - Position 6</label>
                                         <input type="text" name="videotitle[]" class="form-control" id="e1" placeholder="" value="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->video_title != NULL) ? $getIfAlreadyVideo[5]->video_title : '') : '')); ?>">
                                      </div>
                                      <div class="form-group col-lg-6" style="padding:0px;">
                                         <label for="e1">Select category</label>
                                         <select class="input-select" style="width:100%" name="categoryvideo[]" >
                                             <option value="" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "") ? 'selected' : '') : '') : ''); ?>>Select Category</option>
                                             <option value="theater" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "theater") ? 'selected' : '') : '') : ''); ?>><strong>theater</strong></option>
                                             <option value="voiceover" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "voiceover") ? 'selected' : '') : '') : ''); ?>>voiceover</option>
                                             <option value="comedian" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "comedian") ? 'selected' : '') : '') : ''); ?>>comedian</option>
                                             <option value="photography" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "photography") ? 'selected' : '') : '') : ''); ?>>Photography</option>
                                             <option value="video grapher" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "video grapher") ? 'selected' : '') : '') : ''); ?>>Video Grapher</option>
                                             <option value="makeup artrist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "makeup artrist") ? 'selected' : '') : '') : ''); ?>>MakeUp Artrist</option>
                                             <option value="actor" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "actor") ? 'selected' : '') : '') : ''); ?>>Actor</option>
                                             <option value="writer" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "writer") ? 'selected' : '') : '') : ''); ?>>Writer</option>
                                             <option value="anchor" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "anchor") ? 'selected' : '') : '') : ''); ?>>Anchor / Presenter</option>
                                             <option value="off screen" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "off screen") ? 'selected' : '') : '') : ''); ?>>Off Screen</option>
                                             <option value="professional artist" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "professional artist") ? 'selected' : '') : '') : ''); ?>>Proffessional Artrist</option>
                                             <option value="modeling" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "modeling") ? 'selected' : '') : '') : ''); ?>>Modeling</option>
                                             <option value="singer /Musician" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "singer /Musician") ? 'selected' : '') : '') : ''); ?>>singer /Musician</option>
                                             <option value="editor" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "editor") ? 'selected' : '') : '') : ''); ?>>Editor</option>
                                             <option value="other" <?php echo ((count($getIfAlreadyVideo) >= 6) ? (($getIfAlreadyVideo[5]->category != NULL) ? (($getIfAlreadyVideo[5]->category == "other") ? 'selected' : '') : '') : ''); ?>>others</option>
                                        </select>
                                      </div>
                                      <div class="form-group">
                                         <label for="e1">Video Description </label>
                                         <textarea id="w3review" name="videodesc[]" rows="4" cols="50" spellcheck="false"><?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->description != NULL) ? $getIfAlreadyVideo[5]->description : '') : '')); ?></textarea>
                                      </div>
                                      <div class="form-group">
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="uploadVideo('6')"><i class="cv cvicon-cv-upload-video" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload Video</a>
                                            <input type="file" class="d-none" id="videoFile6" name="videoFile[]">
                                         </div>
                                         <div class="u-close" id="select-p" style="float:left; margin-right:20px;">
                                            <div class="browse-photo" onclick="uploadVideoThumbnail('6')"><i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </div>
                                            <a href="javascript:void(0)" style="text-align:Center;display:block;margin-top:9px;">Upload  Thumbnail</a>
                                            <input type="file" class="d-none" id="videoThumb6" name="videoThumbFile[]">
                                         </div>
                                         <div class="u-close" style="float:left; margin-right:20px;">
                                            <img src="<?php echo ((count($getIfAlreadyVideo) >= 6 ? (($getIfAlreadyVideo[5]->thumb != NULL || $getIfAlreadyVideo[5]->thumb != '') ? $getIfAlreadyVideo[5]->thumb : 'https://entertainmentbugs.com/dev_html/images/sv-5.png') : 'https://entertainmentbugs.com/dev_html/images/sv-5.png')); ?>" style="width:100px;height:100px;object-fit:cover;margin-right:2px;">
                                            <?php 
                                                if(count($getIfAlreadyVideo) >= 6)
                                                {
                                                    if($getIfAlreadyVideo[5]->thumb != NULL || $getIfAlreadyVideo[5]->thumb != '')
                                                    {
                                            ?>
                                            <a href="<?php echo URL::to('/delete-production-video-image/'.$getIfAlreadyVideo[5]->id) ?>" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
                                                <i class="cvicon-cv-cancel"></i>
                                            </a>
                                            <?php } } ?>
                                         </div>
                                         <div style="clear:both; overflow:hidden"></div>
                                         <div class="u-progress">
                                            <div class="progress">
                                               <div class="progress-bar" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100" style="width: 35%;">
                                                  <span class="sr-only">35% Complete</span>
                                               </div>
                                            </div>
                                            <div class="u-close cgt">
                                               <a href="#"><i class="cvicon-cv-cancel"></i></a>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                               </div>
                            </div>
                            <br>
                            <div class="row col-md-12 btn-save">
                                <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyVideo) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                            </div>
                         </form>
                      </div>
                   </div>
                </div>
            </div>
            <div id="team" class="tabcontent">
               <div class="container">
                  <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                     <h2>Add Team member (Add 12 member)</h2>
                     <div class="form-group col-lg-6" style="position:relative">
                        <label for="exampleInputEmail1">Position</label>  
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder=""> 
                        <div class="u-progress">
                           <div class="u-close cgt">
                              <a href="#"><i class="cvicon-cv-cancel"></i></a>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-lg-6">
                        <label for="exampleInputEmail1">Search By Name</label>  
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                        <p style="margin-top:4px"> Note: Your team member should be in your following list. <a href="" style="color:red">Follow them first</a></p>
                     </div>
                     <div class="clear"></div>
                     <div class="form-group col-lg-6" style="position:relative">
                        <label for="exampleInputEmail1">Position</label>  
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder=""> 
                        <div class="u-progress">
                           <div class="u-close cgt">
                              <a href="#"><i class="cvicon-cv-cancel"></i></a>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-lg-6">
                        <label for="exampleInputEmail1">Search By Name</label>  
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                     </div>
                     <div class="clear"></div>
                     <div class="form-group col-lg-6" style="position:relative">
                        <label for="exampleInputEmail1">Position</label>  
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder=""> 
                        <div class="u-progress">
                           <div class="u-close cgt">
                              <a href="#"><i class="cvicon-cv-cancel"></i></a>
                           </div>
                        </div>
                     </div>
                     <div class="form-group col-lg-6">
                        <label for="exampleInputEmail1">Search By Name</label>  
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="">
                     </div>
                     <div class="clear"></div>
                     <div class="btn-save">
                        <form action="#" method="post">
                           <button class="btn btn-primary u-btn">Save</button>
                        </form>
                     </div>
                     <a href="" style="background: #000;color: #fff;margin:20px 0 0 0 ;display:block;width:140px; padding: 6px 15px 2px 17px;font-size: 12px;">
                     Add team Member</a>
                  </div>
               </div>
            </div>
            <div id="contact" class="tabcontent">
               <div class="container">
                  <div class="row" style="background:#fff;padding: 30px 15px;margin: 0;">
                     <form action="{{URL::to('save-production-contact')}}" id="contactForm" style="margin-top:0px" enctype="multipart/form-data" method="post">
                         <?php 
                            $userId = session()->get('user_id');
                            $getIfAlreadyContact = DB::select('select * from production_contact_us where user_id='.$userId);
                        ?>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                         <div class="form-group achievemets">
                        <h2>Contact Us</h2>
                        <div class="form-group col-lg-12">
                           <label for="exampleInputEmail1">Title eg: We'd love to hear from you</label>
                           <input type="text" name="titlecontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->title != NULL) ? $getIfAlreadyContact[0]->title : '') : '')); ?>">  
                        </div>
                        <div class="form-group col-lg-12">
                           <label for="exampleInputEmail1"> Description (50 words)</label>
                           <textarea id="w3review" name="descriptioncontact" rows="4" cols="50"><?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->description != NULL) ? $getIfAlreadyContact[0]->description : '') : '')); ?></textarea> 
                        </div>
                        <div style="clear:both; overflow:hidden"></div>
                        <div class="col-lg-3">
                            <div class="form-group"> 
                                <label for="exampleInputEmail1">Address</label>
                                <input type="text" name="addresscontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->address != NULL) ? $getIfAlreadyContact[0]->address : '') : '')); ?>">    
                            </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="form-group"> 
                                <label for="exampleInputEmail1">Email id</label>  
                                <input type="email" name="emailcontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->email_id != NULL) ? $getIfAlreadyContact[0]->email_id : '') : '')); ?>">    
                            </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="form-group"> <label for="exampleInputEmail1">Landline number</label> 
                              <input type="text" name="phonecontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->landline_number != NULL) ? $getIfAlreadyContact[0]->landline_number : '') : '')); ?>">  
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="form-group"> <label for="exampleInputEmail1">Number / Whatsapp No </label>  
                              <input type="text" name="numbercontact" class="form-control" id="exampleInputEmail1" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->number != NULL) ? $getIfAlreadyContact[0]->number : '') : '')); ?>">  
                           </div>
                           <input type="checkbox" name="tick_whatsapp_feature" value="<?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->whatsapp_tick != NULL) ? $getIfAlreadyContact[0]->whatsapp_tick : '') : '')); ?>" style="height: 16px;margin: -1px 5px 0 0;float: left;" <?php echo ((count($getIfAlreadyContact) > 0 ? (($getIfAlreadyContact[0]->whatsapp_tick != NULL && $getIfAlreadyContact[0]->whatsapp_tick != 'no') ? 'checked' : '') : '')); ?>> Check tick for whatsaps features
                        </div>
                        <div class="clear"></div>
                     </div>
                     <div class="col-lg-6"> 
                     </div>
                     <div style="clear:both; overflow:hidden"></div>
                     <div class="row">
                        <div class="col-lg-2"><button type="submit" class="btn btn-cv1"><?php if(count($getIfAlreadyContact) == 0){ echo "Save";}else{ echo "Update"; } ?></button></div>
                     </div>
                    </form> 
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div> 
</div>
</div>
</div>
</div> 
   
   
 <style>
    .d-none{
        display:none!important;
    }
</style>  
  
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> 
<script>
$(document).ready(function() {
    $('.select2').select2();
});
function getStates(value)
{
    $('#state').empty();
    $('#state').append('<option value="">Select State</option>');
    if(value != '')
    {
        $.ajax({
            url:'{{URL::to("/get-states")}}',
            data:{id:value,'_token':'<?php echo csrf_token(); ?>'},
            type:'post',
            success:function(res)
            {
                let parse= JSON.parse(res);
                if(parse.length > 0)
                {
                    for(let i=0;i<parse.length;i++)
                        $('#state').append('<option value="'+parse[i].name+'">'+parse[i].name+'</option>');
                }else{
                    $('#state').append('<option value="">No States Found</option>');    
                }
            }
        })
    }else{
        $('#state').append('<option value="">Select State</option>');    
    }
}

function getCities(value)
{
    $('#city').empty();
    $('#city').append('<option value="">Select City</option>');
    if(value != '')
    {
        $.ajax({
            url:'{{URL::to("/get-cities")}}',
            data:{id:value,'_token':'<?php echo csrf_token(); ?>'},
            type:'post',
            success:function(res)
            {
                let parse= JSON.parse(res);
                if(parse.length > 0)
                {
                    for(let i=0;i<parse.length;i++)
                        $('#city').append('<option value="'+parse[i].name+'">'+parse[i].name+'</option>');
                }else{
                   $('#city').append('<option value="">No Cities Found</option>');     
                }
            }
        })
    }else{
        $('#city').append('<option value="">Select City</option>');     
    }
}

$('#productionForm').on('submit',function(e)
{
    e.preventDefault();
    let arr = $(this).serializeArray();
    let count = 0;
    for(let i =0 ;i<arr.length;i++)
    {
        if(arr[i].name == 'phousecheck[]')
        {
            count++;
        }
    }
    if(count > 4)
    {
        alert('Please Select Upto 4 Multiple Options');
    }else if(count == 0)
    {
        alert('Please Select At Least One Option');
    }else{
        let data = new FormData(this);
        $.ajax({
            url:$(this).attr('action'),
            data:data,
            contentType: false,
            cache: false,
            processData:false,
            type:'post',
            success:function(res)
            {
                alert('Data Saved Successfully');
                location.reload();
            }
        });
    }
})

$('#achivementForm').on('submit',function(e)
{
    e.preventDefault();
    let data = new FormData(this);
    $.ajax({
        url:$(this).attr('action'),
        data:data,
        contentType: false,
        cache: false,
        processData:false,
        type:'post',
        success:function(res)
        {
            alert('Data Saved Successfully');
            location.reload();
        }
    });
})

$('#reelform').on('submit',function(e)
{
    e.preventDefault();
    let data = new FormData(this);
    $.ajax({
        url:$(this).attr('action'),
        data:data,
        contentType: false,
        cache: false,
        processData:false,
        type:'post',
        success:function(res)
        {
            alert('Data Saved Successfully');
            location.reload();
        }
    });
})

$('#videoform').on('submit',function(e)
{
    e.preventDefault();
    let data = new FormData(this);
    $.ajax({
        url:$(this).attr('action'),
        data:data,
        contentType: false,
        cache: false,
        processData:false,
        type:'post',
        success:function(res)
        {
            alert('Data Saved Successfully');
            location.reload();
        }
    });
})

$('#whatwedoform').on('submit',function(e)
{
    e.preventDefault();
    let data = new FormData(this);
    $.ajax({
        url:$(this).attr('action'),
        data:data,
        contentType: false,
        cache: false,
        processData:false,
        type:'post',
        success:function(res)
        {
            alert('Data Saved Successfully');
            location.reload();
        }
    });
})

$('#projectform').on('submit',function(e)
{
    e.preventDefault();
    let data = new FormData(this);
    $.ajax({
        url:$(this).attr('action'),
        data:data,
        contentType: false,
        cache: false,
        processData:false,
        type:'post',
        success:function(res)
        {
            alert('Data Saved Successfully');
            location.reload();
        }
    });
})

$('#contactForm').on('submit',function(e)
{
    e.preventDefault();
    let data = new FormData(this);
    $.ajax({
        url:$(this).attr('action'),
        data:data,
        contentType: false,
        cache: false,
        processData:false,
        type:'post',
        success:function(res)
        {
            alert('Data Saved Successfully');
            location.reload();
        }
    });
})

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

function showAchivementUploader(num)
{
    $('#fileachive'+num).click();
}

function showFileLabel(num)
{
    $('#fileachivelabel'+num).html('1 File Selected <i class="fa fa-times" onclick="removeAchivementFile('+num+')"></i>');
}

function removeAchivementFile(num)
{
    const file = document.querySelector('#fileachive'+num);
    file.value = '';
    $('#fileachivelabel'+num).html('');
}

function openReelVideo()
{
    $('#reelvideo').click();
}

function openReelThumb(){
    $('#reelthumb').click();
}

function addfourmore(num)
{
    if(num == 2)
    {
        $('#portfolio3').removeClass('d-none');
        $('#addPhotoThreeMore').removeClass('d-none');
        $('#removePhotoTwoMore').removeClass('d-none');
        $('#addPhotoTwoMore').addClass('d-none');
    }
    if(num == 3)
    {
        $('#portfolio4').removeClass('d-none');
        $('#addPhotoThreeMore').addClass('d-none');
        $('#removePhotoThreeMore').removeClass('d-none');
        $('#addPhotoTwoMore').addClass('d-none');
    }
}

function removefourmore(num)
{
    if(num == 2)
    {
        $('#portfolio3').addClass('d-none');
        $('#removePhotoTwoMore').addClass('d-none');
        $('#addPhotoTwoMore').removeClass('d-none');
        $('#addPhotoThreeMore').addClass('d-none');
    }
    if(num == 3)
    {
        $('#portfolio4').addClass('d-none');
        if($('#portfolio3').hasClass('d-none'))
        {
            $('#addPhotoThreeMore').addClass('d-none');
        }else{
            $('#addPhotoThreeMore').removeClass('d-none');
        }
        $('#removePhotoThreeMore').addClass('d-none');
    }
}

function showReelVideoLabel()
{
    $('#reelvideolabel').html('1 File Selected <i class="fa fa-times" onclick="removeVideoFile()"></i>');
}

function showReelThumbLabel()
{
    $('#reelvideothumblabel').html('1 File Selected <i class="fa fa-times" onclick="removeVideoThumbFile()"></i>');
}

function removeVideoFile()
{
    const file = document.querySelector('#reelvideolabel');
    file.value = '';
    $('#reelvideolabel').html('');
}

function removeVideoThumbFile()
{
    const file = document.querySelector('#reelvideothumblabel');
    file.value = '';
    $('#reelvideothumblabel').html('');
}

function removeAchivementFile(num)
{
    const file = document.querySelector('#fileachive'+num);
    file.value = '';
    $('#fileachivelabel'+num).html('');
}

function addMoreAchivement()
{
    $('#addMoreAchivement').addClass('d-none');
    $('#removeMoreAchivement').removeClass('d-none');
    $('#cloneachivement').removeClass('d-none');
}

function removeMoreAchivement()
{
    $('#addMoreAchivement').removeClass('d-none');
    $('#removeMoreAchivement').addClass('d-none');
    $('#cloneachivement').addClass('d-none');
}

function removeWhatWeDo()
{
    for(let i=5;i<=8;i++)
    {
        $('#title'+i).val('');
        $('#desc'+i).val('');
    }
    $('#addMore').removeClass('d-none');
    $('#removeMore').addClass('d-none');
    $('#clonediv').addClass('d-none');
}

function addProj()
{
    $('#addProj').addClass('d-none');
    $('#removeProj').removeClass('d-none');
    $('#cloneprojdiv').removeClass('d-none');    
}

function removeProj()
{
    $('#addProj').removeClass('d-none');
    $('#removeProj').addClass('d-none');
    $('#cloneprojdiv').addClass('d-none');
}

function removeMoreVideo()
{
    $('#videoAdd').removeClass('d-none');
    $('#videoRemove').addClass('d-none');
    $('#clonevideodiv').addClass('d-none');
}

function addMoreVideo()
{
    $('#videoAdd').addClass('d-none');
    $('#videoRemove').removeClass('d-none');
    $('#clonevideodiv').removeClass('d-none');    
}

function addMoreWhatWeDo()
{
    $('#addMore').addClass('d-none');
    $('#removeMore').removeClass('d-none');
    $('#clonediv').removeClass('d-none');
}

function openProjUploader(num)
{
    $('#fileproj'+num).click();
}

function uploadVideo(num)
{
    $('#videoFile'+num).click();
}

function uploadVideoThumbnail(num)
{
    $('#videoThumb'+num).click();
}

function showProjFileLabel(num)
{
    $('#fileprojlabel'+num).html('1 File Selected <i class="fa fa-times" onclick="removeProjFile('+num+')"></i>');
}

function removeProjFile(num)
{
    const file = document.querySelector('#fileproj'+num);
    file.value = '';
    $('#fileprojlabel'+num).html('');
}

</script>
 
   <script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
      
      
  
@endsection