@extends('layouts.home')
@php
    $userid = $productionHouse->user_id;
    $getUserDetail = DB::table('users')
        ->where('id', $userid)
        ->get();
    if (count($getUserDetail) > 0) {
        $userid = $getUserDetail[0]->id;
    } else {
        $userid = 0;
    }

    $production_about = \App\Models\ProductionAbout::where('user_id', $userid)->limit(1)->get();
@endphp
@section('meta_tags')
    <meta name="description" content="{{ is_var_exist($production_about, 'about') ? $production_about[0]->about : '' }}" >
    <meta name="keywords" content="{{ is_var_exist($production_about, 'name') ? $production_about[0]->name : '' }}" >
    <meta property="og:title" content="{{ is_var_exist($production_about, 'name') ? $production_about[0]->name : '' }} Vidoes" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:description" content="{{ is_var_exist($production_about, 'about') ? $production_about[0]->about : '' }}" />
    <meta property="og:image" content="{{ is_var_exist($production_about, 'profile_photo') ? $production_about[0]->profile_photo : '' }}" />
@endsection
@section('content')
    <style>
    

#gallery_all .owl-page span{display:none; }
#gallery_all .owl-page:nth-child(1) {position: absolute;left: -19px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow-gray.png) no-repeat;width: 15px;height: 28px;top: 130px;background-size: contain;}  
#gallery_all .owl-page:nth-child(2) {position: absolute;right: -20px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow-gray.png) no-repeat;width: 15px;height: 28px;top: 130px;background-size: contain;}
#gallery_all .owl-page:nth-child(1):hover {position: absolute;left: -19px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow.png) no-repeat;width: 15px;height: 28px;top: 130px;background-size: contain;}  
#gallery_all .owl-page:nth-child(2):hover {position: absolute;right: -20px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow.png) no-repeat;width: 15px;height: 28px;top: 130px;background-size: contain;}



        #live-video-user-list-home {
            display: none
        }
        #audition_main {
            padding: 26px 0 40px 0;
        }

        .casting-search-result {
            width: 100%;
        }

        .production-hs1 a {
            margin: 8px auto 13px;
            width: 100%;
            display: block;
            height: 100%;
        }

        .production-hs1 img {
            width: auto;    border-radius: 1000px;
           margin:21px 0 0 0;
            border: 5px solid #fff;
            height: auto;
            object-fit: cover;
        }

        .production-hs2 h5 {
            margin: 2px 0 3px 0;
        }

        .production-hs2 p.prd-info {
            font-size: 14px;
            margin: 5px 0 5px 0
        }

        #production-house-main {
            padding-top: 10px;
            padding-bottom: 15px;
            clear: both;
            overflow: hidden;
        }
        
 
        
        
        
        
        

        .production-all-display {
            padding: 4px 10px 10px 10px;
        }

        .production-all-display {
            border: 0px solid #e5e5e5;
            margin: 0.5% 0.5%;
        }

        .production-hs2 .sp1 {
            display: block;
        }

        .production-hs1 {
            width: 27%;
            float: left;
        }

        .production-hs2 {
            width: 70%;
            float: right;
        }

        .production-hs2 .sp1 {
            font-size: 14px;
        }

        .home_page_seventh_block {
            clear: both;
            overflow: hidden;
            margin-bottom: 15px;
        }

        .production-hs2 h5 {
            font-size: 25px;
        }

        #production-house-main {
            padding: 20px 0 70px 0;
            background-position: center center;
            background-size: cover;
        }

        #production-house-main {
            position: relative
        }
#production-house-main{height:450px;}
        
        #production-house-main:after {
            height:450px;
            content: '';
            display: block;
            position: absolute;
            left: 0;
            top: 0;
            width: 22%;
            z-index: 1;
            background:#f72e5e;
           
        }
 
.channels-card-image-btn a {
    border: 1px solid #fff !important;}    
    #production-house-main p{color:#fff}    
        
        #production-house-main:before {
            content: '';
            display: block;
            position: absolute;
            right: 0;
            top: 0;
            width: 34%;
            z-index: 1;
          height:450px;
              background:#f4f3f3;
            
        }

        .production-hs1 {
            padding: 2% 0 0 0;
        }

        .content_all h3 {
            line-height: 21px;
            font-weight: bold;
            color: #fff;
            font-size: 36px;
            margin: 110px 0 15px 0;
        }

        .content_all p.top1 {
            width: 100%;
            font-size: 15px;
            color: #fff;
            margin-top: 17px;
        }

        .content_all h5 {
            width: 100%;
            font-size: 18px;
            color: #fff;
            margin-bottom: 0px;
        }

        ul.social-menu {
              overflow: hidden;
    padding: 0;
    margin: 0px 0 10px 0;
    float: left;
    position: absolute;
    right: 0px;
    top: 47%;
        }

        .landing-section ul.social-menu {
            float: none;
            margin: 0 auto;
            display: inline-block;
        }

        ul.social-menu li {
            display: inline;
            list-style-type: none;
            position: relative;
            line-height: 1;
            float: left;
            margin: 0px 5px 10px 0;
            padding: 0 0 0 0;
            overflow: hidden;
            text-align: center;
            -moz-transition: opacity 0.3s ease-in;
            -webkit-transition: opacity 0.3s ease-in;
            -o-transition: opacity 0.3s ease-in;
        }

        ul.social-menu li a {
            margin: 0 0 0 0;
              color: #fff !important;
    position: relative;
    font-size: 14px;
    height: 37px;
    text-align: center;
    background: #f72e5e;
    padding: 11px 3px 8px 12px;
    float: left;
    border: none !important;
        }

        ul.social-menu li a span {
            color: #fff !important;
        }

        ul.social-menu li a i {
            margin-right: 10px;
        }

        .followers-block {
            width: 225px;
            clear: both;
            margin-left: 20px;
            display: block;
        }

        .followers-block img {
            float: left;
            width: 44px;
            border: 2px solid #fff;
            border-radius: 100px;
            margin-left: -25px;
        }

        .followers-block p {
            margin-left: 7px;
            float: left;
            color: #fff;
            font-size: 13px;
            padding-top: 15px;
        }

        .followers-block a {
            background: #f72e5e;
            padding: 7px 16px 3px 14px;
            float: right;
            color: #fff;
            margin-top: 8px;
            font-size: 13px;
        }

.what-we-do-main{position:relative;}
.what-we-do-main:before {
    content: '';
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    width: 6%;
    z-index: 1;
    height: 100%;
    background: #f72e5e;
}

.what-we-do-main:after {
    content: '';
    display: block;
    position: absolute;
    right: 0;
    top: 0;
    width: 6%;
    z-index: 1;
    height: 100%;
    background: #fff;
}

.pro-team{position:relative;}
.pro-team:before {
    content: '';
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    width: 6%;
    z-index: 1;
    height: 100%;
    background: #fff;
}
.pro-team:after {
    content: '';
    display: block;
    position: absolute;
    right: 0;
    top: 0;
    width: 6%;
    z-index: 1;
    height: 100%;
    background: #f72e5e;
}

.video-bg-bottom{position:relative;}
.video-bg-bottom:before {
    content: '';
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    width: 6%;
    z-index: 1;
    height: 100%;
    background: #f72e5e;
}

.video-bg-bottom:after {
    content: '';
    display: block;
    position: absolute;
    right: 0;
    top: 0;
    width: 6%;
    z-index: 1;
    height: 100%;
    background: #fff;
}


.my-work{position:relative;}
.my-work:before {
    content: '';
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    width: 6%;
    z-index: 1;
    height: 100%;
    background: #f4f3f3;
}
.my-work:after {
    content: '';
    display: block;
    position: absolute;
    right: 0;
    top: 0;
    width: 6%;
    z-index: 1;
    height: 100%;
    background: #f72e5e;
}



        .followers-block a:hover {
            background: #f72e5e;
            color: #fff;    cursor: pointer;
            text-decoration: none;
        }

        #production-house-main .container {
            position: relative;
            z-index: 999;
        }

        .production-house-main {
            clear: both;
            overflow: hidden;
            padding: 85px 0 60px 0;
        }
        
              

        .production-house-main h3 {
            line-height: 51px;
            font-weight: bold;
            color: #000;
            font-size: 38px;
            margin: 75px 0 6px 2px;
            position: relative;
        }

        .production-house-main p {
            width: 100%;
            font-size: 16px;
            color: #000;
            margin: 0 0 23px 0;
        }

        .production-house-left .small3 {
            font-size: 16px;
        }

        .production-house-left .small3 span {
            color: #f72e5e;
        }

        .production-house-left {
            width: 46%;
            float: left;
        }

        .production-house-right {
            width: 50%;
            float: right;
        }


        .project-work-extra-block {
            height: auto;
            margin: 0px 0 20px 0;
            padding: 33px 0;
            float: left;
            width: 100%;
             
        }

        .project-work-extra-block p {
            font-size: 15px;
    color: #000;
    margin-bottom: 22px;
    margin-top: 0px;
    float: right;
    padding: 0 30px 10px 0;
    width: 100%;
    line-height: 23px;
        }

        .project-work-extra-block strong {
              display: block;
    font-size: 23px;
    margin-bottom: 8px;
    color: #f72e5e;
    line-height: 30px;
}

        .project-work-extra-block {position: relative; }

.project-work-extra-block{clear:both; }

.project_left{width:30%;float:left;    position: relative;z-index: 9;}
.project_right{width:70%;float:left;padding:70px 15px 40px 65px;}
.project-work-extra-block:before{content: '';
    display: block;
    position: absolute;
    left: -5%;
    top: -23px;
    width: 201px;
    z-index: 8;
    height: 460px;
    background: #f4f3f3;}

.awards1 .project-work-extra-block:nth-child(odd) .project_left{float:right;}
.awards1 .project-work-extra-block:nth-child(odd) .project_right{float:left;}  
.awards1 .project-work-extra-block:nth-child(odd) .project_right{ padding:97px 48px 90px 5px;}
.awards1 .project-work-extra-block:nth-child(odd):before{  content: '';
    display: block;
    position: absolute;
    right: -5%;
    top: -23px;
    width: 201px;
    left:auto;
    z-index: 8;
    height: 460px;
    background: #f4f3f3;}
    
    
.project-single .project-work-extra-block:nth-child(odd) .project_left{float:right;}
.project-single .project-work-extra-block:nth-child(odd) .project_right{float:left;}  
.project-single .project-work-extra-block:nth-child(odd) .project_right{ padding:97px 48px 90px 5px;}
.project-single .project-work-extra-block:nth-child(odd):before{    content: '';display: block;position: absolute;right: -5%;
left:auto;top: -23px;width: 28%;z-index: 8;height: 460px;background: #f4f3f3;}








        .produciton-project {
            background: #fff;
            padding: 50px 0 8px 0;
        }

        .produciton-project h3 {
            line-height: 51px;
            font-weight: bold;
            color: #000;
            font-size: 38px;
            position: relative;
            margin: 0 0 50px 0;
            text-transform: uppercase;
            text-align: Center;
        }

        .project-work-extra-block a {
            margin-top: 10px;
            width: 150px;
            line-height: 46px;
            display: block;
            color: #fff;
            background: #f72e5e;
            text-align: Center;
        }

        .pro-team {
             background: #f4f3f3;
              padding: 70px 0 35px 0;
        }

        .pro-team h3 {
            line-height: 51px;
            font-weight: bold;
            color: #000;
            font-size: 38px;
            text-transform: uppercase;
            text-align: Center;
            position: relative;
            margin: 0 0 25px 0;
        }

        .pro-team-slide-ind a {
            width: 98%;
            float: left;
            margin: 0 1% 20px 1%;
        }

        .pro-team-slide-ind a img {
            width: 100%;
        }

        .pro-team h6 {
            text-align: center;
            color: #000;
            font-size: 20px;
            position: relative;
            margin: 15px 0 0px 0;
        }

        .pro-team span {
            width: 100%;
            font-size: 14px;
            color: #000;
            margin: 0 0 23px 0;
            text-align: center;
            display: block;
        }


         

        .what-we-do-block h3 {
            text-align: center;
            text-transform: uppercase;
            line-height: 51px;
            font-weight: bold;
            color: #000;
            font-size: 38px;
            margin: 10px 0 10px 2px;
            position: relative;
        }

        .what-we-do-main p.btm02 {
            text-align: center;
            font-size: 15px;
            padding: 0 0px 10px 0;
            width: 49%;
            line-height: 23px;
            margin: -7px auto 4px;
        }

.what-we-do-block-left{position:relative;}

.what-we-do-block-left span{
   position: absolute;
    left: -8%;
    top: -10px;
    width: 100%;
    font-size: 141px;
    opacity: 0.1;
    overflow: hidden;
    line-height: 127px;
    letter-spacing: -7px;
    padding: 19px 0 0 0;
    font-weight: bold;
    color: #f72e5e;
    text-align: left;
   }


        .what-we-do-block-left {
              min-height: 165px;
    float: left;
    width: 24%;
    margin: 0 0.5%;
    padding: 23px 0 00 10px;
    margin-top: 10px;
        }

        .what-we-do-block-left h4 {
              text-align: left;
    color: #000;
    font-size: 20px;
    position: relative;
    font-weight: bold;
    margin: 10px 0 7px 0;
        }


.abt7:after {
    content: "ABOUT";
    position: absolute;
    left: 1%;
    top: 71px;
    width: 65%;
    font-size: 227px;
    opacity: 0.7;
    overflow: hidden;
    text-align: left;
    line-height: 249px;
    letter-spacing: -7px;
    padding: 19px 0 0 0;
    font-weight: bold;
    color: #f4f3f3;
    z-index: -1;
}

.what-we-do-main{
  
            padding: 60px 0 45px 0;
 
    background-color: #f4f3f3;
    /*background-image: url(https://natalie.creativeher.co/wp-content/uploads/2022/10/ink-splatter-e1667217940125.png);*/
    background-size: auto;
    background-position: 0% 0%;
    background-attachment: scroll;
    background-repeat: no-repeat;
}


        .what-we-do-block-left p {
text-align: left;
font-size: 14px;
color: #000;
padding: 0 0px 10px 0;
width: 100%;
line-height: 21px;
margin: 0;
        }

        .contact-block-production { 
            padding: 4px 0 60px 0;
        }

        .contact-new-last {
            background: #f72e5e;
            padding: 21px 0 0px 0;
        }

        .contact-new-last h3 {
            line-height: 51px;
            font-weight: bold;
            color: #fff;
            font-size: 62px;
            text-align: Center;
            position: relative;
            margin: 72px 0 15px 0;
        }

        .contact-new-last a {
            background: #fff;
            width: 200px;
            display: block;
            text-align: center;
            line-height: 40px;
            padding: 14px 0 9px 0;
            float: left;
            font-size: 20px;
            margin: 0 5px;
            color: #f72e5e;
        }

        .contact-new-last p {
            color: #fff;
            width: 62%;
            margin: 0 auto 29px;
            font-size: 20px;
            text-align: center;
        }

        .contact-block-production-inside h3 {
            text-align: center;
            text-transform: uppercase;
            line-height: 51px;
            font-weight: bold;
            color: #f72e5e;
            font-size: 38px;
            margin: 25px 0 11px 2px;
            position: relative;
        }

        .contact-block-production-inside p.btm02 {
            text-align: center;
            font-size: 18px;
            padding: 0 0px 50px 0;
            width: 49%;
            line-height: 23px;
            margin: -7px auto 4px;
        }

        .contact-block-production-left {
            width: 100%;
            float: none;
            margin-top: 30px;
        }

        .contact-block-production-left i {
            width: 24px;
            text-align: Center;
        }

        .contact-block-production-left h3 {
            text-align: center;
            color: #fff;
            font-size: 24px;
            margin: 20px 0 2px 2px;
            position: relative;
        }

        .contact-block-production-left p {
            text-align: left;
            font-size: 14px;
            padding: 0 0px 10px 0;
            width: 13%;
            line-height: 23px;
            margin: 0 0 6px 0;
            color: #fff;
            float: left;
        }

        .contact-block-production-left ul {
            margin-top: 20px;
        }

        .contact-block-production-right {
            width: 46%;
            float: left;
        }

        #audition-project {
            padding: 50px 0 65px 0;
        }

        #audition-project h3 {
            line-height: 51px;
            font-weight: bold;
            color: #000;
            font-size: 38px;
            position: relative;
            margin: 0 0 20px 0;
            text-transform: uppercase;
            text-align: Center;
        }

        #production-house-main {
            padding-top: 10px;
                background: #f72e5e;
            padding-bottom: 20px;
        }

        #audition-project .production-hs1 {
            width: 17%;
            float: left;
        }

        #audition-project .production-hs1 img {
            width: 100%
        }

        #audition-project .production-hs2 {
            width: 83%;
            float: right;
            background: #fff;
            padding: 25px 30px 16px 30px;
        }

        #audition-project .production-hs2 h5 {
            color: #f72e5e !important;
            font-size: 21px;
            margin: 0px 0 8px 0;
        }

        #audition-project .production-hs2 p.prd-info {
            font-size: 14px;
            margin: 5px 0 8px 0
        }

        #audition-project .production-hs2 .sp1 {
            display: block;
            color: #6e6f70;
            margin-bottom: 4px;
            font-size: 14px;
        }

        #audition-project .production-hs2 span {
            margin-right: 5px;
            color: #f72e5e !important;
            font-weight: 500
        }

        #audition-project .width-space {
            width: 15px;
        }

        #audition-project .production-hs2 a {
            color: #f72e5e !important
        }

        #audition-project .production-all-display {
            border: 1px solid #e5e5e5;
            padding: 0;
            margin: 0px;
            background: #f72e5e !important;
        }

        #audition-project .production-hs1 img {
            border: 2px solid #eaeaea;
        }

        #auditions {
            display: block
        }

        #auditions p {
            margin: 0 0 8px;
            float: left;
        }

        #audition-project .width-space,
        .line-0 {
            float: left;
        }

        #auditions .width-space {
            margin: 0 0px;
            height: 1px;
        }

        #audition-project .production-hs2 {
            position: relative;
        }

        #audition-project .production-hs2 a.view-m {
            position: absolute;
            right: 10px;
            bottom: 17px;
            background: #28b47e !important;
            color: #fff !important;
            padding: 6px 22px 4px 22px;
        }

        #audition-project .production-hs1 a {
            display: block;
            overflow: hidden;
            width: 120px;
            margin: 12px auto 0;
            height: 121px;
        }

        #audition-project .production-hs1 img {
            border-radius: 500px;
            object-fit: cover;
            width: 100%;
            height: 100%;
        }


        #content_alls {
            background: #f4f3f3;
            padding: 50px 0 50px 0;
        }

        #content_alls h3 {
            line-height: 51px;
            font-weight: bold;
            color: #f72e5e;
            font-size: 38px;
            position: relative;
            margin: 0 0 20px 0;
            text-transform: uppercase;
            text-align: Center;
        }

        #content_alls img {
            width: 23%;
            float: left;
            margin: 0 1% 20px 1%;
        }

        .production-house-main video {
            height:352px;
        }


        .my-work a {
               width:98%;
    float: left;
    margin: 0px 1% 8px 1%;
        }


.my-work a:after {
    background: linear-gradient(180deg,#f72e5e 0,#000);
    position: absolute;
    left: 0;
    right: 0;
    width: 100%;
    height: 100%;
    content: "";transition: all .3s cubic-bezier(.645,.045,.355,1);
    top:0px;
    z-index: 1;
    opacity: 0;
}

 
.my-work a{overflow:hidden;}
.my-work a{position:relative;}
.my-work a:hover h6{bottom:50px;    position: absolute;
    bottom: 0px;
    width: 100%;
    padding: 50px 35px;
    z-index: 3;
    transition: all .3s cubic-bezier(.645,.045,.355,1);
    text-align: center;
    color: #fff;
    margin: 0px 0 0 0;
    background: rgba(0, 0, 0, 0.5);
    padding: 35px 20px 30px 20px;
    font-size: 17px;} 
    
.my-work h6 {
    position: absolute;
    bottom: 0px;
    width: 100%;
    padding: 50px 35px;
    z-index: 3;
 transition: all .3s cubic-bezier(.645,.045,.355,1);
            text-align: center;
            color: #fff;
            margin: 0px 0 0 0;
            background: rgba(0, 0, 0, 0.5);
            padding: 15px 20px 7px 20px;
            font-size: 16px;
        }

        .my-work {
            background: #fff;
               padding: 55px 0 73px 0;
            clear: both;
            overflow: hidden;
        }

        .my-work h3 {
            line-height: 51px;
            font-weight: bold;
            color: #000;
            font-size: 38px;
            text-transform: uppercase;
            text-align: Center;
            position: relative;
            margin: 0 0 25px 0;
        }

        .video-bg-bottom {
            background: #f4f3f3;
            padding: 50px 0 30px 0;
        }

        .content_alls h3 {
            line-height: 51px;
            font-weight: bold;
            color: #000;
            font-size: 38px;
            text-transform: uppercase;
            -transform: uppercase;
            text-align: Center;
            position: relative;
            margin: 0 0 25px 0;
        }

        .content_alls {
            clear: both;
            overflow: hidden;
        }

        .cd-011 {
            width: 31.6%;
            float: left;
            margin: 0 1% 24px 1%;
            height: 356px;
            overflow: hidden;
        }

.cd-011 video {
width: 100%;
height: 205px;
background-size: cover;
object-fit: cover;
}

  

        .inside {
            padding: 20px;
            background: #f4f3f3;
        }

        .inside h6 {
            margin: 0px;
            padding: 7px 0 3px 0;
            font-size: 17px;
            color: #f72e5e;
        }

        .cd-011.margin-right-off {
            margin-right: 0px !important;
        }

        #contact-and-email {
            width: 420px;
            margin: 0 auto;
        }
  
    
    .abt7{position:relative;padding:50px 0;}
     .abt7:before {
            content: '';
    display: block;
    position: absolute;
    right: 0;
    top: 0;
    width: 34%;
    z-index: -1;
    height: 100%;
    background: #f72e5e;
            
        }
        
    @media  only screen and (max-width: 767px) {
#production-house-main {height: auto;padding-top: 23px;background: #f72e5e;padding-bottom: 50px;}
#production-house-main:before {width: 0;}     
.content_all h3 {margin: 26px 0 15px 0;}       
.production-hs1 {padding: 0% 0 0 0;} 
ul.social-menu li a {background: #d02a52;}
 ul.social-menu{width:280px; position:relative;}
.production-house-left, .production-house-right {width: 100%;float: none;} 
.production-house-main {padding: 0;} 
.production-house-main video {height: 204px;}
.production-house-main h3 {font-size: 38px;position: relative;margin:0px;}
.abt7 {padding: 40px 0 50px 0;}
.abt7:before, .what-we-do-main:before, .what-we-do-main:after, .my-work:before, .my-work:after{display:none;}
.video-bg-bottom:before, .video-bg-bottom:after{display:none;}
.what-we-do-main p.btm02 {font-size: 14px !important;width: 100%;line-height: 20px;margin: -7px auto 4px;}
.what-we-do-block-left {min-height: auto;float: left;width: 99%;margin: 0 0.5%;padding: 0 0 00 10px;margin-top: 3px;}
.what-we-do-block-left span {left: -1%;top: -32px;width: 100%;font-size: 96px;line-height: 127px;letter-spacing: -7px;padding: 19px 0 0 0;}      
.pro-teams.gallery{width:92%;margin:0 auto;}       
.cd-011 {width: 98%;margin: 0 1% -2px 1%;height: auto;}
.project_left, .project_right{width:100% !important; float:none;}
.project-work-extra-block {overflow: hidden;}      
 .project_right {padding: 35px 10px 0 10px;}       
.project-work-extra-block {height: auto;margin: 0px 0 0px 0;padding: 1px 0 0 0;float: left;width: 100%;}
.project-work-extra-block p {width: 100%;}
.project-single .project-work-extra-block:nth-child(odd) .project_right {padding: 35px 0px 20px 2px;}
.pro-team:before, .pro-team:after{display:none;}
.what-we-do-block-left h4 { font-size: 18px}
.awards1 .project-work-extra-block:nth-child(odd) .project_right {padding: 60px 9px 20px 5px;}
#contact-and-email {width: 100%;}     
.contact-new-last p {width: 100%;}
.contact-new-last a {width: 48%;line-height: 40px;padding: 14px 0 9px 0;font-size: 18px;margin: 0 1%;}
.contact-new-last h3 {line-height: 42px;font-size: 38px;margin: 50px 0 15px 0;}
.contact-block-production-left p {    text-align: center;font-size: 14px;padding: 0 0px 10px 0;width: 100% !important;line-height: 22px;margin: 0 0 1px 0 !important;float: left;}
.pro-team-slide-ind a {width: 98%;margin: 0 1% 0 1%;}
.produciton-project h3 {line-height: 51px;margin: 0 0 17px 0;}       
.production-house-main p {    font-size: 14px;}      
.my-work {padding: 45px 0 35px 0;}        
.project-work-extra-block p, .contact-new-last p{padding: 0 0px 10px 0; font-size: 14px;}
.video-bg-bottom {padding: 42px 0 30px 0;}
.produciton-project {padding: 45px 0 8px 0;}
.what-we-do-main {padding: 35px 0 35px 0;}      
.pro-team {padding: 55px 0 35px 0;}      
.project-work-extra-block:before{display:none;}
.project-single .project-work-extra-block:nth-child(odd):before{display:none;}
.project-work-extra-block strong {font-size: 20px;margin-bottom: 8px;line-height: 23px;}

    }
      
      
      
      
        </style>


    </head>

    <body class="light">
        @php
            $production_what_we_do = DB::select('select * from production_what_we_do where user_id=' . $userid);
            
            $production_awards = DB::select('select * from production_awards where user_id=' . $userid);
            $post_audtion = DB::select('select * from post_audtion where user_id=' . $userid);
            $production_gallery = DB::select('select * from production_gallery where user_id=' . $userid);
            $production_video = DB::select('select * from production_video where user_id=' . $userid);
            $production_project = DB::select('select * from production_latest_project where user_id=' . $userid);
            $production_contact = DB::select('select * from production_contact_us where user_id=' . $userid);        
        @endphp
        @if(!$productionHouse->isDeleted)
        <div id="production-house-main">
            <div class="container">
                <div class="row">
                    <div class="content-block">
                        <div class="casting-search-result" id="video-cast-all-vide">
                            <div class="col-lg-12">
                                <!-- Popular Channels -->
                                <div class="content-block">
                                    <div class="production-all-display">
                                        <div class="production-hs1">
                                            <a href="">
                                                <img src="/public/uploads/temp/<?php echo count($production_about) > 0 ? (isset($production_about[0]->profile_photo) ? $production_about[0]->profile_photo : '') : ''; ?>">
                                            </a>
                                        </div>
                                        <div class="production-hs2">
                                            <div class="content_all">
                                                <h3>
                                                    <?php echo count($production_about) > 0 ? (isset($production_about[0]->name) ? $production_about[0]->name : '') : ''; ?>
                                                </h3>
                                                
                                              
<h5>
<?php echo count($production_about) > 0 ? (isset($production_about[0]->multiple_category) ? $production_about[0]->multiple_category : '') : ''; ?>
</h5>
<p style="font-size:13px;margin:5px 0 3px 0"><i class="fa fa-globe" aria-hidden="true"></i>  <?php echo count($production_about) > 0 ? (isset($production_about[0]->city) ? $production_about[0]->city : '') : ''; ?> |  <?php echo count($production_about) > 0 ? (isset($production_about[0]->state) ? $production_about[0]->state : '') : ''; ?></p>

<p style="font-size:13px;margin:0px 0 3px 0"><i class="fa fa-language" aria-hidden="true"></i>  <?php echo count($production_about) > 0 ? (isset($production_about[0]->language) ? $production_about[0]->language : '') : ''; ?>      
</p>   
                                                

<p style="font-size:13px;margin:0px 0 3px 0"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo count($production_about) > 0 ? (isset($production_about[0]->working_hours_one) ? $production_about[0]->working_hours_one : '') : ''; ?> -

<?php echo count($production_about) > 0 ? (isset($production_about[0]->working_hours_two) ? $production_about[0]->working_hours_two : '') : ''; ?>
</p>


<p style="font-size:13px;margin:0px 0 13px 0"><i class="fa fa-calendar" aria-hidden="true"></i> <span>Registration date: </span> <?php echo count($production_about) > 0 ? (isset($production_about[0]->production_house_date) ? $production_about[0]->production_house_date : '') : ''; ?>
</p>               
                                                
                                            
                                                <ul class="social-menu tranz">
                                                      <li>
                                                        <a target="_blank" title="" href="{{ user_url($getUserDetail[0]->slug) }}" class="entertainment_icon-1">
                                                            <img style="width: 17px;float: left;margin: -3px 8px 0 -1px;" src="http://entertainmentbugs.com/public/img/logo_footer-white.png">
                                                            <!--<span style="line-height: 14px;display: inline-block;padding: 5px 0 0 0;">Entertainment Profile</span>-->
                                                        </a>
                                                    </li> <li class="sprite-imdb">
                                                        <a target="_blank" class="mk-social-imdb" title="IMDb"
                                                            href="<?php echo count($production_about) > 0 ? (isset($production_about[0]->facebook) ? $production_about[0]->facebook : '') : ''; ?>
                        ">
                                                            <i class="fa fa-facebook"></i>
                                                            
                                                        </a>
                                                    </li>
                                                    <li class="sprite-instagram">
                                                        <a target="_blank" class="mk-social-photobucket"
                                                            href=" <?php echo count($production_about) > 0 ? (isset($production_about[0]->instagram) ? $production_about[0]->instagram : '') : ''; ?>
                        ">
                                                            <i class="fa fa-instagram"></i>
                                                             
                                                        </a>
                                                    </li>
                                                    <li class="sprite">
                                                        <a target="_blank" class="mk-social" title=""
                                                            href="
                        
                         <?php echo count($production_about) > 0 ? (isset($production_about[0]->youtube) ? $production_about[0]->youtube : '') : ''; ?>
                        ">
                                                            <i class="fa fa-youtube"></i>
                                                            
                                                        </a>
                                                    </li>
                                                    <li class="sprite">
                                                        <a target="_blank" class="mk-social" title=""
                                                            href="
                        
                         <?php echo count($production_about) > 0 ? (isset($production_about[0]->linkedin) ? $production_about[0]->linkedin : '') : ''; ?>
                        ">
                                                            <i class="fa fa-linkedin"></i>
                                                             
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a target="_blank" class="mk-social" title="" href="">
                                                            <i class="fa fa-inbox"></i>
                                                           
                                                        </a>
                                                    </li>
                                                 
                                                </ul>
                                                <div class="followers-block">
                                                    <img
                                                        src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-1-400x400.jpg">
                                                    <img
                                                        src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-2-400x400.jpg">
                                                    <img
                                                        src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-3-400x400.jpg">
                                                    <img
                                                        src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-4-400x400.jpg">
                                                    <img
                                                        src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-5-400x400.jpg">
                                                        
                                                    @if ( Session::get("user_id") && $productionHouse->user->isFollowedBy() )
                                                        <div class="channels-card-image-btn" id="suggested_unfollow_user{{ $productionHouse->user->id }}">
                                                            <a data-do="unfollow" data-user-id="{{ $productionHouse->user->id }}" data-template="with-count" style="background-color: #f72e5e!important;"><i class="fa fa-solid fa-heart"></i> Following </a>
                                                        </div>
                                                    @else
                                                        <div class="channels-card-image-btn" id="suggested_follow_user{{ $productionHouse->user->id }}">
                                                            <a data-do="follow" data-user-id="{{ $productionHouse->user->id }}" data-template="with-count" class="flw"><i class="fa fa-heart-o"></i> Follow me </a>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /Popular Channels -->
                    </div>
                </div>
                <!--casting video END-->
            </div>
        </div>
        <!-- /Featured Videos -->
        <div class="abt7">
            <div class="container">
                <div class="production-house-main">
                    <div class="production-house-left">
                        <h3>About Me</h3>
                        <p>

                            <?php echo count($production_about) > 0 ? (isset($production_about[0]->about) ? $production_about[0]->about : '') : ''; ?>
                        </p>
                        <p class="small3">
                           
          
                         
                       </p>
                    </div>
                    <div class="production-house-right">
                        <video width="100%" height="340"
                            poster="{{ $productionHouse->thumb }}"
                             autoplay loop muted>
                            <source src="{{ $productionHouse->video }}" type="video/mp4">
                            <source src="{{ $productionHouse->video }}" type="video/ogg"> Your browser does not support the video tag.
                        </video>
                    </div>
                </div>
            </div>
       
     
     
   <img src="https://natalie.creativeher.co/wp-content/uploads/2022/10/ink-splatter-e1667217940125.png" style="
    position: absolute;
    width: 23%;
    bottom: -72px;
">
         </div>
        
        <div class="what-we-do-main">
            <div class="container">
                <div class="what-we-do-block">
                    <h3>
                        <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->what_we_do) ? $production_what_we_do[0]->what_we_do : '') : ''; ?>
                    </h3>
                    <p class="btm02">

                        <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->description) ? $production_what_we_do[0]->description : '') : ''; ?>
                    </p>
                    <div class="what-we-do-block-left">
                      <span>1</span>  <h4>

                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->title_1) ? $production_what_we_do[0]->title_1 : '') : ''; ?>
                        </h4>
                        <p>

                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->des_1) ? $production_what_we_do[0]->des_1 : '') : ''; ?>
                        </p>
                    </div>
                    <div class="what-we-do-block-left">
                       <span>2</span> <h4>

                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->title_2) ? $production_what_we_do[0]->title_2 : '') : ''; ?>
                        </h4>
                        <p>

                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->des_2) ? $production_what_we_do[0]->des_2 : '') : ''; ?>
                        </p>
                    </div>
                    <div class="what-we-do-block-left">
                       <span>3</span> <h4>

                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->title_3) ? $production_what_we_do[0]->title_3 : '') : ''; ?>
                        </h4>
                        <p>

                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->des_3) ? $production_what_we_do[0]->des_3 : '') : ''; ?>
                        </p>
                    </div>
                    <div class="what-we-do-block-left">
                       <span>4</span> <h4>

                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->title_4) ? $production_what_we_do[0]->title_4 : '') : ''; ?>
                        </h4>
                        <p>
                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->des_4) ? $production_what_we_do[0]->des_4 : '') : ''; ?>
                        </p>
                    </div>
                    
                    
                    
                    
                    <?php 
        if(count($production_what_we_do) > 0)
        {
            if(isset($production_what_we_do[0]->title_5))
            {
      ?>
                    <div class="what-we-do-block-left">
                     <span>5</span>   <h4>

                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->title_5) ? $production_what_we_do[0]->title_5 : '') : ''; ?>
                        </h4>
                        <p>
                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->des_5) ? $production_what_we_do[0]->des_5 : '') : ''; ?>
                        </p>
                    </div>
                    <?php 
                }
            } 
        ?>
                    <?php 
        if(count($production_what_we_do) > 0)
        {
            if(isset($production_what_we_do[0]->title_6))
            {
      ?>
                    <div class="what-we-do-block-left">
                        <span>6</span><h4>

                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->title_6) ? $production_what_we_do[0]->title_6 : '') : ''; ?>
                        </h4>
                        <p>
                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->des_6) ? $production_what_we_do[0]->des_6 : '') : ''; ?>
                        </p>
                    </div>
                    <?php 
                }
            } 
        ?>
                    <?php 
        if(count($production_what_we_do) > 0)
        {
            if(isset($production_what_we_do[0]->title_7))
            {
      ?>
                    <div class="what-we-do-block-left">
                       <span>7</span> <h4>

                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->title_7) ? $production_what_we_do[0]->title_7 : '') : ''; ?>
                        </h4>
                        <p>
                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->des_7) ? $production_what_we_do[0]->des_7 : '') : ''; ?>
                        </p>
                    </div>
                    <?php 
                }
            } 
        ?>
                    <?php 
        if(count($production_what_we_do) > 0)
        {
            if(isset($production_what_we_do[0]->title_8))
            {
      ?>
                    <div class="what-we-do-block-left">
                       <span>8</span> <h4>

                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->title_8) ? $production_what_we_do[0]->title_8 : '') : ''; ?>
                        </h4>
                        <p>
                            <?php echo count($production_what_we_do) > 0 ? (isset($production_what_we_do[0]->des_8) ? $production_what_we_do[0]->des_8 : '') : ''; ?>
                        </p>
                    </div>
                    <?php 
                }
            } 
        ?>

                </div>
            </div>
        </div>
        <div class="my-work">
            <div class="container">
                <h3>My Gallery</h3>
                
                    <div class="pro-teams gallery">
                        <div class="pro-team-slide-ind">
                          
                          
                            <div class="span12" id="">
              <div id="gallery_all" class="owl-carousel">
                
                          
                            <?php 
                foreach($production_gallery as $ag){
            ?>

                          <div class="item">     <a style="" href="
            
            <?php echo count($production_gallery) > 0 ? (isset($ag->photo) ? $ag->photo : '') : ''; ?>
            ">
                                <img src="<?php echo count($production_gallery) > 0 ? (isset($ag->photo) ? $ag->photo : '') : ''; ?>" alt="<?php echo count($production_gallery) > 0 ? (isset($ag->photo_title) ? $ag->photo_title : '') : ''; ?>"
                                    title="<?php echo count($production_gallery) > 0 ? (isset($ag->photo_title) ? $ag->photo_title : '') : ''; ?>"
                                    style="">
                                <?php 
                if(count($production_gallery) > 0)
                {
                    if(isset($ag->category))
                    {
              ?>
                                <h6>

                                    <?php echo count($production_gallery) > 0 ? (isset($ag->category) ? $ag->category : '') : ''; ?>
                                </h6>
                                <?php } } ?>
                            </a>
                            
                             </div>
                            
                            
                            
                            <?php } ?>
                        </div>
                    </div>
                     
                            
                            
                        </div>
                    </div>
              
            </div>
        </div>
        
        
        
        
        <div class="video-bg-bottom">
            <div class="container">
                <div class="rows">
                    <div class="content_alls">
                        <h3> Video</h3>
                        <div class="slide-ind">
                            <?php 
              $count=0;
              foreach($production_video as $av) { ?>
                            <div class="cd-011 <?php if($count%3==0) {?> margin-right-off <?php } ?>">
                                <video class="" poster="<?php echo count($production_video) > 0 ? (isset($av->thumb) ? $av->thumb : '') : ''; ?>
              " controls>
                                    <source src="
                
                <?php echo count($production_video) > 0 ? (isset($av->video) ? $av->video : '') : ''; ?>
                "
                                        type="video/mp4">
                                    <source src="
                
                <?php echo count($production_video) > 0 ? (isset($av->video) ? $av->video : '') : ''; ?>
                "
                                        type="video/ogg"> Your browser does not support the video tag.
                                </video>
                                <div class="inside">
                                    <small>

                                        <?php echo count($production_video) > 0 ? (isset($av->category) ? $av->category : '') : ''; ?>
                                    </small>
                                    <h6>

                                        <?php echo count($production_video) > 0 ? (isset($av->video_title) ? $av->video_title : '') : ''; ?>
                                    </h6>
                                    <p>

                                        <?php echo count($production_video) > 0 ? (isset($av->description) ? $av->description : '') : ''; ?>
                                    </p>
                                </div>
                            </div>


                            <?php 
            $count=$count+1;
            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="produciton-project project-single">
            <div class="container">
                <h3>Latest Projects</h3>
                <?php 
      foreach($production_project as $ap) {
      ?>
                <div class="project-work-extra-block">
                  
                  <div class="project_left">
                    <img src="
        
        <?php echo count($production_project) > 0 ? (isset($ap->photo) ? $ap->photo : '') : ''; ?>
        " style="width:100%">
                 </div>
                 
                    <div class="project_right">
                    <p>
                        <strong>

                            <?php echo count($production_project) > 0 ? (isset($ap->title) ? $ap->title : '') : ''; ?>
                            <?php
                            echo count($production_project) > 0 ? (isset($ap->date) ? '-' . date('d F, Y', strtotime($ap->date)) : '') : '';
                            ?>
                        </strong>

                        <?php echo count($production_project) > 0 ? (isset($ap->description) ? $ap->description : '') : ''; ?>

                        <a href="<?php echo count($production_project) > 0 ? ($ap->link != '' || $ap->link != null ? $ap->link : 'javascript:void(0)') : 'javascript:void(0)'; ?>">
                            <?php echo count($production_project) > 0 ? (isset($ap->button_name) ? $ap->button_name : '') : ''; ?>
                        </a>
                    </p>
                     </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="pro-team">
            <div class="container">
                <h3>Our professional team</h3>
                <div class="pro-team-slide-ind">
                   
                        <div class="span12" id="scrousal_wrap">
            <div id="achievement_all" class="owl-carousel">
      
      
         
                   
                   
                   
                   
                   
                   
                   
                    <?php 
            $getTeam = DB::select('select * from production_my_team where user_id='.$userid);
            if(count($getTeam) > 0)
            {
                foreach($getTeam as $k=>$v)
                {
        ?>
                   <div class="item">   <a href="javascript:void(0)">
                        <?php 
            if($v->photo != NULL || $v->photo != '')
            {
        ?>
                        <img src="<?php echo $v->photo; ?>">
                        <?php }else{ 
            $getUserDetail = DB::table('users')->where('name',$v->search_by_name)->get();
            if(count($getUserDetail) > 0)
            {
                if($getUserDetail[0]->photo != NULL)
                {
        ?>
                        <img src="{{ URL::to('/') }}/public/uploads/users/small/<?php echo $getUserDetail[0]->photo; ?>">
                        <?php   
                }else{ 
        ?>
                        <img
                            src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-1-400x400.jpg">
                        <?php } ?>
                        <?php }else{ ?>
                        <img
                            src="https://demo.gloriathemes.com/noxe/demo/wp-content/uploads/2020/01/title-photo-1-400x400.jpg">
                        <?php } ?>
                        <?php } ?>
                        <h6><?php echo $v->search_by_name != '' || $v->search_by_name != null ? $v->search_by_name : '-'; ?></h6>
                        <span><?php echo $v->position != '' || $v->position != null ? $v->position : '-'; ?></span>
                    </a>      </div>  
                    <?php } } ?>
                    
                    
                        
             
                </div>     
                </div> 
                    
                    
                    
                    
                    
                </div>
            </div>
        </div>
        <div class="produciton-project awards1">
            <div class="container">
                <h3>Awards </h3>
                <?php foreach($production_awards as $pa) {?>

                <div class="project-work-extra-block">
                   
               <div class="project_left">    
                    <img src=" <?php echo count($production_awards) > 0 ? (isset($pa->photo_1) ? $pa->photo_1 : '') : ''; ?>" style="width:100% ">
                      </div>
                         
               <div class="project_right">    
                   <strong><?php echo count($production_awards) > 0 ? (isset($pa->title_1) ? $pa->title_1 : '') : ''; ?></strong>
                    <p> 
                        <?php echo count($production_awards) > 0 ? (isset($pa->des_1) ? $pa->des_1 : '') : ''; ?></p>
               
                   </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php 
        $getIfAlready = DB::table('post_audtion')->where('user_id',$userid)->get();
        if(count($getIfAlready) > 0)
        {
    ?>
        <div id="audition-project" style="background:#f4f3f3">
            <div class="container">
                <h3>Our Auditions</h3>
                <div class="production-all-display">
                    <div class="production-hs1">
                        <a href="javascript:void(0)">
                            <img src="<?php echo count($getIfAlready) > 0 ? ($getIfAlready[0]->photo != null || $getIfAlready[0]->photo != '' ? $getIfAlready[0]->photo : 'https://1zhit3xkjyq3ki6yq3sav7ahhz-wpengine.netdna-ssl.com/wp-content/uploads/2018/11/Our-Lady-LTD-Season-1-Epix-400x190.png') : 'https://1zhit3xkjyq3ki6yq3sav7ahhz-wpengine.netdna-ssl.com/wp-content/uploads/2018/11/Our-Lady-LTD-Season-1-Epix-400x190.png'; ?>"></a>
                        </a>
                    </div>
                    <div class="production-hs2">
                        <h5><?php echo count($getIfAlready) > 0 ? ($getIfAlready[0]->title != null || $getIfAlready[0]->title != '' ? $getIfAlready[0]->title : '') : ''; ?></h5>
                        <p class="prd-info"> <?php echo count($getIfAlready) > 0 ? ($getIfAlready[0]->description != null || $getIfAlready[0]->description != '' ? $getIfAlready[0]->description : '') : ''; ?></p>
                        <div class="sp1" id="auditions">
                            <p><span><i class="fa fa-list-alt"></i> Category: </span> <?php echo count($getIfAlready) > 0 ? ($getIfAlready[0]->category != null || $getIfAlready[0]->category != '' ? $getIfAlready[0]->category : '') : ''; ?></p>
                            <div class="width-space"></div>
                            <div class="line-0">|</div>
                            <div class="width-space"></div>
                            <p><span><i class="fa fa-map-marker"></i> Location: </span> 
                            <?php echo count($getIfAlready) > 0 ? ($getIfAlready[0]->city != null || $getIfAlready[0]->city != '' ? $getIfAlready[0]->city : '') : ''; ?></p>
                            <div class="width-space"></div>
                            <div class="line-0">|</div>
                            <div class="width-space"></div>
                            <p><span><i class="fa fa-calendar"></i> Posted on: </span> <?php echo count($getIfAlready) > 0 ? ($getIfAlready[0]->posted_date != null || $getIfAlready[0]->posted_date != '' ? date('d F, Y', strtotime($getIfAlready[0]->posted_date)) : '') : ''; ?></p>
                            <div class="clear"></div>
                            <p>
                                <span>
                                    <i class="fa fa-user"></i> Posted By: </span> <?php echo $title; ?>
                            </p>
                            <div class="width-space"></div>
                            <div class="line-0">|</div>
                            <div class="width-space"></div>
                            <p><span><i class="fa fa-language"></i> Language: </span> <?php echo count($getIfAlready) > 0 ? ($getIfAlready[0]->language != null || $getIfAlready[0]->language != '' ? $getIfAlready[0]->language : '') : ''; ?></p>
                            <div class="clear"></div>
                            <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> <?php echo count($getIfAlready) > 0 ? ($getIfAlready[0]->valid_till != null || $getIfAlready[0]->valid_till != '' ? date('d F, Y', strtotime($getIfAlready[0]->valid_till)) : '') : ''; ?></p>
                        </div>
                        <a href="{{ URL::to('/audition/' . $getIfAlready[0]->slug) }}" class="view-m">View Now</a>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="contact-new-last">
            <div class="container">
                <h3><?php echo count($production_contact) > 0 ? (isset($production_contact[0]->title) ? $production_contact[0]->title : '') : ''; ?>
                 </h3>
                <p>Drop us a email to get hired or getin touch with us through whatsapp to get an update on our upcoming new
                    projects of webseries.</p>
                <div id="contact-and-email">
                    <a href="mailto:<?php echo count($production_contact) > 0 ? (isset($production_contact[0]->email_id) ? $production_contact[0]->email_id : '') : ''; ?>">
                        <i class="fa fa-envelope"></i> Email me </a>
                    <a href="https://wa.me/<?php echo count($production_contact) > 0 ? (isset($production_contact[0]->number) ? $production_contact[0]->number : '') : ''; ?>">
                        <i class="fa fa-whatsapp"></i> Contact Us </a>
                </div>
            </div>
        </div>
        <div class="contact-block-production"
            style="    background: #f72e5e;">
            <div class="container">
                <div class="contact-block-production-left">
                    <!--<h3>YOU KNOW WHAT TO DO NOW.</h3>-->
                    <h3>Official Address</h3>
                    <p style="width: 100%;text-align: center;margin: 0 0 -9px 0;">
                        <i class="fa fa-map-marker" style="display: none;"></i><?php echo count($production_contact) > 0 ? (isset($production_contact[0]->address) ? $production_contact[0]->address : '') : ''; ?>
                    </p>
                    <!--<p style="margin-left: 39%;width:auto;">-->
                    <!--    <i class="fa fa-envelope"></i> <?php echo count($production_contact) > 0 ? (isset($production_contact[0]->email_id) ? $production_contact[0]->email_id : '-') : '-'; ?>-->
                    <!--</p>-->
                    <!--<p>-->
                    <!--    <i class="fa fa-phone"></i> <?php echo count($production_contact) > 0 ? (isset($production_contact[0]->landline_number) ? $production_contact[0]->landline_number : '-') : '-'; ?>-->
                    <!--</p>-->
                    <ul class="social-menu tranz" style="display: none;">
                        <li>
                            <a href="https://wa.me/<?php echo count($production_contact) > 0 ? (isset($production_contact[0]->number) ? $production_contact[0]->number : 'javascript:void(0)') : 'javascript:void(0)'; ?>" style="background:#25D366;">
                                <i class="fa fa-whatsapp" style="width:auto"></i>
                                <span>Whatspp</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        </div>
        </div>
        @else
        <div class="under-review"><h5>This profile is under review</h5></div>

    @endif


        
<link href='https://www.jqueryscript.net/demo/Responsive-Touch-enabled-jQuery-Image-Lightbox-Plugin/dist/simple-lightbox.min.css' rel='stylesheet' type='text/css'>

        <script src="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/js/jquery-2.2.4.min.js"></script>
        <script src="https://codefixup.com/demo/multiselect-dropdown-with-checkbox/js/jquery.multi-select.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <script type="text/javascript"
            src="https://www.jqueryscript.net/demo/Responsive-Touch-enabled-jQuery-Image-Lightbox-Plugin/dist/simple-lightbox.jquery.min.js">
        </script>
        <script>
            $(function() {
                var gallery = $('.gallery a').simpleLightbox({
                    navText: ['&lsaquo;', '&rsaquo;']
                });
            });
        </script>
        
        
        <Style>   #achievement_all .item{
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }</Style>
        <script>
$(document).ready(function() {
  var owl = $("#achievement_all");
  owl.owlCarousel({
    items : 4, //10 items above 1000px browser width
    itemsDesktop : [1000,3], //5 items between 1000px and 901px
    itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
    itemsTablet: [600,3], //2 items between 600 and 0;
    itemsMobile :[600,1], // itemsMobile disabled - inherit from itemsTablet option
  });
});
</script>





<script>
$(document).ready(function() {
  var owl = $("#gallery_all");
  owl.owlCarousel({
  items : 4, //10 items above 1000px browser width
  itemsDesktop : [1000,4], //5 items between 1000px and 901px
  itemsDesktopSmall : [900,4], // 3 items betweem 900px and 601px
  itemsTablet: [600,3], //2 items between 600 and 0;
  itemsMobile :[600,1], // itemsMobile disabled - inherit from itemsTablet option
  
  });
});
</script>

    @endsection
