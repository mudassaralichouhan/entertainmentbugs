@extends('layouts.home')

@php
    use Illuminate\Support\Facades\Route;
    $thumbUrlPath = "";

    if( $videoData->thumb ){
        $thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$videoData->thumb->thumb;
    }
    
	$videoUrlPath = VIDEO_DISPLAY_PATH.$videoData->video;
    $isFollowing = $videoData->user->isFollowedBy() ? true : false;
    $userBasePath = '';
    $userUrlPath = '';
    if(isset($videoData->user->photo)){
        $userBasePath = PROFILE_SMALL_UPLOAD_PATH.$videoData->user->photo;
        $userUrlPath = PROFILE_SMALL_DISPLAY_PATH.$videoData->user->photo;
    }
    
    $curuserBastPath = '';
    $curuserUrlPath = '';

    if(!empty($user->photo)){
        $curuserBastPath = PROFILE_SMALL_UPLOAD_PATH.$user->photo;
        $curuserUrlPath = PROFILE_SMALL_DISPLAY_PATH.$user->photo;
        $userID = $user->id;
    }
    $ipAddress = request()->ip();
    $count = session()->get('videoCount');
    $vthumbBastPath='';
@endphp

@section('meta_tags')
    <meta name="description" content="{{ $videoData->description ?? '' }}" >
    <meta name="keywords" content="{{ $videoData->title ?? '' }}" >
    <meta property="og:title" content="{{ $videoData->title ?? '' }} Vidoes" />
    <meta property="og:url" content="{{ url("watch/".$videoData->video_key) }}" />
    <meta property="og:description" content="{{$videoData->description}}" />
    <meta property="og:image" content="{{$thumbUrlPath}}" />
@endsection
@section('content')


 
 
 <style>
.trending-rewards-tags {margin-top: 0px;clear: both; background: #fff;padding: 10px 10px 0 10px;margin-bottom: 13px;height: 46px;}
.trending-rewards-tags ul{list-style:none;text-align:left;margin:0 0 8px 0;padding:0px;}
.trending-rewards-tags li{display:inline-block;text-align:left;}
.trending-rewards-tags li a{   background: #e5e5e5;border: 0px;color: #000;line-height: 27px;height: 25px;padding: 2px 10px 0 10px;margin-bottom: 1px;font-size: 12px;}
#reward_tags1 .owl-item {margin-right:5px;width: auto !important;}
.tag_head{margin-right: 5px;float: left;width: 10%; line-height: 30px; color:#000;      font-size: 12px;  height: 20px;}
#reward_tags1{float:right;    width: 83%;margin-right: 2.4%;}
#reward_tags1 .owl-pagination {display:block !Important;    height: 1px;}
#reward_tags1 .owl-page span{display:none; }
#reward_tags1 .owl-page.active{opacity:0.6;}
#reward_tags1 .owl-page{ }
#reward_tags1 .owl-page:nth-child(1) {position: absolute;left: -16px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow-gray.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}  
#reward_tags1 .owl-page:nth-child(2) {position: absolute;right: -16px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow-gray.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}
#reward_tags1 .owl-page:nth-child(1):hover {position: absolute;left: -16px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}  
#reward_tags1 .owl-page:nth-child(2):hover {position: absolute;right: -16px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}
</style>      



<style>


#video_share_option{width: 200px;float: right;margin-right: -21px; position: relative;z-index: 7;margin-top: -40px;}
.social a.whatsapp-i {background-color: #4FCE5D;}
.social a.instagram {background-color: #d62976;padding-top: 7px;}
.social a{border-radius:0}
#video_share_option a:not(:last-child) {margin-right: 6px;}

.single-video .author .sv-name .c-sub {
    padding-top: 2px;
    padding-bottom: 13px;
}





 #right_b314 .ads-cont {
    margin-top: 0px !important;
}

#recom .videoitem button {
    background: #1769d4;
    border: 0;
    color: #fff;
    font-size: 9px;
    padding: 5px 8px 1px 9px;
    position: absolute;
    bottom: 32px;
    right: 18px;
}
.user_heading-title-bg{display:block !Important;}
.videoitem .user .shortnamevd{background: #28b47e !important;
    border-radius: 100px;
    width: 30px;
    color: #fff;
    height: 30px;
    text-align: center;
    margin-bottom: 7px;
    line-height: 33px;
    font-weight: bold;
    font-size: 14px;}
    
.single-video .custom-tabs .tabs-panel a i.unliked {color: #637076 !important;}
.single-video .custom-tabs .tabs-panel a i.dislikeinactive {color: #637076 !important;}
.single-video .custom-tabs .tabs-panel a i.watchlaterinactive {color: #637076 !important;}
#live-video-user-list-home .cb-header{margin-bottom:0px;border:0px;}
.list-inline li.active a {color: #f72e5e !important;}
#single_tag-Video_display .similar-v.single-video.video-mobile-02{clear:both; overflow:hidden; margin-bottom:20px;padding-left: 9px;}
#single_tag-Video_display  .col-lg-12.videoitem{    width: 25%;padding:0 7px;float:left;}
#single_tag-Video_display .single-video .videoitem .v-img{    width: 100%;padding:0 0px;float:none;height:85px;margin:0 0 0px 0;}
#single_tag-Video_display .v-desc a {font-size: 13px;line-height: 15px;display: block;height: 30px;overflow: hidden;margin-bottom: 3px;}
#single_tag-Video_display  .single-video .videoitem .user {width: 40px;clear: both;overflow: hidden;position: absolute;right: 1px;bottom: -13px;z-index: 99;}.single-video .h-video {margin-bottom: 15px;}
.info-all-new{display:none;}
.single-video .adblock{padding-top:0px;}
video{    height: 395px !important;  }
#only-mobile-video-details{display:none;}
.sv-views-progress {top: 18px;}
 
.sv-name span{text-align:left;display:block;}
.similar-v.single-video.video-mobile-02 .h-video.row{margin-bottom:0px;}
.similar-v.single-video.video-mobile-02 .col-sm-12.col-xs-6{padding:0 5px;}
#ght{margin-top:20px;}

#desktop-social{padding:10px 0 0 0;}
#desktop-social .social a{float: right !important;margin: 0 0 0 7px !important;}
#desktop-social .social p{float: left !important;font-size: 12px;line-height: 30px;}
#video_up_preview .form-group {margin-bottom: 6px;}




/* new white bg css  start ---------------------1056--------------------------------- */
body.light .content-wrapper {background-color: #edecec}
.single-video .custom-tabs .tabs-content > div.active {display: block !important;background: #fff;}
.single-video .custom-tabs .tabs-panel > a.active {background-color: #fff;}
.single-video .custom-tabs .tabs-panel {background: #fff;}
#recom .b-video.last-row{     position: relative;
    min-height: 74px;
    background: #fff;
    margin-bottom: 11px;
    border-radius: 0 10px 10px 0;}
#recom .v-desc a {    width: 50%;      font-size: 13px;
    line-height: 17px;
    display: block;
    font-weight: 400;
    padding-right: 20px;
    margin-bottom: 7px;
    height: 34px;
    overflow: hidden;
    margin-top: 1px;}
#recom .videoitem .user {    width: 40px;right: -24px;bottom: 40px;}
#recom .v-views {padding-bottom: 0px;font-size: 11px;}
#ght {margin-top: 20px;background: #fff;padding: 21px;}
#recom .videoitem{padding:0px;}
 
#recom{margin-top:-17px;}
.similar-v.single-video.video-mobile-02 .videoitem{margin-bottom:20px;}
#similar-b-top {margin: 0;padding: 13px 0 16px 0px;}
footer{background:#fff}

/* new white bg css end --------------------------1056---------------------------- */
#single_tag-Video_display .similar-v.single-video.video-mobile-02 {background: #fff;padding: 20px 25px 0 25px;}

 @media  only screen and (max-width: 767px) {
     
/* new white bg css  start ------------------------1056------------------------------ */ 
#single_tag-Video_display .similar-v.single-video.video-mobile-02 {background: #fff;padding: 0px;}
body.light .content-wrapper {background-color: #fff}
#recom .b-video.last-row {position: relative;min-height: 100px;margin-bottom: 11px;border-radius: 0 0;background: #f4f3f3;}
#recom .videoitem .user {width: 40px;right: -4px;bottom: -1px;}
#recom .videoitem .v-img {width: 155px; margin-right: 8px;height: 100px;}
#recom .v-desc a{    font-weight: 500;}
#recom-head{background: #f4f3f3;margin: 0 0 20px 0 !important;padding: 19px 15px 15px 15px !important;}
#recom{margin-top:0px;}
/* new white bg css  End ----------------------------1056-------------------------- */

.info-all-new a{width:31.3%;margin:0 0.7%;float:left}
.info-all-new span{width:31.3%;margin:0 0.7%;float:left}
.info-all-new span a{width:auto;margin:0 0.0%;float:none}
.user a.user_img {
  margin: 0 10px 5px 0; 
}


.b-video .v-views {font-size: 11px;}
#single_tag-Video_display .v-desc a {font-size: 12px;}
#only-mobile-more-video{display: block !important;width: 100%;padding: 4px 0 0 0;height: 90px;margin-bottom: 0;overflow: hidden;border-bottom: solid 1px #eceff0;}
#only-mobile-more-video .icons_b {position: relative;float: left;width: 54px;height: 82px;margin-bottom: 5px;margin-right: 2px;margin-left: 2px;overflow: hidden;}
#only-mobile-more-video .icons_b.first1{width: 46px;}
#only-mobile-more-video .icon_small a {margin-bottom: 0;}
#only-mobile-more-video .icons_b.first1{height: 53px}
#only-mobile-more-video .icons_b.first1 a{color: #f72e5e; padding: 17px 0 0 0;border-radius: 0px;height: 42px;}

#only-mobile-more-video .icons_b span{position: relative;
    left: 4px;
    color: #000;
    opacity: 1;
    padding: 1px 0 0 0;
    text-align: center;
    z-index: 1;
    background: none !important;
    display: block;
    width: 95%;
    line-height: 14px;
    height: 17px;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;}


.single-video .videoitem .v-img {float: left;width: 45%;margin-right: 15px;height: 87px;}
.single-video .videoitem .user {bottom: -16px; right: -7px;}
#recom .col-lg-12{padding:0px;}
.single-video .b-video.last-row {position: relative;min-height: 87px;}
#single_tag-Video_display  .col-lg-12.videoitem{    width: 50%;padding:0 7px;float:left;}
.video-mobile-02 > .row {display: block;}
#single_tag-Video_display .single-video .videoitem .v-img {height: 100px;}
#single_tag-Video_display .single-video .videoitem .user {  right: 0px;bottom: -3px;z-index: 99;}

#single_tag-Video_display .single-video .videoitem:nth-child(even) .user {  right: 6px;bottom: -3px;z-index: 99;}

#single_tag-Video_display .b-video .v-views {
    font-size: 11px;
}
#taag0 form{display: inline-block;}
#only-mobile-video-details .sv-name {margin: 5px 0 0 0;padding: 0 0 0 0;float: left;width: 80%;}
.single-video .custom-tabs .tabs-content > div.active {display: none !important;} 
#first_ad{display: none !important;} 
.single-v-footer-switch {padding: 11px 0 0 0;}
.tabs-panel.only_mobile{display: none !important}
.similar-v.single-video.video-mobile-02 .h-video.row {margin-bottom: 15px; padding: 0 10px;}
.container .sv-video video {height: auto !important; }
#only-design-tab-in-mob .acide-panel{height:40px;width:100%;}
#only-design-tab-in-mob .tabs-panel a{margin:0px !Important;width:25%;}
#only-design-tab-in-mob .tabs-panel {border-top: solid 1px #eceff0;padding:13px 0 0 0 }
.single-video .content-block .cb-header {margin-bottom: 2px;}
#no-mb091{display:none;}
#only-mobile-video-details{display:block !important;padding: 7px 0 0 0;}
.single-video.light .col-lg-4.col-xs-12.col-sm-12.hidden-xs{display:block! important;    margin: 26px 0 0 0;}
.single-video.light .col-lg-4.col-xs-12.col-sm-12.hidden-xs .col-lg-5.col-sm-5{width:50%; float:left;}
.single-video.light .col-lg-4.col-xs-12.col-sm-12.hidden-xs .col-lg-7.col-sm-7{width:50%; float:left;padding: 0;}
#ght{display:none;}

#only-mobile-video-details{display:block !important;} .sv-video img.sv-avatar {width: 50px;border-radius: 100px;height: 50px;object-fit: cover;}
#only-mobile-video-details .sv-name{margin: 5px 0 0 0; padding: 0 0 0 0;}
#only-mobile-video-details h1 {font-size: 16px !important;padding: 1px 0 0 0;} 
#only-mobile-video-details a { font-size: 12px !important;}
#only-mobile-video-details span { font-size: 11px !important;color:gray;}
#only-mobile-video-details span a{ font-size: 11px !important;color:gray;}
#only-mobile-video-details .img-pi{   margin-right: 10px; }
#only-mobile-video-details #video_up_preview {border-top: solid 1px #eceff0;padding: 15px 0 0;    margin-top: 8px}     
#only-mobile-video-details #video_up_preview .form-group label{font-size:12px;}
#only-mobile-video-details #video_up_preview span{font-size:12px;}
#only-mobile-video-details .form-group  a{font-size: 12px !important;    margin-right: 0px;}
#only-mobile-video-details .sv-tags small {padding: 2px 10px 0 10px;}
#only-mobile-video-details .form-group {margin-bottom: 6px;}
.info-all-new{display:block;}
.single-video .h-video {margin-bottom: 15px;}    
.sv-name span{text-align:left;display:inline-block;}    
#no-view-on-mobile{display:none;}
 #taag0 input{background: #e5e5e5;border: 0px;color: #000;line-height: 27px;height: 25px;padding: 1px 10px 0 10px;margin-bottom: 1px;}
 
 }

</style>
<img src="{{$thumbUrlPath}}" style="width:1px;height:1px;display:none">
<div class="single-video light content-wrapper">
   <div class="container">
       <div class="row">
           <div class="col-lg-8 col-xs-12 col-sm-8"> 
	
   @if(!$videoData->isDeleted && !$videoData->user->isDeleted)
                <div class="sv-video right_big_video">


                    <!--skip ads start-->
                        <div id="ads_skip_all_content" class="video-ads_piopup" style="position:absolute; width:100%; height:100%;left:0px; top:0;z-index:99">
                            <a href="javascript:void(0)" class="skip-091">SKIP ADS </a>
                            <div data-ad-block data-ad-type='ad' data-ad-mode='video'></div>
                        </div>
                    
                    
                    
                    <Style>
                        #ads_skip_all_content video {
                            min-height: 395px !important;
                        }

                        .video-ads_piopup #ads-video-img {
                            width: 100% !important;
                        }

                        .video-ads_piopup #ads-video-img-content {
                            width: 48%;
                            position: absolute !Important;
                            bottom: 112px !Important;
                            left: 7px !Important;
                            background: rgba(255, 255, 255, 0.6) !Important;
                            border-radius: 3px !Important;
                            padding: 7px 10px 9px 10px !Important;
                        }

                        .video-ads_piopup #ads-video-img-content .ne21 {
                            float: right !important;
                            font-size: 11px !important;
                            margin: 4px 5px 0 0 !important;
                            color: #fff !important;
                            background: #f72e5e !important;
                            padding: 7px 15px 1px 15px !important;
                            margin-top: -5px !important;
                        }

                        .video-ads_piopup #ads_name {
                            padding: 5px 8px 0 8px !Important;
                            background: #edecec;
                            right: 10px !Important;
                            top: 7px !Important;
                        }

                        .video-ads_piopup #ads-video-img-content h6 {
                            width: 91% !Important;
                            padding: 2px 5px 2px 0 !important
                        }

                        .video-ads_piopup a.skip-091 {
                            z-index: 999;
                            background: #000;
                            color: #fff;
                            padding: 7px 20px 2px 20px;
                            display: block;
                            position: absolute;
                            right: 0px;
                            top: 265px;
                        }

                        .video-ads_piopup #ads-video-img-content p {
                            font-size: 9.5px !important;
                            line-height: 12px !important;
                            padding-right: 10px !important;
                            margin: 0 0 7px 0 !important;
                            margin-top: -4px !important;
                            height: 13px !important;
                            overflow: hidden !important;
                        }
                    </Style>
                    <!--skip ads end-->


                    <img src="https://www.entertainmentbugs.com/public/img/logo_footer.png" style="position:absolute; right:10px; top:5px; width:35px">



                    <video controls autoplay id="mainVideo" controlsList="nodownload" style="width:100%;height:auto;">

                        <source id="mainVideo" src="{{$videoUrlPath}}" type="video/mp4">
                        </source>
                    </video>

                    <h1 id="no-mb091" style="padding:19px 0 6px 0"> {{$videoData->title}} </h1>

                    <div id="only-mobile-video-details">
                        <div class="author-head">
                            <a class="img-pi" href="{{ user_url($videoData->user->slug) }}">
                                @if(file_exists($userBasePath))
                                {{HTML::image($userUrlPath,null,['class'=>'sv-avatar'])}}
                                @else
                                {{HTML::image('public/img/ava2.png',null,['class'=>'sv-avatar'])}}
                                @endif
                            </a>
                            <div class="sv-name">
                                <!--<div><a target="_blank" href="{{ user_url($videoData->user->slug) }}">{{$videoData->user->name}}</a>  </div>-->
                                <h1> {{$videoData->title}} </h1>
                                <div style="color:gray">
                                    <a style="color:gray" href="{{ user_url($videoData->user->slug) }}">{{$videoData->user->name}}</a> -
                                    <span style="color:gray">{{$videoData->total_views_count}} views</span>
                                </div>


                            </div>
                        </div>



                        <div id="video_up_preview">
                            <div class="col-lg-12" id="">
                                <div class="form-group">
                                    <label for="e2">About:</label>
                                    <span>{{$videoData->description}}</span>
                                </div>
                            </div>
                            <div class="col-lg-12" id="taag0">
                                <div class="form-group">
                                    <label for="e1">Video Tags:</label>

                                    @if(!empty($videoTags))
                                    @foreach($videoTags as $slug => $tag)
                                    <form action="{{URL::to('search')}}" method="post">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" class="form-control" style="margin-bottom: 0px;" name="searchdata" aria-describedby="sizing-addon2" value="{{ $tag }}">
                                        <input type="hidden" name="searchType" id="searchType" value="video" />
                                        <input type="submit" value="{{ $tag }}">
                                    </form>
                                    @endforeach
                                    @endif

                                </div>
                            </div>


                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="e3">Language:</label>
                                    <span>{{$videoData->language}}</span>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="e3">Category:</label>
                                    @if(!empty($videoCategories))
                                    <span>
                                        @php $tc = count($videoCategories); $t = 0; @endphp
                                        @foreach($videoCategories as $slug => $category)
                                        <a href="{{URL::to('category/'.$category)}}">{{$category}}</a>@php $t++; echo (($tc != $t)?',&nbsp;':""); @endphp
                                        @endforeach
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="e3">Release Date:</label>
                                
         <span>{{ \Carbon\Carbon::parse($videoData->created_at)->diffForHumans() }}</span>
 </div>
                            </div>



                            <div class="clear"></div>


                        </div>



                    </div>
                </div>
                @else
                <div class="under-review" style="width:91%;float:left; margin-bottom:10px">
                   <img src="https://www.entertainmentbugs.com/public/img/video_review.png" style="width:100%">
                </div>

                @endif
                
                  		<div class="icon_small">
<div class="icons_b first1"><a href="#" class="user_imgs">MORE VIDEOS</a>	</div> 
               @if($moreVideos)
                    @foreach ($moreVideos as $video)
                        @php
                            $vvideoUrl = ($video->isRewardVideo ? "watch-reward/" : "watch/"). $video->video_key;
                        @endphp
                        @if(!empty($video->thumb))
                            @php
                                $vthumbBastPath = $video->thumb->thumb;
                            @endphp
                        @endif
                       @if(!empty($vthumbBastPath))
                            <div class="icons_b first2"><a href="{{URL::to($vvideoUrl)}}" class="user_img"><img src="{{ asset('public/uploads/video/thumbs/'.$vthumbBastPath) }}" /></a> <span>{{ $video->title }}</span> </div>
                        @else
                            <div class="icons_b first2"><a href="{{URL::to($vvideoUrl)}}" class="user_img"><img src="{{ asset('public/uploads/video/thumbs/'.$vthumbBastPath) }}" /></a> <span>{{ $video->title }}</span> </div>	
                        @endif
                    @endforeach
                @endif

				</div>   
                 <div class="clear"></div>
               <div class="info">
                   <div class="custom-tabs" id="only-design-tab-in-mob">
                       <div class="tabs-panel">
                            <a href="#"  data-tab="tab-1" class="active hidden-xs">
                            <i class="cv cvicon-cv-about" data-toggle="tooltip" data-placement="top" title="About"></i>
                               <span>About</span>
                           </a>
                           @if (!empty($videoData->is_watched_video_count) && $videoData->is_watched_video_count > 0)
                            watched
                           @endif
                           <div class="acide-panel"> 
<a href="{{$videoData->video_key}}" style="font-size:15px;line-height:25px;" id="watchlaterCurrentvideo"><i class="cv cvicon-cv-watch-later" data-toggle="tooltip" data-placement="top" title="" data-original-title="Watch Later"></i><span id="watchLaterCount">{{ $videoWatchLaterCount }}</span></a>
<a href="{{$videoData->video_key}}" style="font-size:15px;line-height:25px;" id="likedvideo"><i class="fa fa-thumbs-up" data-toggle="tooltip" data-placement="top" title="" style="margin-right: 5px;" data-original-title="Liked"></i> <span id="likedCount">{{ $videoLikeCount }}</span></a>
<a href="{{$videoData->video_key}}" style="font-size:15px;line-height:25px;" id="dislikevideo"><i class="fa fa-thumbs-down" data-toggle="tooltip" data-placement="top" title="" style="margin-right: 5px;" data-original-title="Unlinked"></i><span id="unLikedCount">{{ $videoDisLikeCount }}</span></a>
<a data-toggle="modal" href="#myModal" style="font-size:15px;line-height:25px;" id="videoMarkflag"><i class="cv cvicon-cv-flag" data-toggle="tooltip" data-placement="top" title="" data-original-title="Flag"></i><span id="videoFlag"></span></a> 
                            
                            
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">
                                
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Comments</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                              <label for="exampleFormControlTextarea1">Write Video Flag Comments:</label>
                                              <input type="hidden" id="flagKey" value="{{$videoData->video_key}}"/>
                                              <textarea class="form-control" id="flagcomments" rows="3"></textarea>
                                            </div>
                                          </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" id="flagcommentbtn" class="btn btn-success">Submit</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                
                                </div>
                            </div>
                        </div>
                       </div>
                       <div class="tabs-panel only_mobile">
                           <a href="#" data-tab="tab-2">
                               <i class="cv cvicon-cv-share" data-toggle="tooltip" data-placement="top" title="Share"></i>
                               <span>Share</span>
                           </a>
                       </div>
                       <div class="clearfix"></div>
                       <!-- BEGIN tabs-content -->
                       <div class="tabs-content">
                           <!-- BEGIN tab-1 -->
                           <div class="tab-1">
                               <div class="col-xs-12" style="width:100%">
                                   <div class="author" style="box-shadow: none;border: 0px;padding: 10px 0 10px 0px;margin: 0;">
                                        <div class="sv-views">
                                           <div class="sv-views-count">
										   {{$videoData->total_views_count}} views
                                           </div>
                                           <div class="sv-views-progress">
                                               <div class="sv-views-progress-bar"></div>
                                           </div>
                                           <br>
                                                <div id="desktop-social">
                                                <div class="addthis_inline_share_toolbox_n0k8"></div>
                                                
                                           </div>
                                       </div>
                                       
                                       <div class="author-head">
                                           <a class="atr" target="_blank" href="{{ user_url($videoData->user->slug) }}">
                                                @if(file_exists($userBasePath))
                                                        {{HTML::image($userUrlPath,null,['class'=>'sv-avatar'])}}
                                                @else
                                                    {{HTML::image('public/img/ava2.png',null,['class'=>'sv-avatar'])}}
                                                @endif
                                            </a>
                                           <div class="sv-name">
                                               <div><a target="_blank" href="{{ user_url($videoData->user->slug) }}">{{$videoData->user->name}}</a> .
                                               {{ $userVideoCount }} Videos</div>
                                              
                                               <div class="c-sub hidden-xs" id="followunfollowbtn">
                                                 
                                               </div>   <div class="clear"></div> 
                                              
                                               <span class="comment more">{!! $videoData->description !!}</span>
                                               <Style>.author .morecontent span{display:none}</Style>
                                               
                                           </div>
                                       </div>
                                       <div class="author-border"></div>
                                   
                                       <div class="clearfix"></div>
                                   </div>
                                   <div id="video_up_preview">
                                       <div class="col-lg-12" id="about_mbl">
                                           <div class="form-group">
                                               <label for="e1">Video Tags:</label>
                                               <p class="sv-tags">
											    @if(!empty($videoTags))
													@foreach($videoTags as $slug => $tag)
													  <form action="{{URL::to('search')}}" method="post">
                                                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                           <input type="hidden" class="form-control" name="searchdata" aria-describedby="sizing-addon2" value="{{ $tag }}">
                                                           <input type="hidden" name="searchType" id="searchType" value="video"/>
                                                           <input type="submit" value="{{ $tag }}">
                                                       </form>
													@endforeach
                                                @endif 
                                               </p>
                                           </div>
                                       </div>
                                       <div class="col-lg-12">
                                           <div class="form-group">
                                               <label for="e3">Language:</label>
                                               <span>{{$videoData->language}}</span>
                                           </div>
                                       </div>
                                       <div class="col-lg-12">
                                           <div class="form-group">
                                               <label for="e3">Category:</label>
											   @if(!empty($videoCategories))
												   <span>
												   @php $tc = count($videoCategories); $t = 0; @endphp
												   @foreach($videoCategories as $slug => $category)
													<a href="{{URL::to('category/'.$category)}}">{{$category}}</a>@php $t++; echo (($tc != $t)?',&nbsp;':""); @endphp
												   @endforeach
												   </span>
											   @endif
                                           </div>
                                       </div>
                                       <div class="col-lg-12">
                                           <div class="form-group">
                                               <label for="e3">Release Date:</label>
                                              <span>{{ \Carbon\Carbon::parse($videoData->created_at)->diffForHumans() }}</span>
                                           </div>
                                       </div>
                                       
                                         <div class="social" id="video_share_option">
<a href="" style="color: #000;font-size: 12px;margin: 3px 5px 0 0;"> Share:</a>
<!--<a href="" id="facebook" class="facebook"><i class="fa fa-facebook"></i></a>-->
<!--<a href="" id="twitter" class="twitter"><i class="fa fa fa-twitter"></i></a>-->
<!--<a href="#" class="instagram"><i class="fa fa-instagram"></i></a>-->
<!--<a href="#" class="whatsapp-i"><i class="fa fa-whatsapp" style="line-height: 17px;"></i></a>               -->
     <!-- AddToAny BEGIN -->
<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
<a class="a2a_button_facebook"></a>
<a class="a2a_button_twitter"></a>
<a class="a2a_button_whatsapp"></a>
</div>
<script async src="https://static.addtoany.com/menu/page.js"></script>
<!-- AddToAny END -->                        
                                </div>
                                
                                
                                   </div>
                               </div>
                           </div>
                           <!-- END tab-1 -->
                       </div>
                       <!-- END tabs-content -->
                   </div>
    <script>
        var isUserLogin = "{{ Session::get('user_id') }}";
        if(!isUserLogin){
              $("#mainVideo").bind("timeupdate", function(){
                var currentTime = this.currentTime;
                if (currentTime > 225){
                    window.location.href = "{{ url('/login') }}"
                }
            });
        }

    </script>                

                
            <style>
                .info-all-new>span{
                    background: #f72e5e;
                    padding: 5px 5px;
                    display: inline-block;
                    margin-right: 3px
                }
                .info-all-new>span>a{
                    color: #fff;
                }
            </style>
<div class="clear"></div>
                  
                 <div class="info-all-new" style="text-align: center;clear: both;overflow: hidden; border-bottom: solid 1px #eceff0; margin: 3px 0 10px 0;padding: 10px 0 15px 0;">
                        <a href="#" class="flw" style="background: #f72e5e !important;padding: 7px 20px 4px 20px;color: #fff;display: inline-block; "><i class="fa fa-share-alt" style="margin-right: 5px;"></i> Share</a>
                        <span>
                            @if ( Session::get("user_id") && $videoData->user->isFollowedBy() )
                                <a data-do="unfollow" data-user-id="{{ $videoData->user->id }}" class="flw" style="color: #fff;"><i class="fa fa-solid fa-heart" style=" margin-right: 5px;"></i> Following</a>
                            @else
                                <a data-do="follow" data-user-id="{{ $videoData->user->id }}" class="flw" style="color: #fff;"><i class="fa fa-heart-o" style=" margin-right: 5px;"></i> Follow</a>
                            @endif
                        </span>
                        
                         <a href="{{ user_url($videoData->user->slug) }}" class="flw" style="background: #f72e5e !important;padding: 7px 24px 4px 24px; color: #fff;display: inline-block;"><i class="fa fa-user" style=" margin-right: 5px;"></i> Profile</a>
                          
                </div> 
                
                             
 <div class="clear"></div>  
                   
                         
                
                    <div class="icon_small" style="display:none" id="only-mobile-more-video" >
                    <div class="icons_b first1"><a href="#" class="user_imgs">MORE VIDEOS</a>	</div>  
                    
                    			<div class="span12" id="crousal_wra">		
								 <div id="owl-demo" class="owl-carousel21">                                                                        
            								 
               @if($moreVideos)
                    @foreach ($moreVideos as $video)
                        @php
                            $vvideoUrl = $video->video_key;
                        @endphp
                        @if(!empty($video->thumb))
                    
                            @php
                                $vthumbBastPath = $video->thumb->thumb;
                            @endphp
                        @endif
                       @if(!empty($vthumbBastPath))
                          <div class="item">    <div class="icons_b first2"><a href="{{URL::to('watch/'.$vvideoUrl)}}" class="user_img"><img src="{{ asset('public/uploads/video/thumbs/'.$vthumbBastPath) }}" /></a> <span>{{ $video->title }}</span> </div>	</div>   
                        @else
                             <div class="item">  <div class="icons_b first2"><a href="{{URL::to('watch-reward/'.$vvideoUrl)}}" class="user_img"><img src="{{ asset('public/uploads/video/thumbs/'.$vthumbBastPath) }}" /></a> <span>{{ $video->title }}</span> </div></div>	
                        @endif
                    @endforeach
                  @endif 
				</div>   
                	</div>   
                	</div>   
                   

 <div class="clear"></div>  
 
 <!--   <div class="seven_twenty_eight">-->
 <!--<div data-ad-block data-ad-type='ad' data-ad-mode='video' data-ad-size='60'></div>-->
 
 <!--</div>   -->


 <Style>
     .seven_twenty_eight_video #ads_name{top: 100px !Important}
     .seven_twenty_eight_video #ads-video-img{width:30% !important;}
     .seven_twenty_eight_video #ads-video-img-content{width:68% !Important;}
 </Style>
 
 
 
  
       <div class="seven_twenty_eight_video" style="position: relative;margin-top: 20px;"> <div data-ad-block data-ad-type='ad' data-ad-mode='video'></div> </div>

 
 
 
                   <!-- similar videos -->
                   
                   <div class="caption hidden-xs" id="similar-b-top">
                       <div class="left">
                           <a href="#">Similar Video's</a>
                       </div>
                       <div class="clearfix"></div>
                   </div>
                   
                   
                   <div class="single-v-footer" id="single_tag-Video_display">
                       <div class="single-v-footer-switch">
                           <a href="javascript:void(0)" class="active" data-toggle=".similar-v">
                               <i class="cv cvicon-cv-play-circle"></i>
                               <span>Similar Videos</span>
                           </a>
                           <a href="javascript:void(0)" data-toggle=".comments">
                               <i class="cv cvicon-cv-comment"></i>
                               <span> Comments</span>
                           </a>
                       </div>
                       <div class="similar-v single-video video-mobile-02">
                           <div class="row">
							@if(!empty($tagWiseVideo))
								@foreach($tagWiseVideo as $videos)
									@include('elements.post.video_3')							
								@endforeach   
							@endif						   
						   </div>
                      
                           <div style="clear:both; overflow:hidden"> </div>
                           
					   </div>
                       <!-- END similar videos -->
                       
                       
                      <div class="seven_twenty_eight" style="position: relative;margin-top: 20px;">
        <div data-ad-block data-ad-type='ad' data-ad-mode='image' data-ad-size='728x90'></div></div>
    
                       
                       
                       
                       
                       
                       
                       @if($user)
                           <!-- comments -->
                           <div class="comments" id="ght">
                               <div class="reply-comment">
                                   <div class="rc-header"><i class="cv cvicon-cv-comment"></i> 
    							   <span class="semibold">{{ $videoData->total_comments_count }}</span> Comments</div>
                                   <div class="rc-ava">                                   
    								<a target="_blank" href="{{ user_url($user->slug) }}">
    								    @if(file_exists($curuserBastPath))
                                                {{HTML::image($curuserUrlPath,null,['class'=>'sv-avatar'])}}
                                        @else
                                            {{HTML::image('public/img/ava2.png',null,['class'=>'sv-avatar'])}}
                                        @endif 
    								</a>                              
    							   </div>
                                   <div class="rc-comment">
                                       <form method="post" action="{{ route('comment.add') }}" id="commentfrm">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                           <textarea rows="3" name="comment_body" placeholder="Share what you think?"></textarea>
                                           <input type="hidden" name="video_id" value="{{ $videoData->id }}" />
                                           <input type="hidden" name="model" value="Video" />
                                           <button type="submit">
                                               <i class="cv cvicon-cv-add-comment"></i>
                                           </button>
                                        </form>
                                   </div>
                                   <div class="clearfix"></div>
                               </div>
                               <div class="comments-list">
                                   <div class="cl-header">
                                       <div class="c-nav">
                                           <ul class="list-inline">
                                               <li><a href="#" class="active">Popular <span class="hidden-xs">Comments</span></a></li>
                                           </ul>
                                       </div>
                                   </div>
                                   <!-- comment -->
                                   <div id="comments">   
                                     @include('comment.video._comment_replies', ['comments' => $videoData->comments, 'video_id' => $videoData->id])
                                   </div>
                                   <!-- comment -->
                                   
                               </div>
                           </div>
                           <!-- END comments -->
                       @else
                           <!-- comments -->
                           <div class="comments">
                               <div class="reply-comment">
                                   <div class="rc-header"><i class="cv cvicon-cv-comment"></i> 
    							   <span class="semibold">{{ $videoData->total_comments_count }}</span> Comments</div>
                                   <div class="rc-ava">                                   
    								<a target="_blank" href="#">
                                            {{HTML::image('public/img/ava2.png',null,['class'=>'sv-avatar'])}}
    								</a>                              
    							   </div>
                                   <div class="rc-comment">
                                       <form method="post" action="{{ route('comment.add') }}" id="commentfrm">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                           <textarea rows="3" name="comment_body" placeholder="Share what you think?"></textarea>
                                           <input type="hidden" name="video_id" value="{{ $videoData->id }}" />
                                           <input type="hidden" name="model" value="Video" />
                                           <button type="submit">
                                               <i class="cv cvicon-cv-add-comment"></i>
                                           </button>
                                        </form>
                                   </div>
                                   <div class="clearfix"></div>
                               </div>
                               <div class="comments-list">
                                   <div class="cl-header">
                                       <div class="c-nav">
                                           <ul class="list-inline">
                                               <li><a href="#" class="active">Latest <span class="hidden-xs">Comments</span></a></li>
                                           </ul>
                                       </div>
                                   </div>
                                   <!-- comment -->
                                   <div id="comments">   
                                     @include('comment.video._comment_replies', ['comments' => $videoData->comments, 'video_id' => $videoData->id]);
                                   </div>
                                   <!-- comment -->
                                   
                               </div>
                           </div>
                           <!-- END comments -->
                       @endif
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                       
                           <div id="multiple_ads_banner">
    <div class="post" data-photo-id="160" data-loaded="1">
                    <div class="fw-header">
                        <div class="photo-head" style="border:0px;padding-bottom: 0px;margin-bottom: -13px;">
                            <div class="users" style="display: flex; align-items: center; position:relative;">
                            <a href="https://www.entertainmentbugs.com/userprofile/samanta-official" data-photo="1683968979.png" class="user_img">
                            <img src="https://www.entertainmentbugs.com/public/uploads/users/small/1683968979.png" alt="User Icon">
                            </a>
                          <a href="https://www.entertainmentbugs.com/userprofile/samanta-official" style="width:auto; margin-left: 5px">Samanta</a>
                           <span style="background: #f72e5e !important;color: #fff;position: absolute;right:10px;width: 55px;display:block;text-align:center;top:0px;font-size: 13px;line-height: 20px;padding: 4px 0 0px 0px;">ADS</span>
                           </div>
                        </div> 
                       <div class="photo-middle_botttom" style="padding-top: 1px; position:relative">
                         <h2 data-photo-title="photo_title" style="font-size: 13.5px;margin:3px 0px 3px 0;display:block;font-weight: normal;color: #000;line-height: 17px;">
                        Samantha Ruth Prabhu is an Indian actress who primarily works in Telugu and Tamil films. One of the highest-paid South Indian actresses, she is the recipient of several accolades</h2>
                    </div> 
                    
                  			 <div id="owl-demo" class="owl-carousel_ads">                                                                        
            								 
                
                    
                    <div class="item">   
                <div class="ads-banner-slider" style="clear:both; overflow:hidden"> 
                      <div class="ads-banner-slider_inside">
                        <img src="https://www.entertainmentbugs.com/public/uploads/photo/cb46931e4670a54f34f22e4a960f0a0c.png">
                        <h2>Dance India Dance</h2>
                        <p>Join our latest batch</p>
                        <a href="">SHOP</a>
                     </div>  
                </div> 
                  </div>  
                  <div class="item"> 
                 <div class="ads-banner-slider" style="clear:both; overflow:hidden"> 
                      <div class="ads-banner-slider_inside">
                        <img src="https://www.entertainmentbugs.com/public/uploads/photo/cb46931e4670a54f34f22e4a960f0a0c.png">
                        <h2>Dance India Dance</h2>
                        <p>Join our latest batch</p>
                        <a href="">SHOP</a>
                     </div>  
                </div> 
                  </div>  
                 <div class="item"> 
                 <div class="ads-banner-slider" style="clear:both; overflow:hidden"> 
                      <div class="ads-banner-slider_inside">
                        <img src="https://www.entertainmentbugs.com/public/uploads/photo/cb46931e4670a54f34f22e4a960f0a0c.png">
                        <h2>Dance India Dance</h2>
                        <p>Join our latest batch</p>
                        <a href="">SHOP</a>
                     </div>  
                    </div> 
                  </div>  
              	</div>  
            </div>
         </div> 
    </div>





                   </div>
               </div>
    
    
    
    
    <style>
.owl-carousel_ads .owl-item{width:240px !Important; margin-right:20px !Important;}
.owl-carousel_ads{background:#fff;}
.ads-banner-slider{background:#f0f0f0;}
.ads-banner-slider_inside{float:left;width:100%;background:#fff; padding:10px;position: relative; border:1px solid #f1f1f1;}
.ads-banner-slider_inside img{width:100%;}
.ads-banner-slider_inside h2{    text-align: left; font-size: 16px;color: #000;margin: 10px 0 1px 0;padding: 0px;}
.ads-banner-slider_inside p{color:#000; font-size:13px;text-align: left; margin:0px;padding:0px;}
.ads-banner-slider_inside a {background: #28b47e !important;color: #fff;text-align: Center;padding: 5px 17px 2px 17px;display: block;float: right;position: absolute;right: 13px;bottom: 14px;}
#multiple_ads_banner .users a img {width: 35px;height: 35px;border-radius: 100px;margin-right: 5px;object-fit: cover;}
#multiple_ads_banner {margin-top:20px;}
</style>
    
    
<!--ads start-->               
<style>
.ads-cont{margin-top:15px;border:solid 1px #eceff0}
.seven_twenty_eight #ads-banner-img{width:100% !Important}
.seven_twenty_eight #ads-banner-img img{border-bottom: 1px solid #edecec;}
.seven_twenty_eight #ads-banner-img-content{width:100% !Important;padding:6px 10px !important;}
.seven_twenty_eight .ads-trigger-btn{width: 13% !Important; right: 8%  !Important;}
.seven_twenty_eight h6{padding: 0px 5px 2px 0 !important;font-size:10px !Important;}    
.seven_twenty_eight  #ads-banner-img-content p{font-size:9px !Important;}
</style>   
<div class="seven_twenty_eight">
<div data-ad-block data-ad-type='ad' data-ad-mode='image' data-ad-size='728x90'></div>
</div>
<!--ads end-->    

             
 <!--<div data-ad-block data-ad-type='ad' data-ad-mode='video' data-ad-size='60'></div>-->
 
               <div class="content-block head-div head-arrow visible-xs" style="display:none !important">
                   <div class="head-arrow-icon">
                       <i class="cv cvicon-cv-next"></i>
                   </div>
               </div>
           </div>

           <!-- right column -->
           <div class="col-lg-4 col-xs-12 col-sm-4" id="right_b314">
 
 
 
 
<!--<div id="ads-banner-1080" style="background:#fff;margin-bottom:15px">-->
<!--    <div id="ads-banner-img" style="width: 40%;float:left;">-->
<!--    <a href="" target="_blank"><img src="https://lh6.googleusercontent.com/proxy/f1YQuN1h9RUoiKUzpPOXBIbMseqm0pqoHB348qgS0_swF_5Q0OZ1RuovnhJKG5KbQ_pBZ-gYUkg8fErpcVg-Sl2MYXOJ-uHc3e45CoJOHqFTEalDCTfiH1AO7MsniZ3j9tNGNF-sBV-iDQ" style="width:100%"></a>-->
<!--    </div>-->
<!--    <div id="ads-banner-img-content" style="width: 58%;float:right;padding:8px 4px 0 4px;">-->
<!--    <h6 style="font-size: 12px  !important;margin:0px  !important;padding: 2px 5px 7px 0 !important;font-family: arial;font-weight: bold;line-height: 13px;">-->
<!--          Marketing Funnels Made Easy-->
<!--      </h6>-->
<!--    <p style="font-size: 10.5px;line-height: 12px;padding-right: 10px;margin: 0 0 8px 0;font-family: arial;">-->
        
<!--        Get Your Free 14 Day ClickFunnels Trial And Start Building Your First Funnel Right Now.-->
<!--       </p>-->
<!--    <div class="ads-banner-tag">-->
<!--        <span style="border:1px solid  #edecec;padding: 5px 6px 1px 6px;font-size: 10px;margin-bottom: 7px;display: inline-block;">AD - Colgate</span>-->
<!--       <div class="clearfix"></div>-->
<!--     <span style="background: #edecec;padding: 2px 6px 0px 6px;font-size: 10px;margin-right:2px;">clickfunel</span>-->
<!--    <span style="background: #edecec;padding: 2px 6px 0px 6px;font-size: 10px;margin-right:2px;">tabsale</span>-->
     
<!--    <a href="" target="_blank" style="float: right;font-size: 11px;color: #f72e5e;margin: 5px 5px 0 0;">Shop Now <i class="fa fa-external-link" aria-hidden="true"></i></a>-->
<!--    </div>-->
<!--    </div>-->
    
    
     
<!--    <div class="clearfix"></div>-->
<!--    </div>-->
 
 
 
 <div data-ad-block data-ad-type='ad' data-ad-mode='image' data-ad-size='1080x1080'></div>
 
 
 



 <div class="trending-rewards-tags">
   <div class="span12" id="crousal_wrap" style="width:100%">
     <div class="tag_head">Tags: </div>    
        <ul id="reward_tags1" class="owl-carousel" style="display:block">
             @if(isset($tags) && count($tags)>0)
                 @foreach(array_slice($tags,0,20) as $tag)   
   
        <form action="{{URL::to('search')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" class="form-control" name="searchdata" aria-describedby="sizing-addon2" value="{{ $tag }}">
            <input type="hidden" name="searchType" id="searchType" value="video"/>
        
         <li> <input type="submit" value="{{ $tag }}"/> </li>
        </form>
    @endforeach
    @endif
      </ul>
    </div>  
  <div class="clear"></div>
</div>

  
 
               <!-- Recomended Videos -->
               <div id="recom-head" class="caption" style="margin:0;padding:0px;">
                   <div class="left">
                       <a href="#">Recomended Videos</a>
                   </div> 
                   <div class="clearfix"></div>
               </div>
               
               
               <div class="list" id="recom">
                    @if(!empty($categoryWiseVideo))
						@foreach($categoryWiseVideo as $key => $videos)
                            @if( ($key + 1)%5 == 0 )
                                <div class="col-lg-12 col-sm-12 col-sm-12 videoitem extra-block" data-ad-type="video" data-ad-style='category-listing' data-ad-block>
                                </div>
                            @endif
							@include('elements.post.video_v')
						@endforeach   
					@endif
                    <div id="load-more-videos"></div>
               </div>
           </div>
       </div>
       
       
       
   

 
 
    <script>
    
    jQuery(document).ready(function($) {
            $('.owl-carousel_ads').owlCarousel({
                center: true, 
                autoplay:false, 
                autoplayTimeout:5000,
                autoplayHoverPause:false,
                items:1.3,           
                loop:true,     
                margin:10,      
                responsive:{
                    600:{
                        items:1.2 
                    }
                },
                onInitialized : function(){
                    if($('.owl-item').first().hasClass('active'))
                        $('.owl-prev').hide();
                    else
                        $('.owl-prev').show();
                } 
            }); 
           
        }); 
   
    </script>
    

  

       
   </div>
   {{ HTML::style('public/css/mediaelementplayer.min.css')}}
   <!-- Go to www.addthis.com/dashboard to customize your tools -->
  
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-61385eedbd8b385d"></script>

<script>
 $(".skip-091").click(function(){
  $("#ads_skip_all_content").hide();
});
</script>
 
 
  <script>
   $(document).ready(function() { 
       var owl = $("#reward_tags1"); 
       owl.owlCarousel({ 
           items: 4, //10 items above 1000px browser width
           itemsDesktop: [1000, 6], //5 items between 1000px and 901px
           itemsDesktopSmall: [900, 6], // 3 items betweem 900px and 601px
           itemsTablet: [600, 5], //2 items between 600 and 0;
           itemsMobile: [600, 5], // itemsMobile disabled - inherit from itemsTablet option
       });  
   });
</script>



<script type="text/javascript">
	var video_key = $(this).attr('href');
    var userID = '<?php echo $loggedInUser; ?>';
    var videoID = '<?php echo $videoData->id; ?>';
    var APP_URL  = '<?php echo URL::to('/'); ?>';

    if(userID){
        (function(){
            let watchedSeconds = [];
            let send = true;
            let timeout = null;

            const _video = $("#mainVideo");
            _video.bind("timeupdate", function(){
                // var currentTime = this.currentTime / 60;
                // console.log(this.currentTime);
                let currentTime = Math.floor(this.currentTime);
                if( !watchedSeconds.includes(currentTime) ){
                    watchedSeconds.push(currentTime);
                    if( send === true ){
                        if( timeout ){
                            timeout = null;
                        }
    
                        send = false;
                        timeout = setTimeout(() => {
                            $.ajax({
                                type:'get',
                                url: BASE.url('video-watch-time?videoID='+videoID+'&duration='+currentTime),
                            });
                            send = true;
                            clearTimeout(timeout);
                        }, 3000);
                    }
                }
            });
        })();
    }

    function copyText() {
            /* Get the text field */
            var txt = document.getElementById("btn-copy-txt");

            /* Select the text field */
            txt.select();
            txt.setSelectionRange(0, 99999); /*For mobile devices*/

            /* Copy the text inside the text field */
            document.execCommand("copy");

            $('.btn-copy').text('Copied');
    }

    $('#watchlaterCurrentvideo').click(function(e){
        e.preventDefault();
        if(userID){
            var video_key = $(this).attr('href');
            var watchLater = '{{ $videoWatchLaterCount }}';
            $.ajax({
                type:'get',
                url:'watch-later/'+video_key,
                success:function(data) {
                    $("#watchLaterCount").text(data);
                    $(".cvicon-cv-watch-later").css('color','#f72e5e');
                    if(data >= watchLater){
                        $(".cvicon-cv-watch-later").addClass('watchlaterinactive');
                        $(".cvicon-cv-watch-later").removeClass('watchlateractive');
                    }
                    if(data > watchLater){
                        $(".cvicon-cv-watch-later").addClass('watchlateractive');
                        $(".cvicon-cv-watch-later").removeClass('watchlaterinactive');
                    }
                }
             
            });
        } else {  
            window.location.replace(APP_URL+'/login');
        }

    });

    $('#likedvideo').click(function(e){
        e.preventDefault();
        if(userID){
            var video_key = $(this).attr('href');
            var totalLikeCount = '{{ $videoLikeCount }}';
            $.ajax({
                type:'get',
                url:'liked-video/'+video_key,
                success:function(data) {
                    console.log(data)
                    if(data.likeCount >= totalLikeCount){
                        $(".fa-thumbs-up").addClass('unliked');
                        $(".fa-thumbs-up").removeClass('liked');
                    }
                    if(data.likeCount > totalLikeCount){
                        $(".fa-thumbs-up").addClass('liked');
                        $(".fa-thumbs-up").removeClass('unliked');
                    }
                    
                    if(data.disLikeCount > 0)
                    {
                        $(".fa-thumbs-down").css('color','#f72e5e');
                    }
                    
                    $("#likedCount").text(data.likeCount);
                    $("#unLikedCount").text(data.disLikeCount);
                    $(".fa-thumbs-up").css('color','#f72e5e');
                    $(".fa-thumbs-down").css('color','#637076');
                }
            });
        } else {  
            window.location.replace(APP_URL+'/login');
        }
    });

    $('#dislikevideo').click(function(e){
        e.preventDefault();
        if(userID){
            var video_key = $(this).attr('href');
            var totalDisLikeCount = '{{ $videoDisLikeCount }}';
            $.ajax({
                type:'get',
                url:'disliked-video/'+video_key,
                success:function(data) {
                    
                     if(data.disLikeCount >= totalDisLikeCount){
                        $(".fa-thumbs-down").addClass('dislikeinactive');
                        $(".fa-thumbs-down").removeClass('dislikeactive');
                    }
                    if(data.disLikeCount > totalDisLikeCount){
                        $(".fa-thumbs-down").addClass('dislikeactive');
                        $(".fa-thumbs-down").removeClass('dislikeinactive');
                    }
                    
                    {
                        $(".fa-thumbs-up").css('color','#f72e5e'); 
                    }
                    $("#likedCount").text(data.likeCount);
                    $("#unLikedCount").text(data.disLikeCount);
                    $(".fa-thumbs-down").css('color','#f72e5e');
                    $(".fa-thumbs-up").css('color','#637076'); 
                }
            });
        } else {  
            window.location.replace(APP_URL+'/login');
        }
    });

    $('#flagcommentbtn').click(function(e){
        $('#myModal').modal('toggle');
        var comment = $('#flagcomments').val();
        if(userID){
            var video_key = $('#flagKey').val();
            $.ajax({
                type:'get',
                url:'flag-video/'+video_key+'/'+comment,
                success:function(data) {
                    $("#videoFlag").text(data);
                    $(".cvicon-cv-flag").css('color','#f72e5e');
                }
            });
            $('#flagcomments').val('');
        } else {  
            window.location.replace(APP_URL+'/login');
        }
    });

    if(userID){
        var isWatchLaterByUser = '<?php echo $isWatchLaterByUser; ?>';
        var isLikedByUser = '<?php echo $isLikedByUser; ?>';
        var isDisLikedByUser = '<?php echo $isDisLikedByUser; ?>';
        var isFlagByUser = '<?php echo $isFlagByUser; ?>';
        if(isWatchLaterByUser > 0) {
            $(".cvicon-cv-watch-later").css('color','#f72e5e');
        }

        if(isLikedByUser > 0) {
            $(".fa-thumbs-up").css('color','#f72e5e');
        }

        if(isDisLikedByUser > 0) {
            $(".fa-thumbs-down").css('color','#f72e5e');
        }

        if(isFlagByUser > 0) {
            $(".cvicon-cv-flag").css('color','#f72e5e'); 
        }

    }
    
    function showMe(id){
        $(id).toggle();
    }
    
    $("#commentfrm").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                console.log(data);
                $('#comments').html(''); // show response from the php script.
                $('#comments').append(data.html);
            }
        });
        $(this)[0].reset();
    });
    
    var isFollowing = '<?php echo $isFollowing; ?>';
    var followCount = '<?php echo $followCount; ?>';
    follow_unfollow(isFollowing, followCount);

    $(document).on('submit','#follow, #unfollow',function(e){
        e.preventDefault();
         if(userID){
            var form = $(this);
            var url = form.attr('action');
    
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                success: function(data)
                {
                   if(!data.status){
                        follow_unfollow(data.isFollowing, data.followCount);
                        alert(data.message);
                    } else {
                        alert(data.message);
                    }
                }
            });
            $(this)[0].reset();
        } else {  
            window.location.replace(APP_URL+'/login');
        }
    });
    
    function follow_unfollow(isFollowing, followCount){        
        var csrf_field = '<?php echo csrf_field(); ?>';
        var method_field = '<?php echo method_field("DELETE"); ?>';
        var videoData_User_ID = '<?php echo $videoData->user->id; ?>';
        var unfollowformAction = '<?php echo route("unfollow", ["id" => $videoData->user->id]);?>';
        var followformAction = '<?php echo route("follow", ["id" => $videoData->user->id]);?>';

        if(isFollowing){
            var unfollowbtn = '<form id="unfollow" action="'+unfollowformAction+'" method="POST">'
                        +csrf_field+method_field+'<button type="submit" id="delete-follow-'
                        +videoData_User_ID+'" class="c-f">Unfollow</button><div class="c-s">'
                        +followCount+'</div></form><div class="clearfix"></div>';
            $('#followunfollowbtn').html('');
            $('#followunfollowbtn').append(unfollowbtn);
        } else {
            var followbtn = '<form id="follow" action="'+followformAction+'" method="POST">'
            +csrf_field+'<button type="submit" id="follow-user-'+videoData_User_ID+'" class="c-f">follow</button><div class="c-s">'+followCount+'</div></form><div class="clearfix"></div>';
            $('#followunfollowbtn').html('');
            $('#followunfollowbtn').append(followbtn);
        }
    }
    $(function(){
        let page = 1;
        let isLoading = false;
        $(window).on("scroll", function(){
            if( !isLoading ){
                var position = $(window).scrollTop();
                const recom = $("#recom").parent();
                const offsetTop = recom.offset().top;
                var bottom = recom.height() - $(window).height() + offsetTop;
                if( position >= bottom && $("#load-more-videos").length > 0 ){
                    isLoading = true;
                    $("#load-more-videos").remove();
                    $.ajax({
                        type: "GET",
                        url: "{{ url('api/load-more-videos') }}",
                        data: {
                            page: ++page,
                            id: "{{ $videoData->video_key }}"
                        },
                        success: function(response){
                            if( response.status ){
                                $("#recom").append(response.data);
                                isLoading = false;
                            }
                        }
                    })
                }
            }
        });
    })
</script>

  
     <style>
    #owl-demo .item{
  
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }
    .customNavigation{
      text-align: center;
    }
    .customNavigation a{
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }
    
    #crousal_wra{width: 83%;
    float: left;
    margin-left: 1%;}
    
    </style>

    <script>
    $(document).ready(function() { 
      var owl = $(".owl-carousel21"); 
      owl.owlCarousel({ 
 
      items : 16, //10 items above 1000px browser width
      itemsDesktop : [1000,9], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,7], // 3 items betweem 900px and 601px
      itemsTablet: [600,4], //2 items between 600 and 0;
      itemsMobile :[600,5], // itemsMobile disabled - inherit from itemsTablet option
      
   
        });    
     
    });
    </script>
    
    
    
    <script>$(document).ready(function() {
	var showChar = 200;
	var ellipsestext = "...";
	var moretext = "View More";
	var lesstext = "View less";
	$('.more').each(function() {
		var content = $(this).html();
		console.log(content.length)
		if(content.length > showChar) {
			var c = content.substr(0, showChar);
			var h = content.substr(showChar-1, content.length - showChar);

			var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

			$(this).html(html);
		}

	});

	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(lesstext);
		} else {
			$(this).addClass("less");
			$(this).html(moretext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
});</script>
    
       <!--<div id="ads_skip_all_content" class="video-ads_piopup" style="position:absolute; width:100%; height:100%;left:0px; top:0;z-index:99">-->
       <!--                     <a href="javascript:void(0)" class="skip-091">SKIP ADS </a>-->
       <!--                     <div data-ad-block data-ad-type='ad' data-ad-mode='video'></div>-->
       <!--                 </div>-->
                        
                        
    
@endsection