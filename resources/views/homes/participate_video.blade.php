@php
    use Carbon\Carbon;
@endphp

@extends('layouts.home')

@section('style')
    <style>
        .upload-page .u-area {
            padding-top: 29px;
            padding-bottom: 20px;
        }

        .upload-page .u-terms {
            padding: 33px 40px;
        }


        body.light .content-wrapper {
            background-color: #f4f3f3;
        }

        .u-area span:hover img {
            opacity: 0.7;
        }

        .u-area i:hover span {
            color: #637076;
            opacity: 0.7;
        }
    </style>

    <style>
        .tags_tr {
            margin: -10px 0 20px 0;
        }

        .tags_tr a {
            background: #e5e5e5;
            border: 0px;
            color: #000;
            line-height: 27px;
            height: 25px;
            padding: 2px 10px 0 10px;
            margin-bottom: 1px;
            font-size: 12px;
        }

        .tags_tr a.selected {
            background: #f72e5e;
            color: #fff;
        }

        .tags_tr a:hover {
            background: #f72e5e;
            color: #fff;
        }


        .upload-page .u-area .u-text1 {
            display: block;
            margin-top: 41px;
            margin-bottom: 0;
            font-size: 20px;
            color: #2e2e2e;
        }

        #page-title0 {
            text-align: center;
            font-size: 22px;
            margin: 36px 0 7px 0;
            color: #f72e5e;
            text-transform: uppercase;
        }

        #live-video-user-list-home {
            border-bottom: solid 1px #eceff0;
        }

        .single-video .adblock {
            padding-top: 0px;
        }

        #progress {
            height: 6px;
            display: block;
            width: 0%;
            border-radius: 2px;
            background: -moz-linear-gradient(center top, #13DD13 20%, #209340 80%) repeat scroll 0 0 transparent; /* IE hack */
            background: -ms-linear-gradient(bottom, #13DD13, #209340); /* chrome hack */
            background-image: -webkit-gradient(linear, 20% 20%, 20% 100%, from(#13DD13), to(#209340)); /* safari hack */
            background-image: -webkit-linear-gradient(top, #13DD13, #209340); /* opera hack */
            background-image: -o-linear-gradient(#13DD13, #209340);
            box-shadow: 3px 3px 3px #888888;
        }

        .preview {
            border: 1px solid #CDCDCD;
            width: 450px;
            padding: 10px;
            height: auto;
            overflow: auto;
            color: #4D4D4D;
            float: left;
            box-shadow: 3px 3px 3px #888888;
            border-radius: 2px;
        }

        .percents {
            float: right;
        }

        .upload-progress {
            display: none;
        }

        #video-thumb-image {
            display: none;
        }

        .upload-thumb-image {
            padding: 22px 46px;
            border: 2px dotted #ccc;
            font-size: 38px;
            color: #999;
            margin: 5px;
        }

        #thumbnail_image img {
            max-width: 150px;
            max-height: 120px;
            margin: 0px;
        }

        .video-thumb-img {
            position: relative;
            display: inline-block;
            margin: 0px 5px;
        }

        .delete-video-thumb {
            position: absolute;
            right: 0px;
            color: #f72e5e;
            background-color: #fff;
        }

        #mbl-ban {
            display: none;
        }

        @media only screen and (max-width: 767px) {
            #croppie-modal-0 .cr-viewport.cr-vp-square {
                width: 300px !Important;
                height: 150px !Important;
            }

            #croppie-modal-0 .cr-boundary {
                width: 300px !Important;
                height: 150px !Important;
            }


            .u-details {
                padding: 0 20px 15px 20px;
                background: #fff;
                margin-top: 20px;
            }

            .upload-thumb-image {
                width: 100%;
                display: block;
                text-align: center;
            }

            .video-thumb-img {
                margin: 16px auto 1px;
                display: block;
                width: 150px;
            }

            .margin-top-reduce {
                margin-top: 20px !Important;
            }

            .u-area.mt-small {
                margin-top: 40px !important;
            }


            #mbl-ban {
                display: block;
            }

            #dskp-ban {
                display: none;
            }

            #page-title0 {
                text-align: center;
                font-size: 18px;
                margin: 30px 0 0 0;
                color: #f72e5e;
                padding: 0 43px;
            }

            .upload-page .u-area .u-btn {
                width: 100%;
                max-width: 280px;
                margin-top: -4px;
            }
        }

    </style>
    {{ HTML::style('public/css/bootstrap-tagsinput.css')}}
    {{ HTML::script('public/js/jquery.validate.js')}}
    {{ HTML::script('public/js/bootstrap-tagsinput.js')}}
    {{ HTML::script('public/js/bootstrap3-typeahead.js')}}
    {{ HTML::script('public/js/script.js')}}

    {{ HTML::script('public/js/jquery.easytabs.min.js') }}
    {{ HTML::script('public/js/owl.carousel.js') }}
    {{ HTML::script('public/js/only_slider.js') }}
@endsection

@section('content')

    @if( $currentWeekVideoCount > 1 )
        <div class='containers'>
            <div class='alert alert-info' style="text-align:Center">Already uploaded 2 reward videos in this week</div>
        </div>
    @else
        <!--Start Upload Video Part-->
        <div class="content-wrapper" id="upload-video-step1">
            <div class="containers">
                <!--<img src="{{ url('/public/img/rewards_banner.jpg') }}" style="width:100%">-->

                <div class="row" style="margin:0px;">
                    <div class="col-lg-6" style="margin:0px;padding:0">
                        <img id="dskp-ban" src="https://www.entertainmentbugs.com/public/img/banner.jpg"
                             style="width:100%">

                        <img id="mbl-ban" src="https://www.entertainmentbugs.com/public/img/mobile-banner.jpg"
                             style="width:100%">
                    </div>


                    <div class="col-lg-6 upload-page">
                        <h2 id="page-title0">Upload and participate to our reward video</h2>
                        <div class="u-area">
                            {{ Form::open(array('method' => 'post', 'id' => 'video-upload', 'enctype'=>'multipart/form-data')) }}
                            <input name="video"
                                   type="file"
                                   id="uploadVideoFile"
                                   onchange="validateFile(this.files);"
                                   accept="video/*"
                                   data-croppie-file="#uploadVideoFile"
                                   data-croppie-input="#uploadVideoFile"
                                   data-croppie-progress="#upload-photo-progress-1"
                            />
                            <i class="cv cvicon-cv-upload-videoa">
    				    <span style="font-size: 14px;display: block;margin: 14px 0 0 0;font-family: 'Hind Guntur', sans-serif;"> 
    				    <img src="https://www.entertainmentbugs.com/public/img/play-icons.png"
                             style="margin-bottom:10px; width:100px;opacity:0.6"><br>
                            <small style="background:#637076; color:#fff;padding: 1px 12px;opacity: 0.6;">Click here to Browse Video File</small> </span></i>
                            <div id="dropbox">
                                <p class="u-text1">Select Video files to upload</p>
                            </div>
                            <div class="u-progress" id="upload-photo-progress-1" style="display: none; width: 150px">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                         aria-valuemax="100" style="width: 0%;" data-progress="bar">
                                        <span class="sr-only" data-progress="text">0% Complete</span>
                                    </div>
                                </div>
                            </div>
                            {{ Form::button('Upload Video', ['type' => 'button', 'id'=>'submit-video', 'class' => 'btn btn-primary u-btn'])}}{{Form::close()}}
                        </div>
                        <div class="u-terms">
                            <p>By submitting your videos to our platform, you acknowledge that you agree to our <a
                                        href="#">Terms of Service</a> and <a href="#">Community Guidelines</a>.</p>
                            <p class="hidden-xs">Please be sure not to violate others' copyright or privacy rights.
                                Learn more</p></div>
                    </div>
                </div>
            </div>
        </div>
        </div>

        <div class="content-wrapper upload-page edit-page" id="upload-video-step2" style="display:none;">
            {{ Form::open(array('method' => 'post', 'id' => 'uploadVideo')) }}
            {{ Form::hidden('video', null, ['id' => 'selected-video-name']) }}
            {{ Form::hidden('selected_thumb', null, ['id' => 'video-selected-thumbnail']) }}

            <div class="container-fluid u-details-wrap">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="error-message">@include('elements.errorSuccessMessage')</div>
                                <div class="u-details">
                                    <div class="row">
                                        <div class="col-lg-12 ud-caption">Upload Details</div>
                                        <div class="col-lg-12">
                                            <div class="u-title"></div>
                                            <div id="upload-progress">
                                                <div class="u-size"></div>
                                                <div class="u-progress">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="0"
                                                             aria-valuemin="0" aria-valuemax="100" style="width:0%;">
                                                            <span class="sr-only">0% Complete</span>
                                                        </div>
                                                    </div>
                                                    <div class="u-close">
                                                        <a href="#"><i class="cvicon-cv-cancel" id="cancel_upload"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="u-desc"><span class="percent-process"></span>&nbsp;&nbsp;Your
                                                Video is still uploading, please keep this page open until it's done.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row margin-top-reduce" style="margin:40px 0 0 0;">
                            <div class="col-lg-10" id="thumbnail_image">
                                <div class="u-progress" id="upload-photo-progress-6"
                                     style="display: none; width:100%;    margin-bottom: 42px;">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                             aria-valuemax="100" style="width: 0%;" data-progress="bar">
                                            <span class="sr-only" data-progress="text">0% Complete</span>
                                        </div>
                                    </div>
                                </div>
                                <input name="thumb_image" type="file" id="croppie-video-thumb-file"
                                       style="display: none" accept="image/x-png,image/gif,image/jpeg"/>
                                <a href="javascript:void(0);" title="Upload Video Thumbnail Image"
                                   class="upload-thumb-image" data-croppie-progress="#upload-photo-progress-6"
                                   data-croppie data-croppie-file="#croppie-video-thumb-file"
                                   data-croppie-input="#croppie-video-thumb-input"
                                   data-croppie-output="#croppie-video-thumb-preview" data-croppie-bind="file"
                                   data-croppie-viewport='{"width": 400, "height": 200}'
                                   data-croppie-store="public/uploads/video/thumbs/"
                                   data-croppie-boundary='{"width": 400, "height": 200}'><i class="cvicon-cv-plus"></i></a>
                                <input type="hidden" name="thumb[1]" id="croppie-video-thumb-input"/>
                                <input type="hidden" name="duration" data-duration='1'/>
                                <a href="javascript:void(0)" class="video-thumb-img">
                                    <img src="https://www.entertainmentbugs.com/public/img/story_thumb.jpg"
                                         id="croppie-video-thumb-preview"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="u-form">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="video-title">Video Title</label>
                                        {{ Form::text('title', null, ['id' => 'video-title', 'close' => 'form-control', 'placeholder' => 'Rocket League Pro Championship Gameplay (36 characters remaining)', 'required' => true]) }}
                                        <span id="videoTitleError"></span>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="video-description">About</label>
                                        {{ Form::textarea('description', null, ['id' => 'video-description', 'class' => 'form-control', 'rows' => '3', 'required' => true, 'placeholder' => 'Description']) }}
                                    </div>
                                </div>
                                @php
                                    $days = Carbon::now()->subDays(Carbon::now()->dayOfWeek)->startOfDay();
                                    $rewardVideos = \App\Models\Video::where([['isRewardVideo', '=', 1], ['videos.created_at', '>=', $days->format("Y-m-d 00:00:00")]])
                                    ->whereNull('videos.deleted_at')
                                    ->select('videos.id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'videos.created_at')
                                    ->whereNull('videos.deleted_at')
                                    ->orderByDesc('id')
                                    ->with([
                                        'thumb' => function ($thumb) {
                                            $thumb->select('id', 'thumb');
                                        },'video_tag.tag'
                                    ])
                                    ->with([
                                        'totalViews' => function ($query) {
                                            $query->select('video_id')
                                                ->groupBy('video_id', 'video_id')
                                                ->orderBY(DB::raw('count(video_id)'), 'DESC');
                                        }
                                    ])
                                    ->with([
                                        'user.artist' => function ($user) {
                                            $user->where('isDeleted', 0);
                                        }

                                    ])
                                    ->holdUser()
                                    ->withCount(['totalViews'])
                                    ->with('watchTime')
                                    ->withCount(['video_likes'])
                                    ->withCount(['video_dislikes'])
                                    ->orderByDesc('total_views_count')->get();
                                     $array = [];
                                   if(count($rewardVideos)>0){
                                        foreach($rewardVideos->sortByDesc('total_views_count')->values()  as $reward) {
                                            foreach($reward->video_tag as $tags) {
                                             $array[]= $tags->tag->tag;
                                            }
                                        }
                                    }
                                @endphp
                                <div class="col-lg-7">
                                    <div class="form-group">
                                        <label for="e1">Video Tags</label>
                                        {{ Form::text('tags', null, ['id' => 'video-tags', 'close' => 'form-control', 'required' => true, 'placeholder' => 'Select mulitple tags']) }}
                                    </div>
                                    <div class="tags_tr">
                                        <span style="background:none;color:#000;font-size: 10px;margin-right: 10px;">Trending tags:</span>


                                        @if(isset($array) && count($array)>0)
                                            @foreach(array_unique($array) as $tag)
                                                <a href="{{url('/reward-trending-tags/'.$tag)}}">{{$tag}}</a>
                                            @endforeach
                                        @endif

                                    </div>
                                </div>


                                <div class="col-lg-5">
                                    <div class="form-group">
                                        <label for="video-language">Language</label>
                                        {{ Form::text('language', null,['class' => 'form-control', 'placeholder' => 'Choose Language', 'id' => 'video-language'])}}
                                    </div>
                                </div>
                            </div>
                            <div class="row ">
                                <div class="col-lg-12 u-category">Category ( you can select upto 2 categories )</div>
                            </div>

                            <div class="row">
                                @if(!empty($videoCategories))
                                    @foreach($videoCategories as $catId => $catName)
                                        <div class="col-lg-3 col-xs-6">
                                            <div class="checkbox">
                                                <label>
                                                    <label class="checkbox">
                                                        {{ Form::checkbox('categories[]', $catId)}}
                                                        <span class="arrow"></span>
                                                    </label> {{ $catName }}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="u-area mt-small">
                            {{ Form::button('Save', ['type' => 'submit', 'class' => 'btn btn-primary u-btn'])}}
                        </div>
                        <div class="u-terms">
                            <p>
                                By submitting your videos to circle, you acknowledge that you agree to circle's
                                <a href="#">Terms of Service</a>
                                and
                                <a href="#">Community Guidelines</a>.
                            </p>
                            <p class="hidden-xs">
                                Please be sure not to violate others' copyright or privacy rights. Learn more
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close()}}
        </div>
    @endif
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $("i.cvicon-cv-upload-videoa").click(function () {
                $("#uploadVideoFile").trigger('click');
                $('input[type="file"]').change(function (e) {
                    var fileName = e.target.files[0].name;
                    $('#dropbox').prepend(fileName);
                    $('.cvicon-cv-upload-video').css('color', '#f72e5e');
                });
            });

            $('#submit-video').on('click', function (e) {
                var fileData = $('#uploadVideoFile').prop('files');
                if (validateFile(fileData)) {
                    $('#upload-video-step1').hide(0);
                    $('#upload-video-step2').show(0);
                    handleFiles(fileData, "", "#croppie-video-thumb-input", "#croppie-video-thumb-preview", true);
                }
            });

            $("#uploadVideo").on("submit", function (e) {
                const title = $(this).find("[name='title']");
                const val = title.val().trim();

                if (val == "" || typeof val == "undefined") {
                    e.preventDefault();
                    $("#videoTitleError").text("Title must not be empty");
                } else if (val != "" && val.length > 100) {
                    $("#videoTitleError").text("Title must not be more than 100");
                    $('html, body').animate({
                        scrollTop: $("#videoTitleError").offset().top
                    }, 500)
                    e.preventDefault();
                    return false;
                } else {
                    $("#videoTitleError").text("");
                }
            });

            $("#uploadVideo").validate();

            $(".upload-thumb-image").click(function () {
                $("#video-thumb-image").trigger('click');
            });

            $('#video-tags').tagsinput({
                typeahead: {
                    source: function (query) {
                        return JSON.parse('{!!  json_encode(array_unique($array)) !!}');
                    }
                }
            });

            $('#video-language').tagsinput({
                typeahead: {
                    source: ['English', 'Hindi', 'Tamil', 'Bengali', 'Gujarati']
                },
                freeInput: true
            });
        });
    </script>
@endsection