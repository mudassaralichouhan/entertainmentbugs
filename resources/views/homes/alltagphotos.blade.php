@extends('layouts.home')


@section('content')

<Style>
 .inner-photo{ width:16.3%;margin:0 .15%  5px 0.15%;float:left;} 
/*.inner-photo:nth-child(1) {width: 33%;margin: 0 .15% 0px 0.15%;float: left;}*/
/*.inner-photo:nth-child(14){width: 33.1%;margin: 0px .30% 4.55px 0.0%;float: right;}*/
/*.inner-photo:nth-child(15)  {width: 33%;margin: 0 .15% 0px 0.15%;float: left;}*/
/*.inner-photo:nth-child(28){width: 33.1%;margin: 0px .30% 4.55px 0.0%;float: right;}*/
/*.inner-photo:nth-child(37)  {width: 33%;margin: 0 .15% 0px 0.15%;float: left;}*/






.trending-rewards-tags .owl-controls{display:none !important;}



.photo-content-b{position:absolute;left:0px;z-index: 2;bottom:0px;padding: 10px;}
.photo-content-b:before{background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%,rgba(0, 0, 0, 0.8) 90%);  content: '';position: absolute;left: 0;top: 0;bottom: 0;right: 0;z-index: 1;background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%,rgba(0, 0, 0, 0.9) 90%);}
.inner-photo{position:relative;}  

.inner-photo img{width:100%;}
.photo-content-bottom h2 {color: #fff;font-size: 11px;line-height: 15px;text-align: left;height: 27px;overflow: hidden;margin-bottom: 7px;margin-top:0px;}
.photo_wrapper {height:auto; padding: 0px 0 0px 0;background: #f4f3f3;margin: 0 0px; }
.photo_users{position:absolute;left:10px;top:10px; }
.photo_users img{width:30px;border-radius:100px; margin-right:2px;float:left}
.photo_users .name_bl0982{color: #fff;font-size: 11px;padding: 10px 0 0 5px;display: block;float: left;}
.uploaded_process{display:none;}
.tag_nme12 {background: #f72e5e;margin-right: 4px;padding: 5px 6px 3px 6px;color: #fff !important;font-size: 9px;line-height: 8px;margin-bottom: 10px;display: block;float: left;}
.tag_click {overflow: hidden;width: 123%;height: 18px;font-size: 12px;}
.tag_click_main{overflow:hidden; width:100%;}
.pht0923{display:block;}
.photo-content-bottom{position:relative; z-index:5;}
#photo_tag_slide_all .owl-pagination {display:block !Important;}
#photo_tag_slide_all .owl-page span{display:none; }
#photo_tag_slide_all .owl-page.active{opacity:0.6;}
#photo_tag_slide_all .owl-page{ }
#photo_tag_slide_all .owl-page:nth-child(1) {position: absolute;left: -16px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow-gray.png) no-repeat;width: 13px;height: 26px;top: 81px;background-size: contain;}  
#photo_tag_slide_all .owl-page:nth-child(2) {position: absolute;right: -17px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow-gray.png) no-repeat;width: 13px;height: 26px;top: 81px;background-size: contain;}
#photo_tag_slide_all .owl-page:nth-child(1):hover {position: absolute;left: -16px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow.png) no-repeat;width: 13px;height: 26px;top: 81px;background-size: contain;}  
#photo_tag_slide_all .owl-page:nth-child(2):hover {position: absolute;right: -17px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow.png) no-repeat;width: 13px;height: 26px;top: 81px;background-size: contain;}
.content-block .cb-header {
    padding: 5px 0 0 0;
    color: #7e7e7e;
}


  @media  only screen and (max-width: 767px) {
 .inner-photo{ width:49.7% !Important;margin:0 .15%  5px 0.15%  !Important;float:left  !Important;}
 
  }

</Style>


<style>
.trending-rewards-tags {padding-top:10px;}
.trending-rewards-tags ul{list-style:none;text-align: left;margin: 4px 0 1px 10px;padding: 0 12px 0 0;    width: 99%;}
.trending-rewards-tags li{display:inline-block;text-align:left;}
.trending-rewards-tags li a{   background: #e5e5e5;border: 0px;color: #000;line-height: 27px;height: 25px;padding: 2px 10px 0 10px;margin-bottom: 1px;font-size: 12px;}
#reward_tags1 .owl-item {margin-right:5px;width: auto !important;}
.tag_head{margin-right: 5px;float: left;width: 11%; line-height: 30px; color:#fff;    height: 20px;}
.btn.btn-default {padding: 12px 12px 0 12px;} 
#reward_tags1 .owl-pagination {display:block !Important;}
#reward_tags1 .owl-page span{display:none; }
#reward_tags1 .owl-page.active{opacity:0.6;}
#reward_tags1 .owl-page{ }
#reward_tags1 .owl-page:nth-child(1) {position: absolute;left: -16px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow-gray.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}  
#reward_tags1 .owl-page:nth-child(2) {position: absolute;right: -4px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow-gray.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}
#reward_tags1 .owl-page:nth-child(1):hover {position: absolute;left: -16px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}  
#reward_tags1 .owl-page:nth-child(2):hover {position: absolute;right: -4px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}


</Style>
    <div class="trending-rewards-tags" style="    background: #f4f3f3;"> <div class="container">  
       <div class="span12" id="" style="width:100%">
        <ul id="reward_tags1" class="owl-carousel" style="display:block">
          @if(isset($tags) && count($tags)>0)
             @foreach($tags as $tag)   
            <li class="item"><a href="{{url('photo/'.$tag)}}">{{$tag}}</a></li>
            @endforeach
            @endif
         </ul>
        </div>     </div>   
     </div>   
     
<div class="photo_wrapper">
 <div class="container">  
		<div class="content-block head-div">
					<div class="cb-contents chanels-row" id="home_page_channel">
						<div class="cb-header">
							<div class="row">
								<div class="col-lg-10 col-sm-10 col-xs-8">
									<ul class="list-inline">
										<li style="margin-left: 10px;">
											<a href="" class="color-active">
												 <span>Tag Search:  </span> 
											</a>
										</li> 
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>  
       
        <div class="span12" id="" style="width:100%">
       <div id="category_photo_data">
				<div class="photo-block-all-in-one" id="">
         
      @if( $latestPhotos && count($latestPhotos) > 0 )

    @foreach( $latestPhotos as $key => $photo )          
    @php
            $images = unserialize($photo->images);
          
            $tagsList = explode(',', $photo->tag_list);
        @endphp
      <div class="inner-photo"  value="{{ $photo->id }}"> 
       <a class="pht0923" href="javaScript:void(0)">
            <img src="{{ photo_url($images[0]) }}">
        </a>
    
       <div class="photo_users">
            <a href="https://www.entertainmentbugs.com/userprofile/{{$photo->user->slug}}" data-photo="1687634679.png" class="user_img">
            <img src="https://www.entertainmentbugs.com/public/uploads/users/small/{{$photo->user->photo}}" alt="User Icon">
            </a>
            <a href="https://www.entertainmentbugs.com/userprofile/{{$photo->user->slug}}"  class="name_bl0982">{{$photo->user->name}}</a>
            </div>

        <div class="photo-content-b"> 
             <div class="photo-content-bottom">
              
                     <h2>{{$photo->photo_title}}</h2> 
                 <div class="tag_click_main">
                     <div class="tag_click">
                         @foreach($tagsList as $tag)
                        <a href="{{url('photo/'.$tag)}}" class="tag_nme12">{{$tag}}</a>
                       @endforeach
                      </div>  
                </div>
           </div> 
        </div> 
    </div>  
      
      @endforeach
  @endif

    </div>
    
  


    </div>  
     
    </div> 
</div>
      <!-- desktop view start -->
			<div class="over-all">
				<a href="javascript:void(0)" id="closemodel" style="position:absolute; right:20px;top:20px;color:#fff;" class="cross"><i class="cvicon-cv-cancel" style="font-size:24px"></i></a>
				<div class="photo-container">
					<div style="display:none" class="cross_block">
						<a href="javaScript:void(0)" class="cross"><i class="fas fa-angle-left" aria-hidden="true"></i>
							Back
						</a>
					</div>
							<style>
				
				
.social a.whatsapp-i {background-color: #4FCE5D;}
.social a.instagram {background-color: #d62976;padding-top: 7px;}
.social a{border-radius:0}


			   .active4 a{color: #f72e5e !important}   
	
	@media only screen and (max-width: 767px) {
						#home_page_channel .col-lg-2 {
							margin: 1px 1% 0px 1% !important;
						}

						input[type="text"],
						input[type="email"],
						input[type="number"],
						input[type="search"] {
							font-size: 13px;
						}

						.read-comment {
							height: 245px;
							overflow: scroll;
							background: #f4f3f3;
						}
					}
					.user.mt-2:nth-child(1) {
						border: 0px;
					}

					.user_info_mini {
						display: none !important;
					}

					.post {
						margin-bottom: 15px;
					}

					.mobile-view-photos-scroll {
						background: #171717 !important
					}

					.mobile-view-photos-scroll .fw-body {
						background: #fff !important
					}

					.mobile-view-photos-scroll .photo-head .social {
						display: none;
					}

					.mobile-view-photos-scroll .view-more-replies {
						padding: 0px 12px;
						background: #f4f3f3 !important;
						border-radius: 5px;
						height: 17px;
						text-align: right;
						display: block;
						font-size: 10px;
					}

					.mobile-view-photos-scroll .write-comment {
						border-top: 1px solid #e0e1e2;
					}

					.mobile-view-photos-scroll .mob09-comment {
						margin-top: 0px !important
					}

					.mobile-view-photos-scroll .shortnamevd {
						margin-top: 0px;
						font-size: 14px;
					}

					.mobile-view-photos-scroll .user a {
						font-size: 11px;
						padding: 8px 0 0 0;
						color: #f72e5e !Important;
					}

					.mobile-view-photos-scroll .user a.user_img {
						width: 35px;
						margin: 6px 7px 2px 0;
						padding: 0px;
					}

					.mobile-view-photos-scroll .comment span {
						font-size: 13px !important
					}

					.mobile-view-photos-scroll .child_cmnt {
						font-size: 10px !important;
					}

					.mobile-view-photos-scroll .load-more-btn {
						padding: 0px 12px;
						background: #f4f3f3 !important;
						border-radius: 5px;
						height: 17px;
						text-align: right;
						display: block;
						font-size: 10px;
					}

					.mobile-view-photos-scroll .tag-followers span {
						font-size: 10px !important
					}

					.mobile-view-photos-scroll .photo-middle_botttom span {
						font-size: 9px !important;
						margin: 2px 0 !Important;
					}

					.mobile-view-photos-scroll .read-comment {
						display: none;
					}

					.mobile-view-photos-scroll .photo-middle_botttom h2 {
						font-size: 12px !Important;
					}

					@media only screen and (max-width: 767px) {
					    
					    #mobile_social{width: 135px;float: right;margin-top: -1px;}
					    #mobile_social a {margin-right: 5px; width: 20px;height: 20px;padding-top: 2px;margin-bottom: 2px;font-size: 9px;}
					    
						.tag-followers {
							bottom: 1px !important;
						}
					}

					.view-more-replies,
					.load-more-btn {
						background-color: #b3adadbf;
						color: #626262;
						padding: 6px 12px;
						border-radius: 5px;
						border-color: transparent;
						display: flex;
						height: 30px;
						width: calc(100% - 8px);
					}

					.photo-head a.flw {
						cursor: pointer;
						color: #fff;
						display: block;
						height: 30px;
						line-height: 33px;
						position: absolute;
						right: 10px;
						top: 15px;
						background: #28b47e !Important;
						z-index: 9;
						padding: 0 10px;
						font-size: 11px;
					}

					.photo-head a.btn-0flw {
						cursor: pointer;
						color: #fff;
						display: block;
						height: 30px;
						line-height: 33px;
						position: absolute;
						right: 10px;
						top: 15px;
						background: #f72e5e !Important;
						z-index: 9;
						padding: 0 10px;
						font-size: 11px;
					}

					.user a.user_img {
						width: 35px;
						margin: 9px 10px 6px 0;
						padding: 0px;
					}

					#photo-container-all .users a img,
					#mobile-photos .users a img {
						width: 35px;
						height: 35px;
						border-radius: 100px;
						margin-right: 5px;
						object-fit: cover
					}
  
					 .shortnamevd{ background: #28b47e !important;
					border-radius: 100px;
					color: #fff;
					text-align: center;
					font-weight: bold;
					margin-top: 0px;
					font-size: 14px;
					line-height: 36px;
					float: left;
					margin-right: 5px;
					}

					.over-all .photo-container-all i.fa:hover,
					.over-all .photo-container-all i.cv:hover {
						color: #fff !important;
					}

					.fa.fa-thumbs-up {
						color: #fff !important;
					}

					.photo-head .shortnamevd {
						margin-top: 0px;
					}

					.photo-head .users {
						line-height: 39px;
					}

					.child_cmnt {
						background: none;
						border: 0px;
						text-decoration: underline;
						font-size: 12px;
					}

					.sidenav {
						z-index: 9999999;
					}

					.user:nth-child(even) {
						background: #fff;
						padding: 2px 7px 0 18px;
						border-radius: 69px 0 0 69px;
						margin: 4px 0 4px 0;
					}

					.user.mt-2 {
						background: none !important;
						border-radius: 0 !important;
						background: none !important;
						padding: 0 !important;
						border-top: 1px solid #e2e2e2;
						margin-left: 0% !important;
						width: 100% !important;
					}

					.user {
						padding: 2px 7px 0 8px !important;
						border-radius: 10px 0 0 10px !important;
						margin: 4px 0 4px 0;
						border-top: 0px solid #e2e2e2;
						margin-left: 5% !important;
						width: 95% !important;
						background: #fff !important;
					}

					@media only screen and (max-width: 767px) {
						.photo-container-all {
							min-height: auto !important;
						}

						#photo-container-all .photo-head {
							position: absolute;
							width: 100%;
							top: -410px;
						}

						.photo-container-left {
							margin-top: 60px;
						}

						#no-mb00 {
							display: block;
						}

						#no-mb00 {
							display: block;
						}

						.deskp09 {
							display: block;
						}

						.tag-followers {
							right: 9px !important;
						}

						.photo-container-left {
							height: 350px !important;
						}

						.desktop-scroll {
							padding-bottom: 50px;
						}

						.desktop-scroll {
							height: 60vh;
						}

						.user.mt-2 {
							background: none !important;
							border-radius: 0 !important;
							background: none !important;
							padding: 0 !important;
							border-top: 1px solid #e2e2e2;
							margin-left: 0% !important;
							width: 100% !important;
						}

						.user {
							padding: 2px 7px 0 8px !important;
							border-radius: 10px 0 0 10px !important;
							margin: 4px 0 4px 0;
							border-top: 0px solid #e2e2e2;
							margin-left: 5% !important;
							width: 95% !important;
							background: #fff !important;
						}

					}
				</style>
					<style>
						.desc {
							position: absolute;
							color: #fff;
							bottom: 52px;
							left: 18px;
							font-size: 20px;
						}

						.carousel {
							position: inherit;
						}

						.carousel-indicators li {
							width: 40px !important;
							height: 40px !important;
							border-radius: 100px;
							overflow: hidden;
						}

						.carousel-indicators {
							bottom: 0;
						}

						.carousel-indicators .active {
							background: none
						}

						.carousel-control i {
							position: relative;
							top: 30%;
						}
						.tag-followers {
							position: absolute;
							right: 12px;
							bottom: 9px;
						}
						.photo-container{
							overflow: unset;
						}
						.photo-container-left 	.photo-container-inside {display:block !Important;}
					.photo-middle_botttom button{    position: absolute;
    left: -18%;
    top: 489px;
    background: #f72e5e !important;
    color: #fff;
    border: 0px;
    line-height: 26px;
    padding: 4px 20px 2px 20px;}
						.photo-container-inside {
							position: relative;
							display: flex !important;
							flex-direction: row;
							flex-wrap: nowrap;
							clear: unset;
						}
					    .over-all .arrow:hover    {background:#28b47e !important}
					    .carousel-control {
                        	background: none;
                        	text-shadow: 0px 0px 0px #000;
                        	background-image: none !important;
                        }
						.over-all .arrow{
						    position: absolute;
                        	width: 40px;
                        	height: 40px;
                        	left: -60px;
                        	top: calc(50% - 20px);
                        	border-radius: 3%;
                        	background-color: #f72e5e !Important;
                        	color: #fff;
                        	border: 0px;
                        	font-size: 19px;
                        	line-height: 48px;
                        	text-align: center;
                        	padding: 0;
                        	margin: 0;
                        	display: block;
						}
						.over-all .arrow.arrow-next{
							left: auto;
							right: -60px;
						}
					</style>
					<div class="photo-container-inside" id="popup-wrapper--desk"></div>
					<button type="button" class="arrow arrow-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
					<button type="button" class="arrow arrow-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
				</div>
				<input type="hidden" id="start" value="0">
				<input type="hidden" id="rowperpage" value="30">
				<input type="hidden" id="totalrecords" value="{{ $totalrecords }}">
			</div>
		
		
</div>		
		
			<!-- desktop view end -->
			<style>
				.mobile-view-photos-scroll {
					position: fixed;
					top: 0;
					left: 0;
					z-index: 999999999;
					width: 100%;
					height: 100vh;
					background-color: white;
					display: none;
					overflow-y: auto
				}

				@media only screen and (max-width: 576px) {
					#mobile-photos .carousel {
						height: 350px;
						position: relative;
					}

					#mobile-photos .carousel-inner {
						height: 100%;
					}

					#mobile-photos .carousel-inner .item img {
						object-fit: cover;
						width: 100%;
						object-position: top left;
						height: 350px;
					}

					#mobile-photos .carousel-indicators img {
						margin: 0 auto;
						display: block;
						width: 100%;
						height: 100%;
						object-fit: cover;
					}
				}
			</style>
			<!-- mobile view start -->
			<div class="mobile-view-photos-scroll" id="mobile-photos">
				<div class="cross_block" style="position:sticky">
					<a href="javaScript:void(0)" class="cross" data-mobile-close><i class="fas fa-angle-left" aria-hidden="true"></i> Back
					</a>
				</div>
				<div class="fw-container">

				</div>
			</di
    <div class="clear"></div>
</div> 



	 
   	  <script>
           $(document).ready(function() {

               var owl = $("#reward_tags1");

               owl.owlCarousel({

                   items: 14, //10 items above 1000px browser width
                   itemsDesktop: [1000, 6], //5 items between 1000px and 901px
                   itemsDesktopSmall: [900, 8], // 3 items betweem 900px and 601px
                   itemsTablet: [600, 7], //2 items between 600 and 0;
                   itemsMobile: [600, 7], // itemsMobile disabled - inherit from itemsTablet option
               });

           });
       </script>   
	<script type="text/javascript">
			////////////////////////////////////////////////////////////////////////////////////////////////////////
			///////////////////////////////Working with page on scroll load data //////////////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////////
			checkWindowSize();

			// Check if the page has enough content or not. If not then fetch records
			function checkWindowSize() {
				if ($(window).height() >= $(document).height()) {
					// Fetch records
					fetchData();
				}
			}

			// Fetch records
			function fetchData() {
				var start = Number($('#start').val());
				var allcount = Number($('#totalrecords').val());
				var rowperpage = Number($('#rowperpage').val());
				start = start + rowperpage;

				if (start <= allcount) {
					$('#start').val(start);

					$.ajax({
						url: "{{ route('photo.load-ajax') }}",
						data: {
							start: start
						},
						dataType: 'json',
						success: function(response) {
							// Add
							$("#category_photo_data").find(".photo-block-all-in-one").find('.photo-block:last')
								.after(response.html).show().fadeIn("slow");

							// Check if the page has enough content or not. If not then fetch records
							checkWindowSize();
							EBUGS.ads.load();
						}
					});
				}
			}

			$(document).on('touchmove', onScroll); // for mobile

			function onScroll() {
				if ($(window).scrollTop() > $(document).height() - $(window).height() - 100) {
					fetchData();
				}
			}

			$(window).scroll(function() {
				var position = $(window).scrollTop();
				var bottom = $(document).height() - $(window).height() - 100;

				if (position >= bottom) {
					fetchData();
				}
			});

			$("#mobile-photos").on("click", ".click-comment-open-trigger", function() {
				event.preventDefault();
				$(this).closest(".fw-body").find(".read-comment").slideToggle("slow");
			});

			$("#mobile-photos").on("click", "[data-action='do-like']", function(e){
				e.preventDefault();
				
				let my = $(this);
				let photoId = my.data('id');

				$.ajax({
					type: 'get',
					url: 'liked-photo/' + photoId,
					success: function(data) {
						my.find("[data-likes-count]").text(data.likeCount);
					}
				});
			});
				$(document).on("submit", "[data-comment-form='commentfrmphotomobile']", function(e) {
				e.preventDefault();
				var form = $(this);
				var url = form.attr('action');
				const post = $(this).closest("[data-photo-id]");
				const isMobile = window.innerWidth <= 576 ? true : false;
				
				$.ajax({
					type: "POST",
					headers: {
						'X-CSRF-TOKEN': "{{ csrf_token() }}",
					},
					url: url,
					data: form.serialize(), // serializes the form's elements.
					success: function(response) {
						const commentContainer = isMobile ? "fw-body" : "photo-container-all";
						const comments = post.find(`.${commentContainer}>.comments>.read-comment`);
						post.find(`.${commentContainer}>.comments>.read-comment`);
						
						if( comments.css('display') != "block" ){
							comments.slideDown();
						}

						post
							.find(`.${commentContainer}>.comments>.read-comment>.mobile-comments`)
							.prepend(response.html);

						post
							.find("[data-comments-count]")
							.text(parseInt(post.find("[data-comments-count]").text()) + 1);
					}
				});
				$(this)[0].reset();
			});
				$(document).ready(function() {
				$(".cross").click(function() {
					if (window.innerWidth <= 576) {
						$("#mobile-photos").hide();
						$("#mobile-photos .fw-container").html('');
						$("body").css('overflow', 'unset');
					} else {
						$(".over-all").hide();
					}
				});
			});
		</script>   
	  
	  


@endsection