@extends('layouts.home')
@section('content')
<section class="channel light">
<style>	
	.add_video {
		width: 100%;
		height: 150px;
		background-color: #eceff0;
		margin: 20px 0 0 0;
		text-align: center
	}
	
	.add_video a {
		padding: 52px 0 0 0;
		display: block;
		height: 100%;
		width: 100%
	}
</style>
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <!-- Featured Videos -->
                <div class="content-block" style="padding:20px 0">
                    
                     <div class="cb-header">
                        <div class="row">
                            <div class="col-lg-8 col-xs-8 col-sm-6">
                                <ul class="list-inline">
                                    <li><a href="#" class="color-active active">Watch later</a></li>
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="cb-content videolist">
                        <div class="row">
                            @if(!empty($watchlist))
                                @foreach($watchlist as $list)
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                        <div class="b-video">
                                            <div class="v-img">
                                                <a href="{{URL::to($list->url)}}">
                                                    @if(isset($list->thumb->thumb))
                                                    {{HTML::image('public/uploads/video/thumbs/'.$list->thumb->thumb)}}
                                                    @endif
                                                <div class="time">{{ $list->video->video_time}}</div>
                                            </div>
                                            <div class="v-desc">
                                                <a href="{{URL::to($list->url)}}">{{ $list->title}}</a>
                                            </div>
                                            <div class="v-views">
                                                @php
                                                    $created_at = strtotime($list->created_at);
                                                    $curDatetime = time();
                                                    $difference = $curDatetime - $created_at;
                                                @endphp
                                                {{ count($list->viewsCount) }} views. <span class="v-percent"><span class="v-circle"></span>{{ \Carbon\Carbon::parse($list->created_at)->diffForHumans() }}</span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach   
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /Featured Videos -->

            </div>
        </div>
    </div>
</div>
</section>
@endsection