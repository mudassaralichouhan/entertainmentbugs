@extends('layouts.home')
@section('content')
    <style>
        a.frst2 i {
            color: #f72e5e !important;
        }

        .main-profile-default {
            width: 70px;
            display: block;
            height: 70px;
            text-align: center;
            line-height: 88px;
            margin-top: 10px;
            background: #28b47e !important;
            color: #fff;
            border-radius: 100px;
            font-size: 32px;
            font-weight: bold;
            text-transform: uppercase;
        }

        .main-profile-default:hover {
            color: #fff;
        }

        @media (min-width: 100px) and (max-width: 767px) {
            #live-video-user-list-home {
                display: none;
            }

            .follow_pop-up {
                padding: 0 5px;
            }

            .abt-mbl-sml {
                font-size: 12px;
            }

            .main-profile-default {
                width: 75px;
                height: 75px;
                line-height: 88px;
                margin-top: 2px;
                font-size: 32px;
            }

            .search-page-only1 {
                margin: 10px 0 20px 0;
                padding: 15px 10px !important;
            }

        }

        #user-search a.flw {
            cursor: pointer;
        }

        #search-result-follow_full-page a.flw {
            cursor: pointer;
        }

        #search-result-follow .user-search-individual {
            padding: 0 16px;
        }

        #search-result-follow_full-page .user-search-individual {
            padding: 2px 16px 0 16px;
            width: 16%;
            margin: 0 0.3% 5px 0.3%;
            float: left;
            height: 190px;
        }

        #search-result-follow_full-page #new-profile-details {
            width: 100%;
            float: none;
            text-align: center;
        }

        #search-result-follow_full-page #new-profile {
            width: 100%;
            float: none;
        }

        #search-result-follow_full-page .user-search-individual {
            border: solid 1px #eceff0;
        }

        #search-result-follow_full-page a.flw {
            margin: 0 auto;
            background: #28b47e !important;
            display: block;
            color: #fff;
            width: 100px;
            text-align: center;
            z-index: 99;
            position: relative;
            line-height: 29px;
            height: 25px;
        }

        .suggested-option #new-profile-details h5 {
            font-weight: 400;
            font-size: 14px;
            margin-top: 15px;
            margin-bottom: 1px;
        }

        .suggested-option #new-profile-details p {
            font-size: 12px;
        }

        .search-page-only1 {
            margin: 10px 0 20px 0
        }

        .channel .c-details .c-nav .list-inline li a {
            display: block;
        }

        #about-new-01 {
            margin: 0px;
            padding: 23px 0 10px 0
        }

        

        #profile_social a {
            width: 27px;
            margin-right: 0px;
            margin-left: 2px;
            height: 27px;
            font-size: 11px;
        }

        #new-profile-details a {
            font-size: 12px;
        }

        #mobile-video-and-stories a.active {
            border-bottom: 2px solid #f72e5e;
            color: #f72e5e
        }

        #mobile-video-and-stories a:hover {
            border-bottom: 2px solid #f72e5e;
            color: #f72e5e
        }

        .channel .c-details .c-nav .list-inline li a:hover:before {
            opacity: 0;
        }

        #profile_social .gp {
            padding-left: 1px;
        }

        #follower-main1 a {
            width: 175px;
            float: right;
        }

        .user-search-individual {
            border-bottom: solid 1px #eceff0;
        }

        #user-search #new-profile {
            width: 6%;
            float: left
        }

        #user-search #new-profile-details {
            width: 92%;
            float: left;
            margin-left: 2%
        }

        #user-search h5 {
            font-size: 16px;
            margin-top: 18px;
            letter-spacing: 0.2px;
        }

        #user-search p {
            font-size: 12px;
        }

        #user-search a.flw {
            float: right;
            margin: 29px 0 0 0;
            background: #28b47e !important;
            color: #fff;
            width: 90px;
            text-align: center;
            z-index: 99;
            position: relative;
            line-height: 29px;
            height: 25px;
        }

        a:hover {
            text-decoration: none !Important;
        }

        @media only screen and (max-width: 767px) {
            #search-result-follow .user-search-individual {
                padding: 0 10px;
            }

            #search-result-follow_full-page .user-search-individual {
                padding: 2px 16px 0 16px;
                width: 49%;
                margin: 0 0.5% 5px 0.5%;
                float: left;
                height: 185px;
            }

            .desktop-search-result {
                padding: 5px 5px 10px 5px !important;
                max-height: 500px;
                overflow: scroll;
            }

            .desktop-search-result h2 {
                margin: 2px 0 0 0 !important;
                font-size: 13px !important;
            }
        }
    </style>

    <div style="background:#f4f3f3 !important">
        <div class="container follow_pop-up" style="display:block; position:relative; ">

            <!-- Search result block-->
            <div class="desktop-search-result"
                style="background:#fff;padding:1px 20px 20px 20px;margin-top:10px;max-height: 500px;
    overflow: scroll;">
                <form>
                    <div class="topsearch">
                        <i class="cv cvicon-cv-cancel topsearch-close"></i>
                        <div class="input-group">
                            <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-search"></i></span>
                            <input style="font-size: 13px;" id="follow_search" type="text" class="form-control"
                                placeholder="Search Profile" aria-describedby="sizing-addon2">

                            <!-- /btn-group -->
                        </div>
                    </div>
                </form>
                <h2 style="font-size: 15px;padding: 0 10px 0; margin:26px 0 14px 0;">Result: <span
                        id="follow_search_key"></span></h2>

                <div id="search-result-follow" style="position:relative">
                    <div id="user-search" class="follow-search-result">
                        @include('homes.follow-search')
                    </div>
                    <div id="loadMoreContent">

                    </div>
                </div>


            </div>
            <!-- Search result block-->


            <!-- Suggested follower  START-->
            <div class="search-page-only1" style="background:#fff;padding:20px;">
                <div class="suggested-option" id="search-result-follow_full-page">
                    <h2 style="font-size: 15px;padding: 0 10px 0; margin:6px 0 14px 0;">Suggested followers for you</h2>
                    <div id="user-search1">

                        @foreach ($friendsFollower as $user)
                            @php
                                $followers = DB::table('followers')
                                    ->where('follows_id', $user->id)
                                    ->get();
                                $isFollowing = DB::table('followers')
                                    ->where('follows_id', $user->id)
                                    ->where('user_id', session()->get('user_id'))
                                    ->first();
                                $userphotoPath = PROFILE_SMALL_DISPLAY_PATH . $user->photo;
                            @endphp
                            <div class="user-search-individual">
                                <div class="col-lg-2" style="padding:0px;" id="new-profile">
                                    <div class="c-avatar">
                                        @if (!empty($user->photo))
                                            <a href="{{ user_url($user->slug) }}">
                                                <img src="{{ asset($userphotoPath) }}" alt=""
                                                    style="width:60px;height:60px;object-fit:cover; margin:10px auto 10px; display:block;border-radius:100px">
                                            </a>
                                        @else
                                            <a href="{{ user_url($user->slug) }}" class="main-profile-default">
                                                @php$words = explode(' ', $user->name);
                                                    echo '<div class="shortnamepr">' . strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1)) . '</div>';
                                                @endphp

                                            </a>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-10  col-xs-10" id="new-profile-details">
                                    <div class="channel-details">
                                        <div class="row">
                                            <?php $user_id = $user->id; ?>
                                            @if (isset($isFollowing))
                                                <div id="suggested_unfollow_user{{ $user->id }}">
                                                    @include('homes.follow-suggested.unfollow_user')
                                                </div>
                                            @else
                                                <div id="suggested_follow_user{{ $user->id }}">
                                                    @include('homes.follow-suggested.follow_user')
                                                </div>
                                            @endif
                                            <a href="{{ user_url($user->slug) }}" style="display:block">
                                                <h5>{{ $user->slug ?? 'username' }} <img style="width: 15px;"
                                                        src="https://www.entertainmentbugs.com/public/img/verified-profile-icon.png">
                                                    <br> </h5>
                                            </a>
                                            <p>{{ $user->name ?? '' }} <br> {{ count($followers) }} followers</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="ajax-load text-center" style="display:none">
                <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>
            </div>

            <!-- Suggested follower  END-->
        </div>

    </div>
    <script>
        $(function() {

            var page = 7;
            $(document).on("click", "#loadMoreContent", function() {
                page = page + 2;
                loadMoreData(page);
            });

            function loadMoreData(page) {
                var search_key = $("#follow_search").val();
                $.ajax({
                    type: "GET",
                    url: "{{ URL::route('searchFollow') }}",
                    data: {
                        'search_key': search_key,
                        'page': page
                    },
                    cache: false,
                    beforeSend: function(html) {
                        $('#follow_search_key').html(search_key);
                    },
                    success: function(response) {
                        $('.follow-search-result').empty().html(response);
                    }
                });
            }

            $("#follow_search").keyup(function() {
                var search_key = $(this).val();
                $.ajax({
                    type: "GET",
                    url: "{{ URL::route('searchFollow') }}",
                    data: {
                        'search_key': search_key
                    },
                    cache: false,
                    beforeSend: function(html) {
                        $('#follow_search_key').html(search_key);
                        $('#loadMoreContent').html(
                            ' <a style="background: #f72e5e;padding: 6px 12px 2px 10px;color: #fff;margin: 16px 0 0 10px;display: block;width: 149px;text-align: center;" id="loadMoreContent">View More Profile</a>'
                            );
                    },
                    success: function(response) {
                        $('.follow-search-result').empty().html(response);
                    }
                });
            });
        });

        function suggestedFollowUser(user_id) {
            $.ajax({
                type: "GET",
                url: "{{ URL::route('suggested_follow_user') }}",
                data: {
                    'user_id': user_id
                },
                cache: false,
                success: function(response) {
                    $('#suggested_follow_user' + user_id).empty().html(response);
                    $('#suggested_follow_user' + user_id).attr("id", "suggested_unfollow_user" + user_id);

                }
            });
        }

        function suggestedUnFollowUser(user_id) {
            $.ajax({
                type: "GET",
                url: "{{ URL::route('suggested_unfollow_user') }}",
                data: {
                    'user_id': user_id
                },
                cache: false,
                success: function(response) {
                    $('#suggested_unfollow_user' + user_id).empty().html(response);
                    $('#suggested_unfollow_user' + user_id).attr("id", "suggested_follow_user" + user_id);
                }
            });
        }
    </script>
@endsection
