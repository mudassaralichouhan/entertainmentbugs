@extends('layouts.home')
@section('content')
<section class="channel light">
<style>	
	.add_video {
		width: 100%;
		height: 150px;
		background-color: #eceff0;
		margin: 20px 0 0 0;
		text-align: center
	}
	
	.add_video a {
		padding: 52px 0 0 0;
		display: block;
		height: 100%;
		width: 100%
	}
</style>
@include('elements.header_profile')
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <!-- Featured Videos -->
                <div class="content-block" style="padding:20px 0">

                    <div class="cb-content videolist">
                        <div class="row">
                            <div class="col-lg-2 col-sm-6 videoitem">
                                <div class="b-video">
                                    <div class="v-img">
                                        <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">
										{{HTML::image('public/img/video1-1.png')}}</a>
                                        <div class="time">3:50</div>
                                    </div>
                                    <div class="v-desc">
                                        <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">Man's Sky: 21 Minutes of New Gameplay - IGN First</a>
                                    </div>
                                    <div class="v-views">
                                        27,548 views. <span class="v-percent"><span class="v-circle"></span> 78%</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2 col-sm-6 videoitem">
                                <div class="b-video">
                                    <div class="v-img">
                                        <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">{{HTML::image('public/img/video1-2.png')}}</a>
                                        <div class="time">15:19</div>
                                        <div class="plus"><i class="cvicon-cv-plus" aria-hidden="true"></i></div>
                                        <div class="plus-details">
                                            <ul>
                                                <li><a href="#"><i class="cvicon-cv-watch-later" aria-hidden="true"></i> Watch Later</a></li>
                                                <li><a href="#"><i class="cvicon-cv-playlist" aria-hidden="true"></i> Add to Playlist</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="v-desc">
                                        <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">GTA 5: Michael, Franklin, and Trevor in the Flesh</a>
                                    </div>
                                    <div class="v-views">
                                        8,241,542 views. <span class="v-percent"><span class="v-circle"></span> 93%</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2 col-sm-6 videoitem">
                                <div class="b-video">
                                    <div class="v-img">
                                        <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">{{HTML::image('public/img/video1-3.png')}}</a>
                                        <div class="time">4:23</div>
                                    </div>
                                    <div class="v-desc">
                                        <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">Battlefield 3: Official Fault Line Gameplay Trailer</a>
                                    </div>
                                    <div class="v-views">
                                        2,729,347 views . <span class="v-percent"><span class="v-circle"></span> 95%</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2 col-sm-6 videoitem">
                                <div class="b-video">
                                    <div class="v-img">
                                        <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">{{HTML::image('public/img/video1-4.png')}}</a>
                                        <div class="time">7:18</div>
                                    </div>
                                    <div class="v-desc">
                                        <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">Batman Arkham City: Hugo Strange Trailer</a>
                                    </div>
                                    <div class="v-views">
                                        7,239,852 views. <span class="v-percent"><span class="v-circle"></span> 84%</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2 col-sm-6 videoitem">
                                <div class="b-video last-row">
                                    <div class="v-img">
                                        <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">{{HTML::image('public/img/video1-5.png')}}</a>
                                        <div class="time">23:57</div>
                                    </div>
                                    <div class="v-desc">
                                        <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">BATTALION 1944: TAKING ON BATTLEFIELD 5</a>
                                    </div>
                                    <div class="v-views">
                                        19,130 views. <span class="v-percent"><span class="v-circle"></span> 78%</span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-2 col-sm-6 videoitem">
                                <div class="b-video last-row">
                                    <div class="v-img">
                                        <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">
                                            {{HTML::image('public/img/video1-6.png')}}
                                            <div class="watched-mask"></div>
                                            <div class="watched">WATCHED</div>
                                            <div class="time">7:27</div>
                                        </a>
                                    </div>
                                    <div class="v-desc">
                                        <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">Amazon Blocking VIDEO GAMES for Non-Prime...</a>
                                    </div>
                                    <div class="v-views">
                                        185,525 views. <span class="v-percent"><span class="v-circle"></span> 93%</span>
                                    </div>
                                </div>
                            </div>

                            <div style="clear:both; overflow:hidden"></div>

                            <div class="col-lg-2 col-sm-6 videoitem">
                                <div class="add_video">

                                    <a href="http://azyrusthemes.com/circlevideo/single-video-tabs.html">
                                        <i class="cvicon-cv-plus" aria-hidden="true"></i>
                                        <Br>Add Video</div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- /Featured Videos -->

            </div>
        </div>
    </div>
</div>
</section>
@endsection