@extends('layouts.home')

@section('meta_tags')
    <meta name="description" content="Are you looking for artists for your upcoming projects? Make a profile and choose from the best talents according to your need.">
    <meta name="keywords" content="Production House, Production, Agency, Auditions, Casting, Production firm, Find Artist, Search Artist">
    <meta name="author" content="<?php echo ((isset($author) ? $author : 'Production House - Entertainment Bugs')); ?>">
    <meta property="og:site_name" content="{{URL::to('/')}}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="<?php echo ((isset($ogtitle) ? $ogtitle : 'Production House - Entertainment Bugs')); ?>"/>
    <meta property="og:description" content="<?php echo ((isset($ogdescription) ? $ogdescription : 'Are you looking for artists for your upcoming projects? Make a profile and choose from the best talents according to your need.')); ?>"/>
    <meta name="theme-color" content="#f82e5e">
    <meta property="og:keywords" content="Production House, Production, Agency, Auditions, Casting, Production firm, Find Artist, Search Artist">
    <meta property="og:url" content="<?php echo url()->current(); ?>"/>
    <meta property="og:image" content="<?php echo ((isset($ogimage) ? $ogimage : 'https://www.entertainmentbugs.com/public/uploads/logo/production-logo-social-media.png')); ?>"/><!--profile image-->
    
   @endsection
    
@section('content')
    <link href="{{ asset('public/css/casting.css') }}" rel="stylesheet">
    <style>
        #right-side-bar {
            background: #fff;
        }

        #right-side-bar .production-hs2 a.view-m {
            position: absolute;
            right: 3px;
            bottom: 3px;
        }

        .production-all-display {
            width: 99%;
            float: left;
            background: #f4f3f3;
            border: 1px solid #e5e5e5;
            margin: 0.5% 0.5%;
        }

        #right-side-bar .production-hs1 {
            width: 100%;
            float: none;
        }

        #right-side-bar .production-hs2 {
            width: 100%;
            float: none;
        }

        #right-side-bar {
            padding: 5px 10px 16px 10px;
        }

        #right-side-bar h5 {
            color: #f72e5e !important;
            font-size: 14px;
            margin: 7px 0 7px 0;
            padding: 9px 0 0 0;
        }

        #right-side-bar a.ic {
            padding: 0 !important;
            display: block;
            width: 45px;
            margin: 0 10px 10px 0;
            height: 45px;
            float: left;
            background: none !important;
            border: 0px !important;
        }

        #right-side-bar p {
            font-size: 13px;
            margin: 0 0 5px !important;
            line-height: 20px;
        }

        #right-side-bar .production-hs2 a.view-m {
            position: absolute;
            width: 85px;
            border: 0px !important;
            background: #28b47e !important;
            color: #fff !important;
            padding: 6px 10px 4px 10px;
            font-size: 14px;
            margin: 0px !important;
        }

        #right-side-bar a.ic img {
            border: 2px solid #eaeaea;
            width: 100% !important;
            height: 100%;
            object-fit: cover;
            border-radius: 500px
        }

        .banner-video {
            height: 560px;
            position: relative;
            overflow: hidden;
        }

        #production-home-video {
            height: 118vh;
            top: -31%;
        }

        #live-video-user-list-home {
            display: none;
        }

        .casting-menu ul {
            list-style: none;
            padding: 0px;
            text-align: center;
            background: rgba(247, 46, 94, 0.9);
            margin: 0px;
            position: absolute;
            z-index: 999;
            width: 100%;
        }

        #fl-r5 {
            float: right;
        }

        #bl0 {
            float: left
        }

        #spacer-extra {
            display: none;
        }

        .pr-act {
            color: #f72e5e !important;
        }

        .pr-act i {
            color: #f72e5e !important;
        }

        @media all and (max-width: 767px) {
            .intro-banner.main_banner .col-md-12 {
                padding: 0 10px;
            }

            .banner-headline-alt h3 {
                width: 100% !important;
                color: #fff;
                line-height: 36px;
                font-size: 36px;
                margin: 0 auto 10px;
                text-align: center;
            }

            .banner-headline-alt p {
                width: 100%;
                line-height: 21px;
                font-size: 13.5px;
                font-weight: 400;
            }

            .banner-headline-alt a {
                font-size: 14px;
            }

            #third-b2 a {
                font-size: 14px;
            }

            #hire_companies .right_block_companies a {
                font-size: 14px;
            }

            #hire_companies .right_block_companies p {
                font-size: 14px;
                line-height: 22px;
            }

            .intro-banner {
                padding: 99px 0 86px 0;
                position: relative;
            }

            .casting-menu {
                display: none;
            }

            #fl-r5 {
                float: right;
                padding: 0px 20px !important;
            }

            #bl0 {
                padding: 0px 20px !important;
            }

            #bl0 .col-lg-6 {
                width: 50%;
                float: left
            }

            #bl0 .col-lg-6 img {
                width: 100% !important;
            }

            .no-dis {
                display: none;
            }

            #third-b2 h2 {
                font-size: 26px;
                line-height: 31px;
                font-weight: 600;
                width: 100%;
            }

            .text-content.white-font {
                padding: 50px 0px 35px 0 !important
            }

            #third-b2 .col-lg-5.col-md-6.col-sm-12 {
                padding: 0px 20px !important
            }

            #third-b2 p {
                font-size: 14px;
                line-height: 22px;
                font-weight: 400;
            }

            #third-b2 .col-lg-7.col-md-6.col-sm-12 {
                padding: 0px 20px !important
            }

            #mobile-set {
                width: 100% !important;
                padding: 0px !important;
                height: auto !Important;
                margin-bottom: 40px !Important;
            }

            .two-block-content .simple-text-block>h3 {
                width: 100%;
                color: #ffffff;
                font-size: 26px;
                font-weight: 600;
                margin: 20px auto 20px;
                padding: 0 20px;
            }

            .all-pr .simple-text-block>h3 {
                font-size: 30px;
                line-height: 34px;
            }

            .simple-text-block span br {
                display: none;
            }

            .simple-text-block span {
                font-size: 14px;
                padding: 0 20px;
            }

            #last-section9 {
                padding: 0 0px;
                margin-top: -13px
            }

            #last-section9 .production-all-display {
                margin-bottom: 10px;
            }

            .mobile-off2 {
                display: none !important;
            }

            .mobile-show2 {
                display: block !Important;
                margin: 20px auto 0 !important;
            }

            #hire_companies .col-lg-my-own-6 {
                width: 100%;
                float: none !Important
            }

            #user-im {
                width: 200px;
                height: 200px;
                margin: 0 auto 20px;
            }

            #hire_companies .col-lg-my-own-5 {
                width: 100%;
                margin: 20px auto 0;
                position: relative;
                right: 0px;
                padding: 0 20px;
            }

            #hire_companies .alerts {
                top: 0;
                right: 0;
                padding: 14px 20px 20px 25px;
                width: 90%;
                position: relative;
                margin: 0 auto 31px;
            }

            #hire_companies .followers-block img {
                width: 35px;
                margin-left: -26px;
            }

            #hire_companies .right_block_companiesinside {
                padding: 20px 20px 40px 20px;
            }

            #hire_companies h2 {
                font-size: 22px;
                line-height: 29px;
                font-weight: 600;
                margin: 11px 0px 7px 0;
            }

            .block {
                float: left;
                margin: 0px 0 0px 0;
                padding: 70px 0 60px 0;
            }

            .followers-block {
                width: 100%;
            }

            .alerts p {
                font-size: 13px;
            }

            .carousel-indicators {
                margin: 0;
                bottom: 0;
                right: 0;
                position: relative;
                width: 100%;
                left: auto;
                text-align: center;
            }

            #action-page .simple-text-ht {
                width: 100% !important;
            }

            #action-page .simple-text-ht>h3 {
                font-size: 30px;
                text-align: center !important;
            }

            #action-page .simple-text-block span {
                text-align: center !important;
                font-size: 14px;
                margin: 0 auto;
                display: block;
                width: 95%;
            }

            #action-page .right_main1 {
                float: none;
            }

            .right_block_companiesinside {
                padding: 20px 20px 34px 20px;
            }

            .it-home {
                animation: slideleft 9000s infinite linear;
            }


            @keyframes slideleft {
                from {
                    background-position: 0%;
                }

                to {
                    background-position: 90000%;
                }
            }



            .two-block-content {
                padding: 20px 0 10px 0;
            }

            #new5 {
                padding: 5px 20px 20px 20px !important;
                clear: both;
                margin-top: -15px;
            }

            #main-090 {
                margin: 54px 0 0 0;
                clear: both;
                overflow: hidden;
            }

            .main-9 {
                width: 94%;
                margin: 20px auto 0;
                float: none;
                position: relative;
            }

            .half-left5 {
                width: 100%;
                float: left;
            }

            .half-left5 img {
                height: 29vh;
            }

            .half-right5 {
                width: 100%;
                float: left;
                padding: 51px 20px 40px 20px;
            }

            .gallery-extra-new {
                padding: 50px 0 !important;
            }

            .gallery-extra-new .simple-text-block>h3 {
                width: 100% !important;
            }

            .gallery-6 img {
                width: 48%;
                float: left;
                margin: 0 1% 8px 1%;
            }

            .gallery-6 img.shor5 {
                display: block !important;
            }

            .half-right5 h3 {
                font-size: 26px !important;
                margin: 0 0 19px 0;
            }

            #production-home-video {
                height: 470px;
            }

            .banner-video .intro-banner:after,
            .banner-video .intro-banner:before {
                height: 470px;
            }

            .banner-video {
                margin-bottom: 20px;
                height: auto;
            }

        }
    </style>




    <div class="content-wrapper">
        <div class="containers">
            <div class="casting-menu">
                <ul>
                    <li><a href="{{ URL::to('artist-profile') }}"> Artist Profile</a></li>
                    <li><a href="{{ URL::to('artist-videos') }}"> Artist Videos</a></li>
                    <li><a href="{{ URL::to('artist-gallery') }}"> Artist Gallery</a></li>
                                  <li><a href="https://www.entertainmentbugs.com/artist-profile?page=1&category[]=New%20Influncer&category[]=Experienced%20Influncer&gender=Select&city=&followers=Select&min_experience=Min:&max_experience=Max:&min_age=&max_age=&min_height=&max_height=&min_weight=&max_weight=&artist_name=">Influencer</a></li>
                    <li><a href="https://www.entertainmentbugs.com/artist-profile?page=1&category[]=Actor&category[]=Comedy%20Actor&category[]=Model&category[]=Pro%20Model&gender=Select&city=&followers=Select&min_experience=Min:&max_experience=Max:&min_age=&max_age=&min_height=&max_height=&min_weight=&max_weight=&artist_name=">Actor / Model</a></li>
                   
                    <li><a href="{{ URL::to('production-houses') }}">Production House</a></li>
                    <li><a href="{{ URL::to('find-auditions') }}">Find Auditions</a></li>
                    <li><a href="{{ URL::to('post-audition') }}">Post Auditions</a></li>
                </ul>
            </div>
        </div>
    </div>


    <div class="banner-video">
        <div class="intro-banner main_banner">

            <video id="production-home-video" autoplay loop muted playsinline>
                <source src="https://www.entertainmentbugs.com/public/web_video/video-production.mp4" type="video/mp4">
                <source src="movie.ogg" type="video/ogg">
                Your browser does not support the video tag.
            </video>

            <div class="container" style="position:relative;z-iindex:999">
                <div class="row">
                    <div class="col-md-12">
                        <br>
                        <div class="banner-headline-alt">
                            <h3>Are you a production agency? <br> You are at the right place </h3>
                            <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of
                                the day </p>

                            <?php
                         $userid=session()->get('user_id');
                         
     if($userid != ''){
   $artist_about=DB::select('select * from artist_about where user_id='.$userid);
    $artist_what_we_do=DB::select('select * from artist_what_we_do where user_id='.$userid);
    $artist_achievment=DB::select('select * from artist_achievment where user_id='.$userid);
    $artist_showreel=DB::select('select * from artist_showreel where user_id='.$userid);
    $artist_gallery=DB::select('select * from artist_gallery where user_id='.$userid);
    $artist_video=DB::select('select * from artist_video where user_id='.$userid);
    $artist_project=DB::select('select * from artist_project where user_id='.$userid);
     $artist_contact=DB::select('select * from artist_contact where user_id='.$userid);
                    $progress = 0;
                 if( count($artist_about) > 0){
                     $progress = 1;
                 }
                  if(count($artist_what_we_do) > 0){
                      $progress = 1;
                 }
                   if(count($artist_achievment) >0){
                     $progress = 1;
                 }
                   if(count($artist_showreel ) >0){
                     $progress = 1;
                 }
                   if(count($artist_gallery ) >0){
                      $progress = 1;
                 }
                   if(count($artist_video ) >0){
                      $progress = 1;
                 }
                   if(count($artist_project ) >0){
                      $progress = 1;
                 }
                    if(count($artist_contact ) >0){
                      $progress = 1;
                 }
                 if($progress==0){
                ?>
                            <a href="{{ URL::to('create-an-production-house-profile') }}" class="button"
                                style="border-color:#6c146b; color:#6c146b;width: 200px;">
                                Create profile</a>
                            <?php }
                  
                  } else {?>
                            <a href="{{ URL::to('create-an-production-house-profile') }}" class="button"
                                style="border-color:#6c146b; color:#6c146b;width: 200px;">
                                Create profile</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>




        </div>
    </div>




    <div class="well-none" id="third-b2" style="border-top: 0px solid #6f116c;   background: #fff;">
        <div class="container">
            <div class="text-content white-font" style="padding:80px 0px 65px 0;">
                <div class="row">
                    <div id="bl0" class="col-lg-6 col-md-6 col-sm-12"
                        style="position:relative;padding: 0 60px; float:right">

                        <style>
                            .small-banner img {
                                float: left
                            }
                        </style>
                        <div class="small-banner" style="position:relative">
                            <img src="{{ asset('public/web_img/camera-04.jpg') }}"
                                style="width: 36.5%;margin-bottom: 10px;">
                             <img src="{{ asset('public/web_img/camera-01.jpg') }}"
                                style="width: 62%;float: right;margin-bottom: 10px;">
                        </div>
                        <div class="small-banner" style="position:relative">
                             <img src="{{ asset('public/web_img/camera-02.jpg') }}" class="no-dis"
                                style="width: 62%;float: left;margin-bottom: 10px;">
                            <img src="{{ asset('public/web_img/camera-03.jpg') }}" class="no-dis"
                                style="width: 36.5%;margin-bottom: 10px;float: right;">
                        </div>

                    </div>
                    <div id="new5" class="col-lg-6 col-md-6 col-sm-12" id="fl-r5"
                        style="padding:60px 20px 60px 30px ">
                        <h2 style="color:#000;width:100%">Create a dedicated page for your Production house</h2>
                        <p> Join Entertainment bugs as a production house and get access to the largest community of artists. Make a deidcated page for your business and let the artists approach you for the next role you are casting. </p>
                        <a href="pages-pricing-plans.html"
                            class="button button-sliding-icon ripple-effect big margin-top-20"
                            style="border-color:#6c146b; color:#6c146b;width: 154.875px;">Get Started <i
                                class="icon-material-outline-arrow-right-alt"></i></a>
                    </div>

                    <!-- Infobox / End -->
                </div>
                <div style="clear:both; overflow:hidden"></div>
            </div>
        </div>
    </div>

    <div class="two-block-middle-section">
        <div class="containers">

            <div class="half-left5">
                <img
                    src="{{ asset('public/web_img/create_profile.jpg') }}">
            </div>

            <div class="half-right5">
                <div class="simple-text-block">
                    <h3>Create you profile and make your vision to life with our help for you project.</h3>
                    <span> Create a profile and get cast and find artist for your new <br> movies and web series.
                        Selected Portfoilio will be featured <br> to trending production house</span>
                    <a href="#" title="">Create a profile</a>
                </div>
            </div>
        </div>
    </div>



    <div class="clear"></div>


    <style>
        .main-9 {
            width: 31.3%;
            margin: 30px 1% 0 1%;
            float: left;
            position: relative;
        }

        .main-9 #user-im {
            width: 225px !Important;
            height: 225px !important;
            margin: 0 auto;
            position: relative;
            z-index: 9999;
            border: 1px solid #f72e5e;
        }

        #hire_companies .main-9 .alerts {
            top: -25px;
            right: 0;
            padding: 27px 14px 20px 22px;
            width: 100%;
            position: relative;
        }

        .main-9 .alerts {
            box-shadow: 7px 5px 11px hsl(0deg 0% 45% / 16%);
        }

        .two-block-content {
            background: #f4f3f3;
            padding: 70px 0 50px 0
        }

        .col-lg-7.col-md-6.col-sm-12 {
            position: relative;
        }

        .col-lg-7.col-md-6.col-sm-12:before {
            content: "";
            width: 92%;
            margin: 0;
            height: 100%;
            position: absolute;
            right: 4%;
            top: 0;
            z-index: 9;
            background: rgba(0, 0, 0, 000.1);
            opacity: 0;
        }
    </style>



    <div class="well-none" id="third-b2" style="border-top: 0px solid #6f116c; background:#f0f2f5">
        <div class="container">
            <div class="text-content white-font" style="adding:70px 0px;">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12" style="position:relative;padding: 0 30px;float:right">
                        <video id="mobile-set" width="100%" height="370" autoplay="" loop="" muted="">
                            <source src="https://www.entertainmentbugs.com/public/web_video/casting.mp4" type="video/mp4">
                            <source src="hhttps://www.entertainmentbugs.com/public/web_video/casting.mp" type="video/ogg">
                            Your browser does not support the video tag.
                        </video>
                    </div>

                    <div class="col-lg-5 col-md-6 col-sm-12" style="padding:50px 20px 23px 30px; ">
                        <h2 style="color:#000;width: 90%;">Showcase your project in our portal.</h2>
                        <p> Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative
                            things to strategy foster collaborative thinking. Leverage agile frameworks to provide
                            <a href="pages-pricing-plans.html"
                                class="button button-sliding-icon ripple-effect big margin-top-20"
                                style="border-color:#6c146b; color:#6c146b;width: 154.875px;">Get Started <i
                                    class="icon-material-outline-arrow-right-alt"></i></a>
                    </div>

                </div>
                <!-- Infobox / End -->
            </div>
            <div style="clear:both; overflow:hidden"></div>
        </div>
    </div>




    <div class="well-none" id="third-b2" style="border-top: 0px solid #6f116c;   background: #fff;  background:#fff">
        <div class="container">
            <div class="text-content white-font" style="adding:70px 0px;">
                <div class="row">

                    <div class="col-lg-7 col-md-6 col-sm-12" style="position:relative;padding: 0 30px;">
                        <video id="mobile-set" width="100%" height="370" autoplay="" loop=""
                            muted="">
                            <source src="https://www.entertainmentbugs.com/public/web_video/team.mp4" type="video/mp4">
                            <source src="https://www.entertainmentbugs.com/public/web_video/team.mp4" type="video/ogg">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12" style="padding:50px 20px 23px 30px; ">
                        <h2 style="color:#000;width:90%">Display team member in your profile.</h2>
                        <p> Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative
                            things to strategy foster collaborative thinking. Leverage agile frameworks to provide
                            <a href="#" class="button button-sliding-icon ripple-effect big margin-top-20"
                                style="border-color:#6c146b; color:#6c146b;width: 154.875px;">Get Started <i
                                    class="icon-material-outline-arrow-right-alt"></i></a>
                    </div>

                </div>
                <!-- Infobox / End -->
            </div>
            <div style="clear:both; overflow:hidden"></div>
        </div>
    </div>




    <div class="well-none" id="third-b2" style="border-top: 0px solid #6f116c;  background:#f4f3f3;">
        <div class="container">
            <div class="text-content white-font" style=" padding:70px 0px;">
                <div class="row">

                    <div class="col-lg-7 col-md-6 col-sm-12"
                        style="position:relative;padding: 0px 20px 30px 10px;float:right">
                        <video id="mobile-set" width="100%" height="370" autoplay loop muted>
                            <source src="https://www.entertainmentbugs.com/public/web_video/casting.mp4" type="video/mp4">
                            <source src="https://www.entertainmentbugs.com/public/web_video/casting.mp" type="video/ogg">
                            Your browser does not support the video tag.
                        </video>
                    </div>

                    <div class="col-lg-5 col-md-6 col-sm-12" style="padding:70px 30px 0px 10px; ">
                        <h2 style="color:#000; width:80%">Post Auditions for your project</h2>
                        <p> Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative
                            things to strategy foster collaborative thinking. Leverage agile frameworks to provide
                            <a href="pages-pricing-plans.html"
                                class="button button-sliding-icon ripple-effect big margin-top-20 mobile-off2"
                                style="border-color:#6c146b; color:#6c146b;width: 154.875px;">Post Auditions <i
                                    class="icon-material-outline-arrow-right-alt"></i></a>
                        </p>
                    </div>
                </div>




                <div id="last-section9">
                    <div class="col-sm-3" style="padding: 20px 4px 0 4px">
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic">
                                    <img
                                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a>
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">

                                <p class="prd-info"> Hi we are looking for a male lead for ...</p>

                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span> Bangalore</p>
                                    <div class="clear"></div>

                                    <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="col-sm-3" style="padding: 20px 4px 0 4px">
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic">
                                    <img
                                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a>
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">

                                <p class="prd-info"> Hi we are looking for a male lead for ...</p>

                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span> Bangalore</p>
                                    <div class="clear"></div>

                                    <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="col-sm-3 mobile-off2" style="padding: 20px 4px 0 4px">
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic">
                                    <img
                                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a>
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">

                                <p class="prd-info"> Hi we are looking for a male lead for ...</p>

                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span> Bangalore</p>
                                    <div class="clear"></div>

                                    <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="col-sm-3 mobile-off2" style="padding: 20px 4px 0 4px">
                        <div class="production-all-display" id="right-side-bar">
                            <div class="production-hs1">
                                <a href="" class="ic">
                                    <img
                                        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a>
                                <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                <div class="clear"></div>
                            </div>
                            <div class="production-hs2">

                                <p class="prd-info"> Hi we are looking for a male lead for ...</p>

                                <div class="sp1" id="auditions">
                                    <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p>
                                    <div class="clear"></div>
                                    <p><span><i class="fa fa-map-marker"></i> Location: </span> Bangalore</p>
                                    <div class="clear"></div>

                                    <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> Mar 29 2022</p>
                                </div>
                                <a href="" class="view-m">View Now</a>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="clear"></div>



                    <a href="#" class="button button-sliding-icon ripple-effect big margin-top-20 mobile-show2"
                        style="display:none;border-color:#6c146b; color:#6c146b;width: 154.875px;">Find Auditions <i
                            class="icon-material-outline-arrow-right-alt"></i></a>


                </div>



            </div>

        </div>
        <!-- Infobox / End -->
    </div>
    <div style="clear:both; overflow:hidden"></div>
    </div>





    <div class="well-none" id="third-b2" style="border-top: 0px solid #6f116c;   background: #fff;  background:#fff">
        <div class="container">
            <div class="text-content white-font" style="adding:70px 0px;">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12" style="position:relative;padding: 0 30px;">
                        <video id="mobile-set" width="100%" height="370" autoplay="" loop=""
                            muted="">
                            <source src="https://www.entertainmentbugs.com/public/web_video/showcase.mp4" type="video/mp4">
                            <source src="https://www.entertainmentbugs.com/public/web_video/showcase.mp4" type="video/ogg">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12" style="padding:50px 20px 23px 30px; ">
                        <h2 style="color:#000">Search artist for your next project.</h2>
                        <p> Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative
                            things to strategy foster collaborative thinking. Leverage agile frameworks to provide
                            <a href="pages-pricing-plans.html"
                                class="button button-sliding-icon ripple-effect big margin-top-20"
                                style="border-color:#6c146b; color:#6c146b;width: 154.875px;">Get Started <i
                                    class="icon-material-outline-arrow-right-alt"></i></a>
                    </div>
                </div>
                <!-- Infobox / End -->
            </div>
            <div style="clear:both; overflow:hidden"></div>
        </div>
    </div>




    <div class="artist-landing-page two-block-content">
        <div id="hire_companies">
            <div class="well-none">
                <div class="container">
                    <div class="row" style="position:relative; ">
                        <div class="simple-text-block">
                            <h3 style="color:#000; margin-bottom:5px">View our latest talented artist profile </h3>
                            <span style="color:#000">Create a profile and get cast and find artist for your new <br> movies
                                and web series.</span>
                        </div>
                        <div id="main-090">
                           @if ($premiumArtist)
                              @foreach ($premiumArtist as $key => $artist)
                                 <div class="main-9">
                                    @if (@$artist->profile_photo)
                                       <img src="{{ @$artist->profile_photo }}" alt="{{ @$artist->profile_name }}" id="user-im">
                                    @else
                                       <span class="shortname">{{ name_to_pic($artist->profile_name) }}</span>
                                    @endif
                                    <div class="alerts user-block">
                                       <span class="username" style="margin:0;color:#f72e5e">{{ $artist->profile_name }}</span>
                                       <p>{{ $artist->about_me }}</p>
                                       <div class="team">
                                          <span class="description_heading"> Location: {{ $artist->city.", ".$artist->state.", ".$artist->country }}</span>
                                          @php
                                             $categories = explode(",", $artist->multiple_category);
                                             $followers = $artist->user ? $artist->user->getFollowers() : "";
                                          @endphp
                                          <span class="description_loc" style="margin:-0px 0 0 0"> </span>
                                          <div class="search_skill">
                                             @foreach( $categories as $category )
                                                <button class="btn btn-default btn-xs bg_prpl">{{ $category }}</button>
                                             @endforeach
                                          </div>
                                          <div class="followers-block">
                                             @if( $followers && count($followers) > 0 )
                                                @foreach($followers as $follower)
                                                   @php
                                                      $user = \App\Models\User::select('name', 'photo')->where('id', $follower->user_id)->first();
                                                   @endphp
            
                                                   @if( @$user->photo )
                                                      <img src="{{ asset("public/uploads/users/small/".@$user->photo) }}" />
                                                   @else
                                                      <img src="{{ asset("public/img/ava2.png") }}" />
                                                   @endif
                                                @endforeach
                                             @endif
                                             <p><i class="fa fa-heart" style="margin:0;color:#f72e5e"></i> {{ $artist->user?$artist->user->getFollowerCount():"" }} Followers</p>
                                             
                                             @if ( Session::get("user_id") && isset($user) && $user->isFollowedBy() )
                                                <a data-do="unfollow" data-user-id="{{ @$artist->user->id }}" style="background-color: #f72e5e!important;"><i class="fa fa-solid fa-heart"></i> Following </a>
                                             @else
                                                <a data-do="follow" data-user-id="{{ @$artist->user->id }}" class="flw"><i class="fa fa-heart-o"></i> Follow me </a>
                                             @endif
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              @endforeach
                           @endif

                            <!-- Infobox / End -->
                        </div>
                        <div style="clear:both; overflow:hidden"></div>
                    </div>
                </div>
            </div>
        </div>


    </div>









    <div class="all-pr gallery-extra-new" style="background:#fff;padding:70px 0">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="simple-text-block">
                        <h3 style="color:#000; margin-bottom:5px">View artist gallery with numerious optios</h3>
                        <span style="color:#000">Create a profile and get cast and find artist for your new <br> movies and
                            web series.</span>
                        <div class="gallery-6"> 
                            <img src="https://www.entertainmentbugs.com/public/web_img/im01.jpg">
                            <img src="https://www.entertainmentbugs.com/public/web_img/im04.jpg">
                            <img src="https://www.entertainmentbugs.com/public/web_img/im03.jpg">
                            <img src="https://www.entertainmentbugs.com/public/web_img/im02.jpg">
                            <img src="https://www.entertainmentbugs.com/public/web_img/im05.jpg">
                            <img src="https://www.entertainmentbugs.com/public/web_img/im01.jpg" class="shor5"
                                style="display:none">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <section id="action-page" style="background:#f72e5e !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="simple-text-block">
                        <div class="simple-text-ht">
                            <h3 style="color: #fff;text-align:left;">Let's start something completely new with us</h3>
                            <span style="color: #fff;text-align:left;">Your Profile in minutes with Production house
                                assistant is ready!</span>
                        </div>
                        <a href="#" title="" class="right_main1">Create an Account</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
