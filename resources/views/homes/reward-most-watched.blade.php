@extends('layouts.home')
@section('content')
<style>
body.light .content-wrapper {background-color: #f4f3f3;padding: 0px 0 0 0;}
#reward_1 .content-block .cb-header {background: #fff;margin: 10px 0 0 0;padding: 10px 0 12px 20px;}
.content-block .cb-content {margin-top: 10px !Important;}

#home_page_video .v-desc a {font-size: 12px !Important; display: block;line-height: 15px;height: 35px;}

#rewards_main_page {padding: 40px 0 10px 0}
#rewards_main_page ul li {font-size: 24px;line-height: 64px;}
#reward_1 .b-video .v-img {height: 92px;overflow: hidden;}
#reward_1 .v-img img { height: 100%;object-fit: cover;}
#mobile-menu-design ul.list-inline{display:none;}
@media only screen and (max-width: 767px){
    #mobile-menu-design ul.list-inline{display:block;}
    #mobile-menu-design{margin-top:15px;}
    #mobile-menu-design ul.list-inline{width:73%;    display: inline-block;}
    #mobile-menu-design ul.list-inline li{width:auto;margin-right:3px;}
}
</style>
    <div class="content-wrapper">
        <div class="container" id="">

            <div class="row">
                <div class="col-lg-12" id="reward_1">
                    <!-- Featured Videos -->
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="{{ URL::to('rewards') }}">
                                                <span class="visible-xs"> All Rewards video </span>
                                                <span class="hidden-xs"> All Rewards video </span>
                                            </a>
                                        </li>
                                        <li><a href="{{ URL::to('rewards/most-watched') }}"  class="color-active">Most watched </a></li>
                                        <li> <a href="{{ URL::to('participate-video') }}" class="promote_your_video1" style="background:#28b47e;"> Participate Now</a> </li>
                                        <li> <a href="{{ URL::to('all-top-entertainer') }}" class="promote_your_video1" style=" background:#28b47e;"> Top
                                                Entertainer List</a> </li>
                                    </ul>
                                </div>                                     
                                <div class="col-lg-2 col-sm-2 col-xs-12" id="mobile-menu-design">
                                        <ul class="list-inline">

                                     <li>  <a href="{{ URL::to('rewards') }}" class="">
                                                <span class="visible-xs"> All Rewards video </span>  </a> </li>
                                        <li><a href="{{ URL::to('rewards/most-watched') }}" class="color-active">Most watched </a></li>
                                       
                                    </ul>
                                    
                                <div class="btn-group pull-right bg-clean">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Sort by <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                            <li><a onclick="rewardVideo('recentdata')"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                            <li><a onclick="rewardVideo('topviwed')"><i class="cv cvicon-cv-view-stats"></i> Top Viewed</a></li>
                                            <li><a onclick="rewardVideo('topliked')"><i class="fa fa-thumbs-up"></i> Top liked</a></li>
                                            <li><a onclick="rewardVideo('toptranding')"><i class="fa fa-heart-o"></i> Top Trending</a></li>
                                            <li><a onclick="rewardVideo('toplongest')"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            </div>
                        </div>
                        
                        
                        
                        
                        
     
<style>
.trending-rewards-tags {margin-top:15px;}
.trending-rewards-tags ul{list-style:none;text-align:left;margin:0 0 15px 0;padding:0px;}
.trending-rewards-tags li{display:inline-block;text-align:left;}
.trending-rewards-tags li a{   background: #e5e5e5;border: 0px;color: #000;line-height: 27px;height: 25px;padding: 2px 10px 0 10px;margin-bottom: 1px;font-size: 12px;}
#reward_tags .owl-item {margin-right:5px;width: auto !important;}
.tag_head{margin-right: 5px;float: left;width: 8%;line-height: 27px;}
#reward_tags{float:right;width:91.1%;}
</style>                    
<div class="trending-rewards-tags">
   <div class="span12" id="crousal_wrap" style="width:100%">
     <div class="tag_head">Trending Tags: </div>    
        <ul id="reward_tags" class="owl-carousel" style="display:block">
             
            @if(isset($tags) && count($tags)>0)
             @foreach($tags as $tag)   
            <li class="item"><a href="{{url('/reward-trending-tags/'.$tag)}}">{{$tag}}</a></li>
            @endforeach
            @endif
           
      </ul>
    </div>
</div>
                   
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        

                        <div class="cb-content videolist" id="home_page_video">
                            @if (!empty($rewardMostWatchedVideos))
                                <div>
                                    <div id="all">
                                        @include('elements.reward.reward') 
                                    </div>
                                    <div style="clear: both; display: flex; justify-content:flex-end">
                                        {!! $rewardMostWatchedVideos->links() !!}
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>
            </div>
        </div>
    </div>
    
     
 
       
       
    <script> 
        function rewardVideo(order_by){
            $.ajax({
                type: "GET",
                url: "{{ URL::to('show_all_reward_order_by_list') }}",
                data: {'keyword':order_by},
                cache: false,
                success: function(response)
                {
                    $('#all').html(response);
                    console.log(response);
                }
            });
        }
    </script>
@endsection
