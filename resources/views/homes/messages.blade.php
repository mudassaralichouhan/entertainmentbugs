@extends('layouts.home')
@section('content')

	<link href="https://www.entertainmentbugs.com/live_website/backup/dev_html/css/chat.css" rel="stylesheet">
 
<style>
#live-video-user-list-home{display:none;}
.messages-all{background:#f4f3f3}
.navigation{display:none;}
.layout .content {padding: 20px 0px 30px 0;}
.chat-body.no-message{    overflow: scroll;}
.chat-header{    background: #fff;}
.dropdown-menu .dropdown-item {padding: 10px 33px;}
.layout .content .sidebar-group .sidebar .list-group-item .users-list-body p {font-size: 12px;}
.layout .content .sidebar-group .sidebar .list-group-item .users-list-body h5 {      font-weight: 500;  margin: 4px 0 -1px 0; font-weight: 500;font-size: 13px;}
 
#button-addon2{    padding-top: .55rem;
    position: absolute;
    right: 1px;
    z-index: 90;
    top: 4px;
}
 .input-group.mb-0{width:100%;}
.ms-3 {
    margin-left: 1rem!important;
}
p.p-2 {
    font-size: 10px;
}


.card{background: #fff;
    padding: 0;
    position: fixed;
    bottom: 10px;
    box-shadow:0 2px 15px -3px rgba(0,0,0,0.07),0 10px 20px -2px rgba(0,0,0,0.04);
    right: 70px;
    z-index: 99999;
    width: 350px;
    z-index: 9999;
}
.card-body{padding:0 20px;}

footer{display:none;}
@media  only screen and (max-width: 767px) { 
.navigation{display:block;}
body:not(.rtl) .sidebar-group.mobile-open {
    opacity: 1;
    right: 0;
    visibility: visible;
    margin-top: 50px;
}    
}
</style>



  	
 
 
        <div class="card">
          <div class="card-header d-flex justify-content-between align-items-center p-3"
            style="border-top: 4px solid #ffa900;">
            <h5 class="mb-0">Chat messages</h5>
            <div class="d-flex flex-row align-items-center">
              <span class="badge bg-warning me-3">20</span>
              <i class="fas fa-minus me-3 text-muted fa-xs"></i>
              <i class="fas fa-comments me-3 text-muted fa-xs"></i>
              <i class="fas fa-times text-muted fa-xs"></i>
            </div>
          </div>
          <div class="card-body" data-mdb-perfect-scrollbar="true" style="position: relative; height: 350px">

            <div class="d-flex justify-content-between">
              <p class="small mb-1">Timona Siera</p>
              <p class="small mb-1 text-muted">23 Jan 2:00 pm</p>
            </div>
            <div class="d-flex flex-row justify-content-start">
              <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava5-bg.webp"
                alt="avatar 1" style="width: 45px; height: 100%;">
              <div>
                <p class="small p-2 ms-3 mb-3 rounded-3" style="background-color: #f5f6f7;">For what reason
                  would it
                  be advisable for me to think about business content?</p>
              </div>
            </div>

            <div class="d-flex justify-content-between">
              <p class="small mb-1 text-muted">23 Jan 2:05 pm</p>
              <p class="small mb-1">Johny Bullock</p>
            </div>
            <div class="d-flex flex-row justify-content-end mb-4 pt-1">
              <div>
                <p class="small p-2 me-3 mb-3 text-white rounded-3 bg-warning">Thank you for your believe in
                  our
                  supports</p>
              </div>
              <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava6-bg.webp"
                alt="avatar 1" style="width: 45px; height: 100%;">
            </div>

            <div class="d-flex justify-content-between">
              <p class="small mb-1">Timona Siera</p>
              <p class="small mb-1 text-muted">23 Jan 5:37 pm</p>
            </div>
            <div class="d-flex flex-row justify-content-start">
              <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava5-bg.webp"
                alt="avatar 1" style="width: 45px; height: 100%;">
              <div>
                <p class="small p-2 ms-3 mb-3 rounded-3" style="background-color: #f5f6f7;">Lorem ipsum dolor
                  sit amet
                  consectetur adipisicing elit similique quae consequatur</p>
              </div>
            </div>

            <div class="d-flex justify-content-between">
              <p class="small mb-1 text-muted">23 Jan 6:10 pm</p>
              <p class="small mb-1">Johny Bullock</p>
            </div>
            <div class="d-flex flex-row justify-content-end mb-4 pt-1">
              <div>
                <p class="small p-2 me-3 mb-3 text-white rounded-3 bg-warning">Dolorum quasi voluptates quas
                  amet in
                  repellendus perspiciatis fugiat</p>
              </div>
              <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava6-bg.webp"
                alt="avatar 1" style="width: 45px; height: 100%;">
            </div>

          </div>
          <div class="card-footer text-muted d-flex justify-content-start align-items-center p-3">
            <div class="input-group mb-0">
              <input type="text" class="form-control" placeholder="Type message"
                aria-label="Recipient's username" aria-describedby="button-addon2" />
              <button class="btn btn-warning" type="button" id="button-addon2" style="padding-top: .55rem;">
                Send
              </button>
            </div>
          </div>
        </div>
 




<!--https://mdbootstrap.com/docs/standard/extended/chat/-->












<div class="messages-all">

<div class="container">
 
    
<!-- layout -->
<div class="layout">

    <!-- navigation -->
    <nav class="navigation">
        <div class="nav-group">
            <ul>
                 
                <li>
                    <a data-navigation-target="chats" class="active" href="#">
                        <i class="fa fa fa-bell"></i>
                    </a>
                </li>
                
            </ul>
        </div>
    </nav>
    <!-- ./ navigation -->

    <!-- content -->
    <div class="content">

        <!-- sidebar group -->
        <div class="sidebar-group">

      <!-- Chats sidebar -->
            <div id="chats" class="sidebar active">
                <header>
                    <span>Chats</span>
                    
                </header>
                <form action="">
                    <input type="text" class="form-control" placeholder="Search chat">
                </form>
                <div class="sidebar-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <figure class="avatar avatar-state-success">
                                <img src="https://soho.laborasyon.com/no-chat/dist/media/img/man_avatar1.jpg" class="rounded-circle">
                            </figure>
                            <div class="users-list-body">
                                <h5>Patsy Paulton</h5>
                                <p>Traditional heading elscas sdscsd sdcsdsc</p>
                                <div class="users-list-action">
                                    <div class="new-message-count">2</div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item open-chat">
                            <div>
                                <figure class="avatar">
                                    <img src="https://soho.laborasyon.com/no-chat/dist/media/img/man_avatar1.jpg" class="rounded-circle">
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Karl Hubane</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar avatar-state-warning">
                                    <img src="https://soho.laborasyon.com/no-chat/dist/media/img/man_avatar1.jpg" class="rounded-circle">
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Jennica Kindred</h5>
                                <p>I know how important this file is to you. You can trust me ;)</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar">
                                    <span class="avatar-title bg-info rounded-circle">M</span>
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Marvin Rohan</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar"> 
                                      <img src="https://soho.laborasyon.com/no-chat/dist/media/img/man_avatar1.jpg" class="rounded-circle">
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Frans Hanscombe</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- ./ Chats sidebar -->

            <!-- Friends sidebar -->
            <div id="friends" class="sidebar">
                <header>
                    <span>Friends</span>
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a class="btn btn-light" href="#" data-toggle="modal" data-target="#addFriends">
                                <i class="ti-plus btn-icon"></i> Add Friends
                            </a>
                        </li>
                        <li class="list-inline-item d-lg-none d-sm-block">
                            <a href="#" class="btn btn-light sidebar-close">
                                <i class="ti-close"></i>
                            </a>
                        </li>
                    </ul>
                </header>
                <form action="">
                    <input type="text" class="form-control" placeholder="Search chat">
                </form>
                <div class="sidebar-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar">
                                    <img src="ahttps://soho.laborasyon.com/no-chat/dist/media/img/women_avatar5.jpg" class="rounded-circle">
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Harrietta Souten</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar">
                                    <span class="avatar-title bg-success rounded-circle">A</span>
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Aline McShee</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar">
                                    <img src="ahttps://soho.laborasyon.com/no-chat/dist/media/img/women_avatar1.jpg" class="rounded-circle">
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Brietta Blogg</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar">
                                    <img src="https://soho.laborasyon.com/no-chat/dist/media/img/man_avatar1.jpg" class="rounded-circle">
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Karl Hubane</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar">
                                    <img src="ahttps://soho.laborasyon.com/no-chat/dist/media/img/man_avatar2.jpg" class="rounded-circle">
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Jillana Tows</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar">
                                    <span class="avatar-title bg-info rounded-circle">AD</span>
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Alina Derington</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar">
                                    <span class="avatar-title bg-warning rounded-circle">S</span>
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Stevy Kermeen</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar">
                                    <img src="https://soho.laborasyon.com/no-chat/dist/media/img/man_avatar1.jpg" class="rounded-circle">
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Stevy Kermeen</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar">
                                    <img src="https://soho.laborasyon.com/no-chat/dist/media/img/man_avatar5.jpg" class="rounded-circle">
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Gloriane Shimmans</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div>
                                <figure class="avatar">
                                    <span class="avatar-title bg-secondary rounded-circle">B</span>
                                </figure>
                            </div>
                            <div class="users-list-body">
                                <h5>Bernhard Perrett</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">Open</a>
                                            <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                            <a href="#" class="dropdown-item">Add to archive</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- ./ Friends sidebar -->

            <!-- Favorites sidebar -->
            <div id="favorites" class="sidebar">
                <header>
                    <span>Favorites</span>
                    <ul class="list-inline">
                        <li class="list-inline-item d-lg-none d-sm-block">
                            <a href="#" class="btn btn-light sidebar-close">
                                <i class="ti-close"></i>
                            </a>
                        </li>
                    </ul>
                </header>
                <form action="">
                    <input type="text" class="form-control" placeholder="Search favorites">
                </form>
                <div class="sidebar-body">
                    <ul class="list-group list-group-flush users-list">
                        <li class="list-group-item">
                            <div class="users-list-body">
                                <h5>Jennica Kindred</h5>
                                <p>I know how important this file is to you. You can trust me ;)</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">View Chat</a>
                                            <a href="#" class="dropdown-item">Forward</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="users-list-body">
                                <h5>Marvin Rohan</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">View Chat</a>
                                            <a href="#" class="dropdown-item">Forward</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="users-list-body">
                                <h5>Frans Hanscombe</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">View Chat</a>
                                            <a href="#" class="dropdown-item">Forward</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="users-list-body">
                                <h5>Karl Hubane</h5>
                                <p>Lorem ipsum dolor sitsdc sdcsdc sdcsdcs</p>
                                <div class="users-list-action action-toggle">
                                    <div class="dropdown">
                                        <a data-toggle="dropdown" href="#">
                                            <i class="ti-more"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a href="#" class="dropdown-item">View Chat</a>
                                            <a href="#" class="dropdown-item">Forward</a>
                                            <a href="#" class="dropdown-item">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- ./ Stars sidebar -->

        </div>
        <!-- ./ sidebar group -->

        <!-- chat -->
        <div class="chat">
            <div class="chat-header">
                <div class="chat-header-user"> 
                        <img src="https://soho.laborasyon.com/no-chat/dist/media/img/man_avatar3.jpg" class="rounded-circle ere">
                     
           <Style>
         .rounded-circle.ere {
    width: 50px;
    padding-top: 15px;
    margin-left: 14px;  margin-bottom: 15px;
    margin-right: 20px;
    height: 50px;
    position: relative;
    object-fit: cover;
}
body:not(.rtl) .sidebar-group.mobile-open {
    z-index: 99999;
}

.btn.btn-secondary {
    background: #0abb87 !important;
    border-color: #0abb87 !important;
}

           </Style>         
                    
                    
                    <div>
                        <h5>Karl Hubane</h5>
                        <small class="text-muted">
                            <i>Online</i> <i class="fa fa-circle" style="color: green"></i>
                        </small>
                    </div>
                </div>
                <div class="chat-header-action">
                    <ul class="list-inline">
                        
                        <li class="list-inline-item">
                            <a href="#" class="btn btn-secondary" data-toggle="dropdown">
                                <i class="caret"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" style="width:200px">
                                <a href="#" data-navigation-target="contact-information" class="dropdown-item">Profile</a>
                                 <a href="#" class="dropdown-item">Mute</a>
                                 <a href="#" class="dropdown-item">Delete</a>
                                 
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="chat-body no-message">
                
               
                <div class="messages">
                    <div class="message-item outgoing-message">
                        <div class="message-content">
                            Hey, Maher! I'm waiting for you to send me the files.
                        </div>
                        <div class="message-action">
                            Am 09:34 <i class="fa fa-check"></i> <i class="fa fa-check" style="margin-left: -4px;"></i>
                        </div>
                    </div>
                    <div class="message-item">
                        <div class="message-content">
                            I'm sorry :( I'll send you as soon as possible.
                        </div>
                        <div class="message-action">
                            Pm 14:20
                        </div>
                    </div>
                    <div class="message-item outgoing-message">
                        <div class="message-content">
                            I'm waiting. Thank you :)
                        </div>
                        <div class="message-action">
                            Pm 14:25 <i class="fa fa-check"></i>
                        </div>
                    </div>
                    <div class="message-item">
                        <div class="message-content">
                            I'm sending files now.
                        </div>
                        <div class="message-action">
                            Pm 14:20
                        </div>
                    </div>
                    <div class="message-item">
                        <div class="message-content message-file">
                           
                            <div>
                                <div>important_documents.pdf <i class="text-muted small">(50KB)</i></div>
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#">Download</a></li>
                                    <li class="list-inline-item"><a href="#">View</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="message-action">
                            Pm 14:25
                        </div>
                    </div>
                    <div class="message-item outgoing-message">
                        <div class="message-content">
                            Thank you so much. After I review these files, I will give you my opinion. If there's a problem, you can send it back. Good luck with!
                        </div>
                        <div class="message-action">
                            Pm 14:50 <i title="Message could not be sent" class="fa fa-check"></i><i class="fa fa-check" style="margin-left: -4px;"></i>
                        </div>
                    </div>
                    <div class="message-item">
                        <div class="message-content">
                            I can't wait
                        </div>
                        <div class="message-action">
                            Pm 15:00
                        </div>
                    </div>
                    <div class="message-item outgoing-message">
                        <div class="message-content">
                            I know how important this file is to you. You can trust me ;)
                        </div>
                        <div class="message-action">
                            Pm 14:50 <i class="fa fa-check"></i> 
                        </div>
                    </div>
                </div>
             
            </div>
             <div class="chat-footer">
                <form action="">
                    <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2">
                    <div class="form-buttons">
                        <button class="btn btn-light btn-floating" type="button">
                            <i class="fa fa-paperclip"></i>
                        </button>
                        <button class="btn btn-light btn-floating" type="button">
                            <i class="fa fa-microphone"></i>
                        </button>
                        <button class="btn btn-primary btn-floating" type="submit">
                            <i class="fa fa-send"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- ./ chat -->

        <div class="sidebar-group">
            <div id="contact-information" class="sidebar">
                <header>
                    <span>About</span>
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="#" class="btn btn-light sidebar-close">
                                <i class="ti-close"></i>
                            </a>
                        </li>
                    </ul>
                </header>
                <div class="sidebar-body">
                    <div class="pl-4 pr-4 text-center">
                        <figure class="avatar avatar-state-danger avatar-xl mb-4">
                            <img src="https://soho.laborasyon.com/no-chat/dist/media/img/women_avatar5.jpg" class="rounded-circle">
                        </figure>
                        <h5 class="text-primary">Frans Hanscombe</h5>
                        <p class="text-muted">Last seen: Today</p>
                    </div>
                    <hr>
                    <div class="pl-4 pr-4">
                        <h6>About</h6>
                        <p class="text-muted">I love reading, traveling and discovering new things. You need to be happy in life.</p>
                    </div>
                    <hr>
                    <div class="pl-4 pr-4">
                        <h6>Phone</h6>
                        <p class="text-muted">(555) 555 55 55</p>
                    </div>
                    <hr>
                    <div class="pl-4 pr-4">
                        <h6>Media</h6>
                        <div class="files">
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="#">
                                        <figure class="avatar avatar-lg">
                                        <span class="avatar-title bg-warning">
                                            <i class="fa fa-file-pdf-o"></i>
                                        </span>
                                        </figure>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <figure class="avatar avatar-lg">
                                            <img src="ahttps://soho.laborasyon.com/no-chat/dist/media/img/women_avatar1.jpg">
                                        </figure>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <figure class="avatar avatar-lg">
                                            <img src="https://soho.laborasyon.com/no-chat/dist/media/img/women_avatar3.jpg">
                                        </figure>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <figure class="avatar avatar-lg">
                                            <img src="https://soho.laborasyon.com/no-chat/dist/media/img/women_avatar4.jpg">
                                        </figure>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <figure class="avatar avatar-lg">
                                        <span class="avatar-title bg-success">
                                            <i class="fa fa-file-excel-o"></i>
                                        </span>
                                        </figure>
                                    </a>
                                </li>
                                <li class="list-inline-item">
                                    <a href="#">
                                        <figure class="avatar avatar-lg">
                                        <span class="avatar-title bg-info">
                                            <i class="fa fa-file-text-o"></i>
                                        </span>
                                        </figure>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <hr>
                    <div class="pl-4 pr-4">
                        <h6>City</h6>
                        <p class="text-muted">Germany / Berlin</p>
                    </div>
                    <hr>
                    <div class="pl-4 pr-4">
                        <h6>Website</h6>
                        <p>
                            <a href="#">www.franshanscombe.com</a>
                        </p>
                    </div>
                    <hr>
                    <div class="pl-4 pr-4">
                        <h6>Social Links</h6>
                        <ul class="list-inline social-links">
                            <li class="list-inline-item">
                                <a href="#" class="btn btn-sm btn-floating btn-facebook">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="btn btn-sm btn-floating btn-twitter">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="btn btn-sm btn-floating btn-dribbble">
                                    <i class="fa fa-dribbble"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="btn btn-sm btn-floating btn-whatsapp">
                                    <i class="fa fa-whatsapp"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="btn btn-sm btn-floating btn-linkedin">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="btn btn-sm btn-floating btn-google">
                                    <i class="fa fa-google"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="btn btn-sm btn-floating btn-behance">
                                    <i class="fa fa-behance"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="btn btn-sm btn-floating btn-instagram">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    
                </div>
            </div>
        </div>

    </div>
    <!-- ./ content -->

					
					 

            </div>
        </div>
 
        </div> 


<!-- ./ layout -->

<!-- JQuery -->
<script src="https://soho.laborasyon.com/no-chat/vendor/jquery-3.4.1.min.js"></script>
<!-- Popper.js -->
<script src="https://soho.laborasyon.com/no-chat/vendor/popper.min.js"></script>
<!-- Bootstrap -->
<script src="https://soho.laborasyon.com/no-chat/vendor/bootstrap/bootstrap.min.js"></script>
<!-- Soho -->
<script src="https://soho.laborasyon.com/no-chat/dist/js/soho.min.js"></script>




@endsection
