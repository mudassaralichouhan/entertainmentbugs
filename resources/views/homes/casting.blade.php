@extends('layouts.home')
@section('content')
  

<link href="https://www.entertainmentbugs.com/public/css/casting.css" rel="stylesheet">
 
 <style>#live-video-user-list-home{display:none}
 
 .row.home_page_second_block .col-lg-2 {
    margin-bottom: 0;
}

</style>
 <div class="content-wrapper">
         <div class="containers">
            <div class="casting-menu">
               <ul>
                    <li><a href=""> Artist Profile</a></li>
                    <li><a href=""> Artist Videos</a></li>
                    <li><a href=""> Artist Gallery</a></li> 
                    <li><a href="">Influencer</a></li>
                    <li><a href="">Actor / Model</a></li>
                    <li><a href="">Production House</a></li>
                    <li><a href="">Find Autions</a></li> 
                   <li><a href="">Post Aution</a></li> 
               </ul>
            </div>
         </div>


 
 <div data-ad-block data-ad-type='ad' data-ad-mode='image' data-ad-size='1080x1080'></div>
 



<div id="casting-top-banner" class="banner"> 
    <div class="intro-banner main_banner">
        <div class="containers">
           <!-- Intro Headline -->
           <div class="row">
              <div class="col-md-6">
                 <div class="banner-headline-alt">
                     <h4>Create your profile as a artist</h4>
                    <h3>Promote your talent and regiter with us</h3>
                    <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day </p>
                <a href="#" class="button" style="border-color:#6c146b; color:#6c146b;width: 240px;">Create Profile</a>
                </div>
              </div>
              
              <div class="col-md-6">
                 <div class="banner-headline-alt">
                     <h4>Production House</h4>
                    <h3> Find artist for your new movies</h3>
                    <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day </p>
                <a href="#" class="button" style="border-color:#6c146b; color:#6c146b;width: 240px;">  Find Artist</a>
                </div>
              </div> 
           </div>
           <!-- Search Bar -->
        </div> 
     </div> 
</div>


 
         <div class="audition_main_list" id="audition_main">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <!-- Featured Videos -->
                     <div class="content-block">
                        <div class="row home_page_seventh_block">
                           <div class="col-lg-12">
                              <!-- Popular Channels -->
                              <div class="content-block">
                                 <div class="content-block" style="border:0px">
                                    <div class="cb-header">
                                       <div class="row">
                                          <div class="col-lg-10 col-sm-10 col-xs-8">
                                             <ul class="list-inline">
                                                <li> 
                                                   <a href="#" class="color-active">
                                                   <span class="visible-xs">Actor / Model</span>
                                                   <span class="hidden-xs">Actor / Model</span>
                                                   </a>
                                                </li>
                                                <li><a href="#">Incluencer</a></li>
                                                <li><a href="#">Modeling</a></li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 @foreach($modeling as $m)
                                 <div class="cb-content chanels-row" id="result-block-main">
                                    <div class="photo-block">
                                       <a href="" class="profile-p"><img src="{{$m->profile_photo}}"></a> 
                                       <div class="info-p">
                                          <h5>Justine Salette</h5>
                                          <p>Mumbai - Maharastra</p>
                                          <p>Age: 30Years</p>
                                          <p>Height: 5' 2"</p>
                                          <p>Hindi,Tamil,English</p>
                                       </div>
                                    </div>
                                    @endforeach
                                 </div>
                              </div>
                              <!-- /Popular Channels -->
                           </div>
                        </div>
						<div style="position:relative"> 
                        <h2 class="middle-head">Find Latest Artist videos</h2>
						 <a href="artist_videos.html" style="position:absolute;right:-12px;top:0px; color:#fff; padding:4px 15px 0 15px; background: #f72e5e !important;">View all</a> 
                      
                        <!-- casting video start-->
                        <div class="row home_page_second_block" style="padding-top:30px;">
                           <div class="col-lg-12">
                              <div class="cb-content videolist" id="home_page_video">
                                 <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                       <a href="#" class="user_img"><img src="https://entertainmentbugs.com/dev_html/images/user.jpg" alt=""></a>
                                    </div>
                                    <div class="b-video">
                                       <div class="v-img">
                                          <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-1.png" alt=""></a>
                                          <div class="time">3:50</div>
                                       </div>
                                       <div class="v-desc">
                                          <p>Actor / Model</p>
                                          <a href="#">Man's Sky: 21 Minutes of New Gameplay - IGN First</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                       <a href="#" class="user_img"><img src="https://entertainmentbugs.com/dev_html/images/user.jpg" alt=""></a>
                                    </div>
                                    <div class="b-video">
                                       <div class="v-img">
                                          <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-2.png" alt=""></a>
                                          <div class="time">15:19</div>
                                       </div>
                                       <div class="v-desc">
                                          <p>Actor / Model</p>
                                          <a href="#">GTA 5: Michael, Franklin, and Trevor in the Flesh</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                       <a href="#" class="user_img"><img src="https://entertainmentbugs.com/dev_html/images/user.jpg" alt=""></a>
                                    </div>
                                    <div class="b-video">
                                       <div class="v-img">
                                          <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-3.png" alt=""></a>
                                          <div class="time">4:23</div>
                                       </div>
                                       <div class="v-desc">
                                          <p>Influencer</p>
                                          <a href="#">Battlefield 3: Official Fault Line Gameplay Trailer</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                       <a href="#" class="user_img"><img src="https://entertainmentbugs.com/dev_html/images/user.jpg" alt=""></a>
                                    </div>
                                    <div class="b-video">
                                       <div class="v-img">
                                          <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-4.png" alt=""></a>
                                          <div class="time">7:18</div>
                                       </div>
                                       <div class="v-desc">
                                          <p>Theater</p>
                                          <a href="#">Batman Arkham City: Hugo Strange Trailer</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                       <a href="#" class="user_img"><img src="https://entertainmentbugs.com/dev_html/images/user.jpg" alt=""></a>
                                    </div>
                                    <div class="b-video last-row">
                                       <div class="v-img">
                                          <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-5.png" alt=""></a>
                                          <div class="time">23:57</div>
                                       </div>
                                       <div class="v-desc">
                                          <p>Modeling</p>
                                          <a href="#">BATTALION 1944: TAKING ON BATTLEFIELD 5</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                       <a href="#" class="user_img"><img src="https://entertainmentbugs.com/dev_html/images/user.jpg" alt=""></a>
                                    </div>
                                    <div class="b-video last-row">
                                       <div class="v-img">
                                          <a href="#">
                                             <img src="https://entertainmentbugs.com/dev_html/images/video1-6.png" alt="">
                                             <div class="watched-mask"></div>
                                             <div class="watched">WATCHED</div>
                                             <div class="time">7:27</div>
                                          </a>
                                       </div>
                                       <div class="v-desc">
                                          <p>Voiceover</p>
                                          <a href="#">Amazon Blocking VIDEO GAMES for Non-Prime...</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div style="clear:both; overflow:hidden"></div>
                                 <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                       <a href="#" class="user_img"><img src="https://entertainmentbugs.com/dev_html/images/user.jpg" alt=""></a>
                                    </div>
                                    <div class="b-video last-row">
                                       <div class="v-img">
                                          <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-7.png" alt=""></a>
                                          <div class="time">12:58</div>
                                       </div>
                                       <div class="v-desc">
                                          <p>Anchor</p>
                                          <a href="#">Amazing Facts About Nebulas Inside Deep Universe</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                       <a href="#" class="user_img"><img src="https://entertainmentbugs.com/dev_html/images/user.jpg" alt=""></a>
                                    </div>
                                    <div class="b-video last-row">
                                       <div class="v-img">
                                          <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-8.png" alt=""></a>
                                          <div class="time">9:47</div>
                                       </div>
                                       <div class="v-desc">
                                          <p>Actor / Model</p>
                                          <a href="#">Cornfield Chase - Outlast II Official Gameplay</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                       <a href="#" class="user_img"><img src="https://entertainmentbugs.com/dev_html/images/user.jpg" alt=""></a>
                                    </div>
                                    <div class="b-video">
                                       <div class="v-img">
                                          <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-2.png" alt=""></a>
                                          <div class="time">15:19</div>
                                       </div>
                                       <div class="v-desc">
                                          <p>Dancer</p>
                                          <a href="#">GTA 5: Michael, Franklin, and Trevor in the Flesh</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                       <a href="#" class="user_img"><img src="https://entertainmentbugs.com/dev_html/images/user.jpg" alt=""></a>
                                    </div>
                                    <div class="b-video">
                                       <div class="v-img">
                                          <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-3.png" alt=""></a>
                                          <div class="time">4:23</div>
                                       </div>
                                       <div class="v-desc">
                                          <p>Dancer</p>
                                          <a href="#">Battlefield 3: Official Fault Line Gameplay Trailer</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                       <a href="#" class="user_img"><img src="https://entertainmentbugs.com/dev_html/images/user.jpg" alt=""></a>
                                    </div>
                                    <div class="b-video">
                                       <div class="v-img">
                                          <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-4.png" alt=""></a>
                                          <div class="time">7:18</div>
                                       </div>
                                       <div class="v-desc">
                                          <p>Comedian</p>
                                          <a href="#">Batman Arkham City: Hugo Strange Trailer</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-lg-2 col-sm-6 videoitem">
                                    <div class="user">
                                       <a href="#" class="user_img"><img src="https://entertainmentbugs.com/dev_html/images/user.jpg" alt=""></a>
                                    </div>
                                    <div class="b-video last-row">
                                       <div class="v-img">
                                          <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-5.png" alt=""></a>
                                          <div class="time">23:57</div>
                                       </div>
                                       <div class="v-desc">
                                          <p>Singer / Musician</p>
                                          <a href="#">BATTALION 1944: TAKING ON BATTLEFIELD 5</a>
                                       </div>
                                    </div>
                                 </div>
                                 <div style="clear:both; overflow:hidden"></div>
                              </div>
                           </div>
                           <!-- /Featured Videos -->
                         </div>
						</div>
                     </div>
                     <!--casting video END-->
                     
                            <div style="position: relative;">
                     <h2 class="middle-head">Modelling</h2>
                      <a href="artist_videos.html" style="position:absolute;right:-12px;top:0px; color:#fff; padding:4px 15px 0 15px; background: #f72e5e !important;">View all modelling</a> 
          </div>            
                     
                     
                     
                     <div class="row home_page_seventh_block  casting-home-sep">
                        <div class="col-lg-12">
                           <!-- Popular Channels -->
                           <div class="content-block">
                              <div class="cb-content chanels-row" id="result-block-main">
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/1.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Mumbai - Maharastra</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/2.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Kolkata - West Bengal</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/3.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Kolkata - West Bengal</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/4.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Kolkata - West Bengal</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/5.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Kolkata - West Bengal</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/6.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Kolkata - West Bengal</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/7.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Kolkata - West Bengal</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- /Popular Channels -->
                        </div>
                     </div>
                     <h2 class="middle-head">Dancer Audition Video</h2>
                     <!-- casting video start-->
                     <div class="row home_page_second_block casting-home-sep">
                        <!-- Featured Videos -->
                        <div class="content-block">
                           <style>
                              .row.home_page_second_block.casting-home-sep .videolist{margin-bottom:5px}
                              #blog .col-lg-2{width:100% !important}
                           </style>
                           <div class="cb-content videolist" id="home_page_video">
                              <div id="blog" class="owl-carousel company-post">
                                 <div class="company">
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                       <div class="b-video">
                                          <div class="v-img">
                                             <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-1.png" alt=""></a>
                                             <div class="time">3:50</div>
                                          </div>
                                          <div class="sv-views-progress"> </div>
                                          <div class="v-desc">
                                             <a href="#">Man's Sky: 21 Minutes of New Gameplay - IGN First</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="company">
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                       <div class="b-video last-row">
                                          <div class="v-img">
                                             <a href="#">
                                                <img src="https://entertainmentbugs.com/dev_html/images/video1-6.png" alt="">
                                                <div class="watched-mask"></div>
                                                <div class="watched">WATCHED</div>
                                                <div class="time">7:27</div>
                                             </a>
                                          </div>
                                          <div class="sv-views-progress">
                                             <div class="sv-views-progress-bar"></div>
                                          </div>
                                          <div class="v-desc">
                                             <a href="#">Amazon Blocking VIDEO GAMES for Non-Prime...</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="company">
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                       <div class="b-video">
                                          <div class="v-img">
                                             <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-1.png" alt=""></a>
                                             <div class="time">3:50</div>
                                          </div>
                                          <div class="sv-views-progress"> </div>
                                          <div class="v-desc">
                                             <a href="#">Man's Sky: 21 Minutes of New Gameplay - IGN First</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="company">
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                       <div class="b-video">
                                          <div class="v-img">
                                             <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-1.png" alt=""></a>
                                             <div class="time">3:50</div>
                                          </div>
                                          <div class="sv-views-progress"> </div>
                                          <div class="v-desc">
                                             <a href="#">Man's Sky: 21 Minutes of New Gameplay - IGN First</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="company">
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                       <div class="b-video">
                                          <div class="v-img">
                                             <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-2.png" alt=""></a>
                                             <div class="time">15:19</div>
                                          </div>
                                          <div class="sv-views-progress"> </div>
                                          <div class="v-desc">
                                             <a href="#">GTA 5: Michael, Franklin, and Trevor in the Flesh</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="company">
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                       <div class="b-video">
                                          <div class="v-img">
                                             <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-3.png" alt=""></a>
                                             <div class="time">4:23</div>
                                          </div>
                                          <div class="sv-views-progress"> </div>
                                          <div class="v-desc">
                                             <a href="#">Battlefield 3: Official Fault Line Gameplay Trailer</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="company">
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                       <div class="b-video">
                                          <div class="v-img">
                                             <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-4.png" alt=""></a>
                                             <div class="time">7:18</div>
                                          </div>
                                          <div class="sv-views-progress"> </div>
                                          <div class="v-desc">
                                             <a href="#">Batman Arkham City: Hugo Strange Trailer</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="company">
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                       <div class="b-video last-row">
                                          <div class="v-img">
                                             <a href="#"><img src="https://entertainmentbugs.com/dev_html/images/video1-5.png" alt=""></a>
                                             <div class="time">23:57</div>
                                          </div>
                                          <div class="sv-views-progress"> </div>
                                          <div class="v-desc">
                                             <a href="#">BATTALION 1944: TAKING ON BATTLEFIELD 5</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="company">
                                    <div class="col-lg-2 col-sm-6 videoitem">
                                       <div class="b-video last-row">
                                          <div class="v-img">
                                             <a href="#">
                                                <img src="https://entertainmentbugs.com/dev_html/images/video1-6.png" alt="">
                                                <div class="watched-mask"></div>
                                                <div class="watched">WATCHED</div>
                                                <div class="time">7:27</div>
                                             </a>
                                          </div>
                                          <div class="sv-views-progress">
                                             <div class="sv-views-progress-bar"></div>
                                          </div>
                                          <div class="v-desc">
                                             <a href="#">Amazon Blocking VIDEO GAMES for Non-Prime...</a>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- /Featured Videos -->
                     </div>
                     <!--casting video END-->

       <div style="position: relative;">
                     <h2 class="middle-head">Influencer</h2>
                      <a href="artist_videos.html" style="position:absolute;right:-12px;top:0px; color:#fff; padding:4px 15px 0 15px; background: #f72e5e !important;">View all Influencer</a> 
          </div>              
                     <div class="row home_page_seventh_block  casting-home-sep">
                        <div class="col-lg-12">
                           <!-- Popular Channels -->
                           <div class="content-block">
                              <div class="cb-content chanels-row" id="result-block-main">
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/1.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Mumbai - Maharastra</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/2.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Kolkata - West Bengal</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/3.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Kolkata - West Bengal</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/4.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Kolkata - West Bengal</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/5.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Kolkata - West Bengal</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/6.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Kolkata - West Bengal</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                                 <div class="photo-block">
                                    <a href="" class="profile-p"><img src="https://marketifythemes.net/html/tokyo/img/portfolio/7.jpg"></a> 
                                    <div class="info-p">
                                       <h5>Justine Salette</h5>
                                       <p>Kolkata - West Bengal</p>
                                       <p>Age: 30Years</p>
                                       <p>Height: 5' 2"</p>
                                       <p>Hindi,Tamil,English</p>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- /Popular Channels -->
                        </div>
                     </div>
					 
					 <div  class="row" style="position:relative; margin-top:20px;">
            <img src="https://onthemarcmedia.com/wp-content/uploads/2014/08/video-production-banner.png" style="width:100%">
            <div class="top-banner" style="left: 45px;">
               <h2 style="font-size:32px">Get cast and find artist for your new movies and web series.  </h2>
               <a href="">Register as a production house</a>
            </div>
         </div>
                     <div style="position:relative">
                        <h2 class="middle-head">Trending Production House</h2>
                        <a href="production_house.html" style="position:absolute;right:-12px;top:0px; color:#fff; padding:4px 15px 0 15px; background: #f72e5e  !important;">View All</a> 
                        <div class="row home_page_seventh_block  casting-home-sep">
                           <div class="col-lg-12">
                              <!-- Popular Channels -->
                              <div class="content-block">
                                 <div class="cb-content chanels-row" id="result-block-main">
                                    <div class="casting-agency-main">
                                       <div class="casting-agency-photo-block">
                                          <a href=""> <img src="https://1zhit3xkjyq3ki6yq3sav7ahhz-wpengine.netdna-ssl.com/wp-content/uploads/2018/11/Our-Lady-LTD-Season-1-Epix-400x190.png"></a> 
                                       </div>
                                       <div class="casting-agency-info">
                                          <h5>Nazione + Studio- Photography</h5>
                                          <p>Mumbai - Maharastra</p>
                                          <p>Years: 10Years</p>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                    <div class="casting-agency-main">
                                       <div class="casting-agency-photo-block">
                                          <a href=""> <img src="https://1zhit3xkjyq3ki6yq3sav7ahhz-wpengine.netdna-ssl.com/wp-content/uploads/2018/10/BET-TV-Show-American-Soul-Casting-Call-400x190.jpg"></a> 
                                       </div>
                                       <div class="casting-agency-info">
                                          <h5>Nazione + Studio- Photography</h5>
                                          <p>Mumbai - Maharastra</p>
                                          <p>Years: 10Years</p>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                    <div class="casting-agency-main">
                                       <div class="casting-agency-photo-block">
                                          <a href=""> <img src="https://media-exp1.licdn.com/dms/image/C511BAQHaAg_lbDzy3g/company-background_10000/0/1532093135284?e=2147483647&v=beta&t=-5AQCoxidtGLbTALY0wh9WsdE7-FP51zAGLM1ijq818"></a> 
                                       </div>
                                       <div class="casting-agency-info">
                                          <h5>Perfect Solution Casting Agency</h5>
                                          <p>Mumbai - Maharastra</p>
                                          <p>Years: 10Years</p>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                    <div class="casting-agency-main">
                                       <div class="casting-agency-photo-block">
                                          <a href=""> <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTn88aqMt8hdfHHOUpDI9IIddxOQDQ59iNTIoEXvZwMhxi-sNZjZv7pGizewVBJMPM-8Go&usqp=CAU"></a> 
                                       </div>
                                       <div class="casting-agency-info">
                                          <h5>Style Icon Kolkata</h5>
                                          <p>Mumbai - Maharastra</p>
                                          <p>Years: 10Years</p>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                    <div class="casting-agency-main">
                                       <div class="casting-agency-photo-block">
                                          <a href=""> <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBUVBxgVFBYYGRgYGxodGxgaGRsYHBsiGhkaHhoYHBgiIC4kHCIpHiAeMjclKS4wNTQ0GyQ5QTkxPi0yNjABCwsLEA8QHRISGDwrJCkyMjIyMjI+PjIyMjUwMDUwMjIwMj4+PjI0MDYyMDA1Mj4yMTI+OjI7PjI+MD4+MjI+O//AABEIAM0A9gMBIgACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABgcBBAUCAwj/xABMEAACAQMCAwQGBQcIBwkAAAABAgADBBEFEgYhMQcTQVEUIjJhcYFSVJGT0hcjQmJyobEVMzSCkrLh8BY3U3OiwdEIJCZDZHSDs7T/xAAYAQEBAQEBAAAAAAAAAAAAAAAAAQMCBP/EACURAQEAAQEIAgMBAAAAAAAAAAABAhESEyFBUmGh0QNRMXHBBP/aAAwDAQACEQMRAD8AuaIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICcPWuKrO0O24rojEZCc2cg9DsUFsH4Tn9ovEhsOGmqp/OOwp088wGYE7j8FDH4gCcLsm4dVdJF/WHeXFwWbe/rMq7iBgnxbGSepyB4QO1X7Q7FE3VDXRfpNbVlXn7yk7Gh8QWt5TZraqtQJjdgEFc5xkEAjOD9k6jrlSPOcPQ9AS11S5ekqolc022ryAZQwbC9ADyPLxJgd0nAmjo+q0rqwWtQbejFgGwR7LFTyPPqJwe0jVWo8NMlPJrXLLQpAHBLVORIPgQucHzxIX2N372+qXWmVuTozOg8NyHZUAJ6gjaRy6BjEFxSN3vGVpTvWo73qVF9pKNOpWK467tikL8DOprrVBotc0f5wUqhp/t7G2f8AFiVl2I69bjTmtCVWuahcZ5GqCB0Piy49ny5+eAnmm8YWdewrVqVQlLdS1XKOpQBWY5VlBJwp5DPSaCdpWlFAfSlGfApUB+zZOrV4dotWuCR6t0tNaigY3bNwJJ8dykA+5Z0ktkWnhUUADkAoAHwAECNflJ0r62n9ip+CbWlcZ2Nzed1b1u8fazbVSp7K4yea+8cuplZ9gSg393kA+pT6/tPLeOk0v5UW4CKtRUdNyqASrlSQTjJwV5fE+cDk1uO7BLvumrMtXIXuzRrCpk42rs2bsnIwMc8iSGlWDUQwyARn1lZD81YAj4ESnOIwPy5W/wC1R/uGXLWOKLHyB/hJyGpo+q0bqwWtQcOjdGHLp1BB5gjyM3icCfm3g7X7rS+6uCjNa3G7K55PsYqxU9FdcfMY+I/Qmk6pSudPWtRcPTcZBH7wR4EHkQeko5tbjKzS/FF3darY20zQrh2z0Kp3eSOvMeRncpVA1MMM4IB5gg8xnmpGQfcecgHElMflc04+PdVf3LVx/Ez12u8VPZ6MtKixWrcbgGHIoq43sp8GOQAfDJI5gQJFq3GNjbXHd1a695nHdoGqPk+BRASD7jNCt2jWCY716tME4BqW9ZAfmUnrgLhSlZaMmFBruoarUPNizDJUN4KOmB5Z6mSitRV6JV1DKwwVYAgjyIPIwNLRtct7u3NS2qrUUHBK55HGcEEAg4nTkd4X4dSyqXK0gFp1Ku9EH6INOmCvuG8NgeREkUBERAREQEREBERARExmBVnbzSY8O0GA5LWwfduRsfwkq7NrtanBFqVPs0wh9xQlSPtH7xOtxBo9O70h7erna4xkdVI5qw94IBlT6GdQ0K+anVoNcWjNuL0gWwem9fosRjKtgHAwfEhdkSEJ2oaWaO43BB+iadTcD5YCn93Ka1jxwr0rq7dai01CpbUWU76pQOS60wM+szAZ6AJzIwYHL17Wlq9paA0q9ajYKSVoUmrfnqg5FgOgAxj9ZDInxfq/cce0dRpULmkpKlxWotSLFRsqKu7kc08fMyweyOiBoNSq7ZuLirUq1lIw6kuyqCOoBwWGfpmbParowueD6n06JFRPMleRUee5SQB54gS+3rq9BXQgq6hlI8QwyD9kqTtF7Nm7572wyGB3vRXIORzL0seOee3z6eU88LcU1V7PKtBg63NuuKIZXBqLuBQLyGSvNdo54Akvtu0vTm08VGqlX8aBRjVDdNgUD1jnlkcoGh2S8XPe6a1Kud1aht9fxdWzhm/WBBBPjyPXMsNukrbss0CrTvLq9q02o+kuTTpMMMqF2fLDw6gAfqnzEn1/e06VsXqMFUZ6+JwTgeZ90UU7/wBn/wDp93+xT/vPLulGdi1cW2o3AuA1IOibWdWVSVY5G4jGfW6fGXezgKSTgAZz4fGBTPEf+vK3+NH+4ZcV6cWbnyVv4GUrr12G7XaVwoZqKPRBqBWKDCgFtwGCAT19xls6rq9FdGep3iFSjhSp3biFPqqBzY58BJyOaG9nukUbzsvp0K67kZqvxU96+HU+BGevx8MyG21e74e4j2ODUtapzy9l1+mueS1FHVfHp0IInHY7eKvCyW7kpVV6nqOCrEE7wVB6jBPMeRkv1/RKN7pj0K67kboR7SnwZT4MP8DkEiURC5v6dz2gaZXoOGpvRuSrD3Icg+RB5EHmCDIn2+U2GrWreBpuB8VcE/uInvhHhWvp/aXRpVSWp4rtScZ2ODTIOB0V+m5fcOowZY3HfCqahohpEhXQ7qbn9FsYwcfokcj8j4QO5pd0tXTadRDlXRGUjyZQRNLiSvcUtLqVrdqQNNHcrURnDbV3Yyrrt6Hnz6ytOFdevNIU2mo29U26k7K6KXVAScjcOTIevgy8+R6CT63x/p1TQKy0rgO9Sk6oiq+9mdCqqF25zkjrFHy7N+KrzUlqVKvo6U6bBdqU6m5iVJ9o1CFA5eBzz6SQ8Qa+tteWyHH56ptb3LjGf7bJ8syIdiNhVpaJXNWm6b6ildyldw2DmM9R751Kunen3d7U8FXuKB8Ayes7D/5NvP8AVnOdsmk/L0f58cLbc/xP7wToCJyuGdR7/RKVQ+0Vww8mX1XH9oGdXwll1jDLHZtn09RMTMqEREBMGZiBDdf0+kOI7EbF/O1q3eZGe8/7vVYB8+0AwBAPIYGOkxb25p8X1adCnTKLaWwCuzAAb668sK2chVBJ67V6yVVbSm9RWemjMvssyglfepI5fKPQ6fpBqd2m8jBfaNxHkWxkj3QK6oU88I2nqqXa/ZSGPqnNzVVkLYJ24UDGOigYk/0lQtiqhUXGQVptuVSGbcqtgcgc+Ax0wMTB0i3NEIaFIqDkL3abQT1O3GM++RntJ1VbLg11pAI9X81TVBtwXzuZQvTC7jkeOIEpvd4tmNJFZwPVDHaCfecZHKcf0rU/q9v98/4Jz+yvXfS+Eqe45qUfzTk8ydgGxiTzOVK8/MGSy9ukpWj1KjBURSzMegAGSfsks15tMM5jNNmX9uH6Vqf1e3++f8Ex6Vqf1e3++f8ABIdo1/ea5f1GWs9rY032gUjtq1D12mp4HGCccgGAweslB7PLEp7Nbd9P0mvu+Pt4z8pNnu63s6J59tr0rU/q9v8AfP8Agnn0jUs59Gts+ffN+CcPReHLiy4xpgXdeta1KdTalR2fYyBcKwztPIkggDoR4Anu8eWwPDFxUDOj06Lujo7oQUUsPZYZ5jxjTub2dE8+2fStT+r2/wB8/wCCY9L1P6vb/fP+CQPsu01r/RKlS4u7zctUoNtzUUY2I3TPmTJ7wvoD2le4DVqtZXdSjVXNRwoUZUt7mLY5Dr8y2e5vp0Tz7Y9L1P6vb/fP+CPStT+r2/3z/gkQ7VeI61jxDZ1aLdEq70OdrqWTKsP4HqDgyc8LcR0b7TBWon3OhxuRvFW/5HxEbPc3s6J59tb0vU/q9v8AfN+CY9L1PP8AR7f75/wT68aWgfh24cM6vTo1GRkd0KsqFlOVYZ5gdZzOy+mTwjRrO71KlQOWd3dyfzjgAbmIGAAOXl7407m+nRPPtvG71PH9Ht/vn/BM+l6n9Xt/vn/BIvxVxfcVuJl0vTmC1C2KtcjdswMuFHQbV6nrnkMHnO3bdn9t3I9IqXNxU/SqVK9ZST44VXAA8hz+JjS/ZvZ0Tz7bfpep/Vrf75/wT16Xqf1e3++f8Ejet8HPa3FGvZXNzTVa1EVKJrOyMr1UUkZOeW7mDnI8sc9DtV0O4o2XpdnXuUVeVWktaptAJwKijd6uDyIHLmDywctO5vZ0Tz7TL0rU/q9v98/4Jjv9S+rW33rfgnvgbXEvOGqVVORA2OuSxR0A3KSSSfAjPPDCRfVqb6h2gdzQq1qdC2QekvTqugdicrSAU4z4Ej9bxAjZv2b6dE8+0iq3GpmmQKFuCQcHvmOPfjZOpw9pno2j06XVlX1j9JjzdvmxM+tNqVDZS3YLkhAzMxYgZPrMSScDxM3o2eOuqZ/LbNJJJ+eHNE6elXtvdVfRWoGlUqNUCVN+VLgbgCOQG7J+c+tS61NKZZ0tMAZJ7yooAHiSVwJ3NTv0oWLVahIVRkkDJ8uQkco2Fa/qCpdA0rcHK2wPrP5GsR4eOwfP3yzThG2GW1NrOTSc/t74b4gubq4P5hBRUkGsKhIYj/ZgoN4z48hJWJ4o0gqAKAABgADAAHQAeE+s6ksnFh8ucyy1xmk+mYiJWZERAREQEruswve04ISDR0+mSwPRqtUYwfPC/YaZky1/VFtdFq3DdKaM2PMgeqvzbA+cgfAnBFvX4fW5vqK1a9yzViz56VDlcYI6j1v60CP8CVv5N7Sq9ix/N1iVTyyPXonPvRivxaTXtfrMnAdbaSN7U1OPIupP24x85B+1jhSnZej3dlTFIK+1tufVcHfTfnnxDfYssO6RNY4C9UgekUwR4hKikHB/ZqLg+7Mg5/Yyq/6CU9uMl6u747yOf9XbJxXq7KRbazY8FGSfgMykuy/iT+TtSq2F7miGfIL8hTfABDHoFZQuG6cgehzLwVgVBHMHxlEUt+P7N9U9GTvjX3Mvddy4YFclgcjAwAfGb3G5zwXdn/09X+40rTh7/XpX/arf/XLL46OODbz/ANvV/ehiiteyG7vE0CqLW3pVV74ks9fuiG2J6oXYcjGOefGWTwpqVzXo1zdUlovTrGmEU7sKKdNgS+fWyWJyMcsSHdgzD/RisPEXBJHuNOnj+B+yWKt5T/lI0R7ewVGAxyBO1S3jk4OP2T5QIRxbRSr2madSqKGRqV0GVhkMGpOMEfKQnWtKutB4gFza5e2qHGDkggnPc1PeP0X/AMRJpxM+O1rTf93W/wCJKgk7v7OnWtGpVVDI4IZSMgg/56+Egj1PX6N/wZXq0TyNGqHU+0jd22VYef8AHrHZmoHAlrj/AGZP2uxP75VfEeiXWh6k9W2Yva11ZDu5ghgR3dQD9IZJVvHHxEtns4H/AIGtP90P4mUVV2J1TU45r1H5s1Co5P6zVqRJ/eZfkoPWbWrofH4ulRmtqjsQR0ZH5vSz0DL1APXap85dej6xRurIVbd1dCOoPMHyYdVPuMDoT43FFXoMjqGVgVYEZBDDBBHkRNHWtYS2tlZyNzulOmpbBdnYKqjl78nlyAM6sCgk1CtoHE1zbopenWTdRXrzOe5fGeeDuVvE7fhLa4L0H0PQkRjuqsS9Z+pao/NyW8cdAfIe+Vn2wEflAs/PZS//AEPj/nLqrVAtIsTgAEk+QHMmOQjSnvuN28VtKWP69bmfsQD7Z99U4po0bnuwHquObrSXeaa+LPjpjy6+6Rrh23vLmnUZSbelcVHqPV/81w2Aq0wfYAQD1j58pNtL0mjb22ykgUdSerMfpMx5sfeZnjbZwe35scMLJlddJJpP7f216zUr3QnFNwyVUZQwPQkEfIg+B6YnjhG9NXh2i7e0F2N5hkJVs/MTT1Dh10uTXsXFKqebUz/M1f2lHRv1hNPge7f026o1KbUmFQVQjc8d4PX2n9Jd6nBHnJrZlNXOxjfitxvCWXTnOXpNJmYHSZmryEREBERAREQOHrvDNC8Tbcd46ZB2Cq6pkDkdqkA/Ob2maetCzWkhbYgCqGYttCgBVBPPAAm9EDma5otG7sDRrqWpkglQzLnacjmpB6zS0HhS3ssi27xVJyUNV2QnGM7WJGf+g8pIIgcbW+G7S7pgXNFKmBgMeTr7lcYYfbOLbdnltTTbRr3tJfoU7l1UZ90mcQI1oXBdnaXhq0qZNU5zVd2qOd3tHLHkT4kdZ0ta0aldWpp1t5psMMquyBhkHDbSCRy6TpxAhdDsz06m2adOohPileqp+GQ862gcK21nWqPQVt1XbvZ3Zyducc2J8zO9ECNXvBlrV1Jbip3rVkxsfvqgKYOQFwwAGSftkgoUttILljjxY5J+J8Z9Ygal9ZpWtGp1VDIwIZW5gg/56+E86Vp1O302nQp52U1Crk5OB0yfGbsQNW8s6dW3NOqiujdVYBlPxBkT/JpYrXL0e/oE9e5rOny6nl7pNogRHT+z+zp6ktw3fVaqEFHrVWcqR0OOQPPzzJdEQIpq3AVldXne3CVKj4ADNWqZABJAADAAAk9POd+2slSiU3OwP02LnGMYy2SR8ZuRAwBEzEBPj3S97uwNwBAOOeCQSM+WQPsn2iBiZiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgIiICIiAiIgf//Z"></a> 
                                       </div>
                                       <div class="casting-agency-info">
                                          <h5>Nazione + Studio- Photography</h5>
                                          <p>Mumbai - Maharastra</p>
                                          <p>Years: 10Years</p>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                    <div class="casting-agency-main">
                                       <div class="casting-agency-photo-block">
                                          <a href=""> 
                                          <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&usqp=CAU"></a> 
                                       </div>
                                       <div class="casting-agency-info">
                                          <h5>Nazione + Studio- Photography</h5>
                                          <p>Mumbai - Maharastra</p>
                                          <p>Years: 10Years</p>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                    <div class="casting-agency-main">
                                       <div class="casting-agency-photo-block">
                                          <a href=""> <img src="https://www.thebalancecareers.com/thmb/QlG3l8buSxMQnDM8DSlUlvEcL0g=/1280x645/filters:no_upscale():max_bytes(150000):strip_icc()/CAA-53b5401f22754d03a0811583b090b2bb.jpg"></a> 
                                       </div>
                                       <div class="casting-agency-info">
                                          <h5>Perfect Solution Casting Agency</h5>
                                          <p>Mumbai - Maharastra</p>
                                          <p>Years: 10Years</p>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                    <div class="casting-agency-main">
                                       <div class="casting-agency-photo-block">
                                          <a href=""> <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTn88aqMt8hdfHHOUpDI9IIddxOQDQ59iNTIoEXvZwMhxi-sNZjZv7pGizewVBJMPM-8Go&usqp=CAU"></a> 
                                       </div>
                                       <div class="casting-agency-info">
                                          <h5>Style Icon Kolkata</h5>
                                          <p>Mumbai - Maharastra</p>
                                          <p>Years: 10Years</p>
                                       </div>
                                       <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>
                                 </div>
                               
                              </div>
                           </div>
                           <!-- /Popular Channels -->
                        </div>
                     </div>
					 
					 
					 
					 
					 
					 
					 
					 <div style="position:relative">
                        <h2 class="middle-head">Auditions</h2>
                        <a href="auditions.html" style="position:absolute;right:-12px;top:0px; color:#fff; padding:4px 15px 0 15px; background:#f72e5e !important">View All</a> 
                   
					  <div class="row">   
					  <div  style="background:#fff;clear: both;overflow: hidden;margin:15px 0 0 0">
                           <div class="col-lg-12" style="padding:0px 5px">
                              <!-- Popular Channels -->
                              <div class="content-block" id="casting-home-page-audition-block">
                                

								<div class="production-all-display" id="right-side-bar">
								  <div class="production-hs1"> 
									<a href="" class="ic"> 
									<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
												<h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
										<div class="clear"></div>	
										</div> 
										<div class="production-hs2">
								
										<p class="prd-info"> Hi we are looking for a male lead for...</p>
									
										<div class="sp1" id="auditions">
												<p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p> <div class="clear"></div>
												<p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>  <div class="clear"></div> 
											 
												<p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
										</div>  
											 <a href="" class="view-m">View Now</a> 
											<div class="clear"></div>	
										  </div> 
								<div class="clear"></div>	 
							  </div>
							  
							  
							 <div class="production-all-display" id="right-side-bar">
								  <div class="production-hs1"> 
									<a href="" class="ic"> 
									<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
												<h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
										<div class="clear"></div>	
										</div> 
										<div class="production-hs2">
								
										<p class="prd-info"> Hi we are looking for a male lead for...</p>
									
										<div class="sp1" id="auditions">
												<p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p> <div class="clear"></div>
												<p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>  <div class="clear"></div> 
											 
												<p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
										</div>  
											 <a href="" class="view-m">View Now</a> 
											<div class="clear"></div>	
										  </div> 
								<div class="clear"></div>	 
							  </div>
							  
							  
							<div class="production-all-display" id="right-side-bar">
								  <div class="production-hs1"> 
									<a href="" class="ic"> 
									<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
												<h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
										<div class="clear"></div>	
										</div> 
										<div class="production-hs2">
								
										<p class="prd-info"> Hi we are looking for a male lead for...</p>
									
										<div class="sp1" id="auditions">
												<p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p> <div class="clear"></div>
												<p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>  <div class="clear"></div> 
											 
												<p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
										</div>  
											 <a href="" class="view-m">View Now</a> 
											<div class="clear"></div>	
										  </div> 
								<div class="clear"></div>	 
							  </div>
							  
							  
							<div class="production-all-display" id="right-side-bar">
								  <div class="production-hs1"> 
									<a href="" class="ic"> 
									<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
												<h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
										<div class="clear"></div>	
										</div> 
										<div class="production-hs2">
								
										<p class="prd-info"> Hi we are looking for a male lead for...</p>
									
										<div class="sp1" id="auditions">
												<p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p> <div class="clear"></div>
												<p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>  <div class="clear"></div> 
											 
												<p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
										</div>  
											 <a href="" class="view-m">View Now</a> 
											<div class="clear"></div>	
										  </div> 
								<div class="clear"></div>	 
							  </div>
							  
							  
							  
							  
							  <div class="production-all-display" id="right-side-bar">
								  <div class="production-hs1"> 
									<a href="" class="ic"> 
									<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
												<h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
										<div class="clear"></div>	
										</div> 
										<div class="production-hs2">
								
										<p class="prd-info"> Hi we are looking for a male lead for...</p>
									
										<div class="sp1" id="auditions">
												<p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p> <div class="clear"></div>
												<p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>  <div class="clear"></div> 
											 
												<p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
										</div>  
											 <a href="" class="view-m">View Now</a> 
											<div class="clear"></div>	
										  </div> 
								<div class="clear"></div>	 
							  </div>
							  
							  
							  
							  <div class="production-all-display" id="right-side-bar">
								  <div class="production-hs1"> 
									<a href="" class="ic"> 
									<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
												<h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
										<div class="clear"></div>	
										</div> 
										<div class="production-hs2">
								
										<p class="prd-info"> Hi we are looking for a male lead for...</p>
									
										<div class="sp1" id="auditions">
												<p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p> <div class="clear"></div>
												<p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>  <div class="clear"></div> 
											 
												<p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
										</div>  
											 <a href="" class="view-m">View Now</a> 
											<div class="clear"></div>	
										  </div> 
								<div class="clear"></div>	 
							  </div>
							  
							  
							  
							  <div class="production-all-display" id="right-side-bar">
								  <div class="production-hs1"> 
									<a href="" class="ic"> 
									<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
												<h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
										<div class="clear"></div>	
										</div> 
										<div class="production-hs2">
								
										<p class="prd-info"> Hi we are looking for a male lead for...</p>
									
										<div class="sp1" id="auditions">
												<p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p> <div class="clear"></div>
												<p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>  <div class="clear"></div> 
											 
												<p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
										</div>  
											 <a href="" class="view-m">View Now</a> 
											<div class="clear"></div>	
										  </div> 
								<div class="clear"></div>	 
							  </div>
							  
							  
							  
							  <div class="production-all-display" id="right-side-bar">
								  <div class="production-hs1"> 
									<a href="" class="ic"> 
									<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a> 
												<h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
										<div class="clear"></div>	
										</div> 
										<div class="production-hs2">
								
										<p class="prd-info"> Hi we are looking for a male lead for...</p>
									
										<div class="sp1" id="auditions">
												<p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager</p> <div class="clear"></div>
												<p><span><i class="fa fa-map-marker"></i> Location: </span>  Bangalore</p>  <div class="clear"></div> 
											 
												<p><span><i class="fa fa-calendar-check-o"></i>  Valid Till: </span> Mar 29 2022</p>
										</div>  
											 <a href="" class="view-m">View Now</a> 
											<div class="clear"></div>	
										  </div> 
								<div class="clear"></div>	 
							  </div>
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
							  
                            
                              </div>
                           </div>
                           <!-- /Popular Channels -->
                        </div>
						   </div>
						
                     </div>
					 
					 
					 
					 
					 
					 
					 
					 
					 
                  </div>
               </div>
               <!-- /Featured Videos -->
            </div>
         </div>
      </div>
      </div>
      
      
      
        <!-- Owl Carousel -->
      <style>
         #owl-demo .item{
         -webkit-border-radius: 3px;
         -moz-border-radius: 3px;
         border-radius: 3px;
         text-align: center;
         }
         .customNavigation{
         text-align: center;
         }
         .customNavigation a{
         -webkit-user-select: none;
         -khtml-user-select: none;
         -moz-user-select: none;
         -ms-user-select: none;
         user-select: none;
         -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
         }
      </style>
      <script>
         $(document).ready(function() {
         
           var owl = $("#owl-demo");
         
           owl.owlCarousel({
         
           items : 15, //10 items above 1000px browser width
           itemsDesktop : [1000,5], //5 items between 1000px and 901px
           itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
           itemsTablet: [600,2], //2 items between 600 and 0;
           itemsMobile :false // itemsMobile disabled - inherit from itemsTablet option
           
           });
         
           // Custom Navigation Events
           $(".next").click(function(){
             owl.trigger('owl.next');
           })
           $(".prev").click(function(){
             owl.trigger('owl.prev');
           })
           $(".play").click(function(){
             owl.trigger('owl.play',1000);
           })
           $(".stop").click(function(){
             owl.trigger('owl.stop');
           })
         
         
         });
      </script>

@endsection
