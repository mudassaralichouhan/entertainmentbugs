@extends('layouts.home')

@section('style')
    <link href="https://entertainmentbugs.com/dev_html/css/chat.css" rel="stylesheet">
    <style>
        .content-block .cb-content {
            margin-bottom: 1px;
        }

        .layout .content .chat .chat-footer {
            padding-top: 0;
        }

        body:not(.form-membership) {
            overflow: auto;
        }

        .content {
            background: #eaeaea !important;
        }

        .chat {
            background: #fff;
        }

        .container .content {
            padding-left: 0px;
            padding-right: 0;
        }

        .navigation {
            display: none
        }

        @media only screen and (max-width: 767px) {
            nav.navigation {
                background: #fff;
                width: 40px;
                position: absolute;
                left: 0px;
                height: 40px;
            }

            .layout .content .chat .chat-header .chat-header-action {
                margin-top: 15px;
                width: 65px;
                float: right;
                margin: -38px 0 0 13px;
            }

            .layout .content .sidebar-group .sidebar .list-group-item .users-list-body h5 {
                font-size: 14px !important;
            }

            .layout .content {
                margin-top: 40px;
            }
        }

        .layout .content .chat .chat-body.no-message {
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            height: 473px;
            overflow: scroll;
            background: #eaeaea;
            margin: 0;
        }


        .messages {
            padding: 20px;
        }
    </style>
    <style>
        #sponsorship h3 {
            line-height: 9px;
            font-weight: normal;
            color: #000;
            font-size: 13px;
            position: relative;
            margin: 9px 0 16px 0;
            text-align: Center;
        }

        #sponsorship img {
            width: 100%;
        }

        .sponsorship {
            margin: 10px 0 20px 0;
            clear: both;
            overflow: hidden
        }

        .sponsorship a {
            float: left;
            margin-left: 0.3%;
            width: 9.7%;
        }

        .product-main {
            clear: both;
            display: block;
            padding: 0 5px;
        }

        .product-main-block {
            text-align: center;
            vertical-align: middle;
            display: inline;
            width: 100%;
            padding: 0;
            margin: 0px 0.5% 7px 0%;
        }

        .content-b h2 {
            color: #fff;
            text-align: left;
            font-size: 16px;
            margin: 022px 0 5px 0;
        }

        .shop-btn {
            z-index: 99;
            position: absolute;
            right: 15px;
            bottom: 13px;
            background: #fff;
            color: #000;
            line-height: 17px;
            padding: 9px 12px 6px 12px;
            font-size: 12px;
            text-align: center;
            display: block;
        }

        .product-main-block {
            position: relative;
        }

        .product-main-block:before {
            background: #000;
            opacity: 0.2;
            left: 0px;
            bottom: 0px;
            height: 57px;
            width: 100%;
            content: '';
            z-index: 9;
            display: block;
            position: absolute;
        }

        .content-b {
            width: 92%;
            position: absolute;
            z-index: 9;
            top: 245px;
            color: #fff;
            left: 10px;
        }

        .product-main-block .product_img img {
            transition: transform .2s;
        }

        .product-main-block:hover .product_img img {
            -ms-transform: scale(1.1); /* IE 9 */
            -webkit-transform: scale(1.1); /* Safari 3-8 */
            transform: scale(1.1);
        }

        .product-main-block .sponsers_user_photo {
            width: 35px;
            height: 35px;
            z-index: 9;
            object-fit: cover;
            display: block;
            position: absolute;
            border-bottom: 100px;
            border-radius: 100px;
            overflow: hidden;
            right: 10px;
            top: 10px;
        }
    </style>
    <style>
        #result-block-main .photo-block a.profile-p {
            width: 100%;
            height: auto;
            overflow: hidden;
            display: block;
        }

        #result-block-main .photo-block a.profile-p img {
            object-fit: cover;
            width: 100%;
            height: 100%
        }

        #result-block-main .photo-block p {
            margin: 0px;
            padding: 0px;
            font-size: 12px;
            line-height: 16px;
        }

        #result-block-main .photo-block h5 {
            margin: 0px 0 2px 0;
            font-size: 16px;
            font-weight: bold;
        }

        .info-p {
            background: #eaeaea;
            padding: 10px;
        }

        #result-block-main .photo-block {
            height: auto;
        }

        #result-block-main .photo-block a.profile-p {
            height: 151px;
        }

        .cat {
            dont-size: 12px;
        }

        #result-block-main {
            padding-bottom: 20px;
        }
    </style>
    <style>
        .layout .content .chat .chat-footer {
            padding-top: 0;
        }

        body:not(.form-membership) {
            overflow: auto;
        }

        .content {
            background: #eaeaea !important;
        }

        .chat {
            background: #fff;
        }

        .container .content {
            padding-left: 0px;
            padding-right: 0;
        }

        .navigation {
            display: none
        }

        @media only screen and (max-width: 767px) {
            nav.navigation {
                background: #fff;
                width: 40px;
                position: absolute;
                left: 0px;
                height: 40px;
            }

            .layout .content .chat .chat-header .chat-header-action {
                margin-top: 15px;
                width: 65px;
                float: right;
                margin: -38px 0 0 13px;
            }

            .layout .content .sidebar-group .sidebar .list-group-item .users-list-body h5 {
                font-size: 14px !important;
            }

            .layout .content {
                margin-top: 40px;
            }
        }

        .layout .content .chat .chat-body.no-message {
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            height: 473px;
            overflow: scroll;
            background: #eaeaea;
            margin: 0;
        }

        footer {
            display: none;
        }

        .messages {
            padding: 20px;
        }
    </style>
    <style>
        .cb-content.videolist:nth-child(odd) {
            background: #eaeaea;
        }

        .cb-content.videolist {
            padding: 10px 0;
            clear: both;
            overflow: hidden
        }
    </style>
    <style>
        .flw213 {
            background: #28b47e !important;
            color: #fff;
            display: block;
            text-align: Center;
            height: 32px;
            line-height: 34px;
            width: 75%;
            margin: 0 auto;
        }

        .new_right {
            width: 85%;
        }

        .v-desc a {
            font-size: 12px;
            line-height: 15px;
            display: block;
            margin-bottom: -8px;
            margin-top: 7px;
            padding-right: 20px;
        }

        #home_page_video_new .nwo {
            background: none !Important;
        }

        .flw213:hover {
            background: #f72e5e !important;
            color: #fff;
            display: block;
            text-align: Center;
            height: 32px;
            line-height: 34px;
            width: 75%;
            margin: 0 auto;
        }

        .cb-content .rewards-0ico {
            width: 61px;
            position: absolute;
            left: 44.5%;
            top: 4px;
            height: 43px;
            background: url(https://www.entertainmentbugs.com/public/img/rewards.png) no-repeat;
            background-size: contain;
        }

        .cb-content:nth-child(1) .rewards-0ico {
            width: 61px;
            position: absolute;
            left: 44.5%;
            top: 9px;
            height: 43px;
            background: url(https://www.entertainmentbugs.com/public/img/rewards.png) no-repeat;
            background-size: contain;
        }


        .videolist .videoitem .h-video .col-lg-1 img {
            height: 50px;
            object-fit: cover;
        }
    </style>
@endsection

@section('content')
    <div id="sponsorship"
         style="display:none;background:#f4f3f3;padding: 20px 30px 60px 30px;clear:both;overflow:hidden;position: relative;">
        <div class="product-main" style="width: 1180px;margin: 0 auto;clear: both;overflow: hidden;">
            <h3 style="color:#f72e5e;text-align: left;padding-left: 5px;">Sponsor Products</h3>

            <div class="sponsorship">
                <a href=""><img src="https://www.crazypundit.com/wp-content/uploads/2014/01/Van-Heusen-logo.gif"></a>
                <a href=""><img src="https://www.crazypundit.com/wp-content/uploads/2014/01/Van-Heusen-logo.gif"></a>
                <a href=""><img src="https://www.crazypundit.com/wp-content/uploads/2014/01/Van-Heusen-logo.gif"></a>
                <a href=""><img src="https://www.crazypundit.com/wp-content/uploads/2014/01/Van-Heusen-logo.gif"></a>
                <a href=""><img src="https://www.crazypundit.com/wp-content/uploads/2014/01/Van-Heusen-logo.gif"></a>
                <a href=""><img src="https://www.crazypundit.com/wp-content/uploads/2014/01/Van-Heusen-logo.gif"></a>
                <a href=""><img src="https://www.crazypundit.com/wp-content/uploads/2014/01/Van-Heusen-logo.gif"></a>
                <a href=""><img src="https://www.crazypundit.com/wp-content/uploads/2014/01/Van-Heusen-logo.gif"></a>
                <a href=""><img src="https://www.crazypundit.com/wp-content/uploads/2014/01/Van-Heusen-logo.gif"></a>
                <a href=""><img src="https://www.crazypundit.com/wp-content/uploads/2014/01/Van-Heusen-logo.gif"></a>
            </div>

            <div id="all-sponsers-display">
                <div class="product-main-block" style="width: 25%;float: left;height: 300px;overflow: hidden;">
                    <a href="" class="sponsers_user_photo">
                        <img src="https://entertainmentbugs.com/balu_development/public/uploads/users/small/1678225387.png">
                    </a>
                    <a href="" class="shop-btn">View Now</a><a href="" class="product_img">
                        <img src="https://wordpressthemes.live/WCM07/WCM070157/wp-content/uploads/2021/12/Main-banner-01.jpg">
                    </a>
                    <div class="content-b">
                        <h2>Women Tops</h2>
                    </div>
                </div>
                <div class="product-main-block"
                     style="width: 24%;float: left;height: 300px;overflow: hidden;margin: 0 !important;padding: 0;">
                    <a href="" class="sponsers_user_photo">
                        <img src="https://entertainmentbugs.com/balu_development/public/uploads/users/small/1675944889.png">
                    </a>
                    <a href="" class="shop-btn">Shop Now</a><a href="" class="product_img">
                        <img src="https://wordpressthemes.live/WCM07/WCM070157/wp-content/uploads/2021/12/Main-banner-05.jpg">
                    </a>
                    <div class="content-b">
                        <h2>Kids Collections</h2>
                    </div>
                </div>
                <div class="product-main-block"
                     style="width: 50%;float: right;height: 300px;overflow: hidden;margin: 0  0 7px 0;">
                    <a href=""
                       class="sponsers_user_photo">
                        <img src="https://entertainmentbugs.com/balu_development/public/uploads/users/small/1678225387.png">
                    </a>
                    <a href="" class="shop-btn">Visit Now</a>
                    <a href="" class="product_img">
                        <img src="https://wordpressthemes.live/WCM07/WCM070157/wp-content/uploads/2021/12/Main-banner-04.jpg">
                    </a>
                    <div class="content-b">
                        <h2>Women Tshirt</h2>
                    </div>
                </div>
                <div style="clear:both; overflow:hidden"></div>
            </div>
        </div>
    </div>

    <section class="portfolio section" style="display:none">
        <div class="container">
            <div class="col-12 filters-content" id="result-block-main">
                <div class="row" style="padding: 15px 20px;">
                    <div class="col-lg-12">
                        <ul class="list-inline" style="margin-top: 15px;">
                            <li><a href="#">Similar Category Profile</a></li>
                        </ul>
                    </div>
                </div>
                <div class=" grid">
                    <div class="col-sm-3 photo-block">
                        <a href="" class="profile-p">
                            <img src="https://entertainmentbugs.com/dev2022/public/uploads/temp/1668668293.png"
                                 height="150" alt="Shiva gupta">
                        </a>
                        <div class="info-p p-inner">
                            <h5>Shiva gupta</h5>
                            <div class="cat">Kolkata West Bengal India<br>
                                Age: 26<br>
                                english,Hindi
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 photo-block">
                        <a href="" class="profile-p">
                            <img src="https://entertainmentbugs.com/dev2022/public/uploads/temp/D895F6A9E67BA340D828E5DBC20A23390FB079B1.png"
                                 height="150" alt="Jacob Hawkins">
                        </a>
                        <div class="info-p p-inner">
                            <h5>Jacob Hawkins</h5>
                            <div class="cat">Kolkata West Bengal India<br>
                                Age: 32<br>
                                english
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 photo-block">
                        <a href="" class="profile-p">
                            <img src="https://entertainmentbugs.com/dev2022/public/uploads/temp/6AAE5B99BA1ACE95927B102E1CBA99D168EFFFC1.png"
                                 height="150" alt="Rajat gupta">
                        </a>
                        <div class="info-p p-inner">
                            <h5>Rajat gupta</h5>
                            <div class="cat">Jhansi Uttar Pradesh India<br>
                                Age: 28<br>
                                hindi, bihari
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 photo-block">
                        <a href="" class="profile-p">
                            <img src="https://entertainmentbugs.com/dev2022/public/uploads/temp/4465CE2784B1A791E616BF97E9190A7C295483CE.png"
                                 height="150" alt="harshal">
                        </a>
                        <div class="info-p p-inner">
                            <h5>harshal</h5>
                            <div class="cat">Dondar Qu?çu Tovuz District Azerbaijan<br>
                                Age: 8<br>
                                aljari
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 photo-block">
                        <a href="" class="profile-p">
                            <img src="https://entertainmentbugs.com/dev2022/public/uploads/temp/498B43602FF1955C05652F537F54A8895FB157B8.png"
                                 height="150" alt="dass">
                        </a>
                        <div class="info-p p-inner">
                            <h5>dass</h5>
                            <div class="cat">Río Grande Tierra del Fuego Argentina<br>
                                Age: 31<br>
                                aljari
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 photo-block">
                        <a href="" class="profile-p">
                            <img src="https://entertainmentbugs.com/dev2022/public/uploads/temp/1667653203.png"
                                 height="150" alt="Richa sharma">
                        </a>
                        <div class="info-p p-inner">
                            <h5>Richa sharma</h5>
                            <div class="cat">East Jaintia Hills Meghalaya India<br>
                                Age: 25<br>
                                Hindi,English
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3 photo-block">
                        <a href="" class="profile-p">
                            <img src="https://entertainmentbugs.com/balu_development/public/uploads/temp/1674882135.png"
                                 height="150" alt="Vani Sharma">
                        </a>
                        <div class="info-p p-inner">
                            <h5>Vani Sharma</h5>
                            <div class="cat">Achalpur Maharashtra India<br>
                                Age: 28<br>
                                Hindi,English
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </section>

    <div class="content-wrapper" style="background:#f4f3f3 !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-6 col-xs-4">
                                    <h5>Top 100 Entertainer List </h5>
                                </div>
                                <div class="col-lg-6 col-xs-8">
                                    <ul class="list-inline" style="float: right">
                                        <li>
                                            <a href="{{ url('/all-top-entertainer') }}"
                                               class="promote_your_video1 blinko"
                                               style="background-color: {{ $selected != 'second' ? 'blue' : '' }}">
                                                Current Week
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/all-top-entertainer/second') }}"
                                               class="promote_your_video1 blinko"
                                               style="background-color: {{ $selected == 'second' ? 'blue' : '' }}">
                                                Previous Week
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/participate-video') }}"
                                               class="promote_your_video1 blinko">
                                                Participate Now
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <table class="table" style="margin-top: 20px">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">User</th>
                                <th scope="col">Video</th>
                                <th scope="col">Title</th>
                                <th scope="col">Views</th>
                                <th scope="col">Rank</th>
                                <th scope="col">Watch Time</th>
                                <th scope="col">Upload Date</th>
                                <th scope="col">Liked</th>
                                <th scope="col">Follow</th>
                                <th scope="col">Rate</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php $id = session()->get('user_id') ?>
                            @foreach ($videos ?? [] as $video)

                                    <?php
                                    $videoUrl = $video->video_key;
                                    ?>

                                @if (!empty($video->thumb->thumb))
                                        <?php
                                        $thumbBastPath = VIDEO_THUMB_UPLOAD_PATH . $video->thumb->thumb;
                                        $thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH . $video->thumb->thumb;
                                        $isFollowing = DB::table('followers')
                                            ->where('follows_id', $video->user->id)
                                            ->where('user_id', $id)
                                            ->first();
                                        ?>
                                    <tr>
                                        <th>
                                            <a href="{{ url($video->user->slug ?? '/') }}">
                                                @if (!empty($video->user->photo))
                                                    <img
                                                            style="width:35px;border-radius:100px; margin:0 auto;"
                                                            src="{{ asset('public/uploads/users/small/' . $video->user->photo) }}"
                                                    />
                                                @else
                                                    <span class="shortname">
                                                        {{ name_to_pic($video->user->name) }}
                                                    </span>
                                                @endif
                                                <span>
                                                    {{ $video->user->name }}
                                                </span>
                                            </a>
                                        </th>
                                        <td>
                                            @if (file_exists($thumbBastPath))
                                                <a href="{{ URL::to('watch-reward/' . $videoUrl) }}">
                                                    {{ HTML::image($thumbUrlPath, '', ['width' => 100]) }}
                                                </a>
                                            @else
                                                <a href="{{ URL::to('watch-reward/' . $videoUrl) }}"> </a>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="new_right">
                                                <div class="v-desc">
                                                    <a href="{{ URL::to('watch-reward/' . $videoUrl) }}">{{ $video->title }}</a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="v-views">
                                                {{ $video->total_views_count }}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="v-rank">
                                                {{ $video->rank ?? 'No Rank' }} -th
                                            </div>
                                        </td>
                                        <td>
                                            <div class="v-views">
                                                    <?php
                                                    $totalWatchTimeInSeconds = $video->total_track_sum;
                                                    $hours = floor($totalWatchTimeInSeconds / 3600); // 3600 seconds in an hour
                                                    $minutes = floor(($totalWatchTimeInSeconds % 3600) / 60);
                                                    $seconds = $totalWatchTimeInSeconds % 60;
                                                    ?>

                                                @if ($hours > 0)
                                                    {{ sprintf("%d hours %d min %02d sec", $hours, $minutes, $seconds) }}
                                                @else
                                                    {{ sprintf("%d min %02d sec", $minutes, $seconds) }}
                                                @endif

                                            </div>
                                        </td>
                                        <td>
                                            <div class="v-views">
                                                {{ $video->created_at->diffForHumans() }}
                                            </div>
                                        </td>
                                        <td>
                                            <div class="acide-panel search_page_like">
                                                <a href="#">
                                                    <i class="fa fa-thumbs-up"
                                                       data-toggle="tooltip" data-placement="top"
                                                       style="margin-right: 5px;"
                                                       data-original-title="Liked"></i>
                                                    {{ count($video->video_likes) }}</a>
                                            </div>
                                        </td>
                                        <td>
                                            @if (isset($isFollowing))
                                                <div id="suggested_unfollow_user{{ $video->user->id }}">
                                                    <a onclick="suggestedUnFollowUser('{{ $video->user->id }}')"
                                                       class="flw nwo">
                                                        <i class="fa fa-solid fa-heart"></i>
                                                        Following
                                                    </a>
                                                </div>
                                            @else
                                                <div id="suggested_follow_user{{ $video->user->id }}">
                                                    <a onclick="suggestedFollowUser('{{ $video->user->id }}')"
                                                       class="flw">
                                                        <i class="fa fa-heart-o"></i>
                                                        Follow
                                                    </a>
                                                </div>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $video->total_current_earning }}
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        function suggestedFollowUser(user_id) {
            $.ajax({
                type: "GET",
                url: "<?= e(URL::route('follow_user')); ?>",
                data: {
                    'user_id': user_id
                },
                cache: false,
                success: function (response) {
                    $('#suggested_follow_user' + user_id).empty().html(response);
                    $('#suggested_follow_user' + user_id).attr("id", "suggested_unfollow_user" + user_id);

                }
            });
        }

        function suggestedUnFollowUser(user_id) {
            $.ajax({
                type: "GET",
                url: "<?= e(URL::route('unfollow_user')); ?>",
                data: {
                    'user_id': user_id
                },
                cache: false,
                success: function (response) {
                    $('#suggested_unfollow_user' + user_id).empty().html(response);
                    $('#suggested_unfollow_user' + user_id).attr("id", "suggested_follow_user" + user_id);
                }
            });
        }
    </script>
@endsection