@extends('layouts.home')
 @section('meta_tags')
  
    <meta name="description" content="Watch the best video content created by the people from around the globe.">
    <meta name="keywords" content="Entertainment Video, All Categories, Cars & Vehicles, Comedy, Education, Entertainment, Film & Animation, Gaming, Music, News & Politics, Non-profits & Activism, People & Blogs, Pets & Animals, Science & Technology, Serials,Sports, Travel & Events">
    <meta name="author" content="<?php echo ((isset($author) ? $author : 'Video - Entertainment Bugs')); ?>">
    <meta property="og:site_name" content="{{URL::to('/')}}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="<?php echo ((isset($ogtitle) ? $ogtitle : 'Entertainment Bugs')); ?>"/>
    <meta property="og:description" content="<?php echo ((isset($ogdescription) ? $ogdescription : 'Watch the best video content created by the people from around the globe.')); ?>"/>
    <meta property="og:url" content="<?php echo url()->current(); ?>"/>
    <meta property="og:image" content="<?php echo ((isset($ogimage) ? $ogimage : 'https://www.entertainmentbugs.com/public/uploads/logo/logo-social-media.png')); ?>"/><!--profile image-->
 
  <meta name="keywords" content="<?php echo ((isset($keywords) ? $keywords : 'Entertainment Video, All Categories, Cars & Vehicles, Comedy, Education, Entertainment, Film & Animation, Gaming, Music, News & Politics, Non-profits & Activism, People & Blogs, Pets & Animals, Science & Technology, Serials,Sports, Travel & Events')); ?>">
   @endsection
@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="row">
                                  
<style>
.b-video .video-container{  opacity:0;  position:absolute;z-index:99;top:0px;}
.b-video:hover .image-container{  display:none; position:relative;z-index:1;opacity:0; }
.b-video:hover .video-container{    opacity:1; position:relative;z-index:9;}
   
 
 

/*#home_page_video .video-container{position:absolute;z-index:-1;}*/
/*#home_page_video .b-video:hover .video-container {position:relative;z-index:9;}*/

/*#home_page_video .b-video:hover .image-container {position:absolute;z-index:-1;}*/



.v-img a::before{display:none !Important;}
.v-img a:hover::after{display:none !Important;}


 
.v-categories #full-width-mobile .time {
    display: block !Important;font-size: 12px;
}
    
.trending-rewards-tags {margin-top:10px;}
.trending-rewards-tags ul{list-style:none;text-align: left;margin: 0 0 0px 21px;padding: 0 12px 0 0;}
.trending-rewards-tags li{display:inline-block;text-align:left;}
.trending-rewards-tags li a{   background: #e5e5e5;border: 0px;color: #000;line-height: 27px;height: 25px;padding: 2px 10px 0 10px;margin-bottom: 1px;font-size: 12px;}
#reward_tags1 .owl-item {margin-right:5px;width: auto !important;}
.tag_head{margin-right: 5px;float: left;width: 11%; line-height: 30px; color:#fff;    height: 20px;}
.btn.btn-default {padding: 12px 12px 0 12px;} 
#reward_tags1 .owl-pagination {display:block !Important;}
#reward_tags1 .owl-page span{display:none; }
#reward_tags1 .owl-page.active{opacity:0.6;}
#reward_tags1 .owl-page{ }
#reward_tags1 .owl-page:nth-child(1) {position: absolute;left: -16px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow-gray.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}  
#reward_tags1 .owl-page:nth-child(2) {position: absolute;right: -4px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow-gray.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}
#reward_tags1 .owl-page:nth-child(1):hover {position: absolute;left: -16px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}  
#reward_tags1 .owl-page:nth-child(2):hover {position: absolute;right: -4px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}


</style>    


<style>
            

/*.v-categories.side-menu .content-block .cb-content .sidebar-menu ul li:nth-child(10){display:none;}        */
/*.v-categories.side-menu .content-block .cb-content .sidebar-menu ul li:nth-child(2){display:none;}   */
/*.v-categories.side-menu .content-block .cb-content .sidebar-menu ul li:nth-child(13){display:none;}   */
            


 
.videoitem {transition: transform .2s;}
#full-width-mobile .col-lg-2 {width: 16.46%;border: 1px solid #f4f3f3;}
#full-width-mobile .col-lg-2:hover {width: 16.46%;border: 1px solid #f4f3f3;}
#full-width-mobile .col-lg-2:hover  .user {top: 128px;}
#full-width-mobile .col-lg-2:hover .v-img {overflow: hidden;margin-top: 5px;height: 87px;}
.videoitem:hover {
    background:#000;padding:10px;
  -ms-transform: scale(1.3); /* IE 9 */
  -webkit-transform: scale(1.3); /* Safari 3-8 */
  transform: scale(1.3);  position:relative; z-index:999;background:#fff;
}
 
            
            
            
    
          
.ads_block_contatiner .v-percent{display:none !Important;}


.v-categories .content-block .cb-header{padding-top:0px;margin-top:-15px;}  
      
body.light .content-wrapper {background-color: #f4f3f3;}
.v-categories.side-menu .content-block .cb-content > .row > div:first-child {padding-left: 0;}
.content-block.head-div.head-arrow .head-arrow-icon{background-color: #f4f3f3;}
.v-categories #full-width-mobile .user {top: 133px;}
.v-categories.side-menu .content-block .cb-content .sidebar-menu ul li{float:left;margin:0 12px -1px 0 !Important;}
#no-dis-on-mob{width:100%;height:auto !important;padding-bottom:16px !Important;}
#full-width-mobile{width:100%;border: 0px !important;padding: 0px !important;}
#full-width-mobile .col-lg-2 {width: 16.66%;}
.btn:hover, .btn:focus, .btn.focus {border:0px solid #fff;outline: none;}
#home_page_video .v-desc a {font-size: 12px;display: block;line-height: 14px;}
.dropdown-menu li a{    cursor: pointer;}
.v-categories #full-width-mobile .b-video .v-views {margin-top: 3px;}
.list-inline li.active a{color:#f72e5e !important;}
.col-lg-12 v-categories.side-menu{padding:0px;}
.v-categories .content-block .cb-content {clear: none;overflow: revert;}
 

.v-categories.side-menu .content-block .cb-content > .row > div:first-child{border-right: solid 0px #eceff0;padding-right: 0px;padding-top:14px;}
.v-categories.side-menu .content-block .cb-content .sidebar-menu ul li a {font-size: 13px;}
.v-categories .b-video .v-img {overflow: hidden;}
.v-categories .b-video .v-img img{ transition: transform .2s;}
.b-video:hover .v-img a img{ -ms-transform: scale(1.1); /* IE 9 */ -webkit-transform: scale(1.1); /* Safari 3-8 */transform: scale(1.1); }
.b-video:hover .v-desc a{color: #f72e5e !important;}


/*
.cb-content .top .col-lg-2:nth-child(1){width: 42.8% !important; height: 325px !important;}
.cb-content .top .col-lg-2:nth-child(1) .v-img img{height:100% !important}
.cb-content .top .col-lg-2:nth-child(1) .b-video .v-img {height: 261px !important;}
.cb-content .top .col-lg-2:nth-child(1) .v-desc{    height: auto;}
.cb-content .top .col-lg-2:nth-child(1) .user{top: 96% !important;}
.cb-content .top .col-lg-2:nth-child(1) .v-desc a {font-size: 16px !important;display: block;line-height: 22px !important; }
*/
 

.cb-content .v-views {font-size: 12px;}

@media  only screen and (max-width: 767px) {
.trending-rewards-tags{display:none;}
.cb-content .top .col-lg-2:nth-child(1) .b-video .v-img {height: 179px !important;} 
.cb-content .top .col-lg-2:nth-child(1){width: 100% !important; height: 250px !important;}
.content-wrapper .cb-content .top .col-lg-2:nth-child(1) .user {position: absolute;right: -5px;top: 195px !important;width: 40px;z-index: 999;}
.btn.btn-default {
    padding: 21px 1px 5px 12px  !important;
}

#home_page_video .v-desc a {
    height: 38px;
}


.v-categories #full-width-mobile .user a.user_heading-title-bg {line-height: 24px;}
.col-lg-12 v-categories.side-menu{padding:0 10px;}
.v-categories .b-video .v-img {height: 90px;}
#home_page_video .v-img img {height: 100%;}
#home_page_video .v-desc a {font-size: 13px;line-height: 16px;}
.v-categories .b-video .v-desc {height: 39px;overflow: hidden;}
.b-video .v-views .v-percent {padding-left: 19px;}
.v-categories #full-width-mobile .user {position: absolute;right: -5px;top: 123px;width: 38px;z-index: 99;}
.videolist .videoitem .b-video .v-desc {margin-bottom: 7px;}
#categories-shown-in-all{display:block !Important;border-top: solid 1px #eceff0;padding: 11px 0 10px 0;border-bottom: solid 1px #eceff0}
#categories-shown-in-all .owl-theme .owl-controls{    display: none !important;}
#no-dis-on-mob{display:none;}
#full-width-mobile{width:100% !important;padding: 0 !important;border: 0px !important;}
#owl-demo1 .owl-item { width: auto !important;padding: 0 3px;}
#owl-demo1 a.nw2{background: #28b47e !important;padding:7px 10px 3px 10px;display: block;font-size: 12px !important;color: #fff;border-radius: 0px;line-height: 16px;}
#owl-demo1  a.color-active.nw2{background:#f72e5e !important; color:#fff !important;}
.sticky {position: fixed;top: 50px;background:#fff;z-index:99;backgroundwidth: 100%;}
#crousal_wrap{width:80% !important;}
}
  
           
</style>
         
	
              <div class="span12" id="categories-shown-in-all" style="display:none"> 
							<div id="owl-demo1" class="owl-carousel">
							      
							     
                                      @php
                                        $url_segment = \Request::segment(2);
                                     @endphp
                                  
								          @if(trim($url_segment) == '')
                                             <a href="{{URL::to('category/')}}" class="color-active nw2">All Categories</a>
                                        @else
                                           <a class="nw2" href="{{URL::to('category/')}}">All Categories</a> 
                                        @endif
                                     
                                       
                                        @foreach($categories as $category)
                                        
                                        
                                            <div class="item" >
                                                @if(trim($category->category) == trim($url_segment))
                                                <a  href="{{ URL::to('category/')}}/{{$category->category}}" class="color-active nw2"> {{ $category->category }}</a> 
                                            @else
                                                <a class="nw2" href="{{ URL::to('category/')}}/{{$category->category}}"> {{$category->category }}</a> 
                                            @endif
                                            
                                            </div>
                                            
                                        @endforeach
                                        
                                        
                                        
                                         </div>    
							</div>	 	 
													 						
					</div> 
			
            
               <div class="rows">
            
            <div class="col-lg-12 v-categories side-menu">

                <!-- Popular Channels -->
                <div class="content-block">
                    
                    <div class="cb-content">
                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-4" id="no-dis-on-mob">
                                <aside class="sidebar-menu">
                                     @php
                                        $url_segment = \Request::segment(2);
                                     @endphp
                                     
									<ul>
                                     
                                       
                                           @if(trim($url_segment) == '')
                                            <li><a href="{{URL::to('category/')}}" class="color-active">View all categories</a></li>
                                        @else
                                            <li><a href="{{URL::to('category/')}}">View all categories</a></li>
                                        @endif
										
										
                                        @foreach($categories as $category)
										@if($category!='')
                                            @if(trim($category->category) == trim($url_segment))
                                                <li><a href="{{ URL::to('category/')}}/{{$category->category}}" class="color-active"> {{ $category->category }}</a>,</li>
                                            @else
                                                <li><a href="{{ URL::to('category/')}}/{{$category->category}}"> {{ $category->category }}</a>,</li>
                                            @endif 
											 @endif 
											 
                                        @endforeach
                                        
                                        
                                        
                                    </ul>
                                    <div class="bg-add"></div>
                                </aside>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-8" id="full-width-mobile" style="padding-left:10px; padding-top:0px">
                                <div class="row">
            <div class="col-lg-12"> 
                <!-- Featured Videos -->
                <div class="content-block">
                    <div class="cb-header">
                        <div class="row">
                            <div class="col-lg-11 col-sm-10 col-xs-8" style="position:relative;z-index:99">
               
                               
      <div class="trending-rewards-tags">
   <div class="span12" id="crousal_wrap" style="width:100%">
        <ul id="reward_tags1" class="owl-carousel" style="display:block">
                @if(isset($tags) && count($tags)>0)
                 @foreach(array_slice($tags,0,30) as $tag)   
   
        <form action="{{URL::to('search')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" class="form-control" name="searchdata" aria-describedby="sizing-addon2" value="{{ $tag }}">
            <input type="hidden" name="searchType" id="searchType" value="video"/>
        
         <li> <input type="submit" value="{{ $tag }}"/> </li>
        </form>
    @endforeach
@endif 
        
      </ul>
    </div>
</div>                             
                               
                               
                            </div>
                            <div class="col-lg-1 col-sm-2 col-xs-4">
                                <div class="btn-group pull-right bg-clean">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Sort by <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                            <li><a onclick="categoryVideoSortByRecent()"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                            <li><a onclick="categoryVideoSortByTopViewed()"><i class="cv cvicon-cv-view-stats"></i> Top Viewed</a></li>
                                            <li><a onclick="categoryVideoSortByTopLiked()"><i class="fa fa-thumbs-up"></i> Top liked</a></li>
                                            <li><a onclick="categoryVideoSortByTopTrending()"><i class="fa fa-heart-o"></i> Top Trending</a></li>
                                            <li><a onclick="categoryVideoSortByTopLongest()"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="cb-content videolist" id="home_page_video">
                        @if(!empty($videoData))
                            <div class="top" id="category_video_data">
                                 @include('elements.post.categoryvideo')	
                            </div>
                        @endif
                    </div>
                <!-- /Featured Videos -->
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /Popular Channels -->
</div>
</div>
</div>
</div>
</div>
<div class="ajax-load text-center" style="display:none">
	<p><img src="https://www.entertainmentbugs.com/public/img/animation_loading.gif" style="width:250px"> </p>
</div>
<script type="text/javascript">
	var loading = false;
    var page = 1;
 $(window).scroll(function() {
	   if($(window).scrollTop() >= $(document).height() - $(window).height() - 10 && loading == false) {
         page++;
      loadMoreData(page);
         loading = false;
     }
 });

	function loadMoreData(page){
	  $.ajax(
	        {
	            url: '?page=' + page,
	            type: "get",
	            beforeSend: function()
	            {
                    loading = true;
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data.html == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
               
	            $('.ajax-load').hide();
	            $("#home_page_video").append(data.html);
                loading = false;
                $("#home_page").append(data.html2);
                // console.log(data.html);
                EBUGS.ads.load();
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	             // alert('server not responding...');
	        });
	}

    function categoryVideoSortByRecent(){
        var url = '{{ $url_segment }}';
        $.ajax({
            type: "GET",
            url: "{{ URL::to('category_video_sort_by_recent') }}",
            data: {'keyword':url},
            cache: false,
            success: function(response)
            {
                $('#category_video_data').empty().html(response);
                EBUGS.ads.load();
            }
        });
    }
    function categoryVideoSortByTopViewed(){
        var url = '{{ $url_segment }}';
        $.ajax({
            type: "GET",
            url: "{{ URL::to('category_video_sort_by_viewed') }}",
            data: {'keyword':url},
            cache: false,
            success: function(response)
            {
                $('#category_video_data').empty().html(response);
                EBUGS.ads.load();
            }
        });
    }
    function categoryVideoSortByTopLiked(){
        var url = '{{ $url_segment }}';
        $.ajax({
            type: "GET",
            url: "{{ URL::to('category_video_sort_by_liked') }}",
            data: {'keyword':url},
            cache: false,
            success: function(response)
            {
                $('#category_video_data').empty().html(response);
                EBUGS.ads.load();
            }
        });
    }
    function categoryVideoSortByTopTrending(){
        var url = '{{ $url_segment }}';
        $.ajax({
            type: "GET",
            url: "{{ URL::to('category_video_sort_by_trending') }}",
            data: {'keyword':url},
            cache: false,
            success: function(response)
            {
                $('#category_video_data').empty().html(response);
                EBUGS.ads.load();
            }
        });
    }
    function categoryVideoSortByTopLongest(){
        var url = '{{ $url_segment }}';
        $.ajax({
            type: "GET",
            url: "{{ URL::to('category_video_sort_by_longest') }}",
            data: {'keyword':url},
            cache: false,
            success: function(response)
            {
                $('#category_video_data').empty().html(response);
                EBUGS.ads.load();
            }
        });
    }
</script>
<style>
    #owl-demo1 .item{ 
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }

 #owl-demo1 .item{ 
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }
    .customNavigation{
      text-align: center;
    }
    .customNavigation a{
      -webkit-user-select: none;
      -khtml-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }
    </style>

<script>
    $(document).ready(function() {

      var owl = $("#owl-demo1");

      owl.owlCarousel({
  
      itemsTablet: [600,4], //2 items between 600 and 0;
      itemsMobile :[600,4] // itemsMobile disabled - inherit from itemsTablet option
      
      }); 
    });
    </script>
  <script>
           $(document).ready(function() {

               var owl = $("#reward_tags1");

               owl.owlCarousel({

                   items: 14, //10 items above 1000px browser width
                   itemsDesktop: [1000, 6], //5 items between 1000px and 901px
                   itemsDesktopSmall: [900, 8], // 3 items betweem 900px and 601px
                   itemsTablet: [600, 7], //2 items between 600 and 0;
                   itemsMobile: [600, 7], // itemsMobile disabled - inherit from itemsTablet option
               });

           });
       </script>

<script>
window.onscroll = function() {myFunction()};

var header = document.getElementById("categories-shown-in-all");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
</script>

@endsection

