@extends('layouts.home')

<?php

$thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH . $videoData->thumb->thumb;
$videoUrlPath = VIDEO_DISPLAY_PATH . $videoData->video;

if (!$loggedInUser) {
    session(['url.intended' => str_replace('{key}', $videoData->video_key, app('router')->current()->uri())]);
    $isFollowing = false;
} else {
    $isFollowing = $activeUser->isFollowing($videoData->user->id);
}


$userBasePath = '';
$userUrlPath = '';
if (isset($videoData->user->photo)) {
    $userBasePath = PROFILE_SMALL_UPLOAD_PATH . $videoData->user->photo;
    $userUrlPath = PROFILE_SMALL_DISPLAY_PATH . $videoData->user->photo;
}

$curuserBastPath = '';
$curuserUrlPath = '';

if (!empty($activeUser->photo)) {
    $curuserBastPath = PROFILE_SMALL_UPLOAD_PATH . $activeUser->photo;
    $curuserUrlPath = PROFILE_SMALL_DISPLAY_PATH . $activeUser->photo;
    $userID = $activeUser->id;
}
$artist = \App\Models\ArtistAbout::where(['user_id' => $videoData->user_id, 'isDeleted' => 0])->first();

?>

@section('meta_tags')
    <meta name="description" content="{{ $videoData->description ?? '' }}">
    <meta name="keywords" content="{{ $videoData->title ?? '' }}">
    <meta property="og:title" content="{{ $videoData->title ?? '' }} Vidoes"/>
    <meta property="og:url" content="{{ url("watch/".$videoData->video_key) }}"/>
    <meta property="og:description" content="{{$videoData->description}}"/>
    <meta property="og:image" content="{{$thumbUrlPath}}"/>
    <meta property="og:type" content="Entertainment Bugs"/>
@endsection
@section('content')
    <style>

        .single-video.light.content-wrapper {
            background: #a0a099;
            padding-bottom: 30px !important;
        }

        .single-video h1 {
            color: #fff;
        }

        .left a {
            color: #fff;
        }

        #spacer-extra {
            display: none;
        }

        .right_big_video {
            width: 100%;
        }


        #about_mbl a.new_tg {
            background: #e5e5e5;
            border: 0px;
            color: #000;
            line-height: 27px;
            height: 25px;
            padding: 9px 10px 0px 10px;
            margin-bottom: 1px;
            margin-right: 4px;
        }


        .b-video img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .single-video.light.content-wrapper {
            padding: 30px 0 0 0;
        }


        .b-video .v-views .v-percent {
            color: #28b47e;
            display: inline-block;
            position: relative;
            padding-left: 19px;
        }

        #recom .videoitem button {
            background: #1769d4;
            border: 0;
            color: #fff;
            font-size: 9px;
            padding: 5px 8px 1px 9px;
            position: absolute;
            bottom: 32px;
            right: 18px;
        }

        .user_heading-title-bg {
            display: block !Important;
        }

        .videoitem .user .shortnamevd {
            background: #28b47e !important;
            border-radius: 100px;
            width: 30px;
            color: #fff;
            height: 30px;
            text-align: center;
            margin-bottom: 7px;
            line-height: 33px;
            font-weight: bold;
            font-size: 14px;
        }

        .single-video .custom-tabs .tabs-panel a i.unliked {
            color: #637076 !important;
        }

        .single-video .custom-tabs .tabs-panel a i.dislikeinactive {
            color: #637076 !important;
        }

        .single-video .custom-tabs .tabs-panel a i.watchlaterinactive {
            color: #637076 !important;
        }

        #live-video-user-list-home .cb-header {
            margin-bottom: 0px;
            border: 0px;
        }

        .list-inline li.active a {
            color: #f72e5e !important;
        }

        #single_tag-Video_display .similar-v.single-video.video-mobile-02 {
            clear: both;
            overflow: hidden;
            margin-bottom: 20px;
            padding-left: 9px;
        }

        #single_tag-Video_display .col-lg-12.videoitem {
            width: 25%;
            padding: 0 7px;
            float: left;
        }

        #single_tag-Video_display .single-video .videoitem .v-img {
            width: 100%;
            padding: 0 0px;
            float: none;
            height: 105px;
            margin: 0 0 0px 0;
        }

        #single_tag-Video_display .v-desc a {
            font-size: 13px;
            line-height: 15px;
            display: block;
            height: 30px;
            overflow: hidden;
            margin-bottom: 3px;
        }

        #single_tag-Video_display .single-video .videoitem .user {
            width: 40px;
            clear: both;
            overflow: hidden;
            position: absolute;
            right: 1px;
            bottom: -3px;
            z-index: 99;
        }

        .single-video .h-video {
            margin-bottom: 15px;
        }

        .info-all-new {
            display: none;
        }

        .single-video .adblock {
            padding-top: 0px;
        }

        video {
            height: 395px !important;
        }

        #only-mobile-video-details {
            display: none;
        }

        .sv-views-progress {
            top: 18px;
        }

        .single-video .author .sv-views {
            position: relative;
        }

        .sv-name span {
            text-align: left;
            display: block;
        }

        .similar-v.single-video.video-mobile-02 .h-video.row {
            margin-bottom: 0px;
        }

        .similar-v.single-video.video-mobile-02 .col-sm-12.col-xs-6 {
            padding: 0 5px;
        }

        #ght {
            margin-top: 20px;
        }

        #desktop-social {
            padding: 10px 0 0 0;
        }

        #desktop-social .social a {
            float: right !important;
            margin: 0 0 0 7px !important;
        }

        #desktop-social .social p {
            float: left !important;
            font-size: 12px;
            line-height: 30px;
        }

        #video_up_preview .form-group {
            margin-bottom: 6px;
        }


        /* new white bg css  start ---------------------1056--------------------------------- */
        body.light .content-wrapper {
            background-color: #edecec
        }

        .single-video .custom-tabs .tabs-content > div.active {
            display: block !important;
            background: #fff;
        }

        .single-video .custom-tabs .tabs-panel > a.active {
            background-color: #f72e5e;
        }

        .single-video .custom-tabs .tabs-panel {
            background: #fff;
        }

        #recom .b-video.last-row {
            position: relative;
            min-height: 85px;
            background: #fff;
            margin-bottom: 20px;
            border-radius: 0 10px 10px 0;
        }

        #recom .v-desc a {
            width: 50%;
            font-size: 13px;
            line-height: 18px;
            display: block;
            font-weight: 400;
            padding-right: 20px;
            margin-bottom: 10px;
            height: 34px;
            overflow: hidden;
            margin-top: 3px;
        }

        #recom .videoitem .user {
            width: 40px;
            right: -24px;
            bottom: 40px;
        }

        #recom .v-views {
            padding-bottom: 0px;
            font-size: 11px;
        }

        #ght {
            margin-top: 20px;
            background: #fff;
            padding: 21px;
        }

        #recom .videoitem {
            padding: 0px;
        }

        #recom .videoitem .v-img {
            height: 85px;
        }

        #recom {
            margin-top: -17px;
        }

        .similar-v.single-video.video-mobile-02 .videoitem {
            margin-bottom: 20px;
        }

        #similar-b-top {
            margin: 0;
            padding: 13px 0 16px 0px;
        }

        footer {
            background: #fff
        }

        /* new white bg css end --------------------------1056---------------------------- */
        #single_tag-Video_display .similar-v.single-video.video-mobile-02 {
            background: #fff;
            padding: 20px 25px 0 25px;
        }

        @media only screen and (max-width: 767px) {

            /* new white bg css  start ------------------------1056------------------------------ */
            #single_tag-Video_display .similar-v.single-video.video-mobile-02 {
                background: #fff;
                padding: 0px;
            }

            body.light .content-wrapper {
                background-color: #fff
            }

            #recom .b-video.last-row {
                position: relative;
                min-height: 100px;
                margin-bottom: 11px;
                border-radius: 0 0;
                background: #f4f3f3;
            }

            #recom .videoitem .user {
                width: 40px;
                right: -4px;
                bottom: -1px;
            }

            #recom .videoitem .v-img {
                width: 155px;
                margin-right: 8px;
                height: 100px;
            }

            #recom .v-desc a {
                font-weight: 500;
            }

            #recom-head {
                background: #f4f3f3;
                margin: 0 0 20px 0 !important;
                padding: 19px 15px 15px 15px !important;
            }

            #recom {
                margin-top: 0px;
            }

            /* new white bg css  End ----------------------------1056-------------------------- */
            .info-all-new a {
                width: 31.3%;
                margin: 0 0.7%;
                float: left
            }

            .info-all-new span {
                width: 31.3%;
                margin: 0 0.7%;
                float: left
            }

            .info-all-new span a {
                width: auto;
                margin: 0 0.0%;
                float: none
            }

            .user a.user_img {
                margin: 0 10px 5px 0;
            }


            .b-video .v-views {
                font-size: 11px;
            }

            #single_tag-Video_display .v-desc a {
                font-size: 12px;
            }

            #only-mobile-more-video {
                display: block !important;
                width: 100%;
                padding: 4px 0 0 0;
                height: 90px;
                margin-bottom: 0;
                overflow: hidden;
                border-bottom: solid 1px #eceff0;
            }

            #only-mobile-more-video .icons_b {
                position: relative;
                float: left;
                width: 54px;
                height: 82px;
                margin-bottom: 5px;
                margin-right: 2px;
                margin-left: 2px;
                overflow: hidden;
            }

            #only-mobile-more-video .icons_b.first1 {
                width: 46px;
            }

            #only-mobile-more-video .icon_small a {
                margin-bottom: 0;
            }

            #only-mobile-more-video .icons_b.first1 {
                height: 53px
            }

            #only-mobile-more-video .icons_b.first1 a {
                color: #f72e5e;
                padding: 17px 0 0 0;
                border-radius: 0px;
                height: 42px;
            }

            #only-mobile-more-video .icons_b span {
                position: relative;
                left: 4px;
                color: #000;
                opacity: 1;
                padding: 1px 0 0 0;
                text-align: center;
                z-index: 1;
                background: none !important;
                display: block;
                width: 95%;
                line-height: 14px;
                height: 17px;
                overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;
            }


            .single-video .videoitem .v-img {
                float: left;
                width: 45%;
                margin-right: 15px;
                height: 87px;
            }

            .single-video .videoitem .user {
                bottom: -16px;
                right: -7px;
            }

            #recom .col-lg-12 {
                padding: 0px;
            }

            .single-video .b-video.last-row {
                position: relative;
                min-height: 87px;
            }

            #single_tag-Video_display .col-lg-12.videoitem {
                width: 50%;
                padding: 0 7px;
                float: left;
            }

            .video-mobile-02 > .row {
                display: block;
            }

            #single_tag-Video_display .single-video .videoitem .v-img {
                height: 100px;
            }

            #single_tag-Video_display .single-video .videoitem .user {
                right: 0px;
                bottom: -3px;
                z-index: 99;
            }

            #single_tag-Video_display .single-video .videoitem:nth-child(even) .user {
                right: 6px;
                bottom: -3px;
                z-index: 99;
            }

            #single_tag-Video_display .b-video .v-views {
                font-size: 11px;
            }

            #taag0 form {
                display: inline-block;
            }

            #only-mobile-video-details .sv-name {
                margin: 5px 0 0 0;
                padding: 0 0 0 0;
                float: left;
                width: 80%;
            }

            .single-video .custom-tabs .tabs-content > div.active {
                display: none !important;
            }

            #first_ad {
                display: none !important;
            }

            .single-v-footer-switch {
                padding: 11px 0 0 0;
            }

            .tabs-panel.only_mobile {
                display: none !important
            }

            .similar-v.single-video.video-mobile-02 .h-video.row {
                margin-bottom: 15px;
                padding: 0 10px;
            }

            .container .sv-video video {
                height: auto !important;
            }

            #only-design-tab-in-mob .acide-panel {
                height: 40px;
                width: 100%;
            }

            #only-design-tab-in-mob .tabs-panel a {
                margin: 0px !Important;
                width: 25%;
            }

            #only-design-tab-in-mob .tabs-panel {
                border-top: solid 1px #eceff0;
                padding: 13px 0 0 0
            }

            .single-video .content-block .cb-header {
                margin-bottom: 2px;
            }

            #no-mb091 {
                display: none;
            }

            #only-mobile-video-details {
                display: block !important;
                padding: 7px 0 0 0;
            }

            .single-video.light .col-lg-4.col-xs-12.col-sm-12.hidden-xs {
                display: block ! important;
                margin: 26px 0 0 0;
            }

            .single-video.light .col-lg-4.col-xs-12.col-sm-12.hidden-xs .col-lg-5.col-sm-5 {
                width: 50%;
                float: left;
            }

            .single-video.light .col-lg-4.col-xs-12.col-sm-12.hidden-xs .col-lg-7.col-sm-7 {
                width: 50%;
                float: left;
                padding: 0;
            }

            #ght {
                display: none;
            }

            .form-group input {
                background: #e5e5e5;
                border: 0px;
                color: #000;
                line-height: 27px;
                height: 25px;
                padding: 1px 10px 0 10px;
                margin-bottom: 1px;
            }

            .form-group form {
                display: inline-block;
            }

            .acide-panel.acide-panel-top {
                width: 100%;
                float: left;
            }

            .single-v-footer-switch > a i::before {
                color: #fff;
            }

            .single-v-footer-switch > a span {
                color: #fff;
            }

            #only-mobile-video-details {
                display: block !important;
            }

            .sv-video img.sv-avatar {
                width: 50px;
                border-radius: 100px;
                height: 50px;
                object-fit: cover;
            }

            #only-mobile-video-details .sv-name {
                margin: 5px 0 0 0;
                padding: 0 0 0 0;
            }

            #only-mobile-video-details h1 {
                font-size: 14px !important;
                padding: 5px 0 0 0;
                line-height: 16px;
            }

            #only-mobile-video-details a {
                font-size: 12px !important;
            }

            #only-mobile-video-details span {
                font-size: 11px !important;
                color: #fff;
            }

            #only-mobile-video-details span a {
                font-size: 11px !important;
                color: #fff;
            }

            #only-mobile-video-details .img-pi {
                margin-right: 10px;
            }

            #only-mobile-video-details #video_up_preview {
                border-top: solid 1px #eceff0;
                padding: 15px 0 0;
                margin-top: 8px
            }

            #only-mobile-video-details #video_up_preview .form-group label {
                font-size: 12px;
            }

            #only-mobile-video-details #video_up_preview span {
                font-size: 12px;
            }

            #only-mobile-video-details .form-group a {
                font-size: 12px !important;
                margin-right: 0px;
            }

            #only-mobile-video-details .sv-tags small {
                padding: 2px 10px 0 10px;
            }

            #only-mobile-video-details .form-group {
                margin-bottom: 6px;
            }

            .info-all-new {
                display: block;
            }

            .single-video .h-video {
                margin-bottom: 15px;
            }

            .sv-name span {
                text-align: left;
                display: inline-block;
            }

            #no-view-on-mobile {
                display: none;
            }

            #taag0 input {
                background: #e5e5e5;
                border: 0px;
                color: #000;
                line-height: 27px;
                height: 25px;
                padding: 1px 10px 0 10px;
                margin-bottom: 1px;
            }

        }

    </style>


    <style>
        .trending-rewards-tags {
            margin-top: 0px;
        }

        .trending-rewards-tags ul {
            list-style: none;
            text-align: left;
            margin: 0 0 15px 0;
            padding: 0px;
        }

        .trending-rewards-tags li {
            display: inline-block;
            text-align: left;
        }

        .trending-rewards-tags li a {
            background: #e5e5e5;
            border: 0px;
            color: #000;
            line-height: 27px;
            height: 25px;
            padding: 2px 10px 0 10px;
            margin-bottom: 1px;
            font-size: 12px;
        }

        #reward_tags1 .owl-item {
            margin-right: 5px;
            width: auto !important;
        }

        .tag_head {
            margin-right: 5px;
            float: left;
            width: 11%;
            line-height: 30px;
            color: #fff;
            height: 20px;
        }

        #reward_tags1 {
            float: right;
            width: 87%;
        }


    </style>


    <div class="single-video light content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xs-12 col-sm-8">
                    @if(!$videoData->isDeleted && !$videoData->user->isDeleted)
                        <div class="sv-video right_big_video">
                            <img src="{{ asset('public/img/logo_footer.png') }}"
                                 style="position:absolute; right:10px; top:5px; width:35px">
                            <video poster="{{$thumbUrlPath}}" id="mainVideo" controls style="width:100%;height:auto;">
                                <source src="{{$videoUrlPath}}" type="video/mp4"/>
                            </video>
                            <h1 id="no-mb091"> {{$videoData->title}} </h1>
                            <div id="only-mobile-video-details">
                                <div class="author-head">
                                    <a class="img-pi" href="{{ user_url(@$videoData->user->slug) }}">
                                        @if(file_exists($userBasePath))
                                            {{HTML::image($userUrlPath,null,['class'=>'sv-avatar'])}}
                                        @else
                                            {{HTML::image('public/img/ava2.png',null,['class'=>'sv-avatar'])}}
                                        @endif
                                    </a>
                                    <div class="sv-name">
                                        <h1> {{$videoData->title}} </h1>
                                        <a style="color:gray"
                                           href="{{ user_url(@$videoData->user->slug) }}">{{@$videoData->user->name}}</a>
                                        - <span style="color:gray">{{@$videoData->total_views_count}} views</span> -
                                        <span style="color:gray">{{@$videoData->timing}}</span>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div id="video_up_preview">
                                    <div class="col-lg-12" id="">
                                        <div class="form-group">
                                            <label for="e2">About:</label>
                                            <span>{{$videoData->description}}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12" id=" ">
                                        <div class="form-group">
                                            <label for="e1">Video Tags:</label>
                                            <p class="sv-tags" style="margin-bottom: 5px;">
                                            @if(!empty($videoTags))
                                                @foreach($videoTags as $slug => $tag)
                                                    <form action="{{URL::to('search')}}" method="post">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="hidden" class="form-control" name="searchdata"
                                                               aria-describedby="sizing-addon2" value="{{ $tag }}">
                                                        <input type="hidden" name="searchType" id="searchType"
                                                               value="video"/>
                                                        <a href="{{url('/reward-trending-tags/'.$tag)}}"> {{ $tag }} </a>
                                                    </form>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="e3">Language:</label>
                                            <span>{{$videoData->language}}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="e3">Category:</label>
                                            @if(!empty($videoCategories))
                                                <span>
                                            @php $tc = count($videoCategories); $t = 0; @endphp
                                                    @foreach($videoCategories as $slug => $category)
                                                        <a href="{{URL::to('category/'.$slug)}}">{{$category}}</a>@php $t++; echo (($tc != $t)?',&nbsp;':""); @endphp
                                                    @endforeach
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label for="e3">Release Date:</label>
                                            <span>{{ \Carbon\Carbon::parse($videoData->created_at)->diffForHumans() }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <div class="info">
                            <div class="custom-tabs">
                                <div class="tabs-panel hidden-xs">


                                    <a href="#" data-tab="tab-1" class="active"
                                       style="width: 50%;text-align: center;display: block;">
                                        <i style="float: none;display: inline-block;color:#fff;" class="fa fa-trophy"
                                           data-toggle="tooltip" data-placement="top" title="About"></i>
                                        <span style="font-size: 16px;margin: 2px 0 0 0;color:#fff;margin: 2px 0 0 0;float: none;display: inline-block">Rank 115th</span>
                                    </a>
                                    <div class="acide-panel hidden-xs">
                                        <a href="{{$videoData->video_key}}" style="font-size:15px;line-height:25px;"
                                           id="watchlaterCurrentvideo"><i class="cv cvicon-cv-watch-later"
                                                                          data-toggle="tooltip" data-placement="top"
                                                                          title=""
                                                                          data-original-title="Watch Later"></i><span
                                                    id="watchLaterCount">{{ $videoWatchLaterCount }}</span></a>
                                        <a href="{{$videoData->video_key}}" style="font-size:15px;line-height:25px;"
                                           id="likevideo"><i class="fa fa-thumbs-up" data-toggle="tooltip"
                                                             data-placement="top" title="" style="margin-right: 5px;"
                                                             data-original-title="Liked"></i> <span
                                                    id="likedCount">{{ $videoLikeCount }}</span></a>
                                        <a href="{{$videoData->video_key}}" style="font-size:15px;line-height:25px;"
                                           id="dislikevideo"><i class="fa fa-thumbs-down" data-toggle="tooltip"
                                                                data-placement="top" title="" style="margin-right: 5px;"
                                                                data-original-title="Unlinked"></i><span
                                                    id="unLikedCount">{{ $videoDisLikeCount }}</span></a>
                                        <a data-toggle="modal" href="#myModal" style="font-size:15px;line-height:25px;"
                                           id="videoMarkflag"><i class="cv cvicon-cv-flag" data-toggle="tooltip"
                                                                 data-placement="top" title=""
                                                                 data-original-title="Flag"></i><span
                                                    id="videoFlag">{{ $videoFlagCount }}</span></a>

                                        <!-- Modal -->
                                        <div class="modal fade" id="myModal" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">
                                                            &times;
                                                        </button>
                                                        <h4 class="modal-title">Comments</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form>
                                                            <div class="form-group">
                                                                <label for="exampleFormControlTextarea1">Write Video
                                                                    Flag Comments:</label>
                                                                <input type="hidden" id="flagKey"
                                                                       value="{{$videoData->video_key}}"/>
                                                                <textarea class="form-control" id="flagcomments"
                                                                          rows="3"></textarea>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" id="flagcommentbtn"
                                                                class="btn btn-success">Submit
                                                        </button>
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">Close
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="acide-panel acide-panel-top">
                                    <a href="#"><i class="cv cvicon-cv-liked" data-toggle="tooltip" data-placement="top"
                                                   title="Liked"></i></a>
                                    <a href="#"><i class="cv cvicon-cv-watch-later" data-toggle="tooltip"
                                                   data-placement="top" title="Watch Later"></i></a>
                                    <a href="#"><i class="cv cvicon-cv-flag" data-toggle="tooltip" data-placement="top"
                                                   title="Flag"></i></a>
                                </div>
                                <div class="tabs-panel only_mobile">
                                    <a href="#" data-tab="tab-2">
                                        <i class="cv cvicon-cv-share" data-toggle="tooltip" data-placement="top"
                                           title="Share"></i>
                                        <span>Share</span>
                                    </a>
                                </div>


                                <div class="clearfix"></div>
                                <!-- BEGIN tabs-content -->
                                <div class="tabs-content">
                                    <!-- BEGIN tab-1 -->
                                    <div class="tab-1">
                                        <div class="col-xs-12" style="width:100%">
                                            <div class="author"
                                                 style="box-shadow: none;border: 0px;padding: 20px 0 10px 0px;margin: 0;">
                                                <div class="sv-views">
                                                    <div class="sv-views-count">
                                                        {{$videoData->total_views_count}} views
                                                    </div>
                                                    <div class="sv-views-progress">
                                                        <div class="sv-views-progress-bar"></div>
                                                    </div>
                                                </div>


                                                <div class="author-head">
                                                    <a class="atr" target="_blank"
                                                       href="{{ user_url(@$videoData->user->slug) }}">
                                                        @if(file_exists($userBasePath))
                                                            {{HTML::image($userUrlPath,null,['class'=>'sv-avatar'])}}
                                                        @else
                                                            {{HTML::image('public/img/ava2.png',null,['class'=>'sv-avatar'])}}
                                                        @endif
                                                    </a>

                                                    <div class="sv-name">
                                                        <div><a target="_blank"
                                                                href="{{ user_url(@$videoData->user->slug) }}">{{@$videoData->user->name}}</a>
                                                            . {{ $userVideoCount }} Videos
                                                        </div>
                                                        <div class="c-sub hidden-xs" id="followunfollowbtn">

                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="author-border"></div>

                                                <div class="clearfix"></div>
                                            </div>
                                            <div id="video_up_preview">
                                                <div class="col-lg-12" id="about_mbl">
                                                    <div class="form-group">
                                                        <label for="e2">About:</label>
                                                        <span>{{$videoData->description}}</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12" id="about_mbl">
                                                    <div class="form-group">
                                                        <label for="e1">Video Tags:</label>
                                                        <p class="sv-tags">

                                                            @if(!empty($videoTags))
                                                                @foreach($videoTags as $slug => $tag)
                                                                    <a class="new_tg"
                                                                       href="{{url('/reward-trending-tags/'.$tag)}}"> {{ $tag }} </a>
                                                                @endforeach
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="e3">Language:</label>
                                                        <span>{{$videoData->language}}</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="e3">Category:</label>
                                                        @if(!empty($videoCategories))
                                                            <span>
												   @php $tc = count($videoCategories); $t = 0; @endphp
                                                                @foreach($videoCategories as $slug => $category)
                                                                    {{$category}}
                                                                    <!--<a href="{{URL::to('/'.$slug)}}"></a>-->
                                                                    @php $t++; echo (($tc != $t)?',&nbsp;':""); @endphp
                                                                @endforeach
												   </span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label for="e3">Particiapte Date:</label>
                                                        <span>2 Days ago</span>
                                                    </div>
                                                </div>

                                                <Style>
                                                    #video_share_option {
                                                        width: 200px;
                                                        float: right;
                                                        margin-right: -21px;
                                                        position: relative;
                                                        z-index: 7;
                                                        margin-top: -40px;
                                                    }

                                                    .social a.whatsapp-i {
                                                        background-color: #4FCE5D;
                                                    }

                                                    .social a.instagram {
                                                        background-color: #d62976;
                                                        padding-top: 7px;
                                                    }

                                                    .social a {
                                                        border-radius: 0
                                                    }

                                                    #video_share_option a:not(:last-child) {
                                                        margin-right: 6px;
                                                    }


                                                </Style>

                                                <div class="social" id="video_share_option">
                                                    <a href="" style="color: #000;font-size: 12px;margin: 3px 5px 0 0;">
                                                        Share:</a>
                                                    <!-- AddToAny BEGIN -->
                                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                                        <a class="a2a_button_facebook"></a>
                                                        <a class="a2a_button_twitter"></a>
                                                        <a class="a2a_button_whatsapp"></a>
                                                    </div>

                                                    <!-- AddToAny END -->
                                                    <!--<a href="" id="facebook" class="facebook a2a_button_facebook"><i class="fa fa-facebook"></i></a>-->
                                                    <!--<a href="" id="twitter" class="twitter a2a_button_twitter"><i class="fa fa fa-twitter"></i></a>-->
                                                    <!--<a href="#" class="instagram a2a_button_instagram"><i class="fa fa-instagram"></i></a>-->
                                                    <!--<a href="#" class="whatsapp a2a_button_whatsapp"><i class="fa fa-whatsapp" style="line-height: 17px;"></i></a>  -->


                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                    <!-- END tab-1 -->
                                    <!-- BEGIN tab-2 -->
                                    <div class="tab-2">
                                        <h4>Share:</h4>
                                        <div class="social">
                                            <div class="addthis_inline_share_toolbox_n0k8"></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-9">
                                                <h4>Link:</h4>
                                                <label class="clipboard">
                                                    <input type="text" id="btn-copy-txt" name="#" class="share-link"
                                                           value="https://entertainmentbugs.com/DwGgdfe-C6c" readonly>
                                                    <div class="btn-copy" data-clipboard-target=".share-link"
                                                         onclick="copyText()">Copy
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="tab-popup popup-share">
                                            <div class="tab-popup-head">
                                                <i class="cv cvicon-cv-share"></i>
                                                <span>Share this video</span>
                                                <a href="#" class="tab-popup-close"><i class="cv cvicon-cv-cancel"></i></a>
                                            </div>
                                            <div class="tab-popup-content">
                                                <h4>Copy Link:</h4>
                                                <label class="clipboard">
                                                    <input type="text" name="#" class="share-link"
                                                           value="https://entertainmentbugs.com./DwGgdfe-C6c" readonly>
                                                    <div class="btn-copy" data-clipboard-target=".share-link">Copy</div>
                                                </label>
                                            </div>
                                            <div class="tab-popup-content">
                                                <div class="popup-share-social">
                                                    <a href="#" class="facebook">
                                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                                        <span>Facebook</span>
                                                    </a>
                                                    <a href="#" class="twitter">
                                                        <i class="fa fa fa-twitter" aria-hidden="true"></i>
                                                        <span>Twitter</span>
                                                    </a>
                                                    <a href="#" class="google">
                                                        <i class="fa fa-google-plus" aria-hidden="true"></i>
                                                        <span>Google Plus</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- END tab-2 -->
                                    <!-- END tab-5 -->
                                </div>
                                <!-- END tabs-content -->
                            </div>
                            @else
                                <div class="under-review text-center"><h5>This content is under review</h5></div>
                            @endif
                            <div class="caption hidden-xs" id="similar-b-top">
                                <div class="left">
                                    <a href="#">Similar Normal Video's</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="single-v-footer" id="single_tag-Video_display">
                                <div class="single-v-footer-switch">
                                    <a href="javascript:void(0)" class="active" data-toggle=".similar-v">
                                        <i class="cv cvicon-cv-play-circle"></i>
                                        <span>Similar Videos</span>
                                    </a>
                                    <a href="javascript:void(0)" data-toggle=".comments">
                                        <i class="cv cvicon-cv-comment"></i>
                                        <span> Comments</span>
                                    </a>
                                </div>
                                <div class="similar-v single-video video-mobile-02">
                                    <div class="row">
                                        @if(!empty($similarVideos))
                                            @foreach($similarVideos as $video)
                                                @include('elements.post.video_re_wardsimilar')
                                            @endforeach
                                        @endif
                                    </div>
                                    <div style="cleaar:both; overflow:hidden"></div>
                                </div>
                                @if($activeUser)
                                    <div class="comments" id="ght">
                                        <div class="reply-comment">
                                            <div class="rc-header"><i class="cv cvicon-cv-comment"></i>
                                                <span class="semibold">{{ $videoData->total_comments_count }}</span>
                                                Comments
                                            </div>
                                            <div class="rc-ava">
                                                <a target="_blank" href="{{ user_url(@$activeUser->slug) }}">
                                                    @if(file_exists($curuserBastPath))
                                                        {{HTML::image($curuserUrlPath,null,['class'=>'sv-avatar'])}}
                                                    @else
                                                        {{HTML::image('public/img/ava2.png',null,['class'=>'sv-avatar'])}}
                                                    @endif
                                                </a>
                                            </div>
                                            <div class="rc-comment">
                                                <form method="post" action="{{ route('comment.add') }}" id="commentfrm">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <textarea
                                                            rows="3"
                                                            name="comment_body"
                                                            placeholder="Share what you think?">
                                                    </textarea>
                                                    <input type="hidden" name="video_id" value="{{ $videoData->id }}"/>
                                                    <input type="hidden" name="model" value="Video"/>
                                                    <button type="submit">
                                                        <i class="cv cvicon-cv-add-comment"></i>
                                                    </button>
                                                </form>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="comments-list">
                                            <div class="cl-header">
                                                <div class="c-nav">
                                                    <ul class="list-inline">
                                                        <li><a href="#" class="active">Popular <span class="hidden-xs">Comments</span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div id="comments">
                                                @include('comment.video._comment_replies', ['comments' => $videoData->comments, 'video_id' => $videoData->id])
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="comments">
                                        <div class="reply-comment">
                                            <div class="rc-header"><i class="cv cvicon-cv-comment"></i>
                                                <span class="semibold">{{ $videoData->total_comments_count }}</span>
                                                Comments
                                            </div>
                                            <div class="rc-ava">
                                                <a target="_blank" href="#">
                                                    {{HTML::image('public/img/ava2.png',null,['class'=>'sv-avatar'])}}
                                                </a>
                                            </div>
                                            <div class="rc-comment">
                                                <form method="post" action="{{ route('comment.add') }}" id="commentfrm">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <textarea
                                                            rows="3"
                                                            name="comment_body"
                                                            placeholder="Share what you think?">
                                                </textarea>
                                                    <input type="hidden" name="video_id" value="{{ $videoData->id }}"/>
                                                    <input type="hidden" name="model" value="Video"/>
                                                    <button type="submit">
                                                        <i class="cv cvicon-cv-add-comment"></i>
                                                    </button>
                                                </form>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="comments-list">
                                            <div class="cl-header">
                                                <div class="c-nav">
                                                    <ul class="list-inline">
                                                        <li><a href="#" class="active">Popular <span class="hidden-xs">Comments</span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div id="comments">
                                                @include('comment.video._comment_replies', ['comments' => $videoData->comments, 'video_id' => $videoData->id])
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="col-lg-4 col-xs-12 col-sm-4 hidden-xs">
                    <div class="trending-rewards-tags">
                        <div class="span12" id="crousal_wrap" style="width:100%">
                            <div class="tag_head">Tags:</div>
                            <ul id="reward_tags1" class="owl-carousel" style="display:block">
                                @if(isset($tags) && count($tags)>0)
                                    @foreach($tags as $tag)
                                        <li class="item">
                                            <a href="{{url('/reward-trending-tags/'.$tag)}}">
                                                {{$tag}}
                                            </a>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div id="recom-head" class="caption" style="margin:0;padding:0px;">
                        <div class="left">
                            <a href="#"> Similar Rewards Videos</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="list" id="recom">
                        @if(!empty($recomendedVideos))
                            @foreach($recomendedVideos as $video)
                                @include('elements.post.video_re_ward')
                            @endforeach
                        @endif
                        <div class="loadmore">
                            <a href="#" style="color:#fff;"> View more reward videos</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')

    <script async src="https://static.addtoany.com/menu/page.js"></script>
    <script src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-61385eedbd8b385d"></script>

    <script>
        var isUserLogin = '{{ Session::get('user_id') }}';
        if (!isUserLogin) {
            $("#mainVideo").bind("timeupdate", function () {
                var currentTime = this.currentTime;
                if (currentTime > 5) {
                    window.location.href = "{{ url('/login') }}"
                }
            });
        }

        function copyText() {
            /* Get the text field */
            var txt = document.getElementById("btn-copy-txt");

            /* Select the text field */
            txt.select();
            txt.setSelectionRange(0, 99999); /*For mobile devices*/

            /* Copy the text inside the text field */
            document.execCommand("copy");

            $('.btn-copy').text('Copied');
        }

        $('#watchlaterCurrentvideo').click(function (e) {
            e.preventDefault();
            if (userID) {
                var video_key = $(this).attr('href');
                $.ajax({
                    type: 'get',
                    url: 'watch-later/' + video_key,
                    success: function (data) {
                        $("#watchLaterCount").text(data);
                        $(".cvicon-cv-watch-later").css('color', '#f72e5e');
                    }
                });
            } else {
                window.location.replace(APP_URL + '/login');
            }

        });

        $('#likevideo').click(function (e) {
            e.preventDefault();
            if (userID) {
                var video_key = $(this).attr('href');
                $.ajax({
                    type: 'get',
                    url: BASE.url('watch/liked-video/' + video_key),
                    success: function (data) {
                        if (data.disLikeCount > 0) {
                            $(".fa-thumbs-down").css('color', '#f72e5e');
                        }
                        $("#likedCount").text(data.likeCount);
                        $("#unLikedCount").text(data.disLikeCount);
                        $(".fa-thumbs-up").css('color', '#f72e5e');
                        $(".fa-thumbs-down").css('color', '#637076');
                    }
                });
            } else {
                window.location.replace(APP_URL + '/login');
            }
        });

        $('#dislikevideo').click(function (e) {
            e.preventDefault();
            if (userID) {
                var video_key = $(this).attr('href');
                $.ajax({
                    type: 'get',
                    url: BASE.url('watch/disliked-video/' + video_key),
                    success: function (data) {
                        if (data.likeCount > 0) {
                            $(".fa-thumbs-up").css('color', '#f72e5e');
                        }
                        $("#likedCount").text(data.likeCount);
                        $("#unLikedCount").text(data.disLikeCount);
                        $(".fa-thumbs-down").css('color', '#f72e5e');
                        $(".fa-thumbs-up").css('color', '#637076');
                    }
                });
            } else {
                window.location.replace(APP_URL + '/login');
            }
        });

        $('#flagcommentbtn').click(function (e) {
            $('#myModal').modal('toggle');
            var comment = $('#flagcomments').val();
            if (userID) {
                var video_key = $('#flagKey').val();
                $.ajax({
                    type: 'get',
                    url: 'flag-video/' + video_key + '/' + comment,
                    success: function (data) {
                        $("#videoFlag").text(data);
                        $(".cvicon-cv-flag").css('color', '#f72e5e');
                    }
                });
                $('#flagcomments').val('');
            } else {
                window.location.replace(APP_URL + '/login');
            }
        });

        if (userID) {
            var isWatchLaterByUser = '<?php echo $isWatchLaterByUser; ?>';
            var isLikedByUser = '<?php echo $isLikedByUser; ?>';
            var isDisLikedByUser = '<?php echo $isDisLikedByUser; ?>';
            var isFlagByUser = '<?php echo $isFlagByUser; ?>';
            if (isWatchLaterByUser > 0) {
                $(".cvicon-cv-watch-later").css('color', '#f72e5e');
            }

            if (isLikedByUser > 0) {
                $(".fa-thumbs-up").css('color', '#f72e5e');
            }

            if (isDisLikedByUser > 0) {
                $(".fa-thumbs-down").css('color', '#f72e5e');
            }

            if (isFlagByUser > 0) {
                $(".cvicon-cv-flag").css('color', '#f72e5e');
            }
        }

        function showMe(id) {
            $(id).toggle();
        }

        $("#commentfrm").submit(function (e) {
            e.preventDefault();
            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function (data) {
                    console.log(data);
                    $('#comments').html(''); // show response from the php script.
                    $('#comments').append(data.html);
                }
            });
            $(this)[0].reset();
        });

        var isFollowing = '<?php echo $isFollowing; ?>';
        var followCount = '<?php echo $followCount; ?>';
        follow_unfollow(isFollowing, followCount);

        $(document).on('submit', '#follow, #unfollow', function (e) {
            e.preventDefault();
            if (userID) {
                var form = $(this);
                var url = form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(),
                    success: function (data) {
                        if (!data.status) {
                            follow_unfollow(data.isFollowing, data.followCount);
                            alert(data.message);
                        } else {
                            alert(data.message);
                        }
                    }
                });
                $(this)[0].reset();
            } else {
                window.location.replace(APP_URL + '/login');
            }
        });

        function follow_unfollow(isFollowing, followCount) {
            var csrf_field = '<?php echo csrf_field(); ?>';
            var method_field = '<?php echo method_field("DELETE"); ?>';
            var videoData_User_ID = '<?php echo @$videoData->user->id; ?>';
            var unfollowformAction = '<?php echo route("unfollow", ["id" => @$videoData->user->id]); ?>';
            var followformAction = '<?php echo route("follow", ["id" => @$videoData->user->id]); ?>';

            if (isFollowing) {
                var unfollowbtn = '<form id="unfollow" action="' + unfollowformAction + '" method="POST">'
                    + csrf_field + method_field + '<button type="submit" id="delete-follow-'
                    + videoData_User_ID + '" class="c-f">Unfollow</button><div class="c-s">'
                    + followCount + '</div></form><div class="clearfix"></div>';
                $('#followunfollowbtn').html('');
                $('#followunfollowbtn').append(unfollowbtn);
            } else {
                var followbtn = '<form id="follow" action="' + followformAction + '" method="POST">'
                    + csrf_field + '<button type="submit" id="follow-user-' + videoData_User_ID + '" class="c-f">follow</button><div class="c-s">' + followCount + '</div></form><div class="clearfix"></div>';
                $('#followunfollowbtn').html('');
                $('#followunfollowbtn').append(followbtn);
            }

        }
    </script>

    <script>
        $(document).ready(function () {

            var owl = $("#reward_tags1");

            owl.owlCarousel({
                items: 6, //10 items above 1000px browser width
                itemsDesktop: [1000, 6], //5 items between 1000px and 901px
                itemsDesktopSmall: [900, 8], // 3 items betweem 900px and 601px
                itemsTablet: [600, 7], //2 items between 600 and 0;
                itemsMobile: [600, 7], // itemsMobile disabled - inherit from itemsTablet option
            });

        });
    </script>

    <script>
        const trackURL = '{{ Session::get('user_id') ? "/reward-video-watch-time/{$videoData->id}" : "/guest-reward-video-watch-time/{$videoData->id}" }}';
        // Variables to track video and AJAX status
        //const videoID = '{{ $videoData->id }}';
        var video = document.getElementById("mainVideo");
        var isVideoPlaying = true;

        // Function to send an AJAX request to update watch time
        function updateWatchTime() {
            $.ajax({
                url: trackURL,
                method: "POST",
                contentType: "application/json",
                headers: {
                    "X-CSRF-TOKEN": CSRF,
                },
                data: {},
                success: function (response) {
                    isVideoPlaying = response.status;
                },
                error: function (error) {
                    setTimeout(() => {
                        isVideoPlaying = true;
                    }, 4000);
                },
            });
        }

        // Track video progress
        video.addEventListener("timeupdate", function () {
            if (isVideoPlaying === true) {
                isVideoPlaying = false;
                updateWatchTime();
            }
        });

        // Track the window or tab close event
        window.addEventListener("beforeunload", function () {
            // Make sure to update watch time when the user leaves the page
            if (!isVideoPlaying) {
                updateWatchTime();
            }
        });

        // Handle video end event
        video.addEventListener("ended", function () {
            updateWatchTime();
        });
    </script>


@endsection