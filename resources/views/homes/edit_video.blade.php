@extends('layouts.home')
@section('content')
{{-- @php
echo $videoData;
 die;
@endphp --}}

<style>
   .upload-thumb-image{
	    padding: 9px 46px;
        border: 2px dotted #ccc;
        font-size: 16px;
        color: #999;
        margin: 5px;
   }
   
   body.light .content-wrapper {
    background-color: #f4f3f3;
}
.edit-page .u-form .checkbox label {
    font-size: 13px;
}
</style>

    <div class="content-wrapper upload-page edit-page">
        {{ Form::open(array('method' => 'post', 'id' => 'uploadVideoss')) }}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="container-fluid u-details-wrap">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="u-details">
                                <div class="row">
                                    <div class="col-lg-12 ud-caption">Upload Details</div>
                                    <div class="col-lg-2">
                                        <div class="imgplace"></div>
                                    </div>
                                    <div class="col-lg-10">
                                        <div class="u-title">{{ $videoData->title}} <span class="hidden-xs">03.08.2016 - 15.13.31.01</span></div>
                                        <div class="u-size">{{ intval($videoData->video_size) }} KB</div>
                                        <div class="u-progress">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="35"
                                                    aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="u-desc">Your video has been successfully upload.</div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="row" style="margin:40px 0 0 0;">
                        <div class="col-lg-10" id="thumbnail_image">	
                            <input name="thumb_image" type="file" id="croppie-video-thumb-file" style="display: none" accept="image/x-png,image/gif,image/jpeg" /> 
                            <a href="javascript:void(0);" title="Upload Video Thumbnail Image" class="upload-thumb-image" data-croppie data-croppie-file="#croppie-video-thumb-file" data-croppie-input="#croppie-video-thumb-input" data-croppie-output="#croppie-video-thumb-preview" data-croppie-bind="file" data-croppie-viewport='{"width": 600, "height": 300}' data-croppie-store="public/uploads/video/thumbs/" data-croppie-boundary='{"width": 600, "height": 300}'><i class="cvicon-cv-plus"></i></a>
                            <input type="hidden" name="thumb[1]" id="croppie-video-thumb-input" />
                            
                                @if(!empty($videoData->thumb->thumb))
                                    @php
                                        $videoUrl = $videoData->video_key;
                                        $thumbBastPath = VIDEO_THUMB_UPLOAD_PATH.$videoData->thumb->thumb;
                                        $thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$videoData->thumb->thumb;
                                    @endphp
                                @endif
                                <a href="javascript:void(0)" class="video-thumb-img"><img src="{{ file_exists($thumbBastPath) ? $thumbUrlPath : asset("public/img/video1-1.png") }}" id="croppie-video-thumb-preview" style="width:100px" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="u-form">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="e1">Video Title</label>
                                    {{ Form::text('title', $videoData->title, ['id' => 'video-title', 'close' => 'form-control', 'placeholder' => 'Title', 'required' => true]) }}
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="e2">About</label>
                                    {{ Form::textarea('description', $videoData->description, ['id' => 'video-description', 'class' => 'form-control', 'rows' => '3', 'required' => true, 'placeholder' => 'Description']) }}
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="e1">Video Tags</label>
                    
                                   {{ Form::text('tags', implode(',',$videoTags), ['id' => 'video-tags', 'close' => 'form-control', 'required' => true, 'placeholder' => 'Select mulitple tags']) }}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="e3">Features</label>
                                    {{ Form::select('feature', unserialize(VIDEO_FEATURES), $videoData->feature, ['class' => 'form-control', 'id' => 'video-feature', 'required' => true]) }}
                                </div>
                            </div> -->
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="e3">Subtitles</label>
                                    {{ Form::select('sub_title', unserialize(VIDEO_SUBTITLES), $videoData->sub_title, ['class' => 'form-control', 'id' => 'video-subtitle', 'required' => true]) }}
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="e3">Language</label>
                                   {{ Form::text('language', $videoData->language,['class' => 'form-control', 'placeholder' => 'Choose Language', 'id' => 'video-language'])}}
                                    
                                </div>
                            </div>

                        </div>

                        <div class="row ">
                            <div class="col-lg-12 u-category">Category ( you can select upto 2 categories )</div>
                        </div>

                        <div class="row">
                            <!-- checkbox 1col -->
                            @php
                                $newcat = [];
                                foreach ($videoData->video_category as $key => $catID){
                                    $newcat[$catID['category_id']] =  $catID['category_id'];
                                }
                            @endphp
							
                            @foreach ($videoCategories as $category)
						
                                <div class="col-lg-2 col-xs-6">
                                    <div class="checkbox">
                                        <label>
                                            <label class="checkbox">
                                                    
													 @if (in_array($category->id, $newcat))
                                                        {{ Form::checkbox('categories[]', $category->id, ['class' => 'checked'])}}
                                                    @else
                                                        {{ Form::checkbox('categories[]', $category->id)}}
                                                    @endif
                                                <span class="arrow"></span>
                                            </label> {{$category->category}}
                                        </label>
                                    </div>
									
									<?php $subcat = DB::table('categories')
            ->select('*')
            ->where('parent_id', $category->id)           
            ->get();
			
			
?>



                        <!-- checkbox 1col -->

						@if(!empty($subcat))

							@foreach($subcat as $catName1)
								@if($catName1->category!='')
								
								<div class="subcat <?php echo $category->id ?>" style="display:none;">

									 <div class="checkbox">

										<label>

											<label class="checkbox">

											 @if (in_array($catName1->id, $newcat))
                                                        {{ Form::checkbox('categories[]', $catName1->id, ['class' => 'checked'])}}
                                                    @else
                                                        {{ Form::checkbox('categories[]', $catName1->id)}}
                                                    @endif
												<span class="arrow"></span>

											</label> {{ $catName1->category }}

										</label>

									 </div>

								</div>
								
								@endif	
						

							@endforeach

						

						@endif
						
						
                                </div>
                            @endforeach
                            

                        </div>
                    </div>

                    <div class="u-area mt-small">
                        <form action="#" method="post">
                            <button class="btn btn-primary u-btn">Save</button>
                        </form>
                    </div>
                    <div class="u-terms">
                        <p>By submitting your videos to Entertainment Bugs, you acknowledge that you agree to Entertainment
                            Bugs <a href="#">Terms of Service</a> and <a href="#">Community Guidelines</a>.</p>
                        <p class="hidden-xs">Please be sure not to violate others' copyright or privacy rights. Learn more
                        </p>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close()}}	
    </div>

    {{ HTML::style('public/css/bootstrap-tagsinput.css')}}
    {{ HTML::script('public/js/bootstrap-tagsinput.js')}}
    {{ HTML::script('public/js/bootstrap3-typeahead.js')}}

    <script type="text/javascript">
        $(document).ready(function() {

 $('input[type="checkbox"]').click(function(){
          var val = $(this).attr("value");
          $("." + val).toggle();
        });
            $(".upload-cover-image").click(function () {
              $("#profile-cover-image").trigger('click');
            });

            // $(".upload-thumb-image").click(function () {
            //   $("#profile-thumb-image").trigger('click');
            // });

            $('#video-tags').tagsinput({
                typeahead: {
                    source: function(query) {
                        //return $.get('http://someservice.com');
                    }
                }
            });

            $('#video-language').tagsinput({
                typeahead: {
                    source: ['English', 'Hindi', 'Tamil', 'Bengali', 'Gujarati']
                },
                freeInput: true
            });
        });
    </script>

@endsection
