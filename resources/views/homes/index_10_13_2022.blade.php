@extends('layouts.home')
@section('content')
<style>.all-trailer{clear:both; overflow:hidden;margin: 20px 0 0px 0;background: #fff;}
.head-tr{float:left; width:100%;}
.head-tr h2 {font-size: 13px;color: #7e7e7e;margin: 20px 0 12px 12px;}
.all-trailer_inside {width: 100%;padding: 0 14px 14px 14px;  height:120px; clear: both;overflow: hidden;}
.all-trailer_inside a img {transition: transform .2s; /* Animation */width: 100%;}
.all-trailer_inside a {width: 96%;overflow: hidden;margin: 0 1%;transition: transform .2s;float: left;height: 100px;}
.all-trailer_inside a:hover img{-ms-transform: scale(1.1); /* IE 9 */-webkit-transform: scale(1.1); /* Safari 3-8 */transform: scale(1.1); }

#home_page_first_photo .cb-content.chanels-row {height: 180px;overflow: hidden;}
#live-video-user-list-home{padding-bottom:0px;}
.list-inline li.active0 a{color:#f72e5e !important;}
#video-bl091{float:right !important;    padding: 50px 0px 20px 45px !important;}
#home_page_video .small_block .v-img img {width: 100%;height: 100%;object-fit: cover;}

.small_block.videoitem .v-img {float: left;width: 43%;margin-right: 10px;height: 64px;overflow: hidden;}
    
#owl-photo .owl-pagination {display: block;}
#home_page_first_photo{padding: 5px 0 2px 0;}
#home-page-banner .small_block.videoitem { background: #fff;}
       
.small_block.videoitem {width: 24%;margin-right: 1%;margin-top: 18px;float: left}
.small_block.videoitem .v-img {float: left;width: 43%;margin-right: 10px;}
.small_block.videoitem .b-video .v-desc {padding-top: 3px;}
.small_block.videoitem .b-video .v-views .v-percent {display: none}
 

#home-page-banner .v-desc a {text-align: left;display: block;font-size: 11px;padding-right: 11px;    margin-top:7px;line-height: 14px}
#home-page-banner .b-video .v-desc {    height: 38px;margin-bottom: 2px;overflow: hidden;}
#home-page-banner .b-video .v-views {font-size: 12px;  text-align: left;    padding-top: 0px;}
#home-page-banner .small_block.videoitem .v-img1 {float: left;width: 43%;    position: relative;margin-right: 10px;}
#home-page-banner .small_block.videoitem .v-img1 img {width: 100%;}
.b-video .v-img1 .watched {position: absolute;top: 5px;left: 5px;padding: 6px 4px 0px 5px;color: #ffffff;font-size: 12px;    line-height: 13px;font-weight: 600;    border-radius: 2px;background-color: #28b47e;}
.b-video .v-img1 .watched-mask {background-color: #ababab;opacity: 0.6;border-radius: 2px;width: 100%;height: 100%;position: absolute;top: 0px;left: 0px;}
.services-sec i{font-size:42px; color:#f72e5e;}
.services-sec {padding: 55px 0 0;}
.services-row {	border-bottom: 1px solid #e2ebef;padding-bottom: 15px;}
.service-col {	text-align: center;}
.service-col > img {	margin-bottom: 25px;}
.service-col > h3 {	color: #1c1b1b;font-size: 16px;	margin-bottom: 16px;}
.service-col > p {	color: #6e6f70;font-size: 16px;	line-height: 23px;}
.service-col:hover > img {
	-webkit-transform: scale(1.2);
	-moz-transform: scale(1.2);
	-ms-transform: scale(1.2);
	-o-transform: scale(1.2);
	transform: scale(1.2);
}

#home-page-banner .time{display:none;}
    
    
@media all and (max-width: 767px) {
    
    #popuplar-view-last-block .small_block.videoitem .v-img{height:90px;}
    
#owl-photo .owl-item {width: 150px !important;}
#blog .b-video {margin-bottom: 0px;}
.services-sec.upload-page .col-lg-3{width:48%; margin:1%; float:left;padding: 0 8px;}

#blog .owl-controls {margin-top: 10px !important;margin-bottom: 16px;}
#middle-stores-banner{margin-top:30px;}
#video-bl091 {padding: 0px 0px 20px 0px !important;}
#middle-stores-banner .left-blc:before{left:5px !important;}
#middle-stores-banner .left-blc:after{right:5px !important;}
#middle-stores-banner .left-blc img {width: 93% !important;margin: 45px auto  30px !important; display: block;}
#reward-block-ban{margin-top:30px !important;}
.videolist .videoitem .user img {object-fit: cover;width: 30px !important;height: 30px !important;}
#popuplar-view-last-block .cb-content.videolist .videoitem {width: 100% !Important;    margin-top: 8px;}
#popuplar-view-last-block .small_block.videoitem .v-img {width: 49.5%;margin-right: 10px;}
#popuplar-view-last-block .small_block.videoitem .b-video .v-desc {padding-top: 10px;}
#popuplar-view-last-block .time{display:none;}
#popuplar-view-last-block{padding-bottom:10px;}
footer {margin-bottom: 51px;}   
.left-blc img {width: 87%;margin: 42px auto 0;display: block;}
.left-blc{float:left;width:100%  !important;height: auto  !important;}
.right-blc {float: right;padding: 0px 0px 25px 0px  !important;width: 100% !important;background: #eaeaea !important;height: auto  !important;}
.right-blc p.reward-p{width: 100% !important;font-size: 24px !important;line-height: 28px !important;}
.right-blc p{width: 100% !important;}
.right-blc h2{font-size:16px !important;}

#btn-only-reward{margin: 20px 0 !Important;float: left !Important;max-width: 195px !Important;padding: 13px 10px !Important;}
.service-col > p {color: #6e6f70;font-size: 14px !important;line-height: 19px !important;} 
}
</style>


<style>

@media  only screen and (max-width: 767px) {
#home-page-banner .videolist{display:none}
#home-page-banner h2{ line-height: 35px;padding: 125px 10px 0 10px !important; font-size: 30px  !important;}
#home-page-banner p{padding:0 0px;font-size: 13px !important}
#home-page-banner p br{display:none}
#home-page-banner .u-area .u-btn {width: 48%;margin:9px 2px 0 2px !important;max-width: 48%;padding: 11px 0 0 0 !important; font-size: 14px; font-weight: normal; height: 40px;}
#home-page-banner{min-height:415px !Important}
.service-col {text-align: center;background: #fff;padding: 27px 0px 0px 0px;margin-bottom: 10px}
.service-col > h3 {font-size: 16px;margin: 12px 0 8px 0;height: 44px;line-height: 21px;}
.services-sec {padding: 17px 0 0;background: #fff;}
.services-sec.upload-page .u-area .u-btn{margin:25px 0 22px 0 !important}
.services-sec.upload-page {margin-bottom:10px;}
.services-sec i {font-size: 34px;}
}
</style>
 
@if(!empty(Session::get('user_id')))
              @if(empty(Session::get('profile_pic')))
                 @php $words = explode(' ', Session::get('user_name'));
                    echo '';
                @endphp 
      @php
            $nameLen = strlen(session()->get('user_name'));
              @endphp
            <!-- {{($nameLen > 6)?substr(session()->get('user_name'),0,6).'...':session()->get('user_name')}} -->
@else
 @endif
 @else
 <style>#top-b{padding-top:10px}</style>
 
 
<style>

#home-page-banner-content-new{position:absolute; top:20px; width:100%;}
#home-page-banner .container{position:relative; z-index:99;}
.video-new001{height: 750px; position:relative; background:url(https://entertainmentbugs.com/dev2022/public/img/top-banner.jpg) no-repeat; background-size:cover;}
.video-new001{overflow:hidden;}
@media  only screen and (max-width: 767px) {
#n12{display:none;}
#crousal_wrap {width: 100% !Important;float: none !important;margin: 0 auto;padding: 0 0 0 12px;}
#live-video-user-list-home .content-block .cb-header {padding: 11px 0 0px 0;}

#home-page-banner-content-new{position:absolute; top:0px; width:100%;}
.video-new001{height:430px; position:relative; background:url(https://entertainmentbugs.com/dev2022/public/img/top-banner.jpg) no-repeat center; background-size:cover;}
.video-new001 #mobile-set{height:430px !important;object-fit: cover;}
#no-need-in-res{margin-top:0px !Important;height:10px !Important;}
}
</style>
 
 
 

  <!--home page Video start-->
        <div class="upload-page video-new001" id="home-page-banner" style="">
              <video id="mobile-set" style="margin:0px;" autoplay="autoplay" loop muted >
                <source src="https://entertainmentbugs.com/dev_html/images/talent_01.mp4" type="video/mp4">
                <source src="http://entertainmentbugs.com/dev2022/public/img/ban-01.jpg" type="video/ogg">
                Your browser does not support the video tag.
             </video> 
          <div id="home-page-banner-content-new">
          
        <div class="container">

<h2 style="color: #fff;padding: 130px 0 0 0; font-size: 44px;    font-family: Montserrat,sans-serif;">Upload video and share with friends</h2>
 <p style="color:#fff;font-size: 16px;">Entertainment Bugs is one of the most entertainer video and stories platform, now retired as a fortune hunter,<br>has settled into a normal life with his wife Elena Fisher. </p>
<div class="u-area" id="promote_page_image">  
					 
							<a href="{{URL::to('login')}}" style="margin:20px 2px" class="btn btn-primary u-btn">Upload Video</a>
	<a href="{{URL::to('login')}}" style="margin:20px 2px" class="btn btn-primary u-btn">Write Stories</a>
					</div>
					 
<div class="cb-content videolist" id="home_page_video" style="height: 91px;overflow: hidden;"> 
                    <!-- Featured Videos -->
                    <div class="content-block"> 
                        <div class="cb-content videolist" id="home_page_video"> 
                             @if(!empty($videoData))
                                @foreach($videoData as $video)
                                    @include('elements.home.small_block_videos')		
                                    @if($loop->iteration % 4 == 0)
                                        <div style="clear:both; overflow:hidden"></div>
                                    @endif
                                @endforeach   
                            @endif 
                            <div style="clear:both; overflow:hidden"></div>  
                    <!-- /Featured Videos --> 
            </div> 
</div>
                  	</div>
                  
	</div>
 </div> 
     </div>
  <!--home page Video End-->
  
  
 
  <!--home page banner start-->
     <div class="upload-page" id="home-page-banner" style="display:none;background:url({{url('/public/img/ban-01.jpg')}}) no-repeat; width:100%;
min-height:510px;margin-top:0px; background-size:cover; background-position:center;">
<div class="container">

<h2 style="color: #fff;padding: 130px 0 0 0; font-size: 44px;    font-family: Montserrat,sans-serif;">Upload video and share with friends</h2>
 <p style="color:#fff;font-size: 16px;">Entertainment Bugs is one of the most entertainer video and stories platform, now retired as a fortune hunter,<br>has settled into a normal life with his wife Elena Fisher. </p>
<div class="u-area" id="promote_page_image">  
					 
							<a href="{{URL::to('login')}}" style="margin:30px 2px" class="btn btn-primary u-btn">Upload Video</a>
	<a href="{{URL::to('login')}}" style="margin:30px 2px" class="btn btn-primary u-btn">Write Stories</a>
					</div>
					 
<div class="cb-content videolist" id="home_page_video" style="height: 91px;overflow: hidden;"> 
                    <!-- Featured Videos -->
                    <div class="content-block"> 
                        <div class="cb-content videolist" id="home_page_video"> 
                             @if(!empty($videoData))
                                @foreach($videoData as $video)
                                    @include('elements.home.small_block_videos')		
                                    @if($loop->iteration % 4 == 0)
                                        <div style="clear:both; overflow:hidden"></div>
                                    @endif
                                @endforeach   
                            @endif 
                            <div style="clear:both; overflow:hidden"></div>  
                    <!-- /Featured Videos --> 
            </div> 
</div>
                  	</div>
                  
	</div>


  </div>
  <!--home page banner end-->


<div id="no-need-in-res" style="background:#eaeaea !important; width:100%; height:20px;margin-top: -180px;position: relative;z-index: 999;"></div>

  <section class="services-sec upload-page" style="background:#fff; position:relative;z-index: 999;">
            <div class="container">
               <div class="services-row">
                  <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        <div class="service-col">
                           <i class="fa fa-sign-in"></i>
                           <h3>Create your profile</h3>
                           <p>Accept money from your fans through tips.</p>
                        </div>
                        <!--service-col end-->
                     </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        <div class="service-col">
                           <i class="fa fa-file-video-o"></i>
                           <h3>Upload video and stories</h3>
                           <p>Join a growing community of young millennialaas &amp; get new fans. </p>
                        </div>
                        <!--service-col end-->
                     </div>
                     <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        <div class="service-col">
                           <i class="fa fa-slideshare"></i>
                           <h3>Promote your content</h3>
                           <p>Upload your videos from YouTube with an easy link copy/paste.</p>
                        </div>
                        <!--service-col end-->
                     </div>
                   <div class="col-lg-3 col-md-3 col-sm-3 col-12">
                        <div class="service-col">
                           <i class="fa fa fa-money"></i>
                           <h3>Make money with Enterteinment Bugs </h3>
                           <p>Extra income through Amazon Affiliates on your channel.</p>
                        </div>
                        <!--service-col end-->
                     </div>
                  </div>
                  <div class="u-area" id="promote_page_image">  
                     <a href="rewards.html" style="margin:30px 2px" class="btn btn-primary u-btn">Create Account</a>
                  </div>
               </div>
               <!--services-row end-->
            </div>
         </section>

@endif









    <div class="content-wrapper home_page_main_block" id="top-b">
        <div class="container">
            <div class="row home_page_first_block">
                
                <div class="col-lg-12">
                    <!-- Featured Videos -->
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="#" class="color-active">
                                                <span class="visible-xs">ALL Latest Videos </span>
                                                <span class="hidden-xs">ALL Latest Videos</span>
                                            </a>
                                        </li>
                                       
                                        
                                    </ul>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <div class="btn-group pull-right bg-clean">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            Sort by <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a onclick="showLatestPost('recent')"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                            <li><a onclick="showLatestPost('viewed')"><i class="cv cvicon-cv-view-stats"></i> Viewed</a></li>
                                            <li><a onclick="showLatestPost('top_liked')"><i class="cv cvicon-cv-star"></i> Top Liked</a></li>
                                            <li><a onclick="showLatestPost('longest')"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content videolist" id="home_page_video">

                            @if(!empty($videoData))
                                <div id="latestVideo"> 
                                     @include('elements.home_sorting_videos.all_latest_video')	
                                </div>

                            @endif
                          

                        </div>                            
                        <div style="clear:both; overflow:hidden"></div>
                    </div>
                    <!-- /Featured Videos -->
                </div>
            </div>




 
		 <!--<div class="row" style="background:#eaeaea !important;"> -->
		 <!--  <div class="container"> -->
		 <!--    <div class="row"> -->
			<!--  <div class="all-trailer">-->
			<!--	<div class="head-tr"><h2>New Featured Promo  & Trailers  </h2></div> -->
			<!--      <div class="all-trailer_inside">-->
			<!--	       <div class="span12" id="crousal_wrap" style="width:100%">		-->
			<!--			 <div id="trailer" class="owl-carousel" style="display:block">                -->
   <!--                         <div class="item"><a href=""><img src="https://streamo.vuejstemplate.com/images/product/movie-03.jpg"></a></div> -->
   <!--                         <div class="item"><a href=""><img src="https://streamo.vuejstemplate.com/images/product/movie-01.jpg"></a></div> -->
   <!--                         <div class="item"><a href=""><img src="https://streamo.vuejstemplate.com/images/product/movie-02.jpg"></a></div> -->
   <!--                         <div class="item"><a href=""><img src="https://streamo.vuejstemplate.com/images/product/movie-04.jpg"></a></div> -->
   <!--                         <div class="item"><a href=""><img src="https://streamo.vuejstemplate.com/images/product/movie-05.jpg"></a></div> -->
   <!--                         <div class="item"><a href=""><img src="https://streamo.vuejstemplate.com/images/product/movie-06.jpg"></a></div> -->
   <!--                         <div class="item"><a href=""><img src="https://streamo.vuejstemplate.com/images/product/movie-03.jpg"></a></div> -->
   <!--                         <div class="item"><a href=""><img src="https://streamo.vuejstemplate.com/images/product/movie-01.jpg"></a></div> -->
   <!--                         <div class="item"><a href=""><img src="https://streamo.vuejstemplate.com/images/product/movie-02.jpg"></a></div> -->
   <!--                         <div class="item"><a href=""><img src="https://streamo.vuejstemplate.com/images/product/movie-04.jpg"></a></div> -->
   <!--                         <div class="item"><a href=""><img src="https://streamo.vuejstemplate.com/images/product/movie-05.jpg"></a></div> -->
   <!--                         <div class="item"><a href=""><img src="https://streamo.vuejstemplate.com/images/product/movie-06.jpg"></a></div> -->
   <!--                     </div> -->
   <!--                  </div> -->
   <!-- 			   </div> -->
   <!-- 		     </div>-->
   <!-- 		  </div> -->
   <!-- 		 </div> -->
   <!--        </div>-->

            <div class="row home_page_ninth_block" id="home_page_first_photo" style="margin-bottom:10px"> 
                <div class="col-lg-12"> 
                    <!-- Popular Channels -->
                    <div class="content-block"> 
                        <div class="cb-header">
                            <div class="row"> 
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li><a href="#">Latest Photos</a></li>
                                    </ul>
                                </div> 
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <ul class="list-inline" style="    text-align: right">
                                        <li><a href="#" class="color-active">View All</a></li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content chanels-row" id="home_page_photos" style="margin-bottom:0px">
                            <div class="rows">
                         
                    <div class="span12" id="crousal_wrap" style="width:100%">		
								 <div id="owl-photo" class="owl-carousel" style="display:block">
					@foreach ($UploadPhotos as $UploadPhoto) 
                    @php
                      $image = unserialize($UploadPhoto->images)
                    @endphp                                                                                        
					   <div class="item"> 
					     <div class="photo-block">
        							<a href="#" class="user_info_mini"><i class="fa fa-thumbs-up"></i><span>{{$UploadPhoto->liked}}</span></a>
        							<a href="https://entertainmentbugs.com/dev2022/single-photo/{{$UploadPhoto->id}}"><img src="https://entertainmentbugs.com/dev2022/public/uploads/photo/{{$image[0]}}"></a>
        							<span style="position: absolute;width: 25px;bottom: 13px;right: 6px;z-index: 99999;">
        						         <i class="far fa-clone" style="font-size: 17px;color: #fff;text-shadow: 2px 1px 1px #000;"></i>
        							</span>
        						</div>	
        				</div>
                        @endforeach		
        					  
        				
        		<!--			   <div class="item"> -->
					     <!--<div class="photo-block">-->
        		<!--					<a href="#" class="user_info_mini"><i class="fa fa-thumbs-up"></i><span>296</span></a>-->
        		<!--					<a href="javaScript:void(0)"><img src=""></a>-->
        		<!--					<span style="position: absolute;width: 25px;bottom: 13px;right: 6px;z-index: 99999;">-->
        		<!--				         <i class="far fa-clone" style="font-size: 17px;color: #fff;text-shadow: 2px 1px 1px #000;"></i>-->
        		<!--					</span>-->
        		<!--				</div>	-->
        		<!--		</div>-->
				 
						     
						     
						     	</div>
						     	
						     	
						     		</div>
                            </div>
                        </div>
                    </div>
                    <!-- Photos --> 
                </div>

            </div>


<Style>
.photo-block a span {
    font-size: 9px;
    display: block;
}
    .photo-block {
    position: relative;
    width: 100%;
    margin: 0 0%;
    padding: 0 4px 8px 4px;
    float: left;
    height: 150px;
}
.photo-block img {
    width: 100%;
    height: 100%;
    object-fit: cover;
}
.photo-block a.user_info_mini {
    z-index: 99;
    background: #f72e5e;
    width: 37px;
    height: 37px;
    border-radius: 100px;
    position: absolute;
    top: 5px;
    color: #fff;
    text-align: center;
    font-size: 10px;
    line-height: 11px;
    padding: 7px 0 0 0;
    right: 10px;
}


.photo-block a, .photo-block a img{-webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;}
.photo-block a:hover img{ transform: scale(1.2);-webkit-transition: all 1s ease;
    -moz-transition: all 1s ease;
    -o-transition: all 1s ease;
    -ms-transition: all 1s ease;
    transition: all 1s ease;}
	
.photo-block a {
    overflow: hidden;
    display: block;
    width: 100%;
    height: 100%;
}</Style>

            <div class="row home_page_eight_block">
                <div class="col-lg-12">


                    <!-- Featured Videos -->
                    <div class="content-block head-div">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="rewards.html" class="color-active">
                                                <span class="visible-xs">Most Viewed </span>
                                                <span class="hidden-xs"> Most Viewed </span>
                                            </a>
                                        </li>                                         
                                        <li><a href="#">View All </a></li>

                                    </ul>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <div class="btn-group pull-right bg-clean">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            Sort by <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a onclick="showMostViewed('recent')"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                            <li><a onclick="showMostViewed('viewed')"><i class="cv cvicon-cv-view-stats"></i> Viewed</a></li>
                                            <li><a onclick="showMostViewed('top_liked')"><i class="cv cvicon-cv-star"></i> Top Liked</a></li>
                                            <li><a onclick="showMostViewed('longest')"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content videolist" id="home_page_video">
                            @if(!empty($mostViewedVideos))
                                <div id="mostViewedVideo">
                                     @include('elements.home_sorting_videos.most_viewed_video')
                                </div>
                            @endif

                            <div style="clear:both; overflow:hidden"></div>



                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>

            <div class="row home_page_first_block">
                <div class="col-lg-12">
                    <!-- Featured Videos -->
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-12">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="#" class="color-active">
                                                <span class="visible-xs">Trending Videos </span>
                                                <span class="hidden-xs">Trending Videos</span>
                                            </a>
                                        </li>
                                        
                                        <li><a href="#">Entertainment Videos</a></li>
                                        <li class="hidden-xs"><a href="#">View All</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <style>
                            #blog .col-lg-2 {
                                width: 100% !important
                            }

                        </style>

                        <div class="cb-content videolist" id="home_page_video">
                            <div id="blog" class="owl-carousel company-post">
                                @if(!empty($trendingVideos))
                                    @foreach($trendingVideos as $video)
                                        @include('elements.home.newvideo')							
                                    @endforeach   
                                @endif
                            </div>

                        </div>


                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>

<script>
    
    function showLatestPost(order_by){
        $.ajax({
            type: "GET",
            url: "{{ URL::to('show_latest_post_order_by') }}",
            data: {'keyword':order_by},
            cache: false,
            success: function(response)
            {
                $('#latestVideo').empty().html(response);
            }
        });
    }
    function showMostViewed(order_by){
        $.ajax({
            type: "GET",
            url: "{{ URL::to('show_most_viewed_order_by') }}",
            data: {'keyword':order_by},
            cache: false,
            success: function(response)
            {
                $('#mostViewedVideo').empty().html(response);
            }
        });
    }
    function showLatestStories(order_by){
        $.ajax({
            type: "GET",
            url: "{{ URL::to('show_latest_stories_order_by') }}",
            data: {'keyword':order_by},
            cache: false,
            success: function(response)
            {
                 $('#latestStories').empty().html(response);
            }
        });
    }
</script>



@if(!empty(Session::get('user_id')))
              @if(empty(Session::get('profile_pic')))
                 @php $words = explode(' ', Session::get('user_name'));
                    echo 's';
                @endphp 
      @php
            $nameLen = strlen(session()->get('user_name'));
              @endphp
            
@else
 @endif
 @else
  
 <div class="containers" id="middle-stores-banner" style="background: #eaeaea !important;">
        <div class="container cb-header">
            <div class="rows  upload-page"> 
                 <div class="left-blc" style="float:left">
                    <!--<img src="http://entertainmentbugs.com/dev2022/public/img/videos_banner.png" style="position:relative;z-index:99">-->
                 <video id="mobile-set" autoplay="autoplay" loop muted  width="100%" height="370" >
                                    <source src="https://entertainmentbugs.com/dev2022/public/img/home-video.mp4" type="video/mp4">
                                    <source src="https://entertainmentbugs.com/dev2022/public/img/photo_shoot.mp4" type="video/ogg">
                                    Your browser does not support the video tag.
                            </video>
                            
                            </div>
                
                <div class="right-blc" id="video-bl091">
                       <h2 style="text-align: left;color: #f72e5e;font-size: 20px;font-family: Montserrat,sans-serif;">Video</h2>
                    <p class="reward-p" style="font-size: 44px;margin: 0 0 0 0;color: #000;line-height: 46px;text-align:left;width: 95%;font-weight: 500;font-family: Montserrat,sans-serif;"> Upload video  to make you most trending.</p>
                     <p style="text-align: left;width: 97%;margin: 19px 0 0 0;">Entertainment Bugs was created to identify who id talented, but today it is being used to entertainment purpose, and help to makes people happy.</p>
                    <div class="u-area" id="promote_page_image"> 
                        <form action="#" method="post"><a href="https://entertainmentbugs.com/dev2022/rewards" style="margin:20px 0;float:left" class="btn btn-primary u-btn" id="btn-only-reward">Create Now</a></form>
                    </div>
                </div>
				  
             <div style="clear:both; overflow:hidden"></div>
				<style>
				#middle-stores-banner .left-blc:before {background: url(https://entertainmentbugs.com/dev2022/public/img/bubbles.png) top center/contain no-repeat;content: '';width: 132px;top: 38px;opacity: 0.3;height: 196px;z-index:9;left: -21px;position: absolute;}
				#middle-stores-banner .left-blc:after {background: url(https://entertainmentbugs.com/dev2022/public/img/bubbles.png) top center/contain no-repeat;content: '';width: 132px;bottom: 38px;height: 196px;z-index:9;right: -21px;opacity: 0.3;position: absolute;}
				#middle-stores-banner .left-blc{float:right;width:37%;height: 400px;margin-right: 2%;background: #eaeaea !important;position:relative;}
				#middle-stores-banner .left-blc img { width: 90%; margin:78px auto 0; display: block;}
				#middle-stores-banner .right-blc {float: left;padding: 50px 20px 20px 0px;width: 60%;background: #eaeaea !important;height: 400px;}

.left-blc::before {background: url(https://entertainmentbugs.com/dev2022/public/img/bubbles.png) top center/contain no-repeat;content: '';width: 132px;top: 38px;opacity: 0.3;height: 196px;z-index:9;left: 10px;position: absolute;}
.left-blc::after {background: url(https://entertainmentbugs.com/dev2022/public/img/bubbles.png) top center/contain no-repeat;content: '';width: 132px;bottom: 38px;height: 196px;z-index:9;right: 10px;opacity: 0.3;position: absolute;}
.left-blc img{position:relative; z-index:9;}
.left-blc{position:relative;}				
				</style>
            </div>
        </div>
    </div>
@endif



 


            <div class="row home_page_eight_block">
                <div class="col-lg-12">


                    <!-- Featured Videos -->
                    <div class="content-block head-div">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="rewards.html" class="color-active">
                                                <span class="visible-xs">Comedy Video </span>
                                                <span class="hidden-xs"> Comedy Video </span>
                                            </a>
                                        </li>                                         
                                        <li><a href="#">View All Video </a></li> 
                                    </ul>
                                </div>
                                 
                            </div>
                        </div>
                        <div class="cb-content videolist" id="home_page_video">

                             @if(!empty($comedyvideo))
                                @foreach($comedyvideo as $videos)
                                    @include('elements.home.popularvideos')
                                     @if($loop->iteration % 6 == 0)
                                        <div style="clear:both; overflow:hidden"></div>
                                    @endif
                                @endforeach   
                            @endif

                            <div style="clear:both; overflow:hidden"></div>



                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>




 
@if(!empty(Session::get('user_id')))
              @if(empty(Session::get('profile_pic')))
                 @php $words = explode(' ', Session::get('user_name'));
                    echo 's';
                @endphp 
      @php
            $nameLen = strlen(session()->get('user_name'));
              @endphp
            {{($nameLen > 6)?substr(session()->get('user_name'),0,6).'...':session()->get('user_name')}}
@else
 @endif
 @else
 
 <div class="containers" id="middle-stores-banner" style="background: #eaeaea !important;">
        <div class="container cb-header">
            <div class="rows  upload-page"> 
                 <div class="left-blc">
                    <img src="https://entertainmentbugs.com/dev2022/public/img/stories_banner.png" style="position:relative;z-index:99">
                </div>
                
                <div class="right-blc">
                       <h2 style="text-align: left;color: #f72e5e;font-size: 20px;font-family: Montserrat,sans-serif;">Stories</h2>
                    <p class="reward-p" style="font-size: 44px;margin: 0 0 0 0;color: #000;line-height: 46px;text-align:left;width: 95%;font-weight: 500;font-family: Montserrat,sans-serif;"> Upload stories to make you most trending</p>
                     <p style="text-align: left;width: 97%;margin: 19px 0 0 0;">Entertainment Bugs was created to identify who id talented, but today it is being used to entertainment purpose, and help to makes people happy.</p>
                    <div class="u-area" id="promote_page_image"> 
                        <form action="#" method="post"><a href="https://entertainmentbugs.com/dev2022/rewards" style="margin:20px 0;float:left" class="btn btn-primary u-btn" id="btn-only-reward">Create Now</a></form>
                    </div>
                </div>
				  
             <div style="clear:both; overflow:hidden"></div>
				<style>
				#middle-stores-banner .left-blc:before {background: url(https://entertainmentbugs.com/dev2022/public/img/bubbles.png) top center/contain no-repeat;content: '';width: 132px;top: 38px;opacity: 0.3;height: 196px;z-index:9;left: -21px;position: absolute;}
				#middle-stores-banner .left-blc:after {background: url(https://entertainmentbugs.com/dev2022/public/img/bubbles.png) top center/contain no-repeat;content: '';width: 132px;bottom: 38px;height: 196px;z-index:9;right: -21px;opacity: 0.3;position: absolute;}
				#middle-stores-banner .left-blc{float:right;width:45%;height: 400px;margin-right: 2%;background: #eaeaea !important;position:relative;}
				#middle-stores-banner .left-blc img { width: 90%; margin:78px auto 0; display: block;}
				#middle-stores-banner .right-blc {float: left;padding: 50px 20px 20px 0px;width: 53%;background: #eaeaea !important;height: 400px;}
#mobile-set{height:auto !Important;    margin-top: 55px;}
.left-blc::before {background: url(https://entertainmentbugs.com/dev2022/public/img/bubbles.png) top center/contain no-repeat;content: '';width: 132px;top: 38px;opacity: 0.3;height: 196px;z-index:9;left: 10px;position: absolute;}
.left-blc::after {background: url(https://entertainmentbugs.com/dev2022/public/img/bubbles.png) top center/contain no-repeat;content: '';width: 132px;bottom: 38px;height: 196px;z-index:9;right: 10px;opacity: 0.3;position: absolute;}
.left-blc img{position:relative; z-index:9;}
.left-blc{position:relative;}				
				</style>
            </div>
        </div>
    </div>
 
 @endif
 









            <div class="row home_page_third_block">
                <div class="col-lg-12"> 
                    <div class="content-block" style="border:0px">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="#" class="color-active">
                                                <span class="visible-xs">Latest Stories </span>
                                                <span class="hidden-xs">Latest Stories</span>
                                            </a>
                                        </li>
                                        <li><a href="#">View all  Stories</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <div class="btn-group pull-right bg-clean">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            Sort by <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a onclick="showLatestStories('recent')"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                            <li><a onclick="showLatestStories('viewed')"><i class="cv cvicon-cv-view-stats"></i> Viewed</a></li>
                                            <li><a onclick="showLatestStories('top_liked')"><i class="cv cvicon-cv-star"></i> Top Liked</a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="story_block" class="">
                        @if(!empty($stories))
                            <div id="latestStories">
                                @include('elements.home_sorting_videos.latest_stories')             
                            </div>
                        @endif
                        <div class="clear"></div>
                        <a target="_blank" href="{{ route('story.index') }}" class="view_all"> View All</a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>




            <!-- /New Videos in India -->



            <!--<div class="row home_page_third_block">-->
            <!--    <div class="col-lg-12"> -->
            <!--        <div class="content-block" style="border:0px">-->
            <!--            <div class="cb-header">-->
            <!--                <div class="row">-->
            <!--                    <div class="col-lg-10 col-sm-10 col-xs-8">-->
            <!--                        <ul class="list-inline">-->
            <!--                            <li>-->
            <!--                                <a href="#" class="color-active">-->
            <!--                                    <span class="visible-xs">Trending Stories </span>-->
            <!--                                    <span class="hidden-xs">Trending Stories</span>-->
            <!--                                </a>-->
            <!--                            </li>-->
            <!--                            <li><a href="#">View all  Stories</a></li>-->
            <!--                        </ul>-->
            <!--                    </div>-->
            <!--                    <div class="col-lg-2 col-sm-2 col-xs-4">-->
            <!--                        <div class="btn-group pull-right bg-clean">-->
            <!--                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"-->
            <!--                                aria-haspopup="true" aria-expanded="false">-->
            <!--                                Sort by <span class="caret"></span>-->
            <!--                            </button>-->
            <!--                            <ul class="dropdown-menu">-->
            <!--                                <li><a href="#"><i class="cv cvicon-cv-relevant"></i> Relevant</a></li>-->
            <!--                                <li><a href="#"><i class="cv cvicon-cv-calender"></i> Recent</a></li>-->
            <!--                                <li><a href="#"><i class="cv cvicon-cv-view-stats"></i> Viewed</a></li>-->
            <!--                                <li><a href="#"><i class="cv cvicon-cv-star"></i> Top Rated</a></li>-->
            <!--                                <li><a href="#"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>-->
            <!--                            </ul>-->
            <!--                        </div>-->
            <!--                        <div class="clearfix"></div>-->
            <!--                    </div>-->
            <!--                </div>-->
            <!--            </div>-->
            <!--        </div>-->

            <!--        <div id="story_block" class="">-->
            <!--            @if(!empty($stories))-->
            <!--                @foreach ($stories as $story)-->
            <!--                    @include('elements.home.story_card')							-->
            <!--                @endforeach   -->
            <!--            @endif-->
            <!--            <div class="clear"></div>-->
            <!--            <a target="_blank" href="{{ route('story.index') }}" class="view_all"> View All</a>-->
            <!--            <div class="clear"></div>-->
            <!--        </div>-->
            <!--    </div>-->
            <!--</div>-->




 



 
@if(!empty(Session::get('user_id')))
              @if(empty(Session::get('profile_pic')))
                 @php $words = explode(' ', Session::get('user_name'));
                    echo 's';
                @endphp 
      @php
            $nameLen = strlen(session()->get('user_name'));
              @endphp
            
@else
 @endif
 @else
 

<div class="containers" id="reward-block-ban" style="margin-top: 11px;margin-bottom: 11px;background: #eaeaea !important;">
        <div class="container cb-header">
            <div class="rows  upload-page">
                
                    <!--<img src="http://entertainmentbugs.com/dev2022/public/img/reward_banner_all.png">-->
                    <!--<img src="http://entertainmentbugs.com/dev2022/public/img/rewards.png">-->
          
                              <div class="left-blc" style="float:left"> 
                                  <video id="mobile-set" autoplay="autoplay" loop muted  width="100%" height="370" >
                                    <source src="https://entertainmentbugs.com/dev2022/public/img/rewards_video.mp4" type="video/mp4">
                                    <source src="https://entertainmentbugs.com/dev2022/public/img/rewards_video.mp4" type="video/ogg">
                                    Your browser does not support the video tag.
                            </video> 
                            </div>
                
                
                
                 <div class="right-blc">
                       <h2 style="text-align: left;color: #f72e5e;font-size: 20px;font-family: Montserrat,sans-serif;">Entertainer</h2>
                    <p class="reward-p" style="font-size: 44px;margin: 0 0 0 0;color: #000;line-height: 46px;text-align:left;width: 95%;font-weight: 500;font-family: Montserrat,sans-serif;"> Showcase your talent and earn reward
                    </p>
                     <p style="text-align: left;width: 97%;margin: 19px 0 0 0;">Entertainment Bugs was created to identify who id talented, but today it is being used to entertainment purpose, and help to makes people happy.</p>

                    <div class="u-area" id="promote_page_image"> 
                        <form action="#" method="post">
                            <a href="{{URL::to('rewards')}}" style="margin:20px 0;float:left" class="btn btn-primary u-btn" id="btn-only-reward">Participate Now</a>
                        </form>
                    </div>
                </div>
                <div style="clear:both; overflow:hidden"></div>
               <style>
                    .left-blc{float:left;width:45%;height: auto;    background: #eaeaea !important;}
                    .left-blc img { width: 82%; margin:42px auto 0; display: block;}
                    .right-blc {float: right;padding: 50px 20px 20px 45px;width: 54%;background: #eaeaea !important;height: 400px;}
                </style> 
            </div>
        </div>
    </div>
@endif
            <!-- /New Videos in India -->


  



            <div class="row home_page_first_blocks" id="popuplar-view-last-block" style="margin-bottom:10px">
                <div class="col-lg-12">
                    <!-- Featured Videos -->
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="#" class="color-active">
                                                <span class="visible-xs"> Latest Rewards Video </span>
                                                <span class="hidden-xs">  Latest Rewards Video</span>
                                            </a>
                                        </li> 

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content videolist" id="home_page_video">

                             @if(!empty($rewardvideoData))
                                @foreach($rewardvideoData as $video)
                                    @include('elements.home.small_block_videos')		
                                    @if($loop->iteration % 4 == 0)
                                        <div style="clear:both; overflow:hidden"></div>
                                    @endif
                                @endforeach   
                            @endif
 
                            <div style="clear:both; overflow:hidden"></div> 
                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>





            <div class="row home_page_first_blocks" id="popuplar-view-last-block" style="margin-bottom:10px">
                <div class="col-lg-12">
                    <!-- Featured Videos -->
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="#" class="color-active">
                                                <span class="visible-xs">Trending  Rewards Video </span>
                                                <span class="hidden-xs">Trending Rewards Video</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content videolist" id="home_page_video">

                             @if(!empty($trendingRewardVideos))
                                @foreach($trendingRewardVideos as $video)
                                    @include('elements.home.small_block_videos')		
                                    @if($loop->iteration % 4 == 0)
                                        <div style="clear:both; overflow:hidden"></div>
                                    @endif
                                @endforeach   
                            @endif
 
                            <div style="clear:both; overflow:hidden"></div> 
                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>
 


@php 
	$rewardVideos =  App\Models\Video::where(['isRewardVideo' => 1])
					->select('id','video_key','title','video_thumb_id','video_time', 'user_id', 'created_at')
				 	->orderBy('total_views_count','desc')
					->with(['thumb' => function($thumb){
						$thumb->select('id','thumb');
					}])
					->with(['user' => function($user){
						$user->select('id','name', 'photo','slug');
					}])
					
					->with(['totalViews' => function($query){
						$query->select('video_id')
						->groupBy('video_id', 'video_id')
						->orderBY(DB::raw('count(video_id)'), 'DESC');
						 
					}])
 					->withCount(['totalViews'])
					->with('watchTime')
					->paginate(10);


@endphp 

<style>
.channels-card-image img {object-fit: cover;}
.reward-all-dis{width:85px;float:left;margin-right:7px;}
.reward-all-dis img{width:100%;margin: 4px 0 4px 0;}
.reward-all-dis.right-rew img{border-radius:100px;object-fit: cover;margin:0 auto;   display:block;transition: 0.3s; width:100%; height:100%;}
.reward-all-dis h4{text-align:center; font-size:14px;margin-top:8px;}
.reward-all-dis p{color: #7e7e7e;text-align: center;font-size: 12px;margin-top: 7px;line-height: 14px;}
.reward-all-dis.right-rew{position:relative;margin:0px;width:95px;}
.reward-all-dis.right-rew strong{width: 4px;height: 2px;display: inline-block;}
.reward-all-dis.right-rew span {position: absolute;top: -8px;right: -1px;background: #f72e5e;color: #fff;display: block;width: 35px;text-align: center;line-height: 10px;height: 35px;border-radius: 100px;font-size: 10px;padding: 10px 0 0 0;}
.zoom_img1 {width:60px; height:60px; border: solid 2px #f72e5e; transition: 0.3s;margin:0 auto;padding:2px; overflow:hidden;border-radius:100px; }
.zoom_img1:hover img {transition: 0.3s;}
.zoom_img1:hover {padding:0px; transition: 0.3s;border: solid 2px #28b47e;}
.reward-all-dis.right-rew a:hover span{background:#28b47e !important;transition: 0.3s;}
 .channel .c-details {border-bottom: solid 0px #eceff0;}
</style>

            <div class="row home_page_ninth_block" id="popular_cat" style="margin-bottom:10px">
                <div class="col-lg-12">
                     <div class="reward_container" style="background:#fafafa !important">
                         <div class="col-xs-12 col-sm-12" id="about-new-01" style="border-top:solid 0px #eceff0;padding-top: 28px;">
                                     <div class="reward-all-dis"> 
                                          <img src="https://entertainmentbugs.com/dev2022/public/img/rewards.png">
                                          <h4>ENTERTAINER <br> REWARDS</h4>
                                     </div>
                                     @if(!empty($rewardVideos))
                                        @foreach($rewardVideos as $video)
                                        	@php
                                        		$videoUrl = $video->video_key;
                                        	@endphp
                                        	
                                        	@if(!empty($video->thumb->thumb))
                        				
                                			@php
                                				$thumbBastPath = VIDEO_THUMB_UPLOAD_PATH.$video->thumb->thumb;
                                				$thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$video->thumb->thumb;
                                			@endphp
                                			@endif
                                         <div class="reward-all-dis right-rew"> 
                                            <a href="{{ URL::to('/'.$video->user->slug) }}"> 
                                            <div class="zoom_img1">
                                                <img src="{{ asset('public/uploads/users/small/'.$video->user->photo) }}">
                                            </div>
                                            <span>Rank <br style="display:block !important"> {{ $loop->index + 1 }}  </span>
                                             <p>214k  <i class="fa fa-eye"></i>  <strong></strong> 214k <i class="fa fa-thumbs-up"></i>  <br style="display:block !important"> 22nd June </p></a>
                                         </div>
                                         @endforeach
                                     @endif 
                                 </div>       
                              </div>   
                            </div> 
                        </div>










<!----- popular categories HIDE


            <div class="row home_page_ninth_block" id="popular_cat" style="margin-bottom:10px">
                <div class="col-lg-12">
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li><a href="#">Popular Categories</a></li>
                                    </ul>
                                </div> 
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <div class="btn-group pull-right bg-clean">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            More <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#"><i class="cv cvicon-cv-relevant"></i> Relevant</a></li>
                                            <li><a href="#"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                            <li><a href="#"><i class="cv cvicon-cv-view-stats"></i> Viewed</a></li>
                                            <li><a href="#"><i class="cv cvicon-cv-star"></i> Top Rated</a></li>
                                            <li><a href="#"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content chanels-row" id="home_page_categories" style="margin-bottom:0px">
                            <div class="row">
                                 @if(!empty($categories))
                                    @foreach($categories as $category)
                                        @include('elements.home.popular_categories')		
                                        @if($loop->iteration % 7 == 0)
                                            <div style="clear:both; overflow:hidden"></div>
                                        @endif
                                    @endforeach   
                                @endif 
                            </div>
                        </div>
                    </div> 
                </div>
            </div>

popular categories HIDE -------------------------------->












 <div class="row home_page_seventh_block" style="margin-bottom:10px;">

                <div class="col-lg-12">
                    <!-- Popular Channels -->
                    <div class="content-block">

                        <div class="content-block" style="border:0px">
                            <div class="cb-header">
                                <div class="row">
                                    <div class="col-lg-10 col-sm-10 col-xs-8">
                                        <ul class="list-inline">
                                            <li>
                                                <a href="#" class="color-active">
                                                    <span class="visible-xs">Popular Channel</span>
                                                    <span class="hidden-xs">Most Followers</span>
                                                </a>
                                            </li>
                                            <li><a href="#">Most Viewed</a></li>
                                        </ul>
                                    </div> 
                                </div>
                            </div>


                        </div>


                        <div class="cb-content chanels-row" id="home_page_channel">
                            <div class="row">
                                
                                 @if(!empty($mostFollowers))
                                    @foreach ($mostFollowers as $user)
                                        @include('elements.home.most_followed_user')							
                                    @endforeach   
                                @endif

                            </div>



                        </div>
                    </div>
                    <!-- /Popular Channels -->
                </div>
            </div>


 



  <!--<div class="cb-content videolist" id="home_page_video">
                             
                            @if (!empty($rewardVideos))
                                @foreach ($rewardVideos as $video)
                                    @include('elements.reward.reward')
                                    @if ($loop->iteration % 6 == 0)
                                        <div style="clear:both; overflow:hidden"></div>
                                    @endif
                                @endforeach
                                {{ $rewardVideos->links() }}
                            @endif
                        </div>
 -->










            <div class="row home_page_second_block">
                <div class="col-lg-12">


                    <!-- Featured Videos -->
                    <div class="content-block head-div">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="#" class="color-active">
                                                <span class="visible-xs">Gaming </span>
                                                <span class="hidden-xs">Gaming</span>
                                            </a>
                                        </li>
                                        <li><a href="#">View All Gaming</a></li> 
                                    </ul>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <div class="btn-group pull-right bg-clean">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            Sort by <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#"><i class="cv cvicon-cv-relevant"></i> Relevant</a></li>
                                            <li><a href="#"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                            <li><a href="#"><i class="cv cvicon-cv-view-stats"></i> Viewed</a></li>
                                            <li><a href="#"><i class="cv cvicon-cv-star"></i> Top Rated</a></li>
                                            <li><a href="#"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content videolist" id="home_page_video">

                            @if(!empty($gamevideo))
                                @foreach($gamevideo as $video)
                                    @include('elements.home.newserials')
                                     @if($loop->iteration % 6 == 0)
                                        <div style="clear:both; overflow:hidden"></div>
                                    @endif
                                @endforeach   
                            @endif

                            <div style="clear:both; overflow:hidden"></div>



                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>





            <div class="row home_page_eight_block" style="margin-bottom:10px">
                <div class="col-lg-12">


                    <!-- Featured Videos -->
                    <div class="content-block head-div">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="rewards.html" class="color-active">
                                                <span class="visible-xs">Music </span>
                                                <span class="hidden-xs"> Music </span>
                                            </a>
                                        </li>                                         
                                        <li><a href="#">View All Video </a></li>

                                    </ul>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-xs-4">
                                    <div class="btn-group pull-right bg-clean">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                            Sort by <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#"><i class="cv cvicon-cv-relevant"></i> Relevant</a></li>
                                            <li><a href="#"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                            <li><a href="#"><i class="cv cvicon-cv-view-stats"></i> Viewed</a></li>
                                            <li><a href="#"><i class="cv cvicon-cv-star"></i> Top Rated</a></li>
                                            <li><a href="#"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content videolist" id="home_page_video">

                             @if(!empty($musicvideo))
                                @foreach($musicvideo as $videos)
                                    @include('elements.home.popularvideos')
                                     @if($loop->iteration % 6 == 0)
                                        <div style="clear:both; overflow:hidden"></div>
                                    @endif
                                @endforeach   
                            @endif

                            <div style="clear:both; overflow:hidden"></div>



                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>



 


 





     <div class="row home_page_eight_block" style="margin-bottom:10px">
                <div class="col-lg-12">


                    <!-- Featured Videos -->
                    <div class="content-block head-div">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="rewards.html" class="color-active">
                                                <span class="visible-xs">Entertainment  </span>
                                                <span class="hidden-xs"> Entertainment  </span>
                                            </a>
                                        </li>                                         
                                        <li><a href="#">View All Video </a></li>

                                    </ul>
                                </div> 
                            </div>
                        </div>
                        <div class="cb-content videolist" id="home_page_video">

                           
                             @if(!empty($entertainmentvideo))
                                @foreach($entertainmentvideo as $videos)
                                    @include('elements.home.popularvideos')
                                     @if($loop->iteration % 6 == 0)
                                        <div style="clear:both; overflow:hidden"></div>
                                    @endif
                                @endforeach   
                            @endif

                            <div style="clear:both; overflow:hidden"></div>



                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>
            
            

            <div class="row home_page_first_blocks" id="popuplar-view-last-block" style="margin-bottom:10px">
                <div class="col-lg-12">
                    <!-- Featured Videos -->
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="#" class="color-active">
                                                <span class="visible-xs">Followers Latest video </span>
                                                <span class="hidden-xs">Followers Latest video</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content videolist" id="home_page_video">

                             @if(!empty($latestFollowerVideo))
                                @foreach($latestFollowerVideo as $video)
                                    @include('elements.home.small_block_videos')		
                                    @if($loop->iteration % 4 == 0)
                                        <div style="clear:both; overflow:hidden"></div>
                                    @endif
                                @endforeach   
                            @endif
 
                            <div style="clear:both; overflow:hidden"></div> 
                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>
 
            
            

            <div class="row home_page_first_blocks" id="popuplar-view-last-block" style="margin-bottom:10px;">
                <div class="col-lg-12">
                    <!-- Featured Videos -->
                    <div class="content-block">
                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">
                                        <li>
                                            <a href="#" class="color-active">
                                                <span class="visible-xs">Trending  Video </span>
                                                <span class="hidden-xs">Trending Video</span>
                                            </a>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="cb-content videolist" id="home_page_video">

                             @if(!empty($trendingRewardVideos))
                                @foreach($trendingRewardVideos as $video)
                                    @include('elements.home.small_block_videos')		
                                    @if($loop->iteration % 4 == 0)
                                        <div style="clear:both; overflow:hidden"></div>
                                    @endif
                                @endforeach   
                            @endif
 
                            <div style="clear:both; overflow:hidden"></div> 
                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>
 


@if(!empty(Session::get('user_id')))
              @if(empty(Session::get('profile_pic')))
                 @php $words = explode(' ', Session::get('user_name'));
                    echo 's';
                @endphp 
      @php
            $nameLen = strlen(session()->get('user_name'));
              @endphp
            {{($nameLen > 6)?substr(session()->get('user_name'),0,6).'...':session()->get('user_name')}}
@else

<div class="containers" style="margin-top: -11px;margin-bottom: -11px;background: #eaeaea !important;">
        <div class="container cb-header">
            <div class="rows  upload-page">
                
                    <!--<img src="http://entertainmentbugs.com/dev2022/public/img/reward_banner_all.png">-->
                     
                              <div class="left-blc" style="float:left"> 
                                  <video id="mobile-set" autoplay="autoplay" loop muted  width="100%" height="370" >
                                    <source src="https://entertainmentbugs.com/dev2022/public/img/rewards_video.mp4" type="video/mp4">
                                    <source src="https://entertainmentbugs.com/dev2022/public/img/rewards_video.mp4" type="video/ogg">
                                    Your browser does not support the video tag.
                            </video> 
                            </div>
                 <div class="right-blc">
                       <h2 style="text-align: left;color: #f72e5e;font-size: 20px;font-family: Montserrat,sans-serif;">Entertainer</h2>
                    <p class="reward-p" style="font-size: 44px;margin: 0 0 0 0;color: #000;line-height: 46px;text-align:left;width: 95%;font-weight: 500;font-family: Montserrat,sans-serif;"> Showcase your talent and earn reward
                    </p>
                     <p style="text-align: left;width: 97%;margin: 19px 0 0 0;">Entertainment Bugs was created to identify who id talented, but today it is being used to entertainment purpose, and help to makes people happy.</p>

                    <div class="u-area" id="promote_page_image"> 
                        <form action="#" method="post">
                            <a href="{{URL::to('rewards')}}" style="margin:20px 0;float:left" class="btn btn-primary u-btn" id="btn-only-reward">Participate Now</a>
                        </form>
                    </div>
                </div>
                <div style="clear:both; overflow:hidden"></div>
               <style>
               #mobile-set {height: auto !Important;margin-top: 55px;}
                    .left-blc{float:left;width:45%;height: auto;    background: #eaeaea !important;}
                    .left-blc img { width: 82%; margin:42px auto 0; display: block;}
                    .right-blc {float: right;padding: 50px 20px 20px 45px;width: 54%;background: #eaeaea !important;height: 400px;}
                </style> 
            </div>
        </div>
    </div>
    
 @endif
 @else
 

@endif










        </div>
    </div>
 
 
 
    <style>
    #owl-photo .item{ 
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        text-align: center;
    }

    </style>
    
	<script>
    $(document).ready(function() {

      var owl = $("#trailer");

      owl.owlCarousel({

      items : 6, //10 items above 1000px browser width
      itemsDesktop : [1000,6], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,6], // 3 items betweem 900px and 601px
      itemsTablet: [600,4], //2 items between 600 and 0;
      itemsMobile :[600,2], // itemsMobile disabled - inherit from itemsTablet option
      }); 
 
  }); 
    </script>	
      
    
    
    
    <script>
    $(document).ready(function() {

      var owl = $("#owl-photo");

      owl.owlCarousel({

      items : 7, //10 items above 1000px browser width
      itemsDesktop : [1000,7], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,6], // 3 items betweem 900px and 601px
      itemsTablet: [600,4], //2 items between 600 and 0;
      itemsMobile :[600,3], // itemsMobile disabled - inherit from itemsTablet option
      
      });

      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      });
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      });
      $(".play").click(function(){
        owl.trigger('owl.play',1000);
      });
      $(".stop").click(function(){
        owl.trigger('owl.stop');
      });
    
    });
    </script>
 
@endsection
