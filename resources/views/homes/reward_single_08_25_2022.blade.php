@extends('layouts.home')
@section('content')
@php
    use Illuminate\Support\Facades\Route;
	$thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$videoData->thumb->thumb;
	$videoUrlPath = VIDEO_DISPLAY_PATH.$videoData->video;
	if(!$loggedInUser){
        session(['url.intended' => str_replace('{key}', $videoData->video_key, Route::getFacadeRoot()->current()->uri())]);
        $isFollowing = false;
    } else{
        $isFollowing = $activeUser->isFollowing($videoData->user->id);
    }
    
    $userBasePath = '';
    $userUrlPath = '';
    if(isset($videoData->user->photo)){
        $userBasePath = PROFILE_SMALL_UPLOAD_PATH.$videoData->user->photo;
        $userUrlPath = PROFILE_SMALL_DISPLAY_PATH.$videoData->user->photo;
    }
    
    $curuserBastPath = '';
    $curuserUrlPath = '';

    if(!empty($user->photo)){
        $curuserBastPath = PROFILE_SMALL_UPLOAD_PATH.$user->photo;
        $curuserUrlPath = PROFILE_SMALL_DISPLAY_PATH.$user->photo;
        $userID = $user->id;
    }
    
	
@endphp
<style>
.single-video .adblock{padding-top:0px;}
video{    height: 400px !important;  }
#only-mobile-video-details{display:none;}

 @media  only screen and (max-width: 767px) {
     #no-mb091{display:none;}
#only-mobile-video-details{display:block !important;padding: 7px 0 0 0;}
.single-video.light .col-lg-4.col-xs-12.col-sm-12.hidden-xs{display:block! important;    margin: 26px 0 0 0;}
.single-video.light .col-lg-4.col-xs-12.col-sm-12.hidden-xs .col-lg-5.col-sm-5{width:50%; float:left;}
.single-video.light .col-lg-4.col-xs-12.col-sm-12.hidden-xs .col-lg-7.col-sm-7{width:50%; float:left;padding: 0;}
#ght{display:none;}

#only-mobile-video-details{display:block !important;} .sv-video img.sv-avatar {width: 50px;border-radius: 100px;height: 50px;object-fit: cover;}
#only-mobile-video-details .sv-name{margin: 5px 0 0 0; padding: 0 0 0 0;}
#only-mobile-video-details h1 {font-size: 16px !important;padding: 1px 0 0 0;} 
#only-mobile-video-details a { font-size: 14px !important;}
.img-pi{   margin-right: 5px; }
#only-mobile-video-details #video_up_preview {border-top: solid 1px #eceff0;padding: 15px 0 0;    margin-top: 8px}     
#only-mobile-video-details #video_up_preview .form-group label{font-size:12px;}
#only-mobile-video-details #video_up_preview span{font-size:12px;}
#only-mobile-video-details .form-group  a{font-size: 12px !important;    margin-right: 0px;}
#only-mobile-video-details .sv-tags small {padding: 2px 10px 0 10px;}
#only-mobile-video-details .form-group {margin-bottom: 6px;}
.single-video .h-video {margin-bottom: 15px;}    
}

</style>
<div class="single-video light content-wrapper">
   <div class="container">
       <div class="row">
           <div class="col-lg-8 col-xs-12 col-sm-12"> 
				<div class="icon_small">
<div class="icons_b first1"><a href="#" class="user_imgs">MORE VIDEOS</a>	</div>
               @if($moreVideoss)
                    @foreach ($moreVideoss as $video)
                        @php
                              $vvideoUrl = $video->video_key;
                              $videothumb = $video->thumb->thumb;
                        @endphp
                        @if(!empty($videothumb))
                    
                            @php
                                $vthumbBastPath = $video->thumb->thumb;
                            @endphp
                        @endif
                        @if(!empty($videothumb))
                            <div class="icons_b first2"><a href="{{URL::to('watch-reward/'.$vvideoUrl)}}" class="user_img"><img src="{{ asset('public/uploads/video/thumbs/'.$vthumbBastPath) }}" /></a> <span>{{ $video->title }}</span> </div>
                        @else
                            <div class="icons_b first2">
   		                        	<a href="{{URL::to('watch-reward/'.$videoUrl)}}">{{HTML::image('public/img/video1-1.png')}}</a>	
                                <span>{{ $video->title }}</span> 
                            </div>	
                        @endif

                    @endforeach
                @endif

				</div>   
                
               <div class="sv-video right_big_video"> 
					 <img src="http://entertainmentbugs.com/dev2020/public/img/logo_footer.png" style="position:absolute; right:10px; top:5px; width:35px">
                   <video poster="{{$thumbUrlPath}}" id="mainVideo" controls style="width:100%;height:auto;">
                      
                       <source src="{{$videoUrlPath}}" type="video/mp4"> </source>
                   </video>
             
               <h1 id="no-mb091"> {{$videoData->title}} </h1>

<div id="only-mobile-video-details">
      <div class="author-head">
                                           <a class="img-pi" href="{{URL::to('userprofile/'.$videoData->user->name)}}">
                                                @if(file_exists($userBasePath))
                                                        {{HTML::image($userUrlPath,null,['class'=>'sv-avatar'])}}
                                                @else
                                                    {{HTML::image('public/img/ava2.png',null,['class'=>'sv-avatar'])}}
                                                @endif
                                            </a>
                                           <div class="sv-name">
                                               <!--<div><a target="_blank" href="{{URL::to('userprofile/'.$videoData->user->name)}}">{{$videoData->user->name}}</a>  </div>-->
 <h1> {{$videoData->title}} </h1>  
 <a style="color:gray" href="{{URL::to('userprofile/'.$videoData->user->name)}}">{{$videoData->user->name}}</a> - 
 <span style="color:gray">{{$videoData->total_views_count}} views</span>
 - <span style="color:gray">{{$videoData->timing}}</span>
 
                                           </div>
                                       </div>
    
    
    
    
    <div class="clear"></div>
    
       
                                   <div id="video_up_preview">
                                       <div class="col-lg-12" id="">
                                           <div class="form-group">
                                               <label for="e2">About:</label>
                                               <span>{{$videoData->description}}</span>
                                           </div>
                                       </div>
                                       <div class="col-lg-12" id=" ">
                                           <div class="form-group">
                                               <label for="e1">Video Tags:</label>
                                               <p class="sv-tags" style="margin-bottom: 5px;">
												@if(!empty($videoTags))
													@foreach($videoTags as $slug => $tag)
													  <form action="{{URL::to('search')}}" method="post">
                                                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                           <input type="hidden" class="form-control" name="searchdata" aria-describedby="sizing-addon2" value="{{ $tag }}">
                                                           <input type="hidden" name="searchType" id="searchType" value="video"/>
                                                           <input type="submit" value="{{ $tag }}">
                                                       </form>
													@endforeach
                                                @endif 
                                               </p>
                                           </div>
                                       </div>
                                       <div class="col-lg-12">
                                           <div class="form-group">
                                               <label for="e3">Language:</label>
                                               <span>{{$videoData->language}}</span>
                                           </div>
                                       </div>
                                       <div class="col-lg-12">
                                           <div class="form-group">
                                               <label for="e3">Category:</label>
											   @if(!empty($videoCategories))
												   <span>
												   @php $tc = count($videoCategories); $t = 0; @endphp
												   @foreach($videoCategories as $slug => $category)
													<a href="{{URL::to('category/'.$slug)}}">{{$category}}</a>@php $t++; echo (($tc != $t)?',&nbsp;':""); @endphp
												   @endforeach
												   </span>
											   @endif
                                           </div>
                                       </div>
                                       <div class="col-lg-12">
                                           <div class="form-group">
                                               <label for="e3">Release Date:</label>
                                               <span>2 Days ago</span>
                                           </div>
                                       </div>
                                   </div>
                                   
    
</div>
               
               
               
               
                 </div>
                 <div class="clear"></div>
               <div class="info">
                   <div class="custom-tabs">
                       <div class="tabs-panel hidden-xs">
                           
                           <a href="#" data-tab="tab-2" class="active">
                               <i class="cv cvicon-cv-share" data-toggle="tooltip" data-placement="top" title="Share"></i>
                               <span>Share</span>
                           </a>
                           <a href="#"  data-tab="tab-1">
                               <i class="cv cvicon-cv-about" data-toggle="tooltip" data-placement="top" title="About"></i>
                               <span>About</span>
                           </a>
                           <div class="acide-panel hidden-xs">
                            <a href="{{$videoData->video_key}}" style="font-size:15px;line-height:25px;" id="watchlaterCurrentvideo"><i class="cv cvicon-cv-watch-later" data-toggle="tooltip" data-placement="top" title="" data-original-title="Watch Later"></i><span id="watchLaterCount">{{ $videoWatchLaterCount }}</span></a>
                            <a href="{{$videoData->video_key}}" style="font-size:15px;line-height:25px;" id="likevideo"><i class="fa fa-thumbs-up" data-toggle="tooltip" data-placement="top" title="" style="margin-right: 5px;" data-original-title="Liked"></i> <span id="likedCount">{{ $videoLikeCount }}</span></a>
                             <a href="{{$videoData->video_key}}" style="font-size:15px;line-height:25px;" id="dislikevideo"><i class="fa fa-thumbs-down" data-toggle="tooltip" data-placement="top" title="" style="margin-right: 5px;" data-original-title="Unlinked"></i><span id="unLikedCount">{{ $videoDisLikeCount }}</span></a>
                            <a data-toggle="modal" href="#myModal" style="font-size:15px;line-height:25px;" id="videoMarkflag"><i class="cv cvicon-cv-flag" data-toggle="tooltip" data-placement="top" title="" data-original-title="Flag"></i><span id="videoFlag">{{ $videoFlagCount }}</span></a> 
                            
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-dialog">
                                
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Comments</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group">
                                              <label for="exampleFormControlTextarea1">Write Video Flag Comments:</label>
                                              <input type="hidden" id="flagKey" value="{{$videoData->video_key}}"/>
                                              <textarea class="form-control" id="flagcomments" rows="3"></textarea>
                                            </div>
                                          </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" id="flagcommentbtn" class="btn btn-success">Submit</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                
                                </div>
                            </div>
                        </div>
                       </div>
                       <div class="acide-panel acide-panel-top">
                           <a href="#"><i class="cv cvicon-cv-liked" data-toggle="tooltip" data-placement="top" title="Liked"></i></a>
                           <a href="#"><i class="cv cvicon-cv-watch-later" data-toggle="tooltip" data-placement="top" title="Watch Later"></i></a>
                           <a href="#"><i class="cv cvicon-cv-flag" data-toggle="tooltip" data-placement="top" title="Flag"></i></a>
                       </div>
                       <div class="tabs-panel only_mobile">
                           <a href="#" data-tab="tab-2">
                               <i class="cv cvicon-cv-share" data-toggle="tooltip" data-placement="top" title="Share"></i>
                               <span>Share</span>
                           </a>
                       </div>
                       <div class="clearfix"></div>
                       <!-- BEGIN tabs-content -->
                       <div class="tabs-content">
                           <!-- BEGIN tab-1 -->
                           <div class="tab-1">
                               <div class="col-xs-12" style="width:100%">
                                   <div class="author" style="box-shadow: none;border: 0px;padding: 20px 0 10px 0px;margin: 0;">
                                       <div class="author-head">
                                           <a  class="atr" target="_blank" href="{{URL::to('userprofile/'.$videoData->user->name)}}">
                                                @if(file_exists($userBasePath))
                                                        {{HTML::image($userUrlPath,null,['class'=>'sv-avatar'])}}
                                                @else
                                                    {{HTML::image('public/img/ava2.png',null,['class'=>'sv-avatar'])}}
                                                @endif
                                            </a>
                                           <div class="sv-name">
                                               <div><a target="_blank" href="{{URL::to('userprofile/'.$videoData->user->name)}}">{{$videoData->user->name}}</a> . {{ $userVideoCount }} Videos</div>
                                               <div class="c-sub hidden-xs" id="followunfollowbtn">
                                                   
                                               </div>
                                           </div>
                                       </div>
                                       <div class="author-border"></div>
                                       <div class="sv-views">
                                           <div class="sv-views-count">
										   {{$videoData->total_views_count}} views
                                           </div>
                                           <div class="sv-views-progress">
                                               <div class="sv-views-progress-bar"></div>
                                           </div>
                                       </div>
                                       <div class="clearfix"></div>
                                   </div>
                                   <div id="video_up_preview">
                                       <div class="col-lg-12" id="about_mbl">
                                           <div class="form-group">
                                               <label for="e2">About:</label>
                                               <span>{{$videoData->description}}</span>
                                           </div>
                                       </div>
                                       <div class="col-lg-12" id="about_mbl">
                                           <div class="form-group">
                                               <label for="e1">Video Tags:</label>
                                               <p class="sv-tags">
												@if(!empty($videoTags))
													@foreach($videoTags as $slug => $tag)
														  <form action="{{URL::to('search')}}" method="post">
                                                           <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                           <input type="hidden" class="form-control" name="searchdata" aria-describedby="sizing-addon2" value="{{ $tag }}">
                                                           <input type="hidden" name="searchType" id="searchType" value="video"/>
                                                           <input type="submit" value="{{ $tag }}">
                                                       </form>
													@endforeach
                                                @endif 
                                               </p>
                                           </div>
                                       </div>
                                       <div class="col-lg-12">
                                           <div class="form-group">
                                               <label for="e3">Language:</label>
                                               <span>{{$videoData->language}}</span>
                                           </div>
                                       </div>
                                       <div class="col-lg-12">
                                           <div class="form-group">
                                               <label for="e3">Category:</label>
											   @if(!empty($videoCategories))
												   <span>
												   @php $tc = count($videoCategories); $t = 0; @endphp
												   @foreach($videoCategories as $slug => $category)
													<a href="{{URL::to('/'.$slug)}}">{{$category}}</a>@php $t++; echo (($tc != $t)?',&nbsp;':""); @endphp
												   @endforeach
												   </span>
											   @endif
                                           </div>
                                       </div>
                                       <div class="col-lg-12">
                                           <div class="form-group">
                                               <label for="e3">Release Date:</label>
                                               <span>2 Days ago</span>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <!-- END tab-1 -->
                           <!-- BEGIN tab-2 -->
                           <div class="tab-2">
                               <h4>Share:</h4>
                               <div class="social">
                                    <div class="addthis_inline_share_toolbox_n0k8"></div>
                               </div>
                               <div class="row">
                                   <div class="col-md-9">
                                       <h4>Link:</h4>
                                       <label class="clipboard">
                                          <input type="text" id="btn-copy-txt" name="#" class="share-link" value="https://entertainmentbugs.com/DwGgdfe-C6c" readonly>
                                           <div class="btn-copy" data-clipboard-target=".share-link" onclick="copyText()">Copy</div>
                                       </label>
                                   </div>
                               </div>
                               <div class="tab-popup popup-share">
                                   <div class="tab-popup-head">
                                       <i class="cv cvicon-cv-share"></i>
                                       <span>Share this video</span>
                                       <a href="#" class="tab-popup-close"><i class="cv cvicon-cv-cancel"></i></a>
                                   </div>
                                   <div class="tab-popup-content">
                                       <h4>Copy Link:</h4>
                                       <label class="clipboard">
                                           <input type="text" name="#" class="share-link" value="https://entertainmentbugs.com./DwGgdfe-C6c" readonly>
                                           <div class="btn-copy" data-clipboard-target=".share-link">Copy</div>
                                       </label>
                                   </div>
                                   <div class="tab-popup-content">
                                       <div class="popup-share-social">
                                           <a href="#" class="facebook">
                                               <i class="fa fa-facebook" aria-hidden="true"></i>
                                               <span>Facebook</span>
                                           </a>
                                           <a href="#" class="twitter">
                                               <i class="fa fa fa-twitter" aria-hidden="true"></i>
                                               <span>Twitter</span>
                                           </a>
                                           <a href="#" class="google">
                                               <i class="fa fa-google-plus" aria-hidden="true"></i>
                                               <span>Google Plus</span>
                                           </a>
                                       </div>
                                   </div>
                               </div>
                           </div>
                           <!-- END tab-2 -->
                           <!-- END tab-5 -->
                       </div>
                       <!-- END tabs-content -->
                   </div>
                   <div class="content-block head-div head-arrow head-arrow-top visible-xs">
                       <div class="head-arrow-icon">
                           <i class="cv cvicon-cv-next"></i>
                       </div>
                   </div>
                   <div class="adblock2" id="first_ad" style="padding-top:0px">
                       <div class="img">
                           <span class="hidden-xs">
                              Entertainment  AdSense<br>728 x 90
                           </span>
                           <span class="visible-xs">
                              Entertainment  AdSense 320 x 50
                           </span>
                       </div>
                   </div>
                   <!-- similar videos -->
                   <div class="caption hidden-xs">
                       <div class="left">
                           <a href="#">More Video's</a>
                       </div>
                       <div class="clearfix"></div>
                   </div>
                   <div class="single-v-footer">
                       <div class="single-v-footer-switch">
                           <a href="javascript:void(0)" class="active" data-toggle=".similar-v">
                               <i class="cv cvicon-cv-play-circle"></i>
                               <span>Similar Videos</span>
                           </a>
                           <a href="javascript:void(0)" data-toggle=".comments">
                               <i class="cv cvicon-cv-comment"></i>
                               <span>236 Comments</span>
                           </a>
                       </div>
                       <div class="similar-v single-video video-mobile-02">
                           <div class="row">
							@if(!empty($similarVideos))
								@foreach($similarVideos as $video)
									@include('elements.post.video_re_wardsimilar')							
								@endforeach   
							@endif						   
						   </div>
                           <div class="adblock2" style="padding-top:0px">
                               <div class="img">
                                   <span class="hidden-xs">
                              Entertainment  AdSense<br>728 x 90
                           </span>
                                   <span class="visible-xs">
                              Entertainment  AdSense 320 x 50
                           </span>
                               </div>
                           </div>
						   
                           <div style="cleaar:both; overflow:hidden"> </div>
                           
					   </div>
                       <!-- END similar videos -->
                       @if($user)
                           <!-- comments -->
                           <div class="comments" id="ght">
                               <div class="reply-comment">
                                   <div class="rc-header"><i class="cv cvicon-cv-comment"></i> 
    							   <span class="semibold">{{ $videoData->total_comments_count }}</span> Comments</div>
                                   <div class="rc-ava">                                   
    								<a target="_blank" href="{{URL::to('userprofile/'.$user->name)}}">
    								    @if(file_exists($curuserBastPath))
                                                {{HTML::image($curuserUrlPath,null,['class'=>'sv-avatar'])}}
                                        @else
                                            {{HTML::image('public/img/ava2.png',null,['class'=>'sv-avatar'])}}
                                        @endif 
    								</a>                              
    							   </div>
                                   <div class="rc-comment">
                                       <form method="post" action="{{ route('comment.add') }}" id="commentfrm">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                           <textarea rows="3" name="comment_body" placeholder="Share what you think?"></textarea>
                                           <input type="hidden" name="video_id" value="{{ $videoData->id }}" />
                                           <input type="hidden" name="model" value="Video" />
                                           <button type="submit">
                                               <i class="cv cvicon-cv-add-comment"></i>
                                           </button>
                                        </form>
                                   </div>
                                   <div class="clearfix"></div>
                               </div>
                               <div class="comments-list">
                                   <div class="cl-header">
                                       <div class="c-nav">
                                           <ul class="list-inline">
                                               <li><a href="#" class="active">Popular <span class="hidden-xs">Comments</span></a></li>
                                           </ul>
                                       </div>
                                   </div>
                                   <!-- comment -->
                                   <div id="comments">   
                                     @include('comment.video._comment_replies', ['comments' => $videoData->comments, 'video_id' => $videoData->id])
                                   </div>
                                   <!-- comment -->
                                   
                               </div>
                           </div>
                           <!-- END comments -->
                       @else
                           <!-- comments -->
                           <div class="comments">
                               <div class="reply-comment">
                                   <div class="rc-header"><i class="cv cvicon-cv-comment"></i> 
    							   <span class="semibold">{{ $videoData->total_comments_count }}</span> Comments</div>
                                   <div class="rc-ava">                                   
    								<a target="_blank" href="#">
                                            {{HTML::image('public/img/ava2.png',null,['class'=>'sv-avatar'])}}
    								</a>                              
    							   </div>
                                   <div class="rc-comment">
                                       <form method="post" action="{{ route('comment.add') }}" id="commentfrm">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                           <textarea rows="3" name="comment_body" placeholder="Share what you think?"></textarea>
                                           <input type="hidden" name="video_id" value="{{ $videoData->id }}" />
                                           <input type="hidden" name="model" value="Video" />
                                           <button type="submit">
                                               <i class="cv cvicon-cv-add-comment"></i>
                                           </button>
                                        </form>
                                   </div>
                                   <div class="clearfix"></div>
                               </div>
                               <div class="comments-list">
                                   <div class="cl-header">
                                       <div class="c-nav">
                                           <ul class="list-inline">
                                               <li><a href="#" class="active">Popular <span class="hidden-xs">Comments</span></a></li>
                                           </ul>
                                       </div>
                                   </div>
                                   <!-- comment -->
                                   <div id="comments">   
                                     @include('comment.video._comment_replies', ['comments' => $videoData->comments, 'video_id' => $videoData->id]);
                                   </div>
                                   <!-- comment -->
                                   
                               </div>
                           </div>
                           <!-- END comments -->
                       @endif
                   </div>
               </div>
               <div class="content-block head-div head-arrow visible-xs">
                   <div class="head-arrow-icon">
                       <i class="cv cvicon-cv-next"></i>
                   </div>
                   <!--<div class="adblock2 adblock2-v2">-->
                   <!--    <div class="img">-->
                   <!--        <span>Google AdSense 300 x 250</span>-->
                   <!--    </div>-->
                   <!--</div>-->
               </div>
           </div>
           <!-- right column -->
           <div class="col-lg-4 col-xs-12 col-sm-12 hidden-xs">
 
               <div class="adblock">
                   <div class="img">
                       Entertainment AdSense                       <br> 336 x 280
                   </div>
               </div>
               <!-- Recomended Videos -->
               <div class="caption">
                   <div class="left">
                       <a href="#">Recomended Videos</a>
                   </div>
                   
                   <!--<div class="right">-->
                   <!--    <a href="#">Autoplay <i class="cv cvicon-cv-btn-off"></i></a>-->
                   <!--</div>-->
                   <div class="clearfix"></div>
               </div>
               <div class="list">
                   @if(!empty($recomendedVideos))
						@foreach($recomendedVideos as $video)
							@include('elements.post.video_re_ward')							
						@endforeach   
					@endif
                    
                    
				   <div class="adblock2" style="padding:0px 0 30px 0">
                       <div class="img" style="width:100%">
                           <span class="hidden-xs">
                              Entertainment  AdSense<br>728 x 90
                           </span>
                           <span class="visible-xs">
                              Entertainment  AdSense 320 x 50
                           </span>
                       </div>
                   </div>
                   <!-- load more -->
                   <div class="loadmore">
                       <a href="#"> View more videos</a>
                   </div>
               </div>
           </div>
       </div>
   </div>
   
       <script>
        var isUserLogin = ''{{ Session::get('user_id') }};
        if(!isUserLogin){
              $("#mainVideo").bind("timeupdate", function(){
                var currentTime = this.currentTime;
                if (currentTime>5){
                      window.location.href = "{{ url('/login') }}"
                }
            });
        }

    </script> 
   </div>
   {{ HTML::style('public/css/mediaelementplayer.min.css')}}
   <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-61385eedbd8b385d"></script>
<script type="text/javascript">
	var video_key = $(this).attr('href');
    var userID = '<?php echo $loggedInUser; ?>';
    var videoID = '<?php echo $videoData->id; ?>';
    var APP_URL  = '<?php echo URL::to('/'); ?>';

    if(userID){
        $("#mainVideo").bind("timeupdate", function(){
            var currentTime = this.currentTime / 60;

            $.ajax({
                type:'get',
                url:APP_URL+'/video-watch-time?userID='+userID+'&videoID='+videoID+'&duration='+currentTime,            
                success:function(data) {
                    console.log("Updated Duration: ", currentTime);
                }
            });
        });
    }

    function copyText() {
            /* Get the text field */
            var txt = document.getElementById("btn-copy-txt");

            /* Select the text field */
            txt.select();
            txt.setSelectionRange(0, 99999); /*For mobile devices*/

            /* Copy the text inside the text field */
            document.execCommand("copy");

            $('.btn-copy').text('Copied');
    }

    $('#watchlaterCurrentvideo').click(function(e){
        e.preventDefault();
        if(userID){
            var video_key = $(this).attr('href');
            $.ajax({
                type:'get',
                url:'watch-later/'+video_key,
                success:function(data) {
                    $("#watchLaterCount").text(data);
                    $(".cvicon-cv-watch-later").css('color','#f72e5e');
                }
            });
        } else {  
            window.location.replace(APP_URL+'/login');
        }

    });

    $('#likevideo').click(function(e){
        e.preventDefault();
        if(userID){
            var video_key = $(this).attr('href');
            $.ajax({
                type:'get',
                url:'liked-video/'+video_key,
                success:function(data) {
                    if(data.disLikeCount > 0)
                    {
                        $(".fa-thumbs-down").css('color','#f72e5e');
                    }
                    $("#likedCount").text(data.likeCount);
                    $("#unLikedCount").text(data.disLikeCount);
                    $(".fa-thumbs-up").css('color','#f72e5e');
                    $(".fa-thumbs-down").css('color','#637076');
                }
            });
        } else {  
            window.location.replace(APP_URL+'/login');
        }
    });

    $('#dislikevideo').click(function(e){
        e.preventDefault();
        if(userID){
            var video_key = $(this).attr('href');
            $.ajax({
                type:'get',
                url:'disliked-video/'+video_key,
                success:function(data) {
                    if(data.likeCount > 0)
                    {
                        $(".fa-thumbs-up").css('color','#f72e5e'); 
                    }
                    $("#likedCount").text(data.likeCount);
                    $("#unLikedCount").text(data.disLikeCount);
                    $(".fa-thumbs-down").css('color','#f72e5e');
                    $(".fa-thumbs-up").css('color','#637076'); 
                }
            });
        } else {  
            window.location.replace(APP_URL+'/login');
        }
    });

    $('#flagcommentbtn').click(function(e){
        $('#myModal').modal('toggle');
        var comment = $('#flagcomments').val();
        if(userID){
            var video_key = $('#flagKey').val();
            $.ajax({
                type:'get',
                url:'flag-video/'+video_key+'/'+comment,
                success:function(data) {
                    $("#videoFlag").text(data);
                    $(".cvicon-cv-flag").css('color','#f72e5e');
                }
            });
            $('#flagcomments').val('');
        } else {  
            window.location.replace(APP_URL+'/login');
        }
    });

    if(userID){
        var isWatchLaterByUser = '<?php echo $isWatchLaterByUser; ?>';
        var isLikedByUser = '<?php echo $isLikedByUser; ?>';
        var isDisLikedByUser = '<?php echo $isDisLikedByUser; ?>';
        var isFlagByUser = '<?php echo $isFlagByUser; ?>';
        if(isWatchLaterByUser > 0) {
            $(".cvicon-cv-watch-later").css('color','#f72e5e');
        }

        if(isLikedByUser > 0) {
            $(".fa-thumbs-up").css('color','#f72e5e');
        }

        if(isDisLikedByUser > 0) {
            $(".fa-thumbs-down").css('color','#f72e5e');
        }

        if(isFlagByUser > 0) {
            $(".cvicon-cv-flag").css('color','#f72e5e'); 
        }

    }
    
    function showMe(id){
        $(id).toggle();
    }
    
    $("#commentfrm").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), // serializes the form's elements.
            success: function(data)
            {
                console.log(data);
                $('#comments').html(''); // show response from the php script.
                $('#comments').append(data.html);
            }
        });
        $(this)[0].reset();
    });
    
    var isFollowing = '<?php echo $isFollowing; ?>';
    var followCount = '<?php echo $followCount; ?>';
    follow_unfollow(isFollowing, followCount);

    $(document).on('submit','#follow, #unfollow',function(e){
        e.preventDefault();
         if(userID){
            var form = $(this);
            var url = form.attr('action');
    
            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                success: function(data)
                {
                   if(!data.status){
                        follow_unfollow(data.isFollowing, data.followCount);
                        alert(data.message);
                    } else {
                        alert(data.message);
                    }
                }
            });
            $(this)[0].reset();
        } else {  
            window.location.replace(APP_URL+'/login');
        }
    });
    
    function follow_unfollow(isFollowing, followCount){        
        var csrf_field = '<?php echo csrf_field(); ?>';
        var method_field = '<?php echo method_field("DELETE"); ?>';
        var videoData_User_ID = '<?php echo $videoData->user->name; ?>';
        var unfollowformAction = '<?php echo route("unfollow", ["id" => $videoData->user->name]);?>';
        var followformAction = '<?php echo route("follow", ["id" => $videoData->user->name]);?>';

        if(isFollowing){
            var unfollowbtn = '<form id="unfollow" action="'+unfollowformAction+'" method="POST">'
                        +csrf_field+method_field+'<button type="submit" id="delete-follow-'
                        +videoData_User_ID+'" class="c-f">Unfollow</button><div class="c-s">'
                        +followCount+'</div></form><div class="clearfix"></div>';
            $('#followunfollowbtn').html('');
            $('#followunfollowbtn').append(unfollowbtn);
        } else {
            var followbtn = '<form id="follow" action="'+followformAction+'" method="POST">'
            +csrf_field+'<button type="submit" id="follow-user-'+videoData_User_ID+'" class="c-f">follow</button><div class="c-s">'+followCount+'</div></form><div class="clearfix"></div>';
            $('#followunfollowbtn').html('');
            $('#followunfollowbtn').append(followbtn);
        }

    }
</script>
@endsection