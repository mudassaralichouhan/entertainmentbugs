@extends('layouts.home')
@section('content')
@php
    $url_segment = \Request::segment(1);
@endphp
@if($url_segment == 'videos') 
    <style>
         .c-details ul.list-inline > li:first-child a{
                border-bottom: 2px solid #f72e5e;
                color: #f72e5e;
            }
    </style>
@endif
    <section class="channel light">

        @include('elements.header_profile')
        <div class="content-wrapper">
            <div class="container">
                <div class="row">
                    @if (!empty($rewardVideos))
                        @foreach ($rewardVideos as $video)
                            @include('elements.post.video')
                        @endforeach
                        {{ $rewardVideos->links() }}
                    @endif
                </div>
            </div>
        </div>
    </section>

@endsection
