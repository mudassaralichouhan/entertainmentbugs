@extends('layouts.home')

@section('meta_tags')
    <meta name="description" content="Are you an Artist? Do you want to be discovered? If yes, then make a profile and let the production houses, Casting agencies and Digital agency reach you with the best offer.">
    <meta name="keywords" content="Artist Profile, Artist Videos, Artist Gallery, Influencer, Actor / Model, Production House, Find Autions, Post Autions, Artist Website">
    <meta name="author" content="<?php echo ((isset($author) ? $author : 'Astist - Entertainment Bugs')); ?>">
    <meta property="og:site_name" content="{{URL::to('/')}}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="<?php echo ((isset($ogtitle) ? $ogtitle : 'Astist - Entertainment Bugs')); ?>"/>
    <meta property="og:description" content="<?php echo ((isset($ogdescription) ? $ogdescription : 'Are you an Artist? Do you want to be discovered? If yes, then make a profile and let the production houses, Casting agencies and Digital agency reach you with the best offer.')); ?>"/>
    <meta name="theme-color" content="#f82e5e">
    <meta property="og:keywords" content="Artist Profile, Artist Videos, Artist Gallery, Influencer, Actor / Model, Production House, Find Autions, Post Autions, Artist Website">
    <meta property="og:url" content="<?php echo url()->current(); ?>"/>
    <meta property="og:image" content="<?php echo ((isset($ogimage) ? $ogimage : 'https://www.entertainmentbugs.com/public/uploads/logo/artist-logo-social-media.png')); ?>"/><!--profile image-->
     
   @endsection
   
@section('content')

    <link href="{{ asset('public/css/casting.css') }}" rel="stylesheet">
    <style>
        #live-video-user-list-home {
            display: none;
        }

        .production-all-display {
            width: 99%;
            float: left;
            background: #f4f3f3;
            border: 1px solid #e5e5e5;
            margin: 0.5% 0.5%;
        }

        #right-side-bar .production-hs1 {
            width: 100%;
            float: none;
        }

        #right-side-bar .production-hs2 {
            width: 100%;
            float: none;
        }

        #right-side-bar {
            background: #fff;
            padding: 5px 10px 16px 10px;
        }

        #right-side-bar h5 {
            color: #f72e5e !important;
            font-size: 14px;
            margin: 7px 0 7px 0;
            padding: 9px 0 0 0;
        }

        #right-side-bar a.ic {
            padding: 0 !important;
            display: block;
            width: 45px;
            margin: 0 10px 10px 0;
            height: 45px;
            float: left;
            background: none !important;
            border: 0px !important;
        }

        #right-side-bar p {
            font-size: 13px;
            margin: 0 0 5px !important;
            line-height: 20px;
        }

        #right-side-bar .production-hs2 a.view-m {
            position: absolute;
            right: 3px;
            bottom: 3px;
            width: 85px;
            border: 0px !important;
            background: #28b47e !important;
            color: #fff !important;
            padding: 6px 10px 4px 10px;
            font-size: 14px;
            margin: 0px !important;
        }

        #right-side-bar a.ic img {
            border: 2px solid #eaeaea;
            width: 100% !important;
            height: 100%;
            object-fit: cover;
            border-radius: 500px
        }

        .col-lg-7.col-md-6.col-sm-12:before {
            content: "";
            width: 92%;
            margin: 0;
            height: 100%;
            position: absolute;
            right: 4%;
            top: 0;
            z-index: 9;
            background: rgba(0, 0, 0, 000.1);
            opacity: 0;
        }

        .col-lg-7.col-md-6.col-sm-12 {
            position: relative;
        }

        .casting-menu ul li a {
            color: #fff;
            font-size: 12px;
            padding: 16px 18px 13px 18px;
            display: block;
            font-family: 'Hind Guntur', sans-serif !important;
        }

        .casting-menu ul {
            list-style: none;
            padding: 0px;
            text-align: center;
            background: rgba(247, 46, 94, 0.9);
            margin: 0px;
            position: absolute;
            z-index: 999;
            width: 100%;
        }


        .pr-artist {
            color: #f72e5e !important;
        }

        .pr-artist i {
            color: #f72e5e !important;
        }
    </style>

    <div class="content-wrapper">
        <div class="content-wrapper">
            <div class="containers">
                <div class="casting-menu">
                    <ul>
                        <li><a href="{{ URL::to('artist-profile') }}"> Artist Profile</a></li>
                        <li><a href="{{ URL::to('artist-videos') }}"> Artist Videos</a></li>
                        <li><a href="{{ URL::to('artist-gallery') }}"> Artist Gallery</a></li>
                        <li><a href="">Influencer</a></li>
                        <li><a href="">Actor / Model</a></li>
                        <li><a href="{{ URL::to('production-houses') }}">Production House</a></li>
                        <li><a href="{{ URL::to('find-auditions') }}">Find Autions</a></li>
                        <li><a href="{{ URL::to('post-auditions') }}">Post Autions</a></li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="banner">
            <div class="intro-banner main_banner">
                <div class="container">
                    <!-- Intro Headline -->
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            <div class="banner-headline-alt">
                                <h3>Are you a Artist? <br> Then you are at the right place </h3>
                                <p>Make a profile and let the Production houses, Casting agencies and Digital agency reach you with the best offer.</p>

                                <?php
                         $userid=session()->get('user_id');
     if($userid != ''){
    $artist_about=DB::select('select * from production_about where user_id='.$userid);
    $artist_what_we_do=DB::select('select * from production_what_we_do where user_id='.$userid);
    $artist_achievment=DB::select('select * from production_awards where user_id='.$userid);
    $production_my_team=DB::select('select * from production_my_team where user_id='.$userid);
    $artist_gallery=DB::select('select * from production_gallery where user_id='.$userid);
    $artist_video=DB::select('select * from production_video where user_id='.$userid);
    $artist_project=DB::select('select * from production_latest_project where user_id='.$userid);
     $artist_contact=DB::select('select * from production_contact_us where user_id='.$userid);
                    $progress = 0;
                 if( count($artist_about) > 0){
                     $progress = 1;
                 }
                  if(count($artist_what_we_do) > 0){
                      $progress = 1;
                 }
                   if(count($artist_achievment) >0){
                     $progress = 1;
                 }
                   if(count($production_my_team ) >0){
                     $progress = 1;
                 }
                   if(count($artist_gallery ) >0){
                      $progress = 1;
                 }
                   if(count($artist_video ) >0){
                      $progress = 1;
                 }
                   if(count($artist_project ) >0){
                      $progress = 1;
                 }
                    if(count($artist_contact ) >0){
                      $progress = 1;
                 }
                 if($progress==0){
                ?>
                                <a href="{{ URL::to('create-an-artist-profile') }}" class="button"
                                    style="border-color:#6c146b; color:#6c146b;width: 240px;"> Create an Artist Profile</a>

                                <?php } }
                    else { ?>
                                <a href="{{ URL::to('create-an-artist-profile') }}" class="button"
                                    style="border-color:#6c146b; color:#6c146b;width: 240px;"> Create an Artist Profile</a>
                                <?php } ?>




                            </div>
                        </div>
                    </div>
                    <!-- Search Bar -->
                </div>
                <div class="background-image-container it-home"
                    style="background:url({{ asset('public/web_img/ban-05.jpg') }}) center top;"></div>
            </div>
        </div>




        <div class="well-none" id="third-b2" style="border-top: 0px solid #6f116c;  background:#fff">
            <div class="container">
                <div class="text-content white-font" style="background: #;padding:90px 0px;">
                    <div class="row">
                        <div id="bl0" class="col-lg-6 col-md-6 col-sm-12" style="position:relative;padding: 0 60px;">
                            <div class="col-lg-6 col-md-6 col-sm-12" style="position:relative">
                                <img src="{{ asset('public/web_img/block-01.png') }}"
                                    style="margin-bottom:30px">
                                <img src="{{ asset('public/web_img/block-03.png') }}" class="no-dis">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12" style="position:relative">
                                <img src="{{ asset('public/web_img/block-02.png') }}"
                                    style=" width:100%;margin-top:30px;">
                                <img src="{{ asset('public/web_img/block-04.png') }}" class="no-dis"
                                    style=" width:100%;margin-top:30px;">
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12" id="fl-r5" style="padding:106px 20px 60px 6px; ">
                            <h2 style="color:#000">Create your profile as an Artist</h2>
                            <p> Join us to create a profile as an artist. Upload your images, audition links and work links all at one place and allow the casting agencies to find you the best roles for you. You can even share a simplified link of your profile to your contacts to showcase your talents.</p>
                            <a href="{{ URL::to('create-an-artist-profile') }}"
                                class="button button-sliding-icon ripple-effect big margin-top-20"
                                style="border-color:#6c146b; color:#6c146b;width: 154.875px;">Get Started <i
                                    class="icon-material-outline-arrow-right-alt"></i></a>
                        </div>





                    </div>
                    <!-- Infobox / End -->
                </div>
                <div style="clear:both; overflow:hidden"></div>
            </div>
        </div>



        <div class="clear"></div>





        <div class="well-none" id="third-b2" style="border-top: 0px solid #6f116c;   background: #f0f2f5;">
            <div class="container">
                <div class="text-content white-font" style="padding:70px 0px;">
                    <div class="row">


                        <div class="col-lg-7 col-md-6 col-sm-12" style="position:relative;padding: 0 30px;float:right">
                            <video id="mobile-set" autoplay="autoplay" loop muted width="100%" height="370">
                                <source src="{{ asset('public/web_video/talent_01.mp4') }}" type="video/mp4">
                                <source src="{{ asset('public/web_video/talent_01.mp4') }}" type="video/ogg">
                                Your browser does not support the video tag.
                            </video>
                        </div>

                        <div class="col-lg-5 col-md-6 col-sm-12" style="padding:50px 20px 23px 30px; ">
                            <h2 style="color:#000">Make a Showreel in our portal.</h2>
                            <p> Make a showreel and upload it in your profile. It will help the ad makers, production houses and castiung agencies to see your talent.
                                <a href="{{ URL::to('create-an-artist-profile') }}"
                                    class="button button-sliding-icon ripple-effect big margin-top-20"
                                    style="border-color:#6c146b; color:#6c146b;width: 154.875px;">Get Started <i
                                        class="icon-material-outline-arrow-right-alt"></i></a>
                        </div>


                    </div>
                    <!-- Infobox / End -->
                </div>
                <div style="clear:both; overflow:hidden"></div>
            </div>
        </div>



        <div class="well-none" id="third-b2" style="border-top: 0px solid #6f116c;   background: #fff;">
            <div class="container">
                <div class="text-content white-font" style="padding:70px 0px;">
                    <div class="row">
                        <div class="col-lg-7 col-md-6 col-sm-12" style="position:relative;padding: 0 10px;">
                            <video id="mobile-set" autoplay="autoplay" loop muted width="100%" height="370">
                                 <source src="{{ asset('public/web_video/photo_shoot.mp4') }}" type="video/mp4">
                                <source src="{{ asset('public/web_video/photo_shoot.mp4') }}" type="video/ogg">
                               
                                Your browser does not support the video tag.
                            </video>
                        </div>

                        <div class="col-lg-5 col-md-6 col-sm-12" style="padding:40px 1px 23px 30px; ">
                            <h2 style="color:#000">Upload photos to hightlight talent.</h2>
                            <p>Upload your photos in your profile to allow your fans as well casting people see you and find the character they are looking for in you.
                                <a href="{{ URL::to('create-an-artist-profile') }}"
                                    class="button button-sliding-icon ripple-effect big margin-top-20"
                                    style="border-color:#6c146b; color:#6c146b;width: 154.875px;">Get Started <i
                                        class="icon-material-outline-arrow-right-alt"></i></a>
                        </div>

                    </div>
                </div>
                <!-- Infobox / End -->
            </div>
            <div style="clear:both; overflow:hidden"></div>
        </div>
    </div>



    <div class="clear"></div>


    <div class="all-pr" style="background:#f4f3f3;padding:70px 0 0 0; margin-bottom:-50px ">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="simple-text-block">
                        <h3 style="color:#000; margin-bottom:5px">Progress is impossible without change</h3>
                        <span style="color:#000">Keep updating your profile with new photos and videos of yourself <br> to be in the run for the best you are looking for.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clear"></div>

    <div class="artist-landing-page">
        <div id="hire_companies">
            <div class="well-none">
                <div class="text-content white-font" style="padding-top:0px;background:#f4f3f3;">
                    <div class="container">
                        <div class="row" style="position:relative; ">
                            <div class="col-lg-my-own-6" style="position:relative">


                                <div id="myCarousel" class="carousel slide" data-ride="carousel">


                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                        @if ($premiumArtist)
                                            @foreach ($premiumArtist as $key => $artist)
                                                <div class="item {{ $key == 0 ? 'active' : '' }}">
                                                    @if ($artist->profile_photo)
                                                        <img src="{{ $artist->profile_photo }}" alt="{{ $artist->profile_name }}" id="user-im">
                                                    @else
                                                        <span class="shortname">{{ name_to_pic($artist->profile_name) }}</span>
                                                    @endif
                                                    <div class="alerts user-block">
                                                        <span class="username" style="margin:0;color:#f72e5e">{{ $artist->profile_name }}</span>
                                                        <p>{{ $artist->about_me }}</p>
                                                        <div class="team">
                                                            <span class="description_heading"> Location: {{ $artist->city.", ".$artist->state.", ".$artist->country }}</span>
                                                            <span class="description_loc" style="margin:-0px 0 0 0">
                                                            </span>
                                                            <div class="search_skill">
																@php
																	$categories = explode(",", $artist->multiple_category);
																	$followers = ($artist->user) ? $artist->user->getFollowers() : "";
																@endphp
																@foreach( $categories as $category )
	                                                                <button class="btn btn-default btn-xs bg_prpl">{{ $category }}</button>
																@endforeach
                                                            </div>
                                                            <div class="followers-block">
																@if( $followers && count($followers) > 0 )
																	@foreach($followers as $follower)
																		@php
																			$user = \App\Models\User::select('name', 'photo')->where('id', $follower->user_id)->where('isDeleted', 0)->first();
																		@endphp

																		@if( !empty($user->photo) )
																			<img src="{{ asset("public/uploads/users/small/".$user->photo) }}" />
																		@else
																			<img src="{{ asset("public/img/ava2.png") }}" />
																		@endif
																	@endforeach
																@endif
                                                                <p><i class="fa fa-heart"style="margin:0;color:#f72e5e"></i> {{ ($artist->user)?$artist->user->getFollowerCount():"" }} Followers</p>
																@if ( Session::get("user_id") && isset($user) && $user->isFollowedBy() !=null )
																	<a data-do="unfollow" data-user-id="{{ @$artist->user->id }}" style="background-color: #f72e5e!important;"><i class="fa fa-solid fa-heart"></i> Following </a>
																@else
																	<a data-do="follow" data-user-id="{{ @$artist->user->id }}" class="flw"><i class="fa fa-heart-o"></i> Follow me </a>
																@endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif

                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                            <li data-target="#myCarousel" data-slide-to="1"></li>
                                            <li data-target="#myCarousel" data-slide-to="2"></li>
                                        </ol>



                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                        <span class="fa fa-angle-left"></span>
                                        <!--<span class="sr-only">Previous</span>-->
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                        <span class="fa fa-angle-right"></span>
                                        <!--<span class="sr-only">Next</span>-->
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-my-own-5 right_block_companies">
                                <div class="right_block_companiesinside">
                                    <h2 style="color:#000 !important">Be a featured Artist.
                                    </h2>
                                    <p>The top profile and showreel gets featured on our website and in the casting and production community. </p>
                                    <a href="{{ URL::to('artist-profile') }}" class="button button-sliding-icon ripple-effect new-bg"
                                        tabindex="0"> All Artist<i class="fa fa-long-arrow-right"></i></a>
                                </div>

                            </div>

                        </div>
                        <!-- Infobox / End -->
                    </div>
                    <div style="clear:both; overflow:hidden"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="clear"></div>

    <div class="block double-gap-top double-gap-bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="simple-text-block">
                        <h3>Top portfolio will be featured in our amazing production house community</h3>
                        <span> Create a profile and get cast and find artist for your new <br> movies and web series.
                            Selected Portfoilio will be featured <br> to trending production house</span>
                        <a href="{{ URL::to('create-an-artist-profile') }}" title="">Create a profile</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clear"></div>

    <div class="well-none" id="third-b2" style="border-top: 0px solid #6f116c;  background:#fff">
        <div class="container">
            <div class="text-content white-font" style="padding:70px 0px;">
                <div class="row">


                    <div class="col-lg-7 col-md-6 col-sm-12" style="position:relative;padding: 0 30px;">
                        <video id="mobile-set" width="100%" height="370" autoplay loop muted>
                            <source src="{{ asset('public/web_video/production_video.mp4') }}" type="video/mp4">
                            <source src="{{ asset('public/web_video/production_video.mp4') }}" type="video/ogg"> 
                            Your browser does not support the video tag.
                        </video>
                    </div>

                    <div class="col-lg-5 col-md-6 col-sm-12" style="padding:50px 20px 23px 30px; ">
                        <h2 style="color:#000">Get cast in your next role today.</h2>
                        <p> Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative
                            things to strategy foster collaborative thinking. Leverage agile frameworks to provide
                            <a href="{{ URL::to('create-an-artist-profile') }}"
                                class="button button-sliding-icon ripple-effect big margin-top-20"
                                style="border-color:#6c146b; color:#6c146b;width: 154.875px;">Get Started <i
                                    class="icon-material-outline-arrow-right-alt"></i></a>
                    </div>
                </div>
                <!-- Infobox / End -->
            </div>
            <div style="clear:both; overflow:hidden"></div>
        </div>
    </div>

    <div class="clear"></div>












    <div class="well-none" id="third-b2" style="border-top: 0px solid #6f116c;  background:#f4f3f3;">
        <div class="container">
            <div class="text-content white-font" style=" padding:70px 0px;">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12"
                        style="position:relative;padding: 0px 20px 30px 10px;float:right">
                        <video id="mobile-set" width="100%" height="370" autoplay loop muted>
                            <source src="{{ asset('public/web_video/casting.mp4') }}" type="video/mp4">
                            <source src="{{ asset('public/web_video/casting.mp4') }}" type="video/ogg"> 
                            Your browser does not support the video tag.
                        </video>
                    </div>

                    <div class="col-lg-5 col-md-6 col-sm-12" style="padding:70px 30px 0px 10px; ">
                        <h2 style="color:#000; width:100%">Find Auditions and boost your career</h2>
                        <p> Leverage agile frameworks to provide a robust synopsis for high level overviews. Iterative
                            things to strategy foster collaborative thinking. Leverage agile frameworks to provide
                            <a href=""
                                class="button button-sliding-icon ripple-effect big margin-top-20 mobile-off2"
                                style="border-color:#6c146b; color:#6c146b;width: 154.875px;">Find Auditions <i
                                    class="icon-material-outline-arrow-right-alt"></i></a>
                    </div>


                    <div id="last-section9">
                        <div class="col-sm-3" style="padding: 20px 4px 0 4px">
                            <div class="production-all-display" id="right-side-bar">
                                <div class="production-hs1">
                                    <a href="" class="ic">
                                        <img
                                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a>
                                    <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                    <div class="clear"></div>
                                </div>
                                <div class="production-hs2">

                                    <p class="prd-info"> Hi we are looking for a male lead for ...</p>

                                    <div class="sp1" id="auditions">
                                        <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager
                                        </p>
                                        <div class="clear"></div>
                                        <p><span><i class="fa fa-map-marker"></i> Location: </span> Bangalore</p>
                                        <div class="clear"></div>

                                        <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> Mar 29 2022</p>
                                    </div>
                                    <a href="" class="view-m">View Now</a>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="col-sm-3" style="padding: 20px 4px 0 4px">
                            <div class="production-all-display" id="right-side-bar">
                                <div class="production-hs1">
                                    <a href="" class="ic">
                                        <img
                                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a>
                                    <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                    <div class="clear"></div>
                                </div>
                                <div class="production-hs2">

                                    <p class="prd-info"> Hi we are looking for a male lead for ...</p>

                                    <div class="sp1" id="auditions">
                                        <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager
                                        </p>
                                        <div class="clear"></div>
                                        <p><span><i class="fa fa-map-marker"></i> Location: </span> Bangalore</p>
                                        <div class="clear"></div>

                                        <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> Mar 29 2022</p>
                                    </div>
                                    <a href="" class="view-m">View Now</a>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="col-sm-3 mobile-off2" style="padding: 20px 4px 0 4px">
                            <div class="production-all-display" id="right-side-bar">
                                <div class="production-hs1">
                                    <a href="" class="ic">
                                        <img
                                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a>
                                    <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                    <div class="clear"></div>
                                </div>
                                <div class="production-hs2">

                                    <p class="prd-info"> Hi we are looking for a male lead for ...</p>

                                    <div class="sp1" id="auditions">
                                        <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager
                                        </p>
                                        <div class="clear"></div>
                                        <p><span><i class="fa fa-map-marker"></i> Location: </span> Bangalore</p>
                                        <div class="clear"></div>

                                        <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> Mar 29 2022</p>
                                    </div>
                                    <a href="" class="view-m">View Now</a>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        <div class="col-sm-3 mobile-off2" style="padding: 20px 4px 0 4px">
                            <div class="production-all-display" id="right-side-bar">
                                <div class="production-hs1">
                                    <a href="" class="ic">
                                        <img
                                            src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTfja8r2uH2pQ2vFlDAlC7hKzxfo_x7Nh9dzug8IsLO2E6JLIyYeEF6xgzcS668CrCU-Dw&amp;usqp=CAU"></a>
                                    <h5>Looking for Male lead &amp; co-producer | 60 days schedule in Nepal</h5>
                                    <div class="clear"></div>
                                </div>
                                <div class="production-hs2">

                                    <p class="prd-info"> Hi we are looking for a male lead for ...</p>

                                    <div class="sp1" id="auditions">
                                        <p><span><i class="fa fa-list-alt"></i> Category: </span> Actor,Production Manager
                                        </p>
                                        <div class="clear"></div>
                                        <p><span><i class="fa fa-map-marker"></i> Location: </span> Bangalore</p>
                                        <div class="clear"></div>

                                        <p><span><i class="fa fa-calendar-check-o"></i> Valid Till: </span> Mar 29 2022</p>
                                    </div>
                                    <a href="" class="view-m">View Now</a>
                                    <div class="clear"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>

                        <div class="clear"></div>



                        <a href="#" class="button button-sliding-icon ripple-effect big margin-top-20 mobile-show2"
                            style="display:none;border-color:#6c146b; color:#6c146b;width: 154.875px;">Find Auditions <i
                                class="icon-material-outline-arrow-right-alt"></i></a>


                    </div>





                </div>

            </div>
            <!-- Infobox / End -->
        </div>
        <div style="clear:both; overflow:hidden"></div>
    </div>
    </div>



    <section id="action-page" style="background:#f72e5e !important;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="simple-text-block">
                        <div class="simple-text-ht">
                            <h3 style="color: #fff;text-align:left;">Let's start something completely new with us</h3>
                            <span style="color: #fff;text-align:left;">Your Profile in minutes with Production house
                                assistant is ready!</span>
                        </div>
                        <a href="#" title="" class="right_main1">Create an Account</a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <style>
        #fl-r5 {
            float: right;
        }

        #bl0 {
            float: left
        }

        @media all and (max-width: 767px) {
            .intro-banner.main_banner .col-md-12 {
                padding: 0 10px;
            }

            .casting-menu ul li a {
               
            }

            .banner-headline-alt h3 {
                width: 100% !important;
                color: #fff;
                line-height: 36px;
                font-size: 36px;
                margin: 0 auto 10px;
                text-align: center;
            }

            .banner-headline-alt p {
                width: 100%;
                line-height: 21px;
                font-size: 13.5px;
                font-weight: 400;
            }

            .banner-headline-alt a {
                font-size: 14px;
            }

            #third-b2 a {
                font-size: 14px;
            }

            #hire_companies .right_block_companies a {
                font-size: 14px;
            }

            #hire_companies .right_block_companies p {
                font-size: 14px;
                line-height: 22px;
            }

            .intro-banner {
                padding: 130px 0 66px 0;
                position: relative;
            }

            #fl-r5 {
                float: right;
                padding: 0px 20px !important;
            }

            #bl0 {
                padding: 0px 20px !important;
            }

            #bl0 .col-lg-6 {
                width: 50%;
                float: left
            }

            #bl0 .col-lg-6 img {
                width: 100% !important;
            }

            .no-dis {
                display: none;
            }

            #third-b2 h2 {
                font-size: 26px;
                line-height: 31px;
                font-weight: 600;
                width: 100%;
            }

            .text-content.white-font {
                padding: 50px 0px !important;
            }

            #third-b2 .col-lg-5.col-md-6.col-sm-12 {
                padding: 0px 20px !important
            }

            #third-b2 p {
                font-size: 14px;
                line-height: 22px;
                font-weight: 400;
            }

            #third-b2 .col-lg-7.col-md-6.col-sm-12 {
                padding: 0px 20px !important
            }

            #mobile-set {
                width: 100% !important;
                padding: 0px !important;
                height: auto !Important;
                margin-bottom: 40px !Important;
            }

            .simple-text-block>h3 {
                width: 99%;
                color: #ffffff;
                font-size: 26px;
                font-weight: 600;
                margin: 0 auto 20px;
            }

            .all-pr .simple-text-block>h3 {
                font-size: 30px;
                line-height: 34px;
            }

            .simple-text-block span br {
                display: none;
            }

            .simple-text-block span {
                font-size: 14px;
                padding: 0 20px;
            }

            #last-section9 {
                padding: 0 10px;
                margin-top: -13px
            }

            #last-section9 .production-all-display {
                margin-bottom: 10px;
            }

            .mobile-off2 {
                display: none !important;
            }

            .mobile-show2 {
                display: block !Important;
                margin: 20px auto 0 !important;
            }

            #hire_companies .col-lg-my-own-6 {
                width: 100%;
                float: none !Important
            }

            #user-im {
                width: 200px;
                height: 200px;
                margin: 0 auto 20px;
            }

            #hire_companies .col-lg-my-own-5 {
                width: 100%;
                margin: 20px auto 0;
                position: relative;
                right: 0px;
                padding: 0 20px;
            }

            #hire_companies .alerts {
                top: 0;
                right: 0;
                padding: 14px 20px 20px 25px;
                width: 90%;
                position: relative;
                margin: 0 auto 31px;
            }

            #hire_companies .followers-block img {
                width: 35px;
                margin-left: -26px;
            }

            #hire_companies .right_block_companiesinside {
                padding: 20px 20px 40px 20px;
            }

            #hire_companies h2 {
                font-size: 22px;
                line-height: 29px;
                font-weight: 600;
                margin: 11px 0px 7px 0;
            }

            .block {
                float: left;
                margin: 0px 0 0px 0;
                padding: 70px 0 60px 0;
            }

            .followers-block {
                width: 100%;
            }

            .alerts p {
                font-size: 13px;
            }

            .carousel-indicators {
                margin: 0;
                bottom: 0;
                right: 0;
                position: relative;
                width: 100%;
                left: auto;
                text-align: center;
            }

            #action-page .simple-text-ht {
                width: 100% !important;
            }

            #action-page .simple-text-ht>h3 {
                font-size: 30px;
                text-align: center !important;
            }

            #action-page .simple-text-block span {
                text-align: center !important;
                font-size: 14px;
                margin: 0 auto;
                display: block;
                width: 95%;
            }

            #action-page .right_main1 {
                float: none;
            }

            .right_block_companiesinside {
                padding: 20px 20px 34px 20px;
            }

            .it-home {
                animation: slideleft 9000s infinite linear;
            }


            @keyframes slideleft {
                from {
                    background-position: 0%;
                }

                to {
                    background-position: 90000%;
                }
            }



        }
    </style>
@endsection
