@extends('layouts.home')
@section('content')
<style>
 a.frst1 i{color: #f72e5e !important;}
#rewards_main_page {padding: 40px 0 10px 0}
#rewards_main_page ul li {font-size: 24px;line-height: 64px;}
#reward_1 .b-video .v-img {height: 105px;overflow: hidden;}
#reward_1 .v-img img { height: 100%;object-fit: cover;}
#mobile-menu-design ul.list-inline{display:none;}
#reward_1 .content-block .cb-header {background: #fff;margin: 15px 0 20px 0;padding:10px 0 12px 20px;}
body.light .content-wrapper {background-color: #f4f3f3;padding: 0px 0 0 0;}
#home_page_video .v-desc a {font-size: 12px;display: block;line-height: 15px;height: 35px;}
.b-video .v-views {font-size: 10.5px;color: #7e7e7e;padding-top: 0px;}
.b-video {margin-bottom: 18px;}

@media only screen and (max-width: 767px){
#mobile-menu-design ul.list-inline{display:block;}
#mobile-menu-design{margin-top:15px;}
#mobile-menu-design ul.list-inline{width:73%;    display: inline-block;}
#mobile-menu-design ul.list-inline li{width:auto;margin-right:3px;}
.content-block .cb-header .list-inline {margin-left: -11px;line-height: 34px;margin-right: 6px;}
}

a.blinko {
  -webkit-animation: NAME-YOUR-ANIMATION 1s infinite;  /* Safari 4+ */
  -moz-animation: NAME-YOUR-ANIMATION 1s infinite;  /* Fx 5+ */
  -o-animation: NAME-YOUR-ANIMATION 1s infinite;  /* Opera 12+ */
  animation: NAME-YOUR-ANIMATION 1s infinite;  /* IE 10+, Fx 29+ */background-color:#ff2a5d;}

@-webkit-keyframes NAME-YOUR-ANIMATION {
  0%, 49% {
    background-color:#cd214a;
  }
  50%, 100% {
    background-color: #ff2a5d;
  }
}

</style>

    <div class="content-wrapper">

        <div class="container" id="">
            <!--<img src="{{ url('/public/img/rewards_banner.jpg') }}" style="width:100%">-->

            <div class="row">
                <div class="col-lg-12" id="reward_1">
                    <!-- Featured Videos -->
                    <div class="content-block">


                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">

                                        <li>
                                            <a href="{{ URL::to('rewards') }}">
                                                <span class="visible-xs"> All Rewards video </span>
                                                <span class="hidden-xs"> All Rewards video </span>
                                            </a>
                                        </li>
                                        <li><a href="{{ URL::to('rewards/most-watched') }}"  class="color-active">Most watched </a></li>
                                      

                                        <li> <a href="{{ URL::to('participate-videos') }}" class="promote_your_video1 blinko" style="background:#28b47e;"><i class="cv cvicon-cv-upload-video"></i>&nbsp Participate Now</a> </li>
                                        <li> <a href="{{ URL::to('all-top-entertainer') }}" class="promote_your_video1" style=" background:#28b47e;"> Top
                                                Entertainer List</a> </li>
                                    </ul>
                                </div>
                                
                                
                                     
                                <div class="col-lg-2 col-sm-2 col-xs-12" id="mobile-menu-design">
                                     
                                     
                                         <ul class="list-inline">

                                     <li>  <a href="{{ URL::to('rewards') }}" class="">
                                                <span class="visible-xs"> All Rewards video </span>  </a> </li>
                                        <li><a href="{{ URL::to('rewards/most-watched') }}" class="color-active">Most watched </a></li>
                                       
                                    </ul>
                                    
                                <div class="btn-group pull-right bg-clean">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Sort by <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                            {{-- <li><a onclick="categoryVideoSortByRecent()"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                            <li><a onclick="categoryVideoSortByTopViewed()"><i class="cv cvicon-cv-view-stats"></i> Top Viewed</a></li>
                                            <li><a onclick="categoryVideoSortByTopLiked()"><i class="fa fa-thumbs-up"></i> Top liked</a></li>
                                            <li><a onclick="categoryVideoSortByTopLongest()"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li> --}}

                                            <li><a onclick="rewardVideo('recentdata')"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                            <li><a onclick="rewardVideo('topviwed')"><i class="cv cvicon-cv-view-stats"></i> Top Viewed</a></li>
                                            <li><a onclick="rewardVideo('topliked')"><i class="fa fa-thumbs-up"></i> Top liked</a></li>
                                            <li><a onclick="rewardVideo('toptranding')"><i class="fa fa-heart-o"></i> Top Trending</a></li>
                                            <li><a onclick="rewardVideo('toplongest')"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            
                            </div>
                        </div>

                        <div class="cb-content videolist" id="home_page_video">
                           

                            {{-- @if (!empty($videos))
                            <div id="all">
                                @include('elements.reward.reward')
                            </div>
                          @endif --}}
                          
                            @if (!empty($rewardMostWatchedVideos))
                           
                                {{-- @foreach ($rewardMostWatchedVideos as $video) --}}
                                <div id="all">
                                     @include('elements.reward.reward') 
                               </div>  

{{--                                 
                                    @if ($loop->iteration % 6 == 0)
                                        <div style="clear:both; overflow:hidden"></div>
                                   @endif --}}
                               
                                {{-- @endforeach --}}
                                
                              
                            @endif

                       
                           
                                   
                              

                              
                            
                        </div>
                      
                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>

            <!-- pagination -->
            {{-- <div class="v-pagination">
                <ul class="list-inline">
                    <li class="v-pagination-prev"><a href="#"><i class="cv cvicon-cv-previous"></i></a></li>
                    <li class="v-pagination-first"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">...</a></li>
                    <li><a href="#">10</a></li>
                    <li class="v-pagination-skin visible-xs"><a href="#">Skip 5 Pages</a></li>
                    <li class="v-pagination-next"><a href="#"><i class="cv cvicon-cv-next"></i></a></li>
                </ul>
            </div> --}}
            <!-- /pagination -->

        </div>
    </div>


    <script> 
        // function rewardVideoSortBy(order_by){
        //     $.ajax({
        //         type: "GET",
        //         url: "{{ URL::to('show_all_reward_order_by') }}",
        //         data: {'keyword':order_by},
        //         cache: false,
        //         success: function(response)
        //         {
        //             $('#allRewardVideos').empty().html(response);
        //         }
        //     });
        // }


        function rewardVideo(order_by){
    
        $.ajax({
            type: "GET",
            url: "{{ URL::to('show_all_reward_order_by') }}",
            data: {'keyword':order_by},
            cache: false,
            success: function(response)
            {
                $('#all').html(response);
                console.log(response);
            }
        });
    }
    </script>
@endsection
