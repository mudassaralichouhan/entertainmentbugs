@extends('layouts.home')
@section('content')
    <style>body.light .content-wrapper {
            background-color: #f4f3f3;
        }

        .form-group {
            margin-bottom: 18px;
        }

        .edit-page .u-form label {
            margin-bottom: 2px;
        }

        .edit-page .u-form .checkbox {
            margin-bottom: 4px;
        }
    </style>
    <!--Start Upload Video Part-->
    <div class="content-wrapper" id="upload-video-step1">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 upload-page">
                    <div class="u-area">
                        {{ Form::open(array('method' => 'post', 'id' => 'video-upload', 'enctype'=>'multipart/form-data')) }}

                        <!--               <i class="cv cvicon-cv-upload-videoa"><span style="font-size: 14px;display: block;margin: 14px 0 20px 0;font-family: 'Hind Guntur', sans-serif;">-->
                        <!--                   <img src="http://www.entertainmentbugs.com/public/img/play-icons.png" style="margin-bottom:10px; width:130px;opacity:0.6"><br>-->
                        <!--                   <small style="background:#637076; color:#fff;padding: 1px 12px;opacity: 0.6;">Click here to Browse Video File</small></span></i>-->
                        <!--<div id="dropbox">-->
                        <!--<p class="u-text1">Select Video files to upload</p>-->
                        <!--<p class="u-text2">Upload video files and enjoy</p>-->
                        <!--</div>-->
                        {{-- Form::button('Upload Video', ['type' => 'button', 'id'=>'submit-video', 'class' => 'btn btn-primary u-btn']) --}}{{Form::close()}}
                    </div>
                    <!--<div class="u-terms">-->
                    <!--                <p>By submitting your videos to our platform, you acknowledge that you agree to our <a href="#">Terms of Service</a> and <a href="#">Community Guidelines</a>.</p>-->
                    <!--                <p class="hidden-xs">Please be sure not to violate others' copyright or privacy rights. Learn more</p>-->
                    <!--            </div>-->
                </div>
            </div>
        </div>
    </div>
    <!-- End Upload Video Part -->
    <style>
        .u-area span:hover img {
            opacity: 0.7;
        }

        .u-area i:hover span {
            color: #637076;
            opacity: 0.7;
        }

        .upload-page .u-area .u-btn {
            border-radius: 5px;
        }

        .text-danger {
            color: red !important;
        }
    </style>
    <!--Start Update Video Part-->

    <div class="content-wrapper upload-page edit-page" id="upload-video-step2">
        {{ Form::open(array('method' => 'post', 'id' => 'uploadVideo','url'=>url('upload-video'))) }}
        {{ method_field("PUT") }}
        {{ Form::hidden('video', null, ['id' => 'selected-video-name']) }}
        {{ Form::hidden('selected_thumb', null, ['id' => 'video-selected-thumbnail']) }}

        <div class="container-fluid u-details-wrap">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="error-message">@include('elements.errorSuccessMessage')</div>
                            <div class="u-details">
                                <div class="row">
                                    <div class="col-lg-12 ud-caption">Upload Details</div>
                                    @php
                                        $videofile = explode('/',$tempvideo->video_path);
                                    @endphp
                                    <div class="col-lg-12 ud-caption">File Name : {{$videofile[3]}}</div>

                                    <video style="display: none;" id="myvid">
                                        <source src="{{asset('public'.$tempvideo->video_path)}}" type="video/mp4">
                                    </video>

                                    <input name="video" type="hidden" id="uploadVideoFile" value="{{$videofile[3]}}"/>
                                    <input type="hidden" name="thumb[1]" id="croppie-video-thumb-input"
                                           value="{{asset('public'.$tempvideo->thumb_path)}}"/>
                                    <input type="hidden" id="duration" name="duration"/>
                                    <input type="hidden" id="tempid" name="tempid" value="{{$tempvideo->id}}"/>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="row margin-top-reduce" style="margin:40px 0 0 0;">
                        <div class="col-lg-2 ud-caption" style="line-height:90px"> Thumbnails Video</div>
                        <div class="col-lg-12" id="thumbnail_image">
                            <div class="u-progress" id="upload-photo-progress-1" style="display: none; width: 100%">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                         aria-valuemax="100" style="width: 0%;" data-progress="bar">
                                        <span class="sr-only" data-progress="text">0% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <input name="thumb_image" type="file" id="croppie-video-thumb-file" style="display: none"
                                   accept="image/x-png,image/gif,image/jpeg"/>
                            <a href="javascript:void(0);" title="Upload Video Thumbnail Image"
                               class="upload-thumb-image" data-croppie data-croppie-file="#croppie-video-thumb-file"
                               data-croppie-progress="#upload-photo-progress-1"
                               data-croppie-input="#croppie-video-thumb-input"
                               data-croppie-output="#croppie-video-thumb-preview" data-croppie-bind="file"
                               data-croppie-viewport='{"width": 600, "height": 300}'
                               data-croppie-store="public/uploads/video/thumbs/"
                               data-croppie-boundary='{"width": 600, "height": 300}'>Browse Thumbnail</a>

                            <a href="javascript:void(0)" class="video-thumb-img"><img
                                        src="{{asset('public'.$tempvideo->thumb_path)}}"
                                        id="croppie-video-thumb-preview"/></a>
                        </div>
                    </div>
                    <canvas id="canvas"></canvas>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="u-form">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="video-title">Video Title</label>
                                    {{ Form::text('title', null, ['id' => 'video-title', 'close' => 'form-control', 'placeholder' => 'Rocket League Pro Championship Gameplay (36 characters remaining)', 'required' => true]) }}
                                    <span class="error" id="videoTitleError"></span>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="video-description">About</label>
                                    {{ Form::textarea('about', null, ['id' => 'summernote', 'class' => 'form-control', 'rows' => '3', 'required' => true, 'placeholder' => 'Description']) }}
                                    <span class="error" id="videoDescriptionsError"></span>
                                </div>
                            </div>


                        </div>

                        <div class="row">
                            <!-- <div class="col-lg-3">
								<div class="form-group">
									<label for="video-feature">Features</label>
									{{ Form::select('feature', unserialize(VIDEO_FEATURES), null, ['class' => 'form-control', 'id' => 'video-feature', 'required' => true]) }}

                            </div>
                        </div> -->

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="e1">Video Tags</label>
                                    {{ Form::text('tags', null, ['id' => 'video-tags', 'close' => 'form-control', 'required' => true, 'placeholder' => 'Select mulitple tags']) }}
                                    <span class="error" id="videoTagsError"></span>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="video-subtitle">Subtitles</label>
                                    {{ Form::select('sub_title', unserialize(VIDEO_SUBTITLES), null, ['class' => 'form-control', 'id' => 'video-subtitle', 'required' => true]) }}
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="video-language">Language</label>
                                    {{ Form::text('language', null,['class' => 'form-control', 'placeholder' => 'Choose Language', 'id' => 'video-language'])}}
                                    <span class="error" id="videoLangugaeError"></span>
                                </div>
                            </div>
                            <!-- <div class="col-lg-3">
                            <div class="form-group">
                                <label for="video-ps">Privacy Settings</label>
                                {{ Form::select('privacy_setting', unserialize(VIDEO_PRIVACY_SETTINGS), null, ['class' => 'form-control', 'id' => 'video-ps', 'required' => true]) }}
                            </div>
                        </div>                         -->
                        </div>
                        <div class="row ">
                            <div class="col-lg-12 u-category">Category ( you can select upto 2 categories )</div>
                            <div class="categoryerror"></div>
                        </div>

                        <div class="row">
                            <!-- checkbox 1col -->
                            @if(!empty($videoCategories))
                                @foreach($videoCategories as $catId => $catName)
                                    <div class="col-lg-3 col-xs-6">
                                        <div class="checkbox">
                                            <label>
                                                <label class="checkbox">
                                                    {{ Form::checkbox('categories[]', $catId)}}
                                                    <span class="arrow"></span>
                                                </label> {{ $catName }}
                                            </label>
                                        </div>
                                    </div>

                                @endforeach

                            @endif

                        </div>
                    </div>
                    <div class="u-area mt-small">
                        {{ Form::button('Save', ['type' => 'submit', 'class' => 'btn btn-primary u-btn uploadVideo'])}}
                    </div>
                    <div class="u-terms">
                        <p>By submitting your videos to circle, you acknowledge that you agree to circle's <a href="#">Terms
                                of Service</a> and <a href="#">Community Guidelines</a>.</p>
                        <p class="hidden-xs">Please be sure not to violate others' copyright or privacy rights. Learn
                            more</p>
                    </div>
                </div>
            </div>
        </div>

        {{ Form::close()}}
    </div>


    <!--End Update Video Part-->

    <style>


        #progress {
            height: 6px;
            display: block;
            width: 0%;
            border-radius: 2px;
            background: -moz-linear-gradient(center top, #13DD13 20%, #209340 80%) repeat scroll 0 0 transparent; /* IE hack */
            background: -ms-linear-gradient(bottom, #13DD13, #209340); /* chrome hack */
            background-image: -webkit-gradient(linear, 20% 20%, 20% 100%, from(#13DD13), to(#209340)); /* safari hack */
            background-image: -webkit-linear-gradient(top, #13DD13, #209340); /* opera hack */
            background-image: -o-linear-gradient(#13DD13, #209340);
            box-shadow: 3px 3px 3px #888888;
        }

        .preview {
            border: 1px solid #CDCDCD;
            width: 450px;
            padding: 10px;
            height: auto;
            overflow: auto;
            color: #4D4D4D;
            float: left;
            box-shadow: 3px 3px 3px #888888;
            border-radius: 2px;
        }

        .percents {
            float: right;
        }

        .upload-progress {
            display: none;
        }

        #video-thumb-image {
            display: none;
        }

        .upload-thumb-image {
            padding: 12px 35px 10px 35px;
            border: 2px dotted #ccc;
            font-size: 12px;
            color: #999;
            margin: 5px;
        }

        #thumbnail_image img {
            max-width: 150px;
            max-height: 120px;
            margin: 0px;
        }

        .video-thumb-img {
            position: relative;
            display: inline-block;
            margin: 0px 5px;
        }

        .delete-video-thumb {
            position: absolute;
            right: 0px;
            color: #f72e5e;
            background-color: #fff;
        }


        #upload-photo-progress-1 .progress {
            height: 8px;
        }

        #upload-photo-progress-1 .progress-bar {
            height: 8px;
            background-color: #28b47e !important;
        }


        @media only screen and (max-width: 767px) {
            .u-details {
                padding: 0 20px 15px 20px;
                background: #fff;
                margin-top: 20px;
            }

            .upload-thumb-image {
                width: 100%;
                display: block;
                text-align: center;
            }

            .video-thumb-img {
                margin: 16px auto 1px;
                display: block;
                width: 150px;
            }

            .margin-top-reduce {
                margin-top: 20px !Important;
            }


            #croppie-modal-0 .cr-viewport.cr-vp-square {
                width: 340px !Important;
                height: 240px !Important;
            }

            #croppie-modal-0 .cr-boundary {
                width: 340px !Important;
                height: 240px !Important;
            }

            #croppie-modal-0 .cr-viewport.cr-vp-square {
                width: 305px !Important;
                height: 185px !Important;
            }

            #croppie-modal-0 .cr-boundary {
                width: 305px !Important;
                height: 185px !Important;
            }

        }
    </style>
    {{ HTML::style('public/css/bootstrap-tagsinput.css')}}
    {{ HTML::script('public/js/jquery.validate.js')}}
    {{ HTML::script('public/js/bootstrap-tagsinput.js')}}
    {{ HTML::script('public/js/bootstrap3-typeahead.js')}}

    <!-- Owl Carousel -->
    {{ HTML::script('public/js/jquery.easytabs.min.js') }}
    {{ HTML::script('public/js/owl.carousel.js') }}
    {{ HTML::script('public/js/only_slider.js') }}
    <!-- Owl Carousel -->
    <script type="text/javascript">
        $(document).ready(function () {
            var myvid = document.getElementById('myvid');

            document.getElementById('duration').value = isNaN(document.getElementById('myvid').duration) ? 0 : document.getElementById('myvid').duration

            $("i.cvicon-cv-upload-videoa").click(function () {
                $("#uploadVideoFile").trigger('click');
                $('input[type="file"]').change(function (e) {
                    var fileName = e.target.files[0].name;
                    $('#dropbox').prepend(fileName);
                    $('.cvicon-cv-upload-video').css('color', '#f72e5e');
                });
            });

            $("#uploadVideo").on("submit", function (e) {
                const title = $(this).find("[name='title']");
                const val = title.val().trim();

                if (val == "" || typeof val == "undefined") {
                    e.preventDefault();
                    $("#videoTitleError").text("Title must not be empty");
                } else if (val != "" && val.length > 100) {
                    $("#videoTitleError").text("Title must not be more than 100");
                    $('html, body').animate({
                        scrollTop: $("#videoTitleError").offset().top
                    }, 500)
                    e.preventDefault();
                    return false;
                } else {
                    $("#videoTitleError").text("");
                }
            })

            $('#submit-video').on('click', function (e) {
                var fileData = $('#uploadVideoFile').prop('files');
                if (validateFile(fileData)) {
                    $('#upload-video-step1').hide(0);
                    $('#upload-video-step2').show(0);
                    handleFiles(fileData, "", "#croppie-video-thumb-input", "#croppie-video-thumb-preview", true);
                }
            });

            $('#cancel_upload').on('click', function (e) {
                $('#upload-video-step1').show(0);
                $('#upload-video-step2').hide(0);
            });

            $('.u-area .uploadVideo').on('click', function () {
                var error = '';
                if ($('.u-form .col-lg-3 .checkbox').hasClass('checked')) {
                    $('.categoryerror').html('');
                } else {
                    $('.categoryerror').html('<label class="text-danger">Category can not blank.</label>');
                    error = 1;
                }


                if ($('input#video-title').val() == '') {
                    $('span#videoTitleError').show();
                    $('span#videoTitleError').html('This field is required');
                    error = 1;
                } else {
                    $('span#videoTitleError').html('');
                }

                if ($('input#video-tags').val() == '') {
                    $('span#videoTagsError').show();
                    $('span#videoTagsError').html('This field is required');
                    error = 1;
                } else {
                    $('span#videoTagsError').html('');
                }

                if ($('textarea#video-description').val() == '') {
                    $('span#videoDescriptionsError').show();
                    $('span#videoDescriptionsError').html('This field is required');
                    error = 1;
                } else {
                    $('span#videoDescriptionsError').html('');
                }

                if ($('input#video-language').val() == '') {
                    $('span#videoLangugaeError').show();
                    $('span#videoLangugaeError').html('This field is required');
                    error = 1;
                } else {
                    $('span#videoLangugaeError').html('');
                }

                if (error) {
                    return false;
                } else {
                    return true;
                }
            });
            $("#uploadVideo").validate({
                rules: {
                    tags: {
                        required: true,
                        maxlength: 50
                    }
                }
            });

            $(".upload-thumb-image").click(function () {
                $("#video-thumb-image").trigger('click');
            });

            $('#video-tags').tagsinput({
                typeahead: {
                    source: function (query) {
                        //return $.get('http://someservice.com');
                    }
                }
            });

            $('#video-language').tagsinput({
                typeahead: {
                    source: ['English', 'Hindi', 'Tamil', 'Bengali', 'Gujarati', 'Punjabi']
                },
                freeInput: true
            });
        });
    </script>
@endsection