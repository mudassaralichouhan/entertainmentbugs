@extends('layouts.home')
@section('content')
 

<div class="content-wrapper upload-page edit-page" style=" background:#eaeaea !important;padding-top:20px">
    <div class="container" style="background:#fff;">
        <div class="row">
            <div class="col-lg-12">
                   {!! Form::open(['route' => 'story.store','id' => 'photo-upload', 'enctype'=>'multipart/form-data']) !!}
                    {{ Form::hidden('video', null, ['id' => 'selected-video-name']) }}
                    {{ Form::hidden('selected_thumb', null, ['id' => 'video-selected-thumbnail']) }}
                    
                <div class="u-form">
                    <div class="rows">
					<br><br>
                        <div class="col-lg-6">
                            <div class="form-group">
                                 
                                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                    {!! Form::label('Photo Title:') !!}
                                    {!! Form::text('title', old('title'), ['class' => 'form-control','id' => 'e1', 'required' => true,
                                    'placeholder' => 'Rocket League Pro Championship Gameplay (100 characters remaining)']) !!}
                                    <span class="text-danger">{{ $errors->first('title') }}</span>
                                </div>
                             </div>
                        </div>
                         						 <div class="col-lg-6">
                            <div class="form-group">
                                
                                 <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
                                    {!! Form::label('Tag Followers (upto 5):') !!}
                                    {!! Form::text('tags', old('tags'), ['class' => 'form-control', 'required' => true,
                                    'placeholder' => 'Search', 'id' => 'photo-tags']) !!}
                                    <span class="text-danger">{{ $errors->first('tags') }}</span>
                                </div>
                                
                                
                                 
                            </div>
                        </div>
						 <div class="col-lg-6">
                            <div class="form-group">
                                
                                 <div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}">
                                    {!! Form::label('Hashtags:') !!}
                                    {!! Form::text('hashtags', old('hashtags'), ['class' => 'form-control', 'required' => true,
                                    'placeholder' => 'Select mulitple tags', 'id' => 'photo-language']) !!}
                                    <span class="text-danger">{{ $errors->first('tags') }}</span>
                                </div>
                                
                                 
                            </div>
                        </div>
						 
					
						 <div class="col-lg-12">
                            <div class="form-group">
                          
										
										<div class="u-close" style="float:left; margin-right:20px;">
										  <img src="https://marketifythemes.net/html/tokyo/img/portfolio/2.jpg" style="width:150px;height:150px;object-fit:cover;margin-right:2px;">
											<a href="#" style="margin-top: 2px;display: block;float: right;margin-left: 8px;">
											<i class="cvicon-cv-cancel"></i></a>
                                        </div>
										
										<div class="u-close" id="select-p" style="float:left; margin-right:20px;">
											<div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i>
											</div>  	<a href="" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                        </div>
										<div class="u-close" id="select-p" style="float:left; margin-right:20px;">
											<div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i></div>  
											<a href="" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a>
                                        </div>
										<div class="u-close" id="select-p" style="float:left; margin-right:20px;">
											<div class="browse-photo"><i class="fa fa-picture-o" aria-hidden="true"></i></div> 	
											<a href="" style="text-align:Center;display:block;margin-top:9px;">Browse photo</a> 
                                        </div>
										 
										<div style="clear:both; overflow:hidden"></div>
										<br>
									<a href="" style="background: #637076;color: #fff;padding: 6px 15px 2px 17px;font-size: 12px;">Add More photos grid</a>
                            </div>
                        </div> 
                    </div>

				
	
	<Style>
.edit-page .u-area {text-align: center;padding-top: 12px;padding-bottom: 20px;}
#select-p i{color: #637076;font-size: 66px;text-align: center;display: block;padding: 38px 0 0 0;}
.upload-page .u-area i {font-size: 90px;color: #637076;}
.upload-page .u-area .u-text1 {margin-top: 10px;margin-bottom: 9px;font-size: 14px;}
.browse-photo{width:150px; height:150px; background:#eceff0}
	</style>
	
		 
 
 
                </div>
                {!! Form::close() !!}
	<div style="clear:both; overflow:hidden"></div>
                <div class="u-area mt-small">
                    <form action="#" method="post">
                        <button class="btn btn-primary u-btn">Upload</button>
                    </form>
                </div>
               
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
{{ HTML::style('public/css/bootstrap-tagsinput.css')}}
{{ HTML::script('public/js/bootstrap-tagsinput.js')}}
{{ HTML::script('public/js/bootstrap3-typeahead.js')}}

    <script type="text/javascript">
        $(document).ready(function() {

            $(".upload-cover-image").click(function () {
              $("#profile-cover-image").trigger('click');
            });

            $(".upload-thumb-image").click(function () {
              $("#profile-thumb-image").trigger('click');
            });

            $('#photo-tags').tagsinput({
                typeahead: {
                    source: function(query) {
                        //return $.get('http://someservice.com');
                    }
                }
            });

            $('#photo-language').tagsinput({
                typeahead: {
                    source: function(query) {
                        //return $.get('http://someservice.com');
                    }
                },
                freeInput: true
            });
        });

        /*Validate Thumb Image File*/
        var fileCount = 0;
        validateThumbFile = function(files,e){

                fileCount++;
                if(fileCount == 1){

                    var flag = true;
                    var imageType = /image.*/;  
                    var file = files[0];
                    // check file type
                    if (!file.type.match(imageType)) {  
                    alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
                    flag = false;
                    return false;	
                    } 
                    // check file size
                    if (parseInt(file.size / 1024) > (1024*2)) {  
                    alert("File \""+file.name+"\" is too big.");
                    flag = false;
                    return false;	
                    } 
                    
                    if(flag == true){
                        uploadImageFile(file);
                    }else{
                        return flag;
                    }	
                }
                
            };
            
            /*Upload Image in Temp Folder*/
            uploadImageFile = function(file){
            var formdata = new FormData();
            formdata.append("image", file);	
            //console.log(file);
            xhr = new XMLHttpRequest();
            //console.log(xhr);
            xhr.open("POST", "upload/temp_image");
            xhr.onload  = function() {
                var jsonResponse = xhr.response;
                console.log(jsonResponse);
                result = JSON.parse(jsonResponse);
                console.log(result);
                if(result.status == true){
                    var fl = $('#cover_image').find('input[type=hidden]').length;
                    fl++;
                    var fNameHtml = '<input name="covers['+fl+']" type="hidden" required="required" id="video-thumb-'+fl+'" value="'+result.file+'" />';
                    var imageHtml = '<a href="javascript:void(0)" key="'+fl+'" class="video-thumb-img"><img src="'+result.image+'" /><span class="delete-video-thumb"><i class="cvicon-cv-cancel"></i><span></a>';
                    $('#cover_image').append(fNameHtml);
                    $('.upload-cover-image').after(imageHtml);
                }else{
                    alert('File is not uploading. Please try again.')
                }	  
            };	
            var csrfToken = $('#video-upload').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken); 	
            xhr.send(formdata);		
        }

         /*Validate Thumb Image File*/
         validateThumbFile1 = function(files,e){
                var flag = true;
                var imageType = /image.*/;  
                var file = files[0];
                // check file type
                if (!file.type.match(imageType)) {  
                alert("File \""+file.name+"\" is not a valid image, Please upload only jpg,png and gif image");
                flag = false;
                return false;	
                } 
                // check file size
                if (parseInt(file.size / 1024) > (1024*2)) {  
                alert("File \""+file.name+"\" is too big.");
                flag = false;
                return false;	
                } 
                
                if(flag == true){
                    uploadImageFile1(file);
                }else{
                    return flag;
                }	
                
            };
            
            /*Upload Image in Temp Folder*/
            uploadImageFile1 = function(file){
            var formdata = new FormData();
            formdata.append("image", file);	
            //console.log(file);
            xhr = new XMLHttpRequest();
            //console.log(xhr);
            xhr.open("POST", "upload/temp_image");
            xhr.onload  = function() {
            var jsonResponse = xhr.response;
            console.log(jsonResponse);
            result = JSON.parse(jsonResponse);
            console.log(result);
            if(result.status == true){
                var fl = $('#thumb_image').find('input[type=hidden]').length;
                fl++;
                var fNameHtml = '<input name="thumbs['+fl+']" type="hidden" required="required" id="video-thumb-'+fl+'" value="'+result.file+'" />';
                var imageHtml = '<a href="javascript:void(0)" key="'+fl+'" class="video-thumb-img"><img src="'+result.image+'" /><span class="delete-video-thumb"><i class="cvicon-cv-cancel"></i><span></a>';
                $('#thumb_image').append(fNameHtml);
                $('.upload-thumb-image').after(imageHtml);
            }else{
                alert('File is not uploading. Please try again.')
            }	  
            };	
            var csrfToken = $('#video-upload').find('input[name=_token]').val();
            xhr.setRequestHeader('X-csrf-token', csrfToken); 	
            xhr.send(formdata);		
        }
        
        /*Delete Video Thumb Image*/
        $(document).on('click', '.delete-video-thumb', function(){
            fileCount--;
            var key = $(this).parent('a:first').attr('key');
            
            /*Ajax Request to remove file in temp folder*/
            $(this).parent('a:first').remove();
            $('#video-thumb-'+key).remove();
                
        });

    </script>

<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script>
 CKEDITOR.replace('about');
</script>

@endsection