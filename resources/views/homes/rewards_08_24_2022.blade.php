@extends('layouts.home')
@section('content')
<style>
 a.frst1 i{color: #f72e5e !important;}
#rewards_main_page {padding: 40px 0 10px 0}
#rewards_main_page ul li {font-size: 24px;line-height: 64px;}
#reward_1 .b-video .v-img {height: 105px;overflow: hidden;}
#reward_1 .v-img img { height: 100%;object-fit: cover;}
#mobile-menu-design ul.list-inline{display:none;}
.content-block .cb-header {padding: 20px 0 10px 0;}
@media only screen and (max-width: 767px){
#mobile-menu-design ul.list-inline{display:block;}
#mobile-menu-design{margin-top:15px;}
#mobile-menu-design ul.list-inline{width:73%;    display: inline-block;}
#mobile-menu-design ul.list-inline li{width:auto;margin-right:3px;}

}
</style>


    <div class="content-wrapper">

        <div class="container">
         <img src="{{ url('/public/img/rewards_banner.jpg') }}" style="width:100%">

            <div class="row">
                <div class="col-lg-12" id="reward_1">
                    <!-- Featured Videos -->
                    <div class="content-block">


                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">

                                        <li>
                                            <a href="#" class="color-active">
                                                <span class="visible-xs"> All Rewards video </span>
                                                <span class="hidden-xs"> All Rewards  video </span>
                                            </a>
                                        </li>
                                        <li><a href="{{ URL::to('rewards/most-watched') }}">Most watched </a></li>
                                        <li class="hidden-xs"><a href="#"> </a></li>

                                        <li> <a href="participate-video" class="promote_your_video1" style="background:#28b47e;"> Participate Now</a> </li>
                                        <li> <a href="all-top-entertainer" class="promote_your_video1" style=" background:#28b47e;"> Top
                                                Entertainer List</a> </li>
                                  
                                            <li style="margin-left:10px;"><a href="">Previous Week  </a></li> | 
                                             <li><a href="">Last Month  </a></li>
                                    </ul>
                               
                                
                                
                                </div>
                                
                                 <div class="col-lg-2 col-sm-2 col-xs-12" id="mobile-menu-design">
                                     
                                     
                                         <ul class="list-inline">

                                        <li>  <a href="{{ URL::to('rewards') }}" class="color-active">
                                                <span class="visible-xs"> All Rewards video </span>  </a> </li>
                                        <li><a href="{{ URL::to('rewards/most-watched') }}">Most watched </a></li>
                                       
                                    </ul>
                                    
                                    
                                <div class="btn-group pull-right bg-clean">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Sort by <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a onclick="rewardVideoSortBy('recent')"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                        <li><a onclick="rewardVideoSortBy('top_viewed')"><i class="cv cvicon-cv-view-stats"></i> Top Viewed</a></li>
                                        <li><a onclick="rewardVideoSortBy('top_liked')"><i class="fa fa-thumbs-up"></i> Top liked</a></li>
                                        <li><a onclick="rewardVideoSortBy('longest')"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        
                        
                          <div class="clearfix"></div>
                        
                                
                            </div>
                        </div>

                        <div class="cb-content videolist" id="home_page_video">
                            @if (!empty($rewardVideos))
                                <div id="allRewardVideos">
                                    @include('elements.home_sorting_videos.all_reward_video')                  
                                </div>

                            @endif
                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>

            <!-- pagination -->
            <!--<div class="v-pagination">-->
            <!--    <ul class="list-inline">-->
            <!--        <li class="v-pagination-prev"><a href="#"><i class="cv cvicon-cv-previous"></i></a></li>-->
            <!--        <li class="v-pagination-first"><a href="#">1</a></li>-->
            <!--        <li><a href="#">2</a></li>-->
            <!--        <li><a href="#">3</a></li>-->
            <!--        <li><a href="#">4</a></li>-->
            <!--        <li><a href="#">5</a></li>-->
            <!--        <li><a href="#">...</a></li>-->
            <!--        <li><a href="#">10</a></li>-->
            <!--        <li class="v-pagination-skin visible-xs"><a href="#">Skip 5 Pages</a></li>-->
            <!--        <li class="v-pagination-next"><a href="#"><i class="cv cvicon-cv-next"></i></a></li>-->
            <!--    </ul>-->
            <!--</div>-->
            <!-- /pagination -->

        </div>
    </div>
    <script> 
        function rewardVideoSortBy(order_by){
            $.ajax({
                type: "GET",
                url: "{{ URL::to('show_all_reward_order_by') }}",
                data: {'keyword':order_by},
                cache: false,
                success: function(response)
                {
                    $('#allRewardVideos').empty().html(response);
                }
            });
        }
    </script>
@endsection
