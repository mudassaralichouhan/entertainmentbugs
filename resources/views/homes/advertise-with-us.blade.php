@extends('layouts.home')

@section('meta_tags')
    <meta name="description" content="Grow your business and reach maximum audience with us. Connect to more clients and customers with our affordable advertising plans.">
    <meta name="keywords" content="Promote Video, Advertisement, Advertising, Marketing, Digital marketing, Branding, Advertising Agency , Socialmedia, Social media marketing, Marketing digital, Advertisements, Brand Promotion, Marketing Strategy">
    <meta name="author" content="<?php echo ((isset($author) ? $author : 'Advertise with us - Entertainment Bugs')); ?>">
    <meta property="og:site_name" content="{{URL::to('/')}}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="<?php echo ((isset($ogtitle) ? $ogtitle : 'Advertise with us - Entertainment Bugs')); ?>"/>
    <meta property="og:description" content="<?php echo ((isset($ogdescription) ? $ogdescription : 'Grow your business and reach maximum audience with us. Connect to more clients and customers with our affordable advertising plans.')); ?>"/>
    <meta name="theme-color" content="#f82e5e">
    <meta property="og:keywords" content="Promote Video, Advertisement, Advertising, Marketing, Digital marketing, Branding, Advertising Agency , Socialmedia, Social media marketing, Marketing digital, Advertisements, Brand Promotion, Marketing Strategy">
    <meta property="og:url" content="<?php echo url()->current(); ?>"/>
    <meta property="og:image" content="<?php echo ((isset($ogimage) ? $ogimage : 'https://www.entertainmentbugs.com/public/uploads/logo/logo-social-media.png')); ?>"/><!--profile image-->
    
   @endsection
   
 
@section('content')
 <link href="https://www.entertainmentbugs.com/public/css/casting.css" rel="stylesheet">
<style>
.container.mobile-extra_fea{display:none;}
#spacer-extra{display:none}
.intro-banner {padding:200px 0 100px 0;position: relative}
.intro-banner.big-padding {padding: 130px 0}
.intro-banner .container {z-index: 100;position: relative}
.intro-banner:after,
.intro-banner:before {content: "";position: absolute;height: 100%;width: 100%;display: block;top: 0;left:0px;z-index: 15;background:rgba(0,0,0,0.2);}
.intro-banner:after {z-index: 5;background-color: #fafafa}
.intro-banner .background-image-container {background-size: cover;background-repeat: no-repeat;background-position: 100% 60%;position: absolute;top: 0;left: 0;height: 100%;width: 100%;left: 0%;z-index: 10;overflow: hidden}
.intro-banner.disable-gradient:before {background: #f2f2f2;opacity: .85}
.intro-banner.disable-gradient .background-image-container {left: 0;width: 100%}
.banner-headline {display: block;max-width: 55%}
.banner-headline-alt {margin: -11px 0 0 0px;display: block}
.banner-headline-alt h3 {font-family: 'Hind Guntur', sans-serif;font-size: 48px;line-height: 49px;font-weight: 600;width: 66%;color: #fff;margin: 0 auto 10px;text-align: center;}
.banner-headline-alt p{ font-family: 'Hind Guntur', sans-serif;color: #565656;width: 42%;margin:0 auto 10px;line-height: 23px;color:#fff;  text-align:center;font-size: 15px;}
.banner-headline-alt  a {background: #f72e5e !important;border: 2px solid #f72e5e !important;color: #fff !important;margin:13px auto 0;width: 200px;padding: 13px 25px 10px 23px;display: block;text-align: center;font-size: 14px;}
.intro-banner .background-image-container {background-size: cover !important;} 
.banner-video {height: 560px;position: relative;overflow: hidden;}
#production-home-video {height: 118vh;top: -31%;}
#live-video-user-list-home {display: none;}
#production-home-video{width:100%;position:absolute;left:0px; z-index:99; left:0px; }
.banner-video{height:82vh;position:relative;}
.banner-video .intro-banner{height:100%;}
.banner-video .intro-banner:after, .banner-video .intro-banner:before {content: "";position: absolute;height: 100%;width: 100%;display: block;top: 0;left: 0px;z-index: 99;background: rgba(0,0,0,0.5);}
#production-home-video {height: 118vh;top: -31%;}

</style>
   

<div class="banner-video">
        <div class="intro-banner main_banner">
 
            <video id="production-home-video" autoplay="" loop="" muted="" playsinline="">
                <source src="https://www.entertainmentbugs.com/public/web_video/advertise-with-us-video.mp4" type="video/mp4">
                <source src="movie.ogg" type="video/ogg">
                Your browser does not support the video tag.
            </video>

            <div class="container" style="position:relative;z-iindex:999">
                <div class="row">
                    <div class="col-md-12">
                        <br>
                        <div class="banner-headline-alt">
                            <h3>Want to grow your business? Promote your business with us </h3>
                            <p>Grow your business and reach maximum audience with us. Connect to more clients and customers with our affordable advertising plans.</p>

<a href="{{URL::to('create-new-ads-campaign')}}" class="button" style="border-color:#6c146b; color:#6c146b;width: 200px;">
                               Start a campaign</a>
                                                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    
    
    
    
    
    
    
<div class="well-none" id="third-b2" style="border-top: 0px solid #6f116c;  background:#f4f3f3">
        <div class="container">
            <div class="text-content white-font" style="padding:80px 0px 65px 0;">
                <div class="row">
                    <div id="bl0" class="col-lg-6 col-md-6 col-sm-12" style="position:relative;padding: 0 60px; float:left">
                        <div class="small-banner" style="position:relative">
                            <img src="https://www.entertainmentbugs.com/public/web_img/camera-04.jpg" style="width: 36.5%;margin-bottom: 10px;">
                            <img src="https://www.entertainmentbugs.com/public/web_img/camera-01.jpg" style="width: 62%;float: right;margin-bottom: 10px;">
                        </div>
                        <div class="small-banner" style="position:relative">
                            <img src="https://www.entertainmentbugs.com/public/web_img/camera-02.jpg" class="no-dis" style="width: 62%;float: left;margin-bottom: 10px;">
                            <img src="https://www.entertainmentbugs.com/public/web_img/camera-03.jpg" class="no-dis" style="width: 36.5%;margin-bottom: 10px;float: right;">
                        </div> 
                    </div>
                    <div id="new5" class="col-lg-6 col-md-6 col-sm-12" style="padding:60px 20px 60px 0px ">
                        <h3 style="font-size: 22px;font-weight: 600;margin: 0 0 -13px 0;padding: 0;">Why Us</h3>
                        <h2 style="color:#000;width:100%">Get the results that matter</h2>
                        <p> Join &amp; Create your Artist Profile and Submit your Artworks to the Global Community of Art Space.
                            Iterative things to strategy foster collaborative thinking. Leverage agile frameworks to provide
                            a robust synopsis for high level overviews. Leverage agile frameworks to provide a robust
                            synopsis for high level overviews.</p>
                        <a href="#" class="button button-sliding-icon ripple-effect big margin-top-20" style="border-color:#6c146b; color:#6c146b;width: 154.875px;">Start a campaign </a>
                    </div>

                    <!-- Infobox / End -->
                </div>
                <div style="clear:both; overflow:hidden"></div>
            </div>
        </div>
    </div>
    
    
    
    
    
    
    
    
           <style>
           .main-bl{clear:both; overflow:hidden;}
           .main-bl-new4{width:20%; float:left}
          #four_block h3{margin: 0;padding: 23px 0 19px 0; font-size:16px;text-align:center}
           </style>
    
          <div class="container" id="four_block">
            <div class="main-bl">
                <div class="main-bl-new4"><h3>Build brand awareness</h2>
                </div>
                 <div class="main-bl-new4"><h3>Higher Conversion</h3>
                </div>
                 <div class="main-bl-new4"><h3>Long term results</h3>
                </div>
                 <div class="main-bl-new4"><h3>Generate Leads</h3>
                </div>
                  <div class="main-bl-new4"><h3>Promote Brands</h3>
                </div>
            </div>
        </div>
    
    
    <Style>
        #bl0232{perspective-origin: 0;
    perspective: 1500px;
    transform-style: preserve-3d;}
        #bl0232 img{width:100%;backface-visibility: hidden;
    transform: rotateY(-35deg) rotateX(15deg);margin-top:30px;
box-shadow: 25px 60px 125px -25px rgba(80,102,144,.1), 16px 40px 75px -40px rgba(0,0,0,.2);}
   
   .margin-l12{margin-left:20px;}
   .margin-l12.extra12{margin-top:15px;}
    </Style>
    
    
    <div class="well-none" id="third-b2" style="border-top: 0px solid #6f116c;   background: #f4f3f3;">
        <div class="container">
            <div class="text-content white-font" style="padding:40px 0px 45px 0;">
                <div class="row">
                    <div id="bl0232" class="col-lg-6 col-md-6 col-sm-12" style="position:relative;padding: 0 60px; float:right">
 <img src="https://www.entertainmentbugs.com/public/web_video/advertiser-reporting.jpg">
                    </div>
                    <div id="new5" class="col-lg-6 col-md-6 col-sm-12" style="padding:20px 20px 60px 30px ">
                        <h2 style="color:#000;width:100%">Detailed and transparent reporting</h2>
                        <p class="margin-l12 extra12"> <strong> Live up-to-date reporting</strong> 
Our reporting details where and when your ads are shown: both geographically and on which publishers on our network.</p>

  <p class="margin-l12"><strong>Breakdown results by ad or campaign</strong> 
See performance metrics for each ad so you can split test and improve your results or look at results month-over-month or one campaign over another.
                        <a href="pages-pricing-plans.html" class="button button-sliding-icon ripple-effect big margin-top-20" style="border-color:#6c146b; color:#6c146b;width: 154.875px;">Get Started <i class="icon-material-outline-arrow-right-alt"></i></a>
                    </div>

                    <!-- Infobox / End -->
                </div>
                <div style="clear:both; overflow:hidden"></div>
            </div>
        </div>
    </div>
    
    
    
    
    
@endsection

   