@extends('layouts.home')


@section('meta_tags')
  
    <meta name="description" content="Create and win the rewards for your efforts.">
    <meta name="keywords" content="Reward, Rewards Videos, Participate Rewards, All Rewards Videos, Earn Rewards">
    <meta name="author" content="<?php echo ((isset($author) ? $author : 'Rewards - Entertainment Bugs')); ?>">
    <meta property="og:site_name" content="{{URL::to('/')}}"/>
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="<?php echo ((isset($ogtitle) ? $ogtitle : 'Rewards - Entertainment Bugs')); ?>"/>
    <meta property="og:description" content="<?php echo ((isset($ogdescription) ? $ogdescription : 'Create and win the rewards for your efforts.')); ?>"/>
    <meta name="theme-color" content="#f82e5e">
    <meta property="og:keywords" content="Reward, Rewards Videos, Participate Rewards, All Rewards Videos, Earn Rewards">
    <meta property="og:url" content="<?php echo url()->current(); ?>"/>
    <meta property="og:image" content="<?php echo ((isset($ogimage) ? $ogimage : 'https://www.entertainmentbugs.com/public/uploads/logo/logo-social-media.png')); ?>"/><!--profile image-->
    
   @endsection
   
   
   
@section('content')
<style>
 a.frst1 i{color: #f72e5e !important;}
#rewards_main_page {padding: 40px 0 10px 0}
#rewards_main_page ul li {font-size: 24px;line-height: 64px;}
#reward_1 .b-video .v-img {height: 92px;overflow: hidden;}
#reward_1 .v-img img { height: 100%;object-fit: cover;}
#mobile-menu-design ul.list-inline{display:none;}
#reward_1 .content-block .cb-header {background: #fff;margin:0 0  10px 0;padding:10px 0 12px 20px;}
body.light .content-wrapper {background-color: #f4f3f3;padding: 10px 0 0 0;}
#home_page_video .v-desc a {    font-size: 12px;display: block;line-height: 14px;height: 35px;margin-bottom: 11px;}
.b-video .v-views {font-size: 10.5px;color: #7e7e7e;padding-top: 0px;}
.b-video {margin-bottom: 18px;}

@media only screen and (max-width: 767px){
#mobile-menu-design ul.list-inline{display:block;}
#mobile-menu-design{argin-top: 8px;margin-bottom: -8px;}
#mobile-menu-design ul.list-inline{width:73%;    display: inline-block;}
#mobile-menu-design ul.list-inline li{width:auto;margin-right:3px;}
.content-block .cb-header .list-inline {margin-left: -11px;line-height: 34px;margin-right: 6px;}
    
}


a.blinko {
  -webkit-animation: NAME-YOUR-ANIMATION 1s infinite;  /* Safari 4+ */
  -moz-animation: NAME-YOUR-ANIMATION 1s infinite;  /* Fx 5+ */
  -o-animation: NAME-YOUR-ANIMATION 1s infinite;  /* Opera 12+ */
  animation: NAME-YOUR-ANIMATION 1s infinite;  /* IE 10+, Fx 29+ */background-color:#ff2a5d;
  }

@-webkit-keyframes NAME-YOUR-ANIMATION {
  0%, 49% {
    background-color:#cd214a;
  }
  50%, 100% {
    background-color: #ff2a5d;
  }
}


</style>


    <div class="content-wrapper">

        <div class="container">
         <!--<img src="{{ url('/public/img/rewards_banner.jpg') }}" style="width:100%">-->

            <div class="row">
                <div class="col-lg-12" id="reward_1">
                    <!-- Featured Videos -->
                    <div class="content-block">


                        <div class="cb-header">
                            <div class="row">
                                <div class="col-lg-10 col-sm-10 col-xs-8">
                                    <ul class="list-inline">

                                        <li>
                                            <a href="{{ URL::to('/rewards') }}" class="color-active">
                                                <span class="visible-xs"> All Rewards video </span>
                                                <span class="hidden-xs"> All Rewards  video </span>
                                            </a>
                                        </li>
                                        <li><a href="{{ URL::to('/rewards/most-watched') }}">Most watched </a></li>
                                        <!--<li class="hidden-xs"><a href="#"> </a></li>-->
<!--<li class=""><a href="{{ url('/rewards') }}" id="oneweek" class="promote_your_video1" style=" background: {{ $selected != "second" ? '#1769d4' : '#28b47e' }}; padding-left:10px; padding-right:10px">Current Week</a></li>-->
                                        <!--<li class=""><a href="{{ url('rewards/second') }}" id="twoweek" class="promote_your_video1" style="background: {{ $selected == "second" ? '#1769d4' : '#28b47e' }}; padding-left:10px; padding-right:10px">Previous Week</a></li>-->
                                   
                                    <li><a href="{{ url('rewards/second') }}" target="_blank">Previous Week </a></li>
                                     <li class=""></li>
                                          <li> <a href="{{ url('all-top-entertainer') }}" class="promote_your_video1" style=" background:#28b47e;"> Top Entertainer List</a> </li>
                                        
                                      <li> <a href="{{ url('/participate-video') }}" class="promote_your_video1 blinko" style="background:#28b47e;"><i class="cv cvicon-cv-upload-video"></i>&nbsp Participate Now</a> </li>
                                      
                                         </ul>
                                </div>
                                 <div class="col-lg-2 col-sm-2 col-xs-12" id="mobile-menu-design">
                                         <ul class="list-inline">
                                        <li>  <a href="{{ URL::to('rewards') }}" class="color-active">
                                                <span class="visible-xs"> All Rewards video </span>  </a> </li>
                                        <li><a href="{{ URL::to('rewards/most-watched') }}">Most watched </a></li>                                       
                                    </ul>
                                    
                                    
                                <div class="btn-group pull-right bg-clean">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Sort by <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a onclick="rewardVideoSortBy('recent')"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                        <li><a onclick="rewardVideoSortBy('top_viewed')"><i class="cv cvicon-cv-view-stats"></i> Top Viewed</a></li>
                                        <li><a onclick="rewardVideoSortBy('top_liked')"><i class="fa fa-thumbs-up"></i> Top liked</a></li>
                                        <li><a onclick="rewardVideoSortBy('longest')"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        
                        
                          <div class="clearfix"></div>
                        
                                
                            </div>
                        </div>
<style>
#all, #oneweek, #twoweek, #threeweek, #fourweek{cursor:pointer;}
</style>




<style>
.trending-rewards-tags {margin-top:15px;}
.trending-rewards-tags ul{list-style:none;text-align:left;margin:0 0 15px 0;padding:0px;}
.trending-rewards-tags li{display:inline-block;text-align:left;}
.trending-rewards-tags li a{   background: #e5e5e5;border: 0px;color: #000;line-height: 27px;height: 25px;padding: 2px 10px 0 10px;margin-bottom: 1px;font-size: 12px;}
#reward_tags_all .owl-item {margin-right:5px;width: auto !important;}
.tag_head{margin-right: 5px;float: left;width: 9%;line-height: 27px;}
#reward_tags_all{float:left; width: 89.1%;}





#reward_tags_all .owl-pagination {display:block !Important;}
#reward_tags_all .owl-page span{display:none; }
#reward_tags_all .owl-page.active{opacity:0.6;}
#reward_tags_all .owl-page{ }
#reward_tags_all .owl-page:nth-child(1) {position: absolute;left: -16px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow-gray.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}  
#reward_tags_all .owl-page:nth-child(2) {position: absolute;right: -17px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow-gray.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}
#reward_tags_all .owl-page:nth-child(1):hover {position: absolute;left: -16px;background: url(https://www.entertainmentbugs.com/public/img/prev_arrow.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}  
#reward_tags_all .owl-page:nth-child(2):hover {position: absolute;right: -17px;background: url(https://www.entertainmentbugs.com/public/img/right_arrow.png) no-repeat;width: 13px;height: 26px;top: 0px;background-size: contain;}


</style>                    




<div class="trending-rewards-tags">
   <div class="span12" id="crousal_wrap" style="width:100%">
     <div class="tag_head">Trending Tags: </div>   
        <ul id="reward_tags_all" class="owl-carousel" style="display:block">
             
            @if(isset($tags) && count($tags)>0)
             @foreach($tags as $tag)   
            <li class="item"><a href="{{url('/reward-trending-tags/'.$tag)}}">{{$tag}}</a></li>
            @endforeach
            @endif
           
      </ul>
    </div>
</div>
@if(Route::current()->getName() === 'trending_tags')
<div>
    {{request()->route()->parameters['id']}}
</div>
@endif


 
   
                        <div class="cb-content videolist" id="home_page_video">
                            <div id="allRewardVideos">
                                @include('elements.home_sorting_videos.all_reward_video')                  
                            </div>
                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>
            </div>
        </div>
    </div>
    
    
    
    
    
    <script>
           $(document).ready(function() { 
               var owl = $("#reward_tags_all"); 
               owl.owlCarousel({ 
                   items: 15, 
                   itemsDesktop: [1000, 6], 
                   itemsDesktopSmall: [900, 8], 
                   itemsTablet: [600, 7],  
                   itemsMobile: [600, 7],  
               }); 
           });
       </script>


    
    <script>
        function rewardVideoSortBy(order_by){
            $.ajax({
                type: "GET",
                url: "{{ URL::to('show_all_reward_order_by') }}",
                data: {'keyword':order_by},
                cache: false,
                success: function(response)
                {
                    $('#allRewardVideos').empty().html(response);
                    console.log(response);
                }
            });
        }
    </script>
@endsection
