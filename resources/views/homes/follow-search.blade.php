@foreach($users as $user)
@php    
    $followers = DB::table('followers')->where('follows_id',$user->id)->get();
    $isFollowing = DB::table('followers')->where('follows_id',$user->id)
                                        ->where('user_id',session()->get('user_id'))
                                        ->first();
   $userphotoPath = PROFILE_SMALL_DISPLAY_PATH.$user->photo;
@endphp
<style>

    .main-profile-default{width: 70px;
    display: block;
    height: 70px;
    text-align: center;
    line-height: 88px;
    margin-top:10px;
    background: #28b47e !important;
    color: #fff;
    border-radius: 100px;
    font-size: 32px; 
    font-weight:bold;
    text-transform: uppercase;}
    
    .main-profile-default:hover{color:#fff;}

@media (min-width: 100px) and (max-width: 767px){ 
    
    
    .abt-mbl-sml{font-size:12px;}
    
.main-profile-default {width: 75px;height: 75px;line-height: 88px;margin-top: 2px;font-size: 32px;}
}

</style>
<div class="user-search-individual">
    <div class="col-lg-2" style="padding:0px;" id="new-profile">
        <div class="c-avatar">
         @if(!empty($user->photo))
            <a href="{{ user_url($user->slug) }}">
                <img src="{{ asset( $userphotoPath ) }}" alt="" style="width:60px;height:60px;object-fit:cover; margin:10px auto 10px; display:block;border-radius:100px">
            </a>
        @else
            <a href="{{ user_url($user->slug) }}" class="main-profile-default">
                <div class="shortnamepr">{{ name_to_pic($user->name) }}</div>
            </a>
        @endif
        </div>
    </div>
    <div class="col-lg-10  col-xs-10" id="new-profile-details">
        <div class="channel-details">
        <div class="row">
            <?php $user_id = $user->id ?>
            @if($user->id !== session()->get('user_id'))
                @if(isset($isFollowing))
                    <div id="unfollow_user{{$user->id}}">
                        @include('homes.follow-search.unfollow_user')
                    </div>
                @else
                    <div id="follow_user{{$user->id}}">
                    @include('homes.follow-search.follow_user')
                    </div>
                @endif
            @endif
            <a href="{{ user_url($user->slug) }}" style="display:block">
                <h5>{{$user->slug ?? 'username'}}   <img style="width: 15px;" src="https://www.entertainmentbugs.com/public/img/verified-profile-icon.png"> <br>  </h5>
            </a>
            <p>{{$user->name ?? ''}} <br> {{count($followers)}} followers</p>
        </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@endforeach
<script>
    function followUser(user_id){
        $.ajax({
            type: "GET",
            url: "{{ URL::route('follow_user') }}",
            data: {'user_id':user_id},
            cache: false,
            success: function(response)
            {
                $('#follow_user'+user_id).empty().html(response);
                $('#follow_user'+user_id).attr("id","unfollow_user"+user_id);

            }
        });
    }
    function unFollowUser(user_id){
        $.ajax({
            type: "GET",
            url: "{{ URL::route('unfollow_user') }}",
            data: {'user_id':user_id},
            cache: false,
            success: function(response)
            {
                $('#unfollow_user'+user_id).empty().html(response);
                $('#unfollow_user'+user_id).attr("id","follow_user"+user_id);
            }
        });
    }
</script>
