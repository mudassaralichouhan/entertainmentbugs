@extends('layouts.home')
@section('content')
<?php
$url_segment = \Request::segment(2);
?>
<style>
.content-block .cb-content {clear: both;overflow: hidden;}
</style>
 <style>
#search_page_video h4 {font-size: 13px;height: 31px;overflow: hidden;line-height: 16px;color:#fff;}
#search_page_video p{font-size:12px;    margin: 0 0 4px 0;}
#search_page_video img.media-object {position: absolute;right: 5px;bottom: 40px;width: 35px;margin: 0 5px 0 0;}
body.light .content-wrapper {background-color: #f4f3f3;}
.content-block.head-div.head-arrow .head-arrow-icon{background-color: #f4f3f3;}
 #desk-pad{padding:0 10px;}
#search_page_video .media h2{font-size:14px;font-weight:bold;margin: 10px 0;}
#search_page_video .media h3{font-size:12px;font-weight:500;margin: 10px 0;}
#search_page_video .media h4{font-size:13px;}
#search_page_video .media h5{font-size:12px;}
#search_page_video .media h6{font-size:12px;}
#search_page_video p {height: 13px;overflow: hidden;}
#search_page_video .media p:nth-child(2){display:block;}
#search_page_video .media p{display:none;}
#search_page_video .media h3{display:none;}
#search_page_video .media h4{display:none;}
#search_page_video .media h2{display:none;}
#search_page_video .media h6{display:none;}
#search_page_video .media h5{display:none;}
#search_page_video .media blockquote{display:none;}
#search_page_video p:nth-child(1){display:none;}
#search_page_video p:nth-child(3){display:none;}
#search_page_video ul.list-inline {  text-align:left;}
#search_page_video ul.list-inline li{font-size:10.80px; text-align:right;}
#search_page_video ul.list-inline li span i{font-size:18px; margin-left:5px;}
#search_page_video ul.list-inline li { padding-left: 1px;padding-right: 1px;}
#search_page_video ul.list-inline-block li a{color:#f72e5e !important}
#search_page_video ul.list-inline{margin:0px;}
.v-categories.side-menu .content-block .cb-header {margin-bottom: 15px;}
.v-categories .content-block .cb-header {padding: 7px 0;}
</style>

    <div class="content-wrapper search_page_main_block">
        <div class="container">
            <div class="col-lg-12">
                <div class="row search_page_first_block">
                    <div class="row">
                        <div class="col-lg-10 col-sm-10 col-xs-8">

                        </div>
                        <div class="col-lg-2 col-sm-2 col-xs-4">
                            @if($searchType == 'video')
                                <div class="btn-group pull-right bg-clean">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Sort by <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a onclick="categoryVideoSortByRecent()"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                        <li><a onclick="categoryVideoSortByTopViewed()"><i class="cv cvicon-cv-view-stats"></i> Top Viewed</a></li>
                                        <li><a onclick="categoryVideoSortByTopLiked()"><i class="fa fa-thumbs-up"></i> Top liked</a></li>
                                        <li><a onclick="categoryVideoSortByTopTrending()"><i class="fa fa-heart-o"></i> Top Trending</a></li>
                                        <li><a onclick="categoryVideoSortByTopLongest()"><i class="cv cvicon-cv-watch-later"></i> Longest</a></li>
                                    </ul>
                                </div>
                            @else
                            <div class="btn-group pull-right bg-clean">
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Sort by <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a onclick="storyVideoSortByRecent()"><i class="cv cvicon-cv-calender"></i> Recent</a></li>
                                    <li><a onclick="storyVideoSortByTopTrending()"><i class="cv cvicon-cv-view-stats"></i>Trending</a></li>
                                    <li><a onclick="storyVideoSortByTopLiked()"><i class="fa fa-thumbs-up"></i> Relenanve </a></li>
                                    <li><a onclick="storyVideoSortByTopViewed()"><i class="fa fa-heart-o"></i> Viewed </a></li>
                                </ul>
                            </div>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    

                   <h3 id="result-display">You are  searching: <span style="color:#f72e5e">{{$searchdata}}</span></h3>
                    <br>
                    <!-- Featured Videos -->
                    <div class="content-block">
                        <div class="cb-content v-history" id="search_page_video">
                            
                            @if($searchType == 'video')

                                @foreach ($criterias as $criteria)                                   
                                    @include('elements.home.search_card')
                                    
                                    @if($loop->iteration % 2 == 0)
                                        <div class="divider" style="clear:both; overflow:hidden"></div>   
                                    @endif                                
                                @endforeach

                            @else
                                @if ($stories)
                                    @include('elements.story.cards')
                                @endif
                            @endif  

                        </div>
                    </div>
                    <!-- /Featured Videos -->
                </div>

            </div>

        </div>

    </div>
    <script>
        // Start story Video Sorting
        function storyVideoSortByRecent(){
            var url = '<?php echo e($url_segment); ?>';
            var searchData='<?php echo $searchdata;?>';
            $.ajax({
                type: "GET",
                url: "<?php echo e(URL::to('story_video_sort_by_recent')); ?>",
                data: {'keyword':url,searchData:searchData},
                cache: false,
                success: function(response)
                {
                    $('div#search_page_video').empty().html(response);
                }
            });
        }

        function storyVideoSortByTopTrending(){
            var url = '{{ $url_segment }}';
            var searchData='<?php echo $searchdata;?>';
            $.ajax({
                type: "GET",
                url: "{{ URL::to('story_video_sort_by_trending') }}",
                data: {'keyword':url,searchData:searchData},
                cache: false,
                success: function(response)
                {
                    $('div#search_page_video').empty().html(response);
                }
            });
        }


        function storyVideoSortByTopLiked(){
            var url = '{{ $url_segment }}';
            var searchData='<?php echo $searchdata;?>';
            $.ajax({
                type: "GET",
                url: "{{ URL::to('story_video_sort_by_liked') }}",
                data: {'keyword':url,searchData:searchData},
                cache: false,
                success: function(response)
                {
                    $('div#search_page_video').empty().html(response);
                }
            });
        }
        function storyVideoSortByTopViewed(){
            var url = '{{ $url_segment }}';
            var searchData='<?php echo $searchdata;?>';
            $.ajax({
                type: "GET",
                url: "{{ URL::to('story_video_sort_by_viewed') }}",
                data: {'keyword':url,searchData:searchData},
                cache: false,
                success: function(response)
                {
                    $('div#search_page_video').empty().html(response);
                }
            });
        }

        function storyVideoSortByTopRated(){
            var url = '{{ $url_segment }}';
            var searchData='<?php echo $searchdata;?>';
            $.ajax({
                type: "GET",
                url: "{{ URL::to('story_video_sort_by_top_rated') }}",
                data: {'keyword':url,searchData:searchData},
                cache: false,
                success: function(response)
                {
                    $('div#search_page_video').empty().html(response);
                }
            });
        }

        function categoryVideoSortByRecent(){
            var url = '{{ $url_segment }}';
            var searchData='<?php echo $searchdata;?>';
            $.ajax({
                type: "GET",
                url: "{{ URL::to('category_video_sort_by_recent') }}",
                data: {'keyword':url,searchData:searchData},
                cache: false,
                success: function(response)
                {
                    $('div#search_page_video').empty().html(response);
                }
            });
        }
        function categoryVideoSortByTopViewed(){
            var url = '{{ $url_segment }}';
            var searchData='<?php echo $searchdata;?>';
            $.ajax({
                type: "GET",
                url: "{{ URL::to('category_video_sort_by_viewed') }}",
                data: {'keyword':url,searchData:searchData},
                cache: false,
                success: function(response)
                {
                    $('div#search_page_video').empty().html(response);
                }
            });
        }
        function categoryVideoSortByTopLiked(){
            var url = '{{ $url_segment }}';
            var searchData='<?php echo $searchdata;?>';
            $.ajax({
                type: "GET",
                url: "{{ URL::to('category_video_sort_by_liked') }}",
                data: {'keyword':url,searchData:searchData},
                cache: false,
                success: function(response)
                {
                    $('div#search_page_video').empty().html(response);
                }
            });
        }
        function categoryVideoSortByTopTrending(){
            var url = '{{ $url_segment }}';
            var searchData='<?php echo $searchdata;?>';
            $.ajax({
                type: "GET",
                url: "{{ URL::to('category_video_sort_by_trending') }}",
                data: {'keyword':url,searchData:searchData},
                cache: false,
                success: function(response)
                {
                    $('div#search_page_video').empty().html(response);
                }
            });
        }
        function categoryVideoSortByTopLongest(){
            var url = '{{ $url_segment }}';
            var searchData='<?php echo $searchdata;?>';
            $.ajax({
                type: "GET",
                url: "{{ URL::to('category_video_sort_by_longest') }}",
                data: {'keyword':url,searchData:searchData},
                cache: false,
                success: function(response)
                {
                    $('div#search_page_video').empty().html(response);
                }
            });
        }


    </script>

@endsection
