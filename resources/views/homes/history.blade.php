@extends('layouts.home')
@section('content')
<div class="content-wrapper">
   <div class="container">
       <div class="row">
           <div class="col-lg-12 v-history">
               <!-- History -->
               <div class="content-block">
                   <div class="cb-header">
                       <div class="row">
                           <div class="col-lg-8 col-xs-8 col-sm-6">
                               <ul class="list-inline">
                                   <li><a href="#" class="color-active active">Watch history</a></li>
                                   <li><a href="#">Search history</a></li>
                               </ul>
                           </div>
                           <div class="col-lg-4 col-xs-4 col-sm-6 h-clear">
                                                <form action="{{ url('/myhistory/deleteall/')}}" method="POST" id="deleteVideo">
                                                <a href="#" onclick="$(this).closest('form').submit()"><i class="cvicon-cv-cancel"></i> Clear all <span class="hidden-xs">Search History</span></a>
                                                {{ csrf_field() }} 
                                            </form>
                                         </div>   
                                            
                           
                               <!--<a href="#"><i class="cvicon-cv-cancel"></i> Clear all <span class="hidden-xs">Search History</span></a>-->
                           
                           <div class="clearfix"></div>
                       </div>
                   </div>
                   <div class="cb-content">
                       <!-- history video row -->
                      
                            
                                 			
                           

                      
                       
                       <!-- ///history video row -->
                   </div>
               </div>
               <!-- /History -->
               
               	<div class="container text-center">
		<div class="row">
		     @if(!empty($histories))
                            @foreach($histories as $history)
                                    @php
                                        $thumbBasePath = '';
                                        $thumbUrlPath = '';
                                    @endphp
                            
                                @if(isset($history->thumb->thumb))
                                    @php
                                        $thumbBasePath = VIDEO_THUMB_UPLOAD_PATH.$history->thumb->thumb;
                                        $thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH.$history->thumb->thumb;
                                    @endphp
                                @endif
                                
                                
			<div class="col-md-12">
				<div class="content">
 				  <div class="row">
                                    <div class="h-video">
                                        <div class="col-lg-2 col-sm-5 col-xs-6">
                                            <div class="v-img">
                                                <a href="{{URL::to($history->url)}}">
                                                    @if(file_exists($thumbBasePath))
                                                        {{HTML::image($thumbUrlPath)}}
                                                    @else
                                                        {{HTML::image('public/img/video2-8.png',null,['style'=>'width:100%'])}}
                                                    @endif
                                                </a>
                                            <div class="time">{{ $history->video_time}}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-sm-5 col-xs-6">
                                            <div class="v-desc">
                                            <a href="{{url('/watch/'.$history->video->video_key)}}"> {{ $history->title}}</a>
                                            </div>
                                            <div class="v-views">
                                                @php
                                                    $created_at = strtotime($history->created_at);
                                                    $curDatetime = time();
                                                    $difference = $curDatetime - $created_at;
                                                @endphp
                                                {{ count($history->viewsCount) }} views. <span class="v-percent"><span class="v-circle"></span> {{ round($difference / 86400) }} days ago </span>
                                            </div>
                                            <div class="v-percent"><span class="v-circle"></span> 84%</div>
                                        </div>
                                        <div class="col-lg-2 col-sm-2 h-clear-list">
                                            <form action="{{ url('/myhistory/delete/'.$history->id)}}" method="POST" id="deleteVideo">
                                                <a href="#" onclick="$(this).closest('form').submit()"><i class="cvicon-cv-cancel"></i></a>
                                                {{ csrf_field() }} 
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="h-divider"></div>
                                    </div>
                                </div>
                                
                                
                                
                               
				</div>
			</div>
		  @endforeach   
                        @endif	 
		</div>
		
		<div class="loadmore hidden-xs">
                    
                       <button class="load-more btn btn-default h-btn">Load more Videos</button>
                    
               </div>
     
	</div>
	
<style>
    
    .next-button-forward a{
        color: rgb(255, 255, 255);
        display: block;
        font-size: 14px;
        text-align: center;
        line-height: 22px;
        border-radius: 0px;
        background: rgb(247, 46, 94);
        padding: 16px 0px 12px;
        visibility:hidden;
    }
</style>
            @if ($histories->lastPage() > 1)
                @if($histories->currentPage() != $histories->lastPage())
                <div class="next-button-forward">
                     <a href="{{ $histories->url($histories->currentPage()+1) }}" class=" btn btn-default h-btn">Load more Videos</a>
                </div>
               @endif
            @endif
           </div>
       </div>
   </div>

</div>
<style>
.col-lg-2.col-sm-5.col-xs-6{padding-left: 0;}
.v-history .h-video {padding-bottom: 0;margin-bottom: 15px;background: #fff;}
.v-history .h-video .v-desc {padding: 16px 0 0 0;}
.col-lg-8.col-sm-5.col-xs-6{text-align:left;}
.col-md-12{display:none;}
a.frst3 i{color: #f72e5e !important;}
body.light .content-wrapper {background-color: #f4f3f3;}
</style>

<script>
    
    $(function () {
		$(".col-md-12").slice(0, 3).show();
		$("body").on('click touchstart', '.load-more', function (e) {
			e.preventDefault();
			$(".col-md-12:hidden").slice(0, 3).slideDown();
			if ($(".col-md-12:hidden").length == 0) {
				$(".load-more").css('visibility', 'hidden');
				$(".next-button-forward a").css('visibility', 'visible');
			}
			$('html,body').animate({
				scrollTop: $(this).offset().top
			}, 1000);
		});
	});
    
    
</script>
@endsection