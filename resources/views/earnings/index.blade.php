@extends('layouts.home')
@section('content')
	<section class="channel light">
		@include('elements.header_profile')
		<div class="content-wrapper p-rel" id="earning-board">
			<div class="_loading">
				<img src="{{ asset('public/img/loading.gif') }}" />
			</div>
			<div class="container">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item active">
						<a class="nav-link" data-tab="videos" id="video-tab" role="tab" aria-controls="video" aria-selected="true">Video</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="story-tab" data-tab="stories" role="tab" aria-controls="story" aria-selected="false">Stories</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" id="photos-tab" data-tab="photos" role="tab" aria-controls="photos" aria-selected="false">Photos</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="reward-tab" data-tab="reward-videos" role="tab" aria-controls="reward" aria-selected="false">Reward</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade active in" id="video" role="tabpanel" aria-labelledby="video-tab">
						<div class="row">
							<div class="col-lg-12">
								<!-- Featured Videos -->
								<div class="content-block" style="padding:20px 0">
									<div class="cb-content videolist">
										<div class="row" id="videos-analytic">
											@include('earnings.layout.videos', ['action' => 'videos'])
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="story" role="tabpanel" aria-labelledby="story-tab">
						<div class="row">
							<div class="col-lg-12">
								<div class="content-block" style="padding:20px 0">
									<div class="cb-content videolist">
										<div class="row" id="stories-analytic">

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="photos" role="tabpanel" aria-labelledby="photos-tab">
						<div class="row">
							<div class="col-lg-12">
								<div class="content-block" style="padding:20px 0">
									<div class="cb-content videolist">
										<div class="row" id="photos-analytic">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="reward" role="tabpanel" aria-labelledby="reward-tab">
						<div class="row">
							<div class="col-lg-12">
								<div class="content-block" style="padding:20px 0">
									<div class="cb-content videolist">
										<div class="row" id="reward-videos-analytic">
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>
	</section>
@endsection
