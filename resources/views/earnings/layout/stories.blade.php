<div class="content-block" style="padding:20px 0">
	<div class="cb-content v-history">
		@if($stories && $stories->count() > 0)
			@foreach ($stories as $story)
				@include('elements.earning.storycard')
			@endforeach
		@else
			No Story Found.
		@endif
	</div>
	{{ $stories->appends(['action' => 'stories'])->links() }}
</div>