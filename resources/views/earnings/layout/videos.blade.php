
<div class="content-block" style="padding:20px">
	<div class="cb-content v-history">
		@if ($videos && $videos->count() > 0)
			@foreach ($videos as $video)
				@include('elements.earning.videocard', ['action' => isset($action) ? $action : "videos"])
			@endforeach
		@endif
	</div>
	{{ $videos->appends(['action' => isset($action) ? $action : "videos"])->links() }}
</div>