<div class="content-block" style="padding:20px">
	<div class="cb-content v-history">
		@if($photos && $photos->count() > 0)
			@foreach ($photos as $photo)
				@include('elements.earning.photocard')
			@endforeach
		@else
			No Photos Found.
		@endif
	</div>
	{{ $photos->appends(['action' => 'photos'])->links() }}
</div>