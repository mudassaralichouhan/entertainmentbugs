   
       <Style>
        #{{$class}} img{width:100%}
       .new1{padding:0 5px;}
       #{{$class}}{margin:0 -5px 5px -5px;}
        #{{$class}}.owl-page span{display:none !important;}
      
 .owl-page:before,  .owl-page:after {
   border-right: 2px solid;
    content: '';
    display: block;
    height: 15px;
    margin-top: -11px;
    position: absolute;
    -moz-transform: rotate(135deg);
    -o-transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
    transform: rotate(135deg);
    right: 17px;
    top: 45%;
    width: 0;
}

 .owl-page:after {
    margin-top: -1px;
    -moz-transform: rotate(45deg);
    -o-transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
}

 .owl-page:hover,  .owl-page:focus,
 .owl-page:hover:before,  .owl-page:hover:after,
 .owl-page:focus:before,  .owl-page:focus:after {
    color: #000;
}



     .owl-page{width: 40px;
    height: 40px;
    top: 58PX;
    border-radius: 3%;
    background-color: #f72e5e !Important;
    color: #fff;
    border: 0px;
    font-size: 19px;
    line-height: 48px;
    text-align: center;
    padding: 0;
    position: absolute;
    left: -15px;
    margin: 0;}
       .owl-page:nth-child(2){left:auto; right:-15px;}
     
       
       </Style>
       
        <div class="container">
           <div class="pro-teams gallery">    
             <div class="pro-team-slide-ind">  
              <div class="span12" id="crousal_wraps">
              <div id="{{$class}}" class="owl-carousel" style="display:block">
 @php                 
 $datas = json_decode($data);
 @endphp
@if( $datas && count($datas) > 0 )
    
    @foreach ($datas as $key => $video)
           
    
        @if (!empty($video) && !empty($video->user))
    <div class="item">
        <div class="new1" style="width:100%"> 
            @if( ($key+1)%8 == 0 )
                <div class="col-lg-2 col-sm-6 video-item ads_block_contatiner" data-ad-type="video" data-ad-style='listing' data-ad-block>

                </div>
            @endif
            @php
                $videoUrl = $video->video_key;
                $userphotoPath = PROFILE_SMALL_DISPLAY_PATH . $video->user->photo;
                $created_at = strtotime($video->created_at);
                $curDatetime = time();
                $difference = $curDatetime - $created_at;
            @endphp
          
                <div class="b-video">
                    <div class="user">
                        @if(!empty($video->user->photo))
                            <a href="{{ user_url($video->user->slug) }}" class="user_img" target="">{{HTML::image($userphotoPath)}}</a>
                         @else
                            <a href="{{ user_url($video->user->slug) }}" class="user_img" target="">
                                <div class="shortnamevd">{{ name_to_pic($video->user->name) }}</div>
                            </a>  
                        @endif
                        
                    </div>
                    <div class="v-img" id="{{ $video->video_key }}">
                        @if (!empty($video->user) && !empty($video->thumb))
                            @php
                                $thumbBastPath = VIDEO_THUMB_UPLOAD_PATH . $video->thumb->thumb;
                                $thumbUrlPath = VIDEO_THUMB_DISPLAY_PATH . $video->thumb->thumb;
                            @endphp

                            @if (file_exists($thumbBastPath))
                                <a href="{{ URL::to('watch/' . $videoUrl) }}">{{ HTML::image($thumbUrlPath) }}
                                  
                          {{--      @if (!empty($video->user) && !empty($video->is_watched_video_count) && $video->is_watched_video_count > 0)
                                     <div class="watched-mask"></div>
                                     <div class="watched">WATCHED</div>
                                 @endif  --}}
                                 </a>
                            @else
                                <a href="{{ URL::to('watch/' . $videoUrl) }}">{{ HTML::image('public/img/video1-1.png') }}</a>
                            @endif
                        @endif
                        <div class="time">{{ $video->video_time }}</div>
                    </div>
                    @if(!empty($video->user))
                    <div class="v-desc">
                        <a href="{{ URL::to('watch/' . $videoUrl) }}">{{ $video->title }}</a>
                    </div>
                    <div class="v-views">{{ $video->total_views_count }} views <span class="v-percent"><span class="v-circle"></span> {{ \Carbon\Carbon::parse($video->created_at)->diffForHumans() }} </span>
                    </div>
                    @endif
                </div>
         </div>
        </div>
        @endif
       
    @endforeach
@endif

<style>.v-img a::before {transition: 0.3s;background: url(https://www.entertainmentbugs.com/public/uploads/logo/favicon.png) no-repeat;position: absolute;width: 43px;height: 43px;top: 50%;margin-left:-21.5px;margin-top:-21.5px;z-index: 999;display: block;content: "";left: 50%;display:none;background-size: contain;}
.v-img a::after {transition: 0.3s;display:none;background: rgba(255,255,255,0.7);position: absolute;width: 100%;height: 100%;top: 0;z-index: 99; content: "";left: 0;}
.v-img a:hover::before{display:block;transition: 0.3s;}
.v-img a:hover::after{display:block;transition: 0.3s;}
</style>
                    
                  
          
                    
                     </div>
                    </div> 
                 </div> 
              </div>
            </div>
               
<script>
    $(document).ready(function(){
        @if($class)
        
        var {{$var}} = $('#{{$class}}');
     {{$var}}.owlCarousel({
    
      itemsTablet: [600,4], //2 items between 600 and 0;
      itemsMobile :[600,2] // itemsMobile disabled - inherit from itemsTablet option

      });
      
      {{$var}}.next()   // Go to next slide
{{$var}}.prev()  

@endif
    });
</script>
  