<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

// Route::get('/route-cache', function() {
//     $exitCode = Artisan::call('route:cache');
//     return 'Routes cache cleared';
// });

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('optimize');
    return "Cache is cleared";
});

Route::get('/update-video-upload-time', function () {
    Artisan::call("update_watch_video_time");
    return "Video Time Updated";
});

Route::get('/update-reward-video-total-earning', function () {
    Artisan::call('update:update-reward-video-total-earning');
    return 'update-reward-video-total-earning';
});

Route::post('/submitcomplain', 'Complain@save')->name('complainsave');

Route::get("test", function () {
    send_mail("forgot-password", "tester@mail.com", [
        "template" => "email.auth.forgot-password",
        "subject" => "Greetings",
        "name" => "Tester",
        "link" => "test"
    ]);
    // App\Http\Controllers\MailController::send('register', 'email@address');
});

Route::get('/', 'HomesController@index');
Route::get('/get-states-by-country', 'UsersController@getStatesByCountry')->name('country.state');
Route::get('/get-languages', 'UsersController@getLanguage')->name('get.language');

Route::post('/get-states', 'HomesController@getStates');
Route::post('/get-cities', 'HomesController@getCities');

Route::any('/artist', 'HomesController@artist');
Route::any('/casting', 'HomesController@casting');
Route::any('/production-house', 'HomesController@productionhouse');

Route::get('/about', 'HomesController@about');
Route::get('/contact', 'HomesController@contact');
Route::get('/help', 'HomesController@help');
Route::any('/privacy', 'HomesController@privacy');
Route::any('/faq', 'HomesController@faq');
Route::any('/getTeamImage', 'HomesController@getTeamImage');
Route::any('/terms-and-conditions', 'HomesController@termsandconditions');
// Route::get('/audition', 'HomesController@audition');
Route::any('/post-audition', 'HomesController@postaudition');
Route::any('/my-audition', 'HomesController@myaudition');
Route::any('/artist/{slug}', 'HomesController@artistprofile');
Route::get('/accountblock', 'HomesController@accountblock');
Route::any('/create-artist-profile-temp', 'HomesController@createTemp');
Route::any('/create-production-profile-temp', 'HomesController@createProdTemp');
Route::any('/save-gallery', 'HomesController@saveGallery');
Route::any('/save-production-team', 'HomesController@saveProductionTeam');
Route::any('/save-production-gallery', 'HomesController@saveProductionGallery');
Route::any('/save-video', 'HomesController@saveVideo');
Route::any('/save-production-video', 'HomesController@saveProductionVideo');
Route::any('/login', 'UsersController@login');
Route::any('/logout', 'UsersController@logout');
Route::any('/register', 'UsersController@register');
Route::any('/email-confirmation/{ukey}', 'UsersController@emailConfirmation');
Route::any('/forgot-password', 'UsersController@forgotPassword');
Route::any('/reset-password/{ukey}', 'UsersController@resetPassword');
Route::any('/users/dashboard', 'UsersController@dashboard');
Route::any('/users/edit', 'UsersController@editprofile');
Route::get('/complatevideo/{id}', 'HomesController@complatevideo')->name('complatevideo');
Route::get('/photos/all-tags', 'HomesController@alltagphotos');
Route::get('/photo/{tag}', 'HomesController@photostag');

Route::group(['middleware' => ['web']], function () {
    Route::get('/users/handlegooglecallback', 'UsersController@handleGoogleCallback');
    Route::get('/users/redirecttogoogle', 'UsersController@redirectToGoogle');
    Route::get('/users/redirecttoacebook', 'UsersController@redirectToFacebook');
    Route::get('/users/handlefacebookcallback', 'UsersController@handleFacebookCallback');
    Route::get('/users/sociallogin', 'UsersController@sociallogin');
});

Route::any('/users/uploadprofileimage', 'UsersController@uploadprofileimage');
Route::any('/upload-video', 'HomesController@uploadVideo');

Route::any('/update-video/{key}', 'HomesController@updateVideo');
Route::any('/myvideos', 'UsersController@myVideos');
Route::get('/profilevideos/{user}', 'UsersController@profileVideos');
Route::any('/production-house/{username}', 'HomesController@productionhouseprofile');
Route::post('/addbankdetails', 'HomesController@addbankdetails')->name('bank.details.add');
Route::post('/addpaypaldetails', 'HomesController@addpaypaldetails')->name('paypal.details.add');
Route::any('/artist-gallery', 'HomesController@artistgallery');
Route::any('/artist-profile', 'HomesController@artistprofiles');
Route::any('/artist-videos', 'HomesController@artistvideos');
Route::any('/find-auditions', 'HomesController@findauditions');
Route::any('/production-houses', 'HomesController@findproductionhouses');

//Route::any('/videos/delete/{key}', 'HomesController@deleteVideo');


// Category Video Sort
Route::any('/category_video_sort_by_recent', 'UploadController@categoryVideoSortByRecent');
Route::any('/category_video_sort_by_viewed', 'UploadController@categoryVideoSortByViewed');
Route::any('/category_video_sort_by_liked', 'UploadController@categoryVideoSortByLiked');
Route::any('/category_video_sort_by_trending', 'UploadController@categoryVideoSortByTrending');
Route::any('/category_video_sort_by_longest', 'UploadController@categoryVideoSortByLongest');
// End Category Video Sort

// story Video Sort
Route::any('/story_video_sort_by_recent', 'UploadController@storyVideoSortByRecent');
Route::any('/story_video_sort_by_viewed', 'UploadController@storyVideoSortByViewed');
Route::any('/story_video_sort_by_liked', 'UploadController@storyVideoSortByLiked');
Route::any('/story_video_sort_by_trending', 'UploadController@storyVideoSortByTrending');
Route::any('/story_video_sort_by_longest', 'UploadController@storyVideoSortByLongest');

// Start Sorted By Latest Video
Route::any('/show_latest_post_order_by', 'UploadController@videoSortedBy');
Route::any('/show_most_viewed_order_by', 'UploadController@videoMostViewed');
Route::any('/show_latest_stories_order_by', 'UploadController@latestStoriesOrderBy');
Route::any('/show_all_reward_order_by_list', 'UploadController@allRewardOrderBy');
// End Sorted By Latest Video

// End story Video Sort
Route::any('stories', 'StoryController@index');
Route::any('videos', 'HomesController@videos');

Route::get('get-video', 'HomesController@getVideo')->name('get.video');

Route::group(['middleware' => 'is_userlogin'], function () {
    Route::any('/myhistory', 'HomesController@mywatchhistory');
    Route::any('/myhistory/delete/{key}', 'HomesController@deleteHistory');
    Route::any('/myhistory/deleteall', 'HomesController@deleteallHistory');
    Route::any('/saved-playlist', 'HomesController@savedPlaylist');
    Route::any('/watch/watch-later/{key?}', 'HomesController@watchLater');
    Route::any('/watch/liked-video/{key}', 'HomesController@likedVideo');
    Route::any('/watch/disliked-video/{key}', 'HomesController@dislikedVideo');
    Route::any('/watch/flag-video/{key}/{comment?}', 'HomesController@flagVideo');
    Route::any('/profile/{user?}', 'UsersController@profile')->name('profileAdd');
    Route::any('/myrewards', 'VideoController@myrewards');
    Route::any('/upload-reward-video', 'HomesController@uploadRewardVideo');
    Route::any('/edit-reward-video/{key}', 'HomesController@editRewardVideo');

    Route::any('/messages', 'HomesController@messages');
    Route::any('/save-contact', 'HomesController@saveContact');
    Route::any('/save-production-contact', 'HomesController@saveProductionContact');
    Route::any('/create-an-artist-profile', 'HomesController@createanartistprofile');
    Route::any('/save-artist-profile', 'HomesController@saveartistprofile');
    Route::any('/save-audition-profile', 'HomesController@saveauditionprofile');

    Route::any('/save-production-profile', 'HomesController@saveproductionprofile');
    Route::any('/update-audition-profile/{id}', 'HomesController@saveauditionprofile');
    Route::any('/save-project', 'HomesController@saveproject');
    Route::any('/production-save-project', 'HomesController@saveproductionproject');
    Route::any('/save-what-we-do-profile', 'HomesController@savewhatwedoprofile');
    Route::any('/production-what-we-do-profile', 'HomesController@productionwhatwedoprofile');
    Route::any('/save-achivement', 'HomesController@saveachivement');
    Route::any('/save-production-achivement', 'HomesController@saveproductionachivement');
    Route::any('/save-reel', 'HomesController@savereel');
    Route::any('/delete-team/{id}', 'HomesController@deleteTeam');
    Route::any('/delete-team-image/{id}', 'HomesController@deleteTeamImage');
    Route::any('/delete-award-image/{id}', 'HomesController@deleteAwardImage');
    Route::any('/delete-photo-image/{id}', 'HomesController@deletePhotoImage');
    Route::any('/delete-proj-image/{id}', 'HomesController@deleteProjImage');
    Route::any('/delete-prod-proj-image/{id}', 'HomesController@deleteProdProjImage');
    Route::any('/delete-achivement-image/{id}', 'HomesController@deleteAchivementImage');
    Route::any('/delete-video-image/{id}', 'HomesController@deleteVideoImage');
    Route::any('/delete-production-video-image/{id}', 'HomesController@deleteProductionVideoImage');
    Route::any('/delete-reel-image/{id}', 'HomesController@deleteReelImage');

    Route::any('/dashboard', 'DashboardController@index')->name('dashboard');
    Route::any('/total-earning', 'DashboardController@totalEarning')->name('total-earning');
    Route::any('/payment-withdraw', 'DashboardController@paymentWithdraw')->name('payment.withdraw');
    Route::post('/payoutrequest', 'HomesController@payoutrequest')->name('payout.request');
    Route::any('/promote-recent-post', 'HomesController@promoterecentpost')->name('posts.promote');
    Route::any('/promoted-posts', 'PromotePostController@promoted_posts')->name('posts.promoted');
    Route::any('/boost-plan', 'HomesController@boostplan')->name('plan.boost');
    Route::any('/theme-plan', 'HomesController@themeplan')->name('plan.theme');
    Route::post('razorpaypayment', 'HomesController@payment')->name('payment');
    Route::any('/ads-overview', 'HomesController@adsoverview')->name('ads.overview');
    Route::any('/active-ads-campaign', 'HomesController@activeadscampaign')->name('ads.campaign.active');
    Route::any('/edit-ads-campaign/{slug}', 'HomesController@editadscampaign')->name('ads.campaign.edit');
    Route::any('/closed-ads-campaign', 'HomesController@closedadscampaign')->name('ads.campaign.closed');
    Route::any('/create-new-ads-campaign', 'HomesController@createnewadscampaign')->name('ads.campagin.create');
    Route::any('/information', 'HomesController@information');
    Route::any('/location-and-language', 'HomesController@locationandlanguage');
    Route::any('/privacy-setting', 'HomesController@privacysetting');
    Route::any('/contact-us', 'HomesController@contactus');

    Route::any('/complain-request', 'HomesController@complainrequest');
    Route::any('/create-an-production-house-profile', 'HomesController@createanproductionhouseprofile');
    Route::any('/photo-upload', 'HomesController@UploadPhotos');
    Route::any('/liked-photo/{key}', 'HomesController@likedPhoto');
    Route::any('photos-child-details', 'HomesController@childdetails')->name('photos.childdetails');
    Route::any('/participate-video', 'HomesController@altopuploadVideo');
    Route::get('/all-top-entertainer/{week?}', 'VideoController@listReward');
    Route::any('/videos/edit-video/{key}', 'HomesController@editVideo');

    Route::any('/edit-profile/{user?}', 'UsersController@profileEdit')->name('edit.profile');
    Route::post('/later-complete-profile', 'UsersController@laterProfile')->name('later.complete.profile');

    Route::any('/videos/delete/{key}', 'HomesController@deleteVideo');
    Route::post('/comment/store', 'CommentController@store')->name('comment.add');
    Route::post('/reply/store', 'CommentController@replyStore')->name('reply.add');

    //Route::get('/userprofile', 'UsersController@index')->name('userprofile');
    Route::get('/userprofile/{user}', 'UsersController@index')->name('userprofile');
    Route::get('/videos/{slug}', 'UsersController@index');
    Route::post('users/{user_id}/follow', 'UsersController@follow')->name('follow');
    Route::any('users/{user_id}/unfollow', 'UsersController@unfollow')->name('unfollow');
    Route::get('/notifications', 'UsersController@allnotifications');
    Route::get('/user/all-notifications', 'UsersController@usernotifications');

    Route::post('/comment/storephoto', 'CommentController@store2')->name('commentPhoto.add');
    Route::post('/reply2/store', 'CommentController@replyStore2')->name('reply2.add');

    Route::get('/photo/comments/{photo_id}/{comment_id?}', 'CommentController@getPhotoComments')->where(['photo_id' => '[0-9]+', 'comment_id' => '[0-9]+']);
    Route::post('/photo/comment/store', 'CommentController@storeReelComment');
    Route::any('/audition/{slug}', 'HomesController@auditionview');
    Route::post('apply-for-audition/{id}', 'HomesController@applyForAudition');

    Route::get('promote-post/video/{watch_key}', 'PromotePostController@video')->name('promotevideo');
    Route::get('promote-post/photo/{id}', 'PromotePostController@photo')->name('promotephoto');
    Route::get('promote-post/story/{slug}', 'PromotePostController@story')->name('promotestory');
    Route::get('promoted-post/{id}', 'PromotePostController@promotedPost');
    Route::post('promote-post/create', 'PromotePostController@createPost');
    Route::put('promote-post/edit/{id}', 'PromotePostController@editPost');
    Route::get('checkout', 'PaymentController@checkout');
    Route::post('artist/save/brand-collaboration', 'ArtistController@saveBrandCollaboration');
    Route::post('artist/save/quote', 'ArtistController@saveQuote');
    Route::post("/mark-notification-as-read", 'UsersController@markNotificationsAsRead');

    Route::get('/video-watch-time', 'VideoController@UpdatewatchVideoTime');
    Route::post('/reward-video-watch-time/{video_id}', 'VideoController@trackRewardVideoWatch');
});
Route::post('/guest-reward-video-watch-time/{video_id}', 'VideoController@guestTrackRewardVideoWatch');

Route::post('/photos/store', 'PhotosController@store');
Route::resource('/photos', 'PhotosController');
Route::any('single-photo/{id}', 'PhotosController@single_photo')->name('single-photo');
Route::get('photos-load-ajax', 'PhotosController@loadmorePhotoAjax')->name('photo.load-ajax');
Route::post('photos/crop-image-upload ', 'PhotosController@uploadCropImage');
Route::any('/myphoto/{user?}', 'PhotosController@myphotos');
Route::any('/{slug?}/photo', 'PhotosController@showPhotos');
Route::any('/photos-details', 'HomesController@details')->name('photos.details');
Route::get('/photos/load', 'PhotosController@show')->name('photos.load');

Route::resource('/story', 'StoryController');
Route::get('advertise-with-us', 'HomesController@advertiseWithUs');
Route::get("most-popular-stories", 'StoryController@mostPopularStories')->name('most-popular-stories');

Route::any('category/{key?}', 'HomesController@categories');
//following
Route::any('following', 'HomesController@following');
//following
Route::any('/show_all_reward_order_by', 'UploadController@allFollowOrderBy');
Route::any('/watch/{key}', 'VideoController@watchVideo')->name('watch.video');
Route::get('/watch-reward/{key}', 'VideoController@showReward')->name('watch.reward-video');


Route::any('/channels', 'ChannelsController@index');
Route::any('/channel/create', 'ChannelsController@create');
Route::any('/channel/{slug}', 'ChannelsController@detail');

Route::any('/upload/video', 'UploadController@uploadVideo');
Route::any('/profile/upload/temp_image', 'UploadController@tempImage');

Route::any('/upload/temp_image', 'UploadController@tempImage');
Route::any('/upload/temp_base64_image', 'UploadController@tempBase64Image');
Route::any('story/display/{key}', 'StoryController@show')->name('story.display');
Route::any('/story/delete/{key}', 'HomesController@deleteStory')->name('story._delete');

Route::any('/story/upload/temp_image', 'UploadController@tempImage');
Route::any('/mystories/edit-story/upload/temp_image/', 'UploadController@tempImage');

Route::any('/story/display/liked-story/{key}', 'StoryController@likedVideo');
Route::any('/story/display/disliked-story/{key}', 'StoryController@dislikedVideo');
Route::any('/mystories/{user?}', 'StoryController@mystories');
Route::any('/{slug?}/stories', 'StoryController@showStories');
Route::any('/{mystories}/edit-story/{key}', 'StoryController@editstory');

Route::get('/earnings', 'EarningController@index')->name('earning');
Route::get('/earnings/rewards', 'EarningController@earningReward')->name('earning.reward');
Route::any('/search', 'HomesController@globalSerach');
Route::any('/search/{key}', 'HomesController@globalFilterSerach');

Route::post('/searchstory', 'StoryController@searchstory');

Route::any('story_category/{key?}', 'StoryController@storyByCategory');
Route::any('story_all/{key?}', 'StoryController@storyByAll');

Route::any('/story/display/rating/{id}/{rating}', 'StoryController@rateStory');
Route::any('/videograph/{id}', 'HomesController@videoGraphData');

Route::any('/unfollow_users/{id}', 'UsersController@unfollowUser');
Route::any('/follow_users/{id}', 'UsersController@followUser');

Route::any('/follow', 'HomesController@showFollow');
Route::any('/search-follow', 'HomesController@searchFollow')->name('searchFollow');
Route::any('/follow-user', 'HomesController@followUser')->name('follow_user');
Route::any('/unfollow-user', 'HomesController@unFollowUserSearch')->name('unfollow_user');

Route::any('/suggested-follow-user', 'HomesController@suggestedFollowUser')->name('suggested_follow_user');
Route::any('/suggested-unfollow-user', 'HomesController@suggestedUnFollowUserSearch')->name('suggested_unfollow_user');

Route::get('/rewards/most-watched', 'HomesController@rewardMostWatched');
Route::any('/rewards/{week?}', 'VideoRewardController@rewards');
Route::get('/reward-trending-tags/{id}', 'VideoController@rewardTrendingTags')->name('trending_tags');
Route::any('/{slug}/rewards', 'UsersController@showRewards');

Route::any('{slug}/followers', 'UsersController@showMyFollwers');
Route::any('{slug}/following', 'UsersController@showMyFollowing');

Route::any('/{slug}/video/{_slug}', 'ArtistController@video');
Route::any('/{slug}/gallery/{_slug}', 'ArtistController@gallery');
Route::any('/{slug}', 'UsersController@showProfile');
Route::get('/deletetemp/{id}', 'UploadController@deletetemp');
