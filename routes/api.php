<?php

use App\Http\Controllers\UsersController;
use Carbon\Carbon;
use App\Models\Video;

use App\Models\ArtistAbout;
use App\Models\ArtistVideo;
use App\Models\PostAudtion;
use Illuminate\Http\Request;
use App\Models\ArtistGallery;
use App\Models\ProductionAbout;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::post('promoted-post/ad', 'PromotePostController@getPostToShow');
Route::get('/load-more-videos', function(Request $request){
	if( $request->get('id') ){
		$categories = Video::join('video_categories', "videos.id", "=", "video_categories.video_id")->where([["videos.video_key", $request->id]])
		->groupBy("video_categories.category_id")
		->pluck('video_categories.category_id')
		->toArray();
		
		$categoryWiseVideo = Video::whereHas('video_category', function ($query) use ($categories) {
			$query->whereHas(
				'category',
				function ($query) use ($categories) {
					$query->orderBy('created_at', 'desc');
					$query->whereIn('id', $categories);
					$query->where('isRewardVideo', '=', 0);

				}
			);
		})
			->select('id', 'video_key', 'title', 'video_thumb_id', 'video_time', 'user_id', 'isRewardVideo', 'created_at')
			->orderBy('created_at', 'desc')
			->where('isRewardVideo', 0)
			->whereRaw("id NOT IN (SELECT post_id from promote_posts WHERE post_type='video' AND `status`='1' AND `is_paid`='1' AND ((`views` * `view_range`) + (`clicks` * `click_range`) < `amount`))")
			->with([
				'thumb' => function ($thumb) {
					$thumb->select('id', 'thumb');
				}
			])
			->with([
				'user' => function ($user) {
					$user->select('id', 'name', 'slug', 'photo');
				}
			])
			->withCount(['isWatchedVideo'])
			->withCount(['totalViews'])
			->with('watchTime')
			->where('video_key', '!=', $request->id)
			->paginate(20);

		if( $categoryWiseVideo ){
			return response()->json(['data' => view('elements.post.category-wise.index', compact('categoryWiseVideo'))->render(), 'status' => true]);
		}
		return response()->json(['data' => "", 'status' => true, 'is_next' => "0"]);
	}
	return response()->json(['data' => "", 'status' => false]);
});

Route::get('/search/cities', function (Request $request) {
	$cities = DB::table('tbl_cities')->select('name')->Where('name', 'like', '%' . $request['query'] . '%')->skip(0)->take(10)->get();
	return json_encode($cities);
})->name('api.search.cities.list');


Route::get('/search/artist/list', function (Request $request) {
	$artist = DB::table('artist_about')
	            ->select('profile_name')
	            ->whereRaw("profile_name like '%{$request['query']}%' OR (`tags` != '' AND FIND_IN_SET('{$request['query']}', `tags`))")
	            ->skip(0)->take(10)->get();
	return json_encode($artist);
})->name('api.search.artist.list');


Route::get('/search/artist-video/re-render', function (Request $request) {
	$video = ArtistVideo::join('artist_about', 'artist_about.user_id', '=', 'artist_video.user_id')->RightJoin('users', 'users.id', '=', 'artist_video.user_id')
		->leftJoin('followers', 'followers.user_id', '=', 'artist_video.user_id');

	//  dd($request['category']);
	if ($request['followers'] && !empty($request['followers']) && $request['followers'] != "Select") {
		// $video = $video->whereRaw("(SELECT count(followers.follows_id) as followers) >= {$request['followers']}");
		$followers = $request['followers']."000";
		if( $request['followers'] == 99 ){
			$followers = "0 && (SELECT count(followers.follows_id) AS follower) < 100000";
		}
		$video = $video->whereRaw("(SELECT count(followers.follows_id) AS follower) >= $followers");
	}

	if ($request['min_experience'] && !empty($request['min_experience']) && $request['min_experience'] != "Min:") {
		$video = $video->whereRaw("CAST(artist_about.exp AS UNSIGNED) >= {$request['min_experience']}");
	}

	if ($request['max_experience'] && !empty($request['max_experience']) && $request['max_experience'] != "Max:") {
		$video = $video->whereRaw("CAST(artist_about.exp AS UNSIGNED) <= {$request['max_experience']}");
	}

	if ($request['min_age'] && !empty($request['min_age']) && $request['min_age'] != "Min:") {
		$video = $video->where('artist_about.age', '>=', $request['min_age']);
	}
	if ($request['max_age'] && !empty($request['max_age']) && $request['max_age'] != "Max:") {
		$video = $video->where('artist_about.age', '<=', $request['max_age']);
	}

	if ($request['min_height'] && !empty($request['min_height']) && $request['min_height'] != "Min:") {
		$video = $video->where('artist_about.height', '>=', $request['min_height']);
	}
	if ($request['max_height'] && !empty($request['max_height']) && $request['max_height'] != "Max:") {
		$video = $video->where('artist_about.height', '<=', $request['max_height']);
	}

	if ($request['min_weight'] && !empty($request['min_weight']) && $request['min_weight'] != "Min:") {
		$video = $video->where('artist_about.weight', '>=', $request['min_weight']);
	}
	if ($request['max_weight'] && !empty($request['max_weight']) && $request['max_weight'] != "Max:") {
		$video = $video->where('artist_about.weight', '<=', $request['max_weight']);
	}

	if ($request['artist_name'] && !empty($request['artist_name'])) {
		$video = $video->where('artist_about.profile_name', 'like', '%' . $request['artist_name'] . '%');
	}

	if ($request['category'] && !empty($request['category'])) {
		// foreach ($request['category'] as $key => $cat) {
		//    if ($key == 0)
		//       $video = $video->where('artist_video.category', 'like', "%{$cat}%");
		//    else
		//       $video = $video->orwhere('artist_video.category', 'like', "%{$cat}%");
		// }
		$video = $video->whereIn('category', $request['category']);
	}

	if ($request['gender'] && !empty($request['gender']) && $request['gender'] != "Select") {
		$video = $video->where('artist_about.gender', '=', $request['gender']);
	}

	if ($request['lang'] && !empty($request['lang'])) {
		// $video = $video->where('artist_about.language', 'like', "%{$lang}%");
		$languages = "(";
		foreach ($request['lang'] as $key => $lang) {
			if ($key == 0)
				$languages .= "artist_about.language like '%$lang%'";
			else
				$languages .= " || artist_about.language like '%$lang%'";
				// $video = $video->orwhere('artist_about.language', 'like', "%{$lang}%");
		}
		$languages .= ")";
		$video = $video->whereRaw($languages);
	}

	if ($request['city'] && !empty($request['city'])) {
		$video = $video->where('artist_about.city', 'like', '%' . $request['city'] . '%');
	}

	$now = \Carbon\Carbon::now()->timestamp;
	$videos = $video->select('artist_video.*', 'artist_about.*', 'users.*', 'artist_video.id AS vid', 'users.slug AS slug', 'artist_video.slug AS _slug', 'artist_video.id AS iid')->whereNotNull('artist_video.id')->groupBy('artist_video.id')->orderBy(DB::raw("((users.plan = 2 AND users.purchase > 0 AND users.plan_expired_at > $now) || (users.is_premium > 0 AND users.premium_ended_at > $now))"), "DESC")->paginate(36);
	if ($request->ajax()) {
		return view('artist.inc.artist-video-data', compact('videos', 'request'))->render();
	}

	return json_encode($video);
})->name('api.search.artist-video.re-render');


Route::get('/search/artist-gallery/re-render', function (Request $request) {
	$Images = ArtistGallery::leftJoin('artist_about', 'artist_about.user_id', '=', 'artist_gallery.user_id')->RightJoin('users', 'users.id', '=', 'artist_gallery.user_id')
		->leftJoin('followers', 'followers.user_id', '=', 'artist_gallery.user_id');

	if ($request['followers'] && !empty($request['followers']) && $request['followers'] != "Select") {
		$followers = $request['followers']."000";
		if( $request['followers'] == 99 ){
			$followers = "0 && (SELECT count(followers.follows_id) AS follower) < 100000";
		}
		$Images = $Images->whereRaw("(SELECT count(followers.follows_id) AS follower) >= $followers");
	}

	if ($request['min_experience'] && !empty($request['min_experience']) && $request['min_experience'] != "Min:") {
		$Images = $Images->whereRaw("CAST(artist_about.exp AS UNSIGNED) >= {$request['min_experience']}");
	}

	if ($request['max_experience'] && !empty($request['max_experience']) && $request['max_experience'] != "Max:") {
		$Images = $Images->whereRaw("CAST(artist_about.exp AS UNSIGNED) <= {$request['max_experience']}");
	}

	if ($request['min_age'] && !empty($request['min_age']) && $request['min_age'] != "Min:") {
		$Images = $Images->where('artist_about.age', '>=', $request['min_age']);
	}
	if ($request['max_age'] && !empty($request['max_age']) && $request['max_age'] != "Max:") {
		$Images = $Images->where('artist_about.age', '<=', $request['max_age']);
	}

	if ($request['min_height'] && !empty($request['min_height']) && $request['min_height'] != "Min:") {
		$Images = $Images->where('artist_about.height', '>=', $request['min_height']);
	}
	if ($request['max_height'] && !empty($request['max_height']) && $request['max_height'] != "Max:") {
		$Images = $Images->where('artist_about.height', '<=', $request['max_height']);
	}

	if ($request['min_weight'] && !empty($request['min_weight']) && $request['min_weight'] != "Min:") {
		$Images = $Images->where('artist_about.weight', '>=', $request['min_weight']);
	}
	if ($request['max_weight'] && !empty($request['max_weight']) && $request['max_weight'] != "Max:") {
		$Images = $Images->where('artist_about.weight', '<=', $request['max_weight']);
	}

	if ($request['artist_name'] && !empty($request['artist_name'])) {
		$Images = $Images->where('artist_about.profile_name', 'like', '%' . $request['artist_name'] . '%');
	}

	if ($request['category'] && !empty($request['category'])) {
		$Images = $Images->whereIn("artist_gallery.category", $request['category']);
	}

	if ($request['gender'] && !empty($request['gender']) && $request['gender'] != "Select") {
		$Images = $Images->where('artist_about.gender', '=', $request['gender']);
	}

	if ($request['lang'] && !empty($request['lang'])) {
		$languages = "(";
		foreach ($request['lang'] as $key => $lang) {
			if ($key == 0)
				$languages .= "artist_about.language like '%$lang%'";
			else
				$languages .= " || artist_about.language like '%$lang%'";
				// $video = $video->orwhere('artist_about.language', 'like', "%{$lang}%");
		}
		$languages .= ")";
		$Images = $Images->whereRaw($languages);
	}

	if ($request['city'] && !empty($request['city'])) {
		$Images = $Images->where('artist_about.city', 'like', '%' . $request['city'] . '%');
	}

	$now = \Carbon\Carbon::now()->timestamp;
	$Images = $Images->select('artist_gallery.*', 'artist_about.*', 'users.slug AS slug', 'artist_gallery.slug AS _slug', 'artist_gallery.id AS iid')->whereNotNull('artist_gallery.id')->groupBy('artist_gallery.id')->orderBy(DB::raw("((users.plan = 2 AND users.purchase > 0 AND users.plan_expired_at > $now) || (users.is_premium > 0 AND users.premium_ended_at > $now))"), "DESC")->paginate(36);
	if ($request->ajax()) {
		return view('artist.inc.artist-gallery-data', compact('Images', 'request'))->render();
	}
	return json_encode($Images);
})->name('api.search.artist-gallery.re-render');


Route::get('/search/artist-profile/re-render', function (Request $request) {
	$artists = ArtistAbout::rightJoin('users', 'users.id', '=', 'artist_about.user_id')->leftJoin('followers', 'followers.user_id', '=', 'artist_about.user_id');

	if ($request['followers'] && !empty($request['followers']) && $request['followers'] != "Select") {
		$followers = $request['followers']."000";
		if( $request['followers'] == 99 ){
			$followers = "0 && (SELECT count(followers.follows_id) AS follower) < 100000";
		}
		$artists = $artists->whereRaw("(SELECT count(followers.follows_id) AS follower) >= $followers");
	}

	if ($request['min_experience'] && !empty($request['min_experience']) && $request['min_experience'] != "Min:") {
		$artists = $artists->whereRaw("CAST(artist_about.exp AS UNSIGNED) >= {$request['min_experience']}");
	}

	if ($request['max_experience'] && !empty($request['max_experience']) && $request['max_experience'] != "Max:") {
		$artists = $artists->whereRaw("CAST(artist_about.exp AS UNSIGNED) <= {$request['max_experience']}");
	}

	if ($request['min_age'] && !empty($request['min_age']) && $request['min_age'] != "Min:") {
		$artists = $artists->where('artist_about.age', '>=', $request['min_age']);
	}
	if ($request['max_age'] && !empty($request['max_age']) && $request['max_age'] != "Max:") {
		$artists = $artists->where('artist_about.age', '<=', $request['max_age']);
	}

	if ($request['min_height'] && !empty($request['min_height']) && $request['min_height'] != "Min:") {
		$artists = $artists->where('artist_about.height', '>=', $request['min_height']);
	}
	if ($request['max_height'] && !empty($request['max_height']) && $request['max_height'] != "Max:") {
		$artists = $artists->where('artist_about.height', '<=', $request['max_height']);
	}

	if ($request['min_weight'] && !empty($request['min_weight']) && $request['min_weight'] != "Min:") {
		$artists = $artists->where('artist_about.weight', '>=', $request['min_weight']);
	}
	if ($request['max_weight'] && !empty($request['max_weight']) && $request['max_weight'] != "Max:") {
		$artists = $artists->where('artist_about.weight', '<=', $request['max_weight']);
	}

	if ($request['artist_name'] && !empty($request['artist_name'])) {
		$artists = $artists->whereRaw("(profile_name like '%{$request['artist_name']}%' OR (`tags` != '' AND FIND_IN_SET('{$request['artist_name']}', `tags`)))");
	}

	if ($request['category'] && !empty($request['category'])) {
		$category = "(";
		foreach ($request['category'] as $key => $cat) {
			if ($key == 0)
				$category .= "artist_about.multiple_category like '%$cat%'";
			else
				$category .= " || artist_about.multiple_category like '%$cat%'";
		}
		$category .= ")";
		$artists = $artists->whereRaw($category);
		// $artists = $artists->whereIn("artist_about.category", $request['category']);
	}

	if ($request['gender'] && !empty($request['gender']) && $request['gender'] != "Select") {
		$artists = $artists->where('artist_about.gender', '=', $request['gender']);
	}

	if ($request['lang'] && !empty($request['lang'])) {
		$languages = "(";
		foreach ($request['lang'] as $key => $lang) {
			if ($key == 0)
				$languages .= "artist_about.language like '%$lang%'";
			else
				$languages .= " || artist_about.language like '%$lang%'";
				// $video = $video->orwhere('artist_about.language', 'like', "%{$lang}%");
		}
		$languages .= ")";
		$artists = $artists->whereRaw($languages);
	}

	if ($request['city'] && !empty($request['city'])) {
		$artists = $artists->where('artist_about.city', 'like', '%' . $request['city'] . '%');
	}

	$now = Carbon::now()->timestamp;
	$artists = $artists
	    ->select('artist_about.*', 'users.*')
	    ->whereNotNull('artist_about.id')
	    ->groupBy('artist_about.id')
	    ->orderBy(DB::raw("((users.plan = 2 AND users.purchase > 0 AND users.plan_expired_at > $now) || (users.is_premium > 0 AND users.premium_ended_at > $now))"), "DESC")
	    ->paginate(36);

	if ($request->ajax()) {
		return view('artist.inc.artist-profile-data', compact('artists', 'request'))->render();
	}
	return json_encode($artists);
})->name('api.search.artist-profile.re-render');


Route::get('/search/find-auditions/re-render', function (Request $request) {
	$auctions = PostAudtion::with('user')->whereRaw("(`status` IS NULL OR `status` <> 'close') AND `valid_till` >= ?", [\Carbon\Carbon::now()->format("Y-m-d")]);
	if ($request['category'] && !empty($request['category'])) {
		 $category = '';
		  foreach ($request['category'] as $key => $cat) {
				if ($key == 0)
					 $category .= "`category` LIKE '%{$cat}%'";
				else
					 $category .= " || `category` LIKE '%{$cat}%'";
		  }
		  // $auctions = $auctions->where('category', 'like', "%{$cat}%");
		  $auctions->whereRaw("($category)");
	}
	if ($request['lang'] && !empty($request['lang'])) {
		 $language = '';
		  foreach ($request['lang'] as $key => $lang) {
				if ($key == 0)
					 $language .= "`language` LIKE '%{$lang}%'";
				else
					 $language .= " || `language` LIKE '%{$lang}%'";

		  // $auctions = $auctions->where('language', 'like', "%{$lang}%");
		  }
		  $auctions->whereRaw("($language)");
	}

	if ($request['city'] && !empty($request['city'])) {
		$auctions = $auctions->where('city', 'like', '%' . $request['city'] . '%');
	}

	if ($request['title'] && !empty($request['title'])) {
		$auctions = $auctions->where('title', 'like', '%' . $request['title'] . '%');
	}
	$auctions = $auctions->paginate(36);
	if ($request->ajax()) {

		return view('audition.inc.find-auditions-data', compact('auctions', 'request'))->render();
	}
	return json_encode($auctions);
})->name('api.search.find-auditions.re-render');

Route::get('/search/production-houses/re-render', function (Request $request) {
	$prductionHouses = ProductionAbout::with('user');
	if ($request['category'] && !empty($request['category'])) {
		foreach ($request['category'] as $key => $cat) {
			if ($key == 0)
				$prductionHouses = $prductionHouses->where('multiple_category', 'like', "%{$cat}%");
			else
				$prductionHouses = $prductionHouses->where('multiple_category', 'like', "%{$cat}%");
		}
	}

	if ($request['lang'] && !empty($request['lang'])) {
		foreach ($request['lang'] as $key => $lang) {
			if ($key == 0)
				$prductionHouses = $prductionHouses->where('language', 'like', "%{$lang}%");
			else
				$prductionHouses = $prductionHouses->where('language', 'like', "%{$lang}%");

		}
	}

	if ($request['city'] && !empty($request['city'])) {
		$prductionHouses = $prductionHouses->where('city', 'like', '%' . $request['city'] . '%');
	}

	if ($request['title'] && !empty($request['title'])) {
		$prductionHouses = $prductionHouses->where('name', 'like', '%' . $request['title'] . '%');
	}
	
	$prductionHouses = $prductionHouses->paginate(10);
	if ($request->ajax()) {

		return view('production-house.inc.production-houses-data', compact('prductionHouses', 'request'))->render();
	}

	return json_encode($prductionHouses);
})->name('api.search.production-houses.re-render');